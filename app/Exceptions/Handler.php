<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *不应该被报告的异常类型列表.(不会做日志)
     * @var array
     */
    protected $dontReport = [

        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
        \App\Exceptions\ApiException::class,
        \App\Exceptions\MobileSuccessException::class,
        \App\Exceptions\NotPermissionException::class,
        \App\Exceptions\MobileErrorException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $sld=explode('.',$_SERVER['HTTP_HOST'])[0].'.';
        //只要二级域名带有api.
        if(is_numeric(strpos($sld,'api.'))){
            //若是接口的全返回json
            if(get_class($exception)=='Illuminate\Validation\ValidationException'){
                //表单验证字段返回信息
                $message = $exception->validator->errors()->first();
            }else{
                $message= $exception->getMessage();
            }
            return response()->json(json_error($message,[],$exception->getCode()), 200);
        }
        //状态码为200前端才能接收数据
        if($exception instanceof ApiException){
            //走下来如果是后台则后退并返回错误
            return back()->withInput($request->all())->withErrors(['error'=>$exception->getMessage()]);
//            return response()->json(json_error($exception->getMessage(),[],$exception->getCode()), 200);
        }elseif ($exception instanceof NotFoundHttpException){
            return mobileView('error','Page Not Found','Page Not Found !');
        }elseif($exception instanceof NotPermissionException){
            return response()->view('errors.403');
        }elseif ($exception instanceof MobileErrorException){
            return mobileView('error','错误提示',$exception->getMessage());
        }elseif ($exception instanceof MobileSuccessException){
            return mobileView('success','成功提示',$exception->getMessage());
        }
        //线下商城后台拦截请求表单验证，2018年8月22日15:07:57 cory
        if($exception instanceof  ValidationException){
            if($sld == env('APP_ADMIN_DOMAIN') ){
                $namespace = explode('/',$request->getRequestUri())[1];
                if($namespace == 'api'){
                    $errors = session('errors')->toArray();
                    return new JsonResponse( api_error(current($errors),$errors));
                }
            }
        }
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }
}
