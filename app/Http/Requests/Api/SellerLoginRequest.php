<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\BasicRequest;

class SellerLoginRequest extends BasicRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'		=> 'bail|required',
            'password'		=> 'bail|required',
        ];
    }

    public function messages()
    {
        return [
            'mobile.required'		=> '手机号码不能为空',
            'password.required'		=> '请填写登录密码',
        ];
    }
}
