<?php

namespace App\Http\Requests\AdminApi;

use Illuminate\Foundation\Http\FormRequest;

/**
 * 商家入驻表单验证
 * @package App\Http\Requests\Admin
 */
class AddSellerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ss'=>'bail|required|numeric',
            'mobile'=>'bail|required|digits:11',
            'merchantName'=>'bail|required|between:1,11',
//            'province_code'=>'required',
//            'city_code'=>'required',
            'district_code'=>'required',
            'merchantAddress'=>'bail|required|between:1,50',
            'category'=>'required',
            'business_time'=>'required',
            'average'=>'bail|required|numeric',
            'divide'=>'bail|required|numeric',
            'servicePhone'=>'bail|required|between:1,15',
            'logoimage'=>'bail|required|url',
            'banner'=>'bail|required|json',
            'recommend_mobile'=>'bail|required|digits:11', //推荐人号码
        ];
    }

    public function messages()
    {
        return [
            'ss.required'=>'缺少二维码参数',
            'ss.numeric'=>'二维码参数不正确',
            'merchantName.required'=>'请输入商家姓名',
            'merchantName.between'=>'商家姓名必须在10字之内',
            'mobile.required'=>'请输入手机号码',
            'mobile.digits'=>'手机号码格式不正确',
//            'province_code.required'=>'请选择省',
//            'city_code.required'=>'请选择市',
            'district_code.required'=>'请选择区县',
            'merchantAddress.required'=>'详细地址不能为空',
            'merchantAddress.between'=>'详细地址必须在50字以内',
            'category.required'=>'经营类目不能为空',
            'business_time.required'=>'请输入营业时间',
            'average.required'=>'人均消费不能为空',
            'average.numeric'=>'人均消费必须为数字',
            'divide.required'=>'商家让利不能为空',
            'divide.numeric'=>'商家让利必须为数字',
            'servicePhone.required'=>'客服电话不能为空',
            'servicePhone.between'=>'客服电话必须在15字以内',
            'logoimage.required'=>'商品logo不能为空',
            'logoimage.url'=>'商品logo必须是有效的地址',
            'banner.required'=>'banner图片不能为空',
            'banner.json'=>'banner图片必须是有效的JSON图片',
            'recommend_mobile.required'=>'请输入推荐人手机号码',
            'recommend_mobile.digits'=>'推荐人手机号码格式不正确',

        ];
    }
}
