<?php

namespace App\Http\Requests\AdminApi;

use Illuminate\Foundation\Http\FormRequest;

/**
 * 商家入驻表单验证
 * @package App\Http\Requests\Admin
 */
class LoginRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'username'=>'bail|required',
            'password'=>'bail|required',
            'identity'=>'bail|required|numeric',
        ];
    }
    public function messages()
    {
        return [
            'username.required'=>'请输入用户名称',
            'password.required'=>'请输入用户密码',
            'identity.required'=>'未选择登录类型',
            'identity.numeric'=>'登录类型不正确',
        ];
    }

}