<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class EditInvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Invoice_number'		=> 'bail|numeric',
            'express_number'		=> 'bail|numeric',
        ];
    }

    public function messages()
    {
        return [
            'Invoice_number.numeric'		=> '发票编号必须为数字',
            'express_number.unique'		=> '快递编号必须为数字',
        ];
    }
}
