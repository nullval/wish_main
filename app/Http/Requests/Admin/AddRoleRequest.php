<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//			'role_name'		=> 'bail|required|unique:admin_role,role_name',
            'role_name'		=> 'bail|required',
//			'password' => 'bail|required|min:6',
        ];
    }
    
    public function messages()
	{
		return [
            'role_name.required'		=> '角色名称不能为空',
            'role_name.unique'		=> '角色名称已存在',
//            'password.required'		=> '管理员密码不能为空',
//            'password.min'		=> '密码不能少于6位数',

		];
	}
}
