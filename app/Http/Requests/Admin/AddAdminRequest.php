<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'username'		=> 'bail|required|unique:admin,username',
//			'password' => 'bail|required|min:6',
        ];
    }
    
    public function messages()
	{
		return [
            'username.required'		=> '管理员用户名不能为空',
            'username.unique'		=> '管理员用户名已存在',
//            'password.required'		=> '管理员密码不能为空',
//            'password.min'		=> '密码不能少于6位数',

		];
	}
}
