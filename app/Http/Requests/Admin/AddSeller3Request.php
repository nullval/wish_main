<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddSeller3Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'		=> 'bail|required|mobile',
            'merchantId'		=> 'bail|required|mobile',
            'futureRate'		=> 'bail|required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'mobile.required'		=> '手机号码不能为空',
            'mobile.mobile'			=> '手机号码必须为可接收验证码格式正确的手机号码',
            'merchantId.required'		=> '子商户编码不能为空',
            'mobile.mobile'			=> '子商户编码必须为可接收验证码格式正确的手机号码',
            'futureRate.required'		=> '费率不能为空',
            'futureRate.numeric'		=> '费率必须为数字',
        ];
    }
}
