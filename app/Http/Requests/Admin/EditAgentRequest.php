<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class EditAgentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'		=> 'bail|required',
            'merchantName'		=> 'required',
//            'merchantAddress'		=> 'required',
//            'corpmanName'		=> 'required',
//            'corpmanId'		=> 'required',
//            'bankaccountNo'		=> 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'mobile.required'		=> '手机号码不能为空',
//            'mobile.mobile'			=> '手机号码必须为可接收验证码格式正确的手机号码',
            'password.required'		=> '请填写登录密码',
            'merchantName.required'		=> '代理名称不能为空',
            'password.min'			=> '登录密码最少为6位数',
            'password.max'			=> '登录密码超出最大字符限制',
            'shortName.required'		=> '代理简称不能为空',
//            'merchantAddress.required'		=> '代理地址不能为空',
//            'corpmanName.required'		=> '法人名字不能为空',
//            'corpmanId.required'		=> '法人身份证不能为空',
//            'corpmanMobile.required'		=> '法人联系手机不能为空',
//            'corpmanMobile.mobile'			=> '手机号码必须为可接收验证码格式正确的手机号码',
//            'bankaccountNo.required'		=> '开户行账号不能为空',
//            'bankaccountNo.numeric'		=> '开户行账号必须为数字',
            'bankaccountName.required'		=> '开户户名不能为空',
        ];
    }
}
