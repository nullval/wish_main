<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class WithdrawBankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_account'		=> 'bail|required|numeric',
            'account_name'		=> 'bail|required',
            'ibankno'		=> 'bail|required',

        ];
    }

    public function messages()
    {
        return [
            'bank_account.required'		=> '银行卡号不能为空',
            'bank_account.numeric'			=> '银行账户必须为数字',
            'account_name.required'		=> '持卡人不能为空',
            'ibankno.required'		=> '联行号不能为空',


        ];
    }
}
