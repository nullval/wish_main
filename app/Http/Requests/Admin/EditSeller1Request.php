<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class EditSeller1Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'		=> 'bail|required|mobile',
            'merchantAddress'		=> 'bail|required',
//            'businessLicense' => 'required',
//            'bankaccountName'=> 'required',
            'merchantName'=> 'required',
//            'corpmanName'=> 'required',
//            'corpmanId'=> 'required|identitycards',
//            'bankaccountName'=> 'required',
//            'bankaccountNo'=> 'required|numeric',
            'divide'=>'required|numeric',
            'average'=>'required|numeric|minus',
        ];
    }

    public function messages()
    {
        return [
            'mobile.required'		=> '手机号码不能为空',
            'mobile.mobile'			=> '手机号码必须为可接收验证码格式正确的手机号码',
            'password.required'		=> '请填写登录密码',
            'merchantName.required'		=> '商户名称不能为空',
            'password.min'			=> '登录密码最少为6位数',
            'password.max'			=> '登录密码超出最大字符限制',
            'shortName.required'		=> '商户简称不能为空',
            'merchantAddress.required'		=> '商户地址不能为空',
//            'corpmanName.required'		=> '法人名字不能为空',
//            'corpmanId.required'		=> '法人身份证不能为空',
//            'corpmanId.identitycards'		=> '法人身份证格式不正确',
            'corpmanMobile.required'		=> '法人联系手机不能为空',
            'corpmanMobile.mobile'			=> '手机号码必须为可接收验证码格式正确的手机号码',
//            'bankaccountNo.required'		=> '开户行账号不能为空',
//            'bankaccountNo.numeric'		=> '开户行账号必须为数字',
//            'bankaccountName.required'		=> '开户户名不能为空',
//            'businessLicense.required'		=> '营业执照号不能为空',
            'divide.required'		=> '商家让利必填',
            'divide.numeric'		=> '商家让利必须为数字',
            'average.required'		=> '人均金额必填',
            'average.numeric'		=> '人均金额必须为数字',
            'average.minus'		=> '人均金额不能为负数',
        ];
    }
}
