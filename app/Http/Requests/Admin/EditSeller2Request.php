<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class EditSeller2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'		=> 'bail|required',
            'bankaccountNo'		=> 'bail|required|numeric',
            'certNo'		=> 'bail|required',
        ];
    }

    public function messages()
    {
        return [
            'mobile.mobile'			=> '子商户编码必须为可接收验证码格式正确的手机号码',
            'bankaccountNo.required'		=> '银行卡号不能为空',
            'bankaccountNo.numeric'		=> '银行卡号必须为数字',
            'certNo.required'		=> '证件号码不能为空',
        ];
    }
}
