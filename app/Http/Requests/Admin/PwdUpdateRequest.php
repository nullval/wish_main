<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PwdUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_pwd'		=> 'bail|required',
            'new_pwd'		=> 'bail|required|between:6,12',
            're_pwd'		=> 'bail|required|between:6,12',
        ];
    }

    public function messages()
    {
        return [
            'old_pwd.required'		=> '原密码不能为空',
            'new_pwd.required'		=> '新密码不能为空',
            're_pwd.required'		=> '确认密码不能为空',
            'new_pwd.between'		=> '新密码长度必须在6-12位之间',
            're_pwd.between'		=> '确认密码长度必须在6-12位之间',
        ];
    }
}
