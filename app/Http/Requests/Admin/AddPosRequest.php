<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddPosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'terminalId'		=> 'bail|required|unique:pos,terminalId',
        ];
    }
    
    public function messages()
	{
		return [
            'terminalId.required'		=> '请输入终端号',
            'terminalId.unique'		=> '该pos机已存在',
		];
	}
}
