<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class GhtAddSellerStepOneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'		=> 'bail|required|mobile',
            'password'		=> 'bail|required|min:6|max:32',
            'ibankno'		=> 'bail|required',

        ];
    }

    public function messages()
    {
        return [
            'mobile.required'		=> '手机号码不能为空',
            'mobile.mobile'			=> '手机号码必须为可接收验证码格式正确的手机号码',
            'password.required'		=> '请填写登录密码',
            'password.min'			=> '登录密码最少为6位数',
            'password.max'			=> '登录密码超出最大字符限制',

        ];
    }
}
