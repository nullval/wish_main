<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddOperateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'		=> 'bail|required|mobile',
//            'parent_mobile'		=> 'bail|required_if:inviter_user_mobile|mobile',
//            'inviter_user_mobile'		=> 'bail|required_if:parent_mobile|mobile'
        ];
    }

    public function messages()
    {
        return [
            'mobile.required'		=> '运营中心手机号不能为空',
            'mobile.mobile'			=> '运营中心手机号格式不正确',
            'parent_mobile.mobile'			=> '上级运营中心手机号格式不正确',
            'mobile.mobile'			=> '推荐人手机格式不正确',
        ];
    }
}
