<?php

namespace App\Http\Requests\Td;

use Illuminate\Foundation\Http\FormRequest;

class AdminLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'		=> 'bail|required',
            'password'		=> 'bail|required',
        ];
    }

    public function messages()
    {
        return [
            'username.required'		=> '管理员名称不能为空',
            'password.required'		=> '请填写登录密码',
        ];
    }
}
