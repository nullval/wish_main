<?php

namespace App\Http\Requests\Td;

use Illuminate\Foundation\Http\FormRequest;

class ForgetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'		=> 'bail|required|mobile',
            'password'		=> 'bail|required|confirmed',
            'password_confirmation'		=> 'bail|required',
            'sms_code'		=> 'bail|required',
        ];
    }
    
    public function messages()
    {
        return [
            'mobile.required'		=> '手机号码不能为空',
            'mobile.mobile'		=> '请输入正确的手机号码',
            'password.required'		=> '请填写新密码',
            'password.confirmed'		=> '新密码必须与确认密码一致',
            'password_confirmation.required'		=> '请填写确认密码',
            'sms_code.required'		=> '验证码不能为空',
        ];
    }
}
