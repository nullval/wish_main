<?php

namespace App\Http\Requests\Seller;

use Illuminate\Foundation\Http\FormRequest;

class WithdrawRequset extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount'		=> 'bail|required|numeric|min:2',
            'bank_account'		=> 'bail|required',
            'account_name'		=> 'bail|required',
        ];
    }

    public function messages()
    {
        return [
            'amount.required'		=> '提现金额不能为空',
            'amount.min'		=> '提现金额不能小于2元',
            'amount.numeric'		=> '提现金额必须为数字',
            'bank_account.required'		=> '银行卡号不能为空',
            'bank_account.numeric'		=> '银行卡号必须为数字',
            'account_name.required'		=> '持卡人不能为空',
        ];
    }
}
