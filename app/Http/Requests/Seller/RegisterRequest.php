<?php

namespace App\Http\Requests\Seller;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		return [
			'mobile'		=> 'bail|required|mobile',
			'password'		=> 'bail|required|min:6|max:32|confirmed',
			'password_confirmation'=> 'required',
			'code'		=> 'bail|required',
		];
	}

	public function messages()
	{
		return [
			'mobile.required'		=> '手机号码不能为空',
			'mobile.mobile'			=> '手机号码必须为可接收验证码格式正确的手机号码',
			'password.required'		=> '请填写登录密码',
			'password.min'			=> '登录密码最少为6位数',
			'password.max'			=> '登录密码超出最大字符限制',
			'password_confirmation.required'	=> '请填写确认密码',
			'password.confirmed'	=> '两次密码输入不一致',
			'code.required'		=> '请填写短信验证码',
		];
	}
}
