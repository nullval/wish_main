<?php

namespace App\Http\Requests\Seller;

use Illuminate\Foundation\Http\FormRequest;

class RoleAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'		=> 'bail|required|mobile',
            'role_name'		=> 'bail|required|numeric',
            'role_job'		=> 'bail|required',
        ];
    }

    public function messages()
    {
        return [
            'role_job.required'		=> '店员职位不能为空',
            'mobile.required'		=> '店员账号不能为空',
            'mobile.mobile'		=> '店员账号必须为可接收验证码格式正确的手机号码',
            'role_name.required'		=> '店员编号不能为空',
            'role_name.numeric'		=> '店员编号必须为数字',
        ];
    }
}
