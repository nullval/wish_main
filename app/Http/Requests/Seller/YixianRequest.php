<?php

namespace App\Http\Requests\Seller;

use Illuminate\Foundation\Http\FormRequest;

class YixianRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'TaxRegistrationNo'		=> 'bail|required',
            'bankarea'		=> 'bail|required',
            'bankUrl1'		=> 'bail|required',
            'bankUrl2'		=> 'bail|required',
            'ICurl1'		=> 'bail|required',
            'ICurl2'		=> 'bail|required',
            'merchantICurl'		=> 'bail|required',
        ];
    }

    public function messages()
    {
        return [
            'TaxRegistrationNo.required'		=> '税务登记证号不能为空',
            'bankarea.required'		=> '开户行所在地区不能为空',
            'bankUrl1.required'		=> '请上传银行卡正面照片',
            'bankUrl2.required'		=> '请上传银行卡反面照片',
            'ICurl1.required'		=> '请上传商户身份证正面照片',
            'ICurl2.required'		=> '请上传商户身份证反面照片',
            'merchantICurl.required'		=> '请上传商户手持身份证正面照片',
        ];
    }
}
