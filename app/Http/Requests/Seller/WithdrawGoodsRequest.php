<?php

namespace App\Http\Requests\Seller;

use Illuminate\Foundation\Http\FormRequest;

class WithdrawGoodsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount'		=> 'bail|required|numeric|min:3',
        ];
    }
    
    public function messages()
    {
        return [
            'amount.required'		=> '提现金额不能为空',
            'amount.numeric'		=> '提现金额必须为数字',
            'amount.min'		=> '提现金额不能小于3元',
        ];
    }
}
