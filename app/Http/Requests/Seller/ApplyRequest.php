<?php

namespace App\Http\Requests\Seller;

use Illuminate\Foundation\Http\FormRequest;

class ApplyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'companyName'		=> 'bail|required',
            'companyAddress'		=> 'bail|required',
            'businessLicense'		=> 'bail|required',
//            'businessimage'		=> 'bail|required',
            'organizationCode'		=> 'bail|required',
            'businessLicense'		=> 'bail|required',
//            'businessimage'		=> 'bail|required',
            'organizationCode'		=> 'bail|required',
            'telephone'		=> 'bail|required|numeric',
            'legalName'		=> 'bail|required',
            'legalIds'		=> 'bail|required|numeric',
            'legalPhone'		=> 'bail|required',
            'parentBankName'		=> 'bail|required',
            'bankName'		=> 'bail|required',
            'account_name'		=> 'bail|required',
            'account_no'		=> 'bail|required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'companyName.required'		=> '商家名称不能为空',
            'companyAddress.required'		=> '商家地址不能为空',
            'businessLicense.required'		=> '营业执照号不能为空',
//            'businessimage.required'		=> '手机号码不能为空',
            'organizationCode.required'		=> '组织机构代码不能为空',
            'telephone.required'		=> '联系电话不能为空',
            'legalName.required'		=> '法人姓名不能为空',
            'legalIds.required'		=> '法人证件号码不能为空',
            'legalPhone.required'		=> '法人手机号码不能为空',
            'parentBankName.required'		=> '开户银行名称不能为空',
            'bankName.required'		=> '开户行支行名称不能为空',
            'account_name.required'		=> '持卡人姓名不能为空',
            'account_no.required'		=> '银行卡号不能为空',
            'pos_num.required'		=> 'pos机数量不能为空',
            'pos_amount.required'		=> '请输入金额',
            'pos_num.numeric'		=> 'pos机数量必须为数值',
            'pos_amount.numeric'		=> '打款金额必须为数值',
            'telephone.numeric'		=> '请输入正确的联系电话',
            'legalIds.numeric'		=> '请输入正确的法人证件号',
            'legalPhone.mobile'		=> '请输入正确的手机号码',
            'account_no.numeric'		=> '请输入正确的银行卡号',
        ];
    }
}
