<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class EditPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password'		=> 'bail|required',
            'new_password'		=> 'bail|required|confirmed|min:6',
            'new_password_confirmation'		=> 'bail|required',
        ];
    }

    public function messages()
    {
        return [
            'old_password.required'		=> '请输入旧密码',
            'new_password.required'		=> '请输入新密码',
            'new_password.min'		=> '新密码不能少于六位',
            'new_password.confirmed'		=> '新密码必须与确认密码一致',
            'new_password_confirmation.required'		=> '请填写确认密码',
        ];
    }
}
