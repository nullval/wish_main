<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class AddSellerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'		=> 'bail|required|mobile',
            'terminalId'		=> 'bail|required',
            'servicePhone'		=> 'bail|required|mobile',
            'merchantName'		=> 'bail|required',
            'merchantAddress'		=> 'bail|required',
            'corpmanName'		=> 'bail|required',
            'corpmanId'		=> 'bail|required',
            'corpmanMobile'		=> 'bail|required|mobile',
            'bankaccountNo'		=> 'bail|required|numeric',
            'futureRate'		=> 'bail|required|numeric|min:3|max:20',

        ];
    }

    public function messages()
    {
        return [
            'mobile.required'		=> '手机号码不能为空',
            'mobile.mobile'			=> '手机号码必须为可接收验证码格式正确的手机号码',
            'terminalId.required'		=> 'POS机终端号不能为空',
            'servicePhone.required'		=> '客服电话不能为空',
            'servicePhone.mobile'			=> '客服电话必须为可接收验证码格式正确的手机号码',
            'merchantName.required'		=> '商户名称不能为空',
            'password.min'			=> '登录密码最少为6位数',
            'password.max'			=> '登录密码超出最大字符限制',
            'shortName.required'		=> '商户简称不能为空',
            'merchantAddress.required'		=> '商户地址不能为空',
            'corpmanName.required'		=> '法人名字不能为空',
            'corpmanId.required'		=> '法人身份证不能为空',
            'corpmanMobile.required'		=> '法人联系手机不能为空',
            'corpmanMobile.mobile'			=> '手机号码必须为可接收验证码格式正确的手机号码',
            'bankaccountNo.required'		=> '开户行账号不能为空',
            'bankaccountNo.numeric'		=> '开户行账号必须为数字',
            'bankaccountName.required'		=> '开户户名不能为空',
            'futureRate.required'		=> '费率不能为空',
            'futureRate.numeric'		=> '费率必须为数字',
            'futureRate.min'		=> '费率不能低于3',
            'futureRate.max'		=> '费率不能高于20',
        ];
    }
}
