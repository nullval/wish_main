<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class PosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'seller_mobile'		=> 'bail|mobile',

        ];
    }

    public function messages()
    {
        return [
//            'seller_mobile.mobile'			=> '商家名必须为可接收验证码格式正确的手机号码',

        ];
    }
}
