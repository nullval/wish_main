<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'		=> 'bail|required',
            'sms_code'		=> 'bail|required',
        ];
    }

    public function messages()
    {
        return [
            'mobile.required'		=> '用户名不能为空',
            'mobile.mobile'			=> '请输入正确的手机号码',
            'sms_code.required'		=> '请输入短信验证码',

        ];
    }
}
