<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class AddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'		=> 'bail|required|mobile',
            'recipient_name'		=> 'bail|required',
            'address'		=> 'bail|required',
            'postalcode'		=> 'bail|required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'mobile.required'		=> '手机号码不能为空',
            'mobile.mobile'			=> '用户名必须为可接收验证码格式正确的手机号码',
            'recipient_name.required'		=> '收件人不能为空',
            'address.required'		=> '地址不能为空',
            'postalcode.required'		=> '邮政编码不能为空',
            'postalcode.numeric'		=> '邮政编码必须为数字',

        ];
    }
}
