<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class AddOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact_user'		=> 'bail|required',
            'contact_address'		=> 'bail|required',
            'count'		=> 'bail|required',
//            'id_arr'		=> 'bail|required',
            'contact_mobile'		=> 'bail|required|mobile',
        ];
    }

    public function messages()
    {
        return [
            'contact_mobile.required'		=> '联系电话不能为空',
            'contact_mobile.mobile'			=> '请输入正确的手机号码',
            'contact_user.required'		=> '收货人不能为空',
            'contact_address.required'		=> '收货地址不能为空',
            'count.required'		=> '申请数量不能为空',
            'id_arr.required'		=> '地推二维码编号不能为空',
        ];
    }
}
