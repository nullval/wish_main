<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class BankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_account'		=> 'bail|required|numeric',
            'account_name'		=> 'bail|required',
//            'account_bank'		=> 'bail|required',
//            'bank_code'		=> 'bail|required',
            'ibankno'		=> 'bail|required|numeric',
            'sms_code'		=> 'bail|required',
        ];
    }
    
    public function messages()
    {
        return [
            'bank_account.required'		=> '银行账户不能为空',
            'bank_account.numeric'			=> '银行账户必须为数字',
            'account_name.required'		=> '持卡人不能为空',
            'account_bank.required'		=> '开户银行不能为空',
            'bank_code.required'		=> '开户银行代码不能为空',
            'ibankno.required'		=> '联行号不能为空',
            'ibankno.numeric'		=> '联行号必须为数字',
            'sms_code.required'		=> '请输入短信验证码',

        ];
    }
}
