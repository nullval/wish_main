<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'bank'		=> 'bail|numeric',
//            'password'		=> 'bail|required|min:6|max:32|confirmed',
//            'password_confirmation'		=> 'bail|required|min:6|max:32',
        ];
    }

    public function messages()
    {
        return [
//            'bank.numeric'		=> '请填写登录密码',
//            'old_password.min'			=> '登录密码最少为6位数',
//            'old_password.max'			=> '登录密码超出最大字符限制',
//            'password.required'		=> '请填写新的登录密码',
//            'password.min'			=> '新的登录密码最少为6位数',
//            'password.max'			=> '新的登录密码超出最大字符限制',
//            'password.confirmed'	=> '两次密码输入不一致',
//            'password_confirmation.required'		=> '请填写确认密码',
//            'password_confirmation.min'			=> '确认密码最少为6位数',
//            'password_confirmation.max'			=> '确认密码超出最大字符限制',
        ];
    }
}
