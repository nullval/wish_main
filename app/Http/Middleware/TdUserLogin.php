<?php

namespace App\Http\Middleware;

use Closure;

class TdUserLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        var_dump(session('admin_info'));exit;
        //登录验证
        if(empty(session('td_user_info'))){
            refresh('/login.html');
        }

        return $next($request);
    }
}
