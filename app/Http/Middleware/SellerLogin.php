<?php

namespace App\Http\Middleware;

use Closure;

class SellerLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //登录验证
        if(empty(session('seller_info'))){
            refresh('/login');
        }

        //后台权限验证（预留 颗粒做细的时候用）
        //判断是否是店长登录
        if (session('role_info') != ''){
            //店员登录
            check_admin_permission('seller');
        }

        return $next($request);
    }
}
