<?php

namespace App\Http\Middleware;

use Closure;

class Login
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    //验证用户是否登录
    public function handle($request, Closure $next)
    {
        if(empty(session('user_info'))){
            refresh('/login');
        }
        return $next($request);
    }
}
