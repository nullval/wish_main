<?php

namespace App\Http\Middleware;

use Closure;

class AgentLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //登录验证
        if(empty(session('agent_info'))){
            refresh('/login');
        }
        return $next($request);
    }
}
