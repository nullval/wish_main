<?php

namespace App\Http\Middleware;

use Closure;

class PushQrLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //登录验证
        if(empty(session('pushQr_user_info'))&&empty(session('agent_info'))){
            refresh('/login?member_type=2');
        }
        return $next($request);
    }
}
