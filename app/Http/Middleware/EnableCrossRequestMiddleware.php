<?php
namespace App\Http\Middleware;

use Closure;
use League\Flysystem\Exception;
/**
 * 授权跨域中间件
 * User: Administrator
 * Date: 2018/8/28 0028
 * Time: 17:53
 */
class EnableCrossRequestMiddleware{

    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if(config('app.env')=='dev'){
            //测试服允许所有跨域
            $response->header('Access-Control-Allow-Origin', '*');
            $response->header('Access-Control-Allow-Headers', 'Origin, Content-Type, Cookie, X-CSRF-TOKEN, Accept, Authorization, X-XSRF-TOKEN');
            $response->header('Access-Control-Expose-Headers', 'Authorization, authenticated');
            $response->header('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, OPTIONS');
            $response->header('Access-Control-Allow-Credentials', 'true');
        }else{
            $origin = $request->server('HTTP_ORIGIN') ? $request->server('HTTP_ORIGIN') : '';
            $allow_origin = [
                'http://localhost:8082',
                'http://cs-ws-web.hychqt.com', //测试服
                'http://ws-web.hychqt.com', //正式服
            ];
            if (in_array($origin, $allow_origin)) {
                $response->header('Access-Control-Allow-Origin', $origin);
                $response->header('Access-Control-Allow-Headers', 'Origin, Content-Type, Cookie, X-CSRF-TOKEN, Accept, Authorization, X-XSRF-TOKEN');
                $response->header('Access-Control-Expose-Headers', 'Authorization, authenticated');
                $response->header('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, OPTIONS');
                $response->header('Access-Control-Allow-Credentials', 'true');
            }
        }
        return $response;
    }
}