<?php

namespace App\Http\Middleware;

use App\Models\AdminPermission;
use Closure;

class AdminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        var_dump(session('admin_info'));exit;
        //登录验证
        if(empty(session('admin_info'))){
            refresh('/login');
        }

        //后台权限验证（预留 颗粒做细的时候用）
        $AdminPermission=new AdminPermission();
        $AdminPermission->check_admin_permission();

        return $next($request);
    }
}
