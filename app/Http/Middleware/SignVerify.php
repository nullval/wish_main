<?php

namespace App\Http\Middleware;

use Closure;
use League\Flysystem\Exception;

class SignVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //本地不算签名
        if(config('app.env')=='dev'){
            return $next($request);
        }
    	$old_param=$param = $request->except(['s']);
    	//没传入签名
        if(!isset($param['sign'])){
			echo json_encode(arr_post(0,'验证签名失败'));exit;
//            return redirect()->route('sign_verify_failed');
        }
    	
    	// null 格式化成空字符串
		clear_null($param);

//        new_logger('sign.log','客户端ip',[$_SERVER["SERVER_ADDR"]]);
//        new_logger('sign.log','客户端请求的参数',$param);

    	$sign = $param['sign'];
    	unset($param['sign']);
        //验签加入状态码
        $param['code']=env('api_code');
		ksort($param);
		//logger($param);
    	$param2 = [];
    	foreach($param as $k => $v){
			$param2[] = $k.'='.$v;
		}
//        new_logger('sign.log','服务端拼接的字段',[implode('&',$param2)]);
		$param_sign = strtolower(md5(implode('&',$param2)));
		
//		dd($param,$param2,$param_sign,$sign);
//        new_logger('sign.log','计算的签名',[$param_sign]);
//        new_logger('sign.log','客户端的签名',[$sign]);
		if($param_sign != $sign){
//            new_logger('sign.log','结果',['算签失败']);
//            new_logger('sign.log','结果',['-------']);
//			return json_error('error');
			echo json_encode(arr_post(0,'验证签名失败'));exit;
			//return redirect()->route('sign_verify_failed');
		}else{
//            new_logger('sign.log','结果',['算签成功']);
//            new_logger('sign.log','结果',['-------']);
			return $next($request);
		}
    }
}
