<?php
namespace App\Http\Controllers\Api;
use App\Exceptions\MobileErrorException;
use App\Exceptions\MobileSuccessException;
use App\Http\Requests\Api\AddSellerRequest;
use App\Models\BankList;
use App\Models\Message;
use App\Models\PushQrQrcode;
use App\Models\PushQrUser;
use App\Models\RuzhuMerchantBasic;
use App\Models\RuzhuMerchantBasicCopy;
use Illuminate\Http\Request;


/**
 * 地推二维码接口
 */
class PushQrController extends CommonController {

    /**
     * 地推二维码跳转接口
     */
	public function create_seller(Request $request){
        $id=request('id');
	    $info=PushQrQrcode::where(['id'=>$id])->first();
	    if(!isset($info->id)){
	        throw new MobileErrorException('二维码不存在');
        }
        if($info->status==3){
            throw new MobileErrorException('该二维码已失效');
        }elseif($info->status==1&&$info->seller_id==0){
	        //未激活
            //入驻商家
            //银行列表
            $bank_list=BankList::get();
            //城市列表
            $city_arr = get_ght_attr('city');
            //经营类别列表
            $category_arr = get_ght_attr('category');
            //去掉名字中的英文名
            foreach ($category_arr as $value){
                $arr = explode(" ",$value->categoryName);
                $value->categoryName = $arr[1];
            }
            //银行代码列表
            $bank_arr = array();
            $bank_arr1 = get_ght_attr('bank');
            foreach ($bank_arr1 as $item => $value){
                foreach ($bank_list as $k => $v){
                    if ($v['bank_name'] == $value->bankName){
                        $bank_arr[$item]->bankCode =  $value->bankCode;
                        $bank_arr[$item]->bankName =  $value->bankName;
                    }
                }
            }
            $qr_info=PushQrUser::where(['id'=>$info->qr_user_id])->first();
            return view('api.pushQr.seller_add',compact('bank_list','city_arr','category_arr','bank_arr','qr_info'));
        }elseif($info->status==2&&$info->seller_id!=0){
	        //已激活
	        //商家id
            $uid=$info->seller_id;
            $rmb_info=RuzhuMerchantBasic::where(['uid'=>$uid])->first();
            if(!isset($rmb_info->merchantId)||$rmb_info->status!=3){
                throw new MobileErrorException('商家审核尚未通过，暂时无法使用该支付码');
            }
	        //跳转到商家支付
            $type=0;
            return redirect("http://server.worldours.com/wechat/ght?sid='.$uid.'&type='.$type");
        }else{
            throw new MobileErrorException('未知错误');
        }
    }



    public function add_seller_submit(AddSellerRequest $addSellerRequest){
        $request_info=$addSellerRequest->all();
        //登记商家入驻信息
        $Basic=new RuzhuMerchantBasicCopy();
        $result=$Basic->add_seller($request_info,1,$request_info['qr_user_id']);
        if($result['code']==1){
            //通知客服去 审核
            if (env('APP_DEBUG') === false){
                $Message=new Message();
                $Message->send_sms(15986733270,15);
            }
            //更新地推二维码状态
            $seller_id=$result['data']['uid'];
            PushQrQrcode::where(['id'=>$request_info['qr_id']])->update([
                'status'=>2,
                'seller_id'=>$seller_id
            ]);
            throw new MobileSuccessException("商家信息已提交，等待审核");
        }elseif ($result['code']==0){
            return back()->withInput($addSellerRequest->all())->withErrors(['error'=>$result['message']]);
        }
    }

    /**
     * 得到银行联行号
     * @param Request $request
     * @return mixed
     */
    public function get_bankNo(Request $request){
        $bank_name=$request->input('bank_name');
        $info=BankList::where('bank_name','like','%'.$bank_name.'%')->first(['lbnkNo','bank_name']);
        if(empty($info->lbnkNo)){
            return json_error('找不到相应的银行信息');
        }else{
            return json_success('银行信息获取成功',$info);
        }
    }

}