<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ApiException;
use App\Jobs\handleBuyPosCopyOrder;
use App\Jobs\handleBuyPosFirstOrder;
use App\Jobs\handleQrOrder;
use App\Jobs\Test;
use App\Jobs\TestJob;
use App\Libraries\BankSdk;
use App\Libraries\Df\esyto\esyto;
use App\Libraries\Df\ght\Daifu;
use App\Libraries\Huanxun\Huanxun;
use App\Libraries\msg\REST;
use App\Libraries\msg\WechatPush;
use App\Models\Api\Geocoding;
use App\Models\Bank;
use App\Models\Gift;
use App\Models\UserBehavior;
use App\Services\Validation;
use DeepCopy\f001\B;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Middleware\TrimStrings;
use App\Http\Requests\Api\SellerLoginRequest;
use App\Jobs\handleBuyPosOrder;
use App\Jobs\handleBuyTdOrder;
use App\Jobs\handleShopOrder;
use App\Jobs\Withdraw;
use App\Libraries\gaohuitong\Ruzhu;
use App\Libraries\gaohuitong_pay\gaohuitong_pay;
use App\Libraries\JPush\JPush;
use App\Models\Agent;
use App\Models\AgentOperate;
use App\Models\JavaIncrement;
use App\Models\JushMessage;
use App\Models\MerchantApply;
use App\Models\Message;
use App\Models\NineFloor;
use App\Models\Order;
use App\Models\Pos;
use App\Models\PosOrderDetail;
use App\Models\PosPre;
use App\Models\RuzhuMerchantBasic;
use App\Models\RuzhuMerchantBusinesss;
use App\Models\Seller;
use App\Models\SysConfig;
use App\Models\User;
use App\Models\UserPurse;
use App\Models\UserWithdraw;
use App\Models\VOrder;
use App\Models\Weachat;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\CountValidator\Exception;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use App\Models\UserFreezeLog;

class TestController extends CommonController
{
    public function test(Request $request){
//        $df=new esyto(false);
//        $result=$df->query('0015306993513440	');
//        12252525
//        $data = [
//            'order_sn' =>'22252525', //商户平台订单号
//            'subject' => '代付提现', //收款人姓名
//            'amount' => 2000, // 单位元
//            'acctNo' => '6214857551829312', //收款人卡号
//            'accBankName' => '李海', //收款人手机号码
//            'bankname' => '招商银行', //
//        ];
//        $result = $df->pay($data);
//        if($result){
//            dd($result);
//        }else{
//            echo $df->error;
//        }
//        exit;
        dd((new Daifu())->queryBalance());
//        $Huanxun=new Huanxun();
//        $Huanxun->toCertificate([
//            'customerCode'=>'13662579183'
//        ]);
        exit;
//        $Huanxun->open([
//            'customerCode'=>'13662579183',
//            'identityNo'=>'440982199212194051',
//            'userName'=>'李伟裕',
//            'mobiePhoneNo'=>'13662579183',
//            'pageUrl'=>url('huanxun/open?action=success'),
//            's2sUrl'=>url('huanxun/open')
//        ]);

//        $Huanxun->bankCard([
//            'customerCode'=>'13662579183',
//            'pageUrl'=>url('huanxun/bankCard?action=success'),
//            's2sUrl'=>url('huanxun/bankCard')
//        ]);
//        $param = \request()->validate([
//            'name'    => 'required|exists:fund_admin,name',
//            'password' => 'required',
////       'captcha'  => 'required|captcha'
//        ],[
//            'captcha.captcha'  => '验证码错误',
//        ]);
//        $Bank=new Bank();
//        $bank_purse_id=$Bank->get_bank_purse_id();
//        $buyer_purse_id=$Bank->userWallet(1,7,2)->purse_id;
//        $Bank->doApiTransfer($bank_purse_id,$buyer_purse_id,9000,1000001,
//            '2018/05/09号根据财务要求,系统拨款给该商户',
//            '系统拨商家充值款给商家');
        exit;
//                        $Bank=new Bank();
//        $sys_point_purse_id=$Bank->get_sys_purse_id(3);
//        $seller_point_purse_id=$Bank->userWallet(8,3,1)->purse_id;
//        $Bank->doApiTransfer($sys_point_purse_id,$seller_point_purse_id,100000,10000,
//            '',
//            '');exit;
//        $JpushMessage=new JushMessage();
//        $JpushMessage->send_order_status('000720180429121608539792',2,0.5,1);
//                $Bank=new Bank();
//        $sys_point_purse_id=$Bank->get_sys_purse_id(3);
//        $seller_point_purse_id=$Bank->userWallet(3,3,2)->purse_id;
//        $Bank->doApiTransfer($sys_point_purse_id,$seller_point_purse_id,10000,10000,
//            '',
//            '');exit;
//        exit;
//        $AgentOperate=new AgentOperate();
//        dd($AgentOperate->find_operate_level_by_time());
//        UserBehavior::whereDate('created_at', '<',date('Y-m-d',strtotime('-1 month')))->delete();exit;
//        ignore_user_abort(true);
//        //允许长时间执行
//        set_time_limit(0);
//        $Bank=new Bank();
//        $UserPurse=new UserPurse();
//        $UserPurse->where(['purse_type'=>3,'owner_type'=>2])->where('balance','>',0)->get()
//            ->each(function ($v) use($Bank) {
//                $back_amount=$v->balance-$v->freeze_value;
//                $seller_point_purse_id=$Bank->userWallet($v->owner_id,10001,2,['purse_id'])->purse_id;
//                $Bank->doApiTransfer($v->purse_id,$seller_point_purse_id,$back_amount,10000009,
//                    '原系统待释放积分转移',
//                    '原系统待释放积分转移');
//            });exit;
//        if(time()<1523635800){
//            echo 2323;exit;
//        }
//        echo 'ok';
//        $Bank=new Bank();
//        $sys_purse_id=$Bank->get_sys_purse_id(1);
//        //获取商家钱包id
//        $seller_purse_id=$Bank->userWallet(163,1,2)->purse_id;
//        //系统收益到商家收益钱包
//        $Bank->doApiTransfer($sys_purse_id,$seller_purse_id,100000,10000008,
//            '',
//            '');
//        $Bank=new Bank();
//        $sys_point_purse_id=$Bank->get_sys_purse_id(3);
//        $seller_point_purse_id=$Bank->userWallet(57,3,2)->purse_id;
//        $Bank->doApiTransfer($sys_point_purse_id,$seller_point_purse_id,10000,10021,
//            '',
//            '商家补贴获得积分');
//        exit;
//        $Message=new Message();
//        dd($Message->send_sms('13631272493',1,'','',[],0));
//        exit;
//
//        exit;
//        $UserWithdraw= new UserWithdraw();
//        $UserWithdraw->sub_withdraw(64,1,1,1000);
//
//        exit;
//        $this->dispatch(new Test());
        $Huanxun=new Huanxun();
//        $Huanxun->toCertificate([
//            'customerCode'=>'13631272493'
//        ]);
//        $rs=$Huanxun->queryOrdersList([
//            'customerCode'=>'13825604939',
//            'ordersType'=>'',
//            'merBillNo'=>'',
//            'ipsBillNo'=>'',
//            'startTime'=>'',
//            'endTime'=>'',
//            'currrentPage'=>'',
//            'pageSize'=>'',
//        ]);
//        $rs=$Huanxun->withdrawal([
//            'merBillNo'=>'15222922037635',
//            'amount'=>7998,
//            'customerCode'=>'13825604939',
//            'realName'=>'方羡燕',
//            's2sUrl'=>'http://www.baidu.com'
//        ]);

//        $rs=$Huanxun->withdrawal([
//            'merBillNo'=>'15222963892320',
//            'amount'=>6836,
//            'customerCode'=>'15625550777',
//            'realName'=>'邢建',
//            's2sUrl'=>'http://www.baidu.com'
//        ]);
//       $rs=$Huanxun->transfer([
//            'merBillNo'=>'009',
//            'customerCode'=>'13631272493',
//            'transferAmount'=>'1.2',
//            'remark'=>'转账'
//        ]);
        if($rs){
            dd($rs);
        }else{
            echo $Huanxun->getError();
        }
        exit;
        $Gift=new Gift();
        $Gift=$Gift->create_gift(100, 1, 1, 1,1);
//        $key=$Gift->cache_name.'11';
//        Cache::put($key,'aa',10);
//        echo Cache::get($key);
////        $Gift->del_redis_gift($key);
//        exit;
//        $Gift=new Gift();
////        $Gift->create_gift(100,100,100,100,100);
//        Cache::get($Gift->cache_name.'_27');
//        exit;
//        $Order=new Order();
//        $Order->is_recharge_enough(100,160,18565670712);
//        exit;
//        dd(Cache::get('tdzd_gift_3'));
//        $Huanxun=new Huanxun();
//        $rs=$Huanxun->queryOrdersList([
//            'customerCode'=>'13926509145',
//            'ordersType'=>'',
//            'merBillNo'=>'',
//            'ipsBillNo'=>'',
//            'startTime'=>'',
//            'endTime'=>'',
//            'currrentPage'=>'',
//            'pageSize'=>'',
//        ]);
//        $rs=$Huanxun->withdrawal([
//            'merBillNo'=>'152178070453922',
//            'amount'=>'4358',
//            'customerCode'=>'15109984111',
//            'realName'=>'吴跃飞',
//            's2sUrl'=>'http://www.baidu.com'
//        ]);
//        $Huanxun->update([
//            'customerCode'=>'15217808632642',
//            'pageUrl'=>'http://www.baidu.com',
//            's2sUrl'=>'http://www.baidu.com'
//        ]);
//        if($rs){
//            dd($rs);
//        }else{
//            echo $Huanxun->getError();
//        }
//        $Huanxun=new Huanxun();
//        $rs=$Huanxun->queryOrdersList([
//            'customerCode'=>'18820298599',
//            'ordersType'=>'',
//            'merBillNo'=>'',
//            'ipsBillNo'=>'',
//            'startTime'=>'',
//            'endTime'=>'',
//            'currrentPage'=>'',
//            'pageSize'=>100,
//        ]);
//        if($rs){
//            dd($rs);
//        }else{
//            echo $Huanxun->getError();
//        }
//        exit;
//        new_logger('232','232','232');
//        DB::transaction(function (){

//            $Bank=new Bank();
//            $amount=0;
//            $uid=0;
//            $point_purse_id=$Bank->userWallet($uid,3,2)->purse_id;
//            $purse_id=$Bank->userWallet($uid,1,2)->purse_id;
//            $sys_purse_id=$Bank->get_sys_purse(1)->purse_id;
//            $sys_point_purse_id=$Bank->get_sys_purse(3)->purse_id;
//            $t=$Bank->doApiTransfer($point_purse_id,$sys_point_purse_id,$amount,10000001,
//                '系统回收积分并补贴收益',
//                '系统回收积分');
//            $Bank->doApiTransfer($sys_purse_id,$purse_id,$amount,10000003,
//                '系统回收积分并补贴收益,相关流水编号'.$t,
//                '系统补贴收益');
//        });
        //系统帮商家货款提现
//        $seller_list=Seller::get();
//        $Ruzhu=new Ruzhu();
//        foreach ($seller_list as $k=>$v){
//            $balance=$Ruzhu->queryBalance($v->child_merchant_no)['body']['balanceAmount'];
//            if($balance>0){
//                $UserWithdraw=new UserWithdraw();
//                $UserWithdraw->goods_sub_withdraw($v->id,$v->child_merchant_no,$balance);
//            }
//        }
//        echo 'ok';exit;
//        $this->dispatch(new Test());
	}

	public function test_redis($id){
        if(cache_lock($id,1,1)){
            echo '操作频繁<br />';
        }else{
            $this->dispatch(new Test());
            echo '成功<br />';
        }
//        dispatch(new handleQrOrder('qr1516516515616155'));
//        $this->dispatch((new handleBuyPosOrder('pos2201801241015931516760055'))->onQueue('sms'));
//        $this->dispatch((new handleBuyPosOrder('pos2201801241015931516760055'))->onQueue('sms'));
//        $this->dispatch((new handleBuyPosOrder('pos2201801241015931516760055'))->onQueue('sms'));
//        $this->dispatch((new handleBuyPosOrder('pos2201801241015931516760055'))->onQueue('sms'));
//        $this->dispatch((new handleBuyPosOrder('pos2201801241015931516760055'))->onQueue('sms'));
//        $this->dispatch((new handleBuyPosOrder('pos2201801241015931516760055'))->onQueue('sms'));
//        $this->dispatch((new handleBuyPosOrder('pos2201801241015931516760055'))->onQueue('sms'));
//        $this->dispatch((new handleBuyPosOrder('pos2201801241015931516760055'))->onQueue('sms'));
//        $this->dispatch((new handleBuyPosOrder('pos2201801241015931516760055'))->onQueue('sms'));
//        $this->dispatch((new Test())->onQueue('sms'));
    }
    
    public function test1(){
        $extras = [
            'data'=> json_encode(['type' => 2,'data' => ['url'=>base64_encode('http://www.baidu.com'),'info'=>'我是大傻逼']])
        ];
        $JPush = new JPush(1);
        $JPush->push_tags('77026810','订单支付通知','订单支付通知',$extras);
        exit;

//        DB::beginTransaction(); //事务开始
//        try {
//            //代码区
//            Test::insert(['value' => 78]);
////            if (!false){
////                DB::commit(); //提交事务
////                return json_success('123');
////            }
//            DB::table('test1') -> insert(['id'=> 16,'value' => 78]);
//            DB::commit(); //提交事务
//        } catch(\Exception $ex) {
//            DB::rollback(); //回滚事务
//            return json_error('处理失败');
//            //异常处理
//        }
//
//            Test::insert(['value' => time2date()]);
//
////            throw new Exception(time2date());
////            DB::table('test1') -> insert(['id' => 16,'value' => time2date()]);
//            DB::table('test1') -> insert(['value' => time2date()]);
//
//
//            DB::commit(); //提交事务
//        $a =1;


//            return json_success($a);
	
	
		$a = [
			['id' => 1,'menber' => time2date(),'user' => rand(3,656),'name' => '晚饭'],
			['id' => 2,'menber' => time2date(),'user' => rand(3,656)],
			['id' => 3,'menber' => time2date(),'user' => rand(3,656)],
			['id' => 4,'menber' => time2date(),'user' => rand(3,656),'name' => '晚饭'],
			['id' => 5,'menber' => time2date(),'user' => rand(3,656)],
		];
		$b = $this->get_array($a,'child');
		return json_success('ok',$b);



	}
	
	/**
	 * @param $arr
	 * @param $child_name 添加新元素的键值
	 * @param int $level 用于递归的辅助参数，使用时不用填
	 * @return array
	 */
	public function get_array($arr,$child_name,$level = 0){
		//存储结果的容器
		$result = array();
		//二维数组的一个元素数组
		$value = $arr[$level];
		//当前元素数组的个数
		$count = count($arr[$level]);
		//复制当前元素数组
		for ($i = 0;$i < $count;$i ++){
			$result[array_keys($value)[$i]] = $value[array_keys($value)[$i]];
		}
		//判断是否为当前新数组添加一个新元素
		if (isset($arr[$level+1])){
			$result[$child_name] = $this->get_array($arr,$child_name,$level+1);
		}else{
			$result[$child_name] = [];
		}
		return $result;
	}
}
