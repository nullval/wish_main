<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ApiException;
use App\Http\Requests\Api\SellerLoginRequest;
use App\Jobs\handleBuyPosOrder;
use App\Jobs\handleBuyTdOrder;
use App\Jobs\handleGhtBuyPosOrder;
use App\Jobs\handleGhtOrder;
use App\Jobs\handleGhtSellerOrder;
use App\Jobs\handleSellerOrder;
use App\Jobs\handleShopOrder;
use App\Jobs\Withdraw;
use App\Libraries\BankSdk;
use App\Libraries\JPush\JPush;
#use JPush\Client as JPush;
use App\Models\Agent;
use App\Models\AgentOperate;
use App\Models\Bank;
use App\Models\JushMessage;
use App\Models\MerchantApply;
use App\Models\Message;
use App\Models\NineFloor;
use App\Models\Order;
use App\Models\OrderQrcode;
use App\Models\Pos;
use App\Models\PosOrderDetail;
use App\Models\RuzhuMerchantBasic;
use App\Models\RuzhuMerchantBusinesss;
use App\Models\Seller;
use App\Models\SellerRole;
use App\Models\SysConfig;
use App\Models\User;
use App\Models\UserPurse;
use App\Models\VOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;



/**高汇通对接pos接口专用**/
class PosPayController extends CommonController
{




    /**
     * 通过mac地址生成未特商城pos机(终端号)
     */
    public function isExit(Request $request){
        new_logger('posPay_isExit.log','arr',$request->all());
        //刷卡用的终端号
        $mac=$request->input('mac');
        $pos_type=$request->input('type');
        $dev=$request->input('dev');

        //所有app通用版本版本检测
        $version=$request->input('version');
        $version_type=$request->input('version_type');
        $SysConfig=new SysConfig();
        if($pos_type==1){
            //高汇通版
            $rs=$SysConfig->check_pos_version($version,$version_type,2);
        }elseif($pos_type==2||$pos_type==3){
            //通联版
            $rs=$SysConfig->check_pos_version($version,$version_type,1);
        }
        if($rs==false){
            return json_error('请在应用商城升级版本');
        }

        if(empty($add_pos)){
            $Pos=new Pos();
            $rs=$Pos->add_pos_by_mac($mac,$pos_type,$dev);
            return $rs;
        }

    }

    /**
     * pos机高汇通反扫支付
     * @param Request $request
     */
    public function pay(Request $request){
        new_logger('posPay_ghtPosPay.log','反扫请求参数:',$request->all());
        $terminal_no = $request->input('terminal_no'); //终端号
        $order_no = $request->input('order_no'); // 订单号
        $bank_code = $request->input('bank_code'); // 银行直连参数 支付宝-BACKSTAGEALIPAY 微信-BACKSTAGEWECHAT
        $auth_code = $request->input('auth_code'); // 反扫授权码
        $amount = $request->input('amount'); //订单金额 精确到小数后两位 元
        //假设子商户为空的话打钱给平台
        $child_merchant_no=empty($request->input('child_merchant_no'))?'18820298599':$request->input('child_merchant_no');

        if(!$terminal_no){
            return json_error('终端号为空');
        }
        if(!$order_no/* || DB::table()->find */){
            return json_error('订单编号有误');
        }
        if(!in_array($bank_code , ['BACKSTAGEALIPAY' , 'BACKSTAGEWECHAT','BACKSTAGEBANK'])){
            return json_error('支付类型有误');
        }
        if(!$auth_code){
            return json_error('授权码为空');
        }
        if(floatval($amount) <= 0){
            return json_error('订单金额有误');
        }



        /*
        测试环境地址：http://test.gaohuitong.com/backStageEntry.do
        测试商户号: 102100000125
        测试终端号：20000147
        测试密钥：857e6g8y51b5k365f7v954s50u24h14w

        测试商户号：000000153990021（一户一码模式使用）
        测试终端号：20001104
        测试密钥：eea5b0d874053400283e00d09f788314
         */

        $URL = env('GHT_FAN_URL'); // 这里写死，本应该为配置项读取
        $merchant_no = env('GHT_FAN_MERCHANT_NO');
        $terminal_no = env('GHT_FAN_TERMINNAL_NO');

        $params = [
            'busi_code' => 'BACKSTAGE_PAY', //  业务代码，固定写死
            'merchant_no' => $merchant_no, // 子商户号
//            'child_merchant_no' => $child_merchant_no, // 商户号
            'terminal_no' => $terminal_no, // 终端号
            'order_no' => $order_no, // 订单号
            'bank_code' => $bank_code, // 银行直连参数，反扫类型
            'auth_code' => $auth_code, // 反扫授权码
            'amount' => $amount , // 金额 元
            'currency_type' => 'CNY' , // 交易币种 人民币-CNY 港币-HKD 美元-USD
            'sett_currency_type' => 'CNY', // 清算币种
            'product_name' => '消费买单支付', // 产品名称
            'product_desc' => '消费买单支付', // 产品描述
            'base64_memo' => 'test' , // 订单备注
            'notify_url' => env('GHT_NOTIFY_URL').'/posPay/ghtNotify', // 服务器订单回调地址
            'client_ip' => '127.0.0.1', // 客户端IP
        ];
        if(env('APP_DEBUG')==false){
            $params['child_merchant_no']=$child_merchant_no;
        }

        $params['sign'] = $this->getSign($params);
//        echo '请求的参数:<br />';
//        print_r($params);
        $result_xml = curl_post($URL , $params);

        // 解析xml
        $result_xml =simplexml_load_string($result_xml);
        $result= json_decode(json_encode($result_xml) , true);
        new_logger('posPay_ghtPosPay.log','反扫请求参数回调:',$result);
//        echo '返回的参数:<br />';
//        print_r($result);

        if($result['resp_code']=='00'){
            if($bank_code=='BACKSTAGEWECHAT'){
                $pay_type=1;
            }elseif($bank_code=='BACKSTAGEALIPAY'){
                $pay_type=2;
            }
            Order::where(['order_sn'=>$order_no])->update(['pay_type'=>$pay_type]);
            $order_info=Order::where(['order_sn'=>$order_no])->first();
            $ruzhu_info=RuzhuMerchantBasic::where(['merchantId'=>$child_merchant_no])->first();
            $data['bank_code']=$bank_code; // 银行直连参数，反扫类型
            if(isset($ruzhu_info->id)){
                $data['merchantName']=$ruzhu_info->merchantName;//商户名称
                $data['merchant_no']=$result['merchant_no'];//商户号
            }else{
                $data['merchantName']='未特商城科技（深圳）有限公司';//商户名称
                $data['merchant_no']='00000000';//商户号
            }
            $data['terminal_no']=$terminal_no;//终端号
            $data['order_id']=$order_info->id+1000000;//凭证号
            $data['order_no']=$order_no;//交易单号
            $data['pay_time']=date('Y/m/d H:i:s');//交易时间
            $data['amount']=$amount;//交易金额
            if($bank_code=='BACKSTAGEWECHAT'){
                //微信
                $data['pay_type']='微信支付';
            }elseif($bank_code=='BACKSTAGEALIPAY'){
                //支付宝
                $data['pay_type']='支付宝支付';
            }else{
                //银行卡
                $data['pay_type']='银行卡支付';
            }
            if($result['resp_desc']== 'Success'){
                new_logger('posPay_ghtPosPay.log','回调参数状态:',['status'=>1]);
                new_logger('posPay_ghtPosPay.log','回调参数:',$data);
                return json_success('支付成功',$data);
            }else{
                if($result['resp_desc']=='CI:请扫描微信支付授权码（以10/11/12/13/14/15为前缀的18位数字）'||$result['resp_desc']=='CI:null'){
                    return json_error('请扫描有效的二维码',[],2);
                }
                new_logger('posPay_ghtPosPay.log','回调参数状态:',['status'=>2,'resp_desc'=>$result['resp_desc']]);
                return json_error('需要用户输入支付密码',[],3);
            }
        } else {
            new_logger('posPay_ghtPosPay.log','回调参数状态:',['status'=>0]);
            if($result['resp_desc']=='CI:请扫描微信支付授权码（以10/11/12/13/14/15为前缀的18位数字）'){
                return json_error('请扫描有效的二维码',[],2);
            }
            if($result['resp_desc']=='CI:null'){
                return json_error('请输入支付密码',[],2);
            }
            return json_error($result['resp_desc']);
        }
        new_logger('posPay_ghtPosPay.log','回调参数状态:',['status'=>3]);
    }

    /**
     * pos机反扫请求接口
     * @param Request $request
     */
    public function sweepPay(Request $request){
        new_logger('posPay_sweepPay.log','反扫请求参数:',$request->all());
        $order_no = $request->input('order_no'); // 订单号
        $auth_code = $request->input('auth_code'); // 反扫授权码
        $order_info=VOrder::where(['order_sn'=>$order_no])->first();
        if(!isset($order_info->id)){
            return json_error('订单不存在');
        }
        $pay_type=$order_info->pay_type;
        $amount=$order_info->amount;
        $buyer_id=$order_info->buyer_id;
        $type=$order_info->type;
        if($pay_type==1){
            $data['pay_type']='微信支付';
//            //购买pos机用越满
//            $pay_typess = 'yueman_stage_wechat';
//            //请求反扫支付 (越满)
//            $result = BankSdk::unified($buyer_id)
//                ->orderNo($order_no)
//                ->notifyUrl(url('posPay/bankNotify'))
//                ->orderType('越满微信反扫支付')
//                ->productName('扫码支付')
//                ->param('auth_code',$auth_code)
//                ->payType($pay_typess,$amount * 100)
//                ->pay();

            $pay_typess = 'huanxun_stage_wechat';
            //请求反扫支付 (环讯)
            $result = BankSdk::unified($buyer_id)
                ->orderNo($order_no)
                ->notifyUrl(url('posPay/bankNotify'))
                ->orderType('环讯微信反扫支付')
                ->productName('扫码支付')
                ->param('auth_code',$auth_code)
                ->param('pay_type',1)
                ->payType($pay_typess,$amount * 100)
                ->pay();
        }elseif($pay_type==2){
            $data['pay_type']='支付宝支付';
//            //购买pos机用越满
//            $pay_typess = 'yueman_stage_alipay';
//            //请求反扫支付 (越满)
//            $result = BankSdk::unified($buyer_id)
//                ->orderNo($order_no)
//                ->notifyUrl(url('posPay/bankNotify'))
//                ->orderType('越满支付宝反扫支付')
//                ->productName('扫码支付')
//                ->param('auth_code',$auth_code)
//                ->payType($pay_typess,$amount * 100)
//                ->pay();

            $pay_typess = 'huanxun_stage_alipay';
            //请求反扫支付 (环讯)
            $result = BankSdk::unified($buyer_id)
                ->orderNo($order_no)
                ->notifyUrl(url('posPay/bankNotify'))
                ->orderType('环讯支付宝反扫支付')
                ->productName('扫码支付')
                ->param('auth_code',$auth_code)
                ->param('pay_type',2)
                ->payType($pay_typess,$amount * 100)
                ->pay();
        }else{
            $data['pay_type']='银行卡支付';
        }
        new_logger('posPay_sweepPay.log','获得参数:',[$result]);
        $ruzhu_info=RuzhuMerchantBasic::where(['merchantId'=>$order_info->mobile])->first();
        if(isset($ruzhu_info->id)){
            $data['merchantName']=$ruzhu_info->merchantName;//商户名称
            $data['merchant_no']=$result['merchant_no'];//商户号
        }else{
            $data['merchantName']='未特商城科技（深圳）有限公司';//商户名称
            $data['merchant_no']='00000000';//商户号
        }
        $data['terminal_no']=$order_info->terminalId;//终端号
        $data['order_id']=$order_info->id+1000000;//凭证号
        $data['order_no']=$order_no;//交易单号
        $data['pay_time']=date('Y/m/d H:i:s');//交易时间
        $data['amount']=$amount;//交易金额
        return json_success('ok',$data);
    }

    /**
     * 生成签名
     * @param $param
     * @return mixed
     */
    private function getSign($param){
//        $key = env('');
        $key = env('GHT_FAN_KEY');
        unset($param['sign']);
        foreach($param as $k => $v){
            if($v=='null')
                unset($param[$k]);
        }
        ksort($param);//自然排序
        $data = [];
        foreach($param as $k => $v){
            $data[] = $k.'='.$v;
        }
        $str = implode('&' , $data) . '&key=' . $key;
        $param_sign = strtolower(hash("sha256", $str));
//        var_dump($str);exit;
//        var_dump($param_sign);exit;

        return $param_sign;
    }


    public function test_sign(Request $request){
        $sign=$this->getSign($request->all());
        var_dump($request->all());
        print_r($sign);exit;
    }

    /**
     *测试接口
     */
    public function test(Request $request){
        dispatch(new handleSellerOrder('dd2201802068065201517917516',0));
//        if(env('APP_DEBUG')==true){
//            $order_sn='dd2201801025558161514881910';
//        }else{
//            $order_sn='pos2201711287484091511850949';
//        }
//
//        $JpushMessage=new JushMessage();
//        //未处理
//        $a=$JpushMessage->send_order_status('dd2201801025558161514881910',2,100);
//
//        exit;

//        $notify_arr[0]=1.3;
//        $notify_arr[1]=2;
//        $notify_arr[2]=4;
//        $notify_arr[3]=5;
//        $notify_arr[4]=date('Y-m-d H:i');
//        $Message=new Message();
//        $Message->send_sms('13631272493',12,'','',$notify_arr,0);
        exit;
        //        if($order_info->type==2){
        //机主版
//        $JPush = new JPush(2);
////        }elseif($order_info->type==1||$order_info->type==3){
////            //商家版
////            $JPush = new JPush(2);
////        }
//        $extras = [
//            'data'=> json_encode(['type' => 1,'data' => ['aa'=>1]])
//        ];
//        $rs=$JPush->push_tags('2xdl100000003','测试','测试',$extras);var_dump($rs);exit;
//
//        $terminalId=$request->input('terminalId');

    }


    /**
     * 高汇通支付通知回调 (pos机消费买单后) (钱入公司平台和平台下的商户号,我们不分账,钱扣回来) 正扫
     * 如果是机主版本回调 机主pos机必须绑定的是公司平台下的属于公司商户号,这样子相当于钱全入公司,我们要分账 并即时到账到他的账户
     * 如果是商家版回调
     *
     */
    public function ghtNotify(Request $request){
        $input_post = file_get_contents("php://input");
        new_logger('posPay_ghtPosNotify.log','反扫支付成功后回调参数',$request->all());

        $params = $request->all();

        $order_sn=$params['order_no'];
        //防止通知时间太短造成重复通知
        if(cache_lock('ghtNotify_'.$order_sn,1,30)==true){
            return arr_post(0,'频繁通知');
        }


//        if(env('APP_DEBUG')==false){
        $order_arr['actual_amount']=$params['amount'];
//        }
        //算签
        $sign=$this->getSign($request->all());
        new_logger('posPay_ghtPosNotify.log','反扫支付成功后签名',['sign'=>$sign]);
        new_logger('posPay_ghtPosNotify.log','反扫支付成功后原签名',['sign'=>$params['sign']]);

        //单位元
        $amount=$params['amount'];
        //订单号
        $order_sn=$params['order_no'];
        //终端号
        //        $terminalId=$params['termid'];
        //订单状态
        $trxstatus=$params['pay_result'];
        //交易完成时间
        $paytime=$params['pay_time'];
        //交易卡号
        if(!empty($request->input('user_bank_card_no'))){
            $acct=$params['user_bank_card_no'];
        }else{
            $acct='****';
        }
        $trxcode='VSP001';
        if($trxstatus==1){
            //成功
            $status=2;
        }else{
            //失败
            $status=3;
        }
        $order_info=Order::where(['order_sn'=>$order_sn])->first();
        //无论成功失败都做极光推送
        $JpushMessage=new JushMessage();
        //支付类型
        if(!isset($params['bank_code'])){
            $pay_type=1;
        }else{
            if($params['bank_code']=='BACKSTAGEWECHAT'){
                $pay_type=1;
            }elseif($params['bank_code']=='BACKSTAGEALIPAY'){
                $pay_type=2;
            }
        }

        //未处理
        if(isset($order_info->status)){
            if($order_info->status==1&&empty($order_info->notify_remark)){
                $JpushMessage->send_order_status($order_sn,$status,$amount,$pay_type);
            }
        }else{
            return json_error('订单号已失效');
        }

//        if($sign==$params['sign']){
                /**相应参数结束**/

                //狗日的高汇通会在毫秒级别的时候通知做队列处理
                $this->dispatch((new handleGhtOrder($order_sn,$pay_type,$input_post,$amount,$status,$acct,$paytime,$trxcode,2))->onQueue('handleOrder'));
//                $this->dispatch(new handleGhtOrder($order_sn,$pay_type,$input_post,$amount,$status,$acct,$paytime,$trxcode,2));
                echo 'success';
//        }else{
//            new_logger('posPay_ghtPosNotify.log','反扫支付成功后算签失败',['order_sn'=>$order_sn]);
//            return json_error('验签失败');exit;
//        }

    }



    /**
     * @param $order_sn 订单号
     * @return array
     * 订单查询
     */
    public function checkOrderStatus(Request $request){
        new_logger('posPay_checkOrderStatus.log','查询订单请求参数',$request->all());

        $order_sn=$request->input('order_sn');
        if(empty($order_sn)){
            return json_error('请输入订单号');
        }
        $Order=new Order();
        $data=$Order->get_order_print_info($order_sn);
        new_logger('posPay_checkOrderStatus.log','返回参数',$data);
        return json_success('查询成功',$data);
    }

    /**
     * 根据终端号获取推荐人信息
     */
    public function getAgentInfo(Request $request){
        new_logger('posPay_getAgentInfo.log','根据终端号获取推荐人信息参数',$request->all());


        $terminalId=$request->input('terminalId');
        $mac=$request->input('mac');
        if(empty($terminalId)){
            $terminalId_pos_info=Pos::where(['mac'=>$mac])->first();
        }else{
            $terminalId_pos_info=Pos::where(['terminalId'=>$terminalId])->first();
        }

        //版本检测
        $version=$request->input('version');
        $pos_type=$terminalId_pos_info->pos_type;
        $SysConfig=new SysConfig();
        if($pos_type==1){
            //高汇通版
            $rs=$SysConfig->check_pos_version($version,1,2);
        }elseif($pos_type==2||$pos_type==3){
            //通联版
            $rs=$SysConfig->check_pos_version($version,1,1);
        }
        if($rs==false){
            return json_error('请在应用商城升级版本');
        }


        if(!isset($terminalId_pos_info->terminalId)){
            return json_error('该终端号不存在');
        }
        $terminalId=$terminalId_pos_info->terminalId;

        $pos_info=Pos::where(['terminalId'=>$terminalId])->first();
        if(!isset($pos_info->id)){
            return json_error('该终端号不存在');
        }
        if(empty($terminalId_pos_info->mac)){
            Pos::where(['terminalId'=>$terminalId])->update(['mac'=>$mac]);
        }

        $agent_info=Agent::where(['id'=>$pos_info->agent_uid])->first();
        if(!isset($agent_info->mobile)){
            return json_error('该终端号未激活,请联系管理员');
        }
        $data['invite_mobile']=$agent_info->mobile;
        $data['is_show']=$pos_info->is_allow_edit; //是否允许修改 1允许 0不允许
        return json_success('推荐人信息获取成功',$data);
    }


    /**
     * 创建购买pos机订单号
     */
    public function createPosOrder(Request $request){
//        if($request->input('terminalId')!='1xdl100000025'&&$request->input('terminalId')!='test1'&&$request->input('terminalId')!='1xdl100000021'){
//            return json_error('您的商户资料不全,请尽快完善，以便便于您使用完整的pos通道');
//        }
        new_logger('posPay_createPosOrder.log','pos机请求订单参数',$request->all());

        //购买pos机的业务员手机号
        $mobile=$request->input('mobile');
        //想要购买的终端号数组,,隔开 空则不传
        $terminalIdArr=$request->input('terminalIdArr');
        //备注
        $pos_remark=$request->input('pos_remark');
        //刷卡用的终端号
        $terminalId=$request->input('terminalId');
        //购买pos机的台数
        $pos_num=$request->input('pos_num');
        //收货人
        $receiver=$request->input('receiver');
        //联系电话
        $contact_number=$request->input('contact_number');
        //联系地址
        $contact_address=$request->input('contact_address');
        //购买pos机填写的推荐人手机号码
        $invite_mobile=$request->input('invite_mobile');
        $mac=$request->input('mac');
        //支付方式 alipay,wechat,bank
        $pay_type=$request->input('pay_type');
        //支付金额 元
        $amount=$request->input('amount');

        //版本检测
        $pos_info=Pos::where(['terminalId'=>$terminalId])->first();
        $version=$request->input('version');
        $pos_type=$pos_info->pos_type;
        $SysConfig=new SysConfig();
        if($pos_type==1){
            //高汇通版
            $rs=$SysConfig->check_pos_version($version,1,2);
        }elseif($pos_type==2||$pos_type==3){
            //通联版
            $rs=$SysConfig->check_pos_version($version,1,1);
        }
        if($rs==false){
            return json_error('请在应用商城升级版本');
        }

        $Order=new Order();
        $data=$Order->create_pos_order($mobile,$terminalIdArr,$pos_remark,$terminalId,$pos_num,$receiver,$contact_number,$contact_address,$invite_mobile,$mac,$pay_type,$amount,1,2);
        return json_success('订单号生成成功',$data);
    }

    /**
     * 生成订单号 (消费买单,订单支付)
     */
    public function createOrder(Request $request){
//        if($request->input('terminalId')!='1xdl100000025'&&$request->input('terminalId')!='test1'&&$request->input('terminalId')!='1xdl100000021'){
//            return json_error('您的商户资料不全,请尽快完善，以便便于您使用完整的pos通道');
//        }
        new_logger('posPay_posOrder.log','pos机请求订单参数',$request->all());
        $type=$request->input('type');
        $mobile=$request->input('mobile');
        $mac=$request->input('mac');
        $terminalId=$request->input('terminalId');
        //店员手机号
        $role_mobile=$request->input('role_mobile');

        //版本检测
        $pos_info=Pos::where(['terminalId'=>$terminalId])->first();
        $version=$request->input('version');
        $pos_type=$pos_info->pos_type;
        $SysConfig=new SysConfig();
        if($pos_type==1){
            //高汇通版
            $rs=$SysConfig->check_pos_version($version,2,2);
        }elseif($pos_type==2||$pos_type==3){
            //通联版
            $rs=$SysConfig->check_pos_version($version,2,1);
        }
        if($rs==false){
            return json_error('请在应用商城升级版本');
        }
        //支付方式 alipay,wechat,bank
        $pay_type=$request->input('pay_type');
        //支付金额 元
        $amount=$request->input('amount');

        $Order=new Order();
        //通知类型高汇通
        $data=$Order->create_order($mobile,$terminalId,$type,$mac,$role_mobile,$pay_type,$amount,2);
        return json_success('订单生成成功',$data);
    }

    public function getOrderList(Request $request){
        new_logger('posPay_getOrderList.log','pos机获取订单列表',$request->all());
        $terminalId=$request->input('terminalId');
        if(empty($terminalId)){
            return json_error('请填写终端号');
        }
        $version_type=$request->input('version_type');
        if(empty($version_type)||$version_type==1){
            //机主版
            $order_list=Order::where(['terminalId'=>$terminalId,'status'=>2])->where(['type'=>2])->orderBy('id','desc')->limit(10)->get(['order_sn']);
        }elseif($version_type==2){
            //商家版
//            $order_list=Order::where(['terminalId'=>$terminalId,'status'=>2])->orWhere(function($query) use ($terminalId){
//				$seller_id = Pos::where(['terminalId'=>$terminalId])->value('uid');
//            	$query->where(['uid'=>$seller_id,'type'=>5,'status'=>2]);
//			})->whereIn('type',[1,3,5,7])->orderBy('id','desc')->limit(10)->get(['order_sn']);
            $order_list=Order::where(['terminalId'=>$terminalId,'status'=>2])->whereIn('type',[1,3,5,7])->orderBy('id','desc')->limit(10)->get(['order_sn']);
        }elseif($version_type==3){
            //珍藏版
            $order_list=Order::where(['terminalId'=>$terminalId,'status'=>2])->whereIn('type',[6])->orderBy('id','desc')->limit(10)->get(['order_sn']);
        }elseif($version_type == 4){
            $order_list=Order::where(['terminalId'=>$terminalId,'status'=>2])->whereIn('type',[4])->orderBy('id','desc')->limit(10)->get(['order_sn']);
        }
        $data=[];
        $Order=new Order();
        foreach ($order_list as $k=>$v){
            $data[$k]['data']=$Order->get_order_print_info($v->order_sn,2);
        }
        return json_success('获取成功',$data);
    }


    /**
     * 初始化绑定推荐人
     */
    public function bindInvterId(Request $request){
        new_logger('posPay_bindInvterId.log','pos机请求订单参数',$request->all());
        $mobile=$request->input('mobile');
        $terminalId=$request->input('terminalId');
        $mac=$request->input('mac');
        $pos_info=Pos::where(['terminalId'=>$terminalId])->first();
        if(!isset($pos_info->id)){
            return json_error('未特商城pos机不存在');
        }

        //所有app通用版本版本检测
        $version=$request->input('version');
        $version_type=$request->input('version_type');
        $pos_type=$pos_info->pos_type;
        $SysConfig=new SysConfig();
        if($pos_type==1){
            //高汇通版
            $rs=$SysConfig->check_pos_version($version,$version_type,2);
        }elseif($pos_type==2||$pos_type==3){
            //通联版
            $rs=$SysConfig->check_pos_version($version,$version_type,1);
        }
        if($rs==false){
            return json_error('请在应用商城升级版本');
        }


        if(!empty($pos_info->agent_uid)){
            return json_error('该未特商城pos机已绑定机主');
        }
        if(empty($mobile)){
            return json_error('请传入要绑定的机主');
        }
        //判断机主是否存在
        $agent_info=Agent::where(['mobile'=>$mobile])->first();
        if(!(isset($agent_info->id))){
            return json_error('此手机号未成为机主,不可绑定使用');
        }
        if(empty($agent_info->inviter_id)&&($agent_info->is_admin==0)){
            return json_error('此手机号未成为机主,不可绑定使用');
        }
        $rs=Pos::where(['terminalId'=>$terminalId,'mac'=>$mac])->update(['agent_uid'=>$agent_info->id]);
        if($rs){
            return json_success('绑定成功');
        }else{
            return json_error('绑定失败');
        }
    }

    /**
     * 根据终端号获取商家信息
     */
    public function isSellerExit(Request $request){
        new_logger('posPay_isSellerExit.log','根据终端号获取商家信息参数',$request->all());

        $terminalId=$request->input('terminalId');
        $pos_info=Pos::where(['terminalId'=>$terminalId])->first();
        if(!isset($pos_info->terminalId)){
            return json_error('该未特商城pos机不存在');
        }

        //所有app通用版本版本检测
        $version=$request->input('version');
        $version_type=$request->input('version_type');
        //先写死 给商家版用
        $version_type=2;
        $pos_type=$pos_info->pos_type;
        $SysConfig=new SysConfig();
        if($pos_type==1){
            //高汇通版
            $rs=$SysConfig->check_pos_version($version,$version_type,2);
        }elseif($pos_type==2||$pos_type==3){
            //通联版
            $rs=$SysConfig->check_pos_version($version,$version_type,1);
        }
        if($rs==false){
            return json_error('请在应用商城升级版本');
        }


        if(empty($pos_info->uid)){
            return json_error('该未特商城pos机未绑定商家');
        }else{
            return json_success('该未特商城pos机已绑定商家');
        }
    }

    /**
     * 初始化绑定商家
     */
    public function bindSeller(Request $request){
        new_logger('posPay_bindSeller.log','pos机请求订单参数',$request->all());
        $mobile=$request->input('mobile');
        $terminalId=$request->input('terminalId');

        $pos_info=Pos::where(['terminalId'=>$terminalId])->first();
        if(!isset($pos_info->id)){
            return json_error('未特商城pos机不存在');
        }

        //所有app通用版本版本检测
        $version=$request->input('version');
        $version_type=$request->input('version_type');
        //先写死 给商家版用
        $version_type=2;
        $pos_type=$pos_info->pos_type;
        $SysConfig=new SysConfig();
        if($pos_type==1){
            //高汇通版
            $rs=$SysConfig->check_pos_version($version,$version_type,2);
        }elseif($pos_type==2||$pos_type==3){
            //通联版
            $rs=$SysConfig->check_pos_version($version,$version_type,1);
        }
        if($rs==false){
            return json_error('请在应用商城升级版本');
        }


//        if(empty($pos_info->agent_uid)){
//            return json_error('该未特商城pos机未绑定机主,无法绑定商家');
//        }
        if(empty($mobile)){
            return json_error('请传入要绑定的商家');
        }


        //判断商家是否能绑定
        $seller_info=Seller::where(['mobile'=>$mobile])->first();
        if(!(isset($seller_info->id))){
            return json_error('此手机号未成为商家,不可绑定使用');
        }
        if($seller_info->status==0){
            return json_error('该商家已被平台禁用，无法使用');
        }
        $rmb_info=RuzhuMerchantBasic::where(['uid'=>$seller_info->id])->first();
        if(!isset($rmb_info->merchantId)){
            return json_error('商家未入驻高汇通无法使用');
        }
        if($rmb_info->status!=3){
            return json_error('该商家未通过商家入驻审核,无法使用');
        }

        //一个商家只能绑定一个机主 一个运营中心
        $uid=$seller_info->id;
        $agent_pos=Pos::where(['uid'=>$uid])
            ->leftJoin('agent','agent.id','=','pos.agent_uid')
            ->whereNotNull('agent_uid')->where('agent_uid','<>',0)->where('agent_uid','<>',$pos_info->agent_uid)
            ->first(['agent.mobile']);
        if(isset($agent_pos->mobile)&&!empty($pos_info->agent_uid)){
            return json_error('该商家已在其他未特商城pos机绑定其他机主,机主号码:'.$agent_pos->mobile);
        }
        $operate_pos=Pos::where(['uid'=>$uid])
            ->leftJoin('agent_operate','agent_operate.id','=','pos.agent_operate_id')
            ->whereNotNull('agent_operate_id')->where('agent_operate_id','<>',0)->where('agent_operate_id','<>',$pos_info->agent_operate_id)
            ->first(['agent_operate.mobile']);
        if(isset($operate_pos->mobile)&&!empty($pos_info->agent_operate_id)){
            return json_error('该商家已在其他未特商城pos机绑定其他运营中心,运营中心号码:'.$operate_pos->mobile);
        }


        $rs=Pos::where(['terminalId'=>$terminalId])->update(['uid'=>$seller_info->id]);
        if($rs){
            return json_success('绑定成功');
        }else{
            return json_error('绑定失败');
        }
    }

    /**
     * 根据终端号获取商家信息
     */
    public function isOperateExit(Request $request){
        new_logger('posPay_isOperateExit.log','根据终端号获取运营中心信息参数',$request->all());

        $terminalId=$request->input('terminalId');
        $pos_info=Pos::where(['terminalId'=>$terminalId])->first();
        if(!isset($pos_info->terminalId)){
            return json_error('该未特商城pos机不存在');
        }

        //所有app通用版本版本检测
        $version=$request->input('version');
        $version_type=$request->input('version_type');
        $pos_type=$pos_info->pos_type;
        $SysConfig=new SysConfig();
        if($pos_type==1){
            //高汇通版
            $rs=$SysConfig->check_pos_version($version,$version_type,2);
        }elseif($pos_type==2||$pos_type==3){
            //通联版
            $rs=$SysConfig->check_pos_version($version,$version_type,1);
        }
        if($rs==false){
            return json_error('请在应用商城升级版本');
        }


        if(empty($pos_info->agent_operate_id)){
            return json_error('该未特商城pos机未绑定运营中心');
        }else{
            return json_success('该未特商城pos机已绑定运营中心');
        }
    }


    /**
     * 初始化绑定运营中心
     */
    public function bindOperate(Request $request){
        new_logger('posPay_bindOperate.log','请求参数',$request->all());
        $mobile=$request->input('mobile');
        $terminalId=$request->input('terminalId');

        $pos_info=Pos::where(['terminalId'=>$terminalId])->first();
        if(!isset($pos_info->id)){
            return json_error('未特商城pos机不存在');
        }

        //所有app通用版本版本检测
        $version=$request->input('version');
        $version_type=$request->input('version_type');
        $pos_type=$pos_info->pos_type;
        $SysConfig=new SysConfig();
        if($pos_type==1){
            //高汇通版
            $rs=$SysConfig->check_pos_version($version,$version_type,2);
        }elseif($pos_type==2||$pos_type==3){
            //通联版
            $rs=$SysConfig->check_pos_version($version,$version_type,1);
        }
        if($rs==false){
            return json_error('请在应用商城升级版本');
        }


        if(empty($mobile)){
            return json_error('请传入要绑定的运营中心');
        }


        //判断商家是否能绑定
        $operate_info=AgentOperate::where(['mobile'=>$mobile])->first();
        if(!(isset($operate_info->id))){
            return json_error('此手机号未成为运营中心,不可绑定使用');
        }
        if($operate_info->status==0){
            return json_error('该商家已被平台禁用，无法使用');
        }
        

        $rs=Pos::where(['terminalId'=>$terminalId])->update(['agent_operate_id'=>$operate_info->id]);
        if($rs){
            return json_success('绑定成功');
        }else{
            return json_error('绑定失败');
        }
    }

    /*
     * 获取未特商城pos机信息
     */
    public function getInfo(Request $request){
        new_logger('posPay_getInfo.log','请求参数',$request->all());
        $terminalId=$request->input('terminalId');
        $version_type=$request->input('version_type');
        $rs=Pos::where(['pos.terminalId'=>$terminalId])
            ->leftJoin('agent as u1','u1.id','=','pos.agent_uid')
            ->leftJoin('seller as u2','u2.id','=','pos.uid')
            ->leftJoin('agent_operate as u3','u3.id','=','pos.agent_operate_id')
            ->leftJoin('ruzhu_merchant_basic as u4','u4.uid','=','u2.id')
            ->first(['pos.terminalId',
                'u1.mobile as agent_mobile',
                'u2.mobile as seller_mobile',
                'pos.uid',
                'u4.merchantName as seller_name',
                'u3.mobile as operate_mobile',
                ]);
        //是否能使用银行卡支付 1可以 0否
        $rs->is_show_bank=0;
        //判断是否能使用银行卡支付 (只限商家)
        //2018/02/07 必须是第一台 正序第一台 商家版防止银行卡支付并发
        $first_terminalId=Pos::where(['uid'=>$rs->uid])->orderBy('id','asc')->value('terminalId');
        if(!empty($first_terminalId)&&$first_terminalId==$terminalId&&$version_type==2){
            $rs->is_show_bank=1;
        }
        if($terminalId=='1xdl100000021'||$terminalId=='1xdl100000227'||$terminalId=='1xdl100000025'){
            //特殊处理
            $rs->is_show_bank=1;
        }

//        if($terminalId=='1xdl100000025'||$terminalId=='1xdl100000227'){
//            //特殊处理
//            $rs->is_show_wechat=1;
//        }else{
//            $rs->is_show_wechat=0;
//        }
        $rs->is_show_wechat=1;
        $rs->is_show_ali=1;
//        if(empty($version_type)){
//            $rs->is_show_bank=0;
//        }
        if(!isset($rs->terminalId)){
            return json_error('该未特商城pos机不存在');
        }
        return json_success('获取信息成功',$rs);
    }

    /**
     * 判断员工是否属于该店员
     */
    public function isRoleExit(Request $request){
        //商家手机号
        $seller_mobile=$request->input('seller_mobile');
        $seller_info=Seller::where(['mobile'=>$seller_mobile])->first(['id']);
        if(!isset($seller_info->id)){
            return json_error('商家不存在');
        }
        //店员编号
        $role_name=$request->input('role_name');
        $role_info=SellerRole::where(['seller_id'=>$seller_info->id,'role_name'=>$role_name])->first(['id','role_name','mobile as role_mobile']);
        if(isset($role_info->id)){
            return json_success('获取店员信息成功',$role_info);
        }else{
            return json_error('店员不存在');
        }

    }



    /**
     * @param Request $request
     * @return string
     * @throws ApiException
     * 请求中央银行支付后的回调地址
     */
    public function bankNotify(Request $request){
        $input_post = file_get_contents("php://input");
        new_logger('posPay_bankNotify.log','请求参数',$request->all());
        $bank_sdk = new BankSdk();
        $var = $bank_sdk->check_sign($request->except(['s']));
        if($var||config('app.env')!='online'){
            $params=$request->except(['s']);
            //算签成功
//            if(cache_lock('hxNotify_'.$order_sn,1,30)==true){
//                return arr_post(0,'频繁通知');
//            }
            //单位元
            $amount=$params['amount']/100;
            //订单号
            $order_sn=$params['order_no'];
            if(empty($amount)){
                throw new ApiException('订单金额不能为空');
            }
            //交易完成时间
            $paytime=date('YmdHis');
            //成功
            $status=2;
            $order_info=Order::where(['order_sn'=>$order_sn])->first();
            //未处理
            if(!isset($order_info->status)){
                return json_error('订单号已失效');
            }
            //支付类型
            $pay_type=$order_info->pay_type;
            $type=$order_info->type;
            $notify_type=$order_info->notify_type;
//            if($type==3){
//                //若是没填手机号码则生成二维码 并通知到安卓
//                $terminalId=$order_info->terminalId;
//                $OrderQrcode=new OrderQrcode();
//                $OrderQrcode->create_qrcode($order_sn,$terminalId);
//            }
            if($type==7){
                //微信跟支付宝要推送
                $JpushMessage=new JushMessage();
                if($order_info->status==1&&empty($order_info->notify_remark)){
                    $JpushMessage->send_order_status($order_sn,$status,$amount,$pay_type);
                }
            }
            //
            //2018/02/06
//            $this->dispatch(new handleGhtOrder($order_sn,$pay_type,$input_post,$amount,$status,'',$paytime,'',$notify_type));
            $this->dispatch((new handleGhtOrder($order_sn,$pay_type,$input_post,$amount,$status,'',$paytime,'',$notify_type))->onQueue('handleOrder'));
            return 'success';
        }else{
            //算签失败
            new_logger('posPay_bankNotify.log','算签失败,参数',$request->all());
            return json_error('算签失败');
        }
    }
	
	
	/**
	 * 生成充值订单号
	 */
	public function create_recharge_Order(Request $request){
//        if($request->input('terminalId')!='1xdl100000025'&&$request->input('terminalId')!='test1'&&$request->input('terminalId')!='1xdl100000021'){
//            return json_error('您的商户资料不全,请尽快完善，以便便于您使用完整的pos通道');
//        }
		new_logger('posPay_posOrder.log','pos机请求订单参数',$request->all());
		$terminalId=$request->input('terminalId');
		
		//版本检测
		$pos_info=Pos::where(['terminalId'=>$terminalId])->first();
		$version=$request->input('version');
		$pos_type=$pos_info->pos_type;
		$SysConfig=new SysConfig();
		if($pos_type==1){
			//高汇通版
			$rs=$SysConfig->check_pos_version($version,2,2);
		}elseif($pos_type==2||$pos_type==3){
			//通联版
			$rs=$SysConfig->check_pos_version($version,2,1);
		}
		if($rs==false){
			return json_error('请在应用商城升级版本');
		}
		//支付方式 alipay,wechat,bank
		$pay_type=$request->input('pay_type');
		//支付金额 元
		$amount=$request->input('amount');
		
		$Order=new Order();
		$data=$Order->create_recharge_order($terminalId,$pay_type,$amount,3);
		return json_success('订单生成成功',$data);
	}


	public function is_create_qrcode(){
	    $order_sn=\request('order_sn');
        $order_info=VOrder::where(['order_sn'=>$order_sn])->first();
	    if(!isset($order_info->id)){
            return json_error('订单不存在');
        }
        $type=$order_info->type;
        if($type==3){
            //若是没填手机号码则生成二维码 并通知到安卓
            $terminalId=$order_info->terminalId;
            $OrderQrcode=new OrderQrcode();
            $data=$OrderQrcode->create_qrcode($order_sn,$terminalId);
            return json_success('获取成功',$data);
        }else{
            return json_error('订单类型不正确');
        }
    }

}
