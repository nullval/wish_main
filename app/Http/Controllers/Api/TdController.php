<?php

namespace App\Http\Controllers\Api;
use App\Models\Api\ApiOrder;
use App\Models\Api\ApiTdPos;
use App\Models\SysConfig;
use Illuminate\Http\Request;

/**刷pos机购买Td接口专用**/
class TdController extends CommonController
{
    public function createTdOrder(Request $request){
        new_logger('td_createTdOrder.log','pos机请求订单参数',$request->all());

        //购买人手机号
        $mobile=$request->input('mobile');
        //刷卡用的终端号
        $terminalId=$request->input('terminalId');


        //版本检测
        $pos_info=ApiTdPos::where(['terminalId'=>$terminalId])->first();
        if(!isset($pos_info->id)){
            return json_error('该未特商城pos机不存在');
        }
        $version=$request->input('version');
        $pos_type=$pos_info->pos_type;
        $SysConfig=new SysConfig();
        if($pos_type==1){
            //高汇通版
            $rs=$SysConfig->check_pos_version($version,3,2);
        }elseif($pos_type==2||$pos_type==3){
            //通联版
            $rs=$SysConfig->check_pos_version($version,3,1);
        }
        if($rs==false){
            return json_error('请在应用商城升级版本');
        }

        $Order=new ApiOrder();
        $data=$Order->create_td_order($mobile,$terminalId);
        return json_success('订单生成成功',$data);
    }

}
