<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ApiException;
use App\Http\Controllers\Api\CommonController;
use App\Http\Requests\BasicRequest;
use App\Jobs\SevenToHalf;
use App\Jobs\Test;
use App\Jobs\TestJob;
use App\Libraries\BankSdk;
use App\Libraries\gaohuitong\Ruzhu;
use App\Libraries\ght\Wangguan;
use App\Libraries\JPush\JPush;
use App\Libraries\msg\WechatPush;
use App\Models\Agent;
use App\Models\JavaIncrement;
use App\Models\JushMessage;
use App\Models\Pos;
use App\Models\SysConfig;
use App\Models\VOrder;
use Illuminate\Http\Request;
use Validator;
use Mockery\Exception;
use Psr\Log\InvalidArgumentException;
use App\Models\User;
use App\Models\Gift;
use App\Models\ShopTnetReginfo;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
class SandboxController extends CommonController
{

	public function test(Request $request) {
	    dd('003'.create_order_no(time()));
	    dd(date('Ymd') . str_pad(mt_rand(1, 99999), 5, '2', STR_PAD_LEFT));
	    dd(md5('ApiVersion=1.0&ClientSource=3&ClientSystem=Mozilla&ClientVersion=1.0&_cmd=user_send_sms&captcha=1598&mobile=13925721424&ticket=2018050425151'));
	    dd(config('pay.notify_type')[6]['pay_method_poundage']);
        $JavaIncrement = new JavaIncrement();
        $JavaIncrement->automatic_guadan('1361272493','82.4',10001131,3,'00012201806098756371528476471');
        exit;
$id=$request->get('id');
        $gift = new Gift();
        $step = 0.045;
        $lng = '113.948765';
        $lat = '22.533055';

//        Cache::get('ccxccc');
        $redis = app('redis.connection');
//        if ($redis->exists($this->cache_arr_name)) {
//           $res= $redis->del('chqt_gifts');
//        $res= $redis->get('chqt_gifts');
//           dd($res);
//        }
//exit;
//        $id= $gift->create_gift(600, 1, $lat,$lng,'中国石油xx');
        echo 'id:'. $id.'<br/>';
        dd($redis->llen('chqt_gift_list_'.$id));
        //查询附近红包
//exit;
        $gift = new Gift();
        $step = 0.045;
        //查询附近红包
        $check = $gift->check_gift(60, $lat, $lng, $step);
        $model = new ShopTnetReginfo();
        $wxinfo = $model->getWxinfo(10001155);
        return json_success('ok', ['gift'=>$check[0],'user'=>$wxinfo]);

//        $res=  $gift->check_gift(60, $lat, $lng,$step);
      dd($res);
//        $res= $gift->open_gift(60, 1);
//        dd($res);
        $redis = app('redis.connection');
//        $redis->llen('gift_list');
//        dd($redis->lpop('gift_list'));  // 返回 'bar0');

//        $pay = new esyto\esyto(false);
//        $data = [
//            'order_sn' => '20180514184147',//date('YmdHis'), //商户平台订单号
//            'subject' => '提现代付', //收款人姓名
//            'amount' => "1", //元为单位
//            'acctNo' => '6214837553183967', //收款人卡号
//            'accBankName' => '鲁在静', //收款人
//            'bankname' => '招商银行', //
//        ];
//        $rs = $pay->pay($data);
//        $rs = $pay->queryBalance();
//        if($rs){
//            dump($rs);
//            // todo
//        }else{
//            echo $pay->error;
//        } exit;
//        $user = User::Whereraw('id >= ((SELECT MAX(id) FROM user)-(SELECT MIN(id) FROM user)) * RAND() + (SELECT MIN(id) FROM user)')->first();
//        $gift = new Gift();
//        $step = 0.045;
//        $lng = '113.948765';
//        $lat = '22.533055';
//        //查询附近红包
//        $result = $gift->check_gift($user->id, $lat, $lng, $step);
//        return json_success('ok', $result);
//        new_logger('open_gt.log', '打开红包请求参数', $user->id);
//        $gift = new Gift();
//        $result = $gift->open_gift($user->id, $giftid);
//        new_logger('open_gt.log', '回调红包结果', $result);
//        if ($result['status']) {
//            unset($result['status']);
//            return json_success('恭喜抢到' . $result["money"] . '元', $result);
//        }

        //接口验证
//        $validator = Validator::make($request->all(), [
//            'title' => 'required',
//            'aa' => 'required',
//        ],[
//            'title.required'=>'标题必传'
//        ]);
//
//        if ($validator->fails()) {
//           return json_error($validator->errors()->first());
//        }
//
//	    exit;

//		$order = VOrder::where(['order_sn'=>'qr2201801106494011515566283'])->first();
//		(new JushMessage())->send_order_status('qr2201801106494011515566283',2,$order->amount,$order->pay_type);
//		Pos::where(['uid'=>54])->where('terminalId','like','1xd%')->pluck('terminalId')->each(function($terminal_id){
//			dump($terminal_id);
//		});

//		$s =  BankSdk::unified(1)->orderNo(time())->orderType('环讯反扫')->productName('付款码支付')->payType('huanxun_stage',1)->notifyUrl('http://notifyasdas.com')->param('auth_code','sdfsf')->param('pay_type','alipay或wechat')->pay();

//        $inviter_has=Agent::where(['inviter_id'=>349])->value('id');
//        dd($inviter_has);
    }

	
	
	public function index(){
		return view('api.sandbox.index');
	}
	
	public function submit(Request $request){
//	    var_dump(232);exit;
		$post = $request->except(['s']);
		$keys = $post['key'];
		$value = $post['value'];
		$curl_data = [];
		unset($post['key'],$post['value']);
		foreach($keys as $k=>$v){
			if(empty($v)) continue;
			$post[$v] = $value[$k];
		}
		$data = $post;

       $data['code']=env('api_code');

		unset($data['sign']);
        unset($data['ClientSystem']);
        unset($data['ClientVersion']);
		ksort($data);
		$str = '';
		foreach($data as $k=>$v){
			if($v === null){
				continue;
			}
			$str .= $k . '=' . $v . "&";
			$curl_data[$k] = $v;
		}
		$str = substr($str , 0 , -1);
        echo '发送的str：';
        print_r($str);
		$sign = $post['sign'] = md5($str);	// sign 算法
		$curl_data['sign'] = $sign;
		$this->script_log('生成sign：'.$sign);
        echo '签名：';
        print_r($sign);
		// 设置sign
		$this->script_sign($sign);
		echo '发送的参数：';
		print_r($post);
		// 执行请求
		$sJson = $this->curlpost($curl_data);
		echo "\n".'返回源JSON：';
		print_r($sJson);
		
		$json = json_decode($sJson,true);
		echo "\n".'格式化JSON：';
		print_r($json);
		// 设置 token
		if($post['_cmd'] == 'user_login') $this->script_token($json['data']['ticket']);
	}
	
	private function script_log($log){
		$date = date('Y-m-d H:i:s');
		echo '<script type="text/javascript">log("'.$log.'----'.$date.'");</script>';
	}
	
	private function script_sign($sign){
		echo '<script type="text/javascript">sign("'.$sign.'");</script>';
	}
	private function script_token($ticket){
		echo '<script type="text/javascript">ticket("'.$ticket.'");</script>';
	}
	
	public function curlpost($array){
//		$post = http_build_query($array);
//		$_cmd = $array['_cmd'];
//		$controller = ucfirst(strstr($_cmd, '_', true));
//		$action = substr(strstr($_cmd, '_'), 1);
//		return redirect()->action($controller . 'Controller@' . $action, $array);
		$s = curl_post('http://'.request()->server('HTTP_HOST'),$array);
		return $s;
		
		
//		$post = http_build_query($array);
//		$ch = curl_init();
//
//		$url = $_SERVER['HTTP_HOST'];
//		curl_setopt($ch, CURLOPT_URL, $url);
////		curl_setopt($ch, CURLOPT_PORT, '5000');
//		curl_setopt($ch, CURLOPT_POST, 1);
//		curl_setopt($ch, CURLOPT_HEADER, 0);
//		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
//		$data = curl_exec($ch);
//		curl_close($ch);
//		return $data;
	}
	
	
}
