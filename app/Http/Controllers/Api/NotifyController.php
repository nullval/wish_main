<?php

namespace App\Http\Controllers\Api;

use App\Jobs\checkGhtEnterStatus;
use App\Jobs\checkGhtPay;
use App\Jobs\checkPay;
use App\Jobs\Withdraw;
use App\Libraries\gaohuitong\Ruzhu;
use App\Models\Agent;
use App\Models\AgentInviteFloor;
use App\Models\AgentOperate;
use App\Models\Api\ApiStatement;
use App\Models\Api\ApiTdGiveUseTdLog;
use App\Models\Api\ApiTdStatement;
use App\Models\Api\ApiTdUser;
use App\Models\Bank;
use App\Models\ImmediateArrival;
use App\Models\JavaIncrement;
use App\Models\NineFloor;
use App\Models\Order;
use App\Models\PosFreezePoint;
use App\Models\RuzhuMerchantBasic;
use App\Models\Seller;
use App\Models\Statement;
use App\Models\StatementDateLog;
use App\Models\SysConfig;
use App\Models\TdUser;
use App\Models\User;
use App\Models\UserPurse;
use App\Models\UserWithdraw;
use App\Models\VOrder;
use App\Models\VUserWithdraw;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;

class NotifyController extends CommonController
{
    /**
     * 每三天评估商家信用
     */
    public function evaluateSellerCredit(){
        $Seller=new Seller();
        $Seller->evaluate_seller_credit();
        echo 'ok';
    }

    /**
     * 定时任务查询第三方代付结果 每2分钟查询一次
     */
    public function checkPay(){
        new_logger('test.log','checkPay',['b'=>time2date()]);
        //获取所有未有代付结果的订单
        $withdraw_list=VUserWithdraw::where(['status'=>1])->whereIn('purse_type',[1,3,4,7,10,12])->get();
        foreach ($withdraw_list as $k=>$v){
            $i=$v->created_at->format('Y');
//            $this->dispatch((new CheckPay($v->spare_order_sn,$i))->onQueue('checkWithdraw'));
            $this->dispatch(new CheckPay($v->spare_order_sn,$i));
        }
        echo 'ok';
    }

    /**
     * 定时任务商家返现对账单 每天0点过后执行
     */
    public function createStatement(){
        ignore_user_abort(true);
        //允许长时间执行
        set_time_limit(0);

        //获取昨天日期 (方便手动调数据) 手动调数据必须先把achievement_date_log表里昨日的数据删掉 要先看看status是否为0 1执行成功 0执行失败 0才要进行手动调整
        if(empty($y_date)){
            $yesterday=date("Y-m-d",strtotime("-1 day"));
        }else{
            $yesterday=$y_date;
        }

        //判断昨天日期是否执行(防止多次执行)业绩执行时间记录 (无论执行成功或失败)
        $log_info=StatementDateLog::where(['date'=>$yesterday])->first();
        if(isset($log_info->id)){
            echo '该日期已执行过';exit;
        }

        $ApiStatement=new ApiStatement();
        $ApiStatement->create_statement();

        //添加业绩执行时间记录
        StatementDateLog::updateOrCreate(['date'=>$yesterday,'status'=>1]);
        echo '执行成功';exit;

    }

    /**
     * 定时任务商家返现 每天0点过后执行
     */
    public function backCash($y_date=0){
        ignore_user_abort(true);
        //允许长时间执行
        set_time_limit(0);
        $ApiStatement=new ApiStatement();
        $ApiStatement->back_cach();
        echo 'ok';
    }



    /**
     * 定时任务查询商家入驻高汇通状态 每12个小时执行一次 (顺便绑定银行卡)
     */
    public function checkGhtEnterStatus(){
        $seller_list=RuzhuMerchantBasic::where(['status'=>2])->whereNotNull('merchantId')->orderBy('id','desc')->get();
        foreach ($seller_list as $k=>$v){
            $this->dispatch(new checkGhtEnterStatus($v->merchantId,$v->uid));
        }
    }

    /**
     * 定时任务查询高汇通商家货款提现结果 每分钟检测一次
     */
    public function checkGhtPay(){
        //2018/03/11停止
        //获取所有未有代付结果的订单
//        $withdraw_list=VUserWithdraw::where(['status'=>1,'purse_type'=>4,'member_type'=>2])->get();
//        foreach ($withdraw_list as $k=>$v){
//            $i=$v->created_at->format('Y');
//            $this->dispatch(new checkGhtPay($v->spare_order_sn,$i));
//        }
//        echo 'ok';
    }


    /**
     * @throws \Exception
     * 每天2点初始化机主9层人脉层级
     */
    public function update_agent_invite_floor(){
        ignore_user_abort(true);
        //允许长时间执行
        set_time_limit(0);
        AgentInviteFloor::where('id','>',0)->delete();
        $agent_list=Agent::get();
        foreach($agent_list as $k=>$v){
            $uid=$v->id;
            $Agent=new Agent();
            $Agent->update_agent_invite($uid);
        }
        echo 'ok';
    }

    /*
     * 做对冲的接口
     */
    public function test(Request $request){
        //测试购买pos机正式流程 要用军哥的账号做购买人 磊哥的账号做推荐人
//        $Bank=new Bank();
//        $amount=32760;
//        //获取中央银行钱包
//        $bank_purse_id=$Bank->get_bank_purse()->purse_id;
//        //获取平台收益钱包
//        $sys_purse_id=$Bank->get_sys_purse(1)->purse_id;
//        $arr=$Bank->doTransfer($sys_purse_id,$bank_purse_id,$amount,10028,
//            '测试购买pos机扣除收益,订单号:pos2201711247665791511516352,pos2201711245418391511516665,pos2201711246274581511517092,pos2201711247575651511519751,,pos2201711245511951511531831',
//            '系统测试扣除');
        echo 'ok';
//        exit;
    }


    /**
     * @param Request $request
     * 做脚本为成为运营中心后，返回旧订单的积分
     */
    public function give_operate_old_transfer(Request $request){
        ignore_user_abort(true);
        //允许长时间执行
        set_time_limit(0);
        //获取运营中心手机号码
        $operate_mobile=$request->input('operate_mobile');
        $operate=AgentOperate::where(['mobile'=>$operate_mobile])->first();
        $agent=Agent::where(['mobile'=>$operate_mobile])->first();
        if(isset($agent->id)){
            $agent_id=$agent->id;
            $operate_id=$operate->id;
            $Agent=new Agent();
            $down_arr=$Agent->find_down([$agent_id]);
            $user_list=Agent::whereIn('id',$down_arr)->get(['mobile']);
//            var_dump(norm_arr($user_list,'mobile'));
//            exit;
            $order_list=VOrder::where(['operate_id'=>0,'status'=>2,'type'=>2])->whereIn('buyer_id',$down_arr)->get();
//            var_dump(count($order_list));exit;
            foreach ($order_list as $k=>$v){
                $buyer_mobile=$Agent::where(['id'=>$v->buyer_id])->first()->mobile;
                $AgentOperate=new AgentOperate();
                $AgentOperate->give_pos_order_profit($operate_id,$v->pos_num,$buyer_mobile,$v->order_sn,$v->actual_amount);
                $data['operate_id']=$operate_id;
                //获取年份
                $year=$v->created_at->format('Y');
                DB::table('order_'.$year)->where(['id'=>$v->id])->update($data);
            }
        }
        echo 'ok';
    }

    /**
     * 定时任务返还机主购买未特商城pos机冻结的500积分
     */
    public function back_agent_freeze_point(){
        $Agent=new Agent();
        $list=PosFreezePoint::where(['status'=>1])->get(['order_sn']);
        foreach ($list as $k=>$v){
            $Agent->is_get_point_freeze($v->order_sn);
        }
        echo 'ok';
    }

    /**
     *  定时任务执行商家积分自动挂单 2点半执行
     */
    public function automaticHandelSellerPoint(){
        $seller_list=Seller::get(['id','is_automatic','mobile']);
        $Bank=new Bank();
        $User=new User();
        foreach ($seller_list as $k=>$v){
            if($v->is_automatic==1){
                $mobile=$v->mobile;
                $seller_uid=$v->id;
                $amount=$Bank->use_money($seller_uid,3,2);
                //获取用户id
                $user_id=$User->get_user_id_by_mobile($mobile);
                //自动挂单
                //强制挂两位小数的积分  java要求 保持精度
                $amount=get_num($amount);
                if($amount>1){
                    $JavaIncrement = new JavaIncrement();
                    $JavaIncrement->automatic_guadan($mobile,$amount,$user_id,4);
                }
            }
        }
        echo 'ok';
    }


    /**
     * 定时任务根据对账
     */
    public function giveBackTd(){
        ignore_user_abort(true);
        //允许长时间执行
        set_time_limit(0);
        $ApiTdStatement=new ApiTdStatement();
        $ApiTdStatement->give_back_td();
        echo 'ok';
    }

    /**
     * 后台创建特殊人员返td脚本
     */
    public function createBackTdLog(){
        //第一个月，在年前2-10号释放500个TD，后面每个月20号释放200个TD，每个人100万封顶，一个月一次，已经确定好了，可以执行了。
        //每月20号生成下次的对账单
        if(date('d')=='20'){
            $arr=[9,10,11,12,13,14];
            foreach($arr as $v){
                $d=date('Y-m',strtotime(date('Y-m-d'))+30*24*3600);
                $log['u_date']=$d.'-20';
                $log['user_id']=$v;
                $log['created_at']=time2date();
                $log['status']=1;
                $log['amount']=200;
                $log['order_list']='';
                $log['left_amount']=200;
                $log['remark']='特殊人员后台数据添加';
                ApiTdGiveUseTdLog::insert($log);
            }
        }
        echo 'ok';
    }
}
