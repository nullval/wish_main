<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ApiException;
use App\Exceptions\NotLoginException;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserTicket;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    public function __construct(){

    }

    public function check_login(){
        $UserTicket = new UserTicket();
        $user = $UserTicket->checkLogin();
        $this->user = $user;
        $this->uid = $user['id'];
    }

    public function checkOpenid($openid){
        if(empty($openid)){
            return arr_post(0,'openid未传入');
        }
//        if(empty($mobile)){
//            return arr_post(0,'手机号码未传入');
//        }
        $user_info=User::where(['openid'=>$openid])->first();
        if(!isset($user_info->id)){
            return arr_post(0,'改用户未绑定openid');
        }
        return arr_post(1,'获取用户信息成功',$user_info);
    }

    public function checkNodeid(){
        $nodeid=\request('uid');
        if(empty($nodeid)){
            throw new ApiException('uid未传入');
        }
        $user_info=User::where(['nodeid'=>$nodeid])->first();
        if(!isset($user_info->id)){
            throw new ApiException('该用户未在积分商城注册');
        }
        return $user_info;
    }
}
