<?php

namespace App\Http\Controllers\Api;

use App\Models\Api\Geocoding;
use App\Models\Api\RuzhuMerchantBasic;
use App\Models\Seller;
use App\Models\User;
use App\Models\VOrder;
use App\Models\VTransfer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AllianceController extends Controller
{
	/**
	 * 查询商铺列表
	 * @param $lat 纬度 
	 * @param $lng 经度
	 * @param $area_id 行政代码 若不写则显示所在城市的所有商铺
	 * @param $page 页码
	 * @param $sort  1、销量最高 2、距离最近 ，不写则按默认排序
	 */
    public function get_shop_list(Request $request){
		new_logger('Alliance.log','商铺列表接口参数:',$request->all());
		$lat = $request->input('lat');
		$lng = $request->input('lng');
		$area_id = $request->input('area_id');
		$sort  = $request->input('sort');
		$page  = $request->input('page');
		//获取当前所在的城市和区域
		if (empty($lat) && empty($lng)){
			//默认深圳南山区的坐标,给全列表
			$longitude = '113.948765';
			$latitude = '22.533055';
//			$longitude = null;
//			$latitude = null;
		}else{
			$longitude = $lng;
			$latitude = $lat;
		}
//		$result = Geocoding::getAddressComponent($longitude, $latitude);
//		new_logger('Alliance.log','请求百度Geocoding API参数:',['经度'=> $longitude,'纬度' => $latitude]);
//		new_logger('Alliance.log','请求百度Geocoding API结果:',$result);
		//当前所在城市
//		$city =$result['result']['addressComponent']['city'];
		//当前行政代码
//		$code =$result['result']['addressComponent']['adcode'];
		//当前所在城市的区域
//		$district =$result['result']['addressComponent']['district'];
		//获取当前城市的全部区域
//		$district_list = $this->get_district($city,$district);
//		new_logger('Alliance.log','区域列表:',$district_list);
//		$data['district_list'] = $district_list;
//		unset($data['district_list']['now_city_code']);
//		unset($data['district_list']['now_district_code']);


		//判断行政代码的情况
		if (!empty($area_id)){
			$area_code= $area_id;
		}elseif (empty($area_id)){
			$area_code = null;
		}

		//获取商铺列表
		$RuzhuMerchantBasic = new RuzhuMerchantBasic();
		$seller_list = $RuzhuMerchantBasic -> get_seller_list($latitude,$longitude,$area_id,$area_code,$sort,null,$page);
		$data['seller_list'] = $seller_list;
//		dd($data['seller_list']);
		new_logger('Alliance.log','请求商铺列表接口结果:',$data);

		return json_success('成功',$data);

	}

	/**
	 * 获取城市的全部区域
	 * @param $lat 纬度
	 * @param $lng 经度
	 * @param $page 页码
	 * @return mixed
	 */
	public function get_district_list(Request $request){
		new_logger('Alliance.log','商铺列表接口参数:',$request->all());
		$lat = $request->input('lat');
		$lng = $request->input('lng');
		//获取当前所在的城市和区域
		if (empty($lat) && empty($lng)){
			//默认深圳南山区的坐标
			$longitude = '113.948765';
			$latitude = '22.533055';
		}else{
			$longitude = $lng;
			$latitude = $lat;
		}
		$result = Geocoding::getAddressComponent($longitude, $latitude);
		new_logger('Alliance.log','请求百度Geocoding API参数:',['经度'=> $longitude,'纬度' => $latitude]);
		new_logger('Alliance.log','请求百度Geocoding API结果:',$result);
		//当前所在城市
		$city_name =$result['result']['addressComponent']['city'];
		//当前所在城市的区域
		$district =$result['result']['addressComponent']['district'];
		// 从文件中读取数据到PHP变量
		$json_string =file_get_contents(asset('sanji/js/code.json')) ;
		// 把JSON字符串转成PHP数组
		$data = json_decode($json_string, true);
		new_logger('Alliance.log','省市区列表:',$data);
		//遍历找出城市的行政代码
		foreach ($data as $k => $v){
			foreach ($v as $key => $value){
				if ($city_name == $value){
					$city_code = $key;
				}
			}
		}
		//判断此时的城市是否是直辖市
		if($city_code == 110000 || $city_code == 120000 || $city_code == 310000){
			$city_code = substr($city_code , 0 , 2).'0100';
		}
		//根据城市的行政代码找出全部的区域的名字和代码
		foreach ($data as $key => $value){
			if ($key == $city_code){
				//获取当前所在区的行政代码
				foreach ($value as $district_code => $district_name) {
					if ($district == $district_name) {
						$district_list[0] = [
							'district_code' => $district_code,
							'district_name' => '附近',
						];
					}
					$district_list[] = [
						'district_code' => $district_code,
						'district_name' => $district_name,
					];
				}
			}
		}
		return json_success('成功',$district_list);
	}

	/**
	 * 搜索商铺接口
	 * @param $lat 纬度
	 * @param $lng 经度
	 * @param $name 搜索内容
	 * * @return mixed
	 */
	public function search(Request $request){
		new_logger('Alliance.log','搜索商铺接口参数:',$request->all());
		$lat = $request->input('lat');
		$lng = $request->input('lng');
		$name = $request->input('name');
		$page  = $request->input('page');
		//获取当前经纬度，没有显示默认
		if (empty($lat) && empty($lng)){
			//默认深圳南山区的坐标
			$longitude = '113.948765';
			$latitude = '22.533055';
		}else{
			$longitude = $lng;
			$latitude = $lat;
		}
		//判断是否有输入内容
		if (empty($name)){
			$sort = 4;
		}else{
			$sort = null;
		}
		//获取商铺列表
		$RuzhuMerchantBasic = new RuzhuMerchantBasic();
		$seller_list = $RuzhuMerchantBasic -> get_seller_list($latitude,$longitude,null,null,$sort,$name,$page);
		$data = $seller_list;
//		dd($data['seller_list']);

		return json_success('成功',$data);
	}

	/**
	 * 商铺详情接口
	 * @param $id 商铺id
	 * @return mixed
	 */
	public function seller_detail(Request $request){
		new_logger('Alliance.log','商铺详情接口参数:',$request->all());
		$id = $request->input('id');
		//获取商铺信息
		$seller = RuzhuMerchantBasic::where(['id' => $id])
			->select('id','logoimage','merchantName','average','business_time','area_id','merchantAddress','servicePhone','uid')
			->first();
		//统计商家的实际销量和人均
		$count = VOrder::where(['uid' => $seller -> uid]) ->count();
		$sum = VOrder::where(['uid' => $seller -> uid]) ->sum('amount');
		if ($count == 0){
			$seller ->average = $seller ->average;
		}else{
			$seller ->average = $seller ->average+round($sum/$count,2);
		}
		//获取详细地址
		// 从文件中读取数据到PHP变量
		$json_string =file_get_contents(asset('sanji/js/code.json')) ;
		// 把JSON字符串转成PHP数组
		$data = json_decode($json_string, true);
		foreach ($data as $item => $value){
			foreach ($value as $k => $v){
				if ($k == substr($seller -> area_id , 0 , 2).'0000'){
					$province = $v;
				}
				if ($k == substr($seller -> area_id , 0 , 4).'00'){
					$city= $v;
				}
				if ($k == $seller -> area_id){
					$district = $v;
				}
			}
		}
		$seller -> address = $province.$city.$district.$seller -> merchantAddress;
		unset($seller -> merchantAddress,$seller -> area_id);
		//获取轮播图
//		$banner = DB::table('banner') -> where(['seller_id' => $seller -> uid])
//			->limit(3)
//			->select('id','businessimage','name')
//			->get();
        $banner=[];
        $banner[0]->businessimage=asset('banner.jpg');

		//判断是否有banner,并将$banner转化为数组
//		$banner_arr = json_decode($banner, ture);
//		for ($i = 0 ; $i < 3 ; $i++){
//			if (empty($banner_arr[$i])){
//				$banner1[$i] = null;
//			}else{
//				$banner1[$i] = $banner_arr[$i];
//			}
//		}
		$seller -> banner = $banner;

		//整理数据
		return json_success('成功',$seller);

	}

	/**
	 * 定位接口
	 * @param $lat 纬度
	 * @param $lng 经度 
	 * * @return mixed
	 */
	public function Location(Request $request){
		new_logger('Alliance.log','定位接口参数:',$request->all());
		$lat = $request->input('lat');
		$lng = $request->input('lng');
		//获取当前经纬度，没有显示默认
		if (empty($lat) && empty($lng)){
			//默认深圳南山区的坐标
			$longitude = '113.948765';
			$latitude = '22.533055';
		}else{
			$longitude = $lng;
			$latitude = $lat;
		}
		$result = Geocoding::getAddressComponent($longitude, $latitude);
		new_logger('Alliance.log','请求百度Geocoding API参数:',['经度'=> $longitude,'纬度' => $latitude]);
		new_logger('Alliance.log','请求百度Geocoding API结果:',$result);
		//当前所在城市
		$data['city'] =$result['result']['addressComponent']['city'];
		//当前所在城市的区域
		$data['district'] =$result['result']['addressComponent']['district'];
		return json_success('成功',$data);
	}


	public function edit(){
		$data = RuzhuMerchantBasic::where('id','<>',null)->get();
		foreach ($data as $value){
			RuzhuMerchantBasic::where('id','=',$value -> id)->update(['volume' => rand(100,1000),'average' => rand(20,80),'business_time' => "8:00-22:00"]);
		}
		return json_success('ok');
	}
	
}
