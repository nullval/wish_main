<?php

namespace App\Http\Controllers\Api;
use App\Exceptions\ApiException;
use App\Jobs\changePoint;
use App\Models\Agent;
use App\Models\Bank;
use App\Models\BankList;
use App\Models\JavaIncrement;
use App\Models\Message;
use App\Models\OptionPointAdmin;
use App\Models\Order;
use App\Models\Seller;
use App\Models\Transfer;
use App\Models\User;
use App\Models\UserBank;
use App\Models\UserPurse;
use App\Models\UserWithdraw;
use App\Models\VOrder;
use App\Models\VTransfer;
use App\Models\VUserWithdraw;
use App\Models\Weachat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/**积分商城/积分增值对接**/
class PointController extends CommonController
{
    //通过oepnid和手机号来做验证
    //积分增值 获取用户积分余额
    public function getUserPoint(Request $request){
        //验证用户openid
        $user_info=$this->checkNodeid();

        $Bank=new Bank();
        $type=empty($request->input('type'))?3:$request->input('type');
        switch ($request->input('member_type')){
            case 0:
                $owner_type=1;
                $user_id=$user_info->id;
                break;
            case 1:
                $owner_type=1;
                $user_id=$user_info->id;
                break;
            case 2:
                $owner_type=2;
                $user_id=Seller::where(['mobile'=>$user_info->mobile])->value('id');
                if(empty($user_id)) return json_error('商家不存在');
                break;
        }
        $purse_info=$Bank->userWallet($user_id,$type,$owner_type);
        $data['balance']=get_last_two_num($purse_info->balance-$purse_info->freeze_value); //可用积分
        $data['freeze_value']=get_last_two_num($purse_info->freeze_value); //冻结积分
        return json_success('用户积分获取成功',$data);
    }

    /**
     * @param Request $request
     * @return array
     * 发送短信 java专用
     */
    public function send_msg(Request $request){
        new_logger('point_getUserPoint.log','res',$request->all());
        $mobile=$request->input('mobile');
        $remark=$request->input('remark');
        if(empty($remark)){
            return json_error('模板不能为空');
        }
        $Message=new Message();
        return $Message->send_sms($mobile,6,$remark,'',[],0);
    }

    /**
     * 判断是否商家
     */
    public function isSeller(Request $request){
        //验证用户
        $user_info=$this->checkNodeid();
        $seller_uid=Seller::where(['mobile'=>$user_info['mobile']])->value('id');
        if(empty($seller_uid)){
            return json_error('不存在该商家');
        }
        return json_success('商家存在');
    }

    /**
     * @param Request $request
     * @return array
     * 撤单返回商家积分
     */
    public function backSellerPoint(Request $request){
        new_logger('point_backSellerPoint.log','res',$request->all());
        $amount=$request->input('amount');
        $order_sn=$request->input('order_sn');
        $uid=$request->input('uid');
        $user_info=$this->checkNodeid($uid);
        $mobile=$user_info['mobile'];
        $seller_uid=Seller::where(['mobile'=>$mobile])->value('id');
        if(!empty($seller_uid)&&$amount>0){
            $Bank=new Bank();
            $sys_point_purse_id=$Bank->get_sys_purse_id(3);
            $seller_point_purse_id=$Bank->userWallet($seller_uid,3,2)->purse_id;

            $Bank->doApiTransfer($sys_point_purse_id,$seller_point_purse_id,$amount,10084,
                '商家积分增值撤单返还补贴积分,订单编号'.$order_sn,
                '商家撤单返还积分');
            return json_success('处理成功');
        }else{
            return json_error('商家不存在');
        }

    }

    /**
     * @param Request $request
     * 积分增值 兑换/转卖积分
     */
    public function changeUserPoint(Request $request){
        new_logger('point_changeUserPoint.log','res',$request->all());
        //验证用户openid
        $user_info=$this->checkNodeid();
        //积分转换
        $arr[0]=$request->input('order_sn');
        $type=empty($request->input('type'))?1:$request->input('type');
        //用户身份
        $member_type=$request->input('member_type');
        $amount=get_last_two_num($request->input('amount'));
        $option=$request->input('option');
        $give_type=$request->input('give_type');
        $give_type=empty($give_type)?1:$give_type;
        $user_id=$user_info->id;
        $mobile=$user_info->mobile;


        //队列处理(java最后一步处理 不管结果)
        $this->dispatch((new changePoint($member_type,$option,$user_id,$amount,$type,$arr,$mobile,$give_type))->onQueue('handleOrder'));
//        $this->dispatch(new changePoint($member_type,$option,$user_id,$amount,$type,$arr,$mobile,$give_type));

        //java要求无改
        return arr_post("1",'操作成功');

    }

    /**
     * @param Request $request
     * @return array
     * 积分增值 验证是否设置了支付米阿米
     */
    public function exitsPayPwd(Request $request){
        $ticket=$request->input('ticket');
        //中转请求积分商城
        $data['ticket']=$ticket;
        $data['_cmd']='usercenter_exists_paypwd';
        $data['sign']=get_shop_pos_sign($data);
        $res=json_decode(curl_post(env('SHOP_DOMAIN'),$data),true);
        return arr_post($res['status'],$res['info'],$res['data']);
    }

    /**
     * @param Request $request
     * @return array
     * 积分增值 验证积分商城支付密码
     */
    public function checkPayPwd(Request $request){
        $ticket=$request->input('ticket');
        $pay_password=$request->input('pay_password');
        //中转请求积分商城
        $data['ticket']=$ticket;
        $data['pay_password']=$pay_password;
        $data['_cmd']='point_checkPayPwd';
        $data['sign']=get_shop_pos_sign($data);
        $res=json_decode(curl_post(env('SHOP_DOMAIN'),$data),true);
        return arr_post($res['status'],$res['info'],$res['data']);
    }

    /**
     * @param Request $request
     * 积分增值 验证ticket 成功则返回oepnid 积分商城用户uid
     */
    public function checkTicket(Request $request){
        $ticket=$request->input('ticket');
        //中转请求积分商城
        $data['ticket']=$ticket;
        $data['_cmd']='point_checkTicket';
        $data['sign']=get_shop_pos_sign($data);
        $res=json_decode(curl_post(env('SHOP_DOMAIN'),$data),true);
        if($res['status']==1){
            //获取用户类型
            $res['data']['member_type']=1;
            $seller_info=Seller::where(['mobile'=>$res['data']['nodecode']])->first();
            if(isset($seller_info->id)){
                $res['data']['member_type']=2;
            }
//            if(env('APP_ENV')!='online'){
//                //测试服务器
//                if($res['data']['nodecode']=='15611111117'||$res['data']['nodecode']=='15611111119'){
//                    //平台账号
//                    $res['data']['member_type']=3;
//                }
//            }
//            if($res['data']['nodecode']=='15923154870'){
//                //平台账号
//                $res['data']['member_type']=3;
//            }
        }
        //判断是用户
//        var_dump($res);exit;
        return arr_post($res['status'],$res['info'],$res['data']);
    }


    /**
     * @param Request $request
     * @return array|bool
     * 积分增值积分购买CHQ币冻结
     */
    public function freezePoint(Request $request){
        new_logger('point_freezePoint.log','res',$request->all());
        //验证用户openid
        $user_info=$this->checkNodeid();

        //验证操作员是否有权限
//        $OPA=new OptionPointAdmin();
//        $option_id=$OPA->check_user_status();
        $Bank=new Bank();
        $amount=get_last_two_num(\request('amount'));
        //根据用户身份冻结不同身份的积分
        $member_type=$request->input('member_type');
        if($member_type==1||empty($member_type)){
            //用户
            $owner_type=1;
            $user_id=$user_info->id;
        }elseif($member_type==2){
            //商家
            $owner_type=2;
            $mobile=$user_info->mobile;
            $seller_info=Seller::where(['mobile'=>$mobile])->first(['id']);
            if(!isset($seller_info->id)){
                return json_error('不存在该商家');
            }
            $user_id=$seller_info->id;
        }
        $hisid=$Bank->doFreeze($user_id,3,$amount,'积分购买CHQ冻结,操作员id:0',$owner_type);
        $data['hisid']=date('Y').'-'.$hisid;
        return json_success('冻结成功',$data);
    }


    public function unFreezePoint(Request $request){
        new_logger('point_unFreezePoint.log','res',$request->all());
        DB::transaction(function(){
            //验证用户openid
            $user_info=$this->checkNodeid();

            $hisid=\request('hisid');
            $his_arr=explode('-',$hisid);
            $type=\request('type');
            $amount=get_last_two_num(\request('amount'));
            $order_sn=\request('order_sn');
            $member_type=\request('member_type');
            $is_all=\request('is_all') ?? 0; //是否全部解冻 0否 1是
            if($is_all==1){
                //全部解冻
                $amount=get_last_two_num(DB::table('freeze_his_'.$his_arr[0])->where(['hisid'=>$his_arr[1]])->value('amount'));
            }
            if(empty($order_sn)){
                return json_error('请传入订单编号');
            }
            //解冻部分资金

            $Bank=new Bank();
            $rs=$Bank->unApiPathFreeze($his_arr[1],$his_arr[0],$amount);
            $mobile=$user_info->mobile;
            //积分钱包
            $point_purse_id=$Bank->userWallet($user_info->id,3,1)->purse_id;
            //1成功成交的订单  2撤回的订单
            if($type==1){
                $UserPurse=new UserPurse();
                if($member_type==1||empty($member_type)){
                    //用户
                    //系统回收
                    $rs[]=$UserPurse->j_to_t_action(1,$mobile,$amount,$order_sn,$point_purse_id);
                }elseif($member_type==2){
                    //商家
                    $seller_info=Seller::where(['mobile'=>$mobile])->first(['id']);
                    if(!isset($seller_info->id)){
                        return json_error('不存在该商家');
                    }
                    $seller_uid=$seller_info->id;
                    $point_seller_purse_id=$Bank->userWallet($seller_uid,3,2)->purse_id;
                    //商家
                    //兑换
                    //商家兑换 商家->用户->系统
                    $rs[]= $UserPurse->j_to_t_action(2,$mobile,$amount,$order_sn,$point_purse_id,$point_seller_purse_id);
                }

            }elseif($type==2){
                //返回用户钱包
                //不做流水处理
            }
        });
//            return json_success('处理成功');
//        return json_error('解冻失败');
        return json_success('处理成功');
    }



    /*
     * @param Request $request
     * 积分商城 使用积分购买商品
     */
    public function buyGoodsUserPoint(Request $request){
        //验证用户openid
        $user_info=$this->checkNodeid();
        $option_id=0;
        $UserPurse=new UserPurse();
        //消耗积分
        $arr[0]=$request->input('order_sn');
        $UserPurse->change_point($option_id,1,$user_info->id,$request->input('amount'),2,$arr);
        return json_success('积分扣除成功');
    }

    /**
     * @param Request $request
     * 同步积分商城用户 java积分增值
     * 积分商城生成用户 主业务也要相应的生成业务 并绑定java积分增值 开通CHQ账号
     * 积分商城登录注册使用
     */
    public function synchroUser(Request $request){
        $mobile=$request->input('mobile');
        $openid=$request->input('openid');
        $uid=$request->input('uid');
        $rs=bind_openid($mobile,$openid,1,$uid);
        if($rs){
            return json_success('同步成功');
        }else{
            return json_error('同步失败');
        }
    }

    /**
     * @param $openid
     * 检测改openid是否存在于机主或商家中
     */
    public function checko(Request $request)
    {
        $openid=$request->input('openid');
        $agent_info=Agent::where(['openid'=>$openid])->first(['id','mobile']);
        //若存在则返回
        if(isset($agent_info->id)){
            User::updateOrCreate(['mobile'=>$agent_info->mobile],['openid'=>$openid]);
            return json_success('代理已存在',['mobile'=>$agent_info['mobile']]);
        }else{
            $seller_info=Seller::where(['openid'=>$openid])->first(['id','mobile']);
            if(isset($seller_info->id)){
                User::updateOrCreate(['mobile'=>$seller_info->mobile],['openid'=>$openid]);
                return json_success('商家已存在',['mobile'=>$seller_info['mobile']]);
            }
        }
        return json_error('不存在');
    }

    /**
     * 用户转账 (适用于积分商城)
     */
    public function doTransfer(Request $request){
        new_logger('point_doTransfer.log','res',$request->all());
        //验证操作员是否有权限
//        $OPA=new OptionPointAdmin();
//        $OPA->check_user_status();

        $Bank=new Bank();
        //转出钱包类型
        $from_purse_type=$request->input('from_purse_type');
        //转出用户类型
        $from_owner_type=$request->input('from_owner_type');
        //转入钱包类型
        $to_purse_type=$request->input('to_purse_type');
        //转入用户类型
        $to_owner_type=$request->input('to_owner_type');

        if($from_owner_type==4){
            //中央银行
            $from_purse_id=$Bank->get_bank_purse_id($from_purse_type);
        }elseif($from_owner_type==3){
            //系统钱包
            $from_purse_id=$Bank->get_sys_purse_id($from_purse_type);
        }elseif($from_owner_type==5){
            //积分钱包
            $from_purse_id=$Bank->get_d_purse(3,3)->purse_id;
        }elseif($from_owner_type==10){
            //积分商城积分钱包
            $from_purse_id=$Bank->get_d_purse(3,10)->purse_id;
        }else{
            //转出用户钱包信息
            $from_uid=$request->input('from_uid');
            $from_info=User::where(['nodeid'=>$from_uid])->first();
            if(!isset($from_info->id)){
                return json_error('转出用户不存在或在积分商城未注册');
            }
            $from=$from_info->id;
            $from_purse_id=$Bank->userWallet($from,$from_purse_type,$from_owner_type)->purse_id;
        }

        if($to_owner_type==4){
            //中央银行
            $to_purse_id=$Bank->get_bank_purse_id($to_purse_type);
        }elseif($to_owner_type==3){
            //系统钱包
            $to_purse_id=$Bank->get_sys_purse_id($to_purse_type);
        }elseif($to_owner_type==5){
            //积分钱包
            $to_purse_id=$Bank->get_d_purse(3,3)->purse_id;
        }elseif($to_owner_type==10){
            //积分商城积分钱包
            $to_purse_id=$Bank->get_d_purse(3,10)->purse_id;
        }else {
            //转入用户钱包信息
            $to_uid = $request->input('to_uid');
            $to_info = User::where(['nodeid' => $to_uid])->first();
            if (!isset($to_info->id)) {
                return json_error('转入用户不存在或在积分商城未注册');
            }
            $to = $to_info->id;
            $to_purse_id = $Bank->userWallet($to, $to_purse_type, $to_owner_type)->purse_id;
        }

        $amount=$request->input('amount');
        $reason=$request->input('reason');
        $detail=$request->input('detail');
        $remarks=$request->input('remarks');

        $Bank->doApiTransfer($from_purse_id ,$to_purse_id ,$amount ,$reason ,$detail,$remarks);
        return json_success('转账成功');
    }


    /**
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\ApiException
     * 获取用户所有财务信息
     */
    public function getUserAllPoint(){
        //验证用户openid
        $user_info=$this->checkNodeid();
        $user_id=$user_info->id;
        $arr=[
            'goods'=>1, //货款
            'point'=>3, //积分
            'cost'=>10, //消费积分
            'exchange'=>11, //兑换积分
            'redpack'=>12 //红包收益
        ];
        $Bank=new Bank();
        $data=[];
        foreach ($arr as  $k=>$v){
            $purse_info=$Bank->userWallet($user_id,$v,1);
            $data[$k]=[
                'balance'=>get_last_two_num($purse_info->balance-$purse_info->freeze_value),
                'freeze_value'=>get_last_two_num($purse_info->freeze_value)
                ];
        }
        return json_success('用户积分获取成功',$data);
    }

    /**
     * 添加银行卡
     */
    public function addBank(){

        //验证用户openid
        $user_info=$this->checkNodeid();

        $UserBank=new UserBank();
        $UserBank->add_bank(
            $user_info->id,
            \request('account_name'),
            \request('account_bank'),
            \request('bank_account'),
            \request('ibankno'),
            \request('isdefault'),1,0,\request('identity'),\request('phone')
        );
        return json_success('银行卡新增成功');

    }

    /**
     * 编辑银行卡
     */
    public function editBank(){

        //验证用户openid
        $user_info=$this->checkNodeid();

        if(empty(\request('bank_id'))){
            return json_error('请选择要修改的银行卡');
        }

        $UserBank=new UserBank();
        $UserBank->add_bank(
            $user_info->id,
            \request('account_name'),
            \request('account_bank'),
            \request('bank_account'),
            \request('ibankno'),
            \request('isdefault'),
            1,
            \request('bank_id'),\request('identity'),\request('phone')
        );
        return json_success('银行卡编辑成功');
    }

    /**
     * 删除银行卡
     */
    public function delBank(){

        //验证用户openid
        $user_info=$this->checkNodeid();

        $UserBank=new UserBank();
        $UserBank->del_bank(
            \request('bank_id'),
            $user_info->id,1);
        return json_success('删除成功');
    }

    /***
     * 获取银行卡列表
     */
    public function getBankList(){

        //验证用户openid
        $user_info=$this->checkNodeid();

        $bank_list=UserBank::where(['uid'=>$user_info->id,'type'=>1])
            ->get(['id as bank_id','account_name','account_bank','bank_account','ibankno','isdefault','created_at','updated_at','identity','phone']);
        foreach ($bank_list as $k=>$v){
            $bank_list[$k]['bank_account']=handle_bankcard($v['bank_account']);
        }
        return json_success('银行卡列表获取成功',$bank_list);
    }

    /**
     * 设置银行卡为默认
     */
    public function setBankDefault(){

        //验证用户openid
        $user_info=$this->checkNodeid();

        $UserBank=new UserBank();
        $UserBank->update_bank_default(
            \request('bank_id'),
            $user_info->id,1);
        return json_success('设置成功');
    }

    /**
     * 获取默认的银行卡信息
     */
    public function getDefaultBank(){

        //验证用户openid
        $user_info=$this->checkNodeid();


        $bank_info=UserBank::where(['uid'=>$user_info->id,'type'=>1,'isdefault'=>1])->first(['id as bank_id','account_name','account_bank','bank_account','ibankno','isdefault','created_at','updated_at']);
        $bank_info['bank_account']=handle_bankcard($bank_info['bank_account']);
        if(isset($bank_info->bank_id)){
            return json_success('获取成功',$bank_info);
        }else{
            return json_error('还未设置默认银行卡,请前往设置');
        }
    }

    /**
     * 通过消费者id获取商家消费让利
     */
    public function platformAmount(){
        $uid=\request('uid');
        $user_info=$this->checkNodeid($uid);
        //消费者id
        $id=$user_info->id;
        $seller_id=Order::whereIn('type',[1,3,5])->where(['buyer_id'=>$id])->where(['status'=>2])->orderBy('id','desc')->value('uid');
//        dd($seller_id);
        $Seller=new Seller();
        $data['platform_amount']=$Seller->get_platform_amount($seller_id);
        return json_success('获取成功',$data);
    }

    /**
     * 模糊查询银行卡所属地区银行联行号
     */
    public function searchIbnkNo(Request $request){

        $bank_list_info=BankList::where('bank_name','like','%'.$request->input('bank_name').'%')->first();
        if(isset($bank_list_info->id)){
            return json_success('获取成功',$bank_list_info);
        }else{
            return json_error('找不到相应银行卡所属地区银行联行号');
        }
    }

    /**
     *获取银行卡所属地区银行联行号列表
     */
    public function getIbnkList(){
        $bank_list=BankList::get(['bank_name','lbnkNo']);
        if($bank_list){
            return json_success('获取成功',$bank_list);
        }else{
            return json_error('获取失败');
        }
    }

    /**
     * 获取银行卡详情
     */
    public function getBankDetail(Request $request){

        //验证用户openid
        $user_info=$this->checkNodeid();
        $bank_id=$request->input('bank_id');
        if(empty($bank_id)){
            return json_error('请选择相应的银行卡');
        }
        $bank_info=UserBank::where(['uid'=>$user_info->id,'type'=>1,'id'=>$bank_id])
            ->first(['id as bank_id','account_name','account_bank','bank_account','ibankno','isdefault','created_at','updated_at']);
        $bank_info['bank_account']=handle_bankcard($bank_info['bank_account']);
        return json_success('获取成功',$bank_info);
    }

    /**
     * 用户提现
     */
    public function withdraw(Request $request){
        new_logger('point_withdraw.log','res',$request->all());
        //验证操作员是否有权限
//        $OPA=new OptionPointAdmin();
//        $OPA->check_user_status();
        //验证用户openid
        $user_info=$this->checkNodeid();
        //用户类型
        $user_type=\request('user_type') ?? 1;
        //钱包类型
        $pursetype=\request('pursetype') ?? 1;
        $UserWithdraw=new UserWithdraw();
        $UserWithdraw->sub_withdraw($user_info->id,$user_type,$pursetype,\request('amount'));
        return json_success('受理成功');
    }

    /**
     * 用户提现列表(结算列表)
     */
    public function withdrawList(Request $request){

        //验证用户openid
        $user_info=$this->checkNodeid();

        $status=\request('status') ?? 0;
        $type=\request('pursetype') ?? 1;

        //视图中获取
        $witddraw_list=VUserWithdraw::where(['user_id'=>$user_info->id,'member_type'=>1])
            ->where(function ($query) use ($status){
                if(!empty($status)){
                    return $query->where(['status'=>$status]);
                }
            })
            ->where(['purse_type'=>$type])
            ->select(['id','account_name','account_bank','bank_account','ibankno','amount','created_at','status','remark'])
            ->orderBy('created_at','desc')
            ->pagesParse();
        foreach ($witddraw_list as $k=>$v){
            $witddraw_list[$k]['bank_account']=handle_bankcard($v['bank_account']);
        }
        return json_success('获取成功',$witddraw_list);

    }



    /**
     * @param Request $request
     * @return array
     * 用户财务列表
     */
    public function transferList(Request $request){
        //验证用户openid
        $user_info=$this->checkNodeid();
        $user_id=$user_info->id;
        $status=$request->input('status');
        $type=\request('type') ?? 1;
        $Bank=new Bank();
        //根据币种类型获取相应的钱包id
        $purse_id=$Bank->userWallet($user_id,$type,1,['purse_id'])->purse_id;
        $Transfer=new Transfer();
        //总收入
        $data1['total_into']=get_last_two_num($Transfer->get_total_by_purse_id($purse_id,'into'));
        //总支出
        $data1['total_out']=get_last_two_num($Transfer->get_total_by_purse_id($purse_id,'out'));

        $sql='';
        for ($i=2017;$i<=date('Y');$i++){
            $sql .= "select * from transfer_".$i;
            if($status==0){
                $sql.=" where into_purse_id=".$purse_id." or out_purse_id=".$purse_id." ";
            }elseif($status==1){
                $sql.=" where into_purse_id=".$purse_id." ";
            }elseif($status==2){
                $sql.=" where out_purse_id=".$purse_id." ";
            }
            $sql.=' and status=0 ';
            if ($i != date('Y')){
                $sql = $sql." union ";
            }
        }

        $page = request()->input('page',1);
        if($page < 1){
            $page = 1;
        }
        $page_size = 15;
        $offset = ($page - 1) * $page_size;
        $transfer=DB::table(DB::raw("(".$sql.") as t"))
            ->orderBy('t.create_time','desc')
            ->orderBy('t.transfer_id','desc')
            ->offset($offset)->limit($page_size)->get();
        //字段处理
        $data2=[];
        foreach ($transfer as $k=>$v){
            if($v->into_purse_id==$purse_id) {
                //收入
                $data2[$k]['balance']=get_last_two_num($v->into_balance);
                $data2[$k]['amount']=get_last_two_num($v->into_amount);
                $data2[$k]['status']=1;
            }elseif($v->out_purse_id==$purse_id){
                //支出
                $data2[$k]['balance']=get_last_two_num($v->out_balance);
                $data2[$k]['amount']=get_last_two_num($v->out_amount);
                $data2[$k]['status']=2;
            }
            $data2[$k]['time']=date('Y-m-d H:i:s',$v->create_time);
            //描述
            if($v->reason=='10026'){
                $reason='兑换';
            }elseif($v->reason=='10027'){
                $reason='转让';
            }else{
                if($v->into_purse_id==$purse_id) {
                    //收入
                    $reason='消费获得';
                }elseif($v->out_purse_id==$purse_id){
                    //支出
                    $reason='消耗';
                }
            }
            $data2[$k]['reason']=$reason;
        }


        $total_data['balance']=$data1;
        $total_data['tranfer']=$data2;

        return json_success('获取成功',$total_data);

    }


    /**
     * 通过手机号码找相应的用户id
     */
    public function getUserIdByMobile(){
        $User=new User();
        $uid=$User->get_user_id_by_mobile(\request('mobile'),\request('type'));
        return json_success('获取成功',['uid'=>$uid]);
    }


    /**
     * 测试接口用于测试自动挂单
     */
    public function automaticPoint(Request $request){
        if(env('APP_ENV')=='local'){
            $Bank=new Bank();
            $JavaIncrement = new JavaIncrement();
            $uid=$request->input('uid');
            $member_type=$request->input('member_type');
            $point=get_last_two_num($request->input('point'));
            $automatic=$request->input('automatic');
            //获取用户id
            $user_info=User::where(['nodeid'=>$uid])->first();
            $id=$user_info->id;
            $mobile=$user_info->mobile;
            $sys_point_purse_id=$Bank->get_sys_purse_id(3);
            if($member_type==1){
                $user_point_purse_id=$Bank->userWallet($id,3,1)->purse_id;
                $Bank->doApiTransfer($sys_point_purse_id,$user_point_purse_id,$point,20000,
                    '',
                    '用户消费买单获得积分');
                if(empty($automatic)) {
                    $JavaIncrement->automatic_guadan($mobile, $point, $id, 3);
                }
            }elseif($member_type==2){
                $seller_id=Seller::where(['mobile'=>$user_info->mobile])->value('id');
                $seller_point_purse_id=$Bank->userWallet($seller_id,3,2)->purse_id;
                $Bank->doApiTransfer($sys_point_purse_id,$seller_point_purse_id,$point,10000,
                    '',
                    '商家返现获得积分');
                if(empty($automatic)){
                    $JavaIncrement->automatic_guadan($mobile,$point,$seller_id,4);
                }
            }
            echo '成功';exit;
        }
        echo '禁止使用';exit;
    }

    /**
     * @param Request $request
     * 积分增值 兑换/转卖积分
     */
    public function notifySendTd(Request $request){
        new_logger('point_notifySendTd.log','res',$request->all());
        $Weachat=new Weachat();
        $uid=$request->input('uid');
//        $member_type=$request->input('member_type');
        $td=$request->input('td')/10000;
        //获取用户id
        $user_info=User::where(['nodeid'=>$uid])->first();
        $mobile=$user_info->mobile;
        $notify_date=date('Y-m-d H:i');
        //获取最近一笔订单
        $order_info=Order::where(['buyer_id'=>$user_info->id,'status'=>2])
            ->whereIn('type',[1,3,5])
            ->orderBy('id','desc')
            ->first();
        if($td>0&&isset($order_info->id)){
            $notify_wechat_arr = [
                '消费者消费获得CHQ',
                $notify_date,
                '消费买单订单',
                handle_mobile($mobile),
                '获得CHQ',
                $td."\n订单编号:   ".$order_info->order_sn."\n消费金额:   ".$order_info->actual_amount
            ];
            $notify_msg_remark = '亲,恭喜您于'.$notify_date.'消费,平台赠送您CHQ'.$td.'个,详情请关注微信公众号-未特商城。';
            $Weachat->push_buy_good_message($mobile, 1, $notify_wechat_arr, $notify_msg_remark);
        }
        return json_success('推送成功');
    }

    /*
     * 获取用户身份
     */
    public function getMemberType(Request $request){
        $nodeid=$request->input('nodeid');
        $is_get_td=\request('is_get_td') ?? 1;
        //获取用户td
        $JavaIncrement=new JavaIncrement();
        $param=[
            'currency'=>101,
            'uid'=>$nodeid
        ];
        if($is_get_td==1){
            $info=$JavaIncrement->change_over('api/json/user/account/historycount.do',$param,'get');
            //用户是否拥有td
            $data['td_balance']=$info['data']?1:0;
        }
        $user_info=User::where(['nodeid'=>$nodeid])->first();
        if(isset($user_info->mobile)){
            $mobile=$user_info->mobile;
            $agent_id=Agent::where(function ($query) use ($mobile){
                return $query->where(['mobile'=>$mobile])->whereNotIn('inviter_id',[0]);
            })->orWhere(function ($query) use ($mobile){
                return $query->where(['mobile'=>$mobile])->where(['is_admin'=>1]);
            })->value('id');
        }
        $buyer_id=VOrder::where(['buyer_id'=>$user_info->id,'status'=>2,'type'=>4])->value('buyer_id');
        $data['is_agent']=empty($agent_id)?0:1;
        $data['is_buy_point']=empty($buyer_id)?0:1;
        return json_success('获取成功',$data);
    }

    /**
     * @return array
     * 获取商家余额
     */
    public function getSellerBalance(){

        $validator = Validator::make(\request()->all(), [
            'mobile' => 'required'
        ],[
            'mobile.required'=>'商家手机号不能为空'
        ]);

        throw_vaildator_error($validator);

        $mobile=\request('mobile');
        $uid=Seller::where(['mobile'=>$mobile])->value('id');
        if(empty($uid)){
            return json_error('商家不存在');
        }
        $purse_type=\request('purse_type') ?? 7;//钱包类型 默认为商家充值钱包
        $Bank=new Bank();
        $purse_info=$Bank->userWallet($uid,$purse_type,2);
        $data['balance']=get_last_two_num($purse_info->balance-$purse_info->freeze_value); //可用积分
        $data['freeze_value']=get_last_two_num($purse_info->freeze_value); //冻结积分
        return json_success('商家余额获取成功',$data);
    }

}
