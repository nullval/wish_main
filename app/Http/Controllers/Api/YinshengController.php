<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ApiException;
use App\Libraries\BankSdk;
use App\Models\Order;
use App\Models\Pos;
use App\Models\RuzhuMerchantBasic;
use App\Models\Seller;
use App\Models\User;
use Illuminate\Http\Request;

class YinshengController extends CommonController
{
    private $new_pay_type=[
        'stage_wechat'=>'wechat',
        'stage_alipay'=>'alipay',
        'bank'=>'bank'
    ];

	/**
	 * App 配置接口
	 * @param Request $request
	 * @return array
	 */
	public function config(Request $request){
		$data = [
			// 商家APP
			'seller'		=> [
				'recharge_button'		=> 1,
				'notification'			=> 1,
				
				'recharge_stage_wechat'	=> 1,
				'recharge_stage_alipay'	=> 1,
				'recharge_bank'			=> 1,
				'recharge_amount_min'	=> 1,
				'recharge_amount_max'	=> 10000000,
				
				'pay_stage_wechat'		=> 1,
				'pay_stage_alipay'		=> 1,
				'pay_bank'				=> 1,
				'pay_amount_min'		=> 1,
				'pay_amount_max'		=> 10000000,
                'version_code'  => 10, //版本号
                'update_type' => 1, //1强制更新 0非强制更新
                'down_url' => '' //下载地址
			],
			// 机主APP
			'agent'			=> [
				'pay_stage_wechat'		=> 1,
				'pay_stage_alipay'		=> 1,
				'pay_bank'				=> 1,
				'pay_amount_min'		=> 1,
				'pay_amount_max'		=> 10000000,
			],
		];
		return json_success('OK',$data);
	}
	
	/**
	 * 魔方pos机统一下单接口，支付方式内传
	 * @param Request $request
	 * @throws ApiException
	 * @return array
	 */
    public function seller_unified(Request $request){
        new_logger('yinsheng_seller_unified.log','请求参数',$request->all());
		$terminalId = $request->input('terminalId');
    	$mobile = $request->input('mobile');
		$auth_code = $request->input('auth_code');
    	$amount = floor($request->input('amount'));
    	$pay_type = $request->input('pay_type');	// stage_wechat/stage_alipay/bank

        //魔方1 银盛2
		$type=\request('type') ?? 2;

        if($type==2){
            //银盛
            $notify_type=7;
        }elseif($type==1){
            //联动
            $notify_type=6;
        }

        //下单
        $order_data=(new Order)->create_order($mobile,$terminalId,empty($mobile)?3:1,'',$this->new_pay_type[$pay_type],$amount / 100,$notify_type,\request('lat'),\request('lng'),$pay_type=='bank'?'bank':'stage');
        $order_no=$order_data['order_sn'];
        $user_id=$order_data['buyer_id'];
        $product_name = '订单支付-'.$order_no;

        if($type==2){
            //银盛
            BankSdk::unified($user_id)->orderNo($order_no)
                ->orderType('银盛POS买单')
                ->productName($product_name)
                ->payType('yinsheng_'.$pay_type,$amount)
                ->notifyUrl(url('posPay/bankNotify'))
                ->param('terminalId',$terminalId)->pay();
        }elseif($type==1){
            //联动
            BankSdk::unified($user_id)->orderNo($order_no)
                ->orderType('魔方POS买单')
                ->productName($product_name)
                ->payType('mofang_' . $pay_type, $amount)
                ->notifyUrl(url('posPay/bankNotify'))
                ->param('terminalId', $terminalId)->pay();
        }
		$data = [
			'user_id'		=> $user_id,
			'order_no'		=> $order_no,
			'amount'		=> $amount,
			'product_name'	=> $product_name,
			'product_desc'	=> $product_name,
			'pay_time'		=> date('Y/m/d H:i:s'),//交易时间
		];
		return json_success('OK',$data);
	}
	
	/**
	 * 商家余额充值，只能使用环迅的正扫和反扫
	 * @param Request $request
	 * @return array
	 * @throws ApiException
	 */
	public function seller_recharge(Request $request){
		$terminalId = $request->input('terminalId');
		$amount = floor($request->input('amount'));
		$auth_code = $request->input('auth_code');
		$pay_type = $request->input('pay_type');	// stage_wechat,stage_alipay
		$pay_types = ['stage_wechat','stage_alipay'];
        //魔方1 银盛2
        $type=\request('type') ?? 2;
		if($pay_types=='stage_alipay'){
            throw new ApiException('暂不支持支付宝');
        }
		$pay_types_cn = [
			'stage_wechat'	=> '微信支付',
			'stage_alipay'	=> '支付宝支付',
		];

        //通知类型环迅
//        $notify_type=3;
        //通知类型扫呗
        $notify_type=8;

        //下单
        $order_data=(new Order)->create_recharge_order($terminalId,$this->new_pay_type[$pay_type],$amount / 100,0,$notify_type,'stage');
        $order_no=$order_data['order_sn'];
        $user_id=$order_data['buyer_id'];
        $product_name = '订单支付-'.$order_no;


        //请求反扫支付 (环迅)
//        $result = BankSdk::unified($user_id)
//            ->orderNo($order_no)
//            ->notifyUrl(url('posPay/bankNotify'))
//            ->orderType('POS充值')
//            ->productName('扫码支付')
//            ->param('auth_code',$auth_code)
//            ->param('pay_type',1)
//            ->payType('huanxun_'.$pay_type,$amount)
//            ->pay();

        $result = BankSdk::unified($user_id)->orderNo($order_no)
            ->orderType('扫呗POS买单')
            ->productName($product_name)
            ->payType('saobei_'.$pay_type, $amount)
            ->notifyUrl(url('posPay/bankNotify'))
            ->param('auth_code', $auth_code)
            ->pay();

		$data = [
			'user_id'		=> $user_id,
			'order_no'		=> $order_no,
			'amount'		=> $amount / 100,
			'product_name'	=> $product_name,
			'product_desc'	=> $product_name,
//			'mobile'		=> $mobile,
//			'merchant_name'	=> $rmb_info->merchantName,
			'auth_code'		=> substr($auth_code,-7),
			'pay_type'		=> $pay_types_cn[$pay_type],
			'pay_time'		=> date('Y/m/d H:i:s'),//交易时间
		];
		return json_success('OK',$data);
	}
	
	/**
	 * 商家信息
	 * @param Request $request
	 * @return array
	 */
	public function seller_info(Request $request){
		$terminalId=$request->input('terminalId');
		$data = Pos::where(['pos.terminalId'=>$terminalId])
			->leftJoin('agent as u1','u1.id','=','pos.agent_uid')
			->leftJoin('seller as u2','u2.id','=','pos.uid')
			->leftJoin('agent_operate as u3','u3.id','=','pos.agent_operate_id')
			->leftJoin('ruzhu_merchant_basic as u4','u4.uid','=','u2.id')
			->first(['pos.terminalId',
				'u1.mobile as agent_mobile',
				'u2.mobile as seller_mobile',
				'pos.uid',
				'u4.merchantName as seller_name',
				'u3.mobile as operate_mobile',
			]);
//		$return = [
//			[
//				'name'		=> '所属运营中心',
//				'value'		=> $data->operate_mobile,
//			],[
//				'name'		=> '所属机主',
//				'value'		=> $data->agent_mobile,
//			],[
//				'name'		=> '当前商家',
//				'value'		=> $data->seller_mobile,
//			],[
//				'name'		=> '商家名称',
//				'value'		=> $data->seller_name,
//			]
//		];
		return json_success('OK',$data);
	}
	
	public function seller_info2(Request $request){
		$terminalId=$request->input('terminalId');
		$data = Pos::where(['pos.terminalId'=>$terminalId])
			->leftJoin('agent as u1','u1.id','=','pos.agent_uid')
			->leftJoin('seller as u2','u2.id','=','pos.uid')
			->leftJoin('agent_operate as u3','u3.id','=','pos.agent_operate_id')
			->leftJoin('ruzhu_merchant_basic as u4','u4.uid','=','u2.id')
			->first(['pos.terminalId',
				'u1.mobile as agent_mobile',
				'u2.mobile as seller_mobile',
				'pos.uid',
				'u4.merchantName as seller_name',
				'u3.mobile as operate_mobile',
			]);
		$return = [
			[
				'name'		=> '所属运营中心',
				'value'		=> $data->operate_mobile,
			],[
				'name'		=> '所属机主',
				'value'		=> $data->agent_mobile,
			],[
				'name'		=> '当前商家',
				'value'		=> $data->seller_mobile,
			],[
				'name'		=> '商家名称',
				'value'		=> $data->seller_name,
			]
		];
		return json_success('OK',$return);
	}
}
