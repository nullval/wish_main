<?php

/*
  *  检查远程服务器信息
  *  @author:
  *  @date 2018年3月21日
  */

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Gift;
use App\Models\SysConfig;
use Illuminate\Http\Request;
use App\Models\ShopTSsoSession;
use App\Models\ShopTnetReginfo;
class CheckController extends  Controller {

    /*
     * 获取远程服务器app更新配置信息
     */
    public function upgrade(Request $request) {
        $configs = SysConfig::whereIn('attr_name', ["version_name", "version_code", "download_type", "download_url", "update_desc"])->get(['attr_name', 'attr_value']);
        $data = [];
        foreach ($configs as $key => $val) {
            switch ($val->attr_name) {
                case 'version_name':
                    $data['version_name'] = $val->attr_value;
                    break;
                case 'version_code':
                    $data['version_code'] = $val->attr_value;
                    break;
                case 'download_type':
                    $data['download_type'] = $val->attr_value;
                    break;
                case 'download_url':
                    $data['download_url'] = $val->attr_value;
                    break;
                case 'update_desc':
                    $data['update_desc'] = $val->attr_value;
                    break;
            }
        }
        return json_success('ok', $data);
    }

    /*
     * 检查附近是否存在可领取的红包
     * @param
     */
    public function checkgift(Request $request) {
        $ticket = $request->input('ticket');
        $lat = $request->input('lat');
        $lng = $request->input('lng');
        if (empty($ticket)) {
            return json_error('未能找到您的票据参数');
        }
        //获取当前所在的城市和区域
        if (empty($lat) || empty($lng)) {
            return json_success('ok', ['gift' => '', 'user' => '']);
        }

        $user = $this->getUser($ticket);
        if (empty($user)) {
            return json_error("身份非法", [], 2);
        }
        if(empty($user->id))
            return json_error("身份非法", [], 2);
        $gift = new Gift();
        $step = 0.045;
        //查询附近红包
        $check = $gift->check_gift($user->id, $lat, $lng, $step);
        if (!empty($check)) {
            $model = new ShopTnetReginfo();
            $wxinfo = $model->getWxinfo($user->nodeid);
        } else {
            $check = $user = '';
        }
        return json_success('ok', ['gift' => $check, 'user' => $wxinfo]);
    }

    public function rmcache() {
        $gift = new Gift();
        $gift->rmcache();
        echo 'ok';
    }

    /*
     * 拆开红包
     */
    public function opengift(Request $request) {
        $ticket = $request->input('ticket');
        $giftid = (int)$request->input('gid');
        $lat = $request->input('lat');
        $lng = $request->input('lng');
        if (empty($ticket)) {
            return json_error('未能找到您的票据参数');
        }
        if ($giftid < 1) {
            return json_error('红包参数异常');
        }
        if (empty($lat) || empty($lng)) {
            return json_error("请开启定位权限", [], 2);
        }
        new_logger('open_gift.log', '打开红包请求参数', $request->all());
        $user = $this->getUser($ticket);
        if (empty($user)) {
            return json_error("身份非法", [], 2);
        }
//        $user=json_decode($user,true);
        $gift = new Gift();
        $step = 0.045;
        $result = $gift->open_gift($user->id, $giftid,$lat,$lng,$step);
        new_logger('open_gift.log', '回调红包结果', $result);
        if ($result['status']) {
            unset($result['status']);
            return json_success('恭喜抢到' . $result["money"] . '元', $result);
        }
    }

    public function  recovergift() {
        $gift = new Gift();
        $result = $gift->recovery_redpack();
        if ($result)
            return json_success('success', $result);
        return json_error('fail', $result);
    }

    /*
     * 获取已领取红包记录
     * @param
     */
    public function gifthis(Request $request) {
        $ticket = $request->input('ticket');
        $page = $request->input('page', 1);
        if (empty($ticket)) {
            return json_error('未能找到您的票据参数');
        }
        $user = $this->getUser($ticket);
        if (empty($user)) {
            return json_error("身份非法", [], 2);
        }
//        $user=json_decode($user,true);
        $gift = new Gift();
        $result = $gift->gifthis($user->id, $page);
        return json_success('ok', $result);
    }

    /*
     * 获取账户
     * 返回总金额 冻结金额
     */
    public function getmoney(Request $request) {
        $ticket = $request->input('ticket');
        if (empty($ticket)) {
            return json_error('未能找到您的票据参数');
        }
        $user = $this->getUser($ticket);
        if (empty($user)) {
            return json_error("身份非法", [], 2);
        }
//        $user=json_decode($user,true);
        $model = new ShopTnetReginfo();
        $wxinfo = $model->getWxinfo($user->nodeid);
        unset($model);
        $Bank = new Bank();
        $money = $Bank->userWallet($user->id, 12, 1, ['balance', 'freeze_value']);
        $data = [
            'redpack' => [
                'balance' => sprintf("%.2f", $money->balance - $money->freeze_value),
                'freeze_value' => sprintf("%.2f", $money->freeze_value),
            ],
            'user' => [
                'photo' => $wxinfo['photourl'],
                'name' => $wxinfo['nodename'],
                'id' => $wxinfo['nodecode'],
            ]
        ];
        unset($Bank);
        return json_success('ok', $data);
    }

    /*
     * 通过$ticket参数获取user信息
     */
    private function getUser($ticket = '') {
        $model = new Gift();
        return $model->getUser($ticket);
    }

}