<?php
namespace App\Http\Controllers\Api;
use App\Http\Requests\BasicRequest;
use App\Exceptions\ApiException;
use App\Jobs\handleGhtOrder;
use App\Jobs\handleQrOrder;
use App\Jobs\handleSellerOrder;
use App\Libraries\BankSdk;
use App\Libraries\ght\Wangguan;
use App\Models\Agent;
use App\Models\JushMessage;
use App\Models\Message;
use App\Models\Order;
use App\Models\Pos;
use App\Models\RuzhuMerchantBasic;
use App\Models\RuzhuMerchantBusinesss;
use App\Models\Seller;
use App\Models\SysConfig;
use App\Models\User;
use App\Models\VOrder;
use Illuminate\Support\Facades\DB;

/**
 * 商城接口 tdzd_shop_back 商城接口中转
 * Class ShopController
 * @package App\Http\Controllers\Api
 */
class ShopController extends CommonController {
	
	public function index($action,BasicRequest $request)
	{
		$post = $request->all();
		$param = [
			'_cmd'			=> 'product_'.$action,
		];
		$param = array_merge($post,$param);
		if(empty($param['terminalId'])){
			return arr_post(0,'终端参数错误');
		}
		// 根据终端ID获取ticket
		$agent_id = Pos::where(['terminalId'=>$param['terminalId']])->value('agent_uid');
		$agent = Agent::where(['id'=>$agent_id])->first();
		$agent_mobile = $agent->mobile;
		$agent_introducer = $agent->inviter_id;
		$user = User::where(['mobile'=>$agent_mobile])->first();
		$node_id = $user->nodeid;
		$openid = $user->openid;
		// 机主在商城未注册，手动调用注册接口
		if($node_id === null){
			return arr_post(0,'未获取到商家用户信息');
		}elseif($node_id === 0 || $node_id === '0'){
			$register_param = [
				'_cmd'		=> 'user_register',
				'mobile'	=> $agent_mobile,
				'openid'	=> $openid ?? 'openid_tdzd_initialize',
				'introducer'=> Agent::where(['id'=>$agent_introducer])->value('mobile'),
			];
			$register_data = $this->_post($register_param);
		}

		if($register_data){
			$ticket = $register_data['ticket'];
		}else{
			$login_param = [
				'_cmd'		=> 'user_ticket',
				'node_id'	=> $node_id
			];
			$login_data = $this->_post($login_param);
			$ticket = $login_data['ticket'];
		}

		$param['ticket'] = $ticket;

		$data = $this->_post($param);


		if($action == 'order_create'){
//            if($request->input('terminalId')!='test1'&&$request->input('terminalId')!='1xdl100000021'){
//                return json_error('您的商户资料不全,请尽快完善，以便便于您使用完整的pos通道');
//            }
			$order = new Order();
			new_logger('posShopPay_createPosOrder.log','pos机商城请求订单参数',$request->all());
			new_logger('posShopPay_createPosOrder.log','pos机商城下单后参数',$data);
			
			$order_no = $data['order_no'];
			//版本检测
			$version=$request->input('version');
			$SysConfig=new SysConfig();
			$rs=$SysConfig->check_pos_version($version,1,2);
			if($rs==false){
				return json_error('请在应用商城升级版本');
			}
			
			// 奖励人的手机号
			$mobile=$request->input('remarks');
			// 用户未注册，调用注册接口
			$buyer = User::where(['mobile'=>$mobile])->first();
			$buyer_id = $buyer->nodeid;
			if(empty($buyer_id)){
				$register_param2 = [
					'_cmd'		=> 'user_register',
					'mobile'	=> $mobile,
					'openid'	=> 'openid_tdzd_buyer_initialize',
					'introducer'=> $agent_mobile,
				];
				$this->_post($register_param2);
			}
			
			//刷卡用的终端号
			$terminalId=$request->input('terminalId');
			$pay_type = $request->input('pay_type');
            $amount=json_decode($request->input('pay_type_group'),true)[3];
			$order->create_order_shop($order_no,$mobile,$terminalId,$pay_type,$amount);
		}
		return json_success('OK',$data);
	}
	
	public function _sign($param){
		unset($param['sign']);
		ksort($param);
		$param2 = [];
		foreach($param as $k => $v){
			$param2[] = $k.'='.$v;
		}
		$sign = strtolower(md5(implode('&',$param2).env('api_code')));
		$param['sign'] = $sign;
		return $param;
	}
	
	public function _post($param){
		$url = config('app.env') == 'local' ? 'http://tdzdshopapi.343wew.top/' : 'http://shop-api.3dqxm.com/';
//		$url = config('app.env')  == 'local' ? 'http://shop-api.tdzd.com/' : 'http://shop-api.3dqxm.com/';
		$param2 = [
			'ClientSystem'	=> 'tdzd',
			'ClientVersion'	=> '0',
			'ApiVersion'	=> '1.0',
		];
		$param = array_merge($param,$param2);
		$param = $this->_sign($param);
		
		new_logger('pos_order_queue_shop.log','请求url',[$url]);
		new_logger('pos_order_queue_shop.log','参数',[$param]);
		$result = curl_post($url,$param);
		new_logger('pos_order_queue_shop.log','请求后',[$result]);
		
		$result = json_decode($result,true);
		new_logger('pos_order_queue_shop.log','解码后',[$result]);
		if($result['status'] != 1){
			throw new ApiException($result['info']);
		}
		return $result['data'];
	}
	
	
	
	
	
	
	
	
	public function seller(BasicRequest $request){
		$sid = $request->input('sid');
		$openid = $request->input('openid');
		$mobile_seller = Seller::where(['id'=>$sid])->value('mobile');
		$data = RuzhuMerchantBasic::select(['merchantName'])->where(['corpmanMobile'=>$mobile_seller])->first();
		$user = User::select(['mobile','nodeid'])->where(['openid_baobeibao'=>$openid])->where('openid_baobeibao','!=','')->first();
		$data['mobile'] = $user->mobile;
		if($user->mobile){
			if(empty($user->nodeid)){
				$register_param = [
					'_cmd'		=> 'user_register',
					'mobile'	=> $user->mobile,
					'openid'	=> 'openid_qr_initialize',
					'introducer'=> $mobile_seller,
				];
				$register_data = $this->_post($register_param);
				$user->nodeid = $register_data['node_id'];
			}
			if($register_data){
				$ticket = $register_data['ticket'];
			}else{
				$login_param = [
					'_cmd'		=> 'user_ticket',
					'node_id'	=> $user->nodeid
				];
				$login_data = $this->_post($login_param);
				$ticket = $login_data['ticket'];
			}
			$data['ticket'] = $ticket;
		}
		return json_success('OK',$data);
	}
	
	// 二次绑定发送验证短信
	public function seller_bind_sms(BasicRequest $request){
		$mobile = $request->input('mobile');
		return (new Message())->send_sms($mobile,1);
	}
	
	public function seller_qr(BasicRequest $request){
		$sid = $request->input('sid');
		//0 商家扫码支付 1地推人员购买空二维码扫码支付
        $type = empty($request->input('type'))?0:$request->input('type');
		$fontpath = './seller/qrcode/msyh.ttf';
		$mobile = Seller::where(['id'=>$sid])->value('mobile');
		$data = RuzhuMerchantBasic::where(['corpmanMobile'=>$mobile])->first();
		$data->merchantName = mb_convert_encoding($data->merchantName,'html-entities','utf-8');
        $oauth_url=config('config.tdzd_shop_url').'/applyWechat/apply.html?sid='.$sid.'&openid=xzx';
		// 生成二维码
		$qr_source = 'http://pan.baidu.com/share/qrcode?w=370&h=370&url='.$oauth_url;
		$pos = imagettfbbox(18,0,$fontpath,$data->merchantName);
		$text_width = $pos[2] - $pos[0];
		$image = \Image::make('./seller/qrcode/desk_qr_template.jpg')->insert($qr_source,'top-left','96','248')->text($data->merchantName,(568 - $text_width) / 2,690,function($font) use ($fontpath){
			$font->file($fontpath);
			$font->size(24);
		});
		$save_name = './seller/qrcode/qrcode_dest_'.$sid.'.png';
//		$image->save($save_name);
//		$save_url = config('app.url').substr($save_name,1);
//		return $save_url;
		return $image->response();
	}
	
	public function seller_qr_back(BasicRequest $request){
		$sid = $request->input('sid');
		$fontpath = './seller/qrcode/msyh.ttf';
		$mobile = Seller::where(['id'=>$sid])->value('mobile');
		$data = RuzhuMerchantBasic::where(['corpmanMobile'=>$mobile])->first();
		$data->merchantName = mb_convert_encoding($data->merchantName,'html-entities','utf-8');
		// 生成二维码
		$qr_source = 'http://pan.baidu.com/share/qrcode?w=370&h=370&url=http://server.worldours.com/wechat/ght?sid='.$sid;
		$pos = imagettfbbox(18,0,$fontpath,$data->merchantName);
		$text_width = $pos[2] - $pos[0];
		$image = \Image::make('./seller/qrcode/desk_qr_template_back.jpg')->insert($qr_source,'top-left','96','200')->insert('./seller/qrcode/logo.png','top-left','232','332');
		$save_name = './seller/qrcode/qrcode_dest_'.$sid.'.png';
//		$image->save($save_name);
//		$save_url = config('app.url').substr($save_name,1);
//		return $save_url;
		return $image->response();
	}
	
	public function order_create_qr(BasicRequest $request){
        throw new ApiException('此二维码已失效');
		$seller_id = $request->input('sid');
		$openid = $request->input('openid');
		$amount = $request->input('amount');
		$mobile = $request->input('mobile');
        //0 商家扫码支付 1地推人员购买空二维码扫码支付
        $type = empty($request->input('type'))?0:$request->input('type');
		if(empty($seller_id)){
			throw new ApiException('商家ID参数为空');
		}
		if($amount <= 0){
			throw new ApiException('金额参数错误');
		}elseif($amount>500){
            throw new ApiException('扫码支付金额不能超过500');
        }



		if($mobile && (strlen($mobile) != 11 || substr($mobile,0,1) != '1')){
			throw new ApiException('手机号格式错误');
		}
		$seller = Seller::find($seller_id);
		//通知类型环迅
        $notify_type=3;
		$order = new Order();
		$data = $order->create_qr_order($seller_id,$openid,$amount,$mobile,'wechat',$notify_type);
        $order_sn=$data['order_sn'];
		if($data){
		    //下单 跳转
            $result=BankSdk::unified($data['buyer_id'])
                ->orderNo($order_sn)
                ->notifyUrl(url('posPay/bankNotify'))
                ->orderType('环讯微信支付')
                ->productName('微信支付')
                ->param('good_name','扫码支付')
                ->param('fail_url',config('config.tdzd_shop_url').'applyWechat/applyFail.html')
                ->param('merchant_url',config('config.tdzd_shop_url').'applyWechat/applySuccess.html')
                ->payType('huanxun_online',$amount * 100)
                ->pay();
        }
        return json_success('OK',['content'=>$result]);
	}
	
	public function order_trans(BasicRequest $request){
		$request = request()->except(['s']);
		$wangguan = new Wangguan('','','',config('app.env') != 'online');
		$input_param = '';
		foreach($request as $k => $v){
			$input_param .= '<input type="hidden" name="'.$k.'" value="'.$v.'" />';
		}
		$html = <<<EOT
<meta charset="utf-8" />
<title></title>
<form method="post" action="$wangguan->url" id="form_to_gaohuitong">
$input_param
<input type="submit" value="　" style="opacity:0" />
</form>
<script>document.getElementById('form_to_gaohuitong').submit();</script>
EOT;
		$html = str_replace("\r\n",'',$html);
		$html = str_replace(PHP_EOL,'',$html);
		return $html;
	}
	
	// 高汇通异步回调参数
	public function order_notify_qr(BasicRequest $request){
        $input_post=file_get_contents("php://input");
        new_logger('shop_order_notify_qr.log','扫码支付成功后回调参数',$request->all());
		$order_no = request('order_no');


        //防止通知时间太短造成重复通知
        if(cache_lock('order_notify_qr_'.$order_no,1,30)==true){
            return arr_post(0,'频繁通知');
        }
		
		$app_env = config('app.env');
		$pay_status = request('pay_result');	// 1 支付成功，0 未支付，2 支付失败
		$pay_no = request('pay_no');
		$base64_memo= base64_decode(request('base64_memo'));	// 订单备注（商户手机号），根据此字段做转发。
        $bank_code = request('bank_code'); //支付类型
		
		$wangguan = new Wangguan('','','',$app_env != 'online');
		if(!$wangguan->verifyParam(request()->except(['s']))){
			logger($order_no.'异步回调验证支付签名失败');
			return '';
		}

        if($bank_code=='PUBLICWECHAT'){
            $pay_type=1;
        }elseif($bank_code=='PUBLICALIPAY'){
            $pay_type=2;
        }else{
            $pay_type=1;
        }
        $status=2;
        $acct=$request->input('user_bank_card_no');
        $paytime=time2date();
		
		// 执行自己的回调订单逻辑
//		$order_arr['status']=2;
        $order_info=VOrder::where(['order_sn'=>$order_no])->first();
//        //实付金额
        $amount=$order_info->actual_amount;
        if($pay_status==1&&$order_info->status==1){
            (new JushMessage())->send_order_status($order_no,2,$order_info->amount,$pay_type);
            //狗日的高汇通会在毫秒级别的时候通知做队列处理
            $this->dispatch(new handleGhtOrder($order_no,$pay_type,$input_post,$amount,$status,$acct,$paytime,$bank_code,2));
        }

        echo 'success';
	}
}