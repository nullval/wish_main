<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018/6/20
 * Time: 18:35
 */

namespace App\Http\Controllers\SellerAdmin;


use App\Libraries\gaohuitong\Ruzhu;
use App\Models\BankList;
use App\Models\RuzhuMerchantBank;
use App\Models\RuzhuMerchantBasic;
use App\Models\RuzhuMerchantBusinesss;
use App\Models\SellerPermission;
use App\Models\SellerRole;
use App\Models\SellerTicket;
use App\Models\UserBank;

class MemberController extends CommonController
{
    private $seller;

    public function __construct()
    {
        $this->seller = (new SellerTicket())->checkLogin();
    }

    /**
     * 商家入驻信息
     *
     * @return mixed
     */
    public function apply()
    {
        //获取账户信息
        $Ruzhu = new Ruzhu();
        $bank_list = BankList::get();
        $city_arr = get_ght_attr('city');
        $category_arr = get_ght_attr('category');
        $bank_arr = get_ght_attr('bank');
        //获取商家基本信息
        $basic_info = RuzhuMerchantBasic::where(['uid' => $this->seller->id, 'merchantId' => $this->seller->child_merchant_no])->first();
        //获取商家银行卡信息
        $bank_info = RuzhuMerchantBank::where(['merchantId' => $this->seller->child_merchant_no])->first();
        //获取商家的业务信息
        $busi_info = RuzhuMerchantBusinesss::where(['merchantId' => $this->seller->child_merchant_no])->first();
        //获取超级代付银行卡信息
        $bank = UserBank::where(['uid' => $this->seller->id, 'type' => 2])->first();
        //支付平台业务列表
        $busi_list = $Ruzhu->queryBusi($this->seller->child_merchant_no)['body']['busiList'];
        return $this->json_success('OK', compact('bank_list', 'city_arr', 'category_arr', 'bank_arr', 'seller_info', 'basic_info', 'bank_info', 'busi_info', 'bank', 'busi_list'));
    }

    /**
     * 获取权限列表
     */
    public function permission_list()
    {
        //todo 获取用户信息
        $role = SellerRole::where(['seller_id' => $this->seller->id])->first();
        $permission_list = SellerPermission::select('*')->orderBy('sort', 'DESC')->get();
        //格式化菜单
        $SellerPermission = new SellerPermission();
        //todo 需要修改权限列表返回格式
        $permission_list = $SellerPermission->sort_permission($permission_list);
        return $this->json_success('OK', $permission_list);
    }

}