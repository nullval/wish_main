<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018/6/20
 * Time: 17:18
 */

namespace App\Http\Controllers\SellerAdmin;


use Illuminate\Routing\Controller;

class CommonController extends Controller
{

    /**
     * 成功返回数据
     *
     * @param $msg
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function json_success($msg, $data = array())
    {
        $data = array(
            'code' => 1,
            'message' => $msg,
            'data' => $data,
        );
        return response()->json($data);
    }


    /**
     * 失败返回数据
     *
     * @param $msg
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function json_error($msg, $data = array())
    {
        $data = array(
            'code' => 0,
            'message' => $msg,
            'data' => $data,
        );
        return response()->json($data);
    }


}