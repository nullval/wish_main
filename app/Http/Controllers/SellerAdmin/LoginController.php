<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018/6/20
 * Time: 17:13
 */

namespace App\Http\Controllers\SellerAdmin;

use App\Exceptions\ApiException;
use App\Http\Requests\Seller\LoginRequest;
use App\Models\Message;
use App\Models\Seller;
use App\Models\SellerTicket;
use App\Models\User;
use Illuminate\Support\Facades\Request;

class LoginController extends CommonController
{
    /**
     * 尝试使用微信登录
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws ApiException
     */
    public function wx_login(Request $request)
    {
        if (empty($_COOKIE['openid']) && config('app.env') == 'online') {
            return $this->json_error('请关注公众号');
        }
        if (!empty($_COOKIE['openid'])) {
            $openid = $_COOKIE['openid'];
            $seller_info = Seller::where(['openid' => $openid])->first();
            //2012/12/31号商家特殊处理
            //其财务，店长，收银都可以登录
            $special_arr = ['18028721647', '13407041762', '18420442201', '15815808982', '15916598554'];
            $user_info = User::where(['openid' => $openid])->first();
            if (isset($user_info->id)) {
                $user_mobile = $user_info->mobile;
                if (in_array($user_mobile, $special_arr)) {
                    //找出特殊商家
                    $seller_info = Seller::where(['mobile' => '15927578671'])->first();
                }
            }
            if (isset($seller_info->id)) {
                //返回TICKET但不存储用户信息
                $seller_ticket = (new SellerTicket())->getTicket($seller_info->id);
                if ($seller_ticket === false) {
                    return $this->json_success('OK',['ticket' => $seller_ticket]);
                } else {
                    return $this->json_error('获取票据异常');
                }
            }
            return $this->json_error('您需要登录');
        }
        return $this->json_error('你需要登录');
    }

    /**
     * 绑定手机并登录
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiException
     */
    public function bind_phone(LoginRequest $request)
    {
        $mobile = $request->input('mobile');
        $sms_code = $request->input('sms_code');
        $openid = $request->input('openid');
        $user = new Seller();
        $seller_id = $user->seller_api_login($mobile, $sms_code, $openid);
        $seller_ticket = (new SellerTicket())->getTicket($seller_id);
        return $this->json_success('OK',['ticket' => $seller_ticket]);
    }


    /**
     * todo 退出登录
     */
    public function logout()
    {
        return $this->json_success();
    }


    /**
     * 登录发送验证码
     *
     * @param Request $request
     * @return mixed
     * @throws ApiException
     */
    public function send_sms(Request $request)
    {
        $mobile = $request->input('mobile');
        $type = $request->input('type');
        if ($type == 5) {
            //判断是否是商家
            $seller_info = Seller::where(['mobile' => $mobile])->first();
//            if (empty($seller_info)){
//                return json_error('您不是商家,无法发送短信！');
//            }
        }
        $Message = new Message();
        $return_arr = $Message->send_sms($mobile, $type);
        if($return_arr['code'] == 0){
            throw new ApiException($return_arr['message']);
        }
        return $this->json_success('发送成功');
    }


}