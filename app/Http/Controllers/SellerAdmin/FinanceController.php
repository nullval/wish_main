<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018/6/20
 * Time: 18:36
 */

namespace App\Http\Controllers\SellerAdmin;


use App\Models\Bank;
use App\Models\Seller;
use App\Models\SellerTicket;
use App\Models\UserPurse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FinanceController extends CommonController
{
    private $seller;

    public function __construct()
    {
        $this->seller = (new SellerTicket())->checkLogin();
    }

    /**
     * 我的钱包
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ApiException
     */
    public function index(Request $request)
    {
        $Bank = new Bank();
        //用户信息
        $data['seller_info'] = Seller::where(['seller.id' => $this->seller->id])->first();
        //钱包信息
        //收益钱包
        $data['x_purse_info'] = $Bank->userWallet($this->seller->id, 1, 2);
        //货款钱包
        $data['h_purse_info'] = $Bank->userWallet($this->seller->id, 4, 2);
        //积分钱包
        $data['j_purse_info'] = $Bank->userWallet($this->seller->id, 3, 2);
        //待返还补贴积分
        $data['de_purse_info'] = $Bank->userWallet($this->seller->id, 10001, 2);
        //
        $data['x_purse_info'] = $Bank->userWallet($this->seller->id, 1, 2);
        //商家充值钱包
        $data['b_purse_info'] = $Bank->userWallet($this->seller->id, 7, 2);
        //微信钱包
//        $data['w_purse_info']=$Bank->userWallet($user_id,8,2);
//        $data['purse_id'] = $purse_id;
//        $data['purse_arr'] = $purse_arr;
        return $this->json_success('OK', $data);
    }

    /**
     * 交易列表
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ApiException
     */
    public function transfer_list(Request $request)
    {
        //币种类型
        $type = $request->input('type');
        //获取用户注册值钱包
        $Bank = new Bank();
        $owner_type = 2;
        if ($type != 0) {
            //根据币种类型获取相应的钱包id
            $purse_id = $Bank->userWallet($this->seller->id, $type, $owner_type)->purse_id;
        } else {
            $purse_arr = norm_new_arr(UserPurse::where(['owner_id' => $this->seller->id, 'owner_type' => $owner_type])->get(['purse_id']), 'purse_id');
        }
        //0全部 1收入 2支出
        $status = $request->input('status');
        $sql = '';
        for ($i = 2017; $i <= date('Y'); $i++) {
            $sql .= "select * from transfer_" . $i;
            if ($status == 0) {
                if ($type != 0) {
                    $sql .= " where into_purse_id=" . $purse_id . " or out_purse_id=" . $purse_id . " ";
                } else {
                    $sql .= " where into_purse_id in (" . implode(',', $purse_arr) . ") or out_purse_id in (" . implode(',', $purse_arr) . ") ";
                }
            } elseif ($status == 1) {
                if ($type != 0) {
                    $sql .= " where into_purse_id=" . $purse_id . " ";
                } else {
                    $sql .= " where into_purse_id in (" . implode(',', $purse_arr) . ") ";
                }
            } elseif ($status == 2) {
                if ($type != 0) {
                    $sql .= " where out_purse_id=" . $purse_id . " ";
                } else {
                    $sql .= " where out_purse_id in (" . implode(',', $purse_arr) . ") ";
                }
            }
            $sql .= ' and status=0 ';
            if ($i != date('Y')) {
                $sql = $sql . " union ";
            }
        }
        $transfer = DB::table(DB::raw("(" . $sql . ") as t"))
//            ->whereNotIn('reason',[10070,10064])
            ->orderBy('t.create_time', 'desc')
            ->orderBy('t.transfer_id', 'desc')
            ->paginate(15);
        //字段处理
        foreach ($transfer as $k => $v) {
            if ($v->into_owner_id == $this->seller->id) {
                $transfer[$k]->purse_type_name = get_purse_type_name($v->into_purse_id);
            } else {
                $transfer[$k]->purse_type_name = get_purse_type_name($v->out_purse_id);
            }
        }
        return $this->json_success('OK', $transfer);

    }


}