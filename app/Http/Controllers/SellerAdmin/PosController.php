<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018/6/20
 * Time: 18:35
 */

namespace App\Http\Controllers\SellerAdmin;


use App\Models\Pos;
use App\Models\SellerTicket;

class PosController extends CommonController
{
    private $seller;

    public function __construct()
    {
        $this->seller = (new SellerTicket())->checkLogin();
    }

    /**
     * 签约POS机列表(终端号搜索)
     *
     * @return mixed
     */
    public function pos_list()
    {
        $query = ['uid' => $this->seller->id];
        if (request()->has('terminalId')) {
            $query = array_merge($query, ['terminalId' => request()->input('terminalId')]);
        }
        return $this->json_success('OK', (new Pos())->pos_list($query));
    }


}