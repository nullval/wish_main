<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018/6/21
 * Time: 11:28
 */

namespace App\Http\Controllers\SellerAdmin;


use App\Exceptions\ApiException;
use App\Http\Requests\Seller\RoleAddRequest;
use App\Models\Pos;
use App\Models\SellerPermission;
use App\Models\SellerRole;
use App\Models\SellerTicket;
use Illuminate\Http\Request;

class ConfigController extends CommonController
{
    private $seller;

    public function __construct()
    {
        $this->seller = (new SellerTicket())->checkLogin();
    }

    /**
     * 店员列表
     */
    public function role_list()
    {
        $role_list = SellerRole::where(['seller_id' => $this->seller->id])
            ->orderBy('id', 'desc')
            ->paginate(15);
        return $this->json_success(['role_list' => $role_list]);
    }


    /**
     * 权限列表
     */
    public function permission_list()
    {
        $SellerPermission = new SellerPermission();
        $permission_list = $SellerPermission->sort_permission();
        return $this->json_success(['permission_list' => $permission_list]);
    }


    /**
     * 添加员工
     */
    public function role_add(Request $request)
    {
        //
        $id = $request->input('id');
        if (!empty($id)) {
            $info = SellerRole::detail($id);
        } else {
            //初始化字段
            $info = init_table_field('seller_role');
        }
        //todo 需要修改
        $pos_list = Pos::where(['uid' => $this->seller->id])->get();
        return $this->json_success(['info' => $info, 'pos_list' => $pos_list]);
    }


    /**
     * 添加角色提交
     * @param RoleAddRequest $roleAddRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiException
     */
    public function role_add_submit(RoleAddRequest $roleAddRequest)
    {
        $request_info = $roleAddRequest->all();
        $SellerRole = new SellerRole();
        $return_arr = $SellerRole->add_role($request_info);
        $des = empty($request_info['id']) ? '添加' : '编辑';
        if ($return_arr['code'] == 0) {
            throw new ApiException($return_arr['message']);
        }
        return $this->json_success('员工' . $des . '成功');
    }


    /**
     * 删除角色
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiException
     */
    public function role_del(Request $request)
    {
        $id = $request->input('id');
        $result = SellerRole::where(['id' => $id])->delete();
        if (!$result) {
            throw new ApiException('删除失败');
        }
        return $this->json_error('删除成功');
    }


}