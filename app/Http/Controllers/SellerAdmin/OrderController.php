<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018/6/20
 * Time: 18:34
 */

namespace App\Http\Controllers\SellerAdmin;

use App\Exceptions\ApiException;
use App\Models\SellerTicket;
use App\Models\VOrder;
use Illuminate\Http\Request;

class OrderController extends CommonController
{

    private $seller;

    public function __construct()
    {
        $this->seller = (new SellerTicket())->checkLogin();
    }


    /**
     * 返回指定日期订单数据汇总
     */
    public function order_sum(Request $request)
    {
        $date = empty($request->input('date')) ? strtotime(date('Y-m-d')) : date('Y-m-d', strtotime($request->input('date')));
        //todo 订单支出和商家充值
        $data = [
            'pay' => strval(VOrder::where(['uid' => $this->seller->id])
                ->whereIn('type', [1, 3, 5])
                ->where('v_order.created_at', '>=', $date . " 00:00:00")->where('v_order.created_at', '<=', $date . " 23:59:59")
                ->whereNotIn('v_order.status', [1])
                ->sum('amount')),
            'charge' => strval(VOrder::where(['uid' => $this->seller->id])
                ->whereIn('type', [7])
                ->where('v_order.created_at', '>=', $date . " 00:00:00")->where('v_order.created_at', '<=', $date . " 23:59:59")
                ->whereNotIn('v_order.status', [1])
                ->sum('amount'))
        ];
        dd($data);
        return $this->json_success('OK', $data);
    }

    /**
     * 订单列表页面
     *
     * @return mixed
     */
    public function order_list(Request $request)
    {
        $date = empty($request->input('date')) ? strtotime(date('Y-m-d')) : date('Y-m-d', strtotime($request->input('date')));
        $order_list = VOrder::where(['uid' => $this->seller->id])
            ->whereIn('type', [1, 3, 5, 7])
            ->where('v_order.created_at', '>=', $date . " 00:00:00")->where('v_order.created_at', '<=', $date . " 23:59:59")
            ->whereNotIn('v_order.status', [1])
            ->leftJoin('seller', 'seller.id', '=', 'v_order.uid')
            ->select(['v_order.*', 'seller.mobile'])
            ->orderBy('created_at', 'desc')
            ->paginate(15);
        return $this->json_success('OK', $order_list);
    }


    /**
     * 订单详情
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiException
     */
    public function order_detail(Request $request)
    {
        if (!$request->has('id')) {
            throw new ApiException('缺少订单ID');
        }
        $order_datail = VOrder::where(['uid' => $this->seller->id, 'v_order.id' => $request->input('id')])
            ->leftJoin('seller', 'seller.id', '=', 'v_order.uid')
            ->select(['v_order.*', 'seller.mobile'])
            ->first();
        return $this->json_success('OK', $order_datail);
    }


}