<?php

namespace App\Http\Controllers\Agent;


use App\Models\Agent;
use App\Models\Seller;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PushQrSellerController extends Controller
{
	/**
	 * 商家管理列表
	 * @param Request $request
	 * @return mixed
	 */
    public function seller_list(Request $request){
        $user=session('pushQr_user_info');
        $uid=$user->id;
		//商家手机号
		$mobile=$request->input('mobile');
		$seller_list=Seller::where(function ($query) use ($mobile){
			if(!empty($mobile)){
				$query->where('seller.mobile','like','%'.$mobile.'%');
			}
			return $query;
		})
			->where(['qr_user_id'=>$uid])
			->leftJoin('ruzhu_merchant_basic','seller.id','=','ruzhu_merchant_basic.uid')
			->select(['seller.*','ruzhu_merchant_basic.status as apply_status'])
			->orderBy('id','desc')
			->paginate(15);
        //判断是否运营商
        $layouts=pushQrUser_is_operate()?'operate_app':'pushQr_app';
		return view('agent.pushQrSeller.seller_list',compact('seller_list','layouts'));
	}




	
}
