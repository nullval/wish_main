<?php

namespace App\Http\Controllers\Agent;

use App\Http\Requests\Agent\WithdrawRequset;
use App\Models\Agent;
use App\Models\AgentOperate;
use App\Models\Bank;
use App\Models\PushQrQrcode;
use App\Models\User;
use App\Models\UserBank;
use App\Models\UserWithdraw;
use App\Models\VTransfer;
use App\Models\VUserWithdraw;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OperateController extends Controller
{
	/**
	 * 获取用户id
	 * @param $mobile 用户手机号
	 * @return \Illuminate\Database\Eloquent\Model|null|static
	 */
	public function get_oprate_id($mobile){
		$uid = AgentOperate::where(['mobile'=>$mobile])->first()->id;
		return $uid;
	}

	public function index(){
        //代理身份id
        $user_id=$this->get_oprate_id(session('agent_info')['mobile']);
        $info=AgentOperate::where(['id'=>$user_id])->first();
        $bank_info=UserBank::where(['user_bank.type'=>3,'user_bank.uid'=>$user_id,'isdefault'=>1])->first();
	    //运营中心基本信息
        return view('agent.operate.index',compact('info','bank_info'));
    }

	/**
	 * 运营中心收益页面
	 */
	public function income(Request $request){
        //币种类型
        $type=$request->input('type') ?? 1;
		//获取用户注册值钱包
		$Bank=new Bank();
		//代理身份id
		$user_id=$this->get_oprate_id(session('agent_info')['mobile']);
		//根据币种类型获取相应的钱包id
		$purse_id=$Bank->userWallet($user_id,$type,11)->purse_id;
		//0全部 1收入 2支出
		$status=$request->input('status');
//		//总收入
//		$data['total_into']=VTransfer::where(['into_purse_id'=>$purse_id])->sum('into_amount');
//		//总支出
//		$data['total_out']=VTransfer::where(['out_purse_id'=>$purse_id])->sum('out_amount');
		$transfer=VTransfer::where(function ($query) use ($purse_id,$status){
			if($status==0){
				return $query->where(['into_purse_id'=>$purse_id])->orWhere(['out_purse_id'=>$purse_id]);
			}elseif($status==1){
				return $query->where(['into_purse_id'=>$purse_id]);
			}elseif($status==2){
				return $query->where(['out_purse_id'=>$purse_id]);
			}
		})
            ->orderBy('create_time','desc')
			->orderBy('transfer_id','desc')
			->paginate(15);
        $transfer->each(function ($v,$k) use($purse_id,$transfer){
                if($v->into_purse_id == $purse_id){
                    $transfer[$k]->purse_type_name=get_purse_type_name($v->into_purse_id);
                }else{
                    $transfer[$k]->purse_type_name=get_purse_type_name($v->out_purse_id);
                }
            });
		//用户信息
		$data['agent_info']=AgentOperate::where(['id'=>$user_id])->first();
		//钱包信息
		$data['x_purse_info']=$Bank->userWallet($user_id,1,11);
        $data['cost_purse_info']=$Bank->userWallet($user_id,10,11);
		$data['purse_id']=$purse_id;
		return view('agent.operate.income',['transfer'=>$transfer,'data'=>$data]);
	}

	/**
	 * 提现列表页面
	 * @param Request $request
	 * @return mixed
	 */
	public function withdraw_list(Request $request){

		$uid=$this->get_oprate_id(session('agent_info')['mobile']);
		$status=$request->input('status');
		$purse_type=$request->input('purse_type');
		$user_withdraw=VUserWithdraw::where(function ($query) use ($status,$purse_type){
			if(!empty($status)){
				$query->where(['v_user_withdraw.status'=>$status]);
			}
            if(!empty($purse_type)){
                $query->where(['v_user_withdraw.purse_type'=>$purse_type]);
            }
			return $query;
		})
			->where(['v_user_withdraw.user_id'=>$uid,'member_type'=>5,'purse_type'=>1])
			->leftJoin('agent_operate','agent_operate.id','=','v_user_withdraw.user_id')
			->orderBy('id','desc')
			->select(['v_user_withdraw.*','agent_operate.mobile'])
			->paginate(15);
//		dd($user_withdraw);
		return view('agent.operate.withdraw_list',['user_withdraw'=>$user_withdraw]);
	}


	/**
	 * 提现页面
	 * @return mixed
	 */
	public function withdraw(){
		$uid=$this->get_oprate_id(session('agent_info')['mobile']);
		$bank_info=UserBank::where(['uid'=>session('agent_info')['id'],'type'=>3,'isdefault'=>1])->first();
		$pursetype=\request('pursetype') ?? 1;
		//获取用户钱包信息
		$Bank=new Bank();
		$purse=$Bank->userWallet($uid,$pursetype,11);
		return view('agent.operate.withdraw',compact('purse','bank_info'));

	}


	/**
	 * 提现申请
	 * @param WithdrawRequset $withdrawRequset
	 * @return mixed
	 */
	public function subwithdraw(WithdrawRequset $requset){

		//判断账户的启用状态
        $pursetype=\request('pursetype') ?? 1;
		//本地判断是否还有余额
		$UserWithdraw=new UserWithdraw();
		$uid=$this->get_oprate_id(session('agent_info')['mobile']);
		$UserWithdraw->sub_withdraw($uid,5,$pursetype,\request('amount'));
		return pageBackSuccess('您的申请已受理');
	}

    /**
     * 子运营商列表
     */
	public function child_operate_list(){
        //获取运营商id
        $uid=$this->get_oprate_id(session('agent_info')['mobile']);
        $list=AgentOperate::where(['parent_id'=>$uid])
            ->paginate(15);
        foreach ($list as $k=>$v){
            //只获取二维码地推的商家
            $list[$k]->seller_count=PushQrQrcode::where(['pushQr_qrcode.operate_id'=>$v->id,'pushQr_qrcode.status'=>2])
                ->where(['r1.status'=>3])
                ->leftJoin('ruzhu_merchant_basic as r1','r1.uid','=','pushQr_qrcode.seller_id')
                ->count();
        }
        return view('agent.operate.child_operate_list',compact('list'));
    }
}
