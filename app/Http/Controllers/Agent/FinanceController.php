<?php

namespace App\Http\Controllers\Agent;

use App\Http\Requests\Agent\WithdrawRequset;
use App\Jobs\Withdraw;
use App\Models\Agent;
use App\Models\Bank;
use App\Models\BankList;
use App\Models\Df;
use App\Models\SysConfig;
use App\Models\Transfer;
use App\Models\User;
use App\Models\UserBank;
use App\Models\UserTicket;
use App\Models\UserWithdraw;
use App\Models\VTransfer;
use App\Models\VUserWithdraw;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class FinanceController extends Controller
{
	/**
	 * 查看商家财务页面
	 */
	public function index(Request $request){
		//币种类型
		$type=$request->input('type');
		//获取用户注册值钱包
		$Bank=new Bank();
		//代理身份id
        $user_id=session('agent_info')['id'];
		//假如是积分查看用户钱包
		$mobile=session('agent_info')['mobile'];
		$user_info=User::where(['mobile'=>$mobile])->first();
		//代理用户身份id
		if(!isset($user_info->id)){
			$uid=User::insertGetId(['mobile'=>$mobile,'created_at'=>time2date()]);
		}else{
			$uid=$user_info->id;
		}
		//根据币种类型获取相应的钱包id
		$purse_id=$Bank->userWallet(session('agent_info')['id'],$type,5)->purse_id;
		if($type==3){
			$purse_id=$Bank->userWallet($uid,$type,1)->purse_id;
		}
		//0全部 1收入 2支出
		$status=$request->input('status');
//		//总收入
//		$data['total_into']=VTransfer::where(['into_purse_id'=>$purse_id])->sum('into_amount');
//		//总支出
//		$data['total_out']=VTransfer::where(['out_purse_id'=>$purse_id])->sum('out_amount');
        //不使用视图 用原生检索后合并加快速度
        $sql='';
        for ($i=2017;$i<=date('Y');$i++){
            $sql .= "select * from transfer_".$i;
            if($status==0){
                $sql.=" where into_purse_id=".$purse_id." or out_purse_id=".$purse_id." ";
            }elseif($status==1){
                $sql.=" where into_purse_id=".$purse_id." ";
            }elseif($status==2){
                $sql.=" where out_purse_id=".$purse_id." ";
            }
            $sql.=' and status=0 ';
            if ($i != date('Y')){
                $sql = $sql." union ";
            }
        }

        $transfer=DB::table(DB::raw("(".$sql.") as t"))
            ->orderBy('t.create_time','desc')
            ->orderBy('t.transfer_id','desc')
			->paginate(15);
		//字段处理
		foreach($transfer as $k=>$v){
            if($v->into_owner_id==$user_id){
                $transfer[$k]->member_id=get_owner_mobile($v->out_owner_type,$v->out_owner_id);
            }else{
                $transfer[$k]->member_id=get_owner_mobile($v->into_owner_type,$v->into_owner_id);
            }
		}
		//用户信息
		$data['agent_info']=Agent::where(['id'=>$user_id])->first();
		//钱包信息
		$data['x_purse_info']=$Bank->userWallet($user_id,1,5);
		//消费积分
		$data['cost_purse_info']=$Bank->userWallet($user_id,10,5);
        //兑换积分
        $data['exchange_purse_info']=$Bank->userWallet($user_id,11,5);
		$data['purse_id']=$purse_id;
		return view('agent.finance.finance',['transfer'=>$transfer,'data'=>$data]);
	}


	/**
	 * 提现列表页面
	 * @param Request $request
	 * @return mixed
	 */
	public function withdraw_list(Request $request){

		$uid=session('agent_info')['id'];
        $status=$request->input('status');
        $purse_type=$request->input('purse_type');
		$user_withdraw=VUserWithdraw::where(function ($query) use ($status,$purse_type){
            if(!empty($status)){
                $query->where(['v_user_withdraw.status'=>$status]);
            }
            if(!empty($purse_type)){
                $query->where(['v_user_withdraw.purse_type'=>$purse_type]);
            }
            return $query;
        })
            ->where(['v_user_withdraw.user_id'=>$uid,'member_type'=>3,'purse_type'=>1])
			->leftJoin('agent','agent.id','=','v_user_withdraw.user_id')
			->orderBy('id','desc')
			->select(['v_user_withdraw.*','agent.mobile'])
			->paginate(15);
//		dd($user_withdraw);
		return view('agent.finance.withdraw_list',['user_withdraw'=>$user_withdraw]);
	}

	/**
	 * 提现页面
	 * @return mixed
	 */
	public function withdraw(){
		$uid=session('agent_info')['id'];
		//判断用户是否有绑定银行卡
		$bank_info=UserBank::where(['uid'=>$uid,'type'=>3,'isdefault'=>1])->first();
		if (empty($bank_info)){
			echo "<script>alert('您还没绑定银行卡，请先去绑定银行卡再提现！');parent.location.href='/info/index';</script>";
		}
        $pursetype=\request('pursetype') ?? 1;
		//获取用户钱包信息
		$Bank=new Bank();
		$purse=$Bank->userWallet($uid,$pursetype,5);
		return view('agent.finance.withdraw',compact('purse','bank_info'));

	}

	/**
	 * 得到银行联行号
	 * @param Request $request
	 * @return mixed
	 */
	public function get_bankNo(Request $request){
		$bank_name=$request->input('bank_name');
		$info=BankList::where('bank_name','like','%'.$bank_name.'%')->first(['lbnkNo','bank_name']);
		if(empty($info->lbnkNo)){
			return json_error('找不到相应的银行信息');
		}else{
			return json_success('银行信息获取成功',$info);
		}
	}

	/**
	 * 获取银行代码
	 * @param Request $request
	 * @return mixed
	 */
	public function getbankcode(Request $request){
        $data=$request->all();
        $Df=new Df();
        return $Df->getbankcode($data['bank_name']);
	}

	/**
	 * 提现申请
	 * @param WithdrawRequset $withdrawRequset
	 * @return mixed
	 */
	public function subwithdraw(WithdrawRequset $requset){
        $pursetype=\request('pursetype') ?? 1;
        $UserWithdraw=new UserWithdraw();
        $UserWithdraw->sub_withdraw(session('agent_info')->id,3,$pursetype, \request('amount'));
        return pageBackSuccess('您的申请已受理');

	}
	
	
	
	

}
