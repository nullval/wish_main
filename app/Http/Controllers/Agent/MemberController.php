<?php

namespace App\Http\Controllers\Agent;

use App\Http\Requests\Agent\BankRequest;
use App\Http\Requests\Agent\ChangePwdRequest;
use App\Models\Agent;
use App\Models\AgentInviteFloor;
use App\Models\BankList;
use App\Models\Message;
use App\Models\User;
use App\Models\UserBank;
use App\Models\UserWithdraw;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{
	/**
	 * 机主信息页面
	 * @return mixed
	 */
    public function index(){
		//获取机主信息
		$user=session('agent_info');
        $info=UserBank::where(['user_bank.type'=>3,'user_bank.uid'=>$user->id,'isdefault'=>1])->first();
        $agent_info=Agent::where(['agent.id'=>$user->id])
            ->leftJoin('agent as a1','a1.id','=','agent.inviter_id')
            ->leftJoin('agent_operate as a2','a2.id','=','agent.operate_id')
            ->select(['agent.*','a1.mobile as inviter_mobile','a2.mobile as operate_mobile','a2.remarks as operate_remarks'])
            ->first();
		//获取推荐人信息
		$inviter=Agent::where(['id'=>$user->inviter_id])->first();
		$bank_list=BankList::where('id','<>',0)->get();
		return view('agent.info.index',compact('info','inviter','bank_list','agent_info'));
	}
	
	/**
	 * 修改密码页面
	 * @return mixed
	 */
	public function change_pwd(){
		return view('agent.info.chang_pwd');
	}
	
	/**
	 * 提交修改密码
	 * @param ChangePwdRequest $changePwdRequest
	 * @return mixed
	 */
	public function change_submit(ChangePwdRequest $changePwdRequest){
		//验证并修改密码
		$User=new User();
		$result=$User->agent_change_pwd($changePwdRequest->all());
		return $result['code']==0?back()->withInput($changePwdRequest->all())->withErrors(['error'=>$result['message']]):redirect('/info/change_pwd')->with(['status'=>$result['message']]);

	}

	/**
	 * 推荐人列表
	 * @param Request $request
	 * @return mixed
	 */
	public function inviter_list(Request $request){
		//推荐人手机号
		$mobile=$request->input('mobile');
		//获取机主ID
		$id=session('agent_info')->id;
		//获取推荐人信息
		$inviter_list=Agent::where(function ($query) use ($mobile){
			if(!empty($mobile)){
				$query->where('agent.mobile','like','%'.$mobile.'%');
			}
			return $query;
		})
			->where(['inviter_id'=>$id])
			->orderBy('id','desc')
			->paginate(15);
		return view('agent.info.inviter_list',['inviter_list'=>$inviter_list]);

	}



	public function nine_floor(Request $request){
        //获取机主ID
        $uid=session('agent_info')->id;
	    $floor=empty($request->input('floor'))?1:$request->input('floor');
	    $invite_info=AgentInviteFloor::where(['agent_uid'=>$uid,'floor'=>$floor])->first();
	    if(!isset($invite_info->id)){
	        $list=[];
        }else{
            $inviter_id_arr=explode(',',$invite_info->inviter_id_arr);
            $list=DB::table('agent as u1')
                ->whereIn('u1.id',$inviter_id_arr)
                ->leftJoin('agent as u2','u1.inviter_id','=','u2.id')
                ->select(['u1.*','u2.mobile as inviter_mobile'])
                ->get();
            foreach ($list as $k=>$v){
                $list[$k]->inviter_count=Agent::where(['inviter_id'=>$v->id])->count();
            }
        }
        return view('agent.info.nine_floor',['list'=>$list]);
    }


	/**
	 * 发送绑定银行卡短信验证码
	 */
	public function send_sms(Request $request){
		$mobile=$request->input('mobile');
		$Message=new Message();
        $agent_id=session('agent_info')->id;
        $mobile=Agent::where(['id'=>$agent_id])->first(['mobile'])->mobile;
        return $Message->send_sms($mobile,4);
	}


    public function bind_submit(BankRequest $bankRequest){
        $agent_id=session('agent_info')->id;
        $bank_account=$bankRequest->input('bank_account');
        $account_name=$bankRequest->input('account_name');
        $account_bank=$bankRequest->input('account_bank');
        $bank_code=$bankRequest->input('bank_code');
        $ibankno=$bankRequest->input('ibankno');
        $sms_code=$bankRequest->input('sms_code');
        $identity=$bankRequest->input('identity');
        $phone=$bankRequest->input('phone');
        //验证短信验证码
        $agent=Agent::where(['id'=>$agent_id])->first();
        $Message=new Message();
        $Message->check_sms($agent->mobile,$sms_code,4);
        $UserBank=new UserBank();
        $UserBank->add_or_update_default_bank($agent_id,$account_name,$account_bank,$bank_account,$ibankno,3,$identity,$phone);
        return back()->with(['status'=>'绑定成功！']);


    }
}
