<?php

namespace App\Http\Controllers\Agent;



use App\Exceptions\ApiException;
use App\Exceptions\MobileErrorException;
use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\JavaIncrement;
use App\Models\Order;
use App\Models\OrderQrcode;
use App\Models\User;
use App\Models\VOrder;
use Illuminate\Support\Facades\DB;

class OrderQrcodeController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws MobileErrorException
     * 订单二维码入口
     */
	public function index(){
	    $id=request('id');
	    $qrcode_info=OrderQrcode::where(['id'=>$id])->first();
	    $status=$qrcode_info->status;
	    if(!isset($qrcode_info->id)){
	        throw new MobileErrorException('二维码不存在');
        }
        if($status==3){
            throw new MobileErrorException('该二维码已废用');
        }elseif($status==1){
            //未激活跳转激活页面
            return redirect(url('/orderQrcode/active_qrcode').'?id='.$id);
        }elseif($status==2){
            //激活的跳转到公众号
            return redirect(config('config.tdzd_sapi_url').'oauth/weixin?state=1');
        }
    }

    /**
     * 激活二维码页面
     */
    public function active_qrcode(){
        $id=request('id');
        $qrcode_info=OrderQrcode::where(['id'=>$id])->first();
        $order_info=VOrder::where(['order_sn'=>$qrcode_info->order_sn])->first();
        $platform_amount=$order_info->platform_amount;
        $user_point=get_last_two_num($platform_amount*0.1);
        if($user_point<=0){
            return redirect(config('config.tdzd_sapi_url').'oauth/weixin?state=1');
        }
	    return view('agent.orderQrcode.acitve_qrcode');
    }

    /**
     * 激活二维码提交
     */
    public function active_qrcode_submit(){
//        return view('agent.orderQrcode.show_qrcode');
        $id=request('id');
        $mobile=request('mobile');
        $qrcode_info=OrderQrcode::where(['id'=>$id])->first();
        $status=$qrcode_info->status;
        if(strlen($mobile) != 11 && substr($mobile,0,1) != '1'){
            throw new ApiException('填写人手机号错误');
        }
        if(!isset($qrcode_info->id)){
            throw new ApiException('二维码不存在');
        }
        if($status==3){
            throw new ApiException('该二维码已废用');
        }elseif($status==1){
            //未激活 赠送积分
            $buyer_id=User::firstOrCreate(['mobile'=>$mobile])->id;
            $order_info=VOrder::where(['order_sn'=>$qrcode_info->order_sn])->first();
            $order_sn=$order_info->order_sn;
            $platform_amount=$order_info->platform_amount;
            $User=new User();
            $total_buyer_platform_amount=$User->get_platform_amount($buyer_id);
            $buyer_b_config=config('seller.buyer_b_config');
            foreach ($buyer_b_config as $k=>$v){
                if($total_buyer_platform_amount>=$v['min_amount']&&$total_buyer_platform_amount<$v['max_amount']){
                    $min_point_radio=$v['min_point_radio']*10;
                    $max_point_radio=$v['max_point_radio']*10;
                    $buyer_radio=rand($min_point_radio,$max_point_radio)/1000;
                    break;
                }
            }
            $user_point=get_last_two_num($platform_amount*($buyer_radio));
            if($user_point<=0){
                return redirect(config('config.tdzd_sapi_url').'oauth/weixin?state=1');
            }
            if(!isset($order_info->order_sn)){
                throw new ApiException('订单不存在');
            }else{
                //5平台给用户积分
                if($user_point>0){
                    DB::transaction(function() use ($user_point,$mobile,$order_sn,$buyer_id,$id,$User){
                        $Bank=new Bank();
                        //获取用户积分钱包
                        $buyer_point_purse_id=$Bank->userWallet($buyer_id,3,1)->purse_id;
                        //将剩余的金额沉淀
                        $sys_user_id=User::firstOrCreate([
                            'mobile'=>'13000000000'
                        ],[
                            'remark'=>'系统账号'
                        ])->id;
                        $sys_user_purse_id=$Bank->userWallet($sys_user_id,1,1,['purse_id'])->purse_id;
                        //系统账号给积分
                        $Bank->doApiTransfer($sys_user_purse_id,$buyer_point_purse_id,$user_point,10020,
                            $mobile.'在使用二维码激活关注公众号,获得积分,订单号:'.$order_sn,
                            '消费买单用户获得积分');

                        //修正二维码状态
                        OrderQrcode::where(['id'=>$id])->update([
                            'status'=>2,
                            'buyer_id'=>$buyer_id,
                            'point'=>$user_point
                        ]);

                        //更新订单状态
                        Order::where(['order_sn'=>$order_sn])->update(['buyer_id'=>$buyer_id]);

                        //累计用户消费让利
                        $User->add_platform_amount($buyer_id,$order_sn);
                    });

                    //自动挂单 java通知
                    $JavaIncrement = new JavaIncrement();
                    $JavaIncrement->automatic_guadan($mobile,get_num($user_point),$buyer_id,3);
//                    $JavaIncrement->automatic_guadan($mobile,get_num($user_point),$buyer_id,3,$order_sn);
                    return view('agent.orderQrcode.force',compact('order_sn'));
//                    return redirect('https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzI2MDAzNDU0OQ==&scene=124#wechat_redirect');
//                    return view('agent.orderQrcode.show_qrcode');
//                    return redirect(config('config.tdzd_sapi_url').'oauth/weixin?state=1');
                }
            }
        }elseif($status==2){
            throw new MobileErrorException('该二维码已激活,无法使用');
        }
    }

    public function force(){
        $order_sn='00012201806095731551528476041';
        return view('agent.orderQrcode.force',compact('order_sn'));
    }

    /**
     * 检查获取的CHQ数值 todo JAVA返回的数据有BUG
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function check_get_chq(){

        $JavaIncrement=new JavaIncrement();
        return $JavaIncrement->change_over('api/json/user/order/getOrderInfo.do',['order_no'=>request('order_sn')]);

    }
}
