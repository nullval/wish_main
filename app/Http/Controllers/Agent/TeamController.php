<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Http\Requests\Agent\AddTeamRequest;
use App\Models\PushQrQrcode;
use App\Models\PushQrUser;
use App\Models\Seller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeamController extends CommonController
{

    /**
     * 团队列表
     */
    public function team_list(){
        $user_info=$this->get_pushQr_info();
        //判断是否运营中心
        if($user_info->level==1){
            $where=['pushQr_user.operate_id'=>$user_info->operate_id];
        }else{
            $where=['pushQr_user.inviter_id'=>$user_info->id];
        }
        $list=PushQrUser::where($where)->whereNotIn('pushQr_user.mobile',[$user_info->mobile])
            ->whereNotIn('pushQr_user.level',[1])
            ->leftJoin('pushQr_user as s1','s1.id','=','pushQr_user.inviter_id')
            ->select(['s1.mobile as inviter_mobile','pushQr_user.*'])
            ->orderBy('id','desc')
            ->paginate();
        //判断是否运营商
        $layouts=pushQrUser_is_operate()?'operate_app':'pushQr_app';
        return view('agent.team.team_list',compact('list','layouts'));
    }

    /**
     * 添加团队
     */
    public function team_add(){
        //判断是否运营商
        $layouts=pushQrUser_is_operate()?'operate_app':'pushQr_app';
        return view('agent.team.team_add',compact('layouts'));
    }

    public function team_add_submit(AddTeamRequest $request){
        $user_info=$this->get_pushQr_info();
        $PushQrUser=new PushQrUser();
        $mobile=$request->input('mobile');
        $password=$request->input('password');
        $level=$request->input('level');
        $result=$PushQrUser->add_user($user_info->id,$mobile,$level,$password);
        return $result['code']==0?back()->withInput($request->all())->withErrors(['error'=>$result['message']]):redirect('/team/team_list')->with(['status'=>'添加成功！']);
    }

    /**
     * 地推二维码列表
     */
    public function pushQr_list(Request $request){
        $id=request('id');
        $where=['pushQr_qrcode.qr_user_id'=>$id];
        $list=PushQrQrcode::where($where)->trueQr()
            ->leftJoin('seller as s1','s1.id','=','pushQr_qrcode.seller_id')
            ->leftJoin('pushQr_user as s2','s2.id','=','pushQr_qrcode.qr_user_id')
            ->leftJoin('agent_operate as s3','s3.id','=','pushQr_qrcode.operate_id')
            ->select(['pushQr_qrcode.*','s1.mobile as seller_mobile','s2.mobile as push_mobile','s3.mobile as operate_mobile','s2.level as user_level'])
            ->orderBy('id','desc')
            ->paginate();
        //判断是否运营商
        $layouts=pushQrUser_is_operate()?'operate_app':'pushQr_app';
        $info=PushQrUser::where(['pushQr_user.id'=>$id])
            ->leftJoin('pushQr_user as s1','s1.id','=','pushQr_user.inviter_id')
            ->first(['pushQr_user.*','s1.mobile as inviter_mobile']);
        //判断是否运营商
        $layouts=pushQrUser_is_operate()?'operate_app':'pushQr_app';
        return view('agent.team.pushqr_list',compact('list','layouts','info'));
    }

    /**
     * 商家管理列表
     * @param Request $request
     * @return mixed
     */
    public function seller_list(Request $request){
        $uid=request('id');
        //商家手机号
        $seller_list=Seller::where(['qr_user_id'=>$uid])
            ->leftJoin('ruzhu_merchant_basic','seller.id','=','ruzhu_merchant_basic.uid')
            ->select(['seller.*','ruzhu_merchant_basic.status as apply_status'])
            ->orderBy('id','desc')
            ->paginate(15);
        $info=PushQrUser::where(['pushQr_user.id'=>$uid])
            ->leftJoin('pushQr_user as s1','s1.id','=','pushQr_user.inviter_id')
            ->first(['pushQr_user.*','s1.mobile as inviter_mobile']);
        //判断是否运营商
        $layouts=pushQrUser_is_operate()?'operate_app':'pushQr_app';
        return view('agent.team.seller_list',compact('seller_list','layouts','info'));
    }
}
