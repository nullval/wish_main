<?php

namespace App\Http\Controllers\Agent;

use App\Http\Requests\Agent\WithdrawRequset;
use App\Jobs\Withdraw;
use App\Models\Agent;
use App\Models\Bank;
use App\Models\BankList;
use App\Models\Df;
use App\Models\SysConfig;
use App\Models\Transfer;
use App\Models\User;
use App\Models\UserBank;
use App\Models\UserTicket;
use App\Models\UserWithdraw;
use App\Models\VTransfer;
use App\Models\VUserWithdraw;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PushQrFinanceController extends Controller
{
	/**
	 * 查看商家财务页面
	 */
	public function index(Request $request){
		//币种类型
		$type=$request->input('type');

		//获取用户注册值钱包
		$Bank=new Bank();
		//id
        $user_id=session('pushQr_user_info')['id'];

		//根据币种类型获取相应的钱包id
		$purse_id=$Bank->userWallet($user_id,$type,12)->purse_id;

		//0全部 1收入 2支出
		$status=$request->input('status');
		//总收入
		$data['total_into']=VTransfer::where(['into_purse_id'=>$purse_id])->sum('into_amount');
		//总支出
		$data['total_out']=VTransfer::where(['out_purse_id'=>$purse_id])->sum('out_amount');
		$transfer=VTransfer::where(function ($query) use ($purse_id,$status){
			if($status==0){
				return $query->where(['into_purse_id'=>$purse_id])->orWhere(['out_purse_id'=>$purse_id]);
			}elseif($status==1){
				return $query->where(['into_purse_id'=>$purse_id]);
			}elseif($status==2){
				return $query->where(['out_purse_id'=>$purse_id]);
			}
		})
            ->where(['v_transfer.status'=>0])
            ->orderBy('create_time','desc')
			->orderBy('transfer_id','desc')
			->paginate(15);
		//字段处理
		foreach($transfer as $k=>$v){
            if($v->into_owner_id==$user_id){
                $transfer[$k]->member_id=get_owner_mobile($v->out_owner_type,$v->out_owner_id);
            }else{
                $transfer[$k]->member_id=get_owner_mobile($v->into_owner_type,$v->into_owner_id);
            }
		}
		//钱包信息
		$data['x_purse_info']=$Bank->userWallet($user_id,1,12);
		$data['purse_id']=$purse_id;
		return view('agent.pushQrFinance.finance',compact('transfer','data'));
	}


	/**
	 * 提现列表页面
	 * @param Request $request
	 * @return mixed
	 */
	public function withdraw_list(Request $request){

        $uid=session('pushQr_user_info')['id'];
        $status=$request->input('status');
		$user_withdraw=VUserWithdraw::where(function ($query) use ($status){
            if(!empty($status)){
                $query->where(['v_user_withdraw.status'=>$status]);
            }
            return $query;
        })
            ->where(['v_user_withdraw.user_id'=>$uid,'member_type'=>6,'purse_type'=>1])
			->leftJoin('pushQr_user','pushQr_user.id','=','v_user_withdraw.user_id')
			->orderBy('id','desc')
			->select(['v_user_withdraw.*','pushQr_user.mobile'])
			->paginate(15);
//		dd($user_withdraw);
		return view('agent.pushQrFinance.withdraw_list',['user_withdraw'=>$user_withdraw]);
	}

    /**
     * 提现页面
     * @return mixed
     * @throws \App\Exceptions\ApiException
     */
	public function withdraw(){
		$uid=session('pushQr_user_info')['id'];
		//判断用户是否有绑定银行卡
		$bank_info=UserBank::where(['uid'=>$uid,'type'=>6,'isdefault'=>1])->first();
		if (empty($bank_info)){
		    return redirect('/pushQrUser/index')->withErrors(['error'=>'请先填写银行卡信息']);
//			echo "<script>alert('您还没绑定银行卡，请先去绑定银行卡再提现！');parent.location.href='/pushQrUser/index';</script>";
		}
		//获取用户钱包信息
		$Bank=new Bank();
		$purse=$Bank->userWallet($uid,1,12);
		return view('agent.pushQrFinance.withdraw',compact('purse','bank_info'));

	}

	/**
	 * 得到银行联行号
	 * @param Request $request
	 * @return mixed
	 */
	public function get_bankNo(Request $request){
		$bank_name=$request->input('bank_name');
		$info=BankList::where('bank_name','like','%'.$bank_name.'%')->first(['lbnkNo','bank_name']);
		if(empty($info->lbnkNo)){
			return json_error('找不到相应的银行信息');
		}else{
			return json_success('银行信息获取成功',$info);
		}
	}

	/**
	 * 获取银行代码
	 * @param Request $request
	 * @return mixed
	 */
	public function getbankcode(Request $request){
        $data=$request->all();
        $Df=new Df();
        return $Df->getbankcode($data['bank_name']);
	}

    /**
     * 提现申请
     *
     * @param WithdrawRequset $withdrawRequset
     * @return mixed
     * @throws \App\Exceptions\ApiException
     */
	public function subwithdraw(WithdrawRequset $requset){
		//本地判断是否还有余额
        $UserWithdraw=new UserWithdraw();
        $UserWithdraw->sub_withdraw(session('pushQr_user_info')->id,6,1,\request('amount'));
        return pageBackSuccess('您的申请已受理');
	}
	
	
	
	

}
