<?php

namespace App\Http\Controllers\Agent;


use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderPosDetail;
use App\Models\Pos;
use App\Models\PosOrderDetail;
use App\Models\VOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{

    /**
     * 订单管理列表
     */
    public function order_list(Request $request){
        //订单号
        $order_sn=$request->input('order_sn');
        $mobile=$request->input('mobile');
        $terminalId=$request->input('terminalId');
//        $year=empty($request->input('year'))?date('Y'):$request->input('year');
//        $order_table='order_'.$year;
        $order_list=VOrder::where(function ($query) use ($order_sn,$mobile,$terminalId){
            if(!empty($order_sn)){
                $query->where('v_order.order_sn','like','%'.$order_sn.'%');
            }
            if(!empty($mobile)){
                $query->where('agent.mobile','like','%'.$mobile.'%');
            }
            if(!empty($terminalId)){
                $query->where('v_order.terminalId','like','%'.$terminalId.'%');
            }
            return $query;
        })
            ->leftJoin('agent','agent.id','=','v_order.buyer_id')
            ->whereNotIn('v_order.status',[1])
            ->where(['v_order.agent_id'=>session('agent_info')->id,'v_order.type'=>2])
            ->select(['v_order.*','agent.mobile'])
            ->orderBy('created_at','desc')
            ->paginate(15);
        return view('agent.order.order_list',['order_list'=>$order_list]);
    }

    /**
     * 子订单详情列表
     */
    public function order_detail(Request $request){
        //订单号
        $order_sn=$request->input('order_sn');
        $year=$request->input('year');
        $pos_order_detail_table='pos_order_detail_'.$year;
        //获取子订单列表
        $order_detail_list=DB::table($pos_order_detail_table)
            ->where(['agent_id'=>session('agent_info')->id,'order_sn'=>$order_sn])
            ->orderBy('id','desc')
            ->paginate(15);

        $order_table='order_'.$year;
        //获取订单详情
        $order_info=DB::table($order_table)
            ->where(['order_sn'=>$order_sn])
            ->first();

        //获取机主未绑定商家的未特商城pos机
        $pos_list=Pos::where(['agent_uid'=>session('agent_info')->id,'uid'=>0])
            ->get();
//        var_dump(session('agent_info')->id);exit;
        return view('agent.order.order_detail',
            ['order_detail_list'=>$order_detail_list,
            'order_info'=>$order_info,
            'pos_list'=>$pos_list
            ]);
    }

    /**
     * 子订单绑定终端号页面
     */
    public function order_detail_edit(Request $request){
        //订单号
        $order_sn=$request->input('order_sn');
        $order_detail_list=PosOrderDetail::where(['agent_id'=>session('agent_info')->id,'order_sn'=>$order_sn])
            ->orderBy('id','desc')
            ->paginate(15);

        return view('agent.order.order_detail_edit',['order_detail_list'=>$order_detail_list]);
    }

    /**
     * 给子订单绑定pos机
     */
    public function bind_pos(Request $request){
        $terminalId=$request->input('terminalId');
        $order_sku_sn=$request->input('order_sku_sn');
        $order_sn=$request->input('order_sn');
        $year=$request->input('year');

        $agent_uid=session('agent_info')->id;
        $order_table='order_'.$year;
        $pos_order_detail_table='pos_order_detail_'.$year;
        $order_info=DB::table($order_table)
            ->where(['order_sn'=>$order_sn,'agent_id'=>$agent_uid])
            ->first();
        if(!isset($order_info->id)){
            return json_error('订单异常,无法绑定');
        }
        if($order_info->is_true!=1|$order_info->status!=2){
            return json_error('订单异常,无法绑定');
        }
        $buyer_id=$order_info->buyer_id;
        //绑定
        DB::transaction(function () use($pos_order_detail_table,$terminalId,$buyer_id,$order_sku_sn){
            Pos::where(['terminalId'=>$terminalId])->update(['agent_uid'=>$buyer_id]);
            DB::table($pos_order_detail_table)->where(['order_sku_sn'=>$order_sku_sn])->update(['terminalId'=>$terminalId]);
        });
        return json_error('绑定成功');
    }
}
