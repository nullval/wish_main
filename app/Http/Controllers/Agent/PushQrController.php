<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Http\Requests\Agent\AddOrderRequest;
use App\Http\Requests\Agent\BindPushQrRequest;
use App\Models\PushQrOrder;
use App\Models\PushQrQrcode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PushQrController extends CommonController
{

    /**
     * 地推二维码列表
     */
    public function pushQr_list(){
        $user_info=$this->get_pushQr_info();
        $where=['pushQr_qrcode.qr_user_id'=>$user_info->id];
        $list=PushQrQrcode::where($where)->trueQr()
            ->leftJoin('seller as s1','s1.id','=','pushQr_qrcode.seller_id')
            ->leftJoin('pushQr_user as s2','s2.id','=','pushQr_qrcode.qr_user_id')
            ->leftJoin('agent_operate as s3','s3.id','=','pushQr_qrcode.operate_id')
            ->select(['pushQr_qrcode.*','s1.mobile as seller_mobile','s2.mobile as push_mobile','s3.mobile as operate_mobile','s2.level as user_level'])
            ->orderBy('id','desc')
            ->paginate();
        //判断是否运营商
        $layouts=pushQrUser_is_operate()?'operate_app':'pushQr_app';
        return view('agent.pushQr.pushqr_list',compact('list','layouts'));
    }

    /**
     * 绑定地推二维码
     */
    public function bind_pushqr(){
        $user_info=$this->get_pushQr_info();
        //判断是否运营商
        $layouts=pushQrUser_is_operate()?'operate_app':'pushQr_app';
        return view('agent.pushQr.bind_pushqr',compact('user_info','layouts'));
    }

    public function bind_pushqr_submit(BindPushQrRequest $request){
        $user_info=$this->get_pushQr_info();
        $PushQrQrcode=new PushQrQrcode();
        $push_number=$request->input('push_number');
        $mobile=$request->input('mobile');
        $level=$request->input('level');
        $result=$PushQrQrcode->bind_qrcode($push_number,$user_info->id,$mobile,$level);
        return $result['code']==0?back()->withInput($request->all())->withErrors(['error'=>$result['message']]):redirect('/pushQr/pushQr_list')->with(['status'=>'绑定成功！']);
    }


    /**
     * 地推二维码列表
     */
    public function pushQr_order_list(){
        //获取运营中心用户id
        $user_id=$this->get_oprate_id(session('agent_info')['mobile']);
        $list=PushQrOrder::where(['operate_id'=>$user_id])
            ->orderBy('id','desc')
            ->paginate();
        //判断是否运营商
        $layouts=pushQrUser_is_operate()?'operate_app':'pushQr_app';
        return view('agent.pushQr.pushQr_order_list',compact('list','layouts'));
    }

    public function add_order(){
        //判断是否运营商
        $layouts=pushQrUser_is_operate()?'operate_app':'pushQr_app';
        return view('agent.pushQr.add_order',compact('layouts'));
    }

    public function add_order_submit(AddOrderRequest $request){
        //获取运营中心用户id
        $user_id=$this->get_oprate_id(session('agent_info')['mobile']);
        $PushQrOrder=new PushQrOrder();
        $contact_user=$request->input('contact_user');
        $contact_mobile=$request->input('contact_mobile');
        $contact_address=$request->input('contact_address');
        $count=$request->input('count');
        $id_arr=$request->input('id_arr');
        $result=$PushQrOrder->add_order($contact_user,$contact_mobile,$contact_address,$count,$id_arr,$user_id);
        return $result['code']==0?back()->withInput($request->all())->withErrors(['error'=>$result['message']]):redirect('/pushQr/pushQr_order_list')->with(['status'=>'申请成功！']);

    }

    public function pushQr_order_detail(Request $request){
        $id=$request->input('id');
        $info=PushQrOrder::where(['id'=>$id])->first();
        $info->id_arr_group=explode('|',$info->id_arr);
        //判断是否运营商
        $layouts=pushQrUser_is_operate()?'operate_app':'pushQr_app';
        return view('agent.pushQr.pushQr_order_detail',compact('info','layouts'));
    }

}
