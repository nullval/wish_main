<?php

namespace App\Http\Controllers\Agent;

use App\Http\Requests\Agent\BankRequest;
use App\Http\Requests\Agent\EditPasswordRequest;
use App\Models\BankList;
use App\Models\Message;
use App\Models\PushQrUser;
use App\Models\UserBank;
use Illuminate\Http\Request;


class PushQrUserController extends CommonController
{

    /**
     * 地推人员基本信息
     */
    public function index(){
        //获取地推人员基本信息
        $user=session('pushQr_user_info');
        $bank_info=UserBank::where(['user_bank.type'=>6,'user_bank.uid'=>$user->id,'isdefault'=>1])->first();
        $info=PushQrUser::where(['pushQr_user.id'=>$user->id])
            ->leftJoin('pushQr_user as a1','a1.id','=','pushQr_user.inviter_id')
            ->leftJoin('agent_operate as a2','a2.id','=','pushQr_user.operate_id')
            ->select(['pushQr_user.*','a1.mobile as inviter_mobile','a2.mobile as operate_mobile'])
            ->first();
        $bank_list=BankList::where('id','<>',0)->get();
        $layouts=pushQrUser_is_operate()?'operate_app':'pushQr_app';
        return view('agent.pushQrUser.index',compact('info','bank_info','layouts','bank_list'));
    }

    public function send_sms(){
        $Message=new Message();
        $user=session('pushQr_user_info');
        $mobile=PushQrUser::where(['id'=>$user->id])->value('mobile');
        return $Message->send_sms($mobile,4);
    }

    public function bind_submit(BankRequest $bankRequest){
        $user_id=session('pushQr_user_info')->id;
        $bank_account=$bankRequest->input('bank_account');
        $account_name=$bankRequest->input('account_name');
        $account_bank=$bankRequest->input('account_bank');
        $bank_code=$bankRequest->input('bank_code');
        $ibankno=$bankRequest->input('ibankno');
        $sms_code=$bankRequest->input('sms_code');
        $Message=new Message();
        $Message->check_sms(session('pushQr_user_info')->mobile,$sms_code,4);
        $UserBank=new UserBank();
        $result=$UserBank->add_or_update_default_bank($user_id,$account_name,$account_bank,$bank_account,$ibankno,6);
        return $result['code']==0?back()->withInput($bankRequest->all())->withErrors(['error'=>$result['message']]):back()->with(['status'=>'绑定成功！']);
    }

    /**
     * 得到银行联行号
     * @param Request $request
     * @return mixed
     */
    public function get_bankNo(Request $request){
        $bank_name=$request->input('bank_name');
        $info=BankList::where('bank_name','like','%'.$bank_name.'%')->first(['lbnkNo','bank_name']);
        if(empty($info->lbnkNo)){
            return json_error('找不到相应的银行信息');
        }else{
            return json_success('银行信息获取成功',$info);
        }
    }

    public function edit_passrod(){
        //获取地推人员基本信息
        $layouts=pushQrUser_is_operate()?'operate_app':'pushQr_app';
        return view('agent.pushQrUser.edit_passrod',compact('layouts'));
    }

    /**
     * 修改密码
     * @param $old_password 旧密码
     * @param $new_password 新密码
     * @param $new_password_confirmation 确认密码
     * @return mixed
     */
    public function edit_password_submit(EditPasswordRequest $request){
        $old_password = $request -> input('old_password');
        $new_password = $request -> input('new_password');
        $user_id=session('pushQr_user_info')->id;
        $PushQrUser=new PushQrUser();
        $return_arr=$PushQrUser->edit_password($user_id,$old_password,$new_password);
        return $return_arr['code']==0?back() -> withInput($request->all()) -> withErrors(['error'=>$return_arr['message']]):back() -> with('status',$return_arr['message']);

    }
}
