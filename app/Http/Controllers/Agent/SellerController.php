<?php

namespace App\Http\Controllers\Agent;

use App\Http\Requests\Agent\AddSeller1Request;
use App\Http\Requests\Agent\AddSeller2Request;
use App\Http\Requests\Agent\AddSeller3Request;
use App\Http\Requests\Agent\AddSellerRequest;
use App\Libraries\gaohuitong\Ruzhu;
use App\Models\Agent;
use App\Models\Bank;
use App\Models\BankList;
use App\Models\Message;
use App\Models\RuzhuMerchantBank;
use App\Models\RuzhuMerchantBankCopy;
use App\Models\RuzhuMerchantBasic;
use App\Models\RuzhuMerchantBasicCopy;
use App\Models\RuzhuMerchantBusinesss;
use App\Models\RuzhuMerchantBusinesssCopy;
use App\Models\Seller;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SellerController extends Controller
{
	/**
	 * 商家管理列表
	 * @param Request $request
	 * @return mixed
	 */
    public function seller_list(Request $request){
		$agent_id=Agent::where(['id'=>session('agent_info')->id])->first()->id;
		//商家手机号
		$mobile=$request->input('mobile');
		$seller_list=Seller::where(function ($query) use ($mobile){
			if(!empty($mobile)){
				$query->where('seller.mobile','like','%'.$mobile.'%');
			}
			return $query;
		})
			->where(['inviter_id'=>$agent_id])
			->leftJoin('ruzhu_merchant_basic','seller.id','=','ruzhu_merchant_basic.uid')
			->select(['seller.*','ruzhu_merchant_basic.status as apply_status'])
			->orderBy('id','desc')
			->paginate(15);
		return view('agent.seller.seller_list',['seller_list'=>$seller_list]);
	}

	/**
	 * 商家入驻页面
	 * @return mixed
	 */
	public function seller_add(){

		//银行列表
		$bank_list=BankList::get();
		//城市列表
		$city_arr = get_ght_attr('city');
		//经营类别列表
		$category_arr = get_ght_attr('category');
		//去掉名字中的英文名
		foreach ($category_arr as $value){
			$arr = explode(" ",$value->categoryName);
			$value->categoryName = $arr[1];
		}
		//银行代码列表
		$bank_arr = array();
		$bank_arr1 = get_ght_attr('bank');
		foreach ($bank_arr1 as $item => $value){
			foreach ($bank_list as $k => $v){
				if ($v['bank_name'] == $value->bankName){
					$bank_arr[$item]->bankCode =  $value->bankCode;
					$bank_arr[$item]->bankName =  $value->bankName;
				}
			}
		}
		return view('agent.seller.seller_add',compact('bank_list','city_arr','category_arr','bank_arr'));
	}

    /**
     * 登记入住信息
     * @param AddSellerRequest $addSellerRequest
     * @return $this|\Illuminate\Http\RedirectResponse
     */
	public function seller_add_submit(AddSellerRequest $addSellerRequest){
        //登记商家入驻信息
		DB::transaction(function(){
            $Basic=new RuzhuMerchantBasicCopy();
            $Basic->add_seller(\request()->all(),1,session('agent_info')->mobile);

            $Bank=new Bank();
            //落地则赠送100
            $agent_uid=session('agent_info')->id;
            $sys_purse_id=$Bank->get_sys_purse_id(1);
            $a_p_id=$Bank->userWallet($agent_uid,1,5,['purse_id'])->purse_id;
            $agent_info = Agent::where(['id' => $agent_uid])->first();
            $pos_time=$agent_info->pos_time;
//            if ($pos_time> 0) {
//                $Bank->doApiTransfer($sys_purse_id, $a_p_id, 100, 10110,
//                    session('agent_info')->mobile . '使用pos机,终端号为:' . \request('terminalId') . '落地商家' . \request('merchantName') . '获得收益',
//                    '商家落地机主获得收益');
//                //减少次数
//                Agent::where(['id'=>$agent_uid])->decrement('pos_time',1);
//            }
        });

		return pageBackSuccess('商家入驻成功');
	}


	/**
	 * 得到银行联行号
	 * @param Request $request
	 * @return mixed
	 */
	public function get_bankNo(Request $request){
		$bank_name=$request->input('bank_name');
		$info=BankList::where('bank_name','like','%'.$bank_name.'%')->first(['lbnkNo','bank_name']);
		if(empty($info->lbnkNo)){
			return json_error('找不到相应的银行信息');
		}else{
			return json_success('银行信息获取成功',$info);
		}
	}

	/**
	 * 编辑商家入驻信息页面
	 * @param Request $request
	 * @return mixed
	 */
	public function seller_edit(Request $request){
		$uid=$request->input('uid');
		//银行列表
		$bank_list=BankList::get();
		//城市列表
		$city_arr = get_ght_attr('city');
		//经营类别列表
		$category_arr = get_ght_attr('category');
		//去掉名字中的英文名
		foreach ($category_arr as $value){
			$arr = explode(" ",$value->categoryName);
			$value->categoryName = $arr[1];
		}
		//获取用户信息
		$seller_info=Seller::where(['id'=>$uid])->first();
		//兼容旧数据
		if (in_array($seller_info -> mobile,['15886886868','15919996522','13908489731'])){
			//银行代码列表
			$bank_arr = get_ght_attr('bank');
		}else{
			//银行代码列表
			$bank_arr = array();
			$bank_arr1 = get_ght_attr('bank');
			foreach ($bank_arr1 as $item => $value){
				foreach ($bank_list as $k => $v){
					if ($v['bank_name'] == $value->bankName){
						$bank_arr[$item]->bankCode =  $value->bankCode;
						$bank_arr[$item]->bankName =  $value->bankName;
					}
				}
			}
		}
		//获取商家基本信息
		$basic_info=RuzhuMerchantBasic::where(['uid'=>$uid,'merchantId'=>$seller_info->child_merchant_no])->first();
		if (empty($basic_info)){
			//还没想高汇通申请入驻
			//获取商家基本临时信息
			$basic_info=RuzhuMerchantBasicCopy::where(['uid'=>$uid,'merchantId'=>$seller_info->child_merchant_no])->first();
			//是否显示临时表信息 0、是 1、否
			$status = 0;
		}else{
			//是否显示临时表信息 0、是 1、否
			$status = 1;
		}
		//获取商家银行卡信息
		$bank_info=RuzhuMerchantBank::where(['merchantId'=>$seller_info->child_merchant_no])->first();
		if (empty($bank_info)){
			//还没想高汇通申请入驻
			//获取商家银行卡临时信息
			$bank_info=RuzhuMerchantBankCopy::where(['merchantId'=>$seller_info->child_merchant_no])->first();
		}
		//获取商家的业务信息
		$busi_info=RuzhuMerchantBusinesss::where(['merchantId'=>$seller_info->child_merchant_no])->first();
		if (empty($busi_info)){
			//获取商家的业务临时信息
			$busi_info=RuzhuMerchantBusinesssCopy::where(['merchantId'=>$seller_info->child_merchant_no])->first();
		}



		return view('agent.seller.seller_edit',compact('bank_list','city_arr','category_arr','bank_arr','seller_info','basic_info','bank_info','busi_info','bank','status'));
	}


	/**
	 * 编辑商家入驻临时信息
	 * @param AddSellerRequest $addSellerRequest
	 * @return mixed
	 */
	public function seller_edit_submit(AddSellerRequest $addSellerRequest){
		//更新商家入驻临时信息
		$Basic=new RuzhuMerchantBasicCopy();
		$Basic->add_seller($addSellerRequest->all(),2,session('agent_info')->mobile);
		return pageBackSuccess('');

	}
	
	public function saveData(Request $request){
		$request_info=$request->all();
		session(['mobile' => $request_info['mobile']]);
		session(['merchantName' => $request_info['merchantName']]);
		session(['terminalId' => $request_info['terminalId']]);
		session(['city' => $request_info['city']]);
		session(['province10' => $request_info['province10']]);
		session(['province_code' => $request_info['province_code']]);
		session(['city10' => $request_info['city10']]);
		session(['city_code' => $request_info['city_code']]);
		session(['district10' => $request_info['district10']]);
		session(['district_code' => $request_info['district_code']]);
		session(['merchantAddress' => $request_info['merchantAddress']]);
		session(['category' => $request_info['category']]);
		session(['servicePhone' => $request_info['servicePhone']]);
		session(['corpmanName' => $request_info['corpmanName']]);
		session(['corpmanId' => $request_info['corpmanId']]);
		session(['corpmanMobile' => $request_info['corpmanMobile']]);
		session(['businessLicense' => $request_info['businessLicense']]);
		session(['businessimagebase64' => $request_info['businessimagebase64']]);
		session(['img' => $request_info['img']]);
		session(['bankName' => $request_info['bankName']]);
		session(['bankCode' => $request_info['bankCode']]);
		session(['bankaccountNo' => $request_info['bankaccountNo']]);
		session(['account_bank' => $request_info['account_bank']]);
		session(['ibankno' => $request_info['ibankno']]);
		session(['bankaccountType' => $request_info['bankaccountType']]);
		session(['futureRate' => $request_info['futureRate']]);
		session(['lat' => $request_info['lat']]);
		session(['lng' => $request_info['lng']]);
		return arr_post(1,'存储成功');

	}

	public function delData(Request $request){
		$request->session()->forget('mobile');
		$request->session()->forget('merchantName');
		$request->session()->forget('terminalId');
		$request->session()->forget('city');
		$request->session()->forget('province10');
		$request->session()->forget('province_code');
		$request->session()->forget('city10');
		$request->session()->forget('city_code');
		$request->session()->forget('district10');
		$request->session()->forget('district_code');
		$request->session()->forget('merchantAddress');
		$request->session()->forget('category');
		$request->session()->forget('servicePhone');
		$request->session()->forget('corpmanName');
		$request->session()->forget('corpmanId');
		$request->session()->forget('corpmanMobile');
		$request->session()->forget('businessLicense');
		$request->session()->forget('businessimagebase64');
		$request->session()->forget('img');
		$request->session()->forget('bankName');
		$request->session()->forget('bankCode');
		$request->session()->forget('bankaccountNo');
		$request->session()->forget('account_bank');
		$request->session()->forget('ibankno');
		$request->session()->forget('bankaccountType');
		$request->session()->forget('futureRate');
		$request->session()->forget('lat');
		$request->session()->forget('lng');
		return arr_post(1,'删除数据');

	}



	
}
