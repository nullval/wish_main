<?php

namespace App\Http\Controllers\Agent;

use App\Http\Requests\Agent\AddressRequest;
use App\Http\Requests\Agent\InvoiceInfoRequest;
use App\Models\Order;
use App\Models\TicketAddress;
use App\Models\TicketDetail;
use App\Models\TicketInfo;
use App\Models\TicketOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
	/**
	 * 发票订单列表页面
	 * @return mixed
	 */
    public function invoice_list(){
		$agent_uid=session('agent_info')->id;
		$invoice_list=TicketOrder::where(['ticket_order.agent_uid'=>$agent_uid])
			->leftJoin('ticket_info','ticket_info.id','=','ticket_order.info_id')
			->leftJoin('ticket_detail','ticket_detail.id','=','ticket_order.detail_id')
			->orderBy('id','desc')
			->select(['ticket_order.*','ticket_detail.amount','ticket_info.Invoice_header','ticket_detail.Invoice_nature','ticket_detail.method','ticket_detail.status','ticket_detail.remarks'])
			->paginate(15);
		return view('agent.invoice.invoice_list',['invoice_list'=>$invoice_list]);

	}


	/**
	 * 发票寄送地址列表页面
	 * @return mixed
	 */
	public function address_list(){
		$agent_uid=session('agent_info')->id;
		$address_list=TicketAddress::where(['agent_uid'=>$agent_uid])
			->orderBy('id','desc')
			->paginate(15);
		return view('agent.invoice.address_list',['address_list'=>$address_list]);

	}

	/**
	 * 新增/编辑发票寄送地址
	 * @param Request $request
	 * @return mixed
	 */
	public function address_add(Request $request){
		$id=$request->input('id');
		$address=TicketAddress::where(['id'=>$id])->first();
		return view('agent.invoice.address_add',['address'=>$address]);
	}

	/**
	 * 发票寄送地址数据提交
	 * @param AddressRequest $addressRequest
	 * @return mixed
	 */
	public function address_add_submit(AddressRequest $addressRequest){
		$id=$addressRequest->input('id');
		$agent_id=session('agent_info')->id;
		$arr['recipient_name']=$addressRequest->input('recipient_name');
		$arr['address']=$addressRequest->input('address');
		$arr['postalcode']=$addressRequest->input('postalcode');
		$arr['mobile']=$addressRequest->input('mobile');
		$arr['agent_uid']=$agent_id;

		//判断邮政编码的格式是否正确
		if (strlen($arr['postalcode'])!=6){
			return back()->withInput($addressRequest->all())->withErrors(['error'=>'邮政编码格式不正确！']);
		}
		if ($arr['postalcode']=='/^[1-9]\d{5}$/'){
			return back()->withInput($addressRequest->all())->withErrors(['error'=>'邮政编码格式不正确！']);
		}
		//根据id判断新增或编辑
		if (empty($id)){
			//新增地址
			$result=TicketAddress::insert($arr);
			if ($result==false){
				return back()->withInput($addressRequest->all())->withErrors(['error'=>'新增失败！']);
			}
			return redirect('/invoice/address_list')->with('status','新增成功');

		}
		//编辑地址
		TicketAddress::where(['id'=>$id])->update($arr);
		return redirect('/invoice/address_list')->with('status','修改成功');

	}


	/**
	 * 发票详情
	 * @param Request $request
	 * @return mixed
	 */
	public function invoice_detail(Request $request){
		//发票订单ID
		$id=$request->input('id');
		//发票记录
		$invoice=TicketOrder::where(['id'=>$id])->first();
		//发票详情
		$invoice_detail=TicketDetail::where(['id'=>$invoice['detail_id']])->first();
		//发票信息
		$invoice_info=TicketInfo::where(['id'=>$invoice['info_id']])->first();
;		//归属订单号和年份
		$order_list=substr($invoice_detail['order_list'],0,strlen($invoice_detail['order_list'])-1);
		$arr = explode(",",$order_list);
		//获取订单列表
		$order_list=[];
		foreach ($arr as $v){
			$arr1=explode("_",$v);
			$order_id=$arr1[0];
			$year=$arr1[1];
			$Order=new Order($year);
			$order_list[]=$Order->where(['id'=>$order_id])->first();
		}
		return view('agent.invoice.invoice_detail',compact('invoice_info','order_list','invoice_detail','invoice'));

	}

	/**
	 * 发票信息管理页面
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function invoice_info(){

		$agent_uid=session('agent_info')->id;
		$invoice_info=TicketInfo::where(['agent_uid'=>$agent_uid])->first();
		return view('agent.invoice.invoice_info',compact('invoice_info'));
	}

	/**
	 * 发票信息新增/更新
	 * @param InvoiceInfoRequest $invoiceInfoRequest
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	public function invoice_info_submit(InvoiceInfoRequest $invoiceInfoRequest){
		$data=$invoiceInfoRequest->all();
		$TicketInfo=new TicketInfo();
		$result=$TicketInfo->subinfo($data);
		return $result['code']==0?back()->withInput($invoiceInfoRequest->all())->withErrors(['error'=>$result['message']]):redirect('/invoice/invoice_info')->with('status',$result['message']);

	}

	/**
	 * 索取发票页面
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function invoice_add(Request $request){
		$order_list=$request->input('order_list');
		$amount=$request->input('amount');
		$agent_uid=session('agent_info')->id;
		//判断发票金额的有效性
		if ($amount<=0){
			return back()->withInput($request->all())->withErrors(['error'=>'请选择相应的订单以便生成发票！']);
		}
		//判断归属订单的有效性
		$list=substr($order_list,0,strlen($order_list)-1);
		$arr = explode(",",$list);
		foreach ($arr as $v){
			$arr1=explode("_",$v);
			$order_id=$arr1[0];
			$year=$arr1[1];
			$Order=new Order($year);
			$list=$Order->where(['id'=>$order_id,])->first();
			if($list['status']!=2){
				return back()->withInput($request->all())->withErrors(['error'=>'订单号：'.$list['order_sn'].'不是有效订单，不能用于生成发票！']);
			}
			if($list['is_true']==2){
				return back()->withInput($request->all())->withErrors(['error'=>'订单号：'.$list['order_sn'].'不是有效订单，不能用于生成发票！']);
			}
			if($list['is_make']==1){
				return back()->withInput($request->all())->withErrors(['error'=>'订单号：'.$list['order_sn'].'不能重复生成发票！']);
			}
		}
		//获取发票信息
		$ticket_info=TicketInfo::where(['agent_uid'=>$agent_uid])->first();
		//获取收取地址列表
		$address_list=TicketAddress::where(['agent_uid'=>$agent_uid])->get();
		//整理获得的数据以便展示
		$info['order_list']=$order_list;
		$info['amount']=$amount;
		$info['ticket_info']=$ticket_info;
		$info['address_list']=$address_list;
		return view('agent.invoice.invoice_add',compact('info'));

	}

	/**
	 * 新增发票
	 * @param Request $request
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	public function invoice_add_submit(Request $request){
		$data['order_list']=$request->input('order_list');
		$data['amount']=$request->input('amount');
		$data['address_id']=$request->input('address_id');
		$TicketOrder=new TicketOrder();
		$result=$TicketOrder->createorderr($data);
		return $result['code']==0?back()->withInput($request->all())->withErrors(['error'=>$result['message']]):redirect('/invoice/invoice_list')->with('status',$result['message']);

	}

	/**
	 * 删除地址
	 * @param Request $request
	 * @return mixed
	 * @throws \Exception
	 */
	public function address_del(Request $request){
		$agent_uid=session('agent_info')->id;
		$id=$request->input('id');
		$result=TicketAddress::where(['id'=>$id,'agent_uid'=>$agent_uid])->delete();
		if ($result==false){
			return back()->withInput($request->all())->withErrors(['error'=>'删除地址失败！']);
		}
		return back()->with('status','删除地址成功！');
	}

	public function invoice_get(Request $request){
		$agent_uid=session('agent_info')->id;
		$id=$request->input('id');
		$result=TicketDetail::where(['id'=>$id,'agent_uid'=>$agent_uid])->update(['status'=>3]);
		return redirect('/invoice/invoice_list');

	}

}
