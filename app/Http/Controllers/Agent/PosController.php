<?php

namespace App\Http\Controllers\Agent;

use App\Http\Requests\Agent\PosRequest;
use App\Models\Pos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PosController extends Controller
{
	/**
	 * pos机管理列表
	 * @param Request $request
	 * @return mixed
	 */
	public function pos_list(Request $request){
		//绑定商家手机号
		$seller_mobile=$request->input('seller_mobile');
        $terminalId=$request->input('terminalId');
		$pos_list=Pos::where(function ($query) use ($seller_mobile,$terminalId){
			if(!empty($seller_mobile)){
				$query->where(['u1.mobile'=>$seller_mobile]);
			}
            if(!empty($terminalId)){
                $query->where(['pos.terminalId'=>$terminalId]);
            }
			return $query;
		})
			->where(['agent_uid'=>session('agent_info')->id])
			->leftJoin('seller as u1','u1.id','=','pos.uid')
			->select(['pos.*','u1.mobile as seller_mobile'])
			->orderBy('pos.id','desc')
			->paginate(15);

		return view('agent.pos.pos_list',['pos_list'=>$pos_list]);
	}
	
	/**
	 * 商家绑定页面
	 * @param Request $request
	 * @return mixed
	 */
	public function pos_edit(Request $request){
		//获取绑定POS机的编号
		$posid=$request->get('id');
		//联表查询获取POS机的信息
		$info=Pos::where(['pos.id'=>$posid])
			->leftJoin('seller as u1','u1.id','=','pos.uid')
			->select(['pos.*','u1.mobile as seller_mobile'])
			->first();;
		return view('agent.pos.pos_edit',compact('info'));

	}
	
	public function pos_edit_submit(PosRequest $posRequest){
	    return pageBackError('暂不开放此功能');
		$data=$posRequest->all();
		//判断POS机的是否绑定商家并更改绑定
		$Pos=new Pos();
		$Pos->pos_edit($data);
		return redirect('/pos/pos_list')->with(['status'=>'绑定成功！']);

	}


}
