<?php

namespace App\Http\Controllers\Agent;

use App\Exceptions\MobileErrorException;
use App\Http\Requests\Agent\LoginRequest;
use App\Http\Requests\Agent\PushQrUserLoginRequest;
use App\Models\Agent;
use App\Models\AgentOperate;
use App\Models\Message;
use App\Models\PushQrUser;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Psy\Exception\ErrorException;

class LoginController extends Controller
{
    /**
     * 登录页面
     * @param $member_type 身份类型 1、机主 2、运营中心
     * @return mixed
     */
    public function login(Request $request){
//		$openid=$request->input('openid');
        $openid=$_COOKIE['openid'];
        $member_type=$request->input('member_type');
        session(["member_type"=>$member_type]);
        if(empty($_COOKIE['openid'])&&config('app.env')=='online'){
            throw new MobileErrorException('请关注公众号');
        }
        //测试的要登录
        if(!empty($openid)){
            $agent_info=Agent::where(['openid'=>$openid])
                ->first();
//            var_dump($agent_info);exit;
            if(isset($agent_info->id)){
				if($agent_info->inviter_id>0||($agent_info->inviter_id==0&&$agent_info->is_admin==1)){
					//判断其身份
					if ($member_type == 2){
						//判断是否是运营中心
						$operate_info=AgentOperate::where(['mobile'=>$agent_info->mobile])->first(['id','mobile']);
                        $operate_id=$operate_info->id;
                        $operate_mobile=$operate_info->mobile;
						if ((!empty($operate_id))&&substr($operate_mobile,0,1)!='-'){
							//是
                            //储存地推人员信息
//                            $info=PushQrUser::where(['level'=>1,'mobile'=>$agent_info->mobile])->first();
//                            session(['pushQr_user_info'=>$info]);
							//session存储用户信息
							session(['agent_info'=>$agent_info]);
							return redirect('/operate/index');
						}
//						//否 跳转到地推人员登录后台
                        return view('agent.login.login');
//						return view('agent.login.error');
                    }
                    //session存储用户信息
                    session(['agent_info'=>$agent_info]);
                    return redirect('/info/index');
                }
            }
//            $post['openid']=$openid;
//            echo redirect_by_post(url('agent/login'),$post);
            return view('agent.login.login');
        }
        return view('agent.login.login');
//        echo '请关注未特商城公众号平台后,再进行登录';

    }

    /**
     * 提交登录
     * @return mixed
     */
    public function login_submit(LoginRequest $loginRequest){
        //获取登录数据
        $mobile=$loginRequest->input('mobile');
        $sms_code=$loginRequest->input('sms_code');
        $openid=$loginRequest->input('openid');
		$menber_type=$loginRequest->input('member_type');
		//登录验证
		$Agent =new Agent();
		$Agent->agent_login($mobile,$sms_code,$openid,$menber_type);

//        $post['openid']=$openid;
//        echo redirect_by_post('agent.login.login',$post);
        $url=$menber_type==1?'/info/index':'/operate/index';
        return redirect($url);
    }

    /**
     * 地推人员登录入口
     */
    public function pushQr_login(){
        return view('agent.login.pushQr_login');
    }

    /**
     * 地推人员登录提交
     */
    public function pushQr_login_submit(PushQrUserLoginRequest $request){
        $PushQrUser=new PushQrUser();
        $mobile=$request->input('mobile');
        $password=$request->input('password');
        $openid=isset($_COOKIE['openid'])?$_COOKIE['openid']:0;
        $return_arr=$PushQrUser->login($mobile,$password,$openid);
        return $return_arr['code']==0?back()->withInput($request->all())->withErrors(['login_error'=>$return_arr['message']]):redirect('/pushQr/pushQr_list');

    }


    /**
     * 退出登录
     * @param Request $request
     * @return mixed
     */
    public function logout(Request $request){
        $request->session()->forget('agent_info');
        return redirect('/login');
    }

    /**
     * 发送注册短信验证码
     */
    public function send_sms(Request $request){
        $mobile=$request->input('mobile');
        //$member_type=$request->input('member_type');
        //判断是否是机主
        $agent_info=Agent::where(['mobile'=>$mobile])->first();
        if (empty($agent_info)){
            return json_error('您还未成为机主,无法发送短信！');
        }elseif((empty($agent_info->inviter_id))&&($agent_info->is_admin==0)){
            return json_error('您还未成为机主,无法发送短信！');
        }
        $Message=new Message();
        if(session('member_type')==1){
            $rs=$Message->send_sms($mobile,2);
        }elseif(session('member_type')==2){
            $rs=$Message->send_sms($mobile,14);
        }
        return $rs;

    }


    public function test(){
        echo "	<title>掌财通测试</title>
	<style>
		h1 {
			padding: 30px 0 0;
			font-size: 20px;
			float: left;
			width: 100%;
		}
		.debug-info {
			padding: 15px 0;
		}
		.debug-info h3{
			display: block;
			font-size: 16px;
			padding-left: 25px;
			margin: 0;
			line-height: 24px;
			float: left;
			width: 100%;
		}
		.debug-info p {
			display: block;
			line-height: 24px;
			padding-left: 45px;
			margin: 0;
			font-size: 14px;
			float: left;
			width: 80%;
			word-break: break-all; 
		}
	</style>";
// 注意文件编码格式和代码上下文获取字段值的编码方式采用UTF8

        header("Content-type: text/html; charset=utf-8");

        include app_path().'/Libraries/DF/utils/zct_object.php';

// require './utils/zct_object.php';
        #ms_RSAEncryptByPubTest("注意文件编码格式和代码上下文采用UTF8", './certs/public_key.pem', './certs/private_key.pem');
#return;

// 服务器地址
        $url = "http://117.185.7.185:8899/service/client/trade/pay";

// 机构编号
        $orgNo = "0000000081";
// 机构账户
        $orgUsrNo = "0000000104";
// 交易日期 Ymd
        $trdDt = date("Ymd",time());
// 交易时间戳 YmdHis
        $reqTm = date("YmdHis",time());
// 交易时间点 His
        $reqDm = date("His",time());
// 代付流水号
        $reqNo = "REQ" . $reqTm;

        $data = array(
            "version" => "1.0.0",
            "orgNo" => $orgNo,
            "reqNo" => $reqNo,
            "reqTm" => $reqTm,
            "payload" => "",
            "signature" => ""
        );

        $payload = array(
            "trdDt" => $trdDt,
            "trdDm" => $reqDm,
            "trdTyp" => "00",
            "orgUsrNo" => $orgUsrNo,
            "reqNo" => $reqNo,

            // 交易金额: 分
            "trdTxnAmt" => "1",
            "lbnkNo" => "105596031033",
            "lbnkDesc" => "中国建设银行梅州梅县支行",
            "bnkOacCls" => "1",
            "bnkOac" => "6217003200000769239",
            "bnkAcnm" => "肖泽健",
            // 异步回调地址
            "notifyUrl" => "http://localhost:8080/daifu-client-sample/callback"
        );
        $signature = "";

        $plainReqPayload = json_encode($payload, JSON_UNESCAPED_UNICODE);

        $zct = new \zct_object(app_path()."/Libraries/DF/certs/private_key.pem",app_path()."/Libraries/DF/certs/platform_public_key.pem");

        try {
            $data["payload"] = $zct->rsaEncrypt($plainReqPayload);
            $data["signature"] = $zct->rsaSign($plainReqPayload);

            echo "<h1>"."外发请求信息</h1>";
            echo "<div class='debug-info'><h3>"."明文请求参数: </h3><p>" . $plainReqPayload . "</p></div><br/>";
            echo "<div class='debug-info'><h3>"."密文请求参数: </h3><p>" . $data["payload"] . "</p></div><br/>";
            echo "<div class='debug-info'><h3>"."请求参数签名: </h3><p>" . $data["signature"] . "</p></div><br/>";


            echo "<h1>"."返回信息</h1>";
            $respstring = $zct->doPost($url, $data);
            echo "<div class='debug-info'><h3>"."返回数据报文：</h3><p> " . $respstring;

            $respData = json_decode($respstring, true);
            echo "<div class='debug-info'><h3>"."返回状态信息</h3><p>state=" . $respData["state"] . ", respCode=" . $respData["respCode"] . ", respMessage=" . $respData["respMessage"] . "</p></div><br>";

            // 交易成功、受理、失败
            if($respData["state"] == 'S' || $respData["state"] == 'R' || $respData["state"] == 'E') {
                $respEncPayload = $respData["payload"];
                $respsignature = $respData["signature"];
                echo "<div class='debug-info'><h3>"."返回密文数据: </h3><p>" . $respEncPayload . "</p></div><br>";

                $respPlainPayload = $zct->rsaDecrypt($respEncPayload, './certs/private_key.pem');
                echo "<div class='debug-info'><h3>"."返回明文数据: </h3><p>" . $respPlainPayload . "</p></div><br>";

                echo "<div class='debug-info'><h3>"."验签结果： </h3><p>" . $zct->rsaVerify($respsignature, $respPlainPayload, './certs/platform_public_key.pem') . "</p></div>";
            }
        }
        finally {
            if($zct) {
                $zct->free();
            }
        }

    }
}
