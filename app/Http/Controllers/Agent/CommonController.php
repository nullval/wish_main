<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Models\AgentOperate;
use App\Models\PushQrUser;


class CommonController extends Controller
{


    public function __construct(){

    }

    /**
     * 获取地推表用户信息
     */
    public function get_pushQr_info(){
        if(!empty(session('pushQr_user_info'))){
            $mobile=session('pushQr_user_info')['mobile'];
        }elseif(!empty(session('agent_info'))){
            $mobile=session('agent_info')['mobile'];
        }
        return PushQrUser::where(['mobile'=>$mobile])->first();
    }

    /**
     * 获取用户id
     * @param $mobile 用户手机号
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function get_oprate_id($mobile){
        $uid = AgentOperate::where(['mobile'=>$mobile])->first()->id;
        return $uid;
    }


}
