<?php

namespace App\Http\Controllers\Td;
use App\Http\Controllers\Controller;
use App\Models\TdUser;
use App\Models\VOrder;
use Illuminate\Http\Request;


class OrderController extends Controller
{

    public function order_list(Request $request){
        $user=session('td_user_info');
//        $order_table='order_'.$year;
        $order_list=VOrder::where(['buyer_id'=>$user->id])
            ->whereIn('type',[6])
            ->whereNotIn('v_order.status',[1])
            ->leftJoin('td_give_log','td_give_log.order_sn','=','v_order.order_sn')
            ->select(['v_order.*','td_give_log.amount as td'])
            ->orderBy('created_at','desc')
            ->paginate(15);
        return view('td.user.order.order_list',compact('order_list'));
    }

}
