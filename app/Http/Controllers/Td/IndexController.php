<?php

namespace App\Http\Controllers\Td;
use App\Http\Controllers\Controller;
use App\Http\Requests\Td\LoginRequest;
use App\Models\TdUser;
use Illuminate\Http\Request;


class IndexController extends Controller
{

    public function index(){
        $user_info=TdUser::where(['id'=>session('td_user_info')->id])->first();
        return view('td.user.index.index',compact('user_info'));
    }

    public function edit_user_info_submit(Request $request){
        $arr['blockchain']=$request->input('blockchain');
        TdUser::where(['id'=>session('td_user_info')->id])->update($arr);
        return back()->with('status','修改成功');
    }
}
