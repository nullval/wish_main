<?php

namespace App\Http\Controllers\Td;
use App\Http\Controllers\Controller;
use App\Http\Requests\Td\EditPasswordRequest;
use App\Http\Requests\Td\ForgetRequest;
use App\Http\Requests\Td\LoginRequest;
use App\Models\Message;
use App\Models\TdUser;
use Illuminate\Http\Request;


class LoginController extends Controller
{


    /**
     * 用户登录页面
     */
    public function user_login(Request $request){
        return view('td.user.login.login');
    }


    /**
     * 提交登录
     */
    public function user_login_submit(LoginRequest $request){
        $TdUser=new TdUser();
        $mobile=$request->input('mobile');
        $password=$request->input('password');
        $return_arr=$TdUser->login($mobile,$password);
        return $return_arr['code']==0?back()->withInput($request->all())->withErrors(['login_error'=>$return_arr['message']]):redirect('index/index');
    }

    /**
     * 退出登录
     */
    public function logout(Request $request){
        $request->session()->forget('td_user_info');
        return redirect('/login.html');
    }

    /**
     * 忘記密碼
     * @return mixed
     */
    public function forget_password(){
        return view('td.user.login.forget_password');
    }

    /**
     * 忘记密码提交验证
     * @param $mobile 手机号
     * @param $password 新密码
     * @param $password_confirmation 确认密码
     * @param $sms_code 短信验证码
     * @return mixed
     */
    public function forget_password_submit(ForgetRequest $forgetRequest){
        $mobile = $forgetRequest -> input('mobile');
        $password = $forgetRequest -> input('password');
        $password_confirmation = $forgetRequest -> input('password_confirmation');
        $sms_code = $forgetRequest -> input('sms_code');
        $TdUser=new TdUser();
        $return_arr=$TdUser->set_password($mobile,$password,$password_confirmation,$sms_code);
        return $return_arr['code']==0?back()->withInput($forgetRequest->all())->withErrors(['forget_error'=>$return_arr['message']]):redirect('login.html') -> with('status',$return_arr['message']);

    }
    

    /**
     * td用户忘记密码发送短信接口
     * @param $mobile 手机号
     */
    public function send_sms(Request $request){
        $mobile = $request -> input('mobile');
        $mobile = preg_replace('/^( |\s)*|( |\s)*$/', '', $mobile);
        $Message = new Message();
        return $Message->send_sms($mobile,18);
    }

    /**
     * 修改密码
     * @return mixed
     */
    public function edit_password(){
        return view('td.user.info.edit_password');
    }
    
    /**
     * 修改密码
     * @param $old_password 旧密码
     * @param $new_password 新密码
     * @param $new_password_confirmation 确认密码
     * @return mixed
     */
    public function edit_password_submit(EditPasswordRequest $editPasswordRequest){
        $old_password = $editPasswordRequest -> input('old_password');
        $new_password = $editPasswordRequest -> input('new_password');
        $new_password_confirmation = $editPasswordRequest -> input('new_password_confirmation');
        $TdUser=new TdUser();
        $return_arr=$TdUser->edit_password($old_password,$new_password,$new_password_confirmation);
        return $return_arr['code']==0?back() -> withInput($editPasswordRequest->all()) -> withErrors(['error'=>$return_arr['message']]):back() -> with('status',$return_arr['message']);

    }
}
