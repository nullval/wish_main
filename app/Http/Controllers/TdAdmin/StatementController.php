<?php

namespace App\Http\Controllers\TdAdmin;
use App\Http\Controllers\Controller;
use App\Models\TdGiveUseTdLog;
use App\Models\VOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class StatementController extends Controller
{

    public function statement_list(Request $request){
        $user_info=session('td_user_info');
//        $order_table='order_'.$year;
        $statement_list=TdGiveUseTdLog::where(['user_id'=>$user_info->id])
            ->select(DB::raw("u_date,Sum(amount) AS amount"))
            ->groupBy(DB::raw("u_date"))
            ->orderBy('u_date','desc')
            ->paginate(15);
        return view('td.user.statement.statement_list',compact('statement_list'));
    }

}
