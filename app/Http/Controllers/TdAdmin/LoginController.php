<?php

namespace App\Http\Controllers\TdAdmin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Td\AdminLoginRequest;
use App\Http\Requests\Td\LoginRequest;
use App\Models\TdAdmin;
use App\Models\TdUser;
use Illuminate\Http\Request;


class LoginController extends Controller
{


    /**
     * 用户登录页面
     */
    public function admin_login(Request $request){
        return view('td.admin.login.login');
    }


    /**
     * 提交登录
     */
    public function admin_login_submit(AdminLoginRequest $request){
        $TdAdmin=new TdAdmin();
        $username=$request->input('username');
        $password=$request->input('password');
        $return_arr=$TdAdmin->login($username,$password);
        return $return_arr['code']==0?back()->withInput($request->all())->withErrors(['login_error'=>$return_arr['message']]):redirect('index/index');
    }

    /**
     * 退出登录
     */
    public function logout(Request $request){
        $request->session()->forget('td_admin_info');
        return redirect('/login.html');
    }


}
