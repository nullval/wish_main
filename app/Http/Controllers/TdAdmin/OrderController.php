<?php

namespace App\Http\Controllers\TdAdmin;
use App\Http\Controllers\Controller;
use App\Models\TdUser;
use App\Models\VOrder;
use Illuminate\Http\Request;


class OrderController extends Controller
{

    public function order_list(Request $request){
        $order_sn=$request->input('order_sn');
        $terminalId=$request->input('terminalId');
        $mobile=$request->input('mobile');
//        $order_table='order_'.$year;
        $order_list=VOrder::whereIn('type',[6])
            ->where(function ($query) use ($order_sn,$terminalId,$mobile){
                if(!empty($order_sn)){
                    $query->where('v_order'.'.order_sn','like','%'.$order_sn.'%');
                }
                if(!empty($mobile)){
                    $query->where('td_user.mobile','like','%'.$mobile.'%');
                }
                if(!empty($terminalId)){
                    $query->where('v_order'.'.terminalId','like','%'.$terminalId.'%');
                }
                return $query;
            })
            ->whereNotIn('v_order.status',[1])
            ->leftJoin('td_give_log','td_give_log.order_sn','=','v_order.order_sn')
            ->leftJoin('td_user','td_user.id','=','v_order.buyer_id')
            ->select(['v_order.*','td_give_log.amount as td','td_user.mobile'])
            ->orderBy('created_at','desc')
            ->paginate(15);
        return view('td.admin.order.order_list',compact('order_list'));
    }

}
