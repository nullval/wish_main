<?php

namespace App\Http\Controllers\TdAdmin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Td\AddManagerRequest;
use App\Http\Requests\Td\LoginRequest;
use App\Models\TdPos;
use App\Models\TdUser;
use Illuminate\Http\Request;


class PosController extends Controller
{

    public function pos_list(Request $request){
        $mobile=$request->input('mobile');
        $terminalId=$request->input('terminalId');
        $pos_list=TdPos::orderBy('created_at','desc')
            ->where(function($query) use($mobile,$terminalId){
                if(!empty($mobile)){
                     $query->where(['td_user.mobile'=>$mobile]);
                }
                if(!empty($terminalId)){
                    $query->where(['td_pos.terminalId'=>$terminalId]);
                }
                return $query;
            })
            ->leftJoin('td_user','td_user.id','=','td_pos.admin_id')
            ->orderBy('created_at','desc')
            ->select(['td_pos.*','td_user.mobile'])
            ->paginate();
        return view('td.admin.pos.pos_list',compact('pos_list'));
    }


    public function add_pos(Request $request){
        $id=$request->input('id');
        $info=TdPos::where(['td_pos.id'=>$id])
            ->leftJoin('td_user','td_user.id','=','td_pos.admin_id')
            ->first(['td_pos.*','td_user.mobile']);
        return view('td.admin.pos.add_pos',compact('info'));
    }
    public function edit_pos_submit(Request $request){
        $request_info=$request->all();
        $TdPos=new TdPos();
        $mobile=$request_info['mobile'];
        $id=$request_info['id'];
        $return_arr=$TdPos->add_pos($id,'',$mobile);
        return $return_arr['code']==0?back()->withInput($request->all())->withErrors(['error'=>$return_arr['message']]):redirect('/pos/add_pos?id='.$id)->with(['status'=>'Pos编辑成功']);
    }

    public function add_pos_submit(Request $request){
        $request_info=$request->all();
        $TdPos=new TdPos();
        $mobile=$request_info['mobile'];
        $terminalId=$request_info['terminalId'];
        $return_arr=$TdPos->add_pos(0,$terminalId,$mobile);
        return $return_arr['code']==0?back()->withInput($request->all())->withErrors(['error'=>$return_arr['message']]):redirect('/pos/pos_list')->with(['status'=>'Pos添加成功']);
   }
}
