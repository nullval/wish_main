<?php

namespace App\Http\Controllers\TdAdmin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Td\LoginRequest;
use App\Models\TdUser;
use Illuminate\Http\Request;


class IndexController extends Controller
{

    public function index(Request $request){
        $mobile=$request->input('mobile');
        $user_list=TdUser::orderBy('created_at','desc')
            ->where(function($query) use ($mobile){
                if(!empty($mobile)){
                    return $query->where(['td_user.mobile'=>$mobile]);
                }
            })
            ->paginate();
        return view('td.admin.index.index',compact('user_list'));
    }

}
