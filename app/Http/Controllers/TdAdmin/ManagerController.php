<?php

namespace App\Http\Controllers\TdAdmin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Td\AddManagerRequest;
use App\Http\Requests\Td\LoginRequest;
use App\Models\TdUser;
use Illuminate\Http\Request;


class ManagerController extends Controller
{

    public function manager_list(Request $request){
        $mobile=$request->input('mobile');
        $user_list=TdUser::where(['is_admin'=>1])
            ->where(function($query) use ($mobile){
                if(!empty($mobile)){
                    return $query->where(['td_user.mobile'=>$mobile]);
                }
            })
            ->orderBy('created_at','desc')->paginate();
        return view('td.admin.manager.manager_list',compact('user_list'));
    }

    public function add_manager(){
        return view('td.admin.manager.add_manager');
    }

    public function add_manager_submit(AddManagerRequest $request){
        $request_info=$request->all();
        $TdUser=new TdUser();
        $mobile=$request_info['mobile'];
        $password=$request_info['password'];
        $remarks=$request_info['remarks'];
        $return_arr=$TdUser->add_manager($mobile,$password,$remarks);
        $des='添加';
        return $return_arr['code']==0?back()->withInput($request->all())->withErrors(['error'=>$return_arr['message']]):redirect('/manager/manager_list')->with(['status'=>'管理员'.$des.'成功']);
    }
}
