<?php

namespace App\Http\Controllers\Admin;


use App\Libraries\Df\esyto\esyto;
use App\Models\Agent;
use App\Models\Bank;
use App\Models\Seller;
use App\Models\Transfer;
use App\Models\User;
use App\Models\UserPurse;
use App\Models\UserWithdraw;
use App\Models\VTransfer;
use App\Models\VUserWithdraw;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FinanceController extends CommonController
{

    /**
     * 财务信息列表
     */
    public function index(Request $request){
        //币种类型
        $purse_id=$request->input('purse_id') ?? 1;
        //根据币种类型获取相应的红包id
        $sys_purse=UserPurse::where('purse_id','<',1000)->get();

        //0全部 1收入 2支出
        $status=$request->input('status');
        //不使用视图 用原生检索后合并加快速度
        $sql='';
        for ($i=2017;$i<=date('Y');$i++){
            $sql .= "select * from transfer_".$i;
            if($status==0){
                $sql.=" where into_purse_id=".$purse_id." or out_purse_id=".$purse_id." ";
            }elseif($status==1){
                $sql.=" where into_purse_id=".$purse_id." ";
            }elseif($status==2){
                $sql.=" where out_purse_id=".$purse_id." ";
            }
            if ($i != date('Y')){
                $sql = $sql." union ";
            }
        }

        $transfer=DB::table(DB::raw("(".$sql.") as t"))
            ->orderBy('t.create_time','desc')
            ->orderBy('t.transfer_id','desc')
            ->paginate(15);
        //字段处理
        foreach($transfer as $k=>$v){
            if($v->into_purse_id==$purse_id){
                $transfer[$k]->mobile=get_owner_mobile($v->out_owner_type,$v->out_owner_id);
            }else{
                $transfer[$k]->mobile=get_owner_mobile($v->into_owner_type,$v->into_owner_id);
            }
        }
        //可使用值
        $data['purse_info']=UserPurse::where(['purse_id'=>$purse_id])->first();

        //商户号
        $data['username']=env('MERCHANT');

        return view('admin.finance.index',compact('transfer','data','sys_purse'));
    }

    /**
     * 提现列表
     */
    public function withdraw_list(Request $request){
        $mobile=$request->input('mobile');
        $status=$request->input('status');
        $member_type=$request->input('member_type') ?? 1;
        $purse_type=$request->input('purse_type');
//        $user_withdraw_table='user_withdraw_'.$year;
        if($member_type==1){
            $user_table='user';
        } elseif($member_type==2){
            $user_table='seller';
        } elseif($member_type==3){
            $user_table='agent';
        }elseif($member_type==5){
            $user_table='agent_operate';
        }else{
            $user_table='user';
        }
        $start_time=$request->input('start_time');
        $end_time=$request->input('end_time');
        $where=VUserWithdraw::where(function ($query) use ($mobile,$status,$member_type,$purse_type,$start_time,$end_time){
            if(!empty($mobile)&&$member_type==1){
                $query->where('user.mobile','like','%'.$mobile.'%');
            }
            if(!empty($mobile)&&$member_type==2){
                $query->where('seller.mobile','like','%'.$mobile.'%');
            }
            if(!empty($mobile)&&$member_type==3){
                $query->where('agent.mobile','like','%'.$mobile.'%');
            }
            if(!empty($mobile)&&$member_type==5){
                $query->where('agent_operate.mobile','like','%'.$mobile.'%');
            }
            if(!empty($status)){
                $query->where(['v_user_withdraw.status'=>$status]);
            }
            if(!empty($purse_type)){
                $query->where(['v_user_withdraw.purse_type'=>$purse_type]);
            }
            if(!empty($start_time)||!empty($end_time)){
                $query->whereBetween('v_user_withdraw.created_at',[$start_time." 00:00:00",$end_time." 23:59:59",]);
            }
            return $query;
        })
            ->leftJoin($user_table,$user_table.'.id','=','v_user_withdraw.user_id')
            ->where(['v_user_withdraw.member_type'=>$member_type]);
        $data['total']=$where->sum('v_user_withdraw.actual_amount');
        $user_withdraw=$where
            ->orderBy('v_user_withdraw.created_at','desc')
            ->orderBy('v_user_withdraw.id','desc')
            ->select(['v_user_withdraw.*',$user_table.'.mobile'])
            ->paginate(15);
        //获取环讯钱包
        $df = new esyto(false);
        $data['e_withdraw']= $df->queryBalance()['availAmt']; //单位分
        return view('admin.finance.withdraw_list',['user_withdraw'=>$user_withdraw,'data'=>$data]);
    }

    /**
     * 充值申请详情页面
     */
    public function withdraw_detail(Request $request){
        $info=VUserWithdraw::where(['spare_order_sn'=>$request->input('spare_order_sn')])->first();
        //处理字段
        return view('admin.finance.withdraw_detail',['info'=>$info]);
    }

    /**
     * 提现审核
     */
    public function withdraw_submit(Request $request){
        $request_info=$request->all();
        $year=empty($request->input('year'))?date('Y'):$request->input('year');
        $member_type=$request->input('member_type');
        $UserWithdraw=new UserWithdraw();
//        var_dump($request_info);exit;
        $return_arr=$UserWithdraw->verity_withdraw($request->input('id'),$year,$request->input('status'),$request->input('remark'));
        return $return_arr['code']==0?back()->withInput($request->all())->withErrors(['error'=>$return_arr['message']]):redirect('/finance/withdraw_list?year='.$year.'&member_type='.$member_type)->with('status','审核成功');
    }


    /**
     * 钱包统计
     * @param Request $request
     */
    public function Purse_statistics(Request $request){
//        $VTransfer = new VTransfer();
//        $Purse_statistics = $VTransfer -> get_purse_data(11,6);
       $UserPurse=new UserPurse();
        $Purse_statistics=$UserPurse->getPurseStatistics();
        return view('admin.finance.Purse_statistics',compact('Purse_statistics'));

    }

    /**
     * 转账统计表
     * @param $form 起始时间 格式为YYYY-XX-RR
     * @param $to 截止时间 格式为YYYY-XX-RR
     * @return mixed
     */
    public function Transfer_statistics(Request $request){
        $request_info = $request->all();
        $form = $request_info['from'];
        $to = $request_info['to'];
        $VTransfer = new VTransfer();
        $statistics = $VTransfer -> get_statistics_data($form,$to);
        return view('admin.finance.Transfer_statistics',compact('statistics'));

    }

    /**
     * 转账统计详情
     * @param $form 起始时间 格式为YYYY-XX-RR
     * @param $to 截止时间 格式为YYYY-XX-RR
     * @param $reason_id 转账代码id
     * @return mixed
     */
    public function Transfer_detail(Request $request){
        $request_info = $request->all();
        $form = $request_info['from'];
        $to = $request_info['to'];
        $reason_id  = $request_info['id'];
        //获取转账信息编码
        $reason_code = DB::table('transfer_reason') -> where(['reason_id' => $reason_id]) -> first() -> reason_code;
        //获取转账信息名称
        $reason_name= DB::table('transfer_reason') -> where(['reason_id' => $reason_id]) -> first() -> reason_name;
        //获取详情列表
        $VTransfer = new VTransfer();
        $Transfer_detail = $VTransfer -> Transfer_detail($reason_code,$form,$to);
        return view('admin.finance.Transfer_detail',compact('Transfer_detail','reason_name','reason_code'));

    }

}
