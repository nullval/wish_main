<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\ApiException;
use App\Http\Requests\Admin\AddGenerlizeRequest;
use App\Http\Requests\Admin\AddSellerRequest;
use App\Http\Requests\Admin\EditGenerlizeRequest;
use App\Http\Requests\Admin\EditSeller1Request;
use App\Models\AdminRole;
use App\Models\BankList;
use App\Models\TnetCouponTransfer;
use App\Models\TnetReginfo;
use App\Models\UserBank;
use App\Models\Wish\WishGeneralize;
use App\Models\Wish\WishGeneralizeBank;
use App\Models\Wish\WishGeneralizeInfo;
use App\Models\Wish\WishSeller;
use App\Models\Wish\WishSellerInfo;
use App\Services\AdminIdentityDispose;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WishGeneralizeController extends CommonController
{

    /**
     * 直推人管理列表 
     */
   public function generalize_list(Request $request){
       $user=session('admin_info');
       $adminIdentity = new AdminIdentityDispose();
       $identity = $adminIdentity->checkMobileIsIdentity($user['username'],AdminIdentityDispose::IDENTITY_AGENT);
       if(!$identity){
           return pageBackSuccess('当前登录用户不是代理');
       }

       //直推人手机号
       $mobile=$request->input('mobile');
       $name=$request->input('name');
       $agent_mobile=$request->input('agent_mobile');
       $user=session('admin_info');
       $generalize_list=WishGeneralize::where(function ($query) use ($mobile,$name,$agent_mobile,$identity,$user){
           if(!empty($mobile)){
               $query->where('wish_generalize.mobile','like','%'.$mobile.'%');
           }
           if(!empty($name)){
               $query->where('a1.merchantName','like','%'.$name.'%');
           }
           if(!empty($agent_mobile)){
               $query->where('a2.mobile','like','%'.$agent_mobile.'%');
           }
           //筛选当前代理下的直推人
           if($user['role_id'] != AdminRole::ROLE_CORPORATION && $identity == AdminIdentityDispose::IDENTITY_AGENT){
               $adminIdentity = new AdminIdentityDispose();
               $generalizeInfo = $adminIdentity->getInfoByIdentityAndPhone($user['username'],$identity);
               $query->where('a2.id',$generalizeInfo['id']);
           }
           return $query;
       })
           ->leftJoin('wish_generalize_info as a1','wish_generalize.id','=','a1.uid')
           ->leftJoin('wish_agent as a2','wish_generalize.inviter_id','=','a2.id')
           ->where('wish_generalize.mobile','!=',$user['username'])
           ->select(['wish_generalize.*',
               'a1.corpmanName',
               'a1.status as apply_status',
               'a1.merchantName'
               ,'a1.id as basic_id',
               'a2.mobile as agent_mobile',
           ])
           ->orderBy('id','desc')
           ->paginate(15);
       return view('admin.wish_generalize.generalize_list',['generalize_list'=>$generalize_list]);
   }

    /**
     * 冻结/解冻商户
     */
    public function change_status(Request $request){

        $request_info=$request->all();
        WishGeneralize::where(['id'=>$request_info['id']])->update(['status'=>$request_info['status']]);
        return arr_post(1,'状态改变成功');
    }

    /**
     * 直推人入驻页面
     * @return mixed
     */
    public function generalize_add(){
        //银行列表
        $bank_list=BankList::get();
        //城市列表
        $city_arr = get_ght_attr('city');
        //经营类别列表
        $category_arr = get_ght_attr('category');
        //去掉名字中的英文名
        foreach ($category_arr as $value){
            $arr = explode(" ",$value->categoryName);
            $value->categoryName = $arr[1];
        }
        //银行代码列表
        $bank_arr = array();
        $bank_arr1 = get_ght_attr('bank');
        foreach ($bank_arr1 as $item => $value){
            foreach ($bank_list as $k => $v){
                if ($v['bank_name'] == $value->bankName){
                    $bank_arr[$item]->bankCode =  $value->bankCode;
                    $bank_arr[$item]->bankName =  $value->bankName;
                }
            }
        }

        //检查当前用户是否是直推人
        $user=session('admin_info');
        $adminIdentity = new AdminIdentityDispose();
        $identity = $adminIdentity->checkMobileIsIdentity($user['username'],AdminIdentityDispose::IDENTITY_AGENT,1);

        $supeerior_mobile = '';
        if($identity){
            $supeerior_mobile = $user['username'];
        }

        return view('admin.wish_generalize.generalize_add',compact('bank_list','city_arr','category_arr','bank_arr','supeerior_mobile'));
    }

    /**
     * 新增直推人提交
     * @param AddSellerRequest $addSellerRequest
     * @return \Illuminate\Http\RedirectResponse
     * @throws ApiException
     */
    public function generalize_add_submit(AddGenerlizeRequest $addSellerRequest){
        $wishSellerInfo = new WishGeneralizeInfo();
        $requestInfo = $addSellerRequest->all();
        $wishSellerInfo->add_generalize($requestInfo,1,$requestInfo['invite_mobile']);
        return pageBackSuccess('直推人入驻成功');
    }

    /**
     * 编辑直推人入驻信息页面
     * @param Request $request
     * @return mixed
     */
    public function generalize_edit(Request $request){
        $uid=$request->input('uid');
        $bank_list=BankList::get();
        $city_arr = get_ght_attr('city');
        $category_arr = get_ght_attr('category');
        $bank_arr = get_ght_attr('bank');
        //获取用户信息
        $generalize_info=WishGeneralize::where(['id'=>$uid])->first();

        //获取直推人基本信息
        $basic_info=WishGeneralizeInfo::where(['uid'=>$uid])->first();
        //获取直推人银行卡信息
        $bank_info=WishGeneralizeBank::where(['merchantId'=>$generalize_info->child_merchant_no])->first();

        return view('admin.wish_generalize.generalize_edit',compact('bank_list','city_arr','category_arr','bank_arr','generalize_info','basic_info','bank_info','bank'));
    }

    /**编辑直推人基本信息
     * @param EditSeller1Request $editSeller1Request
     * @return \Illuminate\Http\RedirectResponse
     * @throws ApiException
     */
    public function generalize_edit_submit(EditGenerlizeRequest $editSeller1Request){
        $wishSellerInfo = new WishGeneralizeInfo();
        $requestInfo = $editSeller1Request->all();
        $wishSellerInfo->add_generalize($requestInfo,2);
        return pageBackSuccess('修改基本信息成功');
    }

    /**
     * 提现银行页面
     * @return mixed
     */
    public function generalize_withdraw_bank(){
        $bank_list=BankList::get();
        $uid=$_GET['uid'];
        $bank_info=UserBank::where(['uid'=>$uid,'type'=>2,'isdefault'=>1])->first();
        return view('admin.generalize.generalize_withdraw_bank',compact('bank_list','bank_info'));
    }


    /**
     * 商家列表
     */
    public function seller_list(Request $request){
        $user=session('admin_info');
        $adminIdentity = new AdminIdentityDispose();
        $identity = $adminIdentity->checkMobileIsIdentity($user['username'],AdminIdentityDispose::IDENTITY_AGENT);
        if(!$identity){
            return pageBackSuccess('当前登录用户不是代理');
        }

        $agentInfo = $adminIdentity->getInfoByIdentityAndPhone($user['username'],$identity);

        //商家手机号
        $mobile=$request->input('mobile');
        $agent_mobile=$request->input('agent_mobile');
        $seller_list = WishSeller::with('wish_generalize')->with('seller_info')->whereIn('inviter_id',function ($query) use ($agentInfo,$agent_mobile){
            $query->select('id')
                ->from('wish_generalize')
                ->where('inviter_id',$agentInfo['id']);
            if($agent_mobile){
                $query->where('mobile',$agent_mobile);
            }
        })->where(function ($query) use ($mobile){
            if(!empty($mobile)){
                $query->where('wish_seller.mobile','like','%'.$mobile.'%');
            }
            return $query;
        })
            ->orderBy('id','desc')
            ->paginate(15);
        return view('admin.wish_generalize.seller_list',['seller_list'=>$seller_list]);
    }

    /**
     * 商家详情
     */
    public function seller_detail(Request $request){
        $sellerId = $request->input('uid');
        //获取商家信息
        $seller_info = WishSellerInfo::where('uid',$sellerId)->first();
        //找到分类信息
        $category_arr = get_ght_attr('category');
        $category_text = '';
        foreach ($category_arr as $arr){
            if($arr->categoryCode == $seller_info['category']){
                $category_text = $arr->categoryName;
                break;
            }
        }
        //组装地址信息
        $districtInfo = DB::table('tnet_area')->where('area_code',$seller_info['area_id'])->first();
        $cityInfo = DB::table('tnet_area')->where('area_id',$districtInfo->parent_id)->first();
        $provinceInfo = DB::table('tnet_area')->where('area_id',$cityInfo->parent_id)->first();
        $address = $provinceInfo->area_name.' '.$cityInfo->area_name.' '.$districtInfo->area_name;

        return view('admin.wish_generalize.seller_detail',compact('seller_info','category_text','address'));
    }

    /**
     * 商家消费记录
     */
    public function seller_consume(Request $request){
        $sellerId = $request->input('uid');
        //找到nodeid
        $sellerInfo = WishSeller::where('id',$sellerId)->first();
        $reginfo = TnetReginfo::where('nodecode',$sellerInfo['mobile'])->first();
        $nodeid = $reginfo['nodeid'];

        $type = $request->input('type', 0);
        $purseType = $request->input('purse_type', 0);
        $page = $request->input('page', 1);

        //获取流水管理页面数据
        $data['_cmd'] = 'OfflineShopJoin_wishFinancePage';
        $data['node_id'] = $nodeid;
        $data['type'] = $type; //类型全部0,支出1,收入2
        $data['purse_type'] = $purseType; //0全部,1现金,3代金券,4推荐奖励
        $data['need'] = 'can_use_num,voucher_balance,transfer_list'; //需要什么数据：can_use_num(钱包)voucher_balance(推广奖励记录)transfer_list(商家流水)用逗号隔开

        $data['page'] = $page++;
        $postData = get_offline_sign_data($data);
        $res = json_decode(curl_post(env('SHOP_DOMAIN'), $postData), true);
        $getData = $res['data'];

        //钱包余额
        $money = empty($getData['voucher_balance']['cash']['balance']) ? 0 : $getData['voucher_balance']['cash']['balance'];
        //推广券余额
        $voucher = empty($getData['can_use_num']) ? 0 : $getData['can_use_num'];
        //商家流水
        $transferList = $getData['transfer_list'];

        return view('admin.wish_generalize.seller_consume',compact('sellerInfo', 'money', 'voucher', 'transferList', 'page', 'purseType', 'type'));

    }

    /**
     * 推广券奖励记录
     */
    public function seller_transfer(Request $request){
        $sellerId = $request->input('uid');
        //找到nodeid
        $sellerInfo = WishSeller::where('id',$sellerId)->first();
        $reginfo = TnetReginfo::where('nodecode',$sellerInfo['mobile'])->first();
        $nodeid = $reginfo['nodeid'];

        //获取流水管理页面数据
        $data['_cmd'] = 'OfflineShopJoin_wishFinancePage';
        $data['node_id'] = $nodeid;
        $data['need'] = 'can_use_num,voucher_balance';

        $postData = get_offline_sign_data($data);
        $res = json_decode(curl_post(env('SHOP_DOMAIN'), $postData), true);
        $getData = $res['data'];
        //钱包余额
        $money = empty($getData['voucher_balance']['cash']['balance']) ? 0 : $getData['voucher_balance']['cash']['balance'];
        //推广券余额
        $voucher = empty($getData['can_use_num']) ? 0 : $getData['can_use_num'];

        $order_list = TnetCouponTransfer::where('node_id', $nodeid)
            ->when(\request()->has('type'), function ($order_list) {
                $order_list->where('type', \request()->input('type'));
            })
            ->when(\request()->has('start_time'), function ($order_list) {
                $order_list->where('create_time', '>=', \request()->input('start_time'));
            })
            ->when(\request()->has('end_time'), function ($order_list) {
                $order_list->where('create_time', '<=', \request()->input('end_time'));
            })
            ->whereIn('type', [3, 4])
            ->orderBy('id', 'desc')->paginate();
        //判断是否运营商
        $layouts = 'app';
        return view('admin.wish_generalize.seller_transfer', compact('sellerInfo','order_list', 'layouts','money', 'voucher'));
    }









}
