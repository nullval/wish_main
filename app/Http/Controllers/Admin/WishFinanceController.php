<?php

namespace App\Http\Controllers\Admin;


use App\Models\TnetCouponTransfer;
use App\Models\TnetReginfo;
use Illuminate\Http\Request;

class WishFinanceController extends CommonController
{
    public function index(Request $request)
    {
        $user = session('admin_info');
        $type = $request->input('type', 0);
        $purseType = $request->input('purse_type', 0);
        $page = $request->input('page', 1);

        $nodeid = $this->getNodeId();

        //获取流水管理页面数据
        $data['_cmd'] = 'OfflineShopJoin_wishFinancePage';
        $data['node_id'] = $nodeid;
        $data['type'] = $type; //类型全部0,支出1,收入2
        $data['purse_type'] = $purseType; //0全部,1现金,3代金券,4推荐奖励 5货款钱包
        $data['need'] = 'can_use_num,voucher_balance,transfer_list'; //需要什么数据：can_use_num(钱包)voucher_balance(推广奖励记录)transfer_list(商家流水)用逗号隔开

        $data['page'] = $page++;
        $postData = get_offline_sign_data($data);
        $res = json_decode(curl_post(env('SHOP_DOMAIN'), $postData), true);
        $getData = $res['data'];

        //钱包余额
        $money = empty($getData['voucher_balance']['cash']['balance']) ? 0 : $getData['voucher_balance']['cash']['balance'];
        //推广券余额
        $voucher = empty($getData['can_use_num']) ? 0 : $getData['can_use_num'];
        //商家流水
        $transferList = $getData['transfer_list'];


        return view('admin.wish_finance.datum', compact('user', 'money', 'voucher', 'transferList', 'page', 'purseType', 'type'));
    }

    /**
     * 推广券流水记录（废弃）
     */
//    public function transfer_list()
//    {
//        $user = session('admin_info');
//        $nodeid = $this->getNodeId();
//
//        //获取流水管理页面数据
//        $data['_cmd'] = 'OfflineShopJoin_wishFinancePage';
//        $data['node_id'] = $nodeid;
//        $data['need'] = 'can_use_num,voucher_balance';
//
//        $postData = get_offline_sign_data($data);
//        $res = json_decode(curl_post(env('SHOP_DOMAIN'), $postData), true);
//        $getData = $res['data'];
//        //钱包余额
//        $money = empty($getData['voucher_balance']['cash']['balance']) ? 0 : $getData['voucher_balance']['cash']['balance'];
//        //推广券余额
//        $voucher = empty($getData['can_use_num']) ? 0 : $getData['can_use_num'];
//
//        $order_list = TnetCouponTransfer::where('node_id', $nodeid)
//            ->when(\request()->has('type'), function ($order_list) {
//                $order_list->where('type', \request()->input('type'));
//            })
//            ->when(\request()->has('start_time'), function ($order_list) {
//                $order_list->where('create_time', '>=', \request()->input('start_time'));
//            })
//            ->when(\request()->has('end_time'), function ($order_list) {
//                $order_list->where('create_time', '<=', \request()->input('end_time'));
//            })
//            ->whereIn('type', [3, 4])
//            ->orderBy('id', 'desc')->paginate();
//        //判断是否运营商
//        $layouts = 'app';
//        return view('admin.wish_finance.transfer_list', compact('user','order_list', 'layouts','money', 'voucher'));
//
//    }


    /**
     * 获得node_id
     */
    private function getNodeId(){
        $user = session('admin_info');
        $tnetReginfo = new TnetReginfo();
        $nodeid = $tnetReginfo->getNodeIdByMobile($user['username'])['nodeid'];
        return $nodeid;
    }


}