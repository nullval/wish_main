<?php

namespace App\Http\Controllers\Admin;
use App\Exceptions\ApiException;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Services\AdminIdentityDispose;
use Illuminate\Http\Request;


class LoginController extends Controller
{


    /**
     * 登录页面
     */
    public function login(Request $request){


        $loginType = $request->input('login_type','seller');

        return view('admin.login.login',compact('loginType'));
    }


    /**
     * 提交登录
     */
    public function login_submit(Request $request){
        $loginType = $request->input('login_type','seller');
        $adminIdentityModel = new AdminIdentityDispose();
        //根据英文得到英文标识
        $identity = $adminIdentityModel->getIdentityBySign($loginType);

        $Admin=new Admin();
        $return_arr=$Admin->login($request->all(),$identity);
        return redirect(get_admin_jump_url());
    }

    /**
     * 退出登录
     */
    public function logout(Request $request){
        //退出到指定的身份后台地址
        $user=session('admin_info');
        $adminIdentityModel = new AdminIdentityDispose();
        $identity = $adminIdentityModel->getUserIdentity($user['role_id']);
        $sign = $adminIdentityModel->getSignByIdentity($identity);

        //删除所有数据
        $request->session()->flush();
        return redirect('/login?login_type='.$sign);
    }

    public function test(){
        $UserTree=new UserTree();
        $UserTree->mount_tree(10002,10001);
    }

}
