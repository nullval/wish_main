<?php

namespace App\Http\Controllers\Admin;


use App\Libraries\Huanxun\Huanxun;
use App\Models\Seller;
use Illuminate\Http\Request;

class HuanxunController extends CommonController
{
    /**
     * 添加银行卡
     * @return \Illuminate\Http\Response
     */
    public function open(){
        $Huanxun=new Huanxun();
        $seller_id=request('uid');
        if(request('action')=='success'){
            //开户成功跳转页面
            return return_success_page('开户成功');
        }
        $seller_info=Seller::where(['seller.id'=>$seller_id])
            ->leftJoin('ruzhu_merchant_basic as a1','seller.id','=','a1.uid')
            ->select(['seller.mobile','a1.*'])->first();
        $corpmanId=$seller_info->corpmanId;
        //商家身份证必须正确
        if(!is_idcard($corpmanId)){
            return return_error_page('商家身份证异常');
        }
        $Huanxun->open([
            'customerCode'=>$seller_info->mobile,
            'identityNo'=>$corpmanId,
            'userName'=>$seller_info->corpmanName,
            'mobiePhoneNo'=>$seller_info->mobile,
            'pageUrl'=>url('huanxun/open?action=success'),
            's2sUrl'=>url('huanxun/open')
        ]);
    }


    /**
     * 添加银行卡
     * @return \Illuminate\Http\Response
     */
    public function bankCard(){
        $Huanxun=new Huanxun();
        $seller_id=request('uid');
        if(request('action')=='success'){
            //开户成功跳转页面
            return return_success_page('银行卡添加成功');
        }
        $mobile=Seller::where(['seller.id'=>$seller_id])->value('mobile');
        $Huanxun->bankCard([
            'customerCode'=>$mobile,
            'pageUrl'=>url('huanxun/bankCard?action=success'),
            's2sUrl'=>url('huanxun/bankCard')
        ]);
    }


    /**
     * 实名认证
     * @return \Illuminate\Http\Response
     */
    public function toCertificate(){
        $Huanxun=new Huanxun();
        $seller_id=request('uid');
        $mobile=Seller::where(['seller.id'=>$seller_id])->value('mobile');
        $Huanxun->toCertificate([
            'customerCode'=>$mobile
        ]);
    }

}
