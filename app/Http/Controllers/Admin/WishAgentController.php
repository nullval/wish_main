<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\ApiException;
use App\Http\Requests\Admin\AddAgentRequest;
use App\Http\Requests\Admin\AddSellerRequest;
use App\Http\Requests\Admin\EditAgentRequest;
use App\Http\Requests\Admin\EditSeller1Request;
use App\Models\AdminRole;
use App\Models\BankList;
use App\Models\TnetCouponTransfer;
use App\Models\TnetReginfo;
use App\Models\Wish\WishAgent;
use App\Models\Wish\WishAgentBank;
use App\Models\Wish\WishAgentInfo;
use App\Models\Wish\WishGeneralize;
use App\Models\Wish\WishSeller;
use App\Models\Wish\WishSellerInfo;
use App\Services\AdminIdentityDispose;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WishAgentController extends CommonController
{

    /**
     * 代理管理列表
     */
   public function agent_list(Request $request){
       $user=session('admin_info');
       $adminIdentity = new AdminIdentityDispose();
       $identity = $adminIdentity->checkMobileIsIdentity($user['username'],AdminIdentityDispose::IDENTITY_COMPANY);
       if(!$identity){
           return pageBackSuccess('当前登录用户不是分公司');
       }

       //代理手机号
       $mobile=$request->input('mobile');
       $name=$request->input('name');
       $user=session('admin_info');
       $agent_list=WishAgent::where(function ($query) use ($mobile,$name,$identity,$user){
           if(!empty($mobile)){
               $query->where('wish_agent.mobile','like','%'.$mobile.'%');
           }
           if(!empty($name)){
               $query->where('a1.merchantName','like','%'.$name.'%');
           }

           //筛选当前代理下的直推人
           if($user['role_id'] != AdminRole::ROLE_CORPORATION && $identity == AdminIdentityDispose::IDENTITY_COMPANY){
               $adminIdentity = new AdminIdentityDispose();
               $generalizeInfo = $adminIdentity->getInfoByIdentityAndPhone($user['username'],$identity);
               $query->where('a2.id',$generalizeInfo['id']);
           }

           return $query;
       })
           ->leftJoin('wish_agent_info as a1','wish_agent.id','=','a1.uid')
           ->leftJoin('wish_company as a2','wish_agent.inviter_id','=','a2.id')
           ->where('wish_agent.mobile','!=',$user['username'])
           ->select(['wish_agent.*',
               'a1.corpmanName',
               'a1.status as apply_status',
               'a1.merchantName'
               ,'a1.id as basic_id',
               'a2.mobile as agent_mobile',
               'a2.company_no as company_no',
           ])
           ->orderBy('id','desc')
           ->paginate(15);

       return view('admin.wish_agent.agent_list',['agent_list'=>$agent_list]);
   }

    /**
     * 冻结/解冻商户
     */
    public function change_status(Request $request){

        $request_info=$request->all();
        WishAgent::where(['id'=>$request_info['id']])->update(['status'=>$request_info['status']]);
        return arr_post(1,'状态改变成功');
    }

    /**
     * 代理入驻页面
     * @return mixed
     */
    public function agent_add(){
        //银行列表
        $bank_list=BankList::get();
        //城市列表
        $city_arr = get_ght_attr('city');
        //经营类别列表
        $category_arr = get_ght_attr('category');
        //去掉名字中的英文名
        foreach ($category_arr as $value){
            $arr = explode(" ",$value->categoryName);
            $value->categoryName = $arr[1];
        }
        //银行代码列表
        $bank_arr = array();
        $bank_arr1 = get_ght_attr('bank');
        foreach ($bank_arr1 as $item => $value){
            foreach ($bank_list as $k => $v){
                if ($v['bank_name'] == $value->bankName){
                    $bank_arr[$item]->bankCode =  $value->bankCode;
                    $bank_arr[$item]->bankName =  $value->bankName;
                }
            }
        }

        //检查当前用户是否是分公司
        $user=session('admin_info');
        $adminIdentity = new AdminIdentityDispose();
        $identity = $adminIdentity->checkMobileIsIdentity($user['username'],AdminIdentityDispose::IDENTITY_COMPANY,1);
        $supeerior_mobile = '';
        if($identity){
            $supeerior_mobile = $user['username'];
        }



        //得到代理等级信息
        $agent_level_list = WishAgent::AGENT_LEVEL;
        return view('admin.wish_agent.agent_add',compact('bank_list','city_arr','category_arr','bank_arr','agent_level_list','supeerior_mobile'));
    }

    /**
     * 新增代理提交
     * @param AddSellerRequest $addSellerRequest
     * @return \Illuminate\Http\RedirectResponse
     * @throws ApiException
     */
    public function agent_add_submit(AddAgentRequest $addSellerRequest){
        $wishSellerInfo = new WishAgentInfo();
        $requestInfo = $addSellerRequest->all();
        $wishSellerInfo->add_agent($requestInfo,1,$requestInfo['invite_mobile']);
        return pageBackSuccess('代理入驻成功');
    }

    /**
     * 编辑代理入驻信息页面
     * @param Request $request
     * @return mixed
     */
    public function agent_edit(Request $request){
        $uid=$request->input('uid');
        $bank_list=BankList::get();
        $city_arr = get_ght_attr('city');
        $category_arr = get_ght_attr('category');
        $bank_arr = get_ght_attr('bank');
        //获取用户信息
        $agent_info=WishAgent::where(['id'=>$uid])->first();
        //获取代理基本信息
        $basic_info=WishAgentInfo::where(['uid'=>$uid])->first();
        //获取代理银行卡信息
        $bank_info=WishAgentBank::where(['merchantId'=>$agent_info->child_merchant_no])->first();
        //得到代理等级信息
        $agent_level_list = WishAgent::AGENT_LEVEL;

        return view('admin.wish_agent.agent_edit',compact('bank_list','city_arr','category_arr','bank_arr','agent_info','basic_info','bank_info','bank','agent_level_list'));
    }

    /**编辑代理基本信息
     * @param EditSeller1Request $editSeller1Request
     * @return \Illuminate\Http\RedirectResponse
     * @throws ApiException
     */
    public function agent_edit_submit(EditAgentRequest $editSeller1Request){
        $wishSellerInfo = new WishAgentInfo();
        $requestInfo = $editSeller1Request->all();
        $wishSellerInfo->add_agent($requestInfo,2);
        return pageBackSuccess('修改基本信息成功');
    }

    /**
     * 直推人列表
     */
    public function generalize_list(Request $request){
        $user=session('admin_info');
        $adminIdentity = new AdminIdentityDispose();
        $identity = $adminIdentity->checkMobileIsIdentity($user['username'],AdminIdentityDispose::IDENTITY_COMPANY);
        if(!$identity){
            return pageBackSuccess('当前登录用户不是分公司');
        }

        //当前登录用户条数
        $companyInfo = $adminIdentity->getInfoByIdentityAndPhone($user['username'],$identity);
        $user=session('admin_info');


        //直推人手机号
        $mobile=$request->input('mobile');
        $agent_mobile=$request->input('agent_mobile');
        $generalize_list=WishGeneralize::with('generalize_info')->where('wish_generalize.mobile','!=',$user['username'])->whereIn('inviter_id',function ($query)use ($companyInfo,$agent_mobile){
            $query->select('id')
                ->from('wish_agent')
                ->where('inviter_id',$companyInfo['id']);
            if($agent_mobile){
                $query->where('mobile',$agent_mobile);
            }

        })->where(function ($query) use ($mobile){
            if(!empty($mobile)){
                $query->where('wish_generalize.mobile','like','%'.$mobile.'%');
            }
            return $query;
        })
            ->orderBy('id','desc')
            ->paginate(15);
        return view('admin.wish_agent.generalize_list',['generalize_list'=>$generalize_list]);
    }

    /**
     * 商家列表
     */
    public function seller_list(Request $request){
        $user=session('admin_info');

        $adminIdentity = new AdminIdentityDispose();
        $identity = $adminIdentity->checkMobileIsIdentity($user['username'],AdminIdentityDispose::IDENTITY_COMPANY);
        if(!$identity){
            return pageBackSuccess('当前登录用户不是分公司');
        }

        $companyInfo = $adminIdentity->getInfoByIdentityAndPhone($user['username'],$identity);

        //商家手机号
        $mobile=$request->input('mobile');
        $generalize_mobile=$request->input('generalize_mobile');
        $seller_list = WishSeller::with('wish_generalize')->with('seller_info')->whereIn('inviter_id',function ($query) use ($companyInfo,$generalize_mobile){
            $query->select('id')
                ->from('wish_generalize')
                ->whereIn('inviter_id',function ($query)use($companyInfo){
                  $query->select('id')
                      ->from('wish_agent')
                      ->where('inviter_id',$companyInfo['id']);
                });
            if($generalize_mobile){
                $query->where(['mobile'=>$generalize_mobile]);
            }
        })->where(function ($query) use ($mobile){
            if(!empty($mobile)){
                $query->where('wish_seller.mobile','like','%'.$mobile.'%');
            }
            return $query;
        })
            ->orderBy('id','desc')
            ->paginate(15);
        return view('admin.wish_agent.seller_list',['seller_list'=>$seller_list]);
    }

    /**
     * 商家详情
     */
    public function seller_detail(Request $request){
        $sellerId = $request->input('uid');
        //获取商家信息
        $seller_info = WishSellerInfo::where('uid',$sellerId)->first();
        //找到分类信息
        $category_arr = get_ght_attr('category');
        $category_text = '';
        foreach ($category_arr as $arr){
            if($arr->categoryCode == $seller_info['category']){
                $category_text = $arr->categoryName;
                break;
            }
        }
        //组装地址信息
        $districtInfo = DB::table('tnet_area')->where('area_code',$seller_info['area_id'])->first();
        $cityInfo = DB::table('tnet_area')->where('area_id',$districtInfo->parent_id)->first();
        $provinceInfo = DB::table('tnet_area')->where('area_id',$cityInfo->parent_id)->first();
        $address = $provinceInfo->area_name.' '.$cityInfo->area_name.' '.$districtInfo->area_name;

        return view('admin.wish_agent.seller_detail',compact('seller_info','category_text','address'));
    }

    /**
     * 商家消费记录
     */
    public function seller_consume(Request $request){
        $sellerId = $request->input('uid');
        //找到nodeid
        $sellerInfo = WishSeller::where('id',$sellerId)->first();
        $reginfo = TnetReginfo::where('nodecode',$sellerInfo['mobile'])->first();
        $nodeid = $reginfo['nodeid'];

        $type = $request->input('type', 0);
        $purseType = $request->input('purse_type', 0);
        $page = $request->input('page', 1);

        //获取流水管理页面数据
        $data['_cmd'] = 'OfflineShopJoin_wishFinancePage';
        $data['node_id'] = $nodeid;
        $data['type'] = $type; //类型全部0,支出1,收入2
        $data['purse_type'] = $purseType; //0全部,1现金,3代金券,4推荐奖励
        $data['need'] = 'can_use_num,voucher_balance,transfer_list'; //需要什么数据：can_use_num(钱包)voucher_balance(推广奖励记录)transfer_list(商家流水)用逗号隔开

        $data['page'] = $page++;
        $postData = get_offline_sign_data($data);
        $res = json_decode(curl_post(env('SHOP_DOMAIN'), $postData), true);
        $getData = $res['data'];

        //钱包余额
        $money = empty($getData['voucher_balance']['cash']['balance']) ? 0 : $getData['voucher_balance']['cash']['balance'];
        //推广券余额
        $voucher = empty($getData['can_use_num']) ? 0 : $getData['can_use_num'];
        //商家流水
        $transferList = $getData['transfer_list'];

        return view('admin.wish_agent.seller_consume',compact('sellerInfo', 'money', 'voucher', 'transferList', 'page', 'purseType', 'type'));

    }

    /**
     * 推广券奖励记录
     */
    public function seller_transfer(Request $request){
        $sellerId = $request->input('uid');
        //找到nodeid
        $sellerInfo = WishSeller::where('id',$sellerId)->first();
        $reginfo = TnetReginfo::where('nodecode',$sellerInfo['mobile'])->first();
        $nodeid = $reginfo['nodeid'];

        //获取流水管理页面数据
        $data['_cmd'] = 'OfflineShopJoin_wishFinancePage';
        $data['node_id'] = $nodeid;
        $data['need'] = 'can_use_num,voucher_balance';

        $postData = get_offline_sign_data($data);
        $res = json_decode(curl_post(env('SHOP_DOMAIN'), $postData), true);
        $getData = $res['data'];
        //钱包余额
        $money = empty($getData['voucher_balance']['cash']['balance']) ? 0 : $getData['voucher_balance']['cash']['balance'];
        //推广券余额
        $voucher = empty($getData['can_use_num']) ? 0 : $getData['can_use_num'];

        $order_list = TnetCouponTransfer::where('node_id', $nodeid)
            ->when(\request()->has('type'), function ($order_list) {
                $order_list->where('type', \request()->input('type'));
            })
            ->when(\request()->has('start_time'), function ($order_list) {
                $order_list->where('create_time', '>=', \request()->input('start_time'));
            })
            ->when(\request()->has('end_time'), function ($order_list) {
                $order_list->where('create_time', '<=', \request()->input('end_time'));
            })
            ->whereIn('type', [3, 4])
            ->orderBy('id', 'desc')->paginate();
        //判断是否运营商
        $layouts = 'app';
        return view('admin.wish_agent.seller_transfer', compact('sellerInfo','order_list', 'layouts','money', 'voucher'));
    }




}
