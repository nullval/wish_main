<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\ApiException;
use App\Http\Requests\Admin\AddSellerRequest;
use App\Http\Requests\Admin\EditSeller1Request;
use App\Models\AdminRole;
use App\Models\BankList;
use App\Models\OfflineOrder;
use App\Models\PushQrQrcode;
use App\Models\TnetCouponTransfer;
use App\Models\TnetReginfo;
use App\Models\Wish\WishSeller;
use App\Models\Wish\WishSellerBank;
use App\Models\Wish\WishSellerInfo;
use App\Services\AdminIdentityDispose;
use Illuminate\Http\Request;
use labs7in0\coord\Coord;

class WishSellerController extends CommonController
{

    /**
     * 商家管理列表
     */
   public function seller_list(Request $request){
       $user=session('admin_info');
       $adminIdentity = new AdminIdentityDispose();
       $identity = $adminIdentity->checkMobileIsIdentity($user['username'],AdminIdentityDispose::IDENTITY_GENERALIZE);
       if(!$identity){
           return pageBackSuccess('当前登录用户不是直推人');
       }

       //商家手机号
       $mobile=$request->input('mobile');
       $name=$request->input('name');
       $agent_mobile=$request->input('agent_mobile');
       $seller_list=WishSeller::where(function ($query) use ($mobile,$name,$agent_mobile,$identity,$user){
           if(!empty($mobile)){
               $query->where('wish_seller.mobile','like','%'.$mobile.'%');
           }
           if(!empty($name)){
               $query->where('a1.merchantName','like','%'.$name.'%');
           }
           if(!empty($agent_mobile)){
               $query->where('a2.mobile','like','%'.$agent_mobile.'%');
           }
           //筛选当前直推人下的商家条件
           if($user['role_id'] != AdminRole::ROLE_CORPORATION && $identity == AdminIdentityDispose::IDENTITY_GENERALIZE){
               $adminIdentityDispose = new AdminIdentityDispose();
               $generalizeInfo = $adminIdentityDispose->getInfoByIdentityAndPhone($user['username'],$identity);
               $query->where('a2.id',$generalizeInfo['id']);
           }

           return $query;
       })
           ->leftJoin('wish_seller_info as a1','wish_seller.id','=','a1.uid')
           ->leftJoin('wish_generalize as a2','wish_seller.inviter_id','=','a2.id')
           ->leftJoin('extension_code as a3','wish_seller.id','=','a3.sid')
           ->select(['wish_seller.*',
               'a1.corpmanName',
               'a1.status as apply_status',
               'a1.merchantName'
               ,'a1.id as basic_id',
               'a2.mobile as agent_mobile',
               'a3.code_number',
               'a3.state as code_state',
               'a3.id as code_id',
           ])
           ->orderBy('id','desc')
           ->paginate(15);

       return view('admin.wish_seller.seller_list',['seller_list'=>$seller_list]);
   }



    /**
     * 冻结/解冻商户
     */
    public function change_status(Request $request){

        $request_info=$request->all();
        WishSeller::where(['id'=>$request_info['id']])->update(['status'=>$request_info['status']]);
        return arr_post(1,'状态改变成功');
    }

    /**
     * 商家入驻页面
     * @return mixed
     */
    public function seller_add(){

        //银行列表
        $bank_list=BankList::get();
        //城市列表
        $city_arr = get_ght_attr('city');
        //经营类别列表
        $category_arr = get_ght_attr('category');
        //去掉名字中的英文名
        foreach ($category_arr as $value){
            $arr = explode(" ",$value->categoryName);
            $value->categoryName = $arr[1];
        }
        //银行代码列表
        $bank_arr = array();
        $bank_arr1 = get_ght_attr('bank');
        foreach ($bank_arr1 as $item => $value){
            foreach ($bank_list as $k => $v){
                if ($v['bank_name'] == $value->bankName){
                    $bank_arr[$item]->bankCode =  $value->bankCode;
                    $bank_arr[$item]->bankName =  $value->bankName;
                }
            }
        }
        //检查当前用户是否是直推人
        $user=session('admin_info');
        $adminIdentity = new AdminIdentityDispose();
        $identity = $adminIdentity->checkMobileIsIdentity($user['username'],AdminIdentityDispose::IDENTITY_GENERALIZE,1);
        $supeerior_mobile = '';
        if($identity){
            $supeerior_mobile = $user['username'];
        }

        return view('admin.wish_seller.seller_add',compact('bank_list','city_arr','category_arr','bank_arr','supeerior_mobile'));
    }

    /**
     * 新增商家提交
     * @param AddSellerRequest $addSellerRequest
     * @return \Illuminate\Http\RedirectResponse
     * @throws ApiException
     * @throws \labs7in0\coord\Exceptions\UnknownTypeException
     */
    public function seller_add_submit(AddSellerRequest $addSellerRequest){

        $wishSellerInfo = new WishSellerInfo();
        $requestInfo = $addSellerRequest->all();
        $wishSellerInfo->add_seller($requestInfo,1,$requestInfo['invite_mobile']);
        return pageBackSuccess('商家入驻成功');
    }

    /**
     * 编辑商家入驻信息页面
     * @param Request $request
     * @return mixed
     * @throws \labs7in0\coord\Exceptions\UnknownTypeException
     */
    public function seller_edit(Request $request){
        $uid=$request->input('uid');
        $bank_list=BankList::get();
        $city_arr = get_ght_attr('city');
        $category_arr = get_ght_attr('category');
        $bank_arr = get_ght_attr('bank');
        //获取用户信息
        $seller_info=WishSeller::where(['id'=>$uid])->first();

        //获取商家基本信息
        $basic_info=WishSellerInfo::where(['uid'=>$uid])->first();

        //将GCJ02坐标转换成百度坐标
        $coord = new Coord($basic_info->lng, $basic_info->lat,Coord::GCJ02);
        $coord_arr=explode(',',(string)$coord->copy()->toBd09());
        $basic_info->lng=$coord_arr[0];
        $basic_info->lat=$coord_arr[1];
        //获取商家银行卡信息
        $bank_info=WishSellerBank::where(['merchantId'=>$seller_info->child_merchant_no])->first();
        $basic_info['banner_image'] = json_decode($basic_info['banner_image'],true);

        //兼容旧版的banner图
        if(!empty($basic_info['banner_image']['banner1']) || !empty($basic_info['banner_image']['banner2'])){
            $new_banner = [];
            foreach ($basic_info['banner_image'] as $image){
                $new_banner[] = $image;
            }
            $basic_info['banner_image'] = $new_banner;
        }

        return view('admin.wish_seller.seller_edit',compact('bank_list','city_arr','category_arr','bank_arr','seller_info','basic_info','bank_info','bank'));
    }

    /**编辑商家基本信息
     * @param EditSeller1Request $editSeller1Request
     * @return \Illuminate\Http\RedirectResponse
     * @throws ApiException
     */
    public function seller_edit_submit(EditSeller1Request $editSeller1Request){
        $wishSellerInfo = new WishSellerInfo();
        $requestInfo = $editSeller1Request->all();

        $wishSellerInfo->add_seller($requestInfo,2);
        return pageBackSuccess('修改基本信息成功');
    }

    /**
     * 商家查看的消费订单
     */
    public function consume_list(Request $request){
        $user=session('admin_info');

        //判断当前登录的是否是商家
        $adminIdentity = new AdminIdentityDispose();
        $identity = $adminIdentity->checkMobileIsIdentity($user['username'],AdminIdentityDispose::IDENTITY_SELLER,1);
        if(!$identity){
            return pageBackSuccess('当前登录用户不是商家');
        }else{
            //根据用户ID找到商家nodeId
            $sellerId = WishSeller::where('mobile',$user['username'])->value('id');
            $order_list = OfflineOrder::with('consumer_mobile')->where(['uid'=>$sellerId])
                ->leftJoin('wish_seller','wish_seller.id','=','offline_order.uid')
                ->select(['offline_order.*','wish_seller.mobile'])
                ->orderBy('created_at','desc')
                ->paginate(15);
        }


        return view('admin.wish_seller.consume_list',compact('order_list'));
    }

    /**
     * 商家消费记录
     */
    public function seller_consume(Request $request){
        $sellerId = $request->input('uid');
        //找到nodeid
        $sellerInfo = WishSeller::where('id',$sellerId)->first();
        $reginfo = TnetReginfo::where('nodecode',$sellerInfo['mobile'])->first();
        $nodeid = $reginfo['nodeid'];

        $type = $request->input('type', 0);
        $purseType = $request->input('purse_type', 0);
        $page = $request->input('page', 1);

        //获取流水管理页面数据
        $data['_cmd'] = 'OfflineShopJoin_wishFinancePage';
        $data['node_id'] = $nodeid;
        $data['type'] = $type; //类型全部0,支出1,收入2
        $data['purse_type'] = $purseType; //0全部,1现金,3代金券,4推荐奖励
        $data['need'] = 'can_use_num,voucher_balance,transfer_list'; //需要什么数据：can_use_num(钱包)voucher_balance(推广奖励记录)transfer_list(商家流水)用逗号隔开

        $data['page'] = $page++;
        $postData = get_offline_sign_data($data);
        $res = json_decode(curl_post(env('SHOP_DOMAIN'), $postData), true);
        $getData = $res['data'];

        //钱包余额
        $money = empty($getData['voucher_balance']['cash']['balance']) ? 0 : $getData['voucher_balance']['cash']['balance'];
        //推广券余额
        $voucher = empty($getData['can_use_num']) ? 0 : $getData['can_use_num'];
        //商家流水
        $transferList = $getData['transfer_list'];

        return view('admin.wish_seller.seller_consume',compact('sellerInfo', 'money', 'voucher', 'transferList', 'page', 'purseType', 'type'));

    }

    /**
     * 推广券奖励记录
     */
    public function seller_transfer(Request $request){
        $sellerId = $request->input('uid');
        //找到nodeid
        $sellerInfo = WishSeller::where('id',$sellerId)->first();
        $reginfo = TnetReginfo::where('nodecode',$sellerInfo['mobile'])->first();
        $nodeid = $reginfo['nodeid'];

        //获取流水管理页面数据
        $data['_cmd'] = 'OfflineShopJoin_wishFinancePage';
        $data['node_id'] = $nodeid;
        $data['need'] = 'can_use_num,voucher_balance';

        $postData = get_offline_sign_data($data);
        $res = json_decode(curl_post(env('SHOP_DOMAIN'), $postData), true);
        $getData = $res['data'];
        //钱包余额
        $money = empty($getData['voucher_balance']['cash']['balance']) ? 0 : $getData['voucher_balance']['cash']['balance'];
        //推广券余额
        $voucher = empty($getData['can_use_num']) ? 0 : $getData['can_use_num'];

        $order_list = TnetCouponTransfer::where('node_id', $nodeid)
            ->when(\request()->has('type'), function ($order_list) {
                $order_list->where('type', \request()->input('type'));
            })
            ->when(\request()->has('start_time'), function ($order_list) {
                $order_list->where('create_time', '>=', \request()->input('start_time'));
            })
            ->when(\request()->has('end_time'), function ($order_list) {
                $order_list->where('create_time', '<=', \request()->input('end_time'));
            })
            ->whereIn('type', [3, 4])
            ->orderBy('id', 'desc')->paginate();
        //判断是否运营商
        $layouts = 'app';
        return view('admin.wish_seller.seller_transfer', compact('sellerInfo','order_list', 'layouts','money', 'voucher'));
    }


}
