<?php

namespace App\Http\Controllers\Admin;


use App\Exceptions\ApiException;
use App\Http\Requests\Agent\AddOrderRequest;
use App\Http\Requests\Agent\BindPushQrRequest;
use App\Jobs\handleGhtOrder;
use App\Models\Admin;
use App\Models\Agent;
use App\Models\AgentOperate;
use App\Models\Message;
use App\Models\Order;
use App\Models\Pos;
use App\Models\PushQrOrder;
use App\Models\PushQrQrcode;
use App\Models\PushQrUser;
use App\Models\User;
use App\Models\VOrder;
use App\Models\Wish\WishGeneralize;
use App\Models\Wish\WishSeller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use ZipArchive;

class OrderController extends CommonController
{

    /**
     * 未特商城pos机消费订单列表
     * @deprecated
     */
    public function order_list(Request $request)
    {
        //订单号
        $order_sn = $request->input('order_sn');
        $buyer_mobile = $request->input('buyer_mobile');
        $seller_mobile = $request->input('seller_mobile');
        $agent_mobile = $request->input('agent_mobile');
        $terminalId = $request->input('terminalId');
        $notify_type = $request->input('notify_type');
        $type = $request->input('type');
        $pay_type = $request->input('pay_type');
        $start_time = $request->input('start_time');
        $end_time = $request->input('end_time');
        $status = \request('pay_status');
        $order_where = VOrder::where(function ($query) use ($order_sn, $buyer_mobile, $seller_mobile, $agent_mobile, $terminalId, $notify_type, $type, $pay_type, $start_time, $end_time) {
            if (!empty($order_sn)) {
                //增加上游订单编号搜索 2018年6月29日13:57:12
                $query->where('v_order' . '.order_sn', 'like', '%' . $order_sn . '%')->orWhere('v_order' . '.up_order_sn', 'like', '%' . $order_sn . '%');
            }
            if (!empty($buyer_mobile)) {
                $query->where('u1.mobile', 'like', '%' . $buyer_mobile . '%');
            }
            if (!empty($seller_mobile)) {
                $query->where('u2.mobile', 'like', '%' . $seller_mobile . '%');
            }
            if (!empty($agent_mobile)) {
                $query->where('u3.mobile', 'like', '%' . $agent_mobile . '%');
            }
            if (!empty($terminalId)) {
                $query->where('v_order' . '.terminalId', 'like', '%' . $terminalId . '%');
            }
            if (!empty($pay_type)) {
                $query->where(['v_order.pay_type' => $pay_type]);
            }
            if (!empty($notify_type)) {
                $query->where(['v_order.notify_type' => $notify_type]);
            }
            if (!empty($type)) {
                if ($type == 4) {
                    //赛选有效订单
                    $query->where('actual_amount', '>', '1');
                }
                $query->where(['v_order.type' => $type]);
            }
//            if(!empty($start_time)||!empty($end_time)){
//                $query->whereBetween('v_order.created_at',[$start_time." 00:00:00",$end_time." 23:59:59"]);
//            }
            if (!empty($start_time) || !empty($end_time)) {
                $query->where('v_order.created_at', '>', $start_time . " 00:00:00")->where('v_order.created_at', '<', $end_time . " 23:59:59");
//                $query->whereDate('v_order.created_at',[$start_time." 00:00:00",$end_time." 23:59:59"]);
            }
            return $query;
        })
            ->leftJoin('user as u1', 'u1.id', '=', 'v_order.buyer_id')
            ->leftJoin('seller as u2', 'u2.id', '=', 'v_order.uid')
            ->leftJoin('agent as u3', 'u3.id', '=', 'v_order.agent_id')
            ->leftJoin('ruzhu_merchant_basic as u4', 'u4.uid', '=', 'v_order.uid');
//        if(empty($status)){
        $order_where->consumeOrder();
//        }else{
//            $order_where->consumeOrder($status);
//        }
        //统计 必须放在订单列表前面不然读取不到
        $sum_data = $order_where
            ->select(DB::raw('SUM(actual_amount) as actual_amount,SUM(seller_amount) as seller_amount,SUM(platform_amount) as platform_amount,SUM(poundage) as poundage'))
            ->first()->toArray();
        $sum_data['recharge'] = VOrder::where(['type' => 7, 'status' => 2])->where(function ($query) use ($order_sn, $start_time, $end_time) {
            if (!empty($order_sn)) {
                $query->where('v_order' . '.order_sn', 'like', '%' . $order_sn . '%');
            }
            if (!empty($order_sn)) {
                $query->where('v_order' . '.order_sn', 'like', '%' . $order_sn . '%');
            }
            if (!empty($start_time) || !empty($end_time)) {
                $query->whereBetween('v_order.created_at', [$start_time . " 00:00:00", $end_time . " 23:59:59",]);
            }
            return $query;
        })->sum('actual_amount');
        //订单列表
        $order_list = $order_where->orderBy('v_order.created_at', 'desc')
            ->select(['v_order.*',
                'u1.mobile as buyer_mobile',
                'u2.mobile as seller_mobile',
                'u3.mobile as agent_mobile',
                'u4.merchantName as seller_name',
            ])
            ->paginate(15);
//        pos商城购买商品以前记录的id是益邦客id 单独处理
        foreach ($order_list as $k => $v) {
            if ($v->type == 4 && $v->created_at->format('Y-m-d') < '2018-01-05') {
                $order_list[$k]->buyer_mobile = Agent::where(['id' => $v->buyer_id])->first(['mobile'])->mobile;
            }
            if ($v->type == 5) {
                $inviter_id = Seller::where(['id' => $v->uid])->value('inviter_id');
                $order_list[$k]->agent_mobile = Agent::where(['id' => $inviter_id])->value('mobile');
            }
        }
        return view('admin.order.order_list', compact('order_list', 'sum_data'));
    }

    /**
     * 购买未特商城pos机订单列表
     *
     * @deprecated
     */
    public function pos_order_list(Request $request)
    {
        //订单号
        $order_sn = $request->input('order_sn');
        $buyer_mobile = $request->input('buyer_mobile');
        $agent_mobile = $request->input('agent_mobile');
        $terminalId = $request->input('terminalId');
        $is_true = $request->input('is_true');
        $notify_type = $request->input('notify_type');
        $start_time = $request->input('start_time');
        $end_time = $request->input('end_time');
        $order_where = VOrder::where(function ($query) use ($order_sn, $buyer_mobile, $agent_mobile, $terminalId, $is_true, $notify_type, $start_time, $end_time) {
            if (!empty($order_sn)) {
                $query->where('v_order' . '.order_sn', 'like', '%' . $order_sn . '%');
            }
            if (!empty($buyer_mobile)) {
                $query->where('u1.mobile', 'like', '%' . $buyer_mobile . '%');
            }
            if (!empty($agent_mobile)) {
                $query->where('u2.mobile', 'like', '%' . $agent_mobile . '%');
            }
            if (!empty($terminalId)) {
                $query->where('v_order' . '.terminalId', 'like', '%' . $terminalId . '%');
            }
            if (!empty($is_true)) {
                $query->where(['v_order.is_true' => $is_true]);
            }
            if (!empty($notify_type)) {
                $query->where(['v_order.notify_type' => $notify_type]);
            }
            if (!empty($start_time) || !empty($end_time)) {
                $query->whereBetween('v_order.created_at', [$start_time . " 00:00:00", $end_time . " 23:59:59",]);
            }
            return $query;
        })
            ->leftJoin('agent as u1', 'u1.id', '=', 'v_order' . '.buyer_id')
            ->leftJoin('agent as u2', 'u2.id', '=', 'v_order' . '.agent_id')
            ->posOrder();
        //统计 必须放在订单列表前面不然读取不到
        $sum_data = $order_where
            ->select(DB::raw('SUM(actual_amount) as actual_amount,SUM(poundage) as poundage'))
            ->first();
        //订单列表
        $order_list = $order_where->select(['v_order.*', 'u1.mobile as buyer_mobile', 'u2.mobile as agent_mobile'])
            ->orderBy('v_order.created_at', 'desc')
            ->paginate(15);
        return view('admin.order.pos_order_list', compact('order_list', 'sum_data'));
    }

    /**
     * 购买未特商城pos机订单详情
     *
     * @deprecated
     */
    public function pos_order_detail(Request $request)
    {
        //订单号
        $order_sn = $request->input('order_sn');
        $year = $request->input('year');
        $pos_order_detail_table = 'pos_order_detail_' . $year;
        //获取子订单列表
        $order_detail_list = DB::table($pos_order_detail_table)
            ->where(['order_sn' => $order_sn])
            ->orderBy('id', 'desc')
            ->paginate(15);

        $order_table = 'order_' . $year;
        //获取订单详情
        $order_info = DB::table($order_table)
            ->where(['order_sn' => $order_sn])
            ->first();

//        var_dump(session('agent_info')->id);exit;
        return view('admin.order.pos_order_detail',
            ['order_detail_list' => $order_detail_list,
                'order_info' => $order_info
            ]);
    }

    /**
     * 地推二维码申请列表
     * 适用范围(所有角色)
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function pushQr_order_list(Request $request)
    {
//        leftJoin('wish_admin as u1', 'u1.id', '=', 'wish_pushqr_order.operate_id')
        $role_id = session('admin_info')->role_id;
        $order_list = PushQrOrder::select(['wish_pushqr_order.*'])->when($request->has('order_sn'), function ($query) use ($request) {
            $query->where('order_sn', $request->input('order_sn'));
        })
            ->orderBy('id', 'desc')->where('operate_id', session('admin_info')->id)
            ->when($request->has('operate_mobile'), function ($query) use ($request) {
                $query->where('contact_mobile', $request->input('operate_mobile'));
            })->paginate();//寻找下发至下级的订单
        $sum_data = (new PushQrOrder())->getAmount(session('admin_info')->id, $role_id);
        return view('admin.order.pushQr_order_list', compact('order_list', 'role_id', 'sum_data'));
    }

    /**
     * 地推二维码订单详情
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pushQr_order_detail(Request $request)
    {
        $id = $request->input('id');
        $info = PushQrOrder::where(['wish_pushqr_order.id' => $id])
//            ->leftJoin('wish_agent_operate as u1', 'u1.id', '=', 'wish_pushqr_order.operate_id')
            ->select(['wish_pushqr_order.*'])
            ->first();
        $info->id_arr_group = explode('|', $info->id_arr);
        //无法操作自己的订单,无法操作已处理的订单
        if (session('admin_info')->id == $info->operate_id or $info->status == 1) {
            $is_visual = false;
        } else {
            $is_visual = true;
        }
        return view('admin.order.pushQr_order_detail', compact('info', 'is_visual'));
    }

    /**
     * 地推二维码提交
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function pushQr_order_detail_submit(Request $request)
    {
        //修改表记录
        $id = $request->input('id');
        $arr['status'] = $request->input('status');
        $arr['shipping_company'] = $request->input('shipping_company');
        $arr['shipping_number'] = $request->input('shipping_number');
        $arr['finish_time'] = time2date();
        //申请人必须是自己的下级
        $children = (new Admin())->find_down(session('admin_info')->id)->toArray();
        $sid = PushQrOrder::where('id', $id)->value('operate_id');
        if (!in_array($sid, $children)) {
            return back()->withInput($request->all())->withErrors(['error' => '您无权操作此订单']);
        }
        //变更为已处理的订单要判断当前库存是否充足,总公司无需检查库存
        if ($arr['status'] == 1 and session('admin_info')->role_id != 1) {
            $res = (new PushQrOrder())->checkStock(session('admin_info')->id, $id);
            if ($res['code'] == 0) {
                return back()->withInput($request->all())->withErrors(['error' => $res['message']]);
            }
        }
        //判断该角色是否需要下载二维码 直推人才可以生成二维码
        if (in_array((int)session('admin_info')->role_id, [8])) {
            $result = (new PushQrOrder())->downloadQr($id);
            if ($result['code'] == 0) {
                return back()->withInput($request->all())->withErrors(['error' => $result['message']]);
            }
        }
        PushQrOrder::where(['id' => $id])->update($arr);
        return back()->with(['status' => '修改完成！']);

    }

    /**
     * 生成二维码
     */
    public function make_qrcode(Request $request)
    {
        if (\request()->isMethod('POST')) {
            //开始生成二维码
            $num = $request->input('count');
            if (session('admin_info')->role_id == 1) {
                $result = (new PushQrOrder())->downloadQr($num);
                if ($result['code'] == 0) {
                    return back()->withInput($request->all())->withErrors(['error' => $result['message']]);
                } else {
                    return back()->with(['status' => '生成任务成功！']);
                }
            } else {
                return back()->with(['status' => '只有总公司才可以生成二维码！']);
            }
        } else {
            //返回视图
            $layouts = 'app';
            return view('admin.order.make_qrcode', compact('layouts'));
        }
    }


    /**
     * 创建POS订单
     *
     * @deprecated
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create_pos_order(Request $request)
    {
        return view('admin.order.create_pos_order');

    }

    /**
     * POS订单提交
     *
     * @deprecated
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\ApiException
     */
    public function create_pos_order_submit(Request $request)
    {
        $mobile = \request('mobile');
        $invite_mobile = \request('invite_mobile');
        $is_direct_buy = \request('is_direct_buy');
        $terminalId = \request('terminalId');
        //需要管理员验证
        $Message = new Message();
        $Message->check_sms('17771200619', \request('code'), 17);
        $Order = new Order();
        $notify_type = 0;
        //购买pos机订单
        $amount = $is_direct_buy == 1 ? config('pos.pos.amount') : config('pos.pos.second_amount');
        $data = $Order->create_pos_order($mobile, $terminalId, '线下购买', '', 1, '', '', '', $invite_mobile, '', 'wechat', $amount, $is_direct_buy, $notify_type);
        dispatch((new handleGhtOrder($data['order_sn'], $data['pay_type'], '', $data['amount'], 2, '', date('YmdHis'), '', 0))->onQueue('handleOrder'));
        return pageBackSuccess('处理成功');
    }

    /**
     * 创建二维码订单
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add_pushQr_order()
    {
        //判断是否运营商
        $layouts = 'app';
        return view('admin.pushQr.add_order', compact('layouts'));
    }

    /**
     * 创建二维码订单提交
     *
     * @param AddOrderRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function add_pushQr_order_submit(Request $request)
    {
        //获取运营中心用户id 2018-8-10 18:00:30
        $user_id = session('admin_info')->id;
        if (session('admin_info')->role_id == 9) {
            return back()->withInput($request->all())->withErrors(['error' => '商家无法下发二维码']);
        }
        $PushQrOrder = new PushQrOrder();
        $contact_user = $request->input('contact_user');
        $contact_mobile = $request->input('contact_mobile');
//        $contact_address = $request->input('contact_address');
        $count = $request->input('count');
        if (!is_numeric($count) or $count < 1) {
            return back()->withInput($request->all())->withErrors(['error' => '下发数量请输入正整数!']);
        }
        //变更为已处理的订单要判断当前库存是否充足,总公司无需检查库存
        //判断该角色是否需要下载二维码 直推人才可以生成二维码
        $id_arr = $request->input('id_arr');
        $result = $PushQrOrder->add_order($contact_user, $contact_mobile, $count, $id_arr, $user_id, session('admin_info')->role_id);
        return $result['code'] == 0 ? back()->withInput($request->all())->withErrors(['error' => $result['message']]) : redirect('/order/pushQr_order_list')->with(['status' => '申请成功！']);

    }

    /**
     * 获取用户id
     *
     * @param string $mobile 用户手机号
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    private function get_oprate_id($mobile)
    {
        $uid = AgentOperate::where(['mobile' => $mobile])->first()->id;
        return $uid;
    }

    /**
     * 地推二维码列表
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function pushQr_list()
    {
//        $where = [];
//        $user_info = $this->get_pushQr_info();
//        $where = ['wish_pushqr_qrcode.qr_user_id' => $user_info->id];
        $query = PushQrQrcode::
        leftJoin('wish_admin as s1', 's1.id', '=', 'extension_code.agent_id')
            ->leftJoin('wish_seller_info as s2', 'extension_code.sid', '=', 's2.uid')
//            ->leftJoin('wish_agent_operate as s3', 's3.id', '=', 'extension_code.agent_id')
            ->select([
                'extension_code.*',
                's1.username as push_mobile',
                's2.merchantName'
//                's2.mobile as push_mobile',
//                's3.mobile as operate_mobile',
//                's2.level as user_level'
            ])
            ->when(\request()->has('code_number'), function ($query) {
                $query->where('code_number', \request()->input('code_number'));
            })
            ->when(\request()->has('operate_mobile'), function ($query) {
                $query->where('s1.username', \request()->input('operate_mobile'));
            })
            ->orderBy('id', 'desc');
        //能看到自己下级和自己的二维码
        if (session('admin_info')->role_id == 9) {
            //商家查看自己和所有的
            $query->where('agent_id', session('admin_info')->id);
        } elseif (session('admin_info')->role_id == 1) {
            //总公司不受限制
        } else {
            //所有人
            $children = (new Admin())->find_down(session('admin_info')->id)->toArray();
            array_push($children, session('admin_info')->id);
            $query->whereIn('agent_id', $children);
        }
        $list = $query->paginate();
        //判断是否运营商
        $layouts = 'app';
        $role_id = session('admin_info')->role_id;
        return view('admin.pushQr.pushqr_list', compact('list', 'layouts', 'role_id'));
    }

    /**
     * 绑定商家
     */
    public function bind_seller(Request $request){
        $code_id = $request->get('id');
        //查询码编号
        $PushQrQrcode = new PushQrQrcode();
        $qr_info = $PushQrQrcode->where('id',$code_id)->first();

        return view('admin.pushQr.bind_seller',compact('qr_info'));
    }
    /**
     * 提交绑定商家
     */
    public function bind_seller_submit(Request $request){
        $request_info = $request->all();
        $admin_info = session('admin_info');
        if(empty($request_info['seller_mobile'])){
            throw new ApiException('商家手机号不能为空');
        }
        //检查该手机号商户是否属于自己名下
        $wish_seller_model = new WishSeller();
        $wish_generalize_model = new WishGeneralize();
        //找到当前登录用户的推荐人身份ID
        $generalize_id = $wish_generalize_model->where('mobile',$admin_info['username'])->value('id');
        $seller_id = $wish_seller_model->where('inviter_id',$generalize_id)
            ->where('mobile',$request_info['seller_mobile'])
            ->value('id');
        if(empty($seller_id)){
            throw new ApiException('该商家不存在或者不属于您的团队');
        }
        //检查是否有权限使用二维码
        $admin_model = new Admin();
        $is_count = $admin_model->where('username',$admin_info['username'])->count();
        if(empty($is_count)){
            throw new ApiException('您没有权限使用该条二维码');
        }
        $PushQrQrcode = new PushQrQrcode();
        //检查该商家是否已经绑定
        $bind_count = $PushQrQrcode->where('sid',$seller_id)->where('state',2)->count();
        if($bind_count > 0){
            throw new ApiException('该所属商家已经绑定二维码');
        }
        //检查二维码
        $qr_info = $PushQrQrcode
            ->where('id',$request_info['code_id'])
            ->where('state',1)
            ->first();
        if(empty($qr_info)){
            throw new ApiException('二维码不存在或者已经绑定');
        }
        //绑定商家二维码
        $res = PushQrQrcode::where('id', $request_info['code_id'])->update([
            'sid' => $seller_id,
            'state' => 2//状态已使用
        ]);
        if (empty($res)) {
            return mobileView('error', '错误提示', '未知错误,请联系客服');
        }
        return pageBackSuccess('绑定成功！');
    }

        /**
     * 绑定地推二维码
     */
    public function bind_pushqr()
    {
        $user_info = $this->get_pushQr_info();
        //判断是否运营商
        $layouts = 'app';
//        $layouts = pushQrUser_is_operate() ? 'operate_app' : 'pushQr_app';
        return view('admin.pushQr.bind_pushqr', compact('user_info', 'layouts'));
    }

    /**
     * 绑定地推二维码提交
     *
     * @param BindPushQrRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bind_pushqr_submit(BindPushQrRequest $request)
    {
        $user_info = $this->get_pushQr_info();
        $PushQrQrcode = new PushQrQrcode();
        $push_number = $request->input('push_number');
        $mobile = $request->input('mobile');
        $level = $request->input('level');
        $result = $PushQrQrcode->bind_qrcode($push_number, $user_info->id, $mobile, $level);
        return $result['code'] == 0 ? back()->withInput($request->all())->withErrors(['error' => $result['message']]) : redirect('/pushQr/pushQr_list')->with(['status' => '绑定成功！']);
    }

    /**
     * 获取地推表用户信息
     */
    private function get_pushQr_info()
    {
        if (!empty(session('pushQr_user_info'))) {
            $mobile = session('pushQr_user_info')['mobile'];
        } elseif (!empty(session('agent_info'))) {
            $mobile = session('agent_info')['mobile'];
        }
        return PushQrUser::where(['mobile' => $mobile])->first();
    }

    /**
     * 二维码下载
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function pushQr_download(Request $request)
    {
        if ($request->has('ids')) {
            //批量下载
            //获取列表
            $datalist = PushQrQrcode::whereIn('id', explode(',', $request->ids))->select('code_url', 'code_number')->get();
            $filename = storage_path('app' . DIRECTORY_SEPARATOR . 'qrcode' . DIRECTORY_SEPARATOR . uuid() . '.zip');//最终生成的文件名（含路径）
            if (!file_exists($filename)) {
                //重新生成文件
                $zip = new ZipArchive();//使用本类，linux需开启zlib，windows需取消php_zip.dll前的注释
                if ($zip->open($filename, ZIPARCHIVE::CREATE) !== true) {
                    exit('无法打开文件，或者文件创建失败');
                }
                foreach ($datalist as $val) {
                    $tmp_file = $this->getFilePath($val->code_url, $val->code_number);
                    if (file_exists($tmp_file)) {
                        $zip->addFile($tmp_file, basename($tmp_file));//第二个参数是放在压缩包中的文件名称，如果文件可能会有重复，就需要注意一下
                    }
                }
                $zip->close();//关闭
            }
            if (!file_exists($filename)) {
                exit("无法找到文件"); //即使创建，仍有可能失败。。。。
            }
        } else {
            //单张下载
            $info = PushQrQrcode::find($request->id);
            if (empty($info)) {
                return back()->withInput($request->all())->withErrors(['error' => '记录不存在']);
            }
            $filename = $this->getFilePath($info->code_url, $info->code_number);
        }
        return response()->download($filename);
    }

    /**
     * 根据URL保存到文件路径
     *
     * @param $code_url
     * @return string
     */
    private function getFilePath($code_url, $code_number)
    {
        $file_data = file_get_contents($code_url);
        //写入文件
        $file_name = storage_path('app' . DIRECTORY_SEPARATOR . 'qrcode' . DIRECTORY_SEPARATOR . $code_number . '.jpg');
        if (!is_dir(storage_path('app' . DIRECTORY_SEPARATOR . 'qrcode'))) {
            mkdir(storage_path('app' . DIRECTORY_SEPARATOR . 'qrcode'));
        }
        if (!file_exists($file_name)) {
            file_put_contents($file_name, $file_data);
        }
        return $file_name;
    }

    /**
     * 扫描二维码
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function scan_code(Request $request){
        //判断二维码是否已经被使用
        $num = intval($request->input('s'));
        $code_number = PushQrQrcode::getCodeNumber($num);
        $info = PushQrQrcode::where('code_number', $code_number)->first();

        if ($info['state'] == 2 && $info['sid']) {
            //检查商家状态
            $status = WishSeller::where('id',$info['sid'])->value('status');
            if($status == 0){
                //该商家已禁用
                $win_back = "window.location.href='http://ws-web.hychqt.com/#';";
                echo "<script>alert('该商家已禁用');{$win_back}</script>";
                exit;
//                return redirect('http://ws-web.hychqt.com/#');
            }

            //正式环境需要更新跳转地址 2018-8-13 22:23:29
            if (env('APP_ENV') == 'online') {
                //正式服 http://cs-ws-web.hychqt.com/#/StoreDetail?uid=14
                return redirect('http://ws-web.hychqt.com/#/StoreDetail?uid='.$info['sid']);
            } else {
                //测试服
                return redirect('http://cs-ws-web.hychqt.com/#/StoreDetail?uid='.$info['sid']);
            }
        }else{
            //二维码未使用
            if (env('APP_ENV') == 'online') {
                //正式服
                return redirect('http://ws-web.hychqt.com/#/admission?ss='.$num);
            } else {
                //测试服
                return redirect('http://cs-ws-web.hychqt.com/#/admission?ss='.$num);
            }
        }


    }

    /**
     * 扫描二维码(废弃)
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
//    public function scan_code(Request $request)
//    {
//        if ($request->isMethod('POST')) {
//            $mobile = $request->input('mobile');
//            if ($mobile && (strlen($mobile) != 11 || substr($mobile, 0, 1) != '1')) {
//                return mobileView('error', '错误提示', '手机号格式错误');
//            }
//            $seller_id = DB::table('wish_seller')->where('mobile', $mobile)->value('id');
//            if (empty($seller_id)) {
//                //商家不存在,跳到,总公司要手动填写直推人
//                $leader_mobile = DB::table('wish_admin')->where('id', PushQrQrcode::where('id', $request->input('qr_id'))->value('agent_id'))->value('username');
//                //添加推荐人信息至session
//                Session::put('seller_mobile', $mobile);
//                Session::put('invite_mobile', $leader_mobile);
//                return redirect('/seller/add');
//            }
//            //检查用户是否已拥有,一户一码
//            $count = PushQrQrcode::where('sid', $seller_id)->count();
//            if ($count > 0) {
//                return mobileView('error', '错误提示', '每个商户最多绑定一张二维码');
//            }
//            $res = PushQrQrcode::where('id', $request->input('qr_id'))->update([
//                'sid' => $seller_id,
//                'state' => 2//状态已使用
//            ]);
//            if (empty($res)) {
//                return mobileView('error', '错误提示', '未知错误,请联系客服');
//            }
//            //绑定二维码
//            return mobileView('success', '成功提示', '绑定成功');
//        }
//        //
//        if (!$request->has('s')) {
//            return mobileView('error', '错误提示', '参数错误');
//        }
//        $num = intval($request->input('s'));
//
//        $code_number = str_pad(($num - 5) / 88, 9, '0', STR_PAD_LEFT);
//        $info = PushQrQrcode::where('code_number', $code_number)->first();
//        if (empty($info)) {
//            return mobileView('error', '错误提示', '二维码不存在');
//        }
//        if (empty($info->sid)) {
//            return mobileView('error', '错误提示', '二维码参数不存在');
//        }
//
//        //查看二维码是否已绑定,未使用
//        if (empty($info->sid) and $info->state == 1) {
//            if (env('APP_ENV') == 'online') {
//                //正式服
//                return redirect('http://ws-web.hychqt.com/#/StoreDetail?shop_id=' . $info->sid . '&code_number=' . $code_number);
//            } else {
//                //测试服
//                return redirect('http://cs-ws-web.hychqt.com/#/StoreDetail?shop_id=' . $info->sid . '&code_number=' . $code_number);
//            }
//        }
//    }

}
