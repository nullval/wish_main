<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests\Admin\AddPosRequest;
use App\Http\Requests\Admin\EditPosRequest;
use App\Jobs\handleGhtOrder;
use App\Models\Agent;
use App\Models\AgentOperate;
use App\Models\Message;
use App\Models\Order;
use App\Models\Pos;
use App\Models\UsedOperate;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Excel;

class PosController extends CommonController
{
    /**
     * pos机管理列表
     */
    public function pos_list(Request $request)
    {
        //终端号
        $terminalId = $request->input('terminalId');
        //绑定商家手机号
        $seller_mobile = $request->input('seller_mobile');
        //所属机主
        $agent_mobile = $request->input('agent_mobile');
        //所属运营中心
        $operate_mobile = $request->input('operate_mobile');
        $pos_type = $request->input('pos_type');
        //设备号
        $dev = $request->input('dev');
        $pos_list = Pos::where(function ($query) use ($terminalId, $seller_mobile, $agent_mobile, $dev, $operate_mobile, $pos_type) {
            if (!empty($terminalId)) {
                $query->where('pos.terminalId', 'like', '%' . $terminalId . '%');
            }
            if (!empty($dev)) {
                $query->where('pos.dev', 'like', '%' . $dev . '%');
            }
            if (!empty($seller_mobile)) {
                $query->where(['u1.mobile' => $seller_mobile]);
            }
            if (!empty($agent_mobile)) {
                $query->where(['u2.mobile' => $agent_mobile]);
            }
            if (!empty($operate_mobile)) {
                $query->where(['u3.mobile' => $operate_mobile]);
            }
            if (!empty($pos_type)) {
                $query->where(['pos.pos_type' => $pos_type]);
            }
            return $query;
        })
            ->leftJoin('seller as u1', 'u1.id', '=', 'pos.uid')
            ->leftJoin('agent as u2', 'u2.id', '=', 'pos.agent_uid')
            ->leftJoin('agent_operate as u3', 'u3.id', '=', 'pos.agent_operate_id')
            ->leftJoin('admin', 'admin.id', '=', 'pos.admin_id')
            ->select(['pos.*', 'u1.mobile as seller_mobile', 'u2.mobile as agent_mobile', 'u3.mobile as agent_operate_mobile', 'admin.username'])
            ->orderBy('pos.id', 'desc')
            ->paginate(15);

        return view('admin.pos.pos_list', ['pos_list' => $pos_list]);
    }

    /**
     * pos机编辑/新增页面
     */
    public function pos_add()
    {
        ;
        //初始化字段
        $info = init_table_field('pos');
        return view('admin.pos.pos_add', ['info' => $info]);
    }

    public function pos_edit()
    {
        $id = \request('id');
        $info = Pos::where(['pos.id' => $id])
            ->leftJoin('seller as u1', 'u1.id', '=', 'pos.uid')
            ->leftJoin('agent as u2', 'u2.id', '=', 'pos.agent_uid')
            ->leftJoin('agent_operate as u3', 'u3.id', '=', 'pos.agent_operate_id')
            ->leftJoin('agent_operate as o1', 'o1.id', '=', 'pos.operate_level1')
            ->leftJoin('agent_operate as o2', 'o2.id', '=', 'pos.operate_level2')
            ->leftJoin('agent_operate as o3', 'o3.id', '=', 'pos.operate_level3')
            ->select(['pos.*',
                'u1.mobile as seller_mobile',
                'u2.mobile as agent_mobile',
                'u3.mobile as operate_mobile',
                'o1.mobile as operate1_mobile',
                'o2.mobile as operate2_mobile',
                'o3.mobile as operate3_mobile'
            ])
            ->first();
//        dd($info->toArray());
        return view('admin.pos.pos_edit', ['info' => $info]);
    }

    /**
     * 下载模板文件
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function pos_demo_download(){
//        return response()->download(public_path('admin/批量导入POS机模板文件.xlsx'));
        header("Content-type:application/octet-stream");
        header("Accept-Ranges:bytes");
//        header("Accept-Length:".$file_Size);
        header("Content-Disposition: attachment; filename=批量导入POS机模板文件.xlsx");
        echo file_get_contents(public_path('admin/批量导入POS机模板文件.xlsx'));
        exit();
    }

    /**
     * pos机新增提交
     */
    public function pos_add_submit(AddPosRequest $request)
    {
        //添加pos机并购买
        $data = DB::transaction(function () {
            $Pos = new Pos();
            $Pos->add_pos(0,
                \request('terminalId'),
                \request('pos_type'),
                \request('seller_mobile'),
                \request('agent_mobile'),
                \request('operate_mobile'),
                \request('remark'),
                0,
                \request('operate_level1'),
                \request('operate_level2'),
                \request('operate_level3'),
                \request('is_direct_buy')
            );
            $is_direct_buy = \request('is_direct_buy');
            if ($is_direct_buy == 1) {
                //公司直购的话,更新运营中心先后进入顺序
                for ($i = 1; $i <= 3; $i++) {
                    $operate_id = \request('operate_level' . $i);
                    if (!empty($operate_id)) {
                        UsedOperate::updateOrCreate(['level' => $i], ['operate_id' => $operate_id]);
                    }
                }
            }
            $data['is_direct_buy'] = $is_direct_buy;
            return $data;
        });
        return redirect('/pos/pos_list')->with('status', 'pos机新增成功');
    }

    /**
     * pos机编辑提交
     */
    public function pos_edit_submit()
    {
        $Pos = new Pos();
        $Pos->add_pos(\request('id'),
            '',
            \request('pos_type'),
            \request('seller_mobile'),
            \request('agent_mobile'),
            \request('operate_mobile'),
            \request('remark'),
            \request('is_allow_edit'),
            \request('operate_level1'),
            \request('operate_level2'),
            \request('operate_level3'),
            \request('is_direct_buy')
        );
        return pageBackSuccess('pos机编辑成功');
    }

    /**
     * 用户上传xls
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function pos_upload()
    {
        $file = \request()->file('xlsfile');
        if(empty($file)){
            return json_error('文件上传失败');
        }
        // 文件是否上传成功
        if ($file->isValid()) {
            // 获取文件相关信息
            $ext = $file->getClientOriginalExtension();     // 扩展名
            $realPath = $file->getRealPath();   //临时文件的绝对路径
            if (!in_array($ext, ['xls', 'xlsx'])) {
                return json_error('文件类别不合法');
            }
            $data = [];
            Excel::load($realPath, function ($reader) use (&$data) {
                //获取并跳过第一行
                $reader = $reader->getSheet(0);
                $data = $reader->toArray();
                array_shift($data);
            });
            if (empty($data)) {
                return json_error('文件内有效条目为空');
            }
            $pre = (new Pos())->pre_add_pos($data);
            return json_success('OK', $pre);
        } else {
            return json_error('文件上传失败');
        }
    }

    /**
     * POS机批量导入
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pos_import()
    {
        return view('admin.pos.pos_import');
    }

    /**
     * POS机批量导入处理
     */
    public function pos_upload_submit(){
        $count=(new Pos())->handle_pre_add_pos(\request()->input('pre_key'));
        return json_success('本地成功新增'.$count.'台POS机');
    }


    /**
     * ajax获取未绑定的pos机信息
     */
    public function ajax_get_pos_detail(Request $request)
    {
        $terminalId = $request->input('terminalId');
        if (empty($terminalId)) {
            return json_error('请输入终端号');
        }
        $info = Pos::where(['terminalId' => $terminalId])->first();
        if (!isset($info->id)) {
            return json_error('找不到该pos机');
        }
        if (!empty($info->uid)) {
            return json_error('该pos机已绑定用户');
        }
        return json_success('pos机查询成功', $info);
    }


    public function relieve(Request $request)
    {
        $type = $request->input('type');
        $terminalId = $request->input('terminalId');
        switch ($type) {
            case 1;
                //商家
                $field = 'uid';
                break;
            case 2;
                //机主
                $field = 'agent_uid';
                break;
            case 3;
                //运营中心
                $field = 'agent_operate_id';
                break;
        }
        Pos::where(['terminalId' => $terminalId])->update([$field => 0]);
        return json_success('解绑成功');
    }

    public function get_operate()
    {
        $is_direct_buy = \request('is_direct_buy');
        $operate_mobile = \request('operate_mobile');
        if ($is_direct_buy == 1) {
            //直接向公司购买 按时间找
            $data1 = AgentOperate::find_operate_level_by_time();
        } else {
            //不是按等级找
            $operate_info = AgentOperate::where(['mobile' => $operate_mobile])->first();
            if (!isset($operate_info->id)) {
                return json_error('运营中心不存在');
            }
            $data1 = AgentOperate::find_operate_level($operate_info->id);
        }
        foreach ($data1 as $k => $v) {
            $data[$k]['mobile'] = $v['mobile'];
            $data[$k]['id'] = $v['id'];
        }
        return json_success('获取成功', $data);
    }
}
