<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\ApiException;
use App\Http\Requests\Admin\AddAdminRequest;
use App\Http\Requests\Admin\AddRoleRequest;
use App\Http\Requests\Agent\WithdrawRequset;
use App\Http\Requests\UpgradeRequest;
use App\Jobs\handleGhtOrder;
use App\Libraries\Huanxun\Huanxun;
use App\Libraries\JPush\JPush;
use App\Models\AdminBank;
use App\Models\AdminPermission;
use App\Models\AdminRole;
use App\Models\AdminWithdrawLog;
use App\Models\Bank;
use App\Models\Df;
use App\Models\Message;
use App\Models\Order;
use App\Models\Pos;
use App\Models\Seller;
use App\Models\SellerPermission;
use App\Models\SysConfig;
use App\Models\UserWithdraw;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;
use App\Models\Admin;

class ConfigController extends CommonController
{
    /**
     * 系统参数设置页面
     */
    public function set_upgrade() {
//        $data['is_open_upgrade']=SysConfig::attrValue('is_open_upgrade')['attrValue'];
        $SysConfig = new SysConfig();
        $config_name=[
            'is_allow_withdraw',
            'poundage_radio',
            'withdraw_limit_value',
            'withdraw_limit_num',
            'is_allow_back_point',
            'point_deduction_money',
            'credit_day'
        ];
        foreach ($config_name as $k=>$v){
            $data[$v]=$SysConfig->get_value($v);
        }
        return view('admin.config.set_upgrade', ['data' => $data]);
    }

    public function set_upgrade_submit(Request $request){
        $request_info=filter_update_arr($request->all());
        foreach($request_info as $k=>$v){
            SysConfig::where(['attr_name'=>$k])->update(['attr_value'=>$v]);
        }
        return redirect('/config/set_upgrade')->with(['status'=>'配置成功']);
    }

    /**
     *管理员列表
     */
    public function admin_list(){
        $admin_list=Admin::whereNotIn('wish_admin.id',[1])
            ->leftJoin('wish_admin_role','wish_admin_role.id','=','wish_admin.role_id')
            ->select(['wish_admin_role.role_name','wish_admin.*'])
            ->orderBy('wish_admin.id','desc')
            ->paginate(15);
        return view('admin.config.admin_list',['admin_list'=>$admin_list]);
    }

    /**
     *改变管理员状态
     */
    public function change_admin_status(Request $request){
        $request_info=$request->all();
        Admin::where(['id'=>$request_info['id']])->update(['status'=>$request_info['status']]);
        return arr_post(1,'状态改变成功');
    }

    /**
     *添加管理员
     */
    public function admin_add(Request $request){
        //获取角色列表
        $role_list=AdminRole::orderBy('id','desc')->get();
        $id=$request->input('id');
        if(!empty($id)){
            $info=Admin::detail($id);
        }else{
            //初始化字段
            $info=init_table_field('admin');
        }
        return view('admin.config.admin_add',['role_list'=>$role_list,'info'=>$info]);
    }

    /**
     *添加管理员提交
     */
    public function admin_add_submit(Request $request){
        $request_info=$request->all();
        if($request_info['role_id'] == 1){
            return pageBackError('无法设置为总公司');
        }
        $Admin=new Admin();
        $Admin->add_admin($request_info['username'],$request_info['password'],$request_info['role_id'],$request_info['id']);
        $des=empty($request_info['id'])?'发布':'编辑';
        return redirect('/config/admin_list')->with(['status'=>'管理员'.$des.'成功']);
    }

    /**
     *角色列表
     */
    public function role_list(){
        $role_list=AdminRole::with('admin_dividend')->orderBy('id','desc')
            ->paginate(15);
        return view('admin.config.role_list',['role_list'=>$role_list]);
    }

    /**
     *添加角色
     */
    public function role_add(Request $request){
        //
        $AdminPermission=new AdminPermission();
        $permission_list=$AdminPermission->sort_permission();
        $id=$request->input('id');
        if(!empty($id)){
            $info=AdminRole::detail($id);
        }else{
            //初始化字段
            $info=init_table_field('wish_admin_role');
        }
        return view('admin.config.role_add',['permission_list'=>$permission_list,'info'=>$info]);
    }

    /**
     *添加角色提交
     */
    public function role_add_submit(AddRoleRequest $request){
        $request_info=$request->all();
        $permission=implode(',',$request_info['permission']);
        $AdminRole=new AdminRole();
        $return_arr=$AdminRole->add_role($request_info['role_name'],$permission,$request_info['id']);
        $des=empty($request_info['id'])?'添加':'编辑';
        return redirect('/config/role_list')->with(['status'=>'角色'.$des.'成功']);
    }

    /**
     * 管理员提现
     */
    public function withdraw(){
        //获取系统收益
        $Bank=new Bank();
        $purse=$Bank->get_sys_purse(1);
        //获取提现管理员银行卡信息
//        $admin_bank_list=AdminBank::get();
        $Huanxun=new Huanxun();
        $data=$Huanxun->querybankcard([
            'customerCode'=>'13662579183'
        ]);
        if($data){
            $bank_info=(object)[
                'account_bank'=>$data['bankName'],
                'bank_account'=>$data['bankCard'],
                'account_name'=>'李伟裕',
            ];
        }else{
            $bank_info=(object)[];
        }
        return view('admin.config.withdraw',['purse'=>$purse,'bank_info'=>$bank_info]);
    }


    /**
     * 获取银行代码
     * @param Request $request
     * @return mixed
     */
    public function getbankcode(Request $request){
        $data=$request->all();
        $Df=new Df();
        return $Df->getbankcode($data['bank_name']);
    }

    /**
     * 提现申请
     * @param WithdrawRequset $withdrawRequset
     * @return mixed
     */
    public function subwithdraw(Request $withdrawRequset){
//        return back()->withInput($withdrawRequset->all())->withErrors(['error'=>'暂不支持提现']);
        //判断账户的启用状态
        $data=$withdrawRequset->all();
        $Message=new Message();
        $Message->check_sms(config('phone.config_phone')['boss_phone'],$data['code'],17);

        //使用环讯提现
        $amount=$withdrawRequset->input('amount');
//        if($amount>10000){
//            return pageBackError('提现金额不能超过1w');
//        }
        $mobile='13662579183';
        $realName='李伟裕';
        $spare_order_sn=time().rand(1000,9999);
        $d_poundage=3;
        $Huanxun=new Huanxun();
        //转账成功 (扣钱)
        $rs=$Huanxun->transfer([
            'merBillNo'=>$spare_order_sn,
            'customerCode'=>$mobile,
            'transferAmount'=>$amount-2,
            'remark'=>'转账'
        ]);
        if($rs){
            //发起提现(无论成不成都返回成功)
            $Huanxun->withdrawal([
                'merBillNo'=>$spare_order_sn,
                'amount'=>$amount-2,
                'customerCode'=>$mobile,
                'realName'=>$realName,
                's2sUrl'=>'http://www.baidu.com'
            ]);
            //添加提现记录
            $log=[
                'order_sn'=>$spare_order_sn,
                'amount'=>$amount,
                'd_poundage'=>$d_poundage,
                'created_at'=>time2date()
            ];
            AdminWithdrawLog::insertGetId($log);
            return back()->with('status','您的申请已受理，请稍等1-2分钟');
        }else{
            return back()->withErrors(['error'=>$Huanxun->getError()]);
        }


    }

    /**
     * @param Request $request
     * @return array
     * 系统提现发送安全码
     */
    public function send_message(Request $request){
        $send_type=\request('send_type') ?? 1;
        if($send_type==1){
            //市场部
            $mobile=config('phone.config_phone')['boss_phone'];
        }elseif ($send_type==2){
            //技术部
            $mobile=config('phone.config_phone')['technique_phone'];
        }elseif ($send_type=3){
            //夏阳
            $mobile='17771200619';
        }
        $Message=new Message();
        return $Message->send_sms($mobile,17,'',$request->input('account_name'));
    }

    /**
     * @param Request $request
     * @return array
     * 查看商家后台安全码验证
     */
    public function check_safe_code(Request $request){
        $safe_code=$request->input('safe_code');
        $seller_id=$request->input('seller_id');
        $sys_safe_code=SysConfig::attrValue('safe_code')['attrValue'];
        if(md5($safe_code.'seller')!=$sys_safe_code){
            return json_error('安全码不正确');
//            return redirect('/seller/seller_list')->withErrors(['error'=>'安全码不正确']);
        }
        return json_success('安全码正确',['code'=>base64_encode($seller_id)]);
//        echo "<script>window.open('http://".env('APP_SELLER_DOMAIN') . env('APP_DOMAIN')."/nologin?code=".base64_encode($seller_id)."');</script>";
//        return back();
//        return redirect('http://'.env('APP_SELLER_DOMAIN') . env('APP_DOMAIN').'/nologin?code='.base64_encode($seller_id));
    }

    public function print_page(){
        return view('admin.config.print_page');
    }

    public function print_page_submit(){
        $order_sn=\request('order_sn');
        $terminal_id=\request('terminalId');
        $Order=new Order();
        $data=$Order->get_order_print_info($order_sn,2,0);
        $data['status']=2;
        $extras = [
            'data'=> json_encode(['type' => 1,'data' => $data])
        ];
        //商家版
        $JPush=new JPush(2);
        $JPush->push_tags($terminal_id,'订单支付通知','订单支付通知',$extras);
        return pageBackSuccess('通知成功');
    }

    public function create_order(){
        return view('admin.config.create_order');
    }

    public function create_order_submit(){
        if(config('app.env')=='online'){
            return pageBackError('正式环境禁止操作');
        }
        $order_type=\request('order_type');
        $mobile=\request('mobile');
        $invite_mobile=\request('invite_mobile');
        $terminalId=\request('terminalId');
        $pay_type=\request('pay_type');
        $amount=$order_type==2?config('pos.pos.amount'):\request('amount');
        $notify_type=\request('notify_type');
        $pay_method=\request('pay_method');
        $Order=new Order();
        if($order_type==7){
            $uid=Seller::where(['mobile'=>$mobile])->value('id');
            if(empty($uid)){
                return pageBackError('商家不存在');
            }
            //商家充值
            $data=$Order->create_recharge_order($terminalId,$pay_type,$amount,0,$notify_type,$pay_method);
        }elseif($order_type==2){
            //购买pos机订单
            $data=$Order->create_pos_order($mobile,'','',$terminalId,1,'','','',$invite_mobile,'',$pay_type,$amount,1,$notify_type);
        }else{
            //消费买单
            $data=$Order->create_order($mobile,$terminalId,empty($mobile)?3:1,'',$pay_type,$amount,$notify_type,0,0,$pay_method);
        }
        dispatch((new handleGhtOrder($data['order_sn'],$data['pay_type'],'',$amount,2,'',$paytime=date('YmdHis'),'',$notify_type))->onQueue('handleOrder'));
        return pageBackSuccess('处理成功');
    }

    public function handle_order(){
        return view('admin.config.handle_order');
    }

    public function handle_order_submit(Request $request){
        if(config('app.env')=='online'){
            return pageBackError('正式环境禁止操作');
        }
        $order_sn=\request('order_sn');
        $amount=\request('amount');
        $is_update=\request('is_update');
        $is_handle=\request('is_handle');
        $is_left=\request('is_left');
        $order_info=Order::where(['order_sn'=>$order_sn])->first();
        $paytime=date('YmdHis');
        $pay_type=$order_info->pay_type;
        $notify_type=\request('notify_type');
        $Order=new Order();
        if($is_update==1){
            $Order->update_pos_success_order($order_sn,$pay_type,'',$amount,2,'',$paytime,'',$notify_type,'','');
        }
        if($is_handle==1){
            $Order->handle_order_benefit($order_sn,$order_info->type);
        }
        if($is_left==1){
            Order::where(['order_sn'=>$order_sn])->update(['is_left'=>1]);
        }
        return pageBackSuccess('处理成功');
    }


    /**
     * 提现参数设置页面
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function set_withdraw(){
        //代付方式
        $pay_typ = config('pay.withdraw_method');
        //各种类型的代付方式
        $user_type = ['withdraw_1','withdraw_2','withdraw_3','withdraw_5','withdraw_recharge'];
        $user_pay_info = SysConfig::whereIn('attr_name',$user_type) -> get();
        return view('admin.config.set_withdraw', [
            'pay_type' => $pay_typ,
            'user_pay_info' => $user_pay_info,
        ]);
    }

    /**
     * 提交参数设置提交
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function set_withdraw_submit(Request $request){
        $request_info=filter_update_arr($request->all());
        foreach($request_info as $k=>$v){
            SysConfig::where(['attr_name'=>$k])->update(['attr_value'=>$v]);
        }
        return redirect('/config/set_withdraw')->with(['status'=>'配置成功']);
    }

    public function recharge(){
        return view('admin.config.recharge');
    }

    public function recharge_submit(){
        $Message=new Message();
        $Message->check_sms(config('phone.config_phone')['technique_phone'],\request('code'),17);
        $amount=\request('amount');
        $mobile=\request('mobile');
        $owner_type=\request('owner_type');
        $pursetype=\request('pursetype') ?? 7;
//        dd(\request()->all());
        if($owner_type==1){
            $table='user';
        }elseif ($owner_type==2){
            $table='seller';
        }elseif ($owner_type==5){
            $table='agent';
        }elseif ($owner_type==11){
            $table='agent_operate';
        }
        $user_id=DB::table($table)->where(['mobile'=>$mobile])->value('id');
        if(empty($user_id)){
            return pageBackError('用户不存在');
        }
        if($amount<0||(empty($amount))){
            return pageBackError('充值金额不能小于0');
        }
        $Bank=new Bank();
        if($pursetype==1){
            $bank_purse_id=$Bank->get_bank_purse_id();
        }elseif($pursetype==3){
            $bank_purse_id=$Bank->get_sys_purse(3)->purse_id;
        }
        $buyer_purse_id=$Bank->userWallet($user_id,$pursetype,$owner_type)->purse_id;
        $Bank->doApiTransfer($bank_purse_id,$buyer_purse_id,$amount,100000000,
            date('Y/m/d').'号根据财务要求,系统拨款给该商户',
            '系统拨款给该用户');
        return pageBackSuccess('充值成功');
    }

    public function issue_withdraw(){
        return view('admin.config.issue_withdraw');
    }

    public function issue_withdraw_submit(){
        $Message=new Message();
        $Message->check_sms(config('phone.config_phone')['technique_phone'],\request('code'),17);
        $UserWithdraw=new UserWithdraw();
        $UserWithdraw->issue_withdraw(\request('spare_order_sn'));
        return pageBackSuccess('处理成功');
    }
}
