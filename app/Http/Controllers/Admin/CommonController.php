<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Libraries\gaohuitong\Ruzhu;
use App\Models\Message;
use Illuminate\Support\Facades\Input;

class CommonController extends Controller
{
    public function __construct(){
    }

    //图片上传
//    public function upload($img_name,$path_name){
//        $file = Input::file($img_name);
//        if(empty($file)){
//            return null;
//        }
//        if($file -> isValid()){
//            $entension = $file -> getClientOriginalExtension(); //上传文件的后缀.
//            $newName = date('YmdHis').mt_rand(100,999).'.'.$entension;
//            $y=date('Y');
//            $m=date('m');
//            $d=date('d');
//            $path_url='/upload/'.$path_name.'/'.$y.'/'.$m.'/'.$d;
//            $path = $file -> move(base_path().'/public'.$path_url,$newName);
//            $filepath = $path_url.'/'.$newName;
//            return $filepath;
//        }
//    }

    /**
     * 上传图片
     * @param $imagebase64 base64格式的图片数据
     * @return mixed
     */
//    public function upload($imagebase64){
//        $size = strlen(file_get_contents($imagebase64))/1024;
//        if($size>1024){
//            return arr_post(0,'请上传不超过1M的图片');
//        }
//        $filebase64=$imagebase64;
//        $arr['img']=$filebase64;
//        $arr['AppId']=env('APPID');
//        $arr['SafeCode']=env('SAFECODE');
//        $result=json_decode(curl_post(env('IMGURL'),$arr),'true');
//        if($result['code']==0){
//            new_logger('image.log','返回的图片信息',$result);
//            return arr_post(0,$result['message']);
//        }else{
//            new_logger('image.log','返回的图片信息',$result);
//            return arr_post(1,'上传成功',['filepath'=>$result['data']['file']]);
//        }
//    }

    /**
     * 编辑高汇通入驻商家基本信息
     * @param $request_info
     * @return mixed
     */
    public function edit_selller_basic($request_info){
        $param = [
            'handleType'			=> $request_info['handleType'],
            'merchantName'			=> $request_info['merchantName'],	// 店名，要与营业执照上一致
            'shortName'				=> $request_info['shortName'],
            'city'					=> $request_info['city'],		// 城市id
            'merchantAddress'		=> $request_info['merchantAddress'],	// 城市中文名
            'servicePhone'			=> $request_info['servicePhone'],			// 客服电话
            'merchantType'			=> $request_info['merchantType'],		// 00公司，01个体
//            'merchantType'			=> '01',		// 00公司，01个体
            'category'				=> $request_info['category'],		// 类别id
            'corpmanName'			=> $request_info['corpmanName'],		// 法人姓名
            'corpmanId'				=> $request_info['corpmanId'],	// 法人身份证
            'corpmanMobile'			=> $request_info['corpmanMobile'],	// 法人联系手机
            'bankCode'				=> $request_info['bankCode'],
            'bankName'				=> $request_info['bankName'],
            'bankaccountNo'			=> $request_info['bankaccountNo'],
            'bankaccountName'		=> $request_info['bankaccountName'],
            'autoCus'				=> 0,		// 0：不自动提现 1：自动提现
            'remark'				=> $request_info['remark'],
        ];
        $Ruzhu=new Ruzhu();
        $result=$Ruzhu->basic($param);
        new_logger('ruzhubasic.log','请求的基本信息',[$param]);
        new_logger('ruzhubasic.log','返回的结果',[$result]);
        if ($result['head']['respCode']!='000000'){
            return arr_post(0,$result['head']['respMsg']);
        }else{
            return arr_post(1,$result['head']['respMsg']);
        }
        
        
    }

    public function upload_img(){
        return upload(request('imagebase64'));
    }

    /**
     * @return array
     * 发送短信
     */
    public function send_sms(){
        $Message=new Message();
        return $Message->send_sms(request('mobile'),request('type'),request('remark'));
    }
}
