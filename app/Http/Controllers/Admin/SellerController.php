<?php

namespace App\Http\Controllers\Admin;


    use App\Exceptions\ApiException;
    use App\Http\Requests\Admin\AddAgentRequest;
    use App\Http\Requests\Admin\AddOperateRequest;
    use App\Http\Requests\Admin\AddSeller1Request;
    use App\Http\Requests\Admin\AddSeller2Request;
    use App\Http\Requests\Admin\AddSeller3Request;
    use App\Http\Requests\Admin\AddSellerRequest;
    use App\Http\Requests\Admin\EditSeller1Request;
    use App\Http\Requests\Admin\EditSeller2Request;
    use App\Http\Requests\Admin\EditSeller3Request;
    use App\Http\Requests\Admin\WithdrawBankRequest;
    use App\Jobs\SevenToHalf;
    use App\Jobs\Test;
    use App\Libraries\gaohuitong\Ruzhu;
    use App\Models\Admin;
    use App\Models\AdminRole;
    use App\Models\Agent;
    use App\Models\AgentOperate;
    use App\Models\AgentSevenToHalf;
    use App\Models\Bank;
    use App\Models\BankList;
    use App\Models\Df;
    use App\Models\FilialeOperate;
    use App\Models\JavaIncrement;
    use App\Models\MerchantApply;
    use App\Models\Message;
    use App\Models\Order;
    use App\Models\Pos;
    use App\Models\PushQrQrcode;
    use App\Models\RuzhuMerchantBank;
    use App\Models\RuzhuMerchantBankCopy;
    use App\Models\RuzhuMerchantBasic;
    use App\Models\RuzhuMerchantBasicCopy;
    use App\Models\RuzhuMerchantBusinesss;
    use App\Models\RuzhuMerchantBusinesssCopy;
    use App\Models\Seller;
    use App\Models\ShopTSsoSession;
    use App\Models\Statement;
    use App\Models\SysConfig;
    use App\Models\Transfer;
    use App\Models\User;
    use App\Models\UserBank;
    use App\Models\UserFreezeLog;
    use App\Models\VTransfer;
    use App\Models\ProfitRelation;
    use App\Models\WishSellerInfo;
    use App\Models\YixiantdzdMerchants;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Storage;

class SellerController extends CommonController{

    /**
     * 商家管理列表
     */
    public function seller_list(Request $request){
        //商家手机号
        $mobile=$request->input('mobile');
        $apply_status=$request->input('apply_status');
        $name=$request->input('name');
        $agent_mobile=$request->input('agent_mobile');

        $seller_list=Seller::where(function ($query) use ($mobile,$apply_status,$name,$agent_mobile){
            if(!empty($mobile)){
                $query->where('seller.mobile','like','%'.$mobile.'%');
            }
            if(!empty($apply_status)){
                if($apply_status!=1){
                    $query->where(['a1.status'=>$apply_status]);
                }else{
                    $query->where('a1.id','<>',0);
                }
            }
            if(!empty($name)){
                $query->where('a1.merchantName','like','%'.$name.'%');

            }
            if(!empty($agent_mobile)){
                $query->where('a2.mobile','like','%'.$agent_mobile.'%');

            }
            return $query;
        })
            ->leftJoin('wish_ruzhu_merchant_basic_copy as a1','wish_seller.id','=','a1.uid')
            ->leftJoin('wish_agent as a2','wish_seller.inviter_id','=','a2.id')
            ->leftJoin('wish_pushqr_user as a3','wish_seller.qr_user_id','=','a3.id')
            ->rightJoin('wish_profit_relation as a4','wish_seller.id','=','a4.seller_id')
            ->where([
                'a4.identity_type'=>ProfitRelation::IDENTITY_TYPE_SELLER
            ])
            ->select(['wish_seller.*',
                'a1.corpmanName',
                'a1.status as apply_status',
                'a1.merchantName'
                ,'a1.id as basic_id',
                'a2.mobile as agent_mobile',
                'a3.mobile as pushQr_mobile'
            ])
            ->orderBy('id','desc')
            ->paginate(15);
        return view('admin.seller.seller_list',['seller_list'=>$seller_list]);
    }



    /**
     * 冻结/解冻商户
     */
    public function change_status(Request $request){
        $request_info=$request->all();
        //获取配置费率的类型 1、用户 2、商户 3、机主
        $type=$request_info['type'];
        if($type==1){
            User::where(['id'=>$request_info['id']])->update(['status'=>$request_info['status']]);
        }elseif($type==2){
            Seller::where(['id'=>$request_info['id']])->update(['status'=>$request_info['status']]);
        }elseif($type==3){
            Agent::where(['id'=>$request_info['id']])->update(['status'=>$request_info['status']]);
        }
        return arr_post(1,'状态改变成功');
    }

    /**
     * 财务列表
     * @param $type   钱包类型
     * @param $user_id  用户id
     * @param $year   财务年份
     * @param $user_type  用户类型 1、用户 2、 商家 3、机主 4、运营中心
     * @return mixed
     */

    public function finance(Request $request){
        //币种类型
        $type=$request->input('type');
        $user_id=$request->input('id');
        //用户类型
        $user_type=$request->input('user_type');
        //根据钱包类型获取用户类型
        if($user_type==1){
            $owner_type=1;
            //用户信息
            $data['user_info']=$user_info=User::where(['id'=>$user_id])->first();
            $nodeid=$user_info->nodeid;
        }elseif($user_type==2){
            $owner_type=2;
            //商家信息
            $data['user_info']=$seller_info=Seller::where(['id'=>$user_id])->first();
            $nodeid=User::where(['mobile'=>$seller_info->mobile])->value('nodeid');
        }elseif($user_type==3){
            $owner_type=5;
            //机主信息
            $data['user_info']=Agent::where(['id'=>$user_id])->first();
        }elseif($user_type==4){
            $owner_type=11;
            //运营中心信息
            $data['user_info']=AgentOperate::where(['id'=>$user_id])->first();
        }


        //获取用户注册值钱包
        $Bank=new Bank();
        //根据币种类型获取相应的红包id
        $purse_id=$Bank->userWallet($user_id,$type,$owner_type)->purse_id;
        //0全部 1收入 2支出
        $status=$request->input('status');
        $Transfer=new Transfer();
        //总收入
        $data['total_into']=get_last_two_num($Transfer->get_total_by_purse_id($purse_id,'into'));
        //总支出
        $data['total_out']=get_last_two_num($Transfer->get_total_by_purse_id($purse_id,'out'));
        //不使用视图 用原生检索后合并加快速度
        $sql='';
        for ($i=2017;$i<=date('Y');$i++){
            $sql .= "select * from wish_transfer_".$i;
            if($status==0){
                $sql.=" where into_purse_id=".$purse_id." or out_purse_id=".$purse_id." ";
            }elseif($status==1){
                $sql.=" where into_purse_id=".$purse_id." ";
            }elseif($status==2){
                $sql.=" where out_purse_id=".$purse_id." ";
            }
            if ($i != date('Y')){
                $sql = $sql." union ";
            }
        }

        $transfer=DB::table(DB::raw("(".$sql.") as t"))
            ->orderBy('t.create_time','desc')
            ->orderBy('t.transfer_id','desc')
            ->paginate(15);

        //字段处理
        foreach($transfer as $k=>$v){
            if($v->into_owner_id==$user_id){
                $transfer[$k]->member_id=get_owner_mobile($v->out_owner_type,$v->out_owner_id);
            }else{
                $transfer[$k]->member_id=get_owner_mobile($v->into_owner_type,$v->into_owner_id);
            }
        }
        //钱包信息

        $JavaIncrement=new JavaIncrement();
        //用户获取TD积分
        if($user_type==1||$user_type==2){
            //积分钱包
            $data['j_purse_info']=$Bank->userWallet($user_id,3,$owner_type);
            //TD钱包
            $data['t_purse_info']['balance']=0;
            $data['t_purse_info']['freeze_value']=0;
            if(!empty($nodeid)){
                switch ($user_type){
                    case 1:
                        $currency=101;
                        break;
                    case 2:
                        $currency=102;
                        break;
                }
                $param=[
                    'currency'=>$currency,
                    'u_id'=>$nodeid
                ];
                $info=$JavaIncrement->change_over('api/json/user/account/getbalance.do',$param,'get');
                $data['t_purse_info']['balance']=$info['data'][$currency]['balance'];
                $data['t_purse_info']['freeze_value']=$info['data'][$currency]['frozen'];

            }
            if($user_type==1){
                //获取用户区块链地址
                $param=[
                    'ticket'=>ShopTSsoSession::where(['node_id'=>$nodeid])->value('ticket'),
                    'version'=>'01',
                    'serviceTag'=>'TD_A',
                    'currency'=>'101'
                ];
                $data['block_address']=$JavaIncrement->change_over('api/json/currency/getNewAddress.do',$param,'post')['data'];
                //用户红包收益
                $data['r_purse_info']=$Bank->userWallet($user_id,12,$owner_type);
            }
            if($user_type==2){
                //商家充值钱包
                $data['b_purse_info']=$Bank->userWallet($user_id,7,$owner_type);
                //商家货款钱包
                $data['h_purse_info']=$Bank->userWallet($user_id,4,$owner_type);
                //商家待补贴积分
                $data['de_purse_info']=$Bank->userWallet($user_id,10001,$owner_type);
            }
        }

        //消费积分
        $data['cost_purse_info']=$Bank->userWallet($user_id,10,$owner_type);
        //收益钱包
        $data['x_purse_info']=$Bank->userWallet($user_id,1,$owner_type);


        $data['purse_id']=$purse_id;
        return view('admin.seller.finance',['transfer'=>$transfer,'data'=>$data]);
    }


    /**
     * 入驻申请编辑页面
     */
    public function edit_apply(Request $request){
        $id=$request->input('uid');
        $bank_list=BankList::get();
        $info=MerchantApply::where(['uid'=>$id])
            ->leftJoin('seller','seller.id','=','merchant_apply.uid')
            ->select(['merchant_apply.*','seller.mobile'])
            ->first();
        $bank_info=UserBank::where(['uid'=>$id,'type'=>2])->first();
        return view('admin.seller.edit_apply',['info'=>$info,'bank_info'=>$bank_info,'bank_list'=>$bank_list]);
    }

    /**
     * 入驻申请编辑提交
     */
    public function edit_apply_submit(Request $request){
        $request_info=$request->all();
        //修改银行卡
        $bank['account_name']=$request_info['account_name'];
        $bank['account_bank']=$request_info['account_bank'];
        $bank['bank_account']=$request_info['bank_account'];
        $bank['ibankno']=$request_info['ibankno'];
        UserBank::where(['uid'=>$request->input('uid'),'type'=>2])->update($bank);

        if(!empty($request_info['businessimagebase64'])){
            $size = strlen(file_get_contents($request_info['businessimagebase64']))/1024;
            if($size>1024){
                return back()->withInput($request->all())->withErrors(['error'=>'请上传不超过1M的图片']);
            }
            $filebase64=$request_info['businessimagebase64'];
            $arr['img']=$filebase64;
            $arr['AppId']=env('APPID');
            $arr['SafeCode']=env('SAFECODE');
            $result=json_decode(curl_post(env('IMGURL'),$arr),'true');
            if($result['code']==0){
                return back()->withInput($request->all())->withErrors(['error'=>$result['message']]);
            }else{
                $request_info['businessimage']=$result['data']['file'];
            }
        }
        unset($request_info['businessimagebase64']);
        unset($request_info['old_businessimage']);
        unset($request_info['ibankno']);
        unset($request_info['bank_account']);
        unset($request_info['account_bank']);
        MerchantApply::where(['id'=>$request->input('id')])->update(filter_update_arr($request_info));
        return back()->with(['status'=>'编辑成功']);
    }

    /**
     * 入驻申请审核页面
     */
    public function verity_apply(Request $request){
        $id=$request->input('uid');
        $info=MerchantApply::where(['uid'=>$id])
            ->leftJoin('seller','seller.id','=','merchant_apply.uid')
            ->select(['merchant_apply.*','seller.mobile'])
            ->first();
        return view('admin.seller.verity_apply',['info'=>$info]);
    }

    /**
     * 入驻申请审核提交
     */
    public function verity_apply_submit(Request $request){
        $request_info=$request->all();
        $uid=$request_info['uid'];
        unset($request_info['uid']);
        //获取所属pos机
        $r1=Pos::where(['uid'=>$uid])->update(['uid'=>0]);
        if(!empty($request_info['terminalId'])){
            $r2=Pos::whereIn('terminalId',$request_info['terminalId'])->update(['uid'=>$uid,'bind_time'=>time2date()]);
            unset($request_info['terminalId']);
        }

        $request_info['admin_id']=session('admin_info')->id;
        $request_info['vertify_time']=time2date();
        $rs=MerchantApply::where(['id'=>$request->input('id')])->update(filter_update_arr($request_info));
        return back()->with(['status'=>'处理成功']);
    }

    /**
     * 配置费率页面
     */
    public function edit_fee(Request $request){
        $id=$request->input('uid');
        //获取配置的类型 1、用户 2、商户 3、机主
        $type=$request->input('type');
        if ($type==1){
            //用户的配置信息
            $info=User::where(['id'=>$id])->first();
            return view('admin.seller.user_edit_fee',['info'=>$info]);
        }elseif ($type==2){
            //商户的配置信息
            $info=Seller::where(['id'=>$id])->first();
//            $info=MerchantApply::where(['uid'=>$id])
//                ->leftJoin('seller','seller.id','=','merchant_apply.uid')
//                ->select(['merchant_apply.seller_fee','merchant_apply.uid','seller.mobile','seller.is_allow_withdraw','seller.is_allow_back_point'])
//                ->first();
            return view('admin.seller.edit_fee',['info'=>$info]);
        }elseif ($type==3){
            //机主的配置信息
            $info=Agent::where(['id'=>$id])->first();
            return view('admin.seller.agent_edit_fee',['info'=>$info]);
        }

    }

    /**
     * 配置费率编辑提交
     */
    public function edit_fee_submit(Request $request){
        $request_info=$request->all();
        $uid=$request_info['uid'];
        //修改配置的类型 1、用户 2、商户 3、机主
        $type=$request->input('type');
        if ($type==1){
            //用户的配置信息
            User::where(['id'=>$uid])->update(['is_allow_withdraw'=>$request_info['is_allow_withdraw'],'is_allow_back_point'=>$request_info['is_allow_back_point']]);
            return back()->with(['status'=>'配置成功']);
        }elseif ($type==2){
            //商户的配置信息
            Seller::where(['id'=>$uid])->update(['is_allow_withdraw'=>$request_info['is_allow_withdraw'],'is_allow_back_point'=>$request_info['is_allow_back_point']]);
            return back()->with(['status'=>'配置成功']);
        }elseif ($type==3){
            //机主的配置信息
            Agent::where(['id'=>$uid])->update(['is_allow_withdraw'=>$request_info['is_allow_withdraw']]);
            return back()->with(['status'=>'配置成功']);
        }

    }

    /**
     * 用户管理列表
     */
    public function user_list(Request $request){
        //用户手机号
        $mobile=$request->input('mobile');
        $nodeid=$request->input('nodeid');
        $user_list=User::where(function ($query) use ($mobile,$nodeid){
            if(!empty($mobile)){
                $query->where('user.mobile','like','%'.$mobile.'%');
            }
            if(!empty($nodeid)){
                $query->where(['user.nodeid'=>$nodeid]);
            }
            return $query;
        })
            ->leftJoin(DB::raw('(select * from user_bank where type=1 and isdefault=1) as b'),'b.uid','=','user.id')
            ->where(DB::raw('left(mobile,1)'),'=','1')
            ->select(['b.account_name','user.*'])
            ->orderBy('id','desc')
            ->paginate(15);
        return view('admin.seller.user_list',['user_list'=>$user_list]);
    }



    /**
     * 机主管理列表
     */
    public function agent_list(Request $request){
        //机主手机号
        $mobile=$request->input('mobile');
        //推荐机主
        $inviter_mobile=$request->input('inviter_mobile');
        //所属机主
        $operate_mobile=$request->input('operate_mobile');
        $agent_list=Agent::where(function ($query) use ($mobile,$inviter_mobile,$operate_mobile){
            if(!empty($mobile)){
                $query->where('agent.mobile','like','%'.$mobile.'%');
            }
            if(!empty($inviter_mobile)){
                $query->where('a1.mobile','like','%'.$inviter_mobile.'%');
            }
            if(!empty($operate_mobile)){
                $query->where('a2.mobile','like','%'.$operate_mobile.'%');
            }
            return $query;
        })
            ->leftJoin(DB::raw('(select * from wish_user_bank where type=3 and isdefault=1) as b'),'b.uid','=','wish_agent.id')
            ->leftJoin('wish_agent as a1','a1.id','=','wish_agent.inviter_id')
            ->leftJoin('wish_agent_operate as a2','a2.id','=','wish_agent.operate_id')
            ->rightJoin('wish_profit_relation as wpr','a1.id','=','wpr.seller_reco_id')
            ->where([
                'wpr.identity_type'=>ProfitRelation::IDENTITY_TYPE_AGENT
            ])
            ->select(['wish_agent.*','a1.mobile as inviter_mobile','a2.mobile as operate_mobile','a2.remarks as operate_remarks','b.account_name'])
            ->orderBy('id','desc')
            ->paginate(15);
        return view('admin.seller.agent_list',['agent_list'=>$agent_list]);
    }

    /**
     * 商家入驻页面
     * @return mixed
     */
    public function seller_add(){
        //银行列表
        $bank_list=BankList::get();
        //城市列表
        $city_arr = get_ght_attr('city');
        //经营类别列表
        $category_arr = get_ght_attr('category');
        //去掉名字中的英文名
        foreach ($category_arr as $value){
            $arr = explode(" ",$value->categoryName);
            $value->categoryName = $arr[1];
        }
        //银行代码列表
        $bank_arr = array();
        $bank_arr1 = get_ght_attr('bank');
        foreach ($bank_arr1 as $item => $value){
            foreach ($bank_list as $k => $v){
                if ($v['bank_name'] == $value->bankName){
                    $bank_arr[$item]->bankCode =  $value->bankCode;
                    $bank_arr[$item]->bankName =  $value->bankName;
                }
            }
        }
        return view('admin.seller.seller_add',compact('bank_list','city_arr','category_arr','bank_arr'));
    }

    /** 新增商家提交
     * @param AddSellerRequest $addSellerRequest
     * @return \Illuminate\Http\RedirectResponse
     * @throws ApiException
     */
    public function seller_add_submit(AddSellerRequest $addSellerRequest){
        //登记商家入驻信息
        $Basic=new RuzhuMerchantBasicCopy();
        $Basic->add_seller($addSellerRequest->all(),1,\request('invite_mobile'));
        return pageBackSuccess('商家入驻成功');
    }


    /**
     * 新增机主页面
     * @return mixed
     */
    public function agent_add(){
        return view('admin.seller.agent_add',compact('info'));
    }


    /**
     * 新增机主
     * @param AddAgentRequest $addAgentRequest
     * @return mixed
     */
    public function agent_add_submit(AddAgentRequest $addAgentRequest){
        $id=\request('id');
        DB::transaction(function () use($id){
            $mobile=\request('mobile');
            $invite_mobile=\request('invite_mobile');
            $operate_mobile=\request('operate_mobile');
            $remark_name=\request('remark_name');
            $Agent=new Agent();
            $Agent->add_agent($id,$mobile,$invite_mobile,$operate_mobile,$remark_name,0);

        });
        $back_message=empty($id)?'机主新增成功':'机主编辑成功';
        return pageBackSuccess($back_message);
    }

    /**
     * 获取银行代码
     * @param Request $request
     * @return mixed
     */
    public function getbankcode(Request $request){
        $data=$request->all();
        $Df=new Df();
        return $Df->getbankcode($data['bank_name']);
    }

    /**
     * 返现对账单列表
     * @param Request $request
     * @return mixed
     */
    public function statement_list(Request $request){
        $seller_id=$request->input('id');
        //获取商家信息
        $seller_info=Seller::where(['id'=>$seller_id])->first();
        //获取该商家的返现列表
        $statement_list=Statement::where(['seller_uid'=>$seller_id])
            ->orderBy('id','desc')
            ->paginate(15);
        return view('admin.seller.statement_list',['statement_list'=>$statement_list,'seller_info'=>$seller_info]);

    }

    /**
     * 返现记录详情
     * @param Request $request
     * @return mixed
     */
    public function statement_details(Request $request){
        $statement_id=$request->input('id');
        //返现记录信息
        $statement_info=Statement::where(['id'=>$statement_id])->first();
        //归属商家信息
        $seller_info=Seller::where(['id'=>$statement_info['seller_uid']])->first();
        //归属订单号
        $arr = explode(",",$statement_info['order_list']);
        //根据返现记录的创建时间获取年份
        if(date('m-d',strtotime($statement_info['created_at']))=='01-01'){
            //如果今天刚好是1月1日则选择去年的表
            $year=date('Y',strtotime($statement_info['created_at']))-1;
        }else{
            //若不是则选择今年的表
            $year=date('Y',strtotime($statement_info['created_at']));
        }
        //根据年份查询相应的表格得出订单列表
        $Order=new Order($year);

        $order_list =$Order->whereIn ('order_sn',$arr)
            ->orderBy('id','desc')
            ->paginate(15);
        return view('admin.seller.statement_detail',compact('statement_info','seller_info','order_list'));
    }

    /**
     * 返现记录详情
     * @param Request $request
     * @return mixed
     */
    public function statement_finance_details(Request $request){
        $statement_id=$request->input('id');
        //返现记录信息
        $statement_info=Statement::where(['id'=>$statement_id])->first();
        //归属商家信息
        $seller_info=Seller::where(['id'=>$statement_info['seller_uid']])->first();
        $seller_id=$statement_info['seller_uid'];
        $Bank=new Bank();
        //获取商家积分和收益钱包
        $purse_id=$Bank->userWallet($seller_id,1,2)->purse_id;
        $point_purse_id=$Bank->userWallet($seller_id,3,2)->purse_id;
        $order_list=VTransfer::whereIn('into_purse_id',[$purse_id,$point_purse_id])
            ->whereIn('reason',[10021,10022])
            ->where('detail','like','%'.'对账单编号:'.$statement_id.'%')
            ->orderBy('create_time','desc')
            ->orderBy('transfer_id','desc')
            ->paginate(15);


        return view('admin.seller.statement_finance_detail',compact('statement_info','seller_info','order_list'));
    }

    /**
     * 得到银行联行号
     * @param Request $request
     * @return mixed
     */
    public function get_bankNo(Request $request){
        $bank_name=$request->input('bank_name');
        $info=BankList::where('bank_name','like','%'.$bank_name.'%')->first(['lbnkNo','bank_name']);
        if(empty($info->lbnkNo)){
            return json_error('找不到相应的银行信息');
        }else{
            return json_success('银行信息获取成功',$info);
        }
    }


    /**
     * 高汇通商家入驻
     */
    public function ght_seller_add(Request $request){
        $Ruzhu=new Ruzhu();
        $bank_list=BankList::get();
        $city_arr = get_ght_attr('city');
        $category_arr = get_ght_attr('category');
        $bank_arr = get_ght_attr('bank');
        $uid=$request->input('uid');
        //获取附表信息
        //获取用户信息
        $seller_info=Seller::where(['id'=>$uid])->first();
        //获取商家附表基本信息
        $basic_info=RuzhuMerchantBasicCopy::where(['uid'=>$uid,'merchantId'=>$seller_info->child_merchant_no])->first();
        //获取商家附表银行卡信息
        $bank_info=RuzhuMerchantBankCopy::where(['merchantId'=>$seller_info->child_merchant_no])->first();
        //获取商家的附表业务信息
        $busi_info=RuzhuMerchantBusinesssCopy::where(['merchantId'=>$seller_info->child_merchant_no])->first();

        //获取主表信息用于判断是否显示按钮
        $real_basic=RuzhuMerchantBasic::where(['uid'=>$uid,'merchantId'=>$seller_info->child_merchant_no])->first(['id']);
        $real_bank=RuzhuMerchantBank::where(['merchantId'=>$seller_info->child_merchant_no])->first(['id']);
        $real_busi=RuzhuMerchantBusinesss::where(['merchantId'=>$seller_info->child_merchant_no])->first(['id']);
        $button_judge['real_basic']=isset($real_basic->id)?1:0;
        $button_judge['real_bank']=isset($real_bank->id)?1:0;
        $button_judge['real_busi']=isset($real_busi->id)?1:0;
        //获取亿贤商家信息
        $yixian_info = YixiantdzdMerchants::where(['merchantNo' => $seller_info->child_merchant_no]) -> first();
        return view('admin.seller.ght_seller_add',compact('bank_list','city_arr','category_arr','seller_info','bank_arr','basic_info','bank_info','busi_info','button_judge','yixian_info'));
    }

    /**
     * @param AddSeller1Request $addSeller1Request
     * @return $this|\Illuminate\Http\RedirectResponse
     * 修改商家附表信息第一步
     */
    public function ght_seller_edit_copy_step1(AddSeller1Request $addSeller1Request){
        $request_info=$addSeller1Request->all();
        $basic_id=$request_info['basic_id'];
        //判断是不是直辖市的郊区,选择区域编号
        if (empty($request_info['district10'])){
            $request_info['area_id'] = $request_info['city_code'];
        }else{
            $request_info['area_id'] = $request_info['district_code'];
        }
        //检查是否上传图片并返回图片地址
        if(!empty($request_info['businessimagebase64'])){
            $result=upload($request_info['businessimagebase64']);
            if($result['code']==0){
                return back()->withInput($addSeller1Request->all())->withErrors(['error'=>$result['message']]);
            }else{
                $request_info['businessimage']=$result['data']['filepath'];
            }
        }
        unset($request_info['basic_id'],$request_info['mobile'],$request_info['old_businessimage'],$request_info['businessimagebase64'],$request_info['province10'],$request_info['province_code'],$request_info['city10'],$request_info['city_code'],$request_info['district10'],$request_info['district_code'],$request_info['cityName1']);
        $request_info['updated_at'] = time2date();
        RuzhuMerchantBasicCopy::where(['id'=>$basic_id])->update(filter_update_arr($request_info));
        return back()->with('status','商家基础登录信息修改成功');

    }

    /**
     * @param AddSeller2Request $addSeller2Request
     * @return \Illuminate\Http\RedirectResponse
     * 修改商家附表信息第二步
     */
    public function ght_seller_edit_copy_step2(AddSeller2Request $addSeller2Request){
        $request_info=$addSeller2Request->all();
        $basic_id=$request_info['basic_id'];
        $request_info['bankName']=$request_info['bankName1'];
        $request_info['bankCode']=$request_info['bankCode1'];
        $request_info['bankbranchNo']=$request_info['ibankno'];
        unset($request_info['basic_id'],$request_info['mobile'],$request_info['uid'],$request_info['bankName1'],$request_info['bankCode1'],$request_info['account_bank'],$request_info['ibankno']);
        $request_info['updated_at'] = time2date();
        RuzhuMerchantBankCopy::where(['basic_id'=>$basic_id])->update(filter_update_arr($request_info));
        return back()->with('status','商家银行卡信息修改成功');
    }

    /**
     * @param AddSeller3Request $addSeller3Request
     * @return \Illuminate\Http\RedirectResponse
     * 修改商家附表信息第三步
     */
    public function ght_seller_edit_copy_step3(AddSeller3Request $addSeller3Request){
        $request_info=$addSeller3Request->all();
        $basic_id=$request_info['basic_id'];
        $request_info['futureRateValue']= $request_info['futureRate'];
        unset($request_info['basic_id'],$request_info['mobile'],$request_info['uid'],$request_info['futureRate']);
        $request_info['updated_at'] = time2date();
        RuzhuMerchantBusinesssCopy::where(['basic_id'=>$basic_id])->update(filter_update_arr($request_info));
        return back()->with('status','商家开通平台支付业务修改成功');
    }

    /**
     * 登记商家入驻基本信息
     * @param AddSeller1Request $addSeller1Request
     * @return mixed
     */
    public function ght_seller_add_submit_step1(AddSeller1Request $addSeller1Request){
        $request_info=$addSeller1Request->all();
        $mobile=$request_info['mobile'];
        DB::beginTransaction();


        //判断该身份下是否已存在
        $seller_info=Seller::where(['mobile'=>$mobile])->first(['id']);

        if(!isset($seller_info->id)){
            return back()->withInput($addSeller1Request->all())->withErrors(['error'=>'该手机号还没注册商家，请先去填写商家基本信息进行登记信息']);
        }
        $user_id=$seller_info->id;

        //向高汇通申请商家入驻信息
        $request_info['handleType']=0;
        $request_info['remark']='商家入驻申请';
//        $result=$this->edit_selller_basic($request_info);
//        if ($result['code']=='0'){
//            return back()->withInput($addSeller1Request->all())->withErrors(['error'=>$result['message']]);
//        }
        unset($request_info['handleType'],$request_info['remark']);
        //判断是不是直辖市的郊区,选择区域编号
        if (empty($request_info['district10'])){
            $area_id = $request_info['city_code'];
        }else{
            $area_id = $request_info['district_code'];
        }

        $basic = [
            'merchantName'			=> $request_info['merchantName'],	// 店名，要与营业执照上一致
            'shortName'				=> $request_info['shortName'],
            'city'					=> $request_info['city'],		// 城市id
            'merchantAddress'		=> $request_info['merchantAddress'],	// 城市中文名
            'servicePhone'			=> $request_info['servicePhone'],			// 客服电话
            'merchantType'			=> $request_info['merchantType'],		// 00公司，01个体
            'category'				=> $request_info['category'],		// 类别id
            'corpmanName'			=> $request_info['corpmanName'],		// 法人姓名
            'corpmanId'				=> $request_info['corpmanId'],	// 法人身份证
            'corpmanMobile'			=> $request_info['corpmanMobile'],	// 法人联系手机
            'bankCode'				=> $request_info['bankCode'],
            'bankName'				=> $request_info['bankName'],
            'bankaccountNo'			=> $request_info['bankaccountNo'],
            'bankaccountName'		=> $request_info['bankaccountName'],
            'businessimage'		=> $request_info['businessimage'],
            'businessLicense'		=> $request_info['businessLicense'],
            'autoCus'				=> 0,		// 0：不自动提现 1：自动提现
            'remark'				=> '商家入驻申请',
            'uid'				=> $user_id,
            'merchantId'				=> $request_info['corpmanMobile'],
            'status'				=> 2,
            'area_id'				=> $area_id,
            'lat'				=> $request_info['lat'],  //纬度
            'lng'				=> $request_info['lng'],  //经度
            'create_time'		=> time2date(),
            'volume'		=> rand(100,300),
            'average'		=> rand(20,80),
        ];
        //商家基本信息登记表插入数据
        $rs[]=RuzhuMerchantBasic::insert(filter_update_arr($basic));
        if(checkTrans($rs)){
            DB::commit();
            return back()->with('status','商家入驻成功,请尽快去登记银行卡信息！');

        }else{
            DB::rollBack();
            return back()->withInput($addSeller1Request->all())->withErrors(['error'=>'商家入驻失败']);
        }
    }


    /**
     * 登记商家银行卡信息
     * @param AddSeller2Request $addSeller2Request
     * @return mixed
     */
    public function ght_seller_add_submit_step2(AddSeller2Request $addSeller2Request){
        $request_info=$addSeller2Request->all();
        $mobile=$request_info['mobile'];
        DB::beginTransaction();
        //判断该身份下是否已存在
        $user_info=Seller::where(['mobile'=>$mobile])->first(['id']);
        if(!isset($user_info->id)){
            return back()->withInput($addSeller2Request->all())->withErrors(['error'=>'该手机号还没注册商家，请先去填写商家基本信息进行登记信息']);
        }
        //判断该用户是否登记了基本信息
        $basic_info=RuzhuMerchantBasic::where(['merchantId'=>$addSeller2Request['merchantId'],'uid'=>$user_info->id])->first();
        if(!isset($basic_info)){
            return back()->withInput($addSeller2Request->all())->withErrors(['error'=>'请先登记基本信息后，才能登记银行卡信息']);
        }
        //判断持卡人是否与法人相同
        if($basic_info->corpmanName!=$addSeller2Request['name']){
            return back()->withInput($addSeller2Request->all())->withErrors(['error'=>'持卡人必须与法人相同']);
        }
        //判断持卡人的身份证号是否与法人的身份证号相同
        if($basic_info->corpmanId!=$addSeller2Request['certNo']){
            return back()->withInput($addSeller2Request->all())->withErrors(['error'=>'持卡人的身份证号与法人的身份证号必须一致']);
        }
        //通过高汇通的接口进行银行信息登记
        $bank_info = [
            'merchantId'		=> $addSeller2Request['merchantId'],
            'handleType'		=> 0,		// 0新增，1删除
            'bankCode'			=> $addSeller2Request['bankCode1'],
            'bankaccProp'		=> $addSeller2Request['bankaccProp'],		// 0私人，1公司
            'name'				=> $addSeller2Request['name'],
            'bankaccountType'	=> $addSeller2Request['bankaccountType'],
            'bankaccountNo'		=> $addSeller2Request['bankaccountNo'],
            'certNo'			=> $addSeller2Request['certNo'],
        ];
//        $Ruzhu=new Ruzhu();
//        $result=$Ruzhu->bank($bank_info);
//        new_logger('ruzhubank.log','请求的新增银行信息',[$bank_info]);
//        new_logger('ruzhubank.log','返回的结果',[$result]);
//        if ($result['head']['respCode']!='000000'){
//            return back()->withInput($addSeller2Request->all())->withErrors(['error'=>'银行信息登记失败！']);
//        }


        $bank_info1 = [
            'basic_id'		=> $basic_info->id,
            'merchantId'		=> $addSeller2Request['merchantId'],
            'bankCode'			=> $addSeller2Request['bankCode1'],
            'bankaccProp'		=> $addSeller2Request['bankaccProp'],		// 0私人，1公司
            'name'				=> $addSeller2Request['name'],
            'bankaccountNo'		=> $addSeller2Request['bankaccountNo'],
            'bankaccountType'	=> $addSeller2Request['bankaccountType'],
            'certCode'			=> '1',			// 1身份证，2护照，3军官证，4回乡证，5台胞证，6港澳通行证，7国际海员证，8外国人永久居住证，9其他
            'certNo'			=> $addSeller2Request['certNo'],
            'remark'		=> '商家银行卡信息登记',		// 备注
            'create_time'		=> time2date(),
            'created_at'		=> time2date(),
            'bankbranchNo'		=> $addSeller2Request['ibankno'],
            'bankName'			=> $addSeller2Request['bankName1'],
        ];
        //插入高汇通银行信息表
        $rs[]=RuzhuMerchantBank::insert(filter_update_arr($bank_info1));


        if(checkTrans($rs)){
            DB::commit();
            return back()->with('status','银行卡信息登记成功，请尽快去开通业务！');

        }else{
            DB::rollBack();
            return back()->withInput($addSeller2Request->all())->withErrors(['error'=>'银行卡信息登记失败！']);
        }
    }
    

    /**
     * 开通支付平台业务
     * @param AddSeller3Request $addSeller3Request
     * @return mixed
     */
    public function ght_seller_add_submit_step3(AddSeller3Request $addSeller3Request){
        $request_info=$addSeller3Request->all();
        $mobile=$request_info['mobile'];
        //判断该身份下是否已存在
        $user_info=Seller::where(['mobile'=>$mobile])->first(['id']);
        if(!isset($user_info->id)){
            return back()->withInput($addSeller3Request->all())->withErrors(['error'=>'该手机号还没注册商家，请先去填写商家基本信息进行登记信息']);
        }
        //判断该用户是否登记了基本信息
        $basic_info=RuzhuMerchantBasic::where(['merchantId'=>$addSeller3Request['merchantId'],'uid'=>$user_info->id])->first();
        if(!isset($basic_info)){
            return back()->withInput($addSeller3Request->all())->withErrors(['error'=>'请先登记基本信息后，才能开通业务']);
        }
        //判断该用户是否登记了银行信息
        $bank_info=RuzhuMerchantBank::where(['merchantId'=>$addSeller3Request['merchantId']])->first();
        if(!isset($bank_info)){
            return back()->withInput($addSeller3Request->all())->withErrors(['error'=>'请先登记银行卡信息后，才能开通业务']);
        }

        //通过高汇通的接口进行业务的开通
        $Businesss_info = [
            'merchantId'		=> $addSeller3Request['merchantId'],
            'handleType'		=> 0,		// 0：新增 1：修改 2：关闭业务 3：重新开通
            'futureRate'		=> $addSeller3Request['futureRate'],		// 设定商家让利百分比
        ];
//        $Ruzhu=new Ruzhu();
//        $result=$Ruzhu->submit($Businesss_info);
//        new_logger('ruzhuBusinesss.log','请求的开通业务信息',[$Businesss_info]);
//        new_logger('ruzhuBusinesss.log','返回的结果',[$result]);
//        if ($result['head']['respCode']!='000000'){
//            return back()->withInput($addSeller3Request->all())->withErrors(['error'=>$result['head']['respMsg']]);
//        }
        
        
        $Businesss_info1 = [
            'basic_id'		=> $basic_info->id,
            'merchantId'		=> $addSeller3Request['merchantId'],
            'handleType'		=> 0,
            'cycleValue'		=> 0,
            'allotFlag'		=> 0,
            'futureRateValue'		=> $addSeller3Request['futureRate'],
            'futureMinAmount'	=> 0,
            'futrueMaxAmount'			=> 0,
            'remark'			=> '开通商家所有业务',// 备注
            'benefit'		=> 1,
            'create_time'		=> time2date(),
            'created_at'		=> time2date(),
        ];
        //插入高汇通业务信息表
        $result=RuzhuMerchantBusinesss::insert(filter_update_arr($Businesss_info1));


        if(!$result){
            return back()->withInput($addSeller3Request->all())->withErrors(['error'=>'业务开通失败！']);

        }
        return back()->with('status','业务开通成功！');

    }

    /**
     * 编辑商家入驻信息页面
     * @param Request $request
     * @return mixed
     */
    public function ght_seller_edit(Request $request){
        $uid=$request->input('uid');
        $Ruzhu=new Ruzhu();
        $bank_list=BankList::get();
        $city_arr = get_ght_attr('city');
        $category_arr = get_ght_attr('category');
        $bank_arr = get_ght_attr('bank');
        //获取用户信息
        $seller_info=Seller::where(['id'=>$uid])->first();
        //获取商家基本信息
        $basic_info=RuzhuMerchantBasicCopy::where(['uid'=>$uid,'merchantId'=>$seller_info->child_merchant_no])->first();
        //获取商家银行卡信息
        $bank_info=RuzhuMerchantBank::where(['merchantId'=>$seller_info->child_merchant_no])->first();
        //获取商家的业务信息
        $busi_info=RuzhuMerchantBusinesss::where(['merchantId'=>$seller_info->child_merchant_no])->first();
//        //获取超级代付银行卡信息
//        $bank=UserBank::where(['uid'=>$seller_info->id,'type'=>2])->first();
        //支付平台业务列表
        $result=$Ruzhu->queryBusi($seller_info->child_merchant_no);
        if ($result['head']['respCode']!='000000'){
            $busi_list=null;
        }else{
            $busi_list=$result['body']['busiList'];
        }
        $yixian_info = YixiantdzdMerchants::where(['merchantNo' => $seller_info->child_merchant_no]) -> first();

        return view('admin.seller.ght_seller_edit',compact('bank_list','city_arr','category_arr','bank_arr','seller_info','basic_info','bank_info','busi_info','bank','busi_list','yixian_info'));
    }

    /**
     * 编辑商家基本信息
     * @param EditSeller1Request $editSeller1Request
     * @return mixed
     */
    public function ght_seller_edit_submit_step1(EditSeller1Request $editSeller1Request){
        $request_info=$editSeller1Request->all();
        $mobile=$request_info['mobile'];
//        $password=$request_info['password'];
        DB::beginTransaction();
        //判断该身份下是否已存在
        $user_info=Seller::where(['mobile'=>$mobile])->first(['id']);
        if(!isset($user_info->id)){
            return back()->withInput($editSeller1Request->all())->withErrors(['error'=>'该手机不存在无法修改信息']);
        }
        //判断是否有商家基本信息
        $basic_info=RuzhuMerchantBasicCopy::where(['uid'=>$user_info->id,'merchantId'=>$request_info['corpmanMobile']])->first();
        if(!isset($basic_info->id)){
            return back()->withInput($editSeller1Request->all())->withErrors(['error'=>'该商家还没登记基本信息']);
        }
        //判断是否新营业执照地址
        if(!empty($request_info['businessimagebase64'])){
            $result=$this->upload($request_info['businessimagebase64']);
            if($result['code']==0){
                return back()->withInput($editSeller1Request->all())->withErrors(['error'=>$result['message']]);
            }else{
                $request_info['businessimage']=$result['data']['filepath'];
            }
        }
        //通过高汇通的商家入驻接口修改入驻信息
        $request_info['handleType']=1;
        $request_info['remark']='修改商家入驻申请';
//        $result=$this->edit_selller_basic($request_info);
//        if ($result['code']=='0'){
//            return back()->withInput($editSeller1Request->all())->withErrors(['error'=>$result['message']]);
//        }
        unset($request_info['handleType']);
        unset($request_info['remark']);

        //判断是否需要修改密码
        if(!empty($request_info['password'])){
            $Admin=new Admin();
            $Admin->add_admin($request_info['mobile'],trim($request_info['password']),AdminRole::ROLE_SELLER,$request_info['uid']);
        }

        //判断是否修改密码
//        $password=empty($password)?$user_info->password:md5(md5($password).'tdzd');
//        $reg['mobile']=$mobile;
//        $reg['password']=$password;
//        $reg['created_at']=time2date();
        $reg['last_time']=time2date();
        $reg['is_bind_phone']=2;
        $reg['child_merchant_no']=$request_info['corpmanMobile'];
        //1插入商家表
        $result=Seller::where(['id'=>$user_info->id])->update($reg);
        $rs[]=$result==0?false:true;


        //判断是不是直辖市的郊区,选择区域编号
        if (empty($request_info['district10'])){
            $area_id = $request_info['city_code'];
        }else{
            $area_id = $request_info['district_code'];
        }

        $basic = [
            'merchantName'			=> $request_info['merchantName'],	// 店名，要与营业执照上一致
            'shortName'				=> $request_info['shortName'],
            'city'					=> $request_info['city'],		// 城市id
            'merchantAddress'		=> $request_info['merchantAddress'],	// 城市中文名
            'servicePhone'			=> $request_info['servicePhone'],			// 客服电话
            'merchantType'			=> $request_info['merchantType'],		// 00公司，01个体
            'category'				=> $request_info['category'],		// 类别id
            'corpmanName'			=> $request_info['corpmanName'],		// 法人姓名
            'corpmanId'				=> $request_info['corpmanId'],	// 法人身份证
//            'corpmanMobile'			=> $request_info['corpmanMobile'],	// 法人联系手机
            'bankCode'				=> $request_info['bankCode'],
            'bankName'				=> $request_info['bankName'],
            'bankaccountNo'			=> $request_info['bankaccountNo'],
            'bankaccountName'		=> $request_info['bankaccountName'],
            'businessimage'		=> $request_info['businessimage'],
            'businessLicense'		=> $request_info['businessLicense'],
            'autoCus'				=> 0,		// 0：不自动提现 1：自动提现
            'remark'				=> '修改商家入驻申请',
            'uid'				=> $user_info->id,
//            'merchantId'				=> $request_info['corpmanMobile'],
            'status'				=> 2,
            'area_id'				=> $area_id,
            'lat'				=> $request_info['lat'],  //纬度
            'lng'				=> $request_info['lng'],  //经度
            'updated_at'		=> time2date(),

        ];
        //商家基本信息登记表插入数据
        $result=RuzhuMerchantBasicCopy::where(['uid'=>$user_info->id,'merchantId'=>$basic_info->merchantId])->update($basic);
        $rs[]=$result==0?false:true;

        if(checkTrans($rs)){
            DB::commit();
            return back()->with('status','修改基本信息成功！');

        }else{
            DB::rollBack();
            return back()->withInput($editSeller1Request->all())->withErrors(['error'=>'修改基本信息失败']);
        }

    }


    /**
     * 编辑商家银行信息
     * @param EditSeller2Request $editSeller2Request
     * @return mixed
     */
    public function ght_seller_edit_submit_step2(EditSeller2Request $editSeller2Request){
        $request_info=$editSeller2Request->all();
        $mobile=$request_info['mobile'];
        DB::beginTransaction();

        //判断该身份下是否已存在
        $user_info=Seller::where(['mobile'=>$mobile])->first(['id']);
        if(!isset($user_info->id)){
            return back()->withInput($editSeller2Request->all())->withErrors(['error'=>'该手机号还没注册商家，请先去填写商家基本信息进行登记信息！']);
        }
        //判断是否有商家基本信息
        $basic_info=RuzhuMerchantBasic::where(['uid'=>$user_info->id,'merchantId'=>$request_info['merchantId']])->first();
        if(!isset($basic_info->id)){
            return back()->withInput($editSeller2Request->all())->withErrors(['error'=>'该商家还没登记基本信息']);
        }
        //判断该商家是否登记了银行信息
        $info=RuzhuMerchantBank::where(['merchantId'=>$editSeller2Request['merchantId']])->first();
        if (empty($info)){
            return back()->withInput($editSeller2Request->all())->withErrors(['error'=>'该商家还没登记银行信息，请先登记银行信息！']);
        }
        //判断持卡人是否与法人相同
        $basic_info=RuzhuMerchantBasic::where(['uid'=>$user_info->id,'merchantId'=>$editSeller2Request['merchantId']])->first();
        if($basic_info->corpmanName!=$editSeller2Request['name']){
            return back()->withInput($editSeller2Request->all())->withErrors(['error'=>'持卡人必须与法人相同！']);
        }
        //判断持卡人的身份证号是否与法人的身份证号相同
        if($basic_info->corpmanId!=$editSeller2Request['certNo']){
            return back()->withInput($editSeller2Request->all())->withErrors(['error'=>'持卡人的身份证号与法人的身份证号必须一致！']);
        }


        //通过高汇通的接口进行删除旧银行信息
//        $obank_info=RuzhuMerchantBank::where(['merchantId'=>$editSeller2Request['merchantId']])->first();
        if ($info->bankaccountNo == $editSeller2Request['bankaccountNo']){
            $bank_info = [
                'merchantId'		=> $editSeller2Request['merchantId'],
                'handleType'		=> 1,		// 0新增，1删除
                'bankaccProp'		=> $info->bankaccProp,		// 0私人，1公司
                'bankCode'			=> $info->bankCode,
                'name'				=> $info->name,
                'bankaccountType'				=> $info->bankaccountType,
                'bankaccountNo'		=> $info->bankaccountNo,
                'certNo'			=> $info->certNo,
            ];
//            $Ruzhu=new Ruzhu();
//            $result=$Ruzhu->bank($bank_info);
//            new_logger('ruzhubank.log','请求的删除银行信息',[$bank_info]);
//            new_logger('ruzhubank.log','返回的结果',[$result]);
//            if ($result['head']['respCode']!='000000'){
//                return back()->withInput($editSeller2Request->all())->withErrors(['error'=>$result['head']['respMsg']]);
//            }
        }


        //通过高汇通的接口进行新增新银行信息
        $bank_info1 = [
            'merchantId'		=> $editSeller2Request['merchantId'],
            'handleType'		=> 0,		// 0新增，1删除
            'bankaccProp'		=> $editSeller2Request['bankaccProp'],		// 0私人，1公司
            'bankCode'			=> $editSeller2Request['bankCode1'],
            'name'				=> $editSeller2Request['name'],
            'bankaccountType'	=> $editSeller2Request['bankaccountType'],
            'bankaccountNo'		=> $editSeller2Request['bankaccountNo'],
            'certNo'			=> $editSeller2Request['certNo'],
        ];
//        $Ruzhu=new Ruzhu();
//        $result=$Ruzhu->bank($bank_info1);
//        new_logger('ruzhubank.log','请求的新增银行信息',[$bank_info1]);
//        new_logger('ruzhubank.log','返回的结果',[$result]);
//        if ($result['head']['respCode']!='000000'){
//            return back()->withInput($editSeller2Request->all())->withErrors(['error'=>$result['head']['respMsg']]);
//        }



        $bank_info2 = [
            'basic_id'		=> $basic_info->id,
            'merchantId'		=> $editSeller2Request['merchantId'],
            'bankCode'			=> $editSeller2Request['bankCode1'],
            'bankaccProp'		=> $editSeller2Request['bankaccProp'],		// 0私人，1公司
            'name'				=> $editSeller2Request['name'],
            'bankaccountNo'		=> $editSeller2Request['bankaccountNo'],
            'bankaccountType'	=> $editSeller2Request['bankaccountType'],
            'certCode'			=> '1',			// 1身份证，2护照，3军官证，4回乡证，5台胞证，6港澳通行证，7国际海员证，8外国人永久居住证，9其他
            'certNo'			=> $editSeller2Request['certNo'],
            'remark'		=> '商家银行卡信息修改',		// 备注
//            'create_time'		=> time2date(),
            'bankbranchNo'		=> $editSeller2Request['ibankno'],
            'bankName'			=> $editSeller2Request['bankName1'],
            'updated_at'		=> time2date(),

        ];
        //修改高汇通银行信息表
        $result=RuzhuMerchantBank::where(['merchantId'=>$editSeller2Request['merchantId']])->update($bank_info2);
        $rs[]=$result==0?false:true;

//        $bank_info3 = [
//            'account_name'		=> $editSeller2Request['name'],
//            'account_bank'		=> $editSeller2Request['account_bank'],
//            'bank_account'		=> $editSeller2Request['bankaccountNo'],
//            'bank_code'			=> $editSeller2Request['bankCode1'],
//            'uid'		=> $user_info->id,
//            'isdefault'				=> 0,
//            'type'		=> 2,
//            'created_at'		=> time2date(),
//            'ibankno'		=> $editSeller2Request['ibankno'],
//        ];
//        //插入超级代付银行信息表
//        $result=UserBank::where(['uid'=>$user_info->id,'type'=>2])->update($bank_info3);
//        $rs[]=$result==0?false:true;

        //改变基本信息表的状态
        $result=RuzhuMerchantBasic::where(['uid'=>$user_info->id,'merchantId'=>$editSeller2Request['merchantId']])->update(['status'=>2]);
        $rs[]=$result==0?false:true;

        if(checkTrans($rs)){
            DB::commit();
            return back()->with('status','银行卡信息修改成功！');

        }else{
            DB::rollBack();
            return back()->withInput($editSeller2Request->all())->withErrors(['error'=>'银行卡信息修改失败！']);
        }
    }

    /**
     * 编辑商家业务
     * @param EditSeller3Request $editSeller3Request
     * @return mixed
     */
    public function ght_seller_edit_submit_step3(EditSeller3Request $editSeller3Request){
        $request_info=$editSeller3Request->all();
        $mobile=$request_info['mobile'];
        DB::beginTransaction();

        //判断该身份下是否已存在
        $user_info=Seller::where(['mobile'=>$mobile])->first(['id']);
        if(!isset($user_info->id)){
            return back()->withInput($editSeller3Request->all())->withErrors(['error'=>'该手机号还没注册商家，请先去填写商家基本信息进行登记信息！']);
        }
        //判断是否有商家基本信息
        $basic_info=RuzhuMerchantBasic::where(['uid'=>$user_info->id,'merchantId'=>$request_info['merchantId']])->first();
        if(!isset($basic_info->id)){
            return back()->withInput($editSeller3Request->all())->withErrors(['error'=>'该商家还没登记基本信息']);
        }
        //判断该商家是否登记了银行信息
        $info=RuzhuMerchantBank::where(['merchantId'=>$editSeller3Request['merchantId']])->first();
        if (empty($info)){
            return back()->withInput($editSeller3Request->all())->withErrors(['error'=>'该商家还没登记银行信息，请先登记银行信息！']);
        }
        //判断该商家是否开通业务
        $info1=RuzhuMerchantBusinesss::where(['merchantId'=>$editSeller3Request['merchantId']])->first();
        if (empty($info1)){
            return back()->withInput($editSeller3Request->all())->withErrors(['error'=>'该商家还没开通业务，请先开通业务！']);
        }

        //通过高汇通的接口进行业务的修改
        $Businesss_info = [
            'merchantId'		=> $editSeller3Request['merchantId'],
            'handleType'		=> 1,		// 0：新增 1：修改 2：关闭业务 3：重新开通
            'futureRate'		=> $editSeller3Request['futureRate'],		// 设定商家让利百分比
        ];
//        $Ruzhu=new Ruzhu();
//        $result=$Ruzhu->submit($Businesss_info);
//        new_logger('ruzhuBusinesss.log','请求的开通业务信息',[$Businesss_info]);
//        new_logger('ruzhuBusinesss.log','返回的结果',[$result]);
//        if ($result['head']['respCode']!='000000'){
//            return back()->withInput($editSeller3Request->all())->withErrors(['error'=>$result['head']['respMsg']]);
//        }

        $basic_info=RuzhuMerchantBasic::where(['uid'=>$user_info->id,'merchantId'=>$editSeller3Request['merchantId']])->first();
        $Businesss_info1 = [
            'basic_id'		=> $basic_info->id,
            'merchantId'		=> $editSeller3Request['merchantId'],
            'handleType'		=> 0,
            'cycleValue'		=> 0,
            'allotFlag'		=> 0,
            'futureRateValue'		=> $editSeller3Request['futureRate'],
            'futureMinAmount'	=> 0,
            'futrueMaxAmount'			=> 0,
            'remark'			=> '修改商家业务',// 备注
            'benefit'		=> 1,
            'updated_at'		=> time2date(),
        ];
        //修改高汇通业务信息表
        $result=RuzhuMerchantBusinesss::where(['merchantId'=>$editSeller3Request['merchantId']])->update($Businesss_info1);
        $rs[]=$result==0?false:true;

        //改变基本信息表的状态
        $result=RuzhuMerchantBasic::where(['uid'=>$user_info->id,'merchantId'=>$editSeller3Request['merchantId']])->update(['status'=>2]);
        $rs[]=$result==0?false:true;



        if(checkTrans($rs)){
            DB::commit();
            return back()->with('status','修改业务成功！');

        }else{
            DB::rollBack();
            return back()->withInput($editSeller3Request->all())->withErrors(['error'=>'修改业务失败！']);
        }
        
    }

    /**
     * 提现银行页面
     * @return mixed
     */
    public function seller_withdraw_bank(){
        $bank_list=BankList::get();
        $uid=$_GET['uid'];
        $bank_info=UserBank::where(['uid'=>$uid,'type'=>2,'isdefault'=>1])->first();
        return view('admin.seller.seller_withdraw_bank',compact('bank_list','bank_info'));

    }

    /**
     * 提现银行信息提交
     * @param WithdrawBankRequest $withdrawBankRequest
     * @return mixed
     */
    public function seller_withdraw_bank_submit(WithdrawBankRequest $withdrawBankRequest){
        $requset_info=$withdrawBankRequest->all();
        $bank = [
            'account_name'		=> $withdrawBankRequest['account_name'],
            'account_bank'		=> $withdrawBankRequest['account_bank'],
            'bank_account'		=> $withdrawBankRequest['bank_account'],
            'uid'		=> $withdrawBankRequest['uid'],
            'isdefault'				=> 1,
            'type'		=> 2,
            'created_at'		=> time2date(),
            'ibankno'		=> $withdrawBankRequest['ibankno'],
            'identity' =>$withdrawBankRequest['identity'],
            'phone'=>$withdrawBankRequest['phone']
        ];
        //判断是否设置过提现银行密码
        $bank_info=UserBank::where(['uid'=>$requset_info['uid'],'type'=>2])->first();
        if (empty($bank_info)){
            //不存在
            //插入超级代付银行信息表
            $result=UserBank::insert($bank);
            if ($result==false){
                return back()->withInput($withdrawBankRequest->all())->withErrors(['error'=>'添加提现银行信息失败！']);
            }
            return back()->with(['status'=>'添加提现银行信息成功！']);
        }else{
            //存在
            //修改超级代付银行信息表
            $result=UserBank::where(['uid'=>$requset_info['uid'],'type'=>2])->update($bank);
            if ($result==0){
                return back()->withInput($withdrawBankRequest->all())->withErrors(['error'=>'修改提现银行信息失败！']);
            }
            return back()->with(['status'=>'修改提现银行信息成功！']);

        }

    }


    /**
     * 运营中心管理列表
     */
    public function operate_list(Request $request){
        //运营中心手机号
        $mobile=$request->input('mobile');
        $operate_list=AgentOperate::where(function ($query) use ($mobile){
            if(!empty($mobile)){
                $query->where('wish_agent_operate.mobile','like','%'.$mobile.'%');
            }
            return $query;
        })
            ->leftJoin('wish_agent_operate as a1','a1.id','=','wish_agent_operate.parent_id')
            ->leftJoin('wish_user as a2','a2.id','=','wish_agent_operate.inviter_user_id')
            ->rightJoin('wish_profit_relation as wpr','a1.id','=','wpr.agent_id')
            ->select(['wish_agent_operate.*','a1.mobile as parent_mobile','a2.mobile as  inviter_user_mobile'])
            ->orderBy('id','desc')
            ->paginate(15);
        return view('admin.seller.operate_list',['operate_list'=>$operate_list]);
    }


    /**
     * 新增运营中心页面
     * @return mixed
     */
    public function operate_add(){
        return view('admin.seller.operate_add',compact('info'));
    }
    
    /**
     * 新增运营中心提交
     * @param $mobile 运营中心手机号
     * @return mixed
     */
    public function operate_add_submit(AddOperateRequest $addOperateRequest){
        $mobile=\request('mobile');
        $parent_mobile=\request('parent_mobile');
        $remarks=\request('remarks');
        $inviter_user_mobile=\request('inviter_user_mobile');
        $level=\request('level');
        //新增运营中心
        $AgentOperate=new AgentOperate();
        $AgentOperate->add_operate($mobile,$parent_mobile,$remarks,$level,$inviter_user_mobile);
        return pageBackSuccess('运营中心添加成功');
    }

    public function pushqr_list(Request $request){
        $id=$request->input('id');
        $list=PushQrQrcode::where(['pushQr_qrcode.operate_id'=>$id])
            ->leftJoin('seller as s1','s1.id','=','pushQr_qrcode.seller_id')
            ->leftJoin('pushQr_user as s2','s2.id','=','pushQr_qrcode.qr_user_id')
            ->select(['pushQr_qrcode.*','s1.mobile as seller_mobile','s2.mobile as push_mobile'])
            ->orderBy('id','desc')
            ->paginate();
        $data=AgentOperate::where(['id'=>$id])->first();
        return view('admin.seller.pushqr_list',compact('list','data'));
    }

    public function create_pushqr(Request $request){
        return view('admin.seller.create_pushqr');
    }

    public function create_pushqr_submit(Request $request){
        $PushQrQrcode=new PushQrQrcode();
        $id=$request->input('id');
        $return_arr=$PushQrQrcode->create_qrcode($request->input('push_number'),$id);
        return $return_arr['code']==0?back()->withInput($request->all())->withErrors(['error'=>$return_arr['message']]):redirect('/seller/pushqr_list?id='.$id)->with('status','地推商家二维码生成成功');
    }

    public function download_pushQr(Request $request){
        $id=$request->input('id');
        $operate_mobile=$request->input('operate_mobile');
        $info=PushQrQrcode::where(['pushQr_qrcode.id'=>$id])
            ->leftJoin('agent_operate as u2','u2.id','=','pushQr_qrcode.operate_id')
            ->first(['u2.mobile']);
        if(isset($info->mobile)) {
            $mobile=$info->mobile;
        }else{
            echo '编号不存在';exit;
        }
        if ($mobile!=$operate_mobile){
            echo '该地推二维码不属于该运营商';exit;
        }
        $fontpath = './seller/qrcode/msyh.ttf';
        // 生成二维码
        $qr_source = 'http://pan.baidu.com/share/qrcode?w=370&h=370&url=http://'. env('APP_API_DOMAIN') . env('APP_DOMAIN').'/pushQr/create_seller?id='.$id;
        $pos = imagettfbbox(18,0,$fontpath,$id);
        $text_width = $pos[2] - $pos[0];
        $str1 = mb_convert_encoding('编号','html-entities','utf-8');
        $str2 = mb_convert_encoding('运营中心','html-entities','utf-8');
        $image = \Image::make('./seller/qrcode/desk_qr_template.jpg')->insert($qr_source,'top-left','96','248')->text($str1.':'.$id.','.$str2.':'.$mobile,(250 - $text_width) / 2,690,function($font) use ($fontpath){
            $font->file($fontpath);
            $font->size(24);
        });
        return $image->response();
    }

    public function yixian_submit(Request $request){
        $request_info = $request -> all();
        $seller = Seller::where(['id' => $request_info['seller_id']]) -> first();
        $yixian = [
            'merchantNo'=> $seller -> mobile,
            'TaxRegistrationNo'		=> $request_info['TaxRegistrationNo'],
            'bankarea'		=> $request_info['bankarea'],
            'bankUrl1'		=> $request_info['bankUrl1'],
            'bankUrl2'		=> $request_info['bankUrl2'],
            'ICurl1'		=> $request_info['ICurl1'],
            'ICurl2'		=> $request_info['ICurl2'],
            'merchantICurl'		=> $request_info['merchantICurl'],
            'is_submit'		=> '1',
        ];
        $yixian_info =YixiantdzdMerchants::where(['merchantNo'=> $seller -> mobile]) -> first();
        if (empty($yixian_info)){
            YixiantdzdMerchants::insert($yixian);
            return pageBackSuccess('添加成功');
        }else{
            YixiantdzdMerchants::where(['merchantNo'=> $seller -> mobile]) -> update($yixian);
            return pageBackSuccess('修改成功');
        }



    }

    /**
     * 3推4 出彩树
     */
    public function agent_tree(){
        $agent_id=\request('id');
//        $agent_id=561;
        $AgentSevenToHalf=new AgentSevenToHalf;
        //先找到当前的位置的父节点
//        $parent_agent_id=$AgentSevenToHalf->find_root($agent_id);
		$users = [];
        $first = AgentSevenToHalf::where(['agent_id'=>$agent_id])->first();
        if($first){
			$child = $first->child();
			$first->parentSeven(7)->merge($child)->unique('id')->sortBy('microtime')->each(function($v) use (&$users){
				$users[] = [
					'id'		=> $v->agent_id,
					'name'		=> Agent::where(['id'=>$v->agent_id])->value('mobile'),
					'className'	=> 'middle-level',
				];
			});
		}
		$user_tree = $AgentSevenToHalf->childrenDeep($users);
//        $user_tree=$parent_agent_id==0?[]:$AgentSevenToHalf->tree_find($parent_agent_id);
//		dd($user_tree);
		$user_tree = json_encode($user_tree);
        return view('admin.seller.agent_tree',compact('user_tree'));
    }

    public function query(){
//        $Ruzhu=new Ruzhu();
//        $result=$Ruzhu->queryInfo('18813951123');
//        dd($result);

    }

    public function update_seller_h_ce(){
        Seller::where([
            'id'=>\request('id')
        ])->update([
            'is_h_certification'=>\request('is_h_certification')
        ]);
        return json_success('修改成功-');
    }

    public function add_terminalId(){
        $terminalId=\request('terminalId');
        $info=Pos::where(['terminalId'=>$terminalId])->first();
        if(!isset($info->id)){
            return json_error('pos机不存在');
        }
        if(!empty($info->agent_uid)){
            return json_error('该机子已绑定其他代理');
        }
        return json_success('可以使用');
    }

    public function clear_openid(){
        $mobile=\request('mobile');
        clear_openid($mobile);
        return json_success('处理成功');
    }

    public function edit_agent(){
        $id=\request('id');
        $info=Agent::where(['wish_agent.id'=>$id])
            ->leftJoin('wish_agent as a1','wish_agent.inviter_id','=','a1.id')
            ->leftJoin('wish_agent_operate as a2','wish_agent.operate_id','=','a2.id')
            ->first(['wish_agent.*','a1.mobile as invite_mobile','a2.mobile as operate_mobile']);
//        dd($info);
        return view('admin.seller.agent_edit',compact('info'));
    }

    /**
     * 手动增加用户
     */
    public function add_user(){
        $res=(new User)->seller_register(\request()->input('mobile'),'123456');
        dd($res);
    }



    /**
     * 分公司管理列表
     */
    public function filiale_operate_list(Request $request){
        //运营中心手机号
        $mobile=$request->input('mobile');
        $operate_list=FilialeOperate::where(function ($query) use ($mobile){
            if(!empty($mobile)){
                $query->where('wish_filiale_operate.mobile','like','%'.$mobile.'%');
            }
            return $query;
        })
            ->leftJoin('wish_filiale_operate as a1','a1.id','=','wish_filiale_operate.parent_id')
            ->leftJoin('wish_user as a2','a2.id','=','wish_filiale_operate.inviter_user_id')
            ->rightJoin('wish_profit_relation as wpr','a1.id','=','wpr.filiale_id')
            ->select(['a1.*','a1.mobile as parent_mobile','a2.mobile as  inviter_user_mobile'])
            ->orderBy('id','desc')
            ->paginate(15);
        return view('admin.seller.filiale_operate_list',['operate_list'=>$operate_list]);
    }

    /**
     * 新增运营中心页面
     * @return mixed
     */
    public function filiale_operate_add(){
        return view('admin.seller.filiale_operate_add',compact('info'));
    }

    /**
     * 新增运营中心提交
     * @param $mobile 运营中心手机号
     * @return mixed
     */
    public function filiale_operate_add_submit(AddOperateRequest $addOperateRequest){
        $mobile=\request('mobile');
        $parent_mobile=\request('parent_mobile');
        $remarks=\request('remarks');
        $inviter_user_mobile=\request('inviter_user_mobile');
        $level=\request('level');
        //新增运营中心
        $AgentOperate=new FilialeOperate();
        $AgentOperate->add_operate($mobile,$parent_mobile,$remarks,$level,$inviter_user_mobile);
        return pageBackSuccess('运营中心添加成功');
    }



}
