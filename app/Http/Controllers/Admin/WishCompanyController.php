<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\ApiException;
use App\Http\Requests\Admin\AddCompanyRequest;
use App\Http\Requests\Admin\AddSellerRequest;
use App\Http\Requests\Admin\EditCompanyRequest;
use App\Http\Requests\Admin\EditSeller1Request;
use App\Models\BankList;
use App\Models\Wish\WishCompany;
use App\Models\Wish\WishCompanyBank;
use App\Models\Wish\WishCompanyInfo;
use Illuminate\Http\Request;

class WishCompanyController extends CommonController
{

    /**
     * 分公司管理列表
     */
   public function company_list(Request $request){
       //分公司手机号
       $mobile=$request->input('mobile');
       $name=$request->input('name');
       $user=session('admin_info');
       $company_list=WishCompany::where(function ($query) use ($mobile,$name){
           if(!empty($mobile)){
               $query->where('wish_company.mobile','like','%'.$mobile.'%');
           }
           if(!empty($name)){
               $query->where('a1.merchantName','like','%'.$name.'%');
           }
           return $query;
       })
           ->leftJoin('wish_company_info as a1','wish_company.id','=','a1.uid')
           ->where('wish_company.mobile','!=',$user['username'])
           ->select(['wish_company.*',
               'a1.corpmanName',
               'a1.status as apply_status',
               'a1.merchantName'
               ,'a1.id as basic_id',
           ])
           ->orderBy('id','desc')
           ->paginate(15);
       return view('admin.wish_company.company_list',['company_list'=>$company_list]);
   }

    /**
     * 冻结/解冻商户
     */
    public function change_status(Request $request){

        $request_info=$request->all();
        WishCompany::where(['id'=>$request_info['id']])->update(['status'=>$request_info['status']]);
        return arr_post(1,'状态改变成功');
    }

    /**
     * 分公司入驻页面
     * @return mixed
     */
    public function company_add(){
        //银行列表
        $bank_list=BankList::get();
        //城市列表
        $city_arr = get_ght_attr('city');
        //经营类别列表
        $category_arr = get_ght_attr('category');
        //去掉名字中的英文名
        foreach ($category_arr as $value){
            $arr = explode(" ",$value->categoryName);
            $value->categoryName = $arr[1];
        }
        //银行代码列表
        $bank_arr = array();
        $bank_arr1 = get_ght_attr('bank');
        foreach ($bank_arr1 as $item => $value){
            foreach ($bank_list as $k => $v){
                if ($v['bank_name'] == $value->bankName){
                    $bank_arr[$item]->bankCode =  $value->bankCode;
                    $bank_arr[$item]->bankName =  $value->bankName;
                }
            }
        }
        return view('admin.wish_company.company_add',compact('bank_list','city_arr','category_arr','bank_arr'));
    }

    /**
     * 新增分公司提交
     * @param AddSellerRequest $addSellerRequest
     * @return \Illuminate\Http\RedirectResponse
     * @throws ApiException
     */
    public function company_add_submit(AddCompanyRequest $addSellerRequest){
        $wishSellerInfo = new WishCompanyInfo();
        $requestInfo = $addSellerRequest->all();
        $wishSellerInfo->add_company($requestInfo,1,$requestInfo['invite_mobile']);
        return pageBackSuccess('分公司入驻成功');
    }

    /**
     * 编辑分公司入驻信息页面
     * @param Request $request
     * @return mixed
     */
    public function company_edit(Request $request){
        $uid=$request->input('uid');
        $bank_list=BankList::get();
        $city_arr = get_ght_attr('city');
        $category_arr = get_ght_attr('category');
        $bank_arr = get_ght_attr('bank');
        //获取用户信息
        $company_info=WishCompany::where(['id'=>$uid])->first();
        //获取分公司基本信息
        $basic_info=WishCompanyInfo::where(['uid'=>$uid])->first();
        //获取分公司银行卡信息
        $bank_info=WishCompanyBank::where(['merchantId'=>$company_info->child_merchant_no])->first();

        return view('admin.wish_company.company_edit',compact('bank_list','city_arr','category_arr','bank_arr','company_info','basic_info','bank_info','bank'));
    }

    /**编辑分公司基本信息
     * @param EditSeller1Request $editSeller1Request
     * @return \Illuminate\Http\RedirectResponse
     * @throws ApiException
     */
    public function company_edit_submit(EditCompanyRequest $editSeller1Request){
        $wishSellerInfo = new WishCompanyInfo();
        $requestInfo = $editSeller1Request->all();
        $wishSellerInfo->add_company($requestInfo,2);
        return pageBackSuccess('修改基本信息成功');
    }



}
