<?php
namespace App\Http\Controllers\Admin;



use App\Models\OfflineOrder;
use App\Models\ProfitRelation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * 店铺管理
 */
class SellerManagerController extends CommonController{


    /**
     * 消费记录
     */
    public function consume_list(Request $request){
//        $p = new ProfitRelation();
//        $p->addRelation(ProfitRelation::IDENTITY_TYPE_SELLER,1,);
//        dd(session('admin_info'));
        //订单号
        $offline_order_sn=$request->input('order_sn');
        $buyer_mobile=$request->input('buyer_mobile');
        $seller_mobile=$request->input('seller_mobile');
        $agent_mobile=$request->input('agent_mobile');
        $terminalId=$request->input('terminalId');
        $notify_type=$request->input('notify_type');
        $type=$request->input('type');
        $pay_type=$request->input('pay_type');
        $start_time=$request->input('start_time');
        $end_time=$request->input('end_time');
        $status=\request('pay_status');

        $offline_order_where=OfflineOrder::where(function ($query) use ($offline_order_sn,$buyer_mobile,$seller_mobile,$agent_mobile,$terminalId,$notify_type,$type,$pay_type,$start_time,$end_time){
            if(!empty($offline_order_sn)){
                //增加上游订单编号搜索 2018年6月29日13:57:12
                $query->where('offline_order'.'.order_sn','like','%'.$offline_order_sn.'%')->orWhere('offline_order'.'.up_order_sn','like','%'.$offline_order_sn.'%');
            }
            if(!empty($buyer_mobile)){
                $query->where('u1.mobile','like','%'.$buyer_mobile.'%');
            }
            if(!empty($seller_mobile)){
                $query->where('u2.mobile','like','%'.$seller_mobile.'%');
            }
            if(!empty($agent_mobile)){
                $query->where('u3.mobile','like','%'.$agent_mobile.'%');
            }
            if(!empty($terminalId)){
                $query->where('offline_order'.'.terminalId','like','%'.$terminalId.'%');
            }
            if(!empty($pay_type)){
                $query->where(['offline_order.pay_type'=>$pay_type]);
            }
            if(!empty($notify_type)){
                $query->where(['offline_order.notify_type'=>$notify_type]);
            }
            if(!empty($type)){
                if($type==4){
                    //赛选有效订单
                    $query->where('actual_amount','>','1');
                }
                $query->where(['offline_order.type'=>$type]);
            }
//            if(!empty($start_time)||!empty($end_time)){
//                $query->whereBetween('offline_order.created_at',[$start_time." 00:00:00",$end_time." 23:59:59"]);
//            }
            if(!empty($start_time)||!empty($end_time)){
                $query->where('offline_order.created_at','>',$start_time." 00:00:00")->where('offline_order.created_at','<',$end_time." 23:59:59");
//                $query->whereDate('offline_order.created_at',[$start_time." 00:00:00",$end_time." 23:59:59"]);
            }
            return $query;
        })
            ->leftJoin('wish_user as u1','u1.id','=','offline_order.buyer_id')
            ->leftJoin('wish_seller as u2','u2.id','=','offline_order.uid')
            ->leftJoin('wish_agent as u3','u3.id','=','offline_order.agent_id')
            ->leftJoin('wish_ruzhu_merchant_basic as u4','u4.uid','=','offline_order.uid');

//        if(empty($status)){
        $offline_order_where->consumeOrder();
//        }else{
//            $offline_order_where->consumeOrder($status);
//        }
        //统计 必须放在订单列表前面不然读取不到
//        $sum_data=$offline_order_where
//            ->select(DB::raw('SUM(actual_amount) as actual_amount,SUM(seller_amount) as seller_amount,SUM(platform_amount) as platform_amount,SUM(poundage) as poundage'))
//            ->first()->toArray();
//        $sum_data['recharge']=OfflineOrder::where(['type'=>7,'status'=>2])->where(function ($query) use ($offline_order_sn,$start_time,$end_time){
//            if(!empty($offline_order_sn)){
//                $query->where('offline_order'.'.order_sn','like','%'.$offline_order_sn.'%');
//            }
//            if(!empty($offline_order_sn)){
//                $query->where('offline_order'.'.order_sn','like','%'.$offline_order_sn.'%');
//            }
//            if(!empty($start_time)||!empty($end_time)){
//                $query->whereBetween('offline_order.created_at',[$start_time." 00:00:00",$end_time." 23:59:59",]);
//            }
//            return $query;
//        })->sum('actual_amount');
        //订单列表
        $order_list=$offline_order_where->orderBy('offline_order.created_at','desc')
            ->select(['offline_order.*',
                'u1.mobile as buyer_mobile',
                'u2.mobile as seller_mobile',
                'u3.mobile as agent_mobile',
                'u4.merchantName as seller_name',
            ])
            ->paginate(15);
//        pos商城购买商品以前记录的id是益邦客id 单独处理
        foreach ($order_list as $k=>$v){
            if($v->type==4&&$v->created_at->format('Y-m-d')<'2018-01-05'){
                $order_list[$k]->buyer_mobile=Agent::where(['id'=>$v->buyer_id])->first(['mobile'])->mobile;
            }
            if($v->type==5){
                $inviter_id=Seller::where(['id'=>$v->uid])->value('inviter_id');
                $order_list[$k]->agent_mobile=Agent::where(['id'=>$inviter_id])->value('mobile');
            }
        }
        return view('admin.seller_manager.consume_list',compact('order_list','sum_data'));
    }

}
