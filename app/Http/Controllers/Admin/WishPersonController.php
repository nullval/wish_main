<?php
namespace App\Http\Controllers\Admin;
/**
 * 个人管理
 * User: Administrator
 * Date: 2018/8/20 0020
 * Time: 13:47
 */

use App\Exceptions\ApiException;
use App\Http\Requests\Admin\EditAgentRequest;
use App\Http\Requests\Admin\EditCompanyRequest;
use App\Http\Requests\Admin\EditGenerlizeRequest;
use App\Http\Requests\Admin\PwdUpdateRequest;
use App\Models\Admin;
use App\Models\Wish\WishAgent;
use App\Models\Wish\WishAgentInfo;
use App\Models\Wish\WishCompany;
use App\Models\Wish\WishCompanyInfo;
use App\Models\Wish\WishGeneralize;
use App\Models\Wish\WishGeneralizeInfo;
use App\Models\Wish\WishSeller;
use App\Services\AdminIdentityDispose;
use Illuminate\Support\Facades\DB;

class WishPersonController extends CommonController
{
    /**
     * 密码修改页面
     */
    public function uppwd(){

        return view('admin.wish_person.uppwd', compact());
    }

    /**
     * 提交密码修改
     */
    public function submit_uppwd(PwdUpdateRequest $request){
        $requestData = $request->all();
        $user = session('admin_info');
        $adminInfo = Admin::where('id',$user['id'])->first();
        if(md5(md5($requestData['old_pwd'])) != $adminInfo['password']){
            throw new ApiException('原密码不正确');
        }
        //验证两次密码
        if($requestData['new_pwd'] != $requestData['re_pwd']){
            throw new ApiException('两次密码不一致');
        }
        $newPwd = md5(md5($requestData['new_pwd']));
        $adminIdentityDisposeModel  = new AdminIdentityDispose();
        $identity = $adminIdentityDisposeModel->getUserIdentity($user['role_id']);
        DB::beginTransaction();
        switch ($identity){
            case AdminIdentityDispose::IDENTITY_SELLER:
                //商家
                WishSeller::where('mobile',$user['username'])->update(['password'=>$newPwd]);
                break;
            case AdminIdentityDispose::IDENTITY_GENERALIZE:
                //推荐人
                WishGeneralize::where('mobile',$user['username'])->update(['password'=>$newPwd]);
                WishSeller::where('mobile',$user['username'])->update(['password'=>$newPwd]);
                break;
            case AdminIdentityDispose::IDENTITY_AGENT:
                //代理
                WishAgent::where('mobile',$user['username'])->update(['password'=>$newPwd]);
                WishGeneralize::where('mobile',$user['username'])->update(['password'=>$newPwd]);
                WishSeller::where('mobile',$user['username'])->update(['password'=>$newPwd]);
                break;
            case AdminIdentityDispose::IDENTITY_COMPANY:
                //分公司
                WishCompany::where('mobile',$user['username'])->update(['password'=>$newPwd]);
                WishAgent::where('mobile',$user['username'])->update(['password'=>$newPwd]);
                WishGeneralize::where('mobile',$user['username'])->update(['password'=>$newPwd]);
                WishSeller::where('mobile',$user['username'])->update(['password'=>$newPwd]);
                break;
        }
        $adminModel = new Admin();
        //修改会员表
        //找出所属的会员表及下线
        $ids = $adminModel->where('username',$user['username'])->pluck('id');
        $upRes = $adminModel->whereIn('id',$ids)->update(['password'=>$newPwd]);
        if($upRes){
            DB::commit();
            return pageBackSuccess('修改密码成功');
        }else{
            DB::rollBack();
            return pageBackSuccess('修改密码失败');
        }
//        $adminModel->add_admin($user['username'], $requestData['new_pwd'],$user['role_id'],$user['id']);
//        DB::commit();
    }

    /**
     * 个人信息管理
     */
    public function info(){
        //转发到指定的页面进行
        $adminIdentityDisposeModel  = new AdminIdentityDispose();
        $user = session('admin_info');
        $identity = $adminIdentityDisposeModel->getUserIdentity($user['role_id']);
        switch ($identity){
            case AdminIdentityDispose::IDENTITY_SELLER:
                //商家
                return $this->seller_info();
                break;
            case AdminIdentityDispose::IDENTITY_GENERALIZE:
                //推荐人
                return $this->generalize_info();
                break;
            case AdminIdentityDispose::IDENTITY_AGENT:
                //代理
                return $this->agent_info();
                break;
            case AdminIdentityDispose::IDENTITY_COMPANY:
                //分公司
                return $this->company_info();
                break;
        }
    }

    /**
     * 商家资料页面
     */
    private function seller_info(){

    }

    /**
     * 商家资料提交页面
     */
    public function seller_info_submit(){

    }

    /**
     * 直推人资料页面
     */
    private function generalize_info(){
        $user = session('admin_info');
        $city_arr = get_ght_attr('city');
        $category_arr = get_ght_attr('category');
        //获取用户信息
        $generalize_info=WishGeneralize::where(['mobile'=>$user['username']])->first();
        //获取直推人基本信息
        $basic_info=WishGeneralizeInfo::where(['uid'=>$generalize_info['id']])->first();
        return view('admin.wish_person.generalize_info',compact('city_arr','category_arr','generalize_info','basic_info'));
    }

    /**
     * 直推人资料提交页面
     */
    public function generalize_info_submit(EditGenerlizeRequest $editSeller1Request){
        $wishSellerInfo = new WishGeneralizeInfo();
        $requestInfo = $editSeller1Request->all();
        $wishSellerInfo->add_generalize($requestInfo,2);
        return pageBackSuccess('修改基本信息成功');
    }

    /**
     * 代理资料页面
     */
    private function agent_info(){
        $user = session('admin_info');
        $city_arr = get_ght_attr('city');
        //获取用户信息
        $agent_info=WishAgent::where(['mobile'=>$user['username']])->first();
        //获取代理基本信息
        $basic_info=WishAgentInfo::where(['uid'=>$agent_info['id']])->first();
        return view('admin.wish_person.agent_info',compact('city_arr','agent_info','basic_info'));
    }

    /**
     * 代理资料提交页面
     */
    public function agent_info_submit(EditAgentRequest $editAgentRequest){
        $user = session('admin_info');
        $agent_info=WishAgent::where(['mobile'=>$user['username']])->first();
        $wishSellerInfo = new WishAgentInfo();
        $requestInfo = $editAgentRequest->all();
        $requestInfo['level'] = $agent_info['level'];
        $wishSellerInfo->add_agent($requestInfo,2);
        return pageBackSuccess('修改基本信息成功');
    }

    /**
     * 分公司资料页面
     */
    private function company_info(){
        $user = session('admin_info');
        $city_arr = get_ght_attr('city');
        //获取用户信息
        $company_info=WishCompany::where(['mobile'=>$user['username']])->first();
        //获取分公司基本信息
        $basic_info=WishCompanyInfo::where(['uid'=>$company_info['id']])->first();

        return view('admin.wish_person.company_info',compact('city_arr','company_info','basic_info'));
    }

    /**
     * 分公司资料提交页面
     */
    public function company_info_submit(EditCompanyRequest $editSeller1Request){
        $wishSellerInfo = new WishCompanyInfo();
        $requestInfo = $editSeller1Request->all();
        $wishSellerInfo->add_company($requestInfo,2);
        return pageBackSuccess('修改基本信息成功');
    }

}