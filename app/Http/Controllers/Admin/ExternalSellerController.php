<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AddSeller1Request;
use App\Models\BankList;
use App\Models\Wish\WishSellerInfo;
use Illuminate\Http\Request;

/**
 * 商户对外的操作类
 * Class OfflineController
 * @package App\Http\Controllers\Admin
 */
class ExternalSellerController extends Controller
{
    /**
     * 扫码添加商户
     */
    public function add(Request $request){
        $seller_mobile = session('seller_mobile');
        $supeerior_mobile = session('invite_mobile');

        //银行列表
        $bank_list=BankList::get();
        //经营类别列表
        $category_arr = get_ght_attr('category');
        //去掉名字中的英文名
        foreach ($category_arr as $value){
            $arr = explode(" ",$value->categoryName);
            $value->categoryName = $arr[1];
        }
        //银行代码列表
        $bank_arr = array();
        $bank_arr1 = get_ght_attr('bank');
        foreach ($bank_arr1 as $item => $value){
            foreach ($bank_list as $k => $v){
                if ($v['bank_name'] == $value->bankName){
                    $bank_arr[$item]->bankCode =  $value->bankCode;
                    $bank_arr[$item]->bankName =  $value->bankName;
                }
            }
        }
        return view('admin.external_seller.add',compact('supeerior_mobile','bank_list','category_arr','bank_arr','seller_mobile'));
    }

    /**
     * 添加商户
     */
    public function add_submit(AddSeller1Request $addSellerRequest){
        $wishSellerInfo = new WishSellerInfo();
        $requestInfo = $addSellerRequest->all();
//        dd($requestInfo);
        $wishSellerInfo->add_seller($requestInfo,1,$requestInfo['invite_mobile']);
//        return pageBackSuccess('商家入驻成功');

        return mobileView('success', '商家入驻成功', '商家入驻成功');
    }


}