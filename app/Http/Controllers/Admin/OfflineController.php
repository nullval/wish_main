<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\AdminDividend;
use App\Models\AdminRole;
use App\Models\Wish\WishAgent;
use App\Models\Wish\WishCompany;
use App\Models\Wish\WishGeneralize;
use App\Models\Wish\WishSeller;
use Illuminate\Http\Request;

/**
 * 对未特商城进行返回比例
 * Class OfflineController
 * @package App\Http\Controllers\Admin
 */
class OfflineController extends Controller
{
    //抵扣券分红比例获取
    public function voucher_dividend(Request $request){
        $sellerList = [];
        //得到商家的手机号码
        $sellerId = $request->input('seller_id');
        //检测商家是否存在
        $sellerInfo = WishSeller::where('id',$sellerId)->first();
        if(empty($sellerInfo)){
            return arr_post(0,'该商家不存在');
        }
        $mobile = $sellerInfo['mobile'];
        //查询分红的比例
        $dividends = AdminDividend::get()->toArray();
        //设置商家比例
        $dividendInfo = $this->getDividendsIndex(AdminRole::ROLE_SELLER,$dividends);
        $sellerList[] = [
            'mobile'=>$mobile,
            'type'=>AdminRole::ROLE_SELLER,
            'type_text'=>'商家',
            'proportion'=>$dividendInfo['voucher_dividend']
        ];
        //设置直推人比例
        $generlize = WishSeller::where(['mobile'=>$mobile])->first()->wish_generalize;
        $dividendInfo = $this->getDividendsIndex(AdminRole::ROLE_GENERALIZE,$dividends);
        $sellerList[] = [
            'mobile'=>$generlize['mobile'],
            'type'=>AdminRole::ROLE_GENERALIZE,
            'type_text'=>'直推人',
            'proportion'=>$dividendInfo['voucher_dividend']
        ];
        //设置代理比例
        $agent = WishAgent::where('id',$generlize['inviter_id'])->first();
        //根据代理等级得到指定的角色类型
        $roleId = AdminRole::getAgentRoleIdByAgentLevel($agent['level']);
        $dividendInfo = $this->getDividendsIndex($roleId,$dividends);
        $sellerList[] = [
            'mobile'=>$agent['mobile'],
            'type'=>$roleId,
            'type_text'=>WishAgent::AGENT_LEVEL[$agent['level']],
            'proportion'=>$dividendInfo['voucher_dividend']
        ];
        //设置分公司代理比例
        $company = WishCompany::where('id',$agent['inviter_id'])->first();
        $dividendInfo = $this->getDividendsIndex(AdminRole::ROLE_COMPANY,$dividends);
        $sellerList[] = [
            'mobile'=>$company['mobile'],
            'type'=>AdminRole::ROLE_COMPANY,
            'type_text'=>'分公司',
            'proportion'=>$dividendInfo['voucher_dividend']
        ];
        return arr_post(1,'获取抵扣券分红比例成功',$sellerList);
    }

    //引流分红比例获取
    public function dividend(Request $request){
        $sellerList = [];
        //得到商家的手机号码
        $mobile = $request->input('mobile');;
        //检测商家是否存在
        $isExist = WishSeller::where('mobile',$mobile)->count();
        if($isExist != 1){
            return arr_post(0,'该商家不存在');
        }
        //查询分红的比例
        $dividends = AdminDividend::get()->toArray();
        //设置商家比例
        $divide = WishSeller::where(['mobile'=>$mobile])->value('divide');
        $sellerList[] = [
            'mobile'=>$mobile,
            'type'=>AdminRole::ROLE_SELLER,
            'type_text'=>'商家',
            'proportion'=>(100-$divide) //剩余的就是商家比例了
        ];
        //设置直推人比例
        $generlize = WishSeller::where(['mobile'=>$mobile])->first()->wish_generalize;
        $dividendInfo = $this->getDividendsIndex(AdminRole::ROLE_GENERALIZE,$dividends);
        $sellerList[] = [
            'mobile'=>$generlize['mobile'],
            'type'=>AdminRole::ROLE_GENERALIZE,
            'type_text'=>'直推人',
            'proportion'=>$dividendInfo['offline_dividend']
        ];
        //设置代理比例
        $agent = WishAgent::where('id',$generlize['inviter_id'])->first();
        //根据代理等级得到指定的角色类型
        $roleId = AdminRole::getAgentRoleIdByAgentLevel($agent['level']);
        $dividendInfo = $this->getDividendsIndex($roleId,$dividends);
        $sellerList[] = [
            'mobile'=>$agent['mobile'],
            'type'=>$roleId,
            'type_text'=>WishAgent::AGENT_LEVEL[$agent['level']],
            'proportion'=>$dividendInfo['offline_dividend']
        ];
        //设置分公司代理比例
        $company = WishCompany::where('id',$agent['inviter_id'])->first();
        $dividendInfo = $this->getDividendsIndex(AdminRole::ROLE_COMPANY,$dividends);
        $sellerList[] = [
            'mobile'=>$company['mobile'],
            'type'=>AdminRole::ROLE_COMPANY,
            'type_text'=>'分公司',
            'proportion'=>$dividendInfo['offline_dividend']
        ];
        return arr_post(1,'获取比例分红成功',$sellerList);
    }

    /**
     * 根据角色id拿到分红数据
     * @param $dividends
     * @return false|int|string
     */
    public function getDividendsIndex($roleId,$dividends){
        $index = array_search($roleId, array_column($dividends, 'admin_role_id'));
        return $dividends[$index];
    }

//$data['_cmd']='offline_dividend';
//$data['username']='xujinhui';
//$data['sign']=$this->getPosSign($data);
////        $rs=json_decode(curl_post('chqt-api.wish_main.com',$data),true);
////        dd($rs);
//dd(curl_post('chqt-api.wish_main.com',$data));
	
}
