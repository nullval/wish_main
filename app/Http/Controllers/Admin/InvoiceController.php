<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\EditInvoiceRequest;
use App\Models\Agent;
use App\Models\Order;
use App\Models\TicketDetail;
use App\Models\TicketInfo;
use App\Models\TicketOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
	/**
	 * 发票订单列表页面
	 * @return mixed
	 */
	public function invoice_list(Request $request){
		//所属机主id
		$agent_mobile=$request->input('mobile');
		$agent=Agent::where(['mobile'=>$agent_mobile])->first();
		$agent_uid=$agent['id'];

		$invoice_list=TicketOrder::where(function ($query) use ($agent_uid){
			if(!empty($agent_uid)){
				$query->where('ticket_order.agent_uid','like','%'.$agent_uid.'%');
			}
			return $query;
		})
			->leftJoin('ticket_info','ticket_info.id','=','ticket_order.info_id')
			->leftJoin('ticket_detail','ticket_detail.id','=','ticket_order.detail_id')
			->leftJoin('agent','agent.id','=','ticket_order.agent_uid')
			->orderBy('id','desc')
			->select(['ticket_order.*','agent.mobile','ticket_detail.amount','ticket_info.Invoice_header','ticket_detail.Invoice_nature','ticket_detail.method','ticket_detail.status','ticket_detail.remarks'])
			->paginate(15);
		return view('admin.invoice.invoice_list',['invoice_list'=>$invoice_list]);
		
	}
	
	/**
	 * 发票详情
	 * @param Request $request
	 * @return mixed
	 */
	public function invoice_detail(Request $request){
		//发票订单ID
		$id=$request->input('id');
		//发票记录
		$invoice=TicketOrder::where(['id'=>$id])->first();
		//发票详情
		$invoice_detail=TicketDetail::where(['id'=>$invoice['detail_id']])->first();
		//发票信息
		$invoice_info=TicketInfo::where(['id'=>$invoice['info_id']])->first();
		//归属机主信息
		$agent=Agent::where(['id'=>$invoice['agent_uid']])->first();
		//归属订单号和年份
		$order_list=substr($invoice_detail['order_list'],0,strlen($invoice_detail['order_list'])-1);
		$arr = explode(",",$order_list);
		//获取订单列表
		$order_list=[];
		foreach ($arr as $v){
			$arr1=explode("_",$v);
			$order_id=$arr1[0];
			$year=$arr1[1];
			$Order=new Order($year);
			$order_list[]=$Order->where(['id'=>$order_id])->first();
		}
		return view('admin.invoice.invoice_detail',compact('invoice_info','order_list','invoice_detail','invoice','agent'));
		
	}

	/**
	 * 发票编辑页面
	 * @param Request $request
	 * @return mixed
	 */
	public function invoice_edit(Request $request){
		$invoice_id=$request->input('id');
		//发票记录
		$info=TicketOrder::where(['ticket_order.id'=>$invoice_id])
			->leftJoin('ticket_detail','ticket_detail.id','=','ticket_order.detail_id')
			->select(['ticket_order.*','ticket_detail.status','ticket_detail.remarks','ticket_detail.express_number','ticket_detail.method','ticket_detail.isvalid','ticket_detail.Invoice_number','ticket_detail.Logistics_company'])
			->first();
		return view('admin.invoice.invoice_edit',['info'=>$info]);
	}

	/**
	 * 发票编辑信息提交
	 * @param EditInvoiceRequest $editInvoiceRequest
	 * @return mixed
	 */
	public function invoice_edit_submit(EditInvoiceRequest $editInvoiceRequest){
		$invoice_id=$editInvoiceRequest->input('id');
		$remarks=$editInvoiceRequest->input('remarks');
		$Invoice_number=$editInvoiceRequest->input('Invoice_number');
		$method=$editInvoiceRequest->input('method');
		$Logistics_company=$editInvoiceRequest->input('Logistics_company');
		$express_number=$editInvoiceRequest->input('express_number');
		$status=$editInvoiceRequest->input('status');
		$isvalid=$editInvoiceRequest->input('isvalid');

		//判断发票编号的格式
		if (strlen(floor($Invoice_number))<8 || strlen(floor($Invoice_number))>10){
			return back()->withInput($editInvoiceRequest->all())->withErrors(['error'=>'发票编码格式错误！']);
		}

		//修改发票详情信息
		$arr['Invoice_number']=$Invoice_number;
		$arr['method']=$method;
		$arr['Logistics_company']=$Logistics_company;
		$arr['express_number']=$express_number;
		$arr['status']=$status;
		$arr['isvalid']=$isvalid;
		$arr['remarks']=$remarks;
		//1、获取发票订单信息
		$invoice=TicketOrder::where(['id'=>$invoice_id])->first();
		//2、修改
		$result=TicketDetail::where(['id'=>$invoice['detail_id']])->update($arr);
		return back()->with(['status'=>'修改成功！']);

	}
}
