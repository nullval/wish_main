<?php
/**
 * 财务管理
 * User: Administrator
 * Date: 2018/9/7 0007
 * Time: 14:06
 */
namespace App\Http\Controllers\AdminApi;


use App\Models\Wish\WishAdminSession;
use Illuminate\Http\Request;

class FinanceController extends CommonController{


    /**
     * showdoc
     * @catalog 财务相关
     * @title 获取用户的消费记录
     * @description 获取用户的消费记录接口
     * @method post
     * @url api/finance/consume
     * @param _cmd 必选 string finance_consume
     * @param mobile 必选 mobile 入驻商户的手机号码
     * @return {"status":"1","message":"ok","data":''}
     */
    public function consume(Request $request){
//        if($this->adminInfo == null){
//            return api_error('未找到用户信息');
//        }
//
//        $type = $request->input('type', 0);
//        $purseType = $request->input('purse_type', 0);
//        $page = $request->input('page', 1);
//
//        $nodeid = $this->getNodeId();
//
//        //获取流水管理页面数据
//        $data['_cmd'] = 'OfflineShopJoin_wishFinancePage';
//        $data['node_id'] = $nodeid;
//        $data['type'] = $type; //类型全部0,支出1,收入2
//        $data['purse_type'] = $purseType; //0全部,1现金,3代金券,4推荐奖励
//        $data['need'] = 'can_use_num,voucher_balance,transfer_list'; //需要什么数据：can_use_num(钱包)voucher_balance(推广奖励记录)transfer_list(商家流水)用逗号隔开
//
//        $data['page'] = $page++;
//        $postData = get_offline_sign_data($data);
//        $res = json_decode(curl_post(env('SHOP_DOMAIN'), $postData), true);
//        $getData = $res['data'];

    }
}