<?php
namespace App\Http\Controllers\AdminApi;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminApi\LoginRequest;
use App\Models\Admin;
use App\Models\Wish\WishAdminSession;
use App\Services\AdminIdentityDispose;
use Illuminate\Http\Request;

/**
 *    用户信息
 */
class UserController extends CommonController {

    /**
     * showdoc
     * @catalog 用户相关
     * @title 用户登录
     * @description 用户登录接口
     * @method post
     * @url api/user/login
     * @param _cmd 必选 user_login 用户名
     * @param username 必选 string 用户名
     * @param password 必选 string 密码
     * @param identity 必选 int 登录身份:分公司=1代理人=2直推人=3商家=4总公司=5
     * @return {"status": "1","message": "登录成功","data": {"ticket": "5C3C71FB-AF87-81C4-0CFC-2C67B262D4AE"}}
     * @return_param ticket string 用户登录凭证
     */
    public function login(LoginRequest $request){
        $username = $request->input('username');
        $password = $request->input('password');
        $identity = $request->input('identity');
//        $clientSystem = $request->input('identity',null);
//        $ClientSystem = $request->input('identity',null);

        $adminIdentityModel = new AdminIdentityDispose();
        //得到要检查登录的身份
        $identityIds = $adminIdentityModel->getRoleIdByIdentity($identity);
        if(!$identityIds){
            return api_error('未找到合适的登录类型');
        }
        $admin_info = Admin::where(['username' => $username])->whereIn('role_id',$identityIds)->first();
        if (empty($admin_info)) {
            return api_error('用户不存在');
        }
        if ($admin_info['status'] == 0) {
            return api_error('您被禁止登录');
        }
        if (md5(md5($password)) != $admin_info->password) {
            return api_error('密码不正确');
        }else{
            //登录成功创建ticket
            $wishAdminSession = new WishAdminSession();
            $ticket = $wishAdminSession->getTicket($admin_info['id']);
            //登录成功记录信息
            return api_success('登录成功',[
                'ticket'=>$ticket,
            ]);
        }

    }

    /**
     * showdoc
     * @catalog 用户相关
     * @title 退出登录
     * @description 用户登录接口
     * @method post
     * @url api/user/logout
     * @param _cmd 必选 user_logout 用户名
     * @param ticket 必选 string ticket
     * @return {"status":"1","message":"退出成功","data":[]}
     */
    public function logout(Request $request){
        $ticket = $request->input('ticket');
        $wishAdminSession = new WishAdminSession();
        $adminInfo = $wishAdminSession->getAdminInfoByTicket($ticket);
        if(empty($adminInfo)){
            return api_error('退出失败，未找到用户信息');
        }else{
            $wishAdminSession->logout($ticket);
            return api_success('退出成功');
        }
    }
}