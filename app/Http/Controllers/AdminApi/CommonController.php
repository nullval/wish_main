<?php

namespace App\Http\Controllers\AdminApi;

use App\Exceptions\ApiException;
use App\Exceptions\NotLoginException;
use App\Http\Controllers\Controller;
use App\Models\TnetReginfo;
use App\Models\User;
use App\Models\UserTicket;
use App\Models\Wish\WishAdminSession;
use App\Services\AdminIdentityDispose;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    protected $ticket = null;
    protected $adminInfo = null;

    public function __construct(){
//        $request = request();
//        if($request->has('ticket')){
//            $this->ticket = $request->input('ticket');
//            //设置用户信息
//            $this->setAdminInfo();
//        }
    }

    /**
     * 获得node_id
     */
    protected function getNodeId(){
        if($this->ticket == null) return false;
        if($this->adminInfo == null) return false;

        $tnetReginfo = new TnetReginfo();
        $nodeid = $tnetReginfo->getNodeIdByMobile($this->adminInfo['username'])['nodeid'];
        return $nodeid;
    }

    /**
     * 获取用户信息
     */
    private function setAdminInfo(){
        if($this->ticket == null) return false;
        if($this->adminInfo) return $this->adminInfo;
        $wishAdminSession = new WishAdminSession();
        $adminInfo = $wishAdminSession->getAdminInfoByTicket($this->ticket);
        if($adminInfo != null){
            $myAdminInfo = [];
            //角色ID
            $myAdminInfo['role_id'] = $adminInfo['role_id'];
            $adminIdentityDispose = new AdminIdentityDispose();
            //角色类型
            $identity = $adminIdentityDispose->getUserIdentity($myAdminInfo['role_id']);
            $myAdminInfo['identity'] = $identity;
            $myAdminInfo['username'] = $adminInfo['username'];
            $this->adminInfo = $myAdminInfo;
            return  $this->adminInfo;
        }else{
            return false;
        }

    }

}
