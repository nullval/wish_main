<?php
namespace App\Http\Controllers\AdminApi;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminApi\AddSellerRequest;
use App\Models\Admin;
use App\Models\PushQrQrcode;
use App\Models\Wish\WishSeller;
use App\Models\Wish\WishSellerInfo;
use App\Services\AdminIdentityDispose;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * 商家管理接口
 */
class SellerController extends CommonController {

    /**
     * 商家列表
     */
    public function list(Request $request){
//        $user=session('admin_info');
//        $adminIdentity = new AdminIdentityDispose();
//        $identity = $adminIdentity->checkMobileIsIdentity($user['username'],AdminIdentityDispose::IDENTITY_GENERALIZE);
//        if(!$identity){
//            return pageBackSuccess('当前登录用户不是直推人');
//        }
        //商家手机号
//        $mobile=$request->input('mobile');
//        $name=$request->input('name');
//        $agent_mobile=$request->input('agent_mobile');
//        $seller_list=WishSeller::where(function ($query) use ($mobile,$name,$agent_mobile,$identity,$user){
//            if(!empty($mobile)){
//                $query->where('wish_seller.mobile','like','%'.$mobile.'%');
//            }
//            if(!empty($name)){
//                $query->where('a1.merchantName','like','%'.$name.'%');
//            }
//            if(!empty($agent_mobile)){
//                $query->where('a2.mobile','like','%'.$agent_mobile.'%');
//            }
//            //筛选当前直推人下的商家条件
//            if($user['role_id'] != AdminRole::ROLE_CORPORATION && $identity == AdminIdentityDispose::IDENTITY_GENERALIZE){
//                $adminIdentityDispose = new AdminIdentityDispose();
//                $generalizeInfo = $adminIdentityDispose->getInfoByIdentityAndPhone($user['username'],$identity);
//                $query->where('a2.id',$generalizeInfo['id']);
//            }
//            return $query;
//        })
//            ->leftJoin('wish_seller_info as a1','wish_seller.id','=','a1.uid')
//            ->leftJoin('wish_generalize as a2','wish_seller.inviter_id','=','a2.id')
//            ->select(['wish_seller.*',
//                'a1.corpmanName',
//                'a1.status as apply_status',
//                'a1.merchantName'
//                ,'a1.id as basic_id',
//                'a2.mobile as agent_mobile',
//            ])
//            ->orderBy('id','desc')
//            ->paginate(15);
//            return $seller_list;
    }

    /**
     * showdoc
     * @catalog 商家相关
     * @title 商家入驻
     * @description 商家入驻接口
     * @method post
     * @url api/seller/add
     * @param _cmd 必选 string seller_add
     * @param mobile 必选 string 商家手机号
     * @param merchantName 必选 string 商户名称
     * @param password 必选 string 商户密码,不填默认为账户名
     * @param province_code 必选 string 省码
     * @param city_code 必选 string 城市码
     * @param district_code 必选 string 区县码
     * @param merchantAddress 必选 string 详细地址
     * @param average 必选 int 人均消费
     * @param business_time 必选 string 营业时间
     * @param servicePhone 必选 string 客服电话
     * @param category 必选 string 经营类目
     * @param divide 必选 int 商家让利
     * @param businessimage 必选 string 营业执照
     * @param logoimage 必选 string Logo图片
     * @param recommend_mobile 必选 string 推荐人手机号
     * @param banner 必选 json banner图片
     * @return {"status":"1","message":"商家入驻成功","data":[]}
     */
    public function add(AddSellerRequest $request){
        //AddSellerRequest $request
//       $requestInfo = [
//            'mobile'=>'15918642915', //商家手机号
//            'merchantName'=>'我是神仙',//商户名称
//            'password'=>'123456',//商户密码
//             //省市区选择
//            'province_code'=>'130000', //省
//            'city_code'=>'130100', //城市
//            'district_code'=>'130104', //区-县
//            'merchantAddress'=>'我的地址是什么',//详细地址
//            'average'=>'18',//人均消费
//            'business_time'=>'8:00-22:00',//营业时间
//            'servicePhone'=>'15918642914',//客服电话
//            'category'=>'5932',//经营类目
//            'divide'=>'5',//商家让利
//            'businessimage'=>'xxxx', //营业执照
//            'logoimage'=>'xxxx1',//logo图片
//            'recommend_mobile'=>'15918642915', //推荐人手机号
//            'banner'=>'["aaa","bbb"]',//多图，json格式
//            'ss'=>'10829'
//        ];

        //直推人手机号（看看）
        $requestInfo = $request->all();
        $wish_generalize = $requestInfo['recommend_mobile'];
        $wishSellerInfo = new WishSellerInfo();
        new_logger('logger.log','seller_add',$requestInfo);
        return $wishSellerInfo->front_add_seller($requestInfo,$wish_generalize);
    }

    /**
     * showdoc
     * @catalog 商家相关
     * @title 经营类别
     * @description 经营类别接口
     * @method post
     * @url api/seller/manage_list
     * @param _cmd 必选 string seller_manage_list
     * @return {"status":"1","message":"获取成功","data":[['categoryCode':'5813','categoryName':'Drinkingplaces（alcoholicbeverages）—bars,taverns, （酒吧、酒馆、夜总会、鸡尾酒大厅、迪斯科舞厅）']]}
     * @return_param categoryCode string 类别的key值
     * @return_param categoryName string 类别的value值
     */
    public function manage_list(){
        //经营类别列表
        $category_arr = get_ght_attr('category');
        return api_success('获取成功',$category_arr);
    }

    /**
     * showdoc
     * @catalog 商家相关
     * @title 推广手机号
     * @description 推广手机号接口
     * @method post
     * @url api/seller/get_generalize_mobile
     * @param  _cmd 必选 string seller_get_generalize_mobile
     * @param ss 必选 string 用户扫码得到的参数
     * @return {"status":"1","message":"","data":'12345678910'}
     * @return_param data string 推广手机号
     */
    public function get_generalize_mobile(Request $request){
        $ss = $request->input('ss');
        if(empty($ss)){
            return api_error('没有传递参数');
        }
        $num = intval($ss);
        $code_number = PushQrQrcode::getCodeNumber($num);
        $info = PushQrQrcode::where('code_number', $code_number)->first();
        if (empty($info)){
            return api_error('没有找到该条二维码');
        }
        if(empty($info['agent_id'])){
            return api_error('未找到拥有者');
        }
        if($info['state'] != 1){
            return api_error('二维码已经被使用了');
        }
        //查询信息
        $mobile = Admin::where('id',$info['agent_id'])->value('username');
        if(empty($mobile)){
            return api_error('未找到推荐人!');
        }
        return api_success('',['phone'=>$mobile]);
    }

    /**
     * showdoc
     * @catalog 商家相关
     * @title 检查商户是否已经入驻
     * @description 检查商户是否已经入驻接口
     * @method post
     * @url api/seller/check_user_enter
     * @param _cmd 必选 string seller_check_user_enter
     * @param mobile 必选 mobile 入驻商户的手机号码
     * @return {"status":"1","message":"ok","data":''}
     */
    public function check_user_enter(Request $request){
        $mobile = $request->input('mobile');
        if(empty($mobile)){
            return api_error('没有传递参数');
        }
        //检查商户是否已入驻
        $isEnter = WishSeller::where('mobile', $mobile)->count();
        if (!empty($isEnter)) {
            return api_error('商户已入驻');
        }
        return api_success('ok');
    }



}