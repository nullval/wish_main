<?php
namespace App\Http\Controllers\AdminApi;
use App\Http\Controllers\Controller;

/**
 * 其它接口信息
 */
class OtherController extends CommonController {

    /**
     * showdoc
     * @catalog 其它相关
     * @title 图片上传
     * @description 图片上传接口
     * @method post
     * @url api/other/upload_img
     * @param _cmd 必选 string other_upload_img
     * @param imagebase64 必选 string 图片的base64字符串
     * @return {"status":"1","message":"获取成功","data":""}
     * @return_param filepath string 新的文件路径
     */
    public function upload_img(){
        $upRes = upload(request('imagebase64'));
        if($upRes['code'] != 1){
            return api_error($upRes['message']);
        }else{
            return api_success($upRes['message'],$upRes['data']);
        }
    }

}