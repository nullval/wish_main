<?php

namespace App\Http\Controllers;

use App\Models\UserBehavior;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __destruct()
    {
        //Todo 2018年9月5日17:16:00 暂时没有这张表
//        if(request()->all()){
//            $now_time_micro = explode(' ',microtime());
//            $now_time_float = $now_time_micro[1] + $now_time_micro[0];
//            $data = [
//                'url'			=> request()->url(),
//                'execute_time'	=> $now_time_float - request()->server('REQUEST_TIME_FLOAT'),
//                '$_GET'			=> print_r($_GET,true),
//                '$_POST'		=> print_r($_POST,true),
//                '$_REQUEST'		=> print_r(request()->all(),true),
//                '$_SERVER'		=> print_r($_SERVER,true),
//                '$_SESSION'		=> print_r($_SESSION,true),
//                '$_COOKIE'		=> print_r($_COOKIE,true),
//                'remarks' =>explode('.',request()->route()->domain())[0]
//            ];
//            UserBehavior::create($data);
//        }
    }
}
