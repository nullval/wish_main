<?php

namespace App\Http\Controllers\Seller;

use App\Models\Pos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PosController extends Controller
{
	/**
	 * pos机管理页面
	 * @return mixed
	 */
    public function pos_list(){
		$user=session('seller_info');
		$pos_list=Pos::where(['uid'=>$user->id])
            ->leftJoin('agent','pos.agent_uid','=','agent.id')
            ->select(['agent.mobile','pos.*'])
			->orderBy('pos.id','desc')
			->paginate(15);

		return view('seller.pos.pos_list',['pos_list'=>$pos_list]);
	}
}
