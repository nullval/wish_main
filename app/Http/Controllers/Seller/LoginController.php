<?php

namespace App\Http\Controllers\Seller;
use App\Exceptions\MobileErrorException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Seller\LoginRequest;
use App\Http\Requests\Seller\RegisterRequest;
use App\Models\Admin;
use App\Models\Merchant_apply;
use App\Models\Message;
use App\Models\Seller;
use App\Models\SellerPermission;
use App\Models\User;
use Illuminate\Http\Request;
//引用对应的命名空间
use Gregwar\Captcha\CaptchaBuilder;
use Session;


class LoginController extends Controller
{

    /**
     * 注册页面
     */
    public function register(){

        return view('seller.login.register');
    }

    /**
     * 登录页面
     */
    public function login(Request $request){
        if(empty($_COOKIE['openid'])&&config('app.env')=='online'){
            throw new MobileErrorException('请关注公众号');
        }
        if(!empty($_COOKIE['openid'])){
            $openid=$_COOKIE['openid'];
            $seller_info=Seller::where(['openid'=>$openid])->first();

            //2012/12/31号商家特殊处理
            //其财务，店长，收银都可以登录
            $special_arr=['18028721647','13407041762','18420442201','15815808982','15916598554'];
            $user_info=User::where(['openid'=>$openid])->first();
            if(isset($user_info->id)){
                $user_mobile=$user_info->mobile;
                if(in_array($user_mobile,$special_arr)){
                    //找出特殊商家
                    $seller_info=Seller::where(['mobile'=>'15927578671'])->first();
                }
            }

            if(isset($seller_info->id)){
                //session存储用户信息
                session(['seller_info'=>$seller_info]);
                session(['role_info'=> '']);
                session(['seller_name'=> '']);
                return redirect('/settle/apply');
            }
            return view('seller.login.login');
        }
        return view('seller.login.login');
//        echo '请关注未特商城公众号平台后,再进行登录';
    }


    /**
     * 提交登录
     */
    public function login_submit(LoginRequest $request){
        $mobile=$request->input('mobile');
        $sms_code=$request->input('sms_code');
        $openid=$request->input('openid');
        $user =new Seller();
        $return_arr=$user->seller_login($mobile,$sms_code,$openid);
//        var_dump($return_arr);exit;
        return $return_arr['code']==0?back()->withInput($request->all())->withErrors(['error'=>$return_arr['message']]):redirect(get_seller_jump_url());
    }

    /**
     * 免登录操作
     */
    public function nologin(Request $request){
        $id=base64_decode($request->input('code'));
        $user_info=Seller::where(['id'=>$id])->first();
        //session存储用户信息
        session(['seller_info'=>$user_info]);
        //获取权限列表
        $permission_list=SellerPermission::select('*')->orderBy('sort','DESC')->get();
        //格式化菜单
        $SellerPermission=new SellerPermission();
        $permission_list=$SellerPermission->sort_permission($permission_list);
        //session存储用户权限
        session([$user_info->id.'_permission_list'=>$permission_list]);
        session(['role_info'=> '']);
        session(['seller_name'=> '']);
        return redirect('/settle/apply');
    }

    /**
     * 退出登录
     */
    public function logout(Request $request){
        $request->session()->forget('seller_info');
        return redirect('/login');
    }

    /**
     * 发送注册短信验证码
     */
    public function send_sms(Request $request){
        $mobile=$request->input('mobile');
        $type=$request->input('type');
        if($type==5){
            //判断是否是商家
            $seller_info=Seller::where(['mobile'=>$mobile])->first();
//            if (empty($seller_info)){
//                return json_error('您不是商家,无法发送短信！');
//            }
        }
        $Message=new Message();
        return $Message->send_sms($mobile,$type);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function captcha($tmp)
    {
        //生成验证码图片的Builder对象，配置相应属性
        $builder = new CaptchaBuilder;
        //可以设置图片宽高及字体
        $builder->build($width = 100, $height = 40, $font = null);
        //获取验证码的内容
        $phrase = $builder->getPhrase();

        //把内容存入session
        Session::flash('milkcaptcha', $phrase);
        //生成图片
        header("Cache-Control: no-cache, must-revalidate");
        header('Content-Type: image/jpeg');
        $builder->output();
    }


    /**
     * 提交注册
     */
    public function register_submit(RegisterRequest $request){
        $mobile=$request->input('mobile');
        $code=$request->input('code');
        $openid=$request->input('openid');
        $password=$request->input('password');
        //验证码验证
        $return_arr=$this->check_code($code);
        if($return_arr['code']==0) return back()->withInput($request->all())->withErrors(['error'=>$return_arr['message']]);

        $User=new Seller();
        $return_arr=$User->seller_register($mobile,$password,$openid);
        //错误信息提示
        if($return_arr['code']==0) return back()->withInput($request->all())->withErrors(['error'=>$return_arr['message']]);
        return redirect('/settle/apply')->with(['status'=>'注册成功！']);

    }

    /**
     * 验证图形验证码
     */
    public function check_code($code){
        $captcha = Session::get('milkcaptcha');
//        var_dump($captcha);exit;
//        var_dump($request->input('captcha'));exit;
        if($code==$captcha){
            return json_success('图形验证码正确');
        }else{
            return json_error('图形验证码错误');
        }
    }


}
