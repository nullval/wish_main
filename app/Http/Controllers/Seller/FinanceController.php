<?php

namespace App\Http\Controllers\Seller;

use App\Http\Requests\Seller\WithdrawGoodsRequest;
use App\Http\Requests\Seller\WithdrawRequset;
use App\Jobs\checkPay;
use App\Libraries\gaohuitong\Ruzhu;
use App\Libraries\Huanxun\Huanxun;
use App\Models\Bank;
use App\Models\Df;
use App\Models\FreezeHis;
use App\Models\JavaIncrement;
use App\Models\MerchantApply;
use App\Models\Order;
use App\Models\RuzhuMerchantBank;
use App\Models\RuzhuMerchantBasic;
use App\Models\Seller;
use App\Models\Statement;
use App\Models\SysConfig;
use App\Models\Transfer;
use App\Models\User;
use App\Models\UserBank;
use App\Models\UserPurse;
use App\Models\UserWithdraw;
use App\Models\VTransfer;
use App\Models\VUserWithdraw;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class FinanceController extends Controller
{
	/**
	 * 查看商家财务页面
	 */
	public function index(Request $request){
		//币种类型
		$type=$request->input('type');
		$user_id=session('seller_info')->id;
		//获取用户注册值钱包
		$Bank=new Bank();
        $owner_type=2;

        if($type!=0){
            //根据币种类型获取相应的钱包id
            $purse_id=$Bank->userWallet(session('seller_info')->id,$type,$owner_type)->purse_id;
        }else{
            $purse_arr=norm_new_arr(UserPurse::where(['owner_id'=>session('seller_info')->id,'owner_type'=>$owner_type])->get(['purse_id']),'purse_id');
        }
		//0全部 1收入 2支出
		$status=$request->input('status');
        $sql='';
        for ($i=2017;$i<=date('Y');$i++){
            $sql .= "select * from transfer_".$i;
            if($status==0){
                if($type!=0){
                    $sql.=" where into_purse_id=".$purse_id." or out_purse_id=".$purse_id." ";
                }else{
                    $sql.=" where into_purse_id in (".implode(',',$purse_arr).") or out_purse_id in (".implode(',',$purse_arr).") ";
                }
            }elseif($status==1){
                if($type!=0){
                    $sql.=" where into_purse_id=".$purse_id." ";
                }else{
                    $sql.=" where into_purse_id in (".implode(',',$purse_arr).") ";
                }
            }elseif($status==2){
                if($type!=0){
                    $sql.=" where out_purse_id=".$purse_id." ";
                }else{
                    $sql.=" where out_purse_id in (".implode(',',$purse_arr).") ";
                }
            }
            $sql.=' and status=0 ';
            if ($i != date('Y')){
                $sql = $sql." union ";
            }
        }
		$transfer=DB::table(DB::raw("(".$sql.") as t"))
//            ->whereNotIn('reason',[10070,10064])
            ->orderBy('t.create_time','desc')
            ->orderBy('t.transfer_id','desc')
			->paginate(15);
		//字段处理
		foreach($transfer as $k=>$v){
			if($v->into_owner_id==$user_id){
                $transfer[$k]->purse_type_name=get_purse_type_name($v->into_purse_id);
			}else{
                $transfer[$k]->purse_type_name=get_purse_type_name($v->out_purse_id);
			}
		}
		//用户信息
		$data['seller_info']=Seller::where(['seller.id'=>$user_id])->first();
		//钱包信息
        //收益钱包
        $data['x_purse_info']=$Bank->userWallet($user_id,1,2);
        //货款钱包
        $data['h_purse_info']=$Bank->userWallet($user_id,4,2);
		//积分钱包
        $data['j_purse_info']=$Bank->userWallet($user_id,3,2);
        //待返还补贴积分
        $data['de_purse_info']=$Bank->userWallet($user_id,10001,2);
        //
        $data['x_purse_info']=$Bank->userWallet($user_id,1,2);
        //商家充值钱包
        $data['b_purse_info']=$Bank->userWallet($user_id,7,2);
        //微信钱包
//        $data['w_purse_info']=$Bank->userWallet($user_id,8,2);

		$data['purse_id']=$purse_id;
        $data['purse_arr']=$purse_arr;
		return view('seller.finance.finance',['transfer'=>$transfer,'data'=>$data]);
	}
	
	/**
	 * 提现列表页面
	 * @param Request $request
	 * @return mixed
	 */
	public function withdraw_list(Request $request){
		$uid=session('seller_info')['id'];
        $status=$request->input('status');
        $purse_type=$request->input('purse_type');
		$user_withdraw=VUserWithdraw::where(function ($query) use ($status,$purse_type){
            if(!empty($status)){
                $query->where(['v_user_withdraw.status'=>$status]);
            }
            if(!empty($purse_type)){
                $query->where(['v_user_withdraw.purse_type'=>$purse_type]);
            }
            return $query;
        })
            ->where(['v_user_withdraw.user_id'=>$uid,'member_type'=>2])
            ->whereIn('v_user_withdraw.purse_type',[1,4,7,8,9])
			->leftJoin('user','user.id','=','v_user_withdraw.user_id')
			->orderBy('created_at','desc')
			->select(['v_user_withdraw.*','user.mobile'])
			->paginate(15);
//		dd($user_withdraw);
		return view('seller.finance.withdraw_list',['user_withdraw'=>$user_withdraw]);
	}

	/**
	 * 提现页面
	 * @return mixed
	 */
	public function withdraw(){
		$uid=session('seller_info')['id'];
		//获取用户钱包信息
		$Bank=new Bank();
		$pursetype=\request('pursetype') ?? 1;
		$purse=$Bank->userWallet($uid,$pursetype,2);
//		if($pursetype==1){
            //获取商家银行卡信息
            $bank_info=UserBank::where(['uid'=>$uid,'type'=>2,'isdefault'=>1])->first();
//        }else{
//            //微信支付宝从环讯获取
//            $Huanxun=new Huanxun();
//            $data=$Huanxun->querybankcard([
//                'customerCode'=>session('seller_info')['mobile']
//            ]);
//            if($data){
//                $bank_info=(object)[
//                    'account_bank'=>$data['bankName'],
//                    'bank_account'=>$data['bankCard'],
//                    'account_name'=>RuzhuMerchantBank::where(['merchantId'=>session('seller_info')['mobile']])->value('name'),
//                ];
//            }else{
//                $bank_info=(object)[];
//            }
//        }
		return view('seller.finance.withdraw',['purse'=>$purse,'bank_info'=>$bank_info]);

	}

	/**
	 * 获取银行代码
	 * @param Request $request
	 * @return mixed
	 */
	public function getbankcode(Request $request){
		$data=$request->all();
		$Df=new Df();
		return $Df->getbankcode($data['bank_name']);

	}
	
	/**
	 * 提现申请
	 * @param WithdrawRequset $withdrawRequset
	 * @return mixed
	 */
	public function subwithdraw(WithdrawRequset $withdrawRequset){
		//判断账户的启用状态
		$data=$withdrawRequset->all();
        $uid=session('seller_info')->id;
        $UserWithdraw=new UserWithdraw();
        if(in_array($data['pursetype'],[1,3,4,7])){
            $UserWithdraw->sub_withdraw($uid,2,$data['pursetype'],$data['amount']);
        }
        return pageBackSuccess('您的申请已受理');


	}




    /**
     * 货款提现列表页面
     * @param Request $request
     * @return mixed
     */
    public function withdraw_goods_list(Request $request){
        $uid=session('seller_info')['id'];
        $status=$request->input('status');
//        $year=empty($request->input('year'))?date('Y'):$request->input('year');
//        $user_withdraw_table='user_withdraw_'.$year;
        $user_withdraw=VUserWithdraw::where(function ($query) use ($status){
            if(!empty($status)){
                $query->where(['v_user_withdraw.status'=>$status]);
            }
            return $query;
        })
            ->where(['v_user_withdraw.user_id'=>$uid,'member_type'=>2,'v_user_withdraw.purse_type'=>4])
            ->leftJoin('user','user.id','=','v_user_withdraw.user_id')
            ->orderBy('created_at','desc')
            ->select(['v_user_withdraw.*','user.mobile'])
            ->paginate(15);
//		dd($user_withdraw);
        return view('seller.finance.withdraw_goods_list',['user_withdraw'=>$user_withdraw]);
    }

    /**
     * 货款提现页面
     * @return mixed
     */
    public function withdraw_goods(){
        $merchantId=session('seller_info')->child_merchant_no;
		$uid=session('seller_info')->id;
		//获取商家银行卡信息并判断是否登记银行卡信息
		$bank_info=RuzhuMerchantBank::where(['merchantId'=>$merchantId])->first();
		if (empty($bank_info)){
			return back()->withErrors(['error'=>'您的银行信息还没登记，请尽快去登记！']);
		}
		//判断商家是否有提现资格
		$basic_info=RuzhuMerchantBasic::where(['merchantId'=>$merchantId,'uid'=>$uid])->first();
		if ($basic_info->status!=3){
			return back()->withErrors(['error'=>'您的入驻信息还没审核通过，请等审核通过在提现！']);
		}
		//获取用户钱包信息
        $Bank=new Bank();
        $purse=$Bank->userWallet($uid,4,2);
		//获取高汇通账户余额
		$Ruzhu=new Ruzhu();
		$query=$Ruzhu->queryBalance($merchantId);
		if($query['head']['respType']=='E'){
			$balance=0;
		}elseif ($query['head']['respType']=='S'){
			$balance=$query['body']['balanceAmount']-$query['body']['freezeAmount'];
		}
        return view('seller.finance.withdraw_goods',['purse'=>$purse,'bank_info'=>$bank_info,'balance'=>$balance]);

    }


    /**
     * 货款提现申请
     * @param WithdrawRequset $withdrawGoodsRequest
     * @return mixed
     */
    public function sub_goods_withdraw(WithdrawGoodsRequest $withdrawGoodsRequest){
        //判断账户的启用状态
        $data=$withdrawGoodsRequest->all();
        $sessionuser=session('seller_info');
        $user=Seller::where(['id'=>$sessionuser->id])->first();
        if ($user->status==0){
            $error='您的账户被禁止!';
            return back()->withInput($withdrawGoodsRequest->all())->withErrors(['error'=>$error]);
        }
        //判断账户的提现资格
        if ($user->is_allow_withdraw==2){
            $error='您的账户禁止提现!';
            return back()->withInput($withdrawGoodsRequest->all())->withErrors(['error'=>$error]);
        }
        $UserWithdraw=new UserWithdraw();
        $UserWithdraw->goods_sub_withdraw($sessionuser->id,$sessionuser->child_merchant_no,$data['amount']);
        return pageBackSuccess('您的申请已受理，1工作日内将会完成审核打款。');

    }



    public function exchange(Request $request){
        $uid=session('seller_info')['id'];
        //获取用户钱包信息
        $Bank=new Bank();
        $purse=$Bank->userWallet($uid,3,2);
        //获取商家信息
        $seller_info=Seller::where(['id'=>$uid])->first();

        $JavaIncrement = new JavaIncrement();
        $nodeId = $JavaIncrement->get_seller_node_id(session('seller_info')['mobile']);

        //页码
        $perPage=15;
        //当前页码
        $current_page=$request->input('page');
        $current_page=empty($current_page)?1:$current_page;
        //请求java(需要总条数和页码 数据)
        $param=[
            'u_id'=>$nodeId,
            'pageSize'=>$perPage,
            'curPage'=>$current_page,
        ];
        $data=$JavaIncrement->change_over('/api/json/user/order/getIncrementList.do',$param,'get')['data'];
        $exchange_list=$data['infos'];
        //总数
        $total=$data['number'];
        $item=array_slice($exchange_list,0,$perPage);
        $paginator=new LengthAwarePaginator($item,$total,$perPage,$current_page,[
            'path'=>Paginator::resolveCurrentPath(),
            'pageName'=>'page'
        ]);
        $new_exchange_list=$paginator->toArray()['data'];
        foreach ($new_exchange_list as  $k=>$v){
            $detail='订单编号'.$v['orderNum'].'。';
//            $new_exchange_list[$k]['profit']=VTransfer::where(['reason'=>10071,'into_owner_id'=>$uid])
//                ->where(['v_transfer.status'=>0])
//                ->where('detail','like','%'.$detail.'%')
//                ->sum('into_amount');
//            if(get_last_three_num($new_exchange_list[$k]['profit'])==get_last_three_num($v['getInte'])){
                $new_exchange_list[$k]['profit']=$v['getInte'];
//            }
            $new_exchange_list[$k]['radio']=get_last_two_num($new_exchange_list[$k]['profit'])/get_last_two_num($v['getInte'])*100;
        }
        return view('seller.finance.exchange',['purse'=>$purse,
            'seller_info'=>$seller_info,
            'config_data'=>$data,
            'new_exchange_list'=>$new_exchange_list,
            'paginator'=>$paginator,
            'total'=>$total
        ]);
    }

    public function exchange_submit(Request $request){
        $seller_uid=session('seller_info')['id'];
        $mobile=session('seller_info')['mobile'];
        $type=$request->input('type');
        //获取用户可用积分
        $Bank=new Bank();
        $amount=$Bank->use_money($seller_uid,3,2);
//        var_dump($amount);exit;
        if($amount<1){
            return back()->withErrors(['error'=>'额度不能小于1个']);
        }
        //获取用户id
        $User=new User();
        $result=$User->get_user_id_by_mobile($mobile,1);
        $user_id=$result['data']['user_id'];
        //强制挂两位小数的积分  java要求 保持精度
        $amount=get_num($amount);

        //自动挂单
        if($amount>0){
            if($type==1){
                $JavaIncrement = new JavaIncrement();
                $rs=$JavaIncrement->automatic_guadan($mobile,$amount,$user_id,4);
                $mark='手动兑换挂单成功';
            }else{
                $JavaIncrement = new JavaIncrement();
                $rs=$JavaIncrement->jfConversionTd($mobile,$amount,$user_id,4);
                $mark='积分增值挂单成功';
            }
        }
        if($rs['success']!=true){
            return back()->withErrors(['error'=>$rs['errMsg']]);
        }
        return back()->with('status',$mark);

    }


    /**
     * 返现对账单列表
     * @param Request $request
     * @return mixed
     */
    public function statement_list(Request $request){
        $seller_id=session('seller_info')['id'];
        //获取商家信息
        $seller_info=Seller::where(['id'=>$seller_id])->first();
        //获取该商家的返现列表
        $statement_list=Statement::where(['seller_uid'=>$seller_id])
            ->orderBy('id','desc')
            ->paginate(15);
        return view('seller.finance.statement_list',['statement_list'=>$statement_list,'seller_info'=>$seller_info]);

    }

    /**
     * 返现记录详情
     * @param Request $request
     * @return mixed
     */
    public function statement_details(Request $request){
        $statement_id=$request->input('id');
        //返现记录信息
        $statement_info=Statement::where(['id'=>$statement_id])->first();
        //归属商家信息
        $seller_info=Seller::where(['id'=>$statement_info['seller_uid']])->first();
        //归属订单号
        $arr = explode(",",$statement_info['order_list']);
        //根据返现记录的创建时间获取年份
        if(date('m-d',strtotime($statement_info['created_at']))=='01-01'){
            //如果今天刚好是1月1日则选择去年的表
            $year=date('Y',strtotime($statement_info['created_at']))-1;
        }else{
            //若不是则选择今年的表
            $year=date('Y',strtotime($statement_info['created_at']));
        }
        //根据年份查询相应的表格得出订单列表
        $Order=new Order($year);

        $order_list =$Order->whereIn ('order_sn',$arr)
            ->orderBy('id','desc')
            ->paginate(15);
        return view('seller.finance.statement_detail',compact('statement_info','seller_info','order_list'));
    }


    /**
     * 返现记录详情
     * @param Request $request
     * @return mixed
     */
    public function statement_finance_details(Request $request){
        $statement_id=$request->input('id');
        $seller_id=session('seller_info')['id'];
        //返现记录信息
        $statement_info=Statement::where(['id'=>$statement_id])->first();
        $Bank=new Bank();
        //获取商家积分和收益钱包
        $purse_id=$Bank->userWallet($seller_id,1,2)->purse_id;
        $point_purse_id=$Bank->userWallet($seller_id,3,2)->purse_id;
        $order_list=VTransfer::whereIn('into_purse_id',[$purse_id,$point_purse_id])
            ->where(['v_transfer.status'=>0])
            ->whereIn('reason',[10021,10022])
            ->where('detail','like','%'.'对账单编号:'.$statement_id.'%')
            ->orderBy('create_time','desc')
            ->orderBy('transfer_id','desc')
            ->paginate(15);


        return view('seller.finance.statement_finance_detail',compact('statement_info','seller_info','order_list'));
    }


    /**
     * 商家每日补贴获得列表
     */
    public function new_statement_list(){
        $seller_id=session('seller_info')['id'];
        $Bank=new Bank();
        //获取商家积分和收益钱包
        $purse_id=$Bank->userWallet($seller_id,1,2)->purse_id;
        $point_purse_id=$Bank->userWallet($seller_id,3,2)->purse_id;
        $sql='';
        for ($i=2017;$i<=date('Y');$i++){
            $sql .= "select FROM_UNIXTIME(create_time,'%Y-%m-%d') as datetime,Sum(into_amount) AS amount,reason from transfer_".$i;
            $sql.=" WHERE into_purse_id IN (".$purse_id.",".$point_purse_id.") AND status=0 AND reason in (10021,10022) GROUP BY FROM_UNIXTIME(create_time,'%Y-%m-%d'),reason";
            if ($i != date('Y')){
                $sql = $sql." union ";
            }
        }

        $list=DB::table(DB::raw("(".$sql.") as t"))
            ->orderBy('t.datetime','desc')
            ->paginate(15);

        return view('seller.finance.new_statement_list',['list'=>$list]);
    }

    /**
     * 商家每日补贴获得详情
     */
    public function new_statement_details(Request $request){
        $date=$request->input('date');
        $seller_id=session('seller_info')['id'];
        $Bank=new Bank();
        $purse_id=$Bank->userWallet($seller_id,1,2)->purse_id;
        $point_purse_id=$Bank->userWallet($seller_id,3,2)->purse_id;
        $sql='';
        for ($i=2017;$i<=date('Y');$i++){
            $sql .= "select * from transfer_".$i;
            $sql.=" WHERE into_purse_id IN (".$purse_id.",".$point_purse_id.") AND status=0 AND reason in (10021,10022) AND FROM_UNIXTIME(create_time,'%Y-%m-%d')='".$date."'";
            if ($i != date('Y')){
                $sql = $sql." union ";
            }
        }
        $list=DB::table(DB::raw("(".$sql.") as t"))
            ->paginate(15);
        $data['amount']=get_last_two_num(DB::table(DB::raw("(".$sql.") as t"))->where(['reason'=>10022])->sum('into_amount'));
        $data['point']=get_last_two_num(DB::table(DB::raw("(".$sql.") as t"))->where(['reason'=>10021])->sum('into_amount'));
//        var_dump($list);exit;
        return view('seller.finance.new_statement_detail',['list'=>$list,'data'=>$data]);
    }

    /**
     * @param Request $request
     * @return array
     * 修改商家自动挂单
     */
    public function change_automatic(Request $request){
        $uid=session('seller_info')['id'];
        $is_automatic=$request->input('is_automatic');
        Seller::where(['id'=>$uid])->update(['is_automatic'=>$is_automatic]);
        return json_success('修改成功');
    }


    public function exchange_list(Request $request){
        $data['_cmd'] = 'user_checkMobile';
        $data['mobile'] = session('seller_info')['mobile'];
        $data['sign'] = get_shop_pos_sign($data);
        $res = json_decode(curl_post(env('SHOP_DOMAIN'), $data), true);
        $nodeId = $res['data']['node_id'];

        //类型 1手动兑换 2积分增值 3全部
//        $orderType=empty($request->input('orderType'))?3:$request->input('orderType');
        //页码
        $perPage=15;
        //当前页码
        if($request->has('page')){
            $current_page=$request->input('page');
            $current_page=$current_page<=0?1:$current_page;
        }
        $current_page=empty($current_page)?1:$current_page;
        //请求java(需要总条数和页码 数据)
        $param=[
            'u_id'=>$nodeId,
            'pageSize'=>$perPage,
            'curPage'=>$current_page,
            'orderType'=>2,
            //只显示卖出
//            'bsFlag'=>2
        ];
        $JavaIncrement = new JavaIncrement();
        $data=$JavaIncrement->change_over('api/json/user/order/listPage.do',$param,'get')['data'];
        $exchange_list=$data['infos'];
        //总数
        $total=$data['number'];
        $item=array_slice($exchange_list,0,$perPage);
        $paginator=new LengthAwarePaginator($item,$total,$perPage,$current_page,[
            'path'=>Paginator::resolveCurrentPath(),
            'pageName'=>'page'
        ]);
        $new_exchange_list=$paginator->toArray()['data'];
        return view('seller.finance.exchange_list',compact('new_exchange_list','paginator','total'));
    }
}
