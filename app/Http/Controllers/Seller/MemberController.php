<?php

namespace App\Http\Controllers\Seller;

use App\Http\Requests\Seller\ApplyRequest;
use App\Http\Requests\Seller\BannerAddRequest;
use App\Libraries\gaohuitong\Ruzhu;
use App\Libraries\SOCKET_UPLOAD;
use App\Models\Bank;
use App\Models\BankList;
use App\Models\MerchantApply;
use App\Models\Pos;
use App\Models\RuzhuMerchantBank;
use App\Models\RuzhuMerchantBasic;
use App\Models\RuzhuMerchantBusinesss;
use App\Models\Seller;
use App\Models\SellerPermission;
use App\Models\SellerRole;
use App\Models\SocketUpload;
use App\Models\User;
use App\Models\UserBank;
use File2base64\File2base64;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Libraries\YZH\PayV2Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class MemberController extends CommonController
{
    /**
     * 商家入驻信息页面
     * @return mixed
     */
    public function apply(){
        //获取账户信息
        $user=session('seller_info');
        $Ruzhu=new Ruzhu();
        $bank_list=BankList::get();
        $city_arr = get_ght_attr('city');
        $category_arr = get_ght_attr('category');
        $bank_arr = get_ght_attr('bank');
        //获取用户信息
        $seller_info=Seller::where(['id'=>$user->id])->first();
        $role = SellerRole::where(['mobile' => session('role_info')->mobile]) -> first();
        //判断是否是操作员进入
        if (empty($role)){
            //获取权限列表
            $permission_list=SellerPermission::select('*')->orderBy('sort','DESC')->get();
            //格式化菜单
            $SellerPermission=new SellerPermission();
            $permission_list=$SellerPermission->sort_permission($permission_list);
            //session存储用户权限
            session([$seller_info->id.'_permission_list'=>$permission_list]);
        }

        //获取商家基本信息
        $basic_info=RuzhuMerchantBasic::where(['uid'=>$user->id,'merchantId'=>$seller_info->child_merchant_no])->first();
        //获取商家银行卡信息
        $bank_info=RuzhuMerchantBank::where(['merchantId'=>$seller_info->child_merchant_no])->first();
        //获取商家的业务信息
        $busi_info=RuzhuMerchantBusinesss::where(['merchantId'=>$seller_info->child_merchant_no])->first();
        //获取超级代付银行卡信息
        $bank=UserBank::where(['uid'=>$seller_info->id,'type'=>2])->first();
        //支付平台业务列表
        $busi_list=$Ruzhu->queryBusi($seller_info->child_merchant_no)['body']['busiList'];
        return view('seller.settle.apply',compact('bank_list','city_arr','category_arr','bank_arr','seller_info','basic_info','bank_info','busi_info','bank','busi_list'));

    }



    /**
     * 商家入驻信息提交
     * @param ApplyRequest $applyRequest
     * @return mixed
     */
    public function apply_submit(ApplyRequest $applyRequest){
        $request_info=$applyRequest->all();
//var_dump($request_info);exit;
		$file = Input::file('businessimage');
        if(empty($file)){
			return back()->withInput($request_info)->withErrors(['error'=>'请上传营业执照']);
        }

        $size = strlen(file_get_contents($request_info['businessimagebase64']))/1024;
        if($size>1024){
            return back()->withInput($request_info)->withErrors(['error'=>'请上传不超过1M的图片']);
        }
        $filebase64=$request_info['businessimagebase64'];
        $arr['img']=$filebase64;
        $arr['AppId']=env('APPID');
        $arr['SafeCode']=env('SAFECODE');
        $result=json_decode(curl_post(env('IMGURL'),$arr),'true');
        if($result['code']==0){
            return back()->withInput($request_info)->withErrors(['error'=>$result['message']]);
        }else{
            $request_info['businessimage']=$result['data']['file'];
            unset($request_info['businessimagebase64']);
        }

//        $businessimage=$this->upload('businessimage','Businesspicture');
//        $whole_businessimage=public_path().$request_info['businessimage'];

        $request_info['issub']=1;
        $request_info['status']=2;
        $request_info['apply_time']=time2date();
        MerchantApply::where(['id'=>$applyRequest->input('id')])->update(filter_update_arr($request_info));

//        $user=session('seller_info');
//        $info=MerchantApply::where(['uid'=>$user->id])
//            ->leftJoin('user','user.id','=','merchant_apply.uid')
//            ->select(['merchant_apply.*','user.mobile'])
//            ->first();
//
//        return view('seller.settle.apply',compact('info'))->with(['status'=>'处理成功']);


        return back()->with(['status'=>'处理成功']);

    }

    /**
     * 我的台码
     * @return mixed
     */
    public function code(){
        return view('seller.settle.code');
    }

    public function test(){
        $yzh=new PayV2Controller(23);
//        $yzh->bizUserId='100009001000';
//        $rs=$yzh->actionQueryBalance(12985739202038);

//        $rs=$yzh->actionGetMemberInfo(100009001000);
//        var_dump($yzh->get_value($rs));
//        echo 2323;

        //创建会员
//        $rs=json_decode($yzh->actionCreateMember('xyf10006',2,2));
//        if($rs->status=='error'){
//            echo $yzh->get_error_info($rs);
//        }else{
//            var_dump($yzh->get_true_value($rs));
//        }

        //设置商家信息
        $yzh->actionSetCompanyInfo('100009001000');

    }

    /**
     * 商家联盟信息页面
     * @return mixed
     */
    public function seller_info(){
        $RuzhuMerchantBasic = RuzhuMerchantBasic::where(['merchantId' => session('seller_info') ->child_merchant_no])
            -> select('id','logoimage','business_time')
            -> first();
        return view('seller.settle.Alliance_info',compact('RuzhuMerchantBasic'));
    }

    /**
     * 商家联盟信息提交
     * @param Request $request
     * @return mixed
     */
    public function seller_info_submit(Request $request){
        $request_info = $request -> all();

        //检查是否上传图片并返回图片地址
        if(!empty($request_info['businessimagebase64'])){
            $size = strlen(file_get_contents($request_info['businessimagebase64']))/1024;
            if($size > 1024){
                return back()->withInput($request->all())->withErrors(['error'=>'请上传不超过1M的图片']);
            }
            $result=upload($request_info['businessimagebase64']);
            if($result['code']==0){
                return back()->withInput($request->all())->withErrors(['error'=>$result['message']]);
            }else{
                $request_info['businessimage']=$result['data']['filepath'];
            }
        }

        $basic = [
            'logoimage' => $request_info['businessimage'],
            'business_time' => $request_info['business_time'],
        ];
        $result = RuzhuMerchantBasic::where(['id' => $request_info['id']]) -> update($basic);
        if ($result == 0){
            return back()->withInput($request->all())->withErrors(['error'=>'发布失败']);

        }
        return back()->with(['status'=>'发布成功']);
    }

    /**
     * banner列表
     * @return mixed
     */
    public function banner_list(){
        $banner_list = DB::table('banner')
                    ->where(['seller_id' => session('seller_info') -> id])
                    ->select('id','businessimage','name')
                    ->paginate(15);
        //轮播图的数量，用于前端判断是否能新增
        $count = DB::table('banner')
            ->where(['seller_id' => session('seller_info') -> id])
            ->count();
        return view('seller.settle.banner_list',compact('banner_list','count'));
    }

    /**
     * 新增/编辑商家banner图
     * @param $id banner图id
     * @return mixed
     */
    public function banner_add(Request $request){
        $id = $request -> input('id');
        $banner_info = DB::table('banner')
                    ->where(['id' => $id])
                    ->select('id','businessimage','name')
                    ->first();
        return view('seller.settle.banner_add',compact('banner_info'));
    }
    
    public function banner_add_submit(BannerAddRequest $bannerAddRequest){
        $request_info = $bannerAddRequest -> all();
        $id = $request_info['id'];

        //检查是否上传图片并返回图片地址
        if(!empty($request_info['businessimagebase64'])){
            $size = strlen(file_get_contents($request_info['businessimagebase64']))/1024;
            if($size > 1024){
                return back()->withInput($bannerAddRequest->all())->withErrors(['error'=>'请上传不超过1M的图片']);
            }
            $result=upload($request_info['businessimagebase64']);
            if($result['code']==0){
                return back()->withInput($bannerAddRequest->all())->withErrors(['error'=>$result['message']]);
            }else{
                $request_info['businessimage']=$result['data']['filepath'];
            }
        }elseif (empty($request_info['businessimagebase64']) && empty($id)){
            return back()->withInput($bannerAddRequest->all())->withErrors(['error'=>'图片不能为空']);
        }

        $banner_info['name'] = $request_info['name'];
        $banner_info['businessimage'] = $request_info['businessimage'];
        $banner_info['seller_id'] = session('seller_info') -> id;
        if (empty($id)){
            //新增
            $banner_info['created_at'] = time2date();
            $result = DB::table('banner') -> insert($banner_info);
            if (!$result){
                return back()->withInput($bannerAddRequest->all())->withErrors(['error'=>'新增失败']);
            }
        }else{
            //更新
            $banner_info['updated_at'] = time2date();
            $banner = DB::table('banner') -> where(['id' => $id]) ->first();
            $result = DB::table('banner') -> where(['id' => $banner -> id]) -> update($banner_info);
        }

        if (empty($id)){
            //信息
            return redirect('/settle/banner_list')->with(['status'=>'发布成功']);
        }else{
            //更新
            return back()->with(['status'=>'发布成功']);
        }

    }

}
