<?php

namespace App\Http\Controllers\Seller;

use App\Http\Requests\Seller\YixianRequest;
use App\Models\Pos;
use App\Models\YixiantdzdMerchants;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class YixianController extends Controller
{
	/**
	 * pos机管理页面
	 * @return mixed
	 */
    public function yixian(){
        $user=session('seller_info');
        $info=YixiantdzdMerchants::where(['merchantNo'=>$user->mobile])->first();
		return view('seller.yixian.yixian',['info'=>$info]);
	}

    /**
     * pos机管理页面
     * @return mixed
     */
    public function yixian_submit(Request $request){
        $request_info = $request -> all();
        $seller = session('seller_info');
        $yixian = [
            'merchantNo'		=> $seller -> mobile,
            'TaxRegistrationNo'		=> $request_info['TaxRegistrationNo'],
            'bankarea'		=> $request_info['bankarea'],
            'bankUrl1'		=> $request_info['bankUrl1'],
            'bankUrl2'		=> $request_info['bankUrl2'],
            'ICurl1'		=> $request_info['ICurl1'],
            'ICurl2'		=> $request_info['ICurl2'],
            'merchantICurl'		=> $request_info['merchantICurl'],
            'is_submit'		=> '1',
        ];
        $result = YixiantdzdMerchants::insert($yixian);
        if (!$result){
            return back()->withInput($request_info)->withErrors(['error'=>'发布失败']);
        }
        return redirect('/yixian/yixian')->with(['status'=>'发布成功']);
    }
}
