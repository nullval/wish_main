<?php

namespace App\Http\Controllers\Seller;

use App\Http\Requests\Seller\RoleAddRequest;
use App\Models\Pos;
use App\Models\Seller;
use App\Models\SellerPermission;
use App\Models\SellerRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfigController extends Controller
{
	/**
	 * 提现页面展示
	 * @return mixed
	 */
    public function cashback(){
		//获取商家id
		$sid=session('seller_info')->id;
		$info=Seller::where(['id'=>$sid])->first();
		$data['is_a_point']=$info->is_a_point;
		return view('seller.config.set_cashback',compact('data'));
	}

	/**
	 * 提现参数提交
	 * @param Request $request
	 * @return mixed
	 */
	public function cashback_submit(Request $request){
		//获取商家id和提现类型
		$sid=session('seller_info')->id;
		$cashback=$request->get('is_a_point');
		Seller::where(['id'=>$sid])->update(['is_a_point'=>$cashback]);
		return back()->with(['status'=>'修改成功']);

	}

	/**
	 *角色列表
	 */
	public function role_list(){
		$role_list=SellerRole::where(['seller_id' => session('seller_info')->id])
			->orderBy('id','desc')
			->paginate(15);
		return view('seller.config.role_list',['role_list'=>$role_list]);
	}


	/**
	 *添加角色
	 */
	public function role_add(Request $request){
		//
		$SellerPermission = new SellerPermission();
		$permission_list = $SellerPermission->sort_permission();
		$id=$request->input('id');
		if(!empty($id)){
			$info = SellerRole::detail($id);
		}else{
			//初始化字段
			$info = init_table_field('seller_role');
		}
		$pos_list = Pos::where(['uid' => session('seller_info')->id]) ->get();
		return view('seller.config.role_add',['permission_list'=>$permission_list,'info'=>$info,'pos_list' => $pos_list]);
	}


	/**
	 *添加角色提交
	 */
	public function role_add_submit(RoleAddRequest $roleAddRequest){
		$request_info=$roleAddRequest->all();
		$SellerRole=new SellerRole();
		$return_arr=$SellerRole->add_role($request_info);
		$des=empty($request_info['id'])?'添加':'编辑';
		return $return_arr['code']==0?back()->withInput($roleAddRequest->all())->withErrors(['error'=>$return_arr['message']]):redirect('/config/role_list')->with(['status'=>'员工'.$des.'成功']);
	}


	public function role_del(Request $request){
		$id = $request->input('id');
		$result = SellerRole::where(['id' => $id]) -> delete();
		if (!$result){
			return back()->with(['error'=>'删除失败']);
		}
		return back()->with(['status'=>'删除成功']);

	}

	public function auto_set(){
        $sid=session('seller_info')->id;
	    $seller_info=Seller::where(['id'=>$sid])->first();
        return view('seller.config.auto_set',compact('seller_info'));
    }

    public function auto_set_submit(Request $request){
        $sid=session('seller_info')->id;
        $arr['withdraw_amount']=$request->input('withdraw_amount');
        Seller::where(['id'=>$sid])->update($arr);
        return redirect('/config/auto_set')->with(['status'=>'更新成功']);
    }
}
