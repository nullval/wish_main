<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Models\PushQrOrder;
use http\Env\Response;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    public function __construct()
    {
    }


    //图片上传
//    public function upload($img_name,$path_name){
//        $file = Input::file($img_name);
//        if(empty($file)){
//            return null;
//        }
//        if($file -> isValid()){
//            $entension = $file -> getClientOriginalExtension(); //上传文件的后缀.
//            $newName = date('YmdHis').mt_rand(100,999).'.'.$entension;
//            $y=date('Y');
//            $m=date('m');
//            $d=date('d');
//            $path_url='/upload/'.$path_name.'/'.$y.'/'.$m.'/'.$d;
//            $path = $file -> move(base_path().'/public'.$path_url,$newName);
//            $filepath = $path_url.'/'.$newName;
//            return $filepath;
//        }
//
//
//    }

    /**
     * @return mixed上传图片
     */
    public function upload_img()
    {
        return upload(request('imagebase64'));
    }

    /**
     * 下载APP引导页
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function download()
    {
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strpos($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
            return redirect('https://itunes.apple.com/cn/app/id1398228674');
        } else {
            return view('seller.common.download', ['download_url' => asset('app/CHQT1.0.apk')]);
        }
    }



}
