<?php

namespace App\Http\Controllers\Seller;

use App\Models\Order;
use App\Models\VOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
	/**
	 * 订单列表页面
	 * @return mixed
	 */
    public function order_list(Request $request){
		$user=session('seller_info');
        $year=empty($request->input('year'))?date('Y'):$request->input('year');
//        $order_table='order_'.$year;
		$order_list=VOrder::where(['uid'=>$user->id])
            ->whereIn('type',[1,3,5])
            ->whereNotIn('v_order.status',[1])
			->leftJoin('seller','seller.id','=','v_order.uid')
			->select(['v_order.*','seller.mobile'])
			->orderBy('created_at','desc')
			->paginate(15);
		return view('seller.order.order_list',compact('order_list'));
	}
}
