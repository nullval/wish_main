<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018/7/14
 * Time: 14:26
 */

namespace App\Libraries;

use Qiniu\Auth;
use Qiniu\Storage\UploadManager;

class Upload
{
    private $accessKey;
    private $secretKey;
    private $bucketName;
    private $domain;

    /**
     * GpuLogic constructor.
     * @param string $accessKey
     * @param string $secretKey
     * @param string $bucketName
     * @param string $domain
     */
    public function __construct($accessKey = '', $secretKey = '', $bucketName = '', $domain = '')
    {
        if (empty($accessKey)) {
            $this->accessKey = env('QINIU_ACCESS');
        } else {
            $this->accessKey = $accessKey;
        }
        if (empty($secretKey)) {
            $this->secretKey = env('QINIU_SECRET');
        } else {
            $this->secretKey = $secretKey;
        }
        if (empty($bucketName)) {
            $this->bucketName = env('QINIU_BUCKET');
        } else {
            $this->bucketName = $bucketName;
        }
        if (empty($domain)) {
            $this->domain = env('QINIU_DOMAIN');
        } else {
            $this->domain = $domain;
        }
    }


    /**
     * 上传BASE64格式图片
     *
     * @param $base64_str
     * @return string
     * @throws \Exception
     */
    public function uploadBase64Img($base64_str)
    {
        $base64_info = $this->base64toBin($base64_str);
        $upManager = new UploadManager();
        $auth = new Auth($this->accessKey, $this->secretKey);
        $token = $auth->uploadToken($this->bucketName);
        $hash = md5($base64_info['bin']);//文件HASH值
        $file_name = $hash . '.' . $base64_info['type'];
        list($ret, $error) = $upManager->put($token, $file_name, $base64_info['bin']);
        if (!empty($error)) {
            throw new \Exception('上传失败');
        }
        return $this->domain . $ret['key'];
    }

    /**
     * 上传二进制文件
     *
     * @param $file
     * @return string
     * @throws \Exception
     */
    public function uploadFile($file)
    {
        $upManager = new UploadManager();
        $auth = new Auth($this->accessKey, $this->secretKey);
        $token = $auth->uploadToken($this->bucketName);
        $hash = md5_file($file);//文件HASH值
        $mime_type = pathinfo($file)['extension'];
        $file_name = $hash . '.' . $mime_type;
        list($ret, $error) = $upManager->putFile($token, $file_name, $file);
        if (!empty($error)) {
            throw new \Exception('上传失败');
        }
        return $this->domain . $ret['key'];
    }

    /**
     * 将base64转为二进制
     *
     * @param $base64_image_content
     * @return array
     * @throws \Exception
     */
    public function base64toBin(string $base64_image_content): array
    {

        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image_content, $result)) {
            return [
                'type' => $result[2],
                'bin' => base64_decode(str_replace($result[1], '', $base64_image_content))
            ];
        } else {
            //如果没有前缀则直接解码为jpeg
            return [
                'type' => 'jpeg',
                'bin' => base64_decode($base64_image_content)
            ];
        }

    }

}