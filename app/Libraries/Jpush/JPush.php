<?php
namespace App\Libraries\JPush;

//极光推送
class JPush {
	//极光app_key
	protected $app_key ;
	//极光推送密钥
	protected $secret ;

	public function __construct($type)
	{
//	    if($type==1){
	        //机主版
//            $this->app_key='0cc462927f07d14ef1261e71';
//            $this->secret='0283f6b63d98dc16bd3c2ece';
//        }elseif($type==2){
//	        //商家版
//            $this->app_key='57e80eeb3e96625fe4d5cfb9';
//            $this->secret='bc7290c9fcb5f7eee59e572b';
//        }elseif($type==4){
            //创新收银宝
			$this->app_key='955e15880de68d69ed236d61';
			$this->secret='49d51ecc1462a11ad22a8d47';
//		}
	}

	/**
	 * @param string $platform  安卓传android,苹果传ios,所有all
	 * @param $title  推送标题
	 * @param $content 推送内容
	 */
	public function push($platform='all',$title,$content){
		/**
		 * 默认日志路径为 ./jpush.log,即保存在当前运行目录，如果想关闭日志，可以指定为 null
		 */
		$client = new \JPush\Client($this->app_key,$this->secret,null);
		$push = $client->push();
		try {
			$push->setPlatform($platform);
			$push->addAllAudience();

			switch ($platform) {
				case 'all': { //所有平台
					$push->androidNotification($content, [
						'sound' => 'sound',
						'badge' => '+1',
						'extras' => [
							'key' => 'value'
						],
						'available' => true,
						'title' => $title,//推送标题
						'builder_id' => 0,
					]);
					$push->iosNotification($content, [
						'sound' => 'sound',
						'badge' => '+1',
						'extras' => [
							'key' => 'value'
						],
						'title' => $title,
					]);
					break;
				}
				case 'android': { //安卓平台
					$push->androidNotification($content, [
						'sound' => 'sound',
						'badge' => '+1',
						'extras' => [
							'key' => 'value'
						],
						'available' => true,
						'title' => $title,//推送标题
						'builder_id' => 0,
					]);
					break;
				}
				case 'ios': { //苹果平台
					$push->iosNotification($content, [
						'sound' => 'sound',
						'badge' => '+1',
						'extras' => [
							'key' => 'value'
						],
						'title' => $title,
					]);
					break;
				}
			}
			$result = $push->send();
		}catch (\Exception $e){
			$result = $e->getMessage();
		}
//		filedebug('所有推送','./log/app_push.php');
//		filedebug($result,'./log/app_push.php');

        var_dump($result);exit;
		//推送成功就执行入库操作
		if($result['http_code'] === 200){
			$params = array(
				'platform' => $platform,
				'title' => $title,
				'content'=> $content
			);
			$data = serialize($params);
		}

	}

	/**
	 * @param $title string  推送标题
	 * @param $alert string 推送弹出内容
	 * @param $merchant_mobile  string 推送Tag为merchant_mobile
	 * @param $extras array 推送扩展参数
	 * @return mixed
	 * Tag推送   AddTag为用户Merchant_mobile
	 * 如果客户端没有tag值为空极光服务端会抛出cannot find user by this audience
	 */
	public function push_tags($merchant_mobile,$title,$alert,$extras = ['key' => 'val']){
	    //测试环境关掉
        if(config('app.env')!='online'&&!in_array($merchant_mobile,['yhcdtest1','yhcdtest2','yhcdtest3','1xdl100000027'])){
            return false;
        }
		/**
		 *	扩展参数 key => value
		 *	数据结构体
		 *	data=>{
		 *		"type": 2   type=1 提现通知  type=2 用户买单提醒
		 *		"data": {
		 *			"create_time": "2016-11-18 10:31:17",
		 *			"end_time": "2016-11-19 10:31:18",
		 *			"id": "30985",
		 *			"income": ".01",
		 *			"node_id": "29820",
		 *			"numrow": "1",
		 *			"remarks": "普通红包",
		 *			"short_title": "",
		 *			"status": "0",
		 *			"title": "线下买单赠送红包",
		 *			"type": "1"
		 *		},
		 *	}
		 **/
		//默认日志路径为 ./jpush.log,即保存在当前运行目录，如果想关闭日志，可以指定为 null
		$client = new \JPush\Client($this->app_key,$this->secret,null);
		$push = $client->push();
		try {
			//推送平台
			$push->setPlatform(['ios', 'android']);
			$push->addTag($merchant_mobile);
			$push->setNotificationAlert($alert);
			//ios 'sound', 'badge', 'content-available', 'mutable-content', category', 'extras');
			$push->iosNotification($alert, [
				'sound' => 'sound',//表示通知提示声音，默认填充为空字符串
				'badge' => '0',//表示应用角标，把角标数字改为指定的数字；为 0 表示清除，支持 '+1','-1' 这样的字符串，表示在原有的 badge 基础上进行增减，默认填充为 '+1'
				'mutable-content' => true,//表示通知扩展,
				'category' => true,
				'content-available' => true, //表示推送唤醒
				'extras' => $extras //表示扩展字段，接受一个数组，自定义 Key/value 信息以供业务使用
			]);

			//安卓
			$push->androidNotification($alert, [
				'title' => $title, //表示通知标题，会替换通知里原来展示 App 名称的地方
				'style' => 1, //有三种可选分别为 bigText=1，Inbox=2，bigPicture=3
//				'builder_id' => 0, //表示通知栏样式 ID
				'extras' => $extras
			]);

			$push->options(array(
				"sendno" => time(),
//				"time_to_live" => 86400, //保存离线时间的秒数默认为一天
                "time_to_live" => 60*5, //保存离线时间的秒数默认为五分钟
			));

			$result = $push->send();
            new_logger('jiguang.log','result',['res'=>$result]);
//			filedebug("merchant_mobile = ".$merchant_mobile,'./log/app_push.php');
//			filedebug($result,'./log/app_push.php');
			//推送成功就执行入库操作
			if($result['http_code'] === 200){
				$params = array(
					'platform' => 'ios,android',
					'title' => $title,
					'content'=> $alert
				);
				$data = serialize($params);
			}
		}catch (\JPush\Exceptions\APIConnectionException $e) {
			$result = $e->getMessage();
			logger($result);
		}catch (\JPush\Exceptions\APIRequestException $e) {
			$result = $e->getMessage();
			logger($result);
		}

		return $result;
	}

}