<?php
namespace App\Libraries\Huanxun;

class HuanxunDf {
	
	protected $Version='V1.0.1';
	protected $MerCode;
	protected $Account;
    protected $MerCert;
    protected $PostUrl;
    private $error = '';
	
	public function __construct($test = false)
	{
	    //深圳市智创未来科技有限公司
		$this->test = $test;
		$this->PostUrl=$test ? 'https://ebp.ips.com.cn/fpms-access/action/trade/transfer.do' : 'https://ebp.ips.com.cn/fpms-access/action/trade/transfer.do';
        $this->MerAcctNo = $test ? '2056110014' : '2056110014'; //交易賬戶號
        $this->MerCert = env('HUANXUN_MERCERT');
    }

    public function pay(string $order_no,string $customerCode,int $amount,string $collectionItemName){
        $param=[
            "merBillNo"	    => $order_no, //商户订单号
            "transferType"	        => 2, //商户转到经销商
            "merAcctNo"	        => $this->MerAcctNo, //加密方式
            "customerCode"	        => $customerCode, //客户号
            "transferAmount"	    => $amount, //订单金额 单位元
            "collectionItemName"	=> $collectionItemName, //付款项目名称
        ];
        $html_text=$this->buildRequest($param);
		$html_text = str_replace(['<![',']>'],['',''],$html_text);
        $xmlResult = simplexml_load_string($html_text);
        $xmlResult = json_decode(json_encode($xmlResult));
        var_dump($xmlResult);exit;
        $strRspCode = $xmlResult->BarCodeScanPayRsp->head->RspCode;
        if($strRspCode == "000000")
        {
            //返回报文验签
            //验签
            $verify_result = $this->checkSign($html_text);
            // 验证成功
            if ($verify_result) {
                $strStatus = $xmlResult->BarCodeScanPayRsp->body->Status;
                if ("Y" == $strStatus){
//                    $message ="交易成功";
                    return true;
                } elseif ("P" == $strStatus){
//                    $message ="交易处理中";
                    $this->error="交易处理中";
                    return false;
                } else{
//                    $message ="交易失败";
                    $this->error="交易失败";
                    return false;
                }
            } else {
                $this->error="验签失败";
                return false;
            }
        }else{
            $this->error=$xmlResult->BarCodeScanPayRsp->head->RspMsg;
            return false;
        }

    }


    /**
     * 建立请求，以表单HTML形式构造（默认）
     * @param $para_temp 请求参数数组
     * @return 提交表单HTML文本
     */
    public function buildRequest($para_temp) {
        $PostUrl=$this->PostUrl;
        try {
            $para = $this->buildRequestPara($para_temp);

            $wsdl = $PostUrl;

            $client=new \SoapClient($wsdl);

            $sReqXml = $client->barCodeScanPay($para);
            logger('付款扫码请求返回报文');
            logger($sReqXml);
            return $sReqXml;
        } catch (\Exception $e) {
//            logger('付款扫码请求异常');
//            logger($e);
            return false;
        }
        return null;
    }

    public function checkSign($param){
        $MerCode=	$this->MerCode;
        $MerCert=	$this->MerCert;
        $xmlResult = simplexml_load_string($param);
        $strSignature = $xmlResult->BarCodeScanPayRsp->head->Signature;
        $strBody = $this->subStrXml("<body>", "</body>", $param);
        if ($this->md5Verify($strBody, $strSignature, $MerCert)) {
            return true;
        } else {
            $this->error='验证签名错误';
            return false;
        }
    }

    /**
     * 生成要请求给IPS的参数XMl
     * @param $para_temp 请求前的参数数组
     * @return 要请求的参数XMl
     */
    protected function buildRequestPara($para_temp) {
        $sReqXml = "<?xml version='1.0.1' encoding='utf-8'?>";
        $sReqXml .= "<Ips>";
        $sReqXml .= "<ipsRequest>";
        $sReqXml .= "<?xml version='1.0.1' encoding='utf-8'?>";
        $sReqXml .= $this->buildHead($para_temp);
        $sReqXml .= $this->buildBody($para_temp);
        $sReqXml .= "</ipsRequest>";
        $sReqXml .= "</Ips>";
        logger('环讯扫码请求报文');
        logger($sReqXml);
        return $sReqXml;
    }
    /**
     * 请求报文头
     * @param   $para_temp 请求前的参数数组
     * @return 要请求的报文头
     */
    protected function buildHead($para_temp){
        $Version=$this->Version;
        $MerCode=$this->MerCode;
        $MerCert=$this->MerCert;
        $sReqXmlHead = "<head>";
        $sReqXmlHead .= "<version>".$Version."</version>";
        $sReqXmlHead .= "<reqIp>".getClientIp()."</reqIp>";
        $sReqXmlHead .= "<reqDate>".date('Y-m-d H:i:s')."</reqDate>";
        $sReqXmlHead .= "<signature>".$this->md5Sign($this->buildBody($para_temp),$MerCert)."</signature>";
        $sReqXmlHead .= "</head>";
        return $sReqXmlHead;
    }

    /**
     *  请求报文体
     * @param  $para_temp 请求前的参数数组
     * @return 要请求的报文体
     */
    protected function buildBody($para_temp){
        $sReqXmlBody = "<body>";
        $sReqXmlBody .= "<merBillNo>".$para_temp["merBillNo"]."</merBillNo>";
        $sReqXmlBody .= "<transferType>".$para_temp["transferType"]."</transferType>";
        $sReqXmlBody .= "<merAcctNo>".$para_temp["merAcctNo"]."</merAcctNo>";
        $sReqXmlBody .= "<customerCode>".$para_temp["customerCode"]."</customerCode>";
        $sReqXmlBody .= "<transferAmount>".$para_temp["transferAmount"]."</transferAmount>";
        $sReqXmlBody .= "<collectionItemName>".$para_temp["collectionItemName"]."</collectionItemName>";
        $sReqXmlBody .= "</body>";
        return $sReqXmlBody;
    }


    /**
     * 签名字符串
     * @param $prestr 需要签名的字符串
     * @param $key 私钥
     * @param $merCode 商戶號
     * return 签名结果
     */
    protected function md5Sign($prestr, $key) {
        $prestr = $prestr  . $key;
        return md5($prestr);
    }

    /**
     * 验证签名
     * @param $prestr 需要签名的字符串
     * @param $sign 签名结果
     * @param $merCode 商戶號
     * @param $key 私钥
     * return 签名结果
     */
    protected function md5Verify($prestr, $sign, $key) {
        $prestr = $prestr . $key;
        $mysgin = md5($prestr);

        if($mysgin == $sign) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * php截取<body>和</body>之間字符串
     * @param string $begin 开始字符串
     * @param string $end 结束字符串
     * @param string $str 需要截取的字符串
     * @return string
     */
    protected function subStrXml($begin,$end,$str){
        $b= (strpos($str,$begin));
        $c= (strpos($str,$end));

        return substr($str,$b,$c-$b + strlen($end));
    }
    /**
     * 对象转数组
     * @param unknown $array
     * @return array
     */
    public function object_array($array)
    {
        if(is_object($array))
        {
            $array = (array)$array;
        }
        if(is_array($array))
        {
            foreach($array as $key=>$value)
            {
                $array[$key] = object_array($value);
            }
        }
        return $array;
    }

    public function getError(){
        return $this->error;
    }
}