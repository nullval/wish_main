<?php
namespace App\Libraries\Huanxun;

class Huanxun {

    protected $Version='v1.0.1';
    protected $MerCode;
    protected $Account;
    protected $MerCert;
    protected $PostUrl;
    private $error = '';
    //3des加密参数
    private $key = "VdNoSsk3sU8cyjQwL5QXEasG";
    private $iv = "3RnWrffG";

    public function __construct($test = false)
    {
        $this->key=env('APP_ENV')=='online'?'VdNoSsk3sU8cyjQwL5QXEasG':'';
        $this->iv=env('APP_ENV')=='online'?'3RnWrffG':'';
        //深圳市智创未来科技有限公司
        $this->test = $test;
        $this->PostUrl=$test ? 'https://newpay.ips.com.cn/psfp-entry/services/order?wsdl' : 'https://newpay.ips.com.cn/psfp-entry/services/order?wsdl';
        $this->MerCode = $test ? '205611' : '205611'; //商戶號
        $this->Account = $test ? '2056110014' : '2056110014'; //交易賬戶號
        $this->MerName = $test ? '深圳市智创未来科技有限公司' : '深圳市智创未来科技有限公司'; //商户名称
        $this->MerCert = env('APP_ENV')=='online'?'UKavqoFsqjTRxgCFfSED3WYJe1Hz5p29LGLo0q3sClNm3xnaWhbYeFQhTlvO7OCFH98TFCTvYxtBFztCbqaZzhYHnUn965qvV2jaDXOQ3yfO29qDNHBTBvwgLChlA2hh':''; //商戶證書
    }


    /**
     * 用户开户,支持个人开户 跳转页面 身份证必须正确
     * @param $param
     */
    public function open($param){
        $url='https://ebp.ips.com.cn/fpms-access/action/user/open';
        //构造arg3DesXmlPara body部分
        $sReqXmlBody = "<body>";
        $sReqXmlBody .= "<merAcctNo>".$this->Account."</merAcctNo>"; #商户账户号
        $sReqXmlBody .= "<userType>2</userType>"; #非直销用户类型 1:企业,2 个人
        $sReqXmlBody .= "<customerCode>".$param['customerCode']."</customerCode>"; #客户号,类型为数字或字母
        $sReqXmlBody .= "<identityType>1</identityType>"; #证件类型 :1:身份证;2 营业执照
        $sReqXmlBody .= "<identityNo>".$param['identityNo']."</identityNo>"; #证件号码
        $sReqXmlBody .= "<userName>".$param['userName']."</userName>"; #个人用户名为个人姓名,企业为企业名称
        $sReqXmlBody .= "<legalName></legalName>"; #法人姓名 (选填)
        $sReqXmlBody .= "<legalCardNo></legalCardNo>"; #法人身份证 (选填)
        $sReqXmlBody .= "<mobiePhoneNo>".$param['mobiePhoneNo']."</mobiePhoneNo>"; #手机号码
        $sReqXmlBody .= "<telPhoneNo></telPhoneNo>"; #固定电话 (选填)
        $sReqXmlBody .= "<email></email>"; #邮箱 (选填)
        $sReqXmlBody .= "<contactAddress></contactAddress>"; #联系地址 (选填)
        $sReqXmlBody .= "<remark></remark>"; #备注 (选填)
        $sReqXmlBody .= "<pageUrl>".$param['pageUrl']."</pageUrl>"; #同步返回地址(页面响应地址)
        $sReqXmlBody .= "<s2sUrl>".$param['s2sUrl']."</s2sUrl>"; #异步返回地址
        $sReqXmlBody .= "</body>";

        //构造arg3DesXmlPara参数
        $arg3DesXmlPara=$this->buildarg3DesXmlPara('openUserReqXml',$sReqXmlBody);

        //构造请求参数 ipsRequest请求xml;
        $ipsRequest=$this->buildipsRequest($arg3DesXmlPara);

        //构造表单请求并跳转第三方页面 (pageToPage提交)
        echo $this->build_form($url,['ipsRequest'=>$ipsRequest],'post');
    }


    /**
     * 用户开户结果查询接口
     * @param $customerCode客户号
     * @return mixed
     */
    public function query($customerCode){
        $url='https://ebp.ips.com.cn/fpms-access/action/user/query';

        //构造arg3DesXmlPara body部分
        $sReqXmlBody = "<body>";
        $sReqXmlBody .= "<customerCode>".$customerCode."</customerCode>"; #商户账户号
        $sReqXmlBody .= "</body>";

        //构造arg3DesXmlPara参数
        $arg3DesXmlPara=$this->buildarg3DesXmlPara('queryUserReqXml',$sReqXmlBody);

        //构造请求参数 ipsRequest请求xml;
        $ipsRequest=$this->buildipsRequest($arg3DesXmlPara);

        $post_data['ipsRequest']  = $ipsRequest;

        //获取xml数据 (点对点)
        $xml_data=$this->curl_post($url,$post_data);
        $array=$this->xmlToArray($xml_data);
        if($array['rspCode']=='M000000'){
            $Para=$this->xmlToArray($this->decrypt3DES($array['p3DesXmlPara']));
            return $Para['body'];
        }else{
            return $this->throwError($array['rspMsg']);
        }
    }

    /**
     * 用户信息修改 跳转页面
     * @param $param
     */
    public function update($param){
        $url='https://ebp.ips.com.cn/fpms-access/action/user/update.html';
        //构造arg3DesXmlPara body部分
        $sReqXmlBody = "<body>";
        $sReqXmlBody .= "<customerCode>".$param['customerCode']."</customerCode>"; #客户号,类型为数字或字母
        $sReqXmlBody .= "<pageUrl>".$param['pageUrl']."</pageUrl>"; #同步返回地址(页面响应地址)
        $sReqXmlBody .= "<s2sUrl>".$param['s2sUrl']."</s2sUrl>"; #异步返回地址
        $sReqXmlBody .= "</body>";

        //构造arg3DesXmlPara参数
        $arg3DesXmlPara=$this->buildarg3DesXmlPara('updateUserReqXml',$sReqXmlBody);

        //构造请求参数 ipsRequest请求xml;
        $ipsRequest=$this->buildipsRequest($arg3DesXmlPara);

        //构造表单请求并跳转第三方页面 (pageToPage提交)
        echo $this->build_form($url,['ipsRequest'=>$ipsRequest],'post');
    }

    /**
     * 转账 转一次收2元
     * @param $param
     * @return mixed
     */
    public function transfer($param){
        $url='https://ebp.ips.com.cn/fpms-access/action/trade/transfer.do';
        new_logger('huanxun_transfer','环讯转账提交参数',[$param]);
        //构造arg3DesXmlPara body部分
        $sReqXmlBody = "<body>";
        $sReqXmlBody .= "<merBillNo>".$param['merBillNo']."</merBillNo>"; #商户系统的订单号、可以传空，否则必须保证唯一 (选填)
        $sReqXmlBody .= "<transferType>2</transferType>"; #2、商户转到经销商
        $sReqXmlBody .= "<merAcctNo>".$this->Account."</merAcctNo>"; #由 IPS 颁发的商户号对应的交易账户号
        $sReqXmlBody .= "<customerCode>".$param['customerCode']."</customerCode>"; #商户为经销商设置的客户号
        $sReqXmlBody .= "<transferAmount>".$param['transferAmount']."</transferAmount>"; #转账金额。最多保留两位小数、如 1000.12,单位元
        $sReqXmlBody .= "<collectionItemName>转账</collectionItemName>"; #付款项目名称
        $sReqXmlBody .= "<remark>".$param['remark']."</remark>"; #备注信息 长度不超过 300 (选填)
        $sReqXmlBody .= "</body>";

        //构造arg3DesXmlPara参数
        $arg3DesXmlPara=$this->buildarg3DesXmlPara('transferReqXml',$sReqXmlBody);

        //构造请求参数 ipsRequest请求xml;
        $ipsRequest=$this->buildipsRequest($arg3DesXmlPara);

        $post_data['ipsRequest']  = $ipsRequest;

        //获取xml数据 (点对点)
        $xml_data=$this->curl_post($url,$post_data);
//        dd($xml_data);
        $array=$this->xmlToArray($xml_data);
        if($array['rspCode']=='M000000'){
            $Para=$this->xmlToArray($this->decrypt3DES($array['p3DesXmlPara']));
            new_logger('huanxun_transfer','环讯转账返回参数',[$Para]);
            if($Para['body']['tradeState']=='10'){
                /**
                 * 返回参数说明
                 * ipsBillNo  Ips 订单编号
                 * tradeId  交易流水号
                 * ipsFee Ips 手续费金额
                 * tradeState V1.0.1 交易状态 9、失败，10、成功
                 */
//                new_logger('','返回参数',$Para['body']);
                return $Para['body'];
            }else{
                return $this->throwError('交易失败');
            }
        }else{
            return $this->throwError($array['rspMsg']);
        }
    }

    /**
     * 用户银行卡绑定 跳转页面
     * @param $param
     */
    public function bankCard($param){
        $url='https://ebp.ips.com.cn/fpms-access/action/withoutCode/bankCard.html';
        //构造arg3DesXmlPara body部分
        $sReqXmlBody = "<body>";
        $sReqXmlBody .= "<customerCode>".$param['customerCode']."</customerCode>"; #客户号,类型为数字或字母
        $sReqXmlBody .= "<merAcctNo>".$this->Account."</merAcctNo>"; #客户号,类型为数字或字母
        $sReqXmlBody .= "<pageUrl>".$param['pageUrl']."</pageUrl>"; #同步返回地址(页面响应地址)
        $sReqXmlBody .= "<s2sUrl>".$param['s2sUrl']."</s2sUrl>"; #异步返回地址
        $sReqXmlBody .= "</body>";

        //构造arg3DesXmlPara参数
        $arg3DesXmlPara=$this->buildarg3DesXmlPara('withoutBankReqXml',$sReqXmlBody);

        //构造请求参数 ipsRequest请求xml;
        $ipsRequest=$this->buildipsRequest($arg3DesXmlPara);

        //构造表单请求并跳转第三方页面 (pageToPage提交)
        echo $this->build_form($url,['ipsRequest'=>$ipsRequest],'post');
    }

    /**
     * 用户提现
     * 快速提现3元每笔是没错的，你们账户上有1500元最低金额提现收1块的。这个是公司硬性要求 也就是商户必须有1.2才能提现 1块手续费+0.2实际到账
     * 2、异步通知 s2sUrl 结果：异步通知 XML 结果参数封装为参数名 ipsResponse 的 POST 参数，异步通知XML 结果格式和同步返回 XML 结果格式内容一致。
     * @param $param
     * @return mixed
     */
    public function withdrawal($param){
        $url='https://ebp.ips.com.cn/fpms-access/action/withdrawal/easywithdrawal';
        new_logger('huanxun_withdraw','环讯提现提交参数',[$param]);
        //构造arg3DesXmlPara body部分
        $sReqXmlBody = "<body>";
        $sReqXmlBody .= "<merBillNo>".$param['merBillNo']."</merBillNo>"; #商户提交的订单号 字母或数字
        $sReqXmlBody .= "<merAcctNo>".$this->Account."</merAcctNo>"; #商户账户号
        $sReqXmlBody .= "<amount>".$param['amount']."</amount>"; #提现金额 单位元
        $sReqXmlBody .= "<realName>".$param['realName']."</realName>"; #姓名/企业名称
        $sReqXmlBody .= "<customerCode>".$param['customerCode']."</customerCode>"; #商户为经销商设置的客户号 手机号码
        $sReqXmlBody .= "<s2sUrl>".$param['s2sUrl']."</s2sUrl>"; #异步通知地址
        $sReqXmlBody .= "</body>";

        //构造arg3DesXmlPara参数
        $arg3DesXmlPara=$this->buildarg3DesXmlPara('easyWithdrawalReqXml',$sReqXmlBody);

        //构造请求参数 ipsRequest请求xml;
        $ipsRequest=$this->buildipsRequest($arg3DesXmlPara);

        $post_data['ipsRequest']  = $ipsRequest;

        //获取xml数据 (点对点)
        $xml_data=$this->curl_post($url,$post_data);
//        dd($xml_data);
        $array=$this->xmlToArray($xml_data);
        if($array['rspCode']=='M000000'){
            $Para=$this->xmlToArray($this->decrypt3DES($array['p3DesXmlPara']));
//            dd($Para);
            new_logger('huanxun_withdraw','环讯提现返回参数',[$Para]);
            $body=$Para['body'];
            if($body['tradeState']!=10){
                return $this->throwError($body['failMsg']);
            }else{
                return $body;
            }
        }else{
            return $this->throwError($array['rspMsg']);
        }
    }


    /**
     * 查询订单列表
     * @param $param
     * @return mixed
     */
    public function queryOrdersList($param){
        $url='https://ebp.ips.com.cn/fpms-access/action/trade/queryOrdersList';

        //构造arg3DesXmlPara body部分
        $sReqXmlBody = "<body>";
        $sReqXmlBody .= "<merAcctNo>".$this->Account."</merAcctNo>"; #由 IPS 颁发的商户号对应的交易账户号
        $sReqXmlBody .= "<customerCode>".$param['customerCode']."</customerCode>"; #商户为经销商设置的客户号
        $sReqXmlBody .= "<ordersType>".$param['ordersType']."</ordersType>"; #全部、收款、提现
        $sReqXmlBody .= "<merBillNo>".$param['merBillNo']."</merBillNo>"; #商户提交的订单号 字母或数字
        $sReqXmlBody .= "<ipsBillNo>".$param['ipsBillNo']."</ipsBillNo>"; #Ips 订单编号
        $sReqXmlBody .= "<startTime>".$param['startTime']."</startTime>"; #格式为 YYYYMMdd，eg:20170906 不填，默认为 30 天前的日期
        $sReqXmlBody .= "<endTime>".$param['endTime']."</endTime>"; #格式为 YYYYMMdd，eg:20170906 不填，默认为当天日期。日期间距须<= 1 月
        $sReqXmlBody .= "<currrentPage>".$param['currrentPage']."</currrentPage>"; #格式为正整数，
        $sReqXmlBody .= "<pageSize>".$param['pageSize']."</pageSize>"; #格式为正整数，每页返回记录数，最大 100
        $sReqXmlBody .= "</body>";

        //构造arg3DesXmlPara参数
        $arg3DesXmlPara=$this->buildarg3DesXmlPara('queryOrderReqXml',$sReqXmlBody);

        //构造请求参数 ipsRequest请求xml;
        $ipsRequest="<ipsRequest>";
        $ipsRequest .= "<argMerCode>".$this->MerCode."</argMerCode>";
        $ipsRequest .= "<arg3DesXmlPara>".$arg3DesXmlPara."</arg3DesXmlPara>";
        $ipsRequest .= "</ipsRequest>";

        $post_data['ipsRequest']  = $ipsRequest;

        //获取xml数据 (点对点)
        $xml_data=$this->curl_post($url,$post_data);
//        dd($xml_data);
        $array=$this->xmlToArray($xml_data);
        if($array['rspCode']=='M000000'){
            $Para=$this->xmlToArray($this->decrypt3DES($array['p3DesXmlPara']));
            return $Para['body'];
        }else{
            return $this->throwError($array['rspMsg']);
        }
    }

    /**
     * 查询默认提现卡
     * @param $param
     * @return mixed
     */
    public function querybankcard($param){
        $url='https://ebp.ips.com.cn/fpms-access/action/withdrawal/querybankcard';

        //构造arg3DesXmlPara body部分
        $sReqXmlBody = "<body>";
        $sReqXmlBody .= "<customerCode>".$param['customerCode']."</customerCode>"; #商户为经销商设置的客户号
        $sReqXmlBody .= "<merAcctNo>".$this->Account."</merAcctNo>"; #由 IPS 颁发的商户号对应的交易账户号
        $sReqXmlBody .= "</body>";

        //构造arg3DesXmlPara参数
        $arg3DesXmlPara=$this->buildarg3DesXmlPara('queryBankCardReqXml',$sReqXmlBody);

        //构造请求参数 ipsRequest请求xml;
        $ipsRequest="<ipsRequest>";
        $ipsRequest .= "<argMerCode>".$this->MerCode."</argMerCode>";
        $ipsRequest .= "<arg3DesXmlPara>".$arg3DesXmlPara."</arg3DesXmlPara>";
        $ipsRequest .= "</ipsRequest>";

        $post_data['ipsRequest']  = $ipsRequest;

        //获取xml数据 (点对点)
        $xml_data=$this->curl_post($url,$post_data);
//        dd($xml_data);
        $array=$this->xmlToArray($xml_data);
        if($array['rspCode']=='M000000'){
            $Para=$this->xmlToArray($this->decrypt3DES($array['p3DesXmlPara']));
            return $Para['body'];
        }else{
            return $this->throwError($array['rspMsg']);
        }
    }


    /**
     * 用户开户,支持个人开户 跳转页面 身份证必须正确
     * @param $param
     */
    public function toCertificate($param){
        $url='https://ebp.ips.com.cn/fpms-access/action/channelCert/toCertificate.html';
        //构造arg3DesXmlPara body部分
        $sReqXmlBody = "<body>";
        $sReqXmlBody .= "<customerCode>".$param['customerCode']."</customerCode>"; #客户号,类型为数字或字母
        $sReqXmlBody .= "<merAcctNo>".$this->Account."</merAcctNo>"; #商户账户号
        $sReqXmlBody .= "</body>";

        //构造arg3DesXmlPara参数
        $arg3DesXmlPara=$this->buildarg3DesXmlPara('certIdentityReqXml',$sReqXmlBody);

        //构造请求参数 ipsRequest请求xml;
        $ipsRequest=$this->buildipsRequest($arg3DesXmlPara);

        //构造表单请求并跳转第三方页面 (pageToPage提交)
        echo $this->build_form($url,['ipsRequest'=>$ipsRequest],'post');
    }

    /**
     * 构造ipsRequest请求xml
     * @param $arg3DesXmlPara
     */
    public function buildipsRequest($arg3DesXmlPara){
        $ipsRequest="<ipsRequest>";
        $ipsRequest .= "<argMerCode>".$this->MerCode."</argMerCode>";
        $ipsRequest .= "<arg3DesXmlPara>".$arg3DesXmlPara."</arg3DesXmlPara>";
        $ipsRequest .= "</ipsRequest>";
        return $ipsRequest;
    }

    /**
     * 构造arg3DesXmlPara参数 (3des加密后)
     * @param $xmlNode 节点名称
     * @param $sReqXmlBody arg3DesXmlPara xml的body节点部分
     * @return string
     */
    protected function buildarg3DesXmlPara($xmlNodeName,$sReqXmlBody){
        //构造arg3DesXmlPara head xml部分
        $sReqXmlHead=$this->buildReqXmlHead($sReqXmlBody);
        //构造arg3DesXmlPara xml;
        $sReqXml = "<?xml version='1.0' encoding='utf-8'?>";
        $sReqXml .= "<".$xmlNodeName.">";
        $sReqXml .= $sReqXmlHead;
        $sReqXml .= $sReqXmlBody;
        $sReqXml .= "</".$xmlNodeName.">";
//        dd($sReqXml);
/*        $sReqXml="<?xml version='1.0' encoding='utf-8'?><queryOrderReqXml ><head><version>v1.0.1</version><reqIp>192.168.0.1</reqIp><reqDate>2016-11-26 10:48:39</reqDate><signature>4b7f225feb78912a3bc6be09f00c245a</signature></head><body><merAcctNo>2056110014</merAcctNo><customerCode>13631272493</customerCode><ordersType>3</ordersType><merBillNo></merBillNo><ipsBillNo></ipsBillNo><startTime></startTime><endTime></endTime><currrentPage></currrentPage><pageSize></pageSize></body></queryOrderReqXml >";*/
        $arg3DesXmlPara=$this->encrypt3DES($sReqXml);
        return $arg3DesXmlPara;
    }

    /**
     * 构造arg3DesXmlPara head部分
     * @param $sReqXmlBody arg3DesXmlPara body部分
     * @return string
     */
    protected function buildReqXmlHead($sReqXmlBody){
        $sReqXmlHead="<head>";
        $sReqXmlHead.="<version>".$this->Version."</version>";
        $sReqXmlHead.="<reqIp>".$this->getClientIp()."</reqIp>";
        $sReqXmlHead.="<reqDate>".date('Y-m-d H:i:s')."</reqDate>";
        $sReqXmlHead.="<signature>".md5($sReqXmlBody.$this->MerCert)."</signature>";
        $sReqXmlHead.="</head>";
        return $sReqXmlHead;
    }

    //3des加密
    public function encrypt3DES($input){
        $size = mcrypt_get_block_size(MCRYPT_3DES,MCRYPT_MODE_CBC);
        $input = $this->pkcs5_pad($input, $size);
        $key = str_pad($this->key,24,'0');
        $td = mcrypt_module_open(MCRYPT_3DES, '', MCRYPT_MODE_CBC, '');
        if( $this->iv == '' )
        {
            $iv = @mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        }
        else
        {
            $iv = $this->iv;
        }
        @mcrypt_generic_init($td, $key, $iv);
        $data = mcrypt_generic($td, $input);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        $data = base64_encode($data);
        return $data;
    }

    /**
     * 获取客户端ip
     */
    public function getClientIp(){
        if(getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
            $ip = getenv('HTTP_CLIENT_IP');
        } elseif(getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif(getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown')) {
            $ip = getenv('REMOTE_ADDR');
        } elseif(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $res =  preg_match ( '/[\d\.]{7,15}/', $ip, $matches ) ? $matches [0] : '';
        return $res;
    }

    //3des解密
    public function decrypt3DES($encrypted){
        $encrypted = base64_decode($encrypted);
        $key = str_pad($this->key,24,'0');
        $td = mcrypt_module_open(MCRYPT_3DES,'',MCRYPT_MODE_CBC,'');
        if( $this->iv == '' )
        {
            $iv = @mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        }
        else
        {
            $iv = $this->iv;
        }
        $ks = mcrypt_enc_get_key_size($td);
        @mcrypt_generic_init($td, $key, $iv);
        $decrypted = mdecrypt_generic($td, $encrypted);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        $y=$this->pkcs5_unpad($decrypted);
        return $y;
    }
    private function pkcs5_pad ($text, $blocksize) {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }
    private function pkcs5_unpad($text){
        $pad = ord($text{strlen($text)-1});
        if ($pad > strlen($text)) {
            return false;
        }
        if (strspn($text, chr($pad), strlen($text) - $pad) != $pad){
            return false;
        }
        return substr($text, 0, -1 * $pad);
    }
    private function PaddingPKCS7($data) {
        $block_size = mcrypt_get_block_size(MCRYPT_3DES, MCRYPT_MODE_CBC);
        $padding_char = $block_size - (strlen($data) % $block_size);
        $data .= str_repeat(chr($padding_char),$padding_char);
        return $data;
    }

    /**
     * post提交
     *
     * */
    function curl_post($url,$post_data = array() , $header = false){
        $ch = curl_init(); //初始化curl
        curl_setopt($ch, CURLOPT_URL, $url);//设置链接
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//设置是否返回信息
        curl_setopt($ch, CURLOPT_POST, 1);//设置为POST方式
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if($header){
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        if(is_string($post_data)){
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);//POST数据
        }else{
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));//POST数据
        }

        $response = curl_exec($ch);//接收返回信息
        if(curl_errno($ch)){	//出错则显示错误信息
            print curl_error($ch);
        }
        curl_close($ch); //关闭curl链接
        return $response;
    }

    /**
     * xml转数组
     * @param $xml
     * @return mixed
     */
    public function xmlToArray($xml){
        $data=simplexml_load_string($xml);
        $xmljson= json_encode($data);
        $arr=json_decode($xmljson,true);
        return $arr;
    }

    public function getError(){
        return $this->error;
    }

    public function throwError($error){
        $this->error=$error;
        return false;
    }

    // 重定向并带post数据
    protected function build_form($url,$post_data,$method = 'post'){
        $input_param = '';
        foreach($post_data as $k => $v){
            $input_param .= '<input type="hidden" name="'.$k.'" value="'.$v.'" />';
        }
        $html = <<<EOT
<form method="$method" action="$url" id="form_to_gpu">
$input_param
<input type="submit" value=""  style="opacity:0" />
</form>
<script>document.getElementById('form_to_gpu').submit();</script>
EOT;
        $html = str_replace("\r\n",'',$html);
        $html = str_replace(PHP_EOL,'',$html);
        return $html;
    }
}