<?php

use App\Libraries\Upload;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use App\Models\Agent;
use App\Models\Seller;
use App\Models\User;
use App\Models\PushQrUser;
use Illuminate\Support\Facades\Cache;
use App\Exceptions\ApiException;

/**
 * @param $arr 需要过滤的数组
 * 过滤添加 修改不必要的字段
 */
function filter_update_arr($arr)
{
    unset($arr['_token'],
        $arr['is_agree'], $arr['s']
    );
    return $arr;
}

/**
 * 检查事务状态成功与否
 * @param $status
 * @return bool
 */
function checkTrans($status)
{
    if (is_array($status)) {
        foreach ($status as $v) {
            if (empty($v)) {
                return false;
            }
        }
    } else {
        if (empty($status)) {
            return false;
        }
    }
    return true;
}

// 刷新网页
function refresh($url)
{
    header("Location:$url");
    exit();
}

/**
 * @param $code 0错误信息 1true
 * @param $message
 * @param $data
 * @return array
 * 模型处理返回数组
 */
function arr_post($code, $message, $data = [])
{
    return ['code' => $code, 'message' => $message, 'data' => $data];
}

/**
 * 时间戳格式化成国内日期格式
 * @param null $time
 * @return false|string
 */
function time2date($time = null)
{
    return date('Y-m-d H:i:s');
}

/**
 * @param $num
 * @return string
 * 去尾法 获取小数后四位
 */
function get_last_two_num($num)
{
    $num1 = $num * 10000;
    $arr = explode('.', $num1);
    if (empty($arr[1])) {
        $num = $num1 + 0.1;
    } else {
        $num = $num1;
    }
    return sprintf("%.4f", substr(sprintf("%.8f", floor($num) / 10000), 0, -2));
}

/**
 * @param $num
 * @return string
 * 去尾法 获取小数后三位
 */
function get_last_three_num($num)
{
    $num1 = $num * 1000;
    $arr = explode('.', $num1);
    if (empty($arr[1])) {
        $num = $num1 + 0.1;
    } else {
        $num = $num1;
    }
    return (float)sprintf("%.4f", substr(sprintf("%.8f", floor($num) / 1000), 0, -2));
}

/**
 * @param $num
 * @return string
 * 去尾法 获取小数后两位
 */
function get_num($num)
{
    return (float)sprintf("%.2f", floor($num * 100) / 100);
}

/**
 * @param $owner_id 流水账户id
 * @param $user_id 账户id
 * @return string
 */
function get_transfer_mobile($owner_id, $user_id)
{
    $u_user_info = \App\Models\User::where(['user.id' => $owner_id])->first(['user.mobile']);
    $member_id = isset($u_user_info->member_id) ? $u_user_info->member_id : '系统';
    return $owner_id == $user_id ? '本账号' : $member_id;
}

/**
 * @param $table
 * @return object
 * 格式化表空字段
 */
function init_table_field($table)
{
    $obj = (object)[];
    $columns = Schema::getColumnListing($table);
    foreach ($columns as $k => $v) {
        $obj->$v = '';
    }
    return $obj;
}

/**
 * 判断一个值是否在一个二维数组中
 */
function value_is_exit_in_two_arr($value, $arr)
{
    $is_exit = false;
    foreach ($arr as $v) {
        foreach ($v as $v1) {
            if ($v1 == $value) {
                $is_exit = true;
            }
        }
    }
    return $is_exit;
}

/**
 * $arr 数组
 * $field_name 字段名称
 * 将统计好的数组以,隔开
 */
function norm_arr($arr, $field_name)
{
    $str = '';
    foreach ($arr as $v) {
        $str .= $v->$field_name . ',';
    }
    return substr($str, 0, strlen($str) - 1);
}

/**
 * $arr 数组
 * $field_name 字段名称
 * 将统计好的数组存到数组中
 */
function norm_new_arr($arr, $field_name)
{
    $new_arr = [];
    foreach ($arr as $v) {
        $new_arr[] = $v->$field_name;
    }
    return $new_arr;
}


/**
 * =======================================
 * Created by Zhihua_W.
 * Author: Zhihua_W
 * Date: 2017/6/8 0008
 * Time: 下午 18:10
 * Project: PHP开发小技巧
 * Power: PHP判断时间在某一时间段内
 * =======================================
 */
function get_curr_time_section()
{
    $checkDayStr = date('Y-m-d ', time());
    $timeBegin1 = strtotime($checkDayStr . "00:00" . ":00");
    $timeEnd1 = strtotime($checkDayStr . "2:00" . ":00");
    $curr_time = time();
    if ($curr_time >= $timeBegin1 && $curr_time <= $timeEnd1) {
        return true;
    }
    return false;
}


function clear_null(&$data = '')
{
//	if($data instanceof stdClass){
    $data = json_decode(json_encode($data), true);
//	}
    if ($data === null || $data === false) {
        $data = [];
    }
    if (is_array($data) && !empty($data)) {
        foreach ($data as &$v) {
            if ($v === null || $v === false) {
                $v = '';
            } else if (is_array($v)) {
                clear_null($v);
            } else if (is_string($v) && stripos($v, '.') === 0) {
                $v = '0' . $v;
            }
        }
    }
}

/**
 * @param string $data
 * 根据空数据类型返回相应的数据格式
 */
function return_null($obj)
{
    //空字符串
    if ($obj == "" || $obj == " ") {
        return "";
    }
    if (count($obj) == 1) {
        return (object)[];
    }
    return [];
}

/**
 * @param $msg
 * @param array $data
 * @return mixed
 */
function api_success($msg, $data = array())
{
    $data = array(
        'status' => 1,
        'message' => $msg,
        'data' => $data,
    );
    new_logger('visit_log.log','success',$data);
    return $data;
}

/**
 * @param $msg
 * @param array $data
 * @return mixed
 */
function api_error($msg, $data = array())
{
    $data = array(
        'status' => 0,
        'message' => $msg,
        'data' => $data,
    );
    new_logger('visit_log.log','error',$data);
    return $data;
}

/**
 * json API 返回成功
 * @param string $msg
 * @param array $data
 * @return array
 */
function json_success($msg = '', $data = [])
{
    $return = [
        'code' => '1',
        'message' => $msg,
        'data' => $data,
    ];
    $data = json_decode(json_encode($data), true);
    if (empty($data)) {
        $return['data'] = return_null($return['data']);
    } else {
        clear_null($return['data']);
    }
    return $return;
}

/**
 * json API 返回失败
 * @param $msg
 * @param $code 0 正常错误code  2 支付密码错误 3余额不足
 * @param array $data
 * @return array
 */
function json_error($msg, $data = [], $code = 0)
{
    $msg = $msg ?: '服务器错误，请稍后重试';
    $return = [
        'code' => $code,
        'message' => $msg,
        'data' => $data,
    ];
    clear_null($return['data']);
    return $return;
}

/**
 * 生成订单号 order_no
 * @param $order_id 订单号
 * @param int $pre 前缀 2-购物订单 3-充值订单 4-vip套餐购买 5-好项目购买 6-免费领取手机 7-免费领取手机线下代理支付 8-拼团 9-消费贷还款 10-pos机
 * @return string
 */
function create_order_no($order_id, $pre = 2)
{
    $pad = str_pad($order_id, 8, '0', STR_PAD_LEFT);
    $order_no = rand(1, 99999) . $pad;
//    $order_no=date('YmdHis').rand(1,9);
    return $pre . $order_no;
}

/**
 * 验证pos签名
 * @param $arr 数组
 * @param $sign_nam 签名键值
 */
function verity_pos_sign($param, $sign_nam)
{
    $sign = $param[$sign_nam];
    //通联分配的appkey
    $param['key'] = '';
    unset($param[$sign_nam]);
    ksort($param);
    $param2 = [];
    foreach ($param as $k => $v) {
        $param2[] = $k . '=' . $v;
    }
    $param_sign = strtolower(md5(implode('&', $param2)));

    if ($param_sign != $sign) {
        return false;
    } else {
        return true;
    }

}

/**
 * @param $string
 * @return array
 * 生成签名
 */
function get_pos_sign($param)
{

    ksort($param);
    $param2 = [];
    foreach ($param as $k => $v) {
        $param2[] = $k . '=' . $v;
    }
    $param_sign = strtolower(md5(implode('&', $param2)));

    return $param_sign;

}

/**
 * @param $string
 * @return array
 * 生成代付签名
 */
function get_df_pos_sign($param, $code)
{

    $param['code'] = $code;
    ksort($param);
    $param2 = [];
    foreach ($param as $k => $v) {
        $param2[] = $k . '=' . $v;
    }
    $param_sign = strtolower(md5(implode('&', $param2)));

    return $param_sign;

}

/**
 * @param $string
 * @return array
 * 生成代付签名
 */
function get_shop_pos_sign($param)
{


    $data = $param;
    unset($data['sign']);
    ksort($data);
    $str = '';
    foreach ($data as $k => $v) {
        if (empty($v)) {
            unset($data[$k]);
        }
    }
    foreach ($data as $k => $v) {
        //解决在url里&符号在foreach里变&amp;导致签名错误问题
        if (stripos($v, 'http://') !== false) {
            $v = htmlspecialchars_decode($v);
        }
        $str .= $k . '=' . $v . "&";
    }
    $str = substr($str, 0, -1);
//    var_dump($str);exit;
    $sign = md5($str . env('api_code'));    // sign 算法

    $param_sign = strtolower($sign);

    return $param_sign;

}


function decode_pos_url_param($string)
{
    $arr1 = explode('&', $string);
    $data = [];
    foreach ($arr1 as $k => $v) {
        $arr2 = explode('=', $v);
        $data[$arr2[0]] = $arr2[1];
    }
    return $data;
}

/**
 * 获取商家后台用户登录跳转链接
 */
function get_seller_jump_url()
{
    $permission_list = session(session('seller_info')->id . '_permission_list');
    //跳转页面
    //获取第一个子url跳转链接
    $url = '';
    foreach ($permission_list as $k => $v) {
        if ($v['pid'] != 0) {
            $url = '/' . $v['c'] . '/' . $v['a'];
            return $url;
        }
    }

}

/**
 * 获取后台用户登录跳转链接
 */
function get_admin_jump_url()
{
    $permission_list = session(session('admin_info')->id . '_permission_list');
    //跳转页面
    //获取第一个子url跳转链接
    $url = '';
    foreach ($permission_list as $k => $v) {
        if ($v['pid'] != 0) {
            $url = '/' . $v['c'] . '/' . $v['a'];
            return $url;
        }
    }

}

/**
 * 后台权限验证
 * @param string $user_name 验证类型名
 * @throws \App\Exceptions\NotPermissionException
 */
function check_admin_permission($user_name = 'admin')
{
    $prefix = substr(request()->route()->getAction()['prefix'], 1, strlen(request()->route()->getAction()['prefix']));
    $route = request()->route()->getAction()['controller'];
    $action = explode('@', $route)[1];
    //权限验证
    $is_true = 0;
    if ($user_name == 'admin') {
        $permission_list = session(session('admin_info')->id . '_permission_list');
        $permission_info = \App\Models\AdminPermission::where(['c' => $prefix, 'a' => $action])->get();
    } elseif ($user_name == 'seller') {
        $permission_list = session(session('seller_info')->id . '_permission_list');
        $permission_info = \App\Models\SellerPermission::where(['c' => $prefix, 'a' => $action])->get();
    }
    //只有存在权限列表里面才做验证
    if (!$permission_info->isEmpty()) {
        foreach ($permission_list as $k => $v) {
            if ($v['c'] == $prefix && $v['a'] == $action) {
                $is_true = 1;
            }
        }
        if ($is_true == 0) {
            throw  new \App\Exceptions\NotPermissionException();
        }
    }
}

/*
     *curl请求：
     *data：utf-8编码的请求参数
     *返回：array()
    */

// curl 模拟 post 请求
function curl_post($url, $post_data = array(), $header = false)
{
    if(strpos($url,'upload_img') === false){
        //上传图片不打日志
        new_logger('visit_log.log',$url,$post_data);
    }

    $ch = curl_init(); //初始化curl
    curl_setopt($ch, CURLOPT_URL, $url);//设置链接
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//设置是否返回信息
    curl_setopt($ch, CURLOPT_POST, 1);//设置为POST方式
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    if ($header) {
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    }
    if (is_string($post_data)) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);//POST数据
    } else {
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));//POST数据
    }

    $response = curl_exec($ch);//接收返回信息
    if (curl_errno($ch)) {    //出错则显示错误信息
        print curl_error($ch);
    }
    curl_close($ch); //关闭curl链接
    return $response;
}

/**
 * json API 根据变量自动判断返回成功或失败
 * @param $var
 * @param string $error_msg
 * @param string $success_msg
 * @param array $data
 * @return array
 */
function json_return($var, $error_msg = '服务器错误，请稍后重试', $success_msg = 'OK', $data = [])
{
    if ($var === false || $var === null) {
        return json_error($error_msg, $data);
    } else {
        return json_success($success_msg, $data);
    }
}


/**
 * @return string
 * 生成唯一guid规则的字符串
 */
function uuid()
{
    if (function_exists('com_create_guid')) {
        $uuid = com_create_guid();
    } else {
        mt_srand(( double )microtime() * 10000); //optional for php 4.2.0 and up.随便数播种，4.2.0以后不需要了。
        $charid = strtoupper(md5(uniqid(rand(), true))); //根据当前时间（微秒计）生成唯一id.
        $hyphen = chr(45); // "-"
        $uuid = '' . //chr(123)// "{"
            substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12);
        //.chr(125);// "}"
    }
    return str_replace(['{', '}'], ['', ''], $uuid);
}

/*
 * 日志
 * @param $path 文件名 vim.log
 * @param $title 标题
 * @param $data 数据
 */
function new_logger($path = '', $title = '', $data = [])
{
    if (!is_array($data)) {
        $data = [$data];
    }
    $log = new Logger('log');

    $log->pushHandler(
        new StreamHandler(
            storage_path('logs/' . date('Y') . '/' . date('m') . '/' . date('d') . '/' . $path),
            Logger::INFO
        )
    );

    $log->addInfo($title, $data);
}

/**
 * 请求数据
 * @param $action
 * @param array $params
 * @param string $type
 * @param $upload
 * @return mixed
 */
function connect($params = array(), $type = 'POST', $upload = false)
{
    $data = array();
    $method = strtoupper($type);
    $url = env('URL');
//    $params['api_key'] = $this->api_key;
    if ($method == 'GET') {
        $url = "{$url}?" . http_build_query($params);
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    if ($method == 'POST') {
        if ($upload) {        //设置上传文件
            $file = new \CURLFile($upload['file'], $upload['type'], $upload['name']);
            $params[$upload['get_name']] = $file;
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    }
    $result = curl_exec($ch);
    curl_close($ch);
//    var_dump($result);exit;
//    if($data === null){
//        $this->dump('请求数据失败.', 2);
//    }else{
//        try{
//            $data = json_decode($result, true);
//        }catch(\Exception $e){
//            $this->dump('parse error.', 2, $e);
//        }
//    }
    return $data;
}

/**
 * 系统异常短信通知
 * @param $remark 备注
 */
function notify_mobile_error($type, $remark = '短信通知异常', $spare)
{
    $Message = new \App\Models\Message();
    $bai_mobile = ['13631272493'];
    foreach ($bai_mobile as $k => $v) {
        $Message->send_sms($v, $type, $remark, $spare);
    }
}

/**
 * 根据所属者类型返回类型名称
 * @param $owner_type所属者类型
 */
function get_owner_type_name($owner_type, $purse_id = null)
{
    if ($owner_type == 1) {
        $owner_type_name = '会员钱包';
    } elseif ($owner_type == 2) {
        $owner_type_name = '商家钱包';
    } elseif ($owner_type == 3) {
        if (!empty($purse_id)) {
            $purse_type = DB::table('user_purse')->where(['purse_id' => $purse_id])->first()->purse_type;
            if ($purse_type == 3) {
                $owner_type_name = '系统积分钱包';
            } elseif ($purse_type == 1) {
                $owner_type_name = '系统收益钱包';
            }
        } else {
            $owner_type_name = '系统钱包';
        }
    } elseif ($owner_type == 4) {
        $owner_type_name = '中央银行';
    } elseif ($owner_type == 5) {
        $owner_type_name = '机主钱包';
    } elseif ($owner_type == 6) {
        $owner_type_name = '第三方代付收益钱包';
    } elseif ($owner_type == 7) {
        $owner_type_name = '通联pos机消费收益钱包';
    } elseif ($owner_type == 8) {
        $owner_type_name = '高汇通代付收益钱包';
    } elseif ($owner_type == 9) {
        $owner_type_name = '高汇通pos机消费收益钱包';
    } elseif ($owner_type == 10) {
        $owner_type_name = '积分商城积分钱包';
    } elseif ($owner_type == 11) {
        $owner_type_name = '运营钱包';
    } elseif ($owner_type == 12) {
        $owner_type_name = '地推人员钱包';
    } elseif ($owner_type == 13) {
        $owner_type_name = '环讯pos机消费收益钱包';
    } elseif ($owner_type == 14) {
        $owner_type_name = '亿贤pos机消费收益钱包';
    } elseif ($owner_type == 15) {
        $owner_type_name = '越满pos机消费收益钱包';
    }

    return $owner_type_name;
}

/*
 * 根据所属者类型获取会员的手机号码
 * @param $owner_type所属者类型
 * @param $owner_id 用户id
 */
function get_owner_mobile($owner_type, $owner_id = 0)
{
    if ($owner_type == 1) {
        $mobile = \App\Models\User::where(['id' => $owner_id])->first(['mobile'])->mobile;
    } elseif ($owner_type == 2) {
        $mobile = \App\Models\Seller::where(['id' => $owner_id])->first(['mobile'])->mobile;
    } elseif ($owner_type == 3) {
        $mobile = '系统钱包';
    } elseif ($owner_type == 4) {
        $mobile = '中央银行';
    } elseif ($owner_type == 5) {
        $mobile = \App\Models\Agent::where(['id' => $owner_id])->first(['mobile'])->mobile;
    } elseif ($owner_type == 6) {
        $mobile = '第三方代付收益钱包';
    } elseif ($owner_type == 7) {
        $mobile = '通联pos机消费收益钱包';
    } elseif ($owner_type == 8) {
        $mobile = '高汇通代付收益钱包';
    } elseif ($owner_type == 9) {
        $mobile = '高汇通pos机消费收益钱包';
    } elseif ($owner_type == 10) {
        $mobile = '积分商城积分钱包';
    } elseif ($owner_type == 11) {
//        var_dump($owner_id);exit;
        $mobile = \App\Models\AgentOperate::where(['id' => $owner_id])->first(['mobile'])->mobile;
    } elseif ($owner_type == 12) {
//        var_dump($owner_id);exit;
        $mobile = \App\Models\PushQrUser::where(['id' => $owner_id])->first(['mobile'])->mobile;
    } elseif ($owner_type == 13) {
        $mobile = '环讯pos机消费收益钱包';
    } elseif ($owner_type == 14) {
        $mobile = '亿贤pos机消费收益钱包';
    } elseif ($owner_type == 15) {
        $mobile = '越满pos机消费收益钱包';
    } elseif ($owner_type == 16) {
        $mobile = '天亓支付收益钱包';
    }
    return $mobile;
}

/**
 * @param $purse_id
 * @return string
 * 获取钱包id钱包类型名称
 */
function get_purse_type_name($purse_id)
{
    $purse_info = \App\Models\UserPurse::where(['purse_id' => $purse_id])->first(['purse_type']);
    if (isset($purse_info->purse_type)) {
        $purse_type = $purse_info->purse_type;
        if ($purse_type == 1) {
            $purse_type_name = '收益钱包';
        } elseif ($purse_type == 3) {
            $purse_type_name = '积分钱包';
        } elseif ($purse_type == 4) {
            $purse_type_name = '货款钱包';
        } elseif ($purse_type == 7) {
            $purse_type_name = '商家充值钱包';
        } elseif ($purse_type == 10) {
            $purse_type_name = '消费积分';
        } elseif ($purse_type == 11) {
            $purse_type_name = '兑换积分';
        } elseif ($purse_type == 10001) {
            $purse_type_name = '待释放补贴积分';
        }
        return $purse_type_name;
    } else {
        return '暂无';
    }
}

/**
 * @param $mobile 手机号码
 * @param $openid
 * @param $is_open_td是否在积分增值开通TD账户 0否 1是
 * @param $uid 积分商城用户id
 * 绑定各个身份的openid
 */
function bind_openid($mobile, $openid = '', $is_open_td = 0, $uid = '')
{
    DB::transaction(function () use ($mobile, $openid, $is_open_td, $uid) {
        //用户身份
        $user_info = User::where(['mobile' => $mobile])->first();
        //判断用户身份没有则要创建,保证与积分商城的一致性
        if (!isset($user_info->id)) {
            $user_id = User::insertGetId(['mobile' => $mobile, 'created_at' => time2date()]);
        } else {
            $user_id = $user_info->id;
        }

        if (!empty($openid)) {
            //代理身份
            $agent_info = Agent::where(['mobile' => $mobile])->first();
            if (isset($agent_info->id)) {
                Agent::where(['id' => $agent_info->id])->update(['openid' => $openid, 'updated_at' => time2date()]);
            }
            //商家身份
            $seller_info = Seller::where(['mobile' => $mobile])->first();
            if (isset($seller_info->id)) {
                Seller::where(['id' => $seller_info->id])->update(['openid' => $openid, 'updated_at' => time2date()]);
            }
            //地推人员身份
            $push_info = PushQrUser::where(['mobile' => $mobile])->first();
            if (isset($push_info->id)) {
                PushQrUser::where(['id' => $push_info->id])->update(['openid' => $openid, 'updated_at' => time2date()]);
            }
            //用户身份
            User::where(['id' => $user_id])->update(['openid' => $openid, 'updated_at' => time2date()]);
        }
        if (!empty($uid)) {
            //用户身份
            User::where(['id' => $user_id])->update(['nodeid' => $uid, 'updated_at' => time2date()]);
        }
//        new_logger('bind_openid.log','res',$rs);

    });
    return true;
}

/**
 * @param string $attr_name 数组名称
 * @return mixed|string
 * 返回高汇通商户入驻基本信息参数
 */
function get_ght_attr($attr_name = '')
{
    $url = app_path() . '/Libraries/ght/json.json';
    $html = file_get_contents($url);
    $html = json_decode($html);
    if (empty($attr_name)) {
        return $html;
    } else {
        return $html->$attr_name;
    }
}

/**
 * @param $action
 * @param $arr
 * @return string
 * 带post参数请求并跳转
 */
function redirect_by_post($action, $arr)
{
    $sHtml = "<form id='submit' name='submit' action='" . $action . "' method='post'>";
    foreach ($arr as $k => $v) {
        $sHtml .= "<input type='hidden' name='" . $k . "' value='" . $v . "'/>";
    }
    $sHtml = $sHtml . "</form>";
    $sHtml = $sHtml . "<script>document.forms['submit'].submit();</script>";

    return $sHtml;
}

/**
 * @param $str
 * 处理银行卡字段
 */
function handle_bankcard($str)
{
    return substr($str, 0, 4) . ' **** **** ' . substr($str, -4, 4);
}

/**
 * @param $str
 * 处理手机字段
 */
function handle_mobile($str)
{
    return substr($str, 0, 3) . ' **** ' . substr($str, -4, 4);
}

/**
 * @param $cache_name 缓存名称
 * @param int $isCache 是否并发处理 1是 2否
 * @param $minute 保存时间 分钟为单位
 * @return bool
 * 缓存加锁处理并发
 */
function cache_lock($cache_name, $is_cache = 1, $minute = 0.1)
{
    //缓存处理并发
    if ($is_cache == 1) {
        $lock = Cache::get($cache_name);
        if (!empty($lock)) {
            //        return arr_post(0,'操作频繁');
            return true;
        } else {
            Cache::put($cache_name, 'cache', $minute);
            return false;
        }
    }
    return false;

}

/**
 * 上传图片到图片服务器
 *
 * @param $imagebase64 base64格式的图片数据
 * @return mixed   成功信息和图片地址
 */
function upload($imagebase64)
{
    $uploadInstance = new Upload();
    $size = strlen(file_get_contents($imagebase64)) / 1024;
    if ($size > 1024*4) {
        return arr_post(0, '请上传不超过4M的图片');
    }
    //本地环境上传图片
    if(env('APP_LOCAL_ENV') == 'local'){
        $file = uuid() . '.jpg';
        $save_name = DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . $file;
        $res = $uploadInstance->base64toBin($imagebase64);
        $file_name = storage_path('app' . $save_name);
        $res = file_put_contents($file_name, $res['bin']);
        if($res !== FALSE){
            return arr_post(1, '上传成功', ['filepath' => DIRECTORY_SEPARATOR.'storage' . DIRECTORY_SEPARATOR . $file]);
        }else{
            return arr_post(0, '上传错误');
        }
    }else{
        $res = $uploadInstance->base64toBin($imagebase64);
        $file_name = storage_path('app' . DIRECTORY_SEPARATOR . 'qrcode' . DIRECTORY_SEPARATOR . uuid() . '.jpg');
        file_put_contents($file_name, $res['bin']);
        (new \App\Libraries\ImageCompress($file_name))->compressImg($file_name);//压缩文件
        $url = $uploadInstance->uploadFile($file_name);
        if (empty($url)) {
            return arr_post(0, '上传错误');
        } else {
            return arr_post(1, '上传成功', ['filepath' => $url]);
        }
    }
}

/**
 * 记文件形式调试信息,也可用于记录日志
 * @param $data //内容
 * @param string $filepath 文件存放路径 如为 ./log或log则为日志记录,自动加上日期文件夹
 */
function filedebug($data, $filepath = './filedebug.php')
{
    // 如果是日志，则将日志分隔成日期文件夹
    $date = date('Ym/d');
    $filepath = str_replace('log/', "log/$date/", $filepath);
    $have = strripos($filepath, '/');
    $path = (false === $have ? '' : substr($filepath, 0, $have + 1)); // 文件夹
    $file = end(explode('/', $filepath)) ?: 'default.php'; // 文件名
    if ($path && !is_dir($path)) {
        mkdir($path, 0777, true);
    }
    $filepath = $path . $file;
    $f = fopen($filepath, 'a+');
    fwrite($f, date('Y-m-d H:i:s') . '----------' . "\n");
    fwrite($f, var_export($data, true));
    fwrite($f, "\n" . '----------');
    fclose($f);
}


/**
 * 二维数组根据某个字段排序
 * @param $array 排序的数组
 * @param $direction  排序顺序标志 SORT_DESC 降序；SORT_ASC 升序
 * @param $field 排序字段
 * @return mixed
 */
function two_array_sort($array, $direction, $field)
{

    $sort = array(
        'direction' => $direction, //排序顺序标志 SORT_DESC 降序；SORT_ASC 升序
        'field' => $field,       //排序字段
    );
    $arrSort = array();
    foreach ($array AS $uniqid => $row) {
        foreach ($row AS $key => $value) {
            $arrSort[$key][$uniqid] = $value;
        }
    }
    if ($sort['direction']) {
        array_multisort($arrSort[$sort['field']], constant($sort['direction']), $array);
    }

    return $array;
}

function generate_password($length = 8)
{
    // 密码字符集，可任意添加你需要的字符
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_ []{}<>~`+=,.;:/?|';

    $password = '';
    for ($i = 0; $i < $length; $i++) {
        // 这里提供两种字符获取方式
        // 第一种是使用 substr 截取$chars中的任意一位字符；
        // 第二种是取字符数组 $chars 的任意元素
        // $password .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        $password .= $chars[mt_rand(0, strlen($chars) - 1)];
    }

    return $password;
}

/**
 * @return bool
 * 判断登录地推登录账号是否运营商
 */
function pushQrUser_is_operate()
{
    if (!empty(session('agent_info')) && session('member_type') == 2 && (session('pushQr_user_info')->level == 1 || empty(session('pushQr_user_info')))) {
        $mobile = session('agent_info')->mobile;
        //强制登录
        $info = \App\Models\PushQrUser::where(['mobile' => $mobile])->first();
        //session存储用户信息
        session(['pushQr_user_info' => $info]);
        //是运营中心
        return true;
    }
    return false;
}

function is_json($string)
{
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}

/**
 * @param $amount
 * @return string
 * @return $pay_type 支付方式 alipay,wechat,bank
 */
function is_rand_amount($amount, $pay_type = 'alipay', $is_true = 0)
{
    if (env('APP_ENV') == 'online') {
        $checkDayStr = date('Y-m-d ', time());
        $timeBegin1 = strtotime($checkDayStr . "09:00" . ":00");
        $timeEnd1 = strtotime($checkDayStr . "22:00" . ":00");
        $curr_time = time();
        if (($curr_time < $timeBegin1 || $curr_time > $timeEnd1) && $is_true) {
//            throw new ApiException('消费时间为09:00-22:00');
        }

        if ($pay_type == 'alipay' || $pay_type == 'wechat') {
            //支付宝/微信 限额1w
            if ($amount > 10000) {
                throw new ApiException('支付金额不能超过10000');
            } elseif ($amount >= 1000 && $amount % 100 == 0 && is_int($amount)) {
                throw new ApiException('支付金额不能是100的倍数');
            }
        } elseif ($pay_type == 'bank') {
            //银行卡 限额5w
            if ($amount > 50000) {
                throw new ApiException('支付金额不能超过50000');
            } elseif ($amount >= 1000 && $amount % 100 == 0 && is_int($amount)) {
                throw new ApiException('支付金额不能是10的倍数');
            }
        }
    }

    return true;
}

/**
 * 获取客户端ip
 */
function getClientIp()
{
    if (getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
        $ip = getenv('HTTP_CLIENT_IP');
    } elseif (getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
        $ip = getenv('HTTP_X_FORWARDED_FOR');
    } elseif (getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown')) {
        $ip = getenv('REMOTE_ADDR');
    } elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    $res = preg_match('/[\d\.]{7,15}/', $ip, $matches) ? $matches [0] : '';
    return $res;
}

/**
 * @param $pay_type
 * @return string
 * 获取订单支付方式
 */
function get_pay_type_name($pay_type)
{
    switch ($pay_type) {
        case 1:
            $pay_type_name = "微信";
            break;
        case 2:
            $pay_type_name = "支付宝";
            break;
        case 3:
            $pay_type_name = "银行卡";
            break;
        default:
            $pay_type_name = "其他";
    }
    return $pay_type_name;
}

/**
 * @param $notify_type
 * @return string
 * 获取订单支付公司名称
 */
function get_notify_type_name($notify_type)
{
    $notify_type_list = config('pay.notify_type');
    return $notify_type_list[$notify_type]['name'];
}


/**
 * @param $notify_type
 * @return string
 * 获取提现支付类型名称
 */
function get_withdraw_type_name($withdraw_type)
{
    $withdraw_type_list = config('pay.withdraw_type');
    return $withdraw_type_list[$withdraw_type];
}


// 重定向并带post数据
function build_form($url, $post_data, $method = 'post')
{
    $input_param = '';
    foreach ($post_data as $k => $v) {
        $input_param .= '<input type="hidden" name="' . $k . '" value="' . $v . '" />';
    }
    /**
     * <meta charset="utf-8" />
     * <meta http-equiv="content-type" name="keywords" content="" />
     * <meta http-equiv="content-type" name="description" content="" />
     * <meta name="viewport" content="width=device-width, initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
     * <meta name="format-detection" content="telephone=no" />
     * <meta name="apple-mobile-web-app-capable" content="yes" />
     * <meta name="apple-mobile-web-app-status-bar-style" content="black" />
     */
    $html = <<<EOT
<form method="$method" action="$url" id="form_to_gpu">
$input_param
<input type="submit" value=""  style="opacity:0" />
</form>
<script>document.getElementById('form_to_gpu').submit();</script>
EOT;
    $html = str_replace("\r\n", '', $html);
    $html = str_replace(PHP_EOL, '', $html);
    return $html;
}

//验证身份证
function is_idcard($id)
{
    $id = strtoupper($id);
    $regx = "/(^\d{15}$)|(^\d{17}([0-9]|X)$)/";
    $arr_split = array();
    if (!preg_match($regx, $id)) {
        return FALSE;
    }
    if (15 == strlen($id)) //检查15位
    {
        $regx = "/^(\d{6})+(\d{2})+(\d{2})+(\d{2})+(\d{3})$/";

        @preg_match($regx, $id, $arr_split);
        //检查生日日期是否正确
        $dtm_birth = "19" . $arr_split[2] . '/' . $arr_split[3] . '/' . $arr_split[4];
        if (!strtotime($dtm_birth)) {
            return FALSE;
        } else {
            return TRUE;
        }
    } else      //检查18位
    {
        $regx = "/^(\d{6})+(\d{4})+(\d{2})+(\d{2})+(\d{3})([0-9]|X)$/";
        @preg_match($regx, $id, $arr_split);
        $dtm_birth = $arr_split[2] . '/' . $arr_split[3] . '/' . $arr_split[4];
        if (!strtotime($dtm_birth)) //检查生日日期是否正确
        {
            return FALSE;
        } else {
            //检验18位身份证的校验码是否正确。
            //校验位按照ISO 7064:1983.MOD 11-2的规定生成，X可以认为是数字10。
            $arr_int = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
            $arr_ch = array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');
            $sign = 0;
            for ($i = 0; $i < 17; $i++) {
                $b = (int)$id{$i};
                $w = $arr_int[$i];
                $sign += $b * $w;
            }
            $n = $sign % 11;
            $val_num = $arr_ch[$n];
            if ($val_num != substr($id, 17, 1)) {
                return FALSE;
            } //phpfensi.com
            else {
                return TRUE;
            }
        }
    }

}

/**
 * 后台成功跳转页面
 * @param string $title
 * @param $message
 * @return \Illuminate\Http\Response
 */
function return_success_page($message, $title = '成功提示')
{
    return response()->view('errors.success', ['title' => $title, 'message' => $message]);
}

/**
 * 后台失败跳转页面
 * @param string $title
 * @param $message
 * @return \Illuminate\Http\Response
 */
function return_error_page($message, $title = '错误提示')
{
    return response()->view('errors.error', ['title' => $title, 'message' => $message]);
}

function get__seller_purse_type_name_by_type($purse_type)
{
    switch ($purse_type) {
        case 1:
            $str = '收益钱包';
            break;
        case 3:
            $str = '补贴积分';
            break;
        case 4:
            $str = '货款钱包';
            break;
        case 7:
            $str = '商家充值钱包';
            break;
        case 8:
            $str = '微信钱包';
            break;
        case 9:
            $str = '支付宝钱包';
            break;
    }
    return $str;
}

/**
 * @return bool
 * 时间限制
 */
function limit_time($star = "10:00", $end = "16:00")
{
    $checkDayStr = date('Y-m-d ', time());
    $timeBegin1 = strtotime($checkDayStr . $star . ":00");
    $timeEnd1 = strtotime($checkDayStr . $end . ":00");
    $curr_time = time();
    if ($curr_time < $timeBegin1 || $curr_time > $timeEnd1) {
        //不在时间点内
        return true;
    }
    return false;
}


/**
 * @param $credit
 * 根据商家信用返速率
 */
function get_seller_speech($credit)
{
    $credit_arr = config('seller.credit');
    $radio = $credit - 500;
    foreach ($credit_arr as $k => $v) {
        if ($radio < $v['max_radio'] && $radio >= $v['min_radio']) {
            return $v['speech'];
        }
    }
}

function getmicrotime()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

function pageBackError($message)
{
    return back()->withInput(request()->all())->withErrors(['error' => $message]);
}

function pageBackSuccess($message)
{
    return back()->with(['status' => $message]);
}

function get_order_type_name($order_type)
{
    $order_type_list = config('pay.order_type');
    foreach ($order_type_list as $k => $v) {
        if ($order_type == $k) {
            return $v;
        }
    }
    return '未知类型订单';
}

function ban_test()
{
    if (config('app.env') != 'online') {
        throw new ApiException('测试环境禁止操作');
    }
}

function mobileView($type = 'error', $title = '错误提示', $message)
{
    return response()->view('errors.' . $type, ['title' => $title, 'message' => $message]);
}


function sys_ban($is_open = true)
{
    if (config('app.env') == 'online' && $is_open == true) {
//        throw new ApiException('系统升级中,暂停操作');
    }
}

function del_dir($dir)
{
    if (!is_dir($dir)) {
        return false;
    }
    $handle = opendir($dir);
    while (($file = readdir($handle)) !== false) {
        if ($file != "." && $file != "..") {
            is_dir("$dir/$file") ? del_dir("$dir/$file") : @unlink("$dir/$file");
        }
    }
    if (readdir($handle) == false) {
        closedir($handle);
        @rmdir($dir);
    }
}

function clear_openid($mobile)
{
    $nodeid = User::where(['mobile' => $mobile])->value('nodeid');
    User::where(['mobile' => $mobile])->update(['openid' => '']);
    Agent::where(['mobile' => $mobile])->update(['openid' => '']);
    Seller::where(['mobile' => $mobile])->update(['openid' => '']);
    \App\Models\ShopTnetOauth::where(['node_id' => $nodeid])->delete();
    return true;
}

function throw_vaildator_error($validator)
{
    if ($validator->fails()) {
        foreach ($validator->errors()->toArray() as $k => $v) {
            throw new ApiException($v[0]);
        }
    }
}

//验证是否是手机号码
function is_mobile($phone)
{
    return preg_match("/^1[345678]{1}\d{9}$/", $phone);
}

/**
 * 未特商城进行签名，返回签名后的数据
 * @param $param
 * @return mixed
 */
function get_offline_sign_data($param)
{
    unset($param['sign']);
    ksort($param);
    $param2 = [];
    foreach ($param as $k => $v) {
        $param2[] = $k . '=' . $v;
    }
    $sign = strtolower(md5(implode('&', $param2) . env('api_code')));
    $param['sign'] = $sign;
    return $param;
}
