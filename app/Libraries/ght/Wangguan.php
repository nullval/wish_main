<?php
namespace App\Libraries\ght;

/**
 * Class Wangguan
 * @package App\Libraries\Libs\gaohuitong
 * 网关支付
 * 业务场景：订单生成后跳到高汇通微信公众号/支付宝服务窗支付对应金额
 */
class Wangguan {
	protected $test = true;
	public $url = '';			// 高汇通请求地址
	public $url_trans = '';		// 中转地址
	protected $merchant_no = '';	// 高汇通账户的商户号
	protected $terminal_no = '';	// 高汇通账户的终端号
	protected $merchant_key = '';	// 高汇通账户的秘钥
	
	public $return_url = '';
	public $notify_url = '';
	public $error = '';
	
	protected $post_url = 'https://epay.gaohuitong.com/entry.do';
//	protected $post_url_test = 'http://test.gaohuitong.com/entry.do';
	protected $post_url_test = 'http://test.pengjv.com/entry.do';
	
	
	/**
	 * Gaohuitong constructor.
	 * @param $trans_url	// 中转地址
	 * @param $return_url	// 同步回调地址
	 * @param string $notify_url	// 异步回调地址
	 * @param boolean $test		// 是否是测试,默认true
	 */
	public function __construct($trans_url,$return_url,$notify_url,bool $test = true){
		$this->test = $test;
		$this->return_url = $return_url;
		$this->notify_url = $notify_url;
		$this->url_trans = $trans_url;
		$this->url = $this->test ? $this->post_url_test : $this->post_url;
      $this->merchant_no = $this->test ? '102100000125' : '549440153996693'; //宝贝宝
//		$this->merchant_no = $this->test ? '102100000125' : '549440153996722'; //全优丰汇
      $this->terminal_no = $this->test ? '20000147' : '20030688';  //宝贝宝
//		$this->terminal_no = $this->test ? '20000147' : '20037047'; //全优丰汇
      $this->merchant_key = $this->test ? '857e6g8y51b5k365f7v954s50u24h14w' : '10eb8ee31cd6f1f4045059ae5400c49c';//宝贝宝
//		$this->merchant_key = $this->test ? '857e6g8y51b5k365f7v954s50u24h14w' : 'edb32b63ee669bddad492b2e773b949a';//全优丰汇
	}
	
	public function getError(){
		return $this->error;
	}
	
	/**
	 * 银行卡支付
	 * @param $child_merchant_no
	 * @param $order_no
	 * @param $product_name	// 商品名/商品简介
	 * @param $amount		// 订单金额，单位元，保留两位
	 * @param $pay_card_no	// 银行卡号
	 * @return mixed
	 */
	public function cardPay($child_merchant_no,$order_no,$product_name,float $amount,$pay_card_no){
		$param = [
			'busi_code'			=> 'PAY',
			'merchant_no'		=> $this->merchant_no,
			'child_merchant_no'	=> $child_merchant_no,
			'terminal_no'		=> $this->terminal_no,
			'bank_code'			=> strtoupper('ABCQBY'),		// 银行直连代码固定
			'user_bank_card_no'	=> $pay_card_no,
			'order_no'			=> $order_no,
			'amount'			=> $amount,
			'currency_type'		=> 'CNY',
			'sett_currency_type'=> 'CNY',
			'product_name'		=> $product_name,
			'product_desc'		=> $product_name,
			'base64_memo'		=> base64_encode($child_merchant_no),		// 订单备注为商户手机号
			'return_url'		=> $this->return_url,
			'notify_url'		=> $this->notify_url,
		];
		logger('post参数','./log/cardPay.php');
		logger($param,'./log/cardPay.php');
		$html = $this->_post($param);
		return $html;
	}
	
	
	/**
	 * 公众号/服务窗支付
	 * @param $child_merchant_no
	 * @param $order_no
	 * @param $product_name	// 商品名/商品简介
	 * @param $amount		// 订单金额，单位元，保留两位
	 * @param $pay_type		// 用户用支付宝或微信扫吗的，PUBLICALIPAY 支付宝/PUBLICWECHAT 微信
	 * @param $openid		// 用户微信openid
	 * @return mixed
	 */
	public function publicPay($child_merchant_no,$order_no,$product_name,$pay_type,float $amount,$openid){
		//如果是其他公众号支付就传其它公众号的app_id和openid，默认是全优丰汇的app_id
		$app_id = 'wx6e6f5d388ee3340f';
		$param = [
			'version'			=> '1.0.0',
			'busi_code'			=> 'PAY',
			'merchant_no'		=> $this->merchant_no,
			'child_merchant_no'	=> $child_merchant_no,
			'sub_mer_no'		=> $child_merchant_no,
			'sub_mer_name'		=> '松岛枫',
			'terminal_no'		=> $this->terminal_no,
			'order_no'			=> $order_no,
			'bank_code'			=> strtoupper($pay_type),
			'amount'			=> $amount,
			'currency_type'		=> 'CNY',
			'sett_currency_type'=> 'CNY',
			'product_name'		=> $product_name,
			'product_desc'		=> $product_name,
			'base64_memo'		=> base64_encode($child_merchant_no),		// 订单备注为商户手机号
			'return_url'		=> $this->return_url,
			'error_url'			=> $this->return_url,
			'notify_url'		=> $this->notify_url,
		  	'user_bank_card_no' => $openid, //用户openid
			'app_id' => $app_id //微信app_id
		];
		new_logger('./log/publicPay.php','post参数',$param);
		$html = $this->_post($param);
		return $html;
	}
	
	/**
	 * @param $order_no	// 由本地项目生成的订单号
	 * @return bool|mixed
	 * 订单查询
	 */
	public function queryOrder($order_no){
		$param = [
			'busi_code'			=> 'SEARCH',
			'merchant_no'		=> $this->merchant_no,
			'terminal_no'		=> $this->terminal_no,
			'order_no'			=> $order_no,
		];
		$html = $this->_post($param);
		return $html;
	}
	
	protected function _post($param){
		$param = $this->_sign($param);
		$html = $this->buildForm($param);
		return $html;
	}
	
	protected function _sync_post($param){
		$xml = curl_post($this->url,$param);
		$return = $this->decodeXml($xml);
		$sign_key = $this->_signParam($return);
		if($sign_key != $return['sign']){
			$this->error = '返回值内算签未通过';
			return false;
		}
		if($return['resp_code'] != '00'){
			$this->error = $return['resp_desc'];
			return false;
		}
		return $return;
	}
	
	protected function _sign($param){
		$sha256 = $this->_signParam($param);
		// 把签名结果追加到原参数后面
		$param['sign'] = $sha256;
		return $param;
	}
	
	// 传递参数数组，计算签名
	protected function _signParam($param){
		$new_param = [];
		if(isset($param['sign']))
			unset($param['sign']);
		foreach($param as $k => $v){
			if($v === '' || $v === null){
				// 空字符和null不用传递，也不参与算签
				unset($param[$k]);
			}else{
				$new_param[] = $k.'='.$v;
			}
		}
		sort($new_param);
		$new_param[] = 'key='.$this->merchant_key;
		$param_string = implode('&',$new_param);
		$sha256 = hash('sha256',$param_string);
		$sha256 = strtolower($sha256);
		return $sha256;
	}
	
	// 根据返回值验证签名
	public function verifyParam($param){
		$new_param = [];
		$param_sign = $param['sign'];
		if(isset($param['sign']))
			unset($param['sign']);
		foreach($param as $k => $v){
			if($v === '' || $v === null){
				// 空字符和null不用传递，也不参与算签
				unset($param[$k]);
			}else{
				$new_param[] = $k.'='.$v;
			}
		}
		sort($new_param);
		$new_param[] = 'key='.$this->merchant_key;
		$param_string = implode('&',$new_param);
		$sha256 = hash('sha256',$param_string);
		$sha256 = strtolower($sha256);
		return $sha256 == $param_sign;
	}
	
	// 根据参数建立form表单提交给高汇通
	protected function buildForm($param){
		$input_param = '';
		foreach($param as $k => $v){
			$input_param .= '<input type="hidden" name="'.$k.'" value="'.$v.'" />';
		}
		$html = <<<EOT
<form method="post" action="$this->url_trans" id="form_to_pay">
$input_param
<input type="submit" value="　" style="opacity:0" />
</form>
<script>document.getElementById('form_to_pay').submit();</script>
EOT;
		$html = str_replace("\r\n",'',$html);
		$html = str_replace(PHP_EOL,'',$html);
		return $html;
	}
	
	protected function decodeXml($xml){
		$xml_array = $this->xml2array($xml);
//		if($xml_array['root']['resp_code'])
		return $xml_array;
	}
	
	// xml 转成数组
	protected function xml2array($xml){
		$xml_object = simplexml_load_string($xml);
		$xml_array = json_decode(json_encode($xml_object),true);
		return $xml_array;
	}
}