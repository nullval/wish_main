<?php
namespace App\Libraries\gaohuitong;

/**
 * **********************************************
 *                   商户入驻                    *
 * **********************************************
 */
class Ruzhu {
	protected $agency_id = '';
	protected $terminal_no = '';	// 高汇通账户的终端号
	protected $rand_key = '';
	protected $aes_key = '';
	protected $rsa_private_key = '';
	protected $rsa_public_key = '';
	protected $test = true;
	protected $reg_sn = '';
	
	protected $post_basic_url = '/interfaceWeb/basicInfo';
	protected $post_bank_url = '/interfaceWeb/bankInfo';
	protected $post_submit_url = '/interfaceWeb/busiInfo';
	protected $post_query_info_url = '/interfaceWeb/qryAuthInfo';
	protected $post_query_balance_url = '/interfaceWeb/qryBalanceInfo';
	protected $post_query_banklist_url = '/interfaceWeb/qryCardInfo';
    protected $post_query_busi_url = '/interfaceWeb/qryBusi';
	protected $post_pay = '/interfaceWeb/realTimeDF';	// 结算
	protected $post_pay_query = '/interfaceWeb/queryResultDF';	// 结算结果查询
	
	public $error = '';
	
	/**
	 * Ruzhu constructor.
	 * @param $merchant_no	// 商户号
	 * @param bool $test	// 是否是测试环境，默认true
	 */
	public function __construct(bool $test = true){
		$this->test = $test;
		$this->agency_id = env('GHT_AGENCY_ID');
		$this->terminal_no = env('GHT_TERMINNAL_NO');
		$rand_md5 = strtolower(md5(rand(10000,99999)));
		$rand_md5 = substr($rand_md5,0,-16);
//		$rand_md5 = '1122334455667788';
		$this->rand_key = $rand_md5;
		if(env('APP_DEBUG')==true){
            $this->rsa_public_key = file_get_contents(app_path().'/Libraries/ght/rsa_public_key_2048.pem');
            $this->rsa_private_key = file_get_contents(app_path().'/Libraries/ght/pkcs8_rsa_private_key_2048.pem');
        }else{
            $this->rsa_public_key = file_get_contents(app_path().'/Libraries/ght/GHT_ROOT.pem');
            $this->rsa_private_key = file_get_contents(app_path().'/Libraries/ght/549440153996693.pem');
        }
        $url_pre=env('GHT_RUL');
		$this->post_basic_url = $url_pre . $this->post_basic_url;
		$this->post_bank_url = $url_pre . $this->post_bank_url;
		$this->post_submit_url = $url_pre . $this->post_submit_url;
		$this->post_query_info_url = $url_pre . $this->post_query_info_url;
		$this->post_query_balance_url = $url_pre . $this->post_query_balance_url;
		$this->post_query_banklist_url = $url_pre . $this->post_query_banklist_url;
        $this->post_query_busi_url = $url_pre . $this->post_query_busi_url;
		$this->post_pay = $url_pre . $this->post_pay;
		$this->post_pay_query = $url_pre . $this->post_pay_query;
	}
	
	public function getError(){
		return $this->error;
	}
	
	// 商家入驻--基本信息 todo 必填验证
	/**
	 * @param $param		// 构造参数
	 * @return mixed		// 失败返回false，成功后返回商户手机号码(string)
	 * 商家入驻
	 */
	public function basic($param){
		$url = $this->post_basic_url;
		$tranCode = '100001';
//		$param = [
//			'merchantName'			=> '全优丰汇',	// 店名，要与营业执照上一致
//			'shortName'				=> '全优丰汇',
//			'city'					=> '5840',		// 城市id
//			'merchantAddress'		=> '广东深圳',	// 城市中文名
//			'servicePhone'			=> '0755-02515451',			// 客服电话
////			'orgCode'				=> '1234567891234567',		// 组织机构代码
//			'merchantType'			=> '01',		// 00公司，01个体
//			'category'				=> '5311',		// 类别id
//			'corpmanName'			=> '晏勇',		// 法人姓名
//			'corpmanId'				=> '421302199212250415',	// 法人身份证
////			'corpmanPhone'			=> '658777777',		// 法人联系座机
//			'corpmanMobile'			=> '17099912460',	// 法人联系手机
//			'corpmanEmail'			=> '512511253@qq.com',
//			'bankCode'				=> '102',
//			'bankName'				=> '中国工商银行',
//			'bankaccountNo'			=> '6212263602058162728',
//			'bankaccountName'		=> '晏勇',
//			'autoCus'				=> '1',		// 0：不自动提现 1：自动提现
//			'remark'				=> '2017-03-23日18购新改版',
//		];
        $param = [
			'handleType'			=> $param['handleType'],
			'merchantName'			=> $param['merchantName'],	// 店名，要与营业执照上一致
            'shortName'				=> $param['shortName'],
            'city'					=> $param['city'],		// 城市id
            'merchantAddress'		=> $param['merchantAddress'],	// 城市中文名
            'servicePhone'			=> $param['servicePhone'],			// 客服电话
//			'orgCode'				=> '1234567891234567',		// 组织机构代码
            'merchantType'			=> $param['merchantType'],		// 00公司，01个体
            'category'				=> $param['category'],		// 类别id
            'corpmanName'			=> $param['corpmanName'],		// 法人姓名
            'corpmanId'				=> $param['corpmanId'],	// 法人身份证
//			'corpmanPhone'			=> '658777777',		// 法人联系座机
            'corpmanMobile'			=> $param['corpmanMobile'],	// 法人联系手机
//            'corpmanEmail'			=> $param['corpmanEmail'],
            'bankCode'				=> $param['bankCode'],
            'bankName'				=> $param['bankName'],
            'bankaccountNo'			=> $param['bankaccountNo'],
            'bankaccountName'		=> $param['bankaccountName'],
            'autoCus'				=> 0,		// 0：不自动提现 1：自动提现
            'remark'				=> $param['remark'],
//            'version'				=> '2.0.0',
        ];
		$data = $this->_encryptParam($tranCode,$param);
		$text = curl_post($url,$data);
		$return = $this->_decrypt($text);
//		if(empty($return)){
//			return false;
//		}
		return $return;
	}
	
	// 商家入驻--银行卡信息
	/**
	 * @param $param		// 构造参数
	 * @return mixed		// 失败返回false，成功后返回商户手机号码(string)
	 */
	public function bank($param){
		$url = $this->post_bank_url;
		$tranCode = '100002';
//		$param = [
//			'merchantId'		=> '17099912460',
//			'handleType'		=> '0',		// 0新增，1删除
//			'bankCode'			=> '102',
//			'bankaccProp'		=> '0',		// 0私人，1公司
//			'name'				=> '晏勇',
//			'bankaccountNo'		=> '6212263602058162728',
//			'bankaccountType'	=> '1',
//			'certCode'			=> '1',		// 1身份证，2护照，3军官证，4回乡证，5台胞证，6港澳通行证，7国际海员证，8外国人永久居住证，9其他
//			'certNo'			=> '421302199212250415',
//			'defaultAcc'		=> '1',		// 设置为默认银行卡
//		];
        $param = [
			'merchantId'		=> $param['merchantId'],
			'handleType'		=> $param['handleType'],		// 0新增，1删除
			'bankCode'			=> $param['bankCode'],
			'bankaccProp'		=> $param['bankaccProp'],		// 0私人，1公司
			'name'				=> $param['name'],
			'bankaccountNo'		=> $param['bankaccountNo'],
			'bankaccountType'	=> $param['bankaccountType'],
            'certCode'			=> '1',			// 1身份证，2护照，3军官证，4回乡证，5台胞证，6港澳通行证，7国际海员证，8外国人永久居住证，9其他
			'certNo'			=> $param['certNo'],
			'defaultAcc'		=> '1',		// 设置为默认银行卡
//			'version'				=> '2.0.0',

		];
		new_logger('ruzhubank.log','请求的高汇通登记银行信息接口信息',$param);
		$data = $this->_encryptParam($tranCode,$param);
		$text = curl_post($url,$data);
		$return = $this->_decrypt($text);
//		if(empty($return)){
//			return false;
//		}
		return $return;
	}
	
	// 商家入驻--银行卡信息
	/**
	 * @param $param		// 构造参数
	 * @return mixed		// 失败返回false，成功后返回商户手机号码(string)
	 */
	public function submit($param){
		$url = $this->post_submit_url;
		$tranCode = '100003';
//		$post_param = [
//			'merchantId'		=> '18312488863',
//			'handleType'		=> '0',		// 0：新增 1：修改 2：关闭业务 3：重新开通
//			'cycleValue'		=> '2',		// 计算周期，1:T+1、2:D+0
//			'allotFlag'			=> '0',		// 当子商户账户余额不够时，是否允 许从机构账户调拨，默认0
//			'futureRate'		=> '2',		// 设定商家让利百分比
//		];
        $post_param = [
			'merchantId'		=> $param['merchantId'],
			'handleType'		=> $param['handleType'],		// 0：新增 1：修改 2：关闭业务 3：重新开通
			'cycleValue'		=> '2',		// 计算周期，1:T+1、2:D+0
			'allotFlag'			=> '0',		// 当子商户账户余额不够时，是否允 许从机构账户调拨，默认0
			'futureRate'		=> $param['futureRate'],		// 设定商家让利百分比
//			'version'				=> '2.0.0',

		];
		$future_rate = $param['futureRate'];		// 商家让利百分比
		unset($param['futureRate']);
		// 固定开通所有业务，因为没有接口查询商家开通了哪些业务，一次全开通即可
		$busiList = [
			[
				'busiCode'			=> 'B00105',	// 快捷支付（借记卡）
				'futureRateType'	=> '1',		// 费率类型，1百分比，2单笔固定金额
				'futureRateValue'	=> $future_rate,		// N% 费率
			],
			[
				'busiCode'			=> 'B00108',	// 快捷支付（信用卡）
				'futureRateType'	=> '1',		// 费率类型，1百分比，2单笔固定金额
				'futureRateValue'	=> $future_rate,		// N% 费率
			],
			[
				'busiCode'			=> 'B00107',	// 扫码支付（微信）
				'futureRateType'	=> '1',		// 费率类型，1百分比，2单笔固定金额
				'futureRateValue'	=> $future_rate,		// N% 费率
			],
			[
				'busiCode'			=> 'B00109',	// 扫码支付（支付宝）
				'futureRateType'	=> '1',		// 费率类型，1百分比，2单笔固定金额
				'futureRateValue'	=> $future_rate,		// N% 费率
			],
			[
				'busiCode'			=> 'B00110',	// 刷卡支付（微信）
				'futureRateType'	=> '1',		// 费率类型，1百分比，2单笔固定金额
				'futureRateValue'	=> $future_rate,		// N% 费率
			],
			[
				'busiCode'			=> 'B00112',	// 刷卡支付（支付宝）
				'futureRateType'	=> '1',		// 费率类型，1百分比，2单笔固定金额
				'futureRateValue'	=> $future_rate,		// N% 费率
			],
			[
				'busiCode'			=> 'B00302',	// 代付
				'futureRateType'	=> '2',		// 费率类型，1百分比，2单笔固定金额
				'futureRateValue'	=> '3',		// 代付1.5元
			],
			[
				'busiCode'			=> 'B00114',	// 微信公众号支付
				'futureRateType'	=> '1',		// 费率类型，1百分比，2单笔固定金额
				'futureRateValue'	=> $future_rate,		// N% 费率
			],
			[
				'busiCode'			=> 'B00116',	// 支付宝服务窗
				'futureRateType'	=> '1',		// 费率类型，1百分比，2单笔固定金额
				'futureRateValue'	=> $future_rate,		// N% 费率
			]
		];
		$post_param['busiList'] = $busiList;
		$data = $this->_encryptParam($tranCode,$post_param);
		$text = curl_post($url,$data);
		$return = $this->_decrypt($text);
//		if(empty($return)){
//			return false;
//		}
		return $return;
	}
	
	/**
	 * @param $merchant_id
	 * @return mixed	// 失败返回false，成功返回商家信息数组
	 */
	public function queryInfo($merchant_id){
		$url = $this->post_query_info_url;
		$tranCode = '100004';
		$post_param = [
			'merchantId'		=> $merchant_id,
		];
		$data = $this->_encryptParam($tranCode,$post_param);
		$text = curl_post($url,$data);
//        var_dump($text);exit;
		$return = $this->_decrypt($text);
//		if(empty($return)){
//			return false;
//		}
		return $return;
	}
	
	/**
	 * @param $merchant_id
	 * @return mixed	// 失败返回false，成功返回商家余额数组
	 */
	public function queryBalance($merchant_id){
		$url = $this->post_query_balance_url;
		$tranCode = '100005';
		$post_param = [
			'merchantId'		=> $merchant_id,
		];
		$data = $this->_encryptParam($tranCode,$post_param);
		$text = curl_post($url,$data);
		$return = $this->_decrypt($text);
//		if(empty($return)){
//			return false;
//		}
		return $return;
	}
	
	/**
	 * @param $merchant_id
	 * @return mixed	// 失败返回false，成功返回商家银行卡数组
	 */
	public function queryBanklist($merchant_id){
		$url = $this->post_query_banklist_url;
		$tranCode = '100006';
		$post_param = [
			'merchantId'		=> $merchant_id,
		];
		$data = $this->_encryptParam($tranCode,$post_param);
		$text = curl_post($url,$data);
		$return = $this->_decrypt($text);
		return $return;
	}

    /**
     * @param $merchant_id
     * @return mixed	业务信息查询
     */
    public function queryBusi($merchant_id){
        $url = $this->post_query_busi_url;
        $tranCode = '100007';
        $post_param = [
            'merchantId'		=> $merchant_id,
        ];
        $data = $this->_encryptParam($tranCode,$post_param);
//        var_dump($data);exit;
        $text = curl_post($url,$data);
        $return = $this->_decrypt($text);
        return $return;
    }
	
	public function pay($bank_param){
		$url = $this->post_pay;
		$tranCode = '200001';
//		$param = [
//			'user_id'			=> '18312488863',
//			'bank_code'			=> '306',
//			'account_no'		=> '622568022100380598923432',
//			'account_name'		=> '赖桂明',
//			'amount'			=> '1',
//			'extra_fee'			=> '0',
//			'ID'				=> '44098119930619227X',
//		];
		$param = [
			'user_id'			=> $bank_param['user_id'],
			'bank_code'			=> $bank_param['bank_code'],
			'account_no'		=> $bank_param['account_no'],
			'account_name'		=> $bank_param['account_name'],
			'amount'			=> $bank_param['amount'],
			'extra_fee'			=> '0',
			'ID'				=> $bank_param['ID'],
		];
		$param['business_code'] = 'B00302';
		$param['terminal_no'] = $this->terminal_no;
		$param['amount'] *= 100;
		$param['extra_fee'] *= 100;
		$data = $this->_encryptParam($tranCode,$param);
		$text = curl_post($url,$data);
		$return = $this->_decrypt($text);
        return $return;
	}
	
	public function payQuery($child_merchant_id,$req_sn){
		$url = $this->post_pay_query;
		$tranCode = '200002';
		$param = [
			'User_id'			=> $child_merchant_id,
			'Query_sn'			=> $req_sn,
		];
		$data = $this->_encryptParam($tranCode,$param);
		$text = curl_post($url,$data);
		$return = $this->_decrypt($text);
		return $return;
	}
	
	
	// 根据参数生成加密数据，返回需要的5个参数
	protected function _encryptParam($tranCode,$param){
		$xml = $this->_param2Xml($tranCode,$param);
		$data = [
			'encryptData'	=> $this->_encryptData($xml),		// 加密后的请求报文
			'encryptKey'	=> $this->_encryptKey(),		// 加密后的AES对称密钥
			'agencyId'		=> $this->agency_id,		// 机构标识
			'signData'		=> $this->_signData($xml),		// 请求报文签名
			'tranCode'		=> $tranCode,		// 交易服务码
		];
		return $data;
	}
	
	// xml 转 encryptData 的base64字符串
	protected function _encryptData($xml){
		$size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
		$input = $this->pkcs5_pad($xml, $size);
		$td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_ECB, '');
		$iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
		mcrypt_generic_init($td, $this->rand_key, $iv);
		$data = mcrypt_generic($td, $input);
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		$data = base64_encode($data);
		return $data;
	}
	private function pkcs5_pad($text, $blocksize) {
		$pad = $blocksize - (strlen($text) % $blocksize);
		return $text . str_repeat(chr($pad), $pad);
	}
	
	// 加密 AES key
	protected function _encryptKey(){
		openssl_public_encrypt($this->rand_key,$encrypted,$this->rsa_public_key);//私钥加密
		$encrypt = base64_encode($encrypted);
		$this->aes_key = $encrypt;
		return $encrypt;
	}
	
	// 使用私钥对xml签名
	protected function _signData($xml){
		$res = openssl_get_privatekey($this->rsa_private_key);
		openssl_sign($xml, $sign, $res);
		openssl_free_key($res);
		//base64编码
		$sign_base64 = base64_encode($sign);
//		$ok = openssl_verify($xml, $sign, $this->rsa_public_key);		// 验证算签
		return $sign_base64;
	}
	
	protected function _param2Xml($tran_code,$param_body){
		$param_string = '';
		$date = date('YmdHis');
		foreach($param_body as $k => $v){
			if(is_array($v)){
				foreach($v as $k2 => $v2){
					$param_string .= '<'.$k.'>';
					foreach($v2 as $k3 => $v3){
						$param_string .= '<'.$k3.'>'.$v3.'</'.$k3.'>';
					}
					$param_string .= '</'.$k.'>';
				}
			}else{
				$param_string .= '<'.$k.'>'.$v.'</'.$k.'>';
			}
		}
		$this->reg_sn = $date.rand(10000,99999);
		$xml = <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
<merchant>
<head>
<version>1.0.0</version>
<msgType>01</msgType>
<agencyId>$this->agency_id</agencyId>
<tranCode>$tran_code</tranCode>
<reqMsgId>$this->reg_sn</reqMsgId>
<reqDate>$date</reqDate>
</head>
<body>
$param_string
</body>
</merchant>
EOT;
		$xml = str_replace("\r\n",'',$xml);
		$xml = str_replace(PHP_EOL,'',$xml);
		return $xml;
	}
	
	
	
	
	/************************************
	 * *            解密部分             *
	 * **********************************
	 */
	// 根据高汇通返回的所有参数进行拆分解密
	protected function _decrypt($text){
		$params = explode('&',$text);
		$params_new = [];
		foreach($params as $v){
			$kv_k = stristr($v,'=',true);
			$kv_v = substr(stristr($v,'='),1);
			$params_new[$kv_k] = $kv_v;
		}
//		var_dump($params_new['encryptKey']);exit;
		$aes_key = $this->_decryptKey($params_new['encryptKey']);
		$data_xml = $this->_decryptData($aes_key,$params_new['encryptData']);
		// 签名计算，略过
//		$sign_check = $this->_unsignData($data,$params_new['signData']);
//		if($sign_check == false){
//			return false;
//		}
		$data_json = $this->xml2array($data_xml);
        return $data_json;
		// 如果成功
		if($data_json['head']['respType'] == 'S' && $data_json['head']['respCode'] == '000000'){
			$this->error = '';
			return $data_json['body'];
		}else if($data_json['head']['respType'] == 'E'){
			$this->error = $data_json['head']['respMsg'];
			return false;
		}else{
			// 未确定 $data_json['head']['respType'] == 'R'
			$this->error = $data_json['head']['respMsg'];
			return null;
		}
	}
	protected function _decryptKey($encrypt_key){
		openssl_private_decrypt(base64_decode($encrypt_key),$decrypted,$this->rsa_private_key);
		return $decrypted;
	}
	
	protected function _decryptData($aes_key,$encrypt_data){
		$decrypted= mcrypt_decrypt(
			MCRYPT_RIJNDAEL_128,
			$aes_key,
			base64_decode($encrypt_data),
			MCRYPT_MODE_ECB
		);
//        var_dump($decrypted);exit;
		
		$dec_s = strlen($decrypted);
		$padding = ord($decrypted[$dec_s-1]);
		$decrypted = substr($decrypted, 0, -$padding);
		$decrypted = str_replace("\n",'',$decrypted);
		return $decrypted;
	}
	
	protected function _unsignData($xml,$sign_data){
		$ok = openssl_verify($xml, $sign_data, $this->rsa_public_key);
		return $ok == 1 ? true : false;
	}
	
	// xml 转成数组
	protected function xml2array($xml){
		$xml_object = simplexml_load_string($xml);
		$xml_array = json_decode(json_encode($xml_object),true);
		return $xml_array;
	}
}