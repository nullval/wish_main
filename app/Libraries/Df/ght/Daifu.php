<?php
namespace App\Libraries\Df\ght;


class Daifu {
	protected $test = true;
	protected $url = '';			// 高汇通请求地址
	protected $merchant_no = '';	// 高汇通账户的商户号
	protected $sign_cert = '';
	protected $sign_cert_password = '123456';
	protected $design_cert = '';
	protected $sign_cert_server = '';
	protected $signature = '';
	
	public $error = '';
	
	
	protected $post_url = 'https://rps.gaohuitong.com:8443/d/merchant/';
	protected $post_url_test = 'http://120.31.132.118:8080/d/merchant/';
	
	/**
	 * Gaohuitong constructor.
	 * @param $return_url	// 同步回调地址
	 * @param string $notify_url	// 异步回调地址
	 * @param boolean $test		// 是否是测试,默认true
	 */
	public function __construct(bool $test = false){
//		$this->test = $test;
//		$this->url = $this->test ? $this->post_url_test : $this->post_url;
//		$this->merchant_no = $this->test ? '000000000100641' : '000000000102285';//宝贝宝
//		$this->merchant_no_short = substr($this->merchant_no,-6).'，创智付，';      // 商户号后六位，做备注操作
//
//		$this->sign_cert = $this->test ? __DIR__.'/daifu_cert/TESTUSER.pfx' : __DIR__.'/daifu_cert/000000000102285.pfx'; //宝贝宝
//		$this->design_cert = $this->test ? __DIR__.'/daifu_cert/TESTUSER.cer' : __DIR__.'/daifu_cert/000000000102285.crt'; //宝贝宝
//
//
//		$this->sign_cert_server = $this->test ? [] : [
//			//宝贝宝
//			'cert'      => [__DIR__.'/daifu_cert/000000000102285.pem',$this->sign_cert_password],
//			'ssl_key'   => __DIR__.'/daifu_cert/000000000102285_key.pem',
//
//		];
        $this->test = $test;
        $this->url =  $this->post_url;
        $this->merchant_no =  '000000000101431';//宝贝宝
        $this->merchant_no_short = substr($this->merchant_no,-6).'，宝贝宝，';      // 商户号后六位，做备注操作

        $this->sign_cert =  __DIR__.'/daifu_cert/000000000101431.pfx'; //宝贝宝
        $this->design_cert = __DIR__.'/daifu_cert/000000000101431.crt'; //宝贝宝


        $this->sign_cert_server = $this->test ? [] : [
            //宝贝宝
            'cert'      => [__DIR__.'/daifu_cert/000000000101431.pem',$this->sign_cert_password],
            'ssl_key'   => __DIR__.'/daifu_cert/000000000101431_key.pem',

        ];
	}
	
	public function test(){
		echo 'success';
	}
	public function getError(){
		return $this->error;
	}
	
	/**
	 * 实时代收请求，可理解为充值操作
	 * @param $req_sn	// 要做数据库存储，查询订单根据这个查询
	 * @param $param_body
	 * @param $remarks
	 * @return mixed
	 */
	public function income($req_sn,$param_body,$remarks = '代收'){
		$trx_code = '100004';
		$business_code = '14900';
//		$param_body = [
//			'BANK_CODE'			=> '102',
//			'ACCOUNT_NO'		=> '9558820200001323775',
//			'ACCOUNT_NAME'		=> '张四',
//			'AMOUNT'			=> '1000',	// 单位分
//			'ID'				=> '42130219911250415',		// 身份证号
//			'TEL'				=> '17099912460',		// 手机号码
//		];
		$param_body['SN'] = '0001';	// 下一个就是 0002
		$param_body['ACCOUNT_TYPE'] = '00';	// 00 银行卡(默认)，01存折
		$param_body['AMOUNT'] *= 100;	// 高汇通单位是分，故乘100
		$param_body['REMARK'] = $this->merchant_no_short . $remarks;	// 高汇通单位是分，故乘100
		$return = $this->_post($trx_code,$business_code,$req_sn,$param_body);
		return $return;
	}
	
	/**
	 * 实时代付请求，用于平台给用户打款
	 * @param $req_sn
	 * @param $param_body
	 * @param $remarks
	 * @return bool|string
	 */
	public function pay($req_sn,$param_body,$remarks = '代付'){
		$trx_code = '100005';
		$business_code = '09100';
//		$param_body = [
//			'BANK_CODE'			=> '102',
//			'ACCOUNT_NO'		=> '9558820200001323775',
//			'ACCOUNT_NAME'		=> '张三',
//			'AMOUNT'			=> '100',		// 单位分
//			'REMARK'			=> '测试入账',		// 备注，必填
//		];
		$param_body['SN'] = '0001';	// 下一个就是 0002
		$param_body['ACCOUNT_TYPE'] = '00';	// 00 银行卡(默认)，01存折
//		$param_body['AMOUNT'] *= 100;	// 高汇通单位是分，故乘100
		$param_body['REMARK'] = $this->merchant_no_short . $remarks;	// 高汇通单位是分，故乘100
        new_logger('ght_pay.log','代付订单号',[$req_sn]);
        new_logger('ght_pay.log','代付请求参数',$param_body);
		$return = $this->_post($trx_code,$business_code,$req_sn,$param_body);
		return $return;
	}
	
	/**
	 * 订单查询，根据原 req_sn 查询
	 * @param $req_sn
	 * @return mixed|bool
	 */
	public function queryOrder($req_sn){
		$trx_code = '200001';
		$param = [
			'QUERY_SN'		=> $req_sn,
		];
		$return_array = $this->_postQuery($trx_code,$param);
		return $return_array;
	}

	
	/**
	 * 根据日期查询历史请求
	 * @param string $start_time
	 * @param string $end_time
	 * @param int $page_num
	 * @param int $page_size
	 * @return bool|mixed
	 */
	public function queryHistory($start_time = '',$end_time = '',$page_num = 1,$page_size = 1000){
		$trx_code = '200002';
		$param = [
			'MERCHANT_ID'		=> $this->merchant_no,
			'BEGIN_DATE'		=> $start_time,
			'END_DATE'			=> $end_time,
			'PAGE_NUM'			=> $page_num,
			'PAGE_SIZE'			=> $page_size,
//			'QUERY_REMARK'		=> 1,
		];
		$return_array = $this->_postQuery($trx_code,$param);
		return $return_array;
	}
	
	/**
	 * 查询账户余额
	 * @return integer
	 */
	public function queryBalance(){
		$trx_code = '200004';
		$param = [
			'MERCHANT_ID'		=> $this->merchant_no,
		];
		$return_array = $this->_postQuery($trx_code,$param);
		//返回金额以分为单位
		return $return_array;
	}
	
	
	public function queryBankInfo($bank_no){
		$trx_code = '200007';
		$param = [
			'BANKNO'		=> $bank_no,
		];
		$return_array = $this->_postQuery($trx_code,$param);
		return $return_array;
	}
	
	protected function _post($trx_code,$business_code,$req_sn,$param){
		$xml = $this->_param2Xml($trx_code,$business_code,$req_sn,$param);
		$xml_signed = $this->_sign($xml);
		$xml_signed = iconv('utf-8','gbk',$xml_signed);
		$return_xml = curl_post($this->url,$xml_signed,$this->sign_cert_server);
		$return = $this->_decrypt($return_xml);
		return $return;
	}
	
	protected function _postQuery($trx_code,$param){
		$xml = $this->_query2Xml($trx_code,$param);
		$xml_signed = $this->_sign($xml);
		$xml_signed = iconv('utf-8','gbk',$xml_signed);
		$return_xml = curl_post($this->url,$xml_signed,$this->sign_cert_server);
		$ret_detail = $this->_decrypt($return_xml,'array');
//        new_logger('ght_pay.log','xml',[$ret_detail]);
		$ret_code = $ret_detail['RET_CODE'] ?? '0000';
		$ret_msg = $ret_detail['ERR_MSG'];
//        new_logger('ght_pay.log','错误信息',['ret_msg'=>$ret_msg]);
		$return = null;
		if($ret_code == '0000'){
			// 成功
//			$this->error = '';
			$return =  $ret_detail;
		}else if(substr($ret_code,0,1) == '0'){
			// 失败了，记录失败原因：$ret_msg
			$this->error = $ret_msg;
			$return =  false;
		}else if(substr($ret_code,0,1) == '2'){
			// 银行处理中，不予理睬
			$this->error = $ret_msg;
		}else{
			// 未知，不予理睬
			$this->error = $ret_msg;
		}
		return $return;
	}
	
	protected function _param2Xml($trx_code,$business_code,$req_sn,$param_body){
		$param_string = '';
		$trans_sum_string = '';
		$date = date('YmdHis');
		$trans_sum = [
			'BUSINESS_CODE'		=> $business_code,
			'MERCHANT_ID'		=> $this->merchant_no,
			'TOTAL_ITEM'		=> 1,
			'TOTAL_SUM'			=> $param_body['AMOUNT'],
		];
		foreach($trans_sum as $k => $v){
			$trans_sum_string .= '<'.$k.'>'.$v.'</'.$k.'>';
		}
		foreach($param_body as $k => $v){
			if(is_array($v)){
				foreach($v as $k2 => $v2){
					$param_string .= '<'.$k.'>';
					foreach($v2 as $k3 => $v3){
						if($v3 === '' || $v3 === null){
							continue;
						}
						$param_string .= '<'.$k3.'>'.$v3.'</'.$k3.'>';
					}
					$param_string .= '</'.$k.'>';
				}
			}else{
				if($v === '' || $v === null){
					continue;
				}
				$param_string .= '<'.$k.'>'.$v.'</'.$k.'>';
			}
		}
		$xml = <<<EOT
<?xml version="1.0" encoding="GBK"?>
<GHT>
<INFO>
<TRX_CODE>$trx_code</TRX_CODE>
<VERSION>04</VERSION>
<LEVEL>0</LEVEL>
<USER_NAME>$this->merchant_no</USER_NAME>
<REQ_SN>$req_sn</REQ_SN>
<SIGNED_MSG></SIGNED_MSG>
</INFO>
<BODY>
<TRANS_SUM>
$trans_sum_string
</TRANS_SUM>
<TRANS_DETAILS>
<TRANS_DETAIL>
$param_string
</TRANS_DETAIL>
</TRANS_DETAILS>
</BODY>
</GHT>
EOT;
		$xml = str_replace("\r\n",'',$xml);
		$xml = str_replace(PHP_EOL,'',$xml);
		return $xml;
	}
	
	protected function _query2Xml($trx_code,$param){
		$param_string = '';
		$date = date('YmdHis');
//		$req_sn = $date.rand(10000,99999);
		$req_sn ='11211231352';
		foreach($param as $k => $v){
			if($v === '' || $v === null){
				continue;
			}
			$param_string .= '<'.$k.'>'.$v.'</'.$k.'>';
		}
		$xml = <<<EOT
<?xml version="1.0" encoding="GBK"?>
<GHT>
<INFO>
<TRX_CODE>$trx_code</TRX_CODE>
<VERSION>03</VERSION>
<REQ_SN>$req_sn</REQ_SN>
<USER_NAME>$this->merchant_no</USER_NAME>
<SIGNED_MSG></SIGNED_MSG>
</INFO>
<BODY>
<QUERY_TRANS>
$param_string
</QUERY_TRANS>
</BODY>
</GHT>
EOT;
		$xml = str_replace("\r\n",'',$xml);
		$xml = str_replace(PHP_EOL,'',$xml);
		return $xml;
	}
	
	
	protected function _sign($xml){
		$sign_msg = $this->_signXml($xml);
		$sign_xml = str_replace('<SIGNED_MSG></SIGNED_MSG>','<SIGNED_MSG>'.$sign_msg.'</SIGNED_MSG>',$xml);
		return $sign_xml;
	}
	
	protected function _signXml($xml){
		$xml_no_sign = preg_replace('/<SIGNED_MSG>.*<\/SIGNED_MSG>/is','',$xml);
		openssl_pkcs12_read(file_get_contents($this->sign_cert), $certs, $this->sign_cert_password); //其中password为你的证书密码
		if(!$certs) return '';
		$public_key = $certs['cert'];
		$private_key = $certs['pkey'];
//		$res = openssl_get_privatekey($certs['pkey']);
		openssl_sign($xml_no_sign, $signature, $private_key);
		$hex_signature = bin2hex($signature);
		return $hex_signature;
	}
	
	
	protected function _curl($url,$xml_signed,$ssl = []){
		$https = substr($url, 0, 8) == "https://" ? true : false;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 13);
		if ($https) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);   // 只信任CA颁布的证书
			curl_setopt($ch, CURLOPT_CAINFO, $ssl['verify']); // CA根证书（用来验证的网站证书是否是CA颁布）
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); // 检查证书中是否设置域名，并且是否与提供的主机名匹配
			
			curl_setopt($ch, CURLOPT_SSLCERT, $ssl['cert'][0]);
			curl_setopt($ch, CURLOPT_SSLCERTPASSWD,$ssl['cert'][1]);
			curl_setopt($ch, CURLOPT_SSLKEY, $ssl['ssl_key']);
		}
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:')); //避免data数据过长问题
		curl_setopt($ch, CURLOPT_POST, true);
		if(is_string($xml_signed)){
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_signed);
		}else{
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($xml_signed)); //data with URLEncode
		}
		
		$ret = curl_exec($ch);
		//var_dump(curl_error($ch));  //查看报错信息
		
		curl_close($ch);
		return $ret;
	}
	
	
	/************************************
	 * *            解密部分             *
	 * **********************************
	 */
	protected function _decrypt($xml,$return_type = 'bool'){
		// 验证签名
//		$signed = $this->_designXml($xml);
//		if(!$signed){
//			$this->error = '验证签名失败';
//			return false;
//		}
		$data_array = $this->xml2array($xml);
		if($return_type == 'bool'){
		    //只记录代付参数
            new_logger('ght_pay.log','返回参数',$data_array);
            new_logger('ght_pay.log','body',$data_array['BODY']['RET_DETAILS']['RET_DETAIL']);
            new_logger('ght_pay.log','end','-----------------------------------------');
        }
		if($data_array['INFO']['RET_CODE'] != '0000'){
			$this->error = $data_array['INFO']['ERR_MSG'];
//			dd($this->error);
			return false;
		}else{
			if($return_type == 'bool'){
				return true;
			}else{
				return $data_array['BODY']['RET_DETAILS']['RET_DETAIL'];
			}
		}
	}
	
	protected function _designXml($xml){
		$xml2 = preg_replace('/<SIGNED_MSG>.*<\/SIGNED_MSG>/is','',$xml);
		preg_match('/<SIGNED_MSG>.*<\/SIGNED_MSG>/is',$xml,$match);
//		$sign_msg = substr($match[0],12,-13);
		$sign_msg = $this->_signXml($xml2);
//		dd($sign_msg);
		$sign_msg= hex2bin($sign_msg);
//		$len = strlen($sign_msg);
//		$sign_msg = pack("H" . $len, $sign_msg);
		$certs = array();
		openssl_pkcs12_read(file_get_contents($this->sign_cert), $certs,  $this->sign_cert_password);
		if(!$certs) return '';
		$ok = openssl_verify($xml2, $sign_msg, $certs['cert']); //openssl_verify验签成功返回1，失败0，错误返回-1
		return  $ok == 1 ? true : false;
		/*
		filePath为crt,cert文件路径。x.509证书
		cer to dem， Convert .cer to .pem, cURL uses .pem
		*/

//		$certificateCAcerContent = file_get_contents($this->design_cert);
//		$certificateCApemContent =  '-----BEGIN CERTIFICATE-----'.PHP_EOL
//			.chunk_split(base64_encode($certificateCAcerContent), 64, PHP_EOL)
//			.'-----END CERTIFICATE-----'.PHP_EOL;
//
//		$pubkeyid = openssl_get_publickey($certificateCApemContent);
//
//		$len = strlen($sign_msg);
//		$signature= pack("H" . $len, $sign_msg); //Php-16进制转换为2进制,看情况。有些接口不需要，有些需要base64解码
//
//		// state whether signature is okay or not
//		$ok = openssl_verify($xml2, $signature, $pubkeyid,'sha1WithRSAEncryption');
//		openssl_free_key($pubkeyid);
//		return $ok == 1 ? true : false;
	}
	
	// xml 转成数组
	protected function xml2array($xml){
		$xml_object = simplexml_load_string($xml);
		$xml_array = json_decode(json_encode($xml_object),true);
		return $xml_array;
	}
}