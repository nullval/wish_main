<?php

namespace App\Libraries\Df\esyto;
/*
 * 智创代付接口
 *@jenyee version 1.0
 */
class esyto
{
    private $merchant_id = '200440353110004';
    public $error;
    private $url = 'http://cp.esyto.com:9000/gateway/api/';
    private $key = '544C1EA9ED6315D867A573158C688CE1';

    public function __construct($test = true)
    {
        if ($test) {
            $this->merchant_id = '200541100000004';
            $this->url = 'http://cp.esyto.com:9001/gateway/api/';
            $this->key = '918A9EDBECCF2B74AAD63A29928E86A2';
        }
    }

    /*
     *算签
     * @param mixed  $post 待算签的参数
     * @jenyee 1.0
     * @return string
     */
    private function get_sign($post = [])
    {
        unset($post['sign']);
        $post = array_filter($post);
        ksort($post,SORT_NATURAL | SORT_FLAG_CASE);
        $param1 = [];
        foreach ($post as $k => $v) {
            $param1[] = $k . '=' .$v;
        }
        $url=implode('&', $param1) . '&key=' . $this->key;
        return strtoupper(md5($url));
    }

    /*
   *  代付请求
   * @param mixed  $param  请求参数
   * @jenyee 1.0
   * @return array
   */
    public function pay(array $param = [])
    {
        new_logger('esyto_pay', '请求', $param);
        $post = [
            'merchno' => $this->merchant_id, //平台商户号
            'businessnumber' => $param['order_sn'], //商户端的流水号，需保证在商户端不重复
            'tranType' => '01',
            'subject' => $param['subject'], //商品名称
            'transactionamount' => $param['amount'], //元为单位
            'bankcardnumber' => $param['acctNo'], //银行卡号码
            'bankcardname' => $param['accBankName'], //银行卡姓名
            'bankname' => $param['bankname'], //银行名称(必填)
            'bankcardtype' => '个人', //银行卡类型：个人、企业
            'bankaccounttype' => '储蓄卡', //收款银行
        ];
        $post['sign'] = $this->get_sign($post);
        $param = json_encode($post, JSON_UNESCAPED_UNICODE);
        $rs = curl_post($this->url . 'backTransReq', $param, ['Content-Type: application/json;']);
        $rs = json_decode($rs, true);
        new_logger('esyto_pay', '返回', $rs);
        if ($rs['respCode'] == '1') {
            return $rs;
        } else {
            $this->error = $rs['message'];
            return false;
        }
    }

    /**
     * 查询订单
     * @param $traceno 交易流水号
     * @jenyee 1.0
     * @return array
     *
     */
    public function query(string $traceno)
    {
        new_logger('esyto_query', '请求', $traceno);
        $post = [
            'merchno' => $this->merchant_id, //商户的商户号
            'businessnumber' => $traceno, //交易流水号
            'tranType'=>'03'
        ];
        $post['sign'] = $this->get_sign($post);
        $rs = curl_post($this->url.'backTransReq', json_encode($post), ['Content-Type: application/json; charset=utf-8']);
        $rs = json_decode($rs,true);
        new_logger('esyto_query', '返回', $rs);
        if ($rs['respCode'] == '1') {
            return $rs;
        } else {
            $this->error = $rs['message'];
            return false;
        }
    }

    /*
     * 查询代付提现余额
     *
     */
     public  function queryBalance(){
         $post = [
             'merchno' => $this->merchant_id, //商户的商户号
             'tranType'=>'04'//
         ];
         $post['sign'] = $this->get_sign($post);
         $rs = curl_post($this->url.'backTransReq', json_encode($post), ['Content-Type: application/json; charset=utf-8']);
         $rs = json_decode($rs,true);
         new_logger('esyto_query', '返回', $rs);
         if ($rs['respCode'] == '1') {
             return $rs;
         } else {
             $this->error = $rs['message'];
             return false;
         }
     }

}