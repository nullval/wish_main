<?php
/*
 *  Copyright (c) 2014 The CCP project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a Beijing Speedtong Information Technology Co.,Ltd license
 *  that can be found in the LICENSE file in the root of the web site.
 *
 *   http://www.yuntongxun.com
 *
 *  An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */
namespace App\Libraries\msg;

class REST {

	function __construct()
	{
        require_once 'ChuanglanSmsHelper/ChuanglanSmsApi.php';
	}


    public function send_msg($mobile,$msg,$sign='未特商城'){
        if(env('APP_ENV')!='online'){
            return arr_post(0,'禁止发送短信');
        }else{
            $clapi  = new \ChuanglanSmsApi();
            $result = $clapi->sendSMS($mobile,'【'.$sign.'】'.$msg);
            $result = $clapi->execResult($result);
//            dd($result);
//        var_dump($result);exit;
            if(isset($result[1]) && $result[1]==0){
                return arr_post(1,'发送成功');
            }else{
                logger('短信发送失败,手机号码为:'.$mobile.',参数:'.$result);
                return arr_post(0,'发送失败');
            }
        }
    }

    

}
?>
