<?php
namespace App\Libraries\msg;
use App\Jobs\SendMsg;
use App\Jobs\SendWechatPush;

/**
 * 微信推送sdk
 * Class Wechat
 * @package App\Libraries\Libs\msg
 */
class WechatPush {
	
	protected $appid = '';
	protected $secret = '';
	protected $url = '';
	protected $key = '';
	
	public function __construct(){
		$this->url = config('config.tdzd_sapi_url').'wechat/at';
		$this->key=env('api_code');
	}


	
	/**
	 * 同步执行推送
	 * @param $openid
	 * @param $template_id
	 * @param $param
	 * @return bool
	 */
	public function send($openid,$template_id,$param){
		$access_token = $this->getAccessToken();
		$param_format = [];
		foreach($param as $k => $v){
			$param_format[$k] = ['value'=>$v,'color'=>'#173177'];
		}
		$post = [
			'touser'		=> $openid,
			'template_id'	=> $template_id,
			'url'			=> '',
			'topcolor'		=> '#FF0000',		// card 顶部颜色标识
			'data'			=> $param_format,
		];
		$post_json = json_encode($post);
		$return = curl_post('https://api.weixin.qq.com/cgi-bin/message/template/send?access_token='.$access_token,$post_json);
		$return_array = json_decode($return,true);

		return $return_array['errcode'] === 0 ? true : false;
	}
	
	/**
	 * 异步指定推送消息
	 * @param $openid
	 * @param $template_id
	 * @param $param
	 * @return bool
	 */
	public function queue($openid,$template_id,$param){
		$job = (new SendWechatPush($openid,$template_id,$param));
		dispatch($job);
		return true;
	}
	
	private function getAccessToken(){
//		$sign = $this->_sign([]);
		$access_token = curl_post($this->url);
		return $access_token;
	}
	
	private function _sign($param){
		$json_param = json_encode($param);
		$sign = md5($json_param.$this->key);
		$post = [
			'Json'	=> $json_param,
			'Sign'	=> $sign,
		];
		return $post;
	}
}