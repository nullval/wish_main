<?php

namespace App\Libraries;

class JavaInterface
{
    private $is_test = true;
    private $base_url = 'http://39.106.204.21:8888/code/api/json/';//测试URL
    private $app_key = 'eIFoCYBgKgMSj7dArSSaSYPoeThKlHnI'; //签名密匙

    /**
     * @var array
     */
    private $common_key = [
        'version' => '1.0',
        'client' => '1'
    ];

    /**
     * JavaInterface constructor.
     * @param bool $is_test
     */
    public function __construct($is_test = true)
    {
        $this->is_test = $is_test;
    }

    /**
     * 发送请求
     *
     * @param $url
     * @param $post_data
     * @return mixed
     * @throws \Exception
     */
    private function _curl($url, $post_data)
    {
        $post_url = $this->base_url . $url;
        if ($this->is_test === true) {
            $result = json_decode($this->curl_post($post_url, $post_data), true);
        } else {
            $this->common_key['sign'] = $this->getSign($post_data);
            $result = json_decode($this->curl_post($post_url, $this->common_key), true);
        }
        return $result;
    }

    /**
     * curl 模拟 post 请求
     *
     * @param $url
     * @param array $post_data
     * @param bool $header
     * @return mixed
     * @throws \Exception
     */
    public function curl_post($url, $post_data = array())
    {
        $ch = curl_init(); //初始化curl
        curl_setopt($ch, CURLOPT_URL, $url);//设置链接
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//设置是否返回信息
        curl_setopt($ch, CURLOPT_POST, true);//设置为POST方式
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));//POST数据
        curl_setopt($ch, CURLOPT_TIMEOUT, 600);//超时时间
        $response = curl_exec($ch);//接收返回信息
        if (curl_errno($ch)) {    //出错则显示错误信息
            throw new \Exception(curl_error($ch));
        }
        curl_close($ch); //关闭curl链接
        return $response;
    }

    /**
     * 检查签名是否正确
     * @param  array $data 待检查的数据
     * @return boolean
     */
    protected function checkSign($data)
    {
        $sign = $data['sign'];
        unset($data['sign']);
        return $sign == $this->getSign($data);
    }

    /**
     * 算签
     *
     * @param $param array 参数体
     * @return mixed
     */
    protected function getSign(array $param)
    {
        $this->common_key = array_merge($this->common_key, $param);
        ksort($this->common_key);
        $str = '';
        foreach ($this->common_key as $k => $v) {
            $str .= $k . '=' . $v . "&";
        }
        $str = substr($str, 0, -1);
        $sign = strtolower(md5($str . $this->app_key));//将签名转化为小写
        return $sign;
    }

    /**
     * 批量生成二维码
     *
     * @param int $num 生码数量
     * @param int $indexOf 从哪个下标开始生产
     * @param string $url cmd
     * @param string $type 阿里云不同目录,正式和测试服要不同
     * @return bool
     * @throws \Exception
     */
    public function fabrication(int $num, int $indexOf, string $url, string $type = 'dev')
    {
        $URL = 'fabrication.do';
        $data = array(
            'cmd' => 'fabricationCode',
            'num' => $num,
            'indexOf' => $indexOf,
            'url' => $url,
//            'type' => $type
        );
        if ($type == 'online') {
            $data['type'] = 1; //正式服加type参数
        }

        $result = $this->_curl($URL, $data);
        new_logger('download.log', 'curl情况', $result);
        if (true == $result['success']) {
            return $result['data'];
        } else {
            throw new \Exception(json_encode($result));
        }
    }

}
