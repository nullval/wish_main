<?php

namespace App\Jobs;

use App\Models\Agent;
use App\Models\Bank;
use App\Models\Message;
use App\Models\Seller;
use App\Models\User;
use App\Models\UserWithdraw;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class Test implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $rs=DB::table('test')->insert(['created_at'=>time2date(),'value'=>33]);
        $Test=new \App\Models\Test();
        $Test->test();
    }

    /**
     * 处理失败任务
     *
     * @return void
     */
    public function failed()
    {
        $content='任务执行失败';
        DB::table('fail_redis_job')->insert(['content'=>$content,'created_at'=>time2date()]);
        // Called when the job is failing...
    }
}
