<?php

namespace App\Jobs;

use App\Models\Agent;
use App\Models\AgentOperate;
use App\Models\Api\ApiAgentOperate;
use App\Models\Api\ApiPushQrUser;
use App\Models\Bank;
use App\Models\Gift;
use App\Models\JavaIncrement;
use App\Models\MerchantApply;
use App\Models\Message;
use App\Models\Order;
use App\Models\Pos;
use App\Models\PosFreezePoint;
use App\Models\RuzhuMerchantBasic;
use App\Models\RuzhuMerchantBusinesss;
use App\Models\Seller;
use App\Models\TdzdShop;
use App\Models\User;
use App\Models\UserFreezeLog;
use App\Models\UserWithdraw;
use App\Models\VOrder;
use App\Models\Weachat;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class handleSellerFirstOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $order_sn;
    private $is_send;

    //红包id
    private $redpack_id;


    /**
     * Create a new job instance.
     * @param $order_sn 订单编号
     * @param $is_send 是否发送推送
     * @return void
     */
    public function __construct($order_sn,$is_send=1)
    {
        $this->order_sn=$order_sn;
        $this->is_send=$is_send;
    }

    /*
     * 处理商家订单 消费买单/订单支付 (通联)
     * $terminalId 终端号
     * $order_sn 订单编号
     * $amount 消费金额 (以元为单位)
     * $status 订单状态 状态  1处理中 2处理成功 3处理失败
     * $paytime 支付时间
     * $acct 交易卡号
     * $trxcode 交易类型
     * $order_type 交易类型 1订单支付 3消费买单 5线下扫码支付
     * $notify 通知类型 1通联 2高汇通
     */
    public function handle()
    {
        //只执行一次
//        $this->job->delete();
        set_time_limit(0);

        new_logger('job_handleSellerOrder.log','order',['order_sn'=>$this->order_sn]);
        $order_sn=$this->order_sn;
        $order_info=Order::where(['order_sn'=>$order_sn])->first();
        if($order_info->status==2){

            $rs=DB::transaction(function() use ($order_sn,$order_info){
                $Seller=new Seller();
                $User=new User();
                $terminalId=$order_info->terminalId;
                //实付金额
                $amount=$order_info->actual_amount;
                //店员手机号码
                $role_mobile=$order_info->role_mobile;
                if($order_info->type==5){
                    $order_remark='线下扫码支付';
                }else{
                    $order_remark='消费买单';
                }
                $pay_type=$order_info->pay_type;
                $pay_type_name=get_pay_type_name($pay_type);

                #消费买单/订单支付
                //通过pos机找到相应的商家id
                $pos_info=Pos::where(['terminalId'=>$terminalId])->first();
                //2018/01/28号如果pos机没找到商户就找订单里的商户
                $uid=empty($pos_info->uid)?$order_info->uid:$pos_info->uid;
                $seller_info=Seller::where(['id'=>$uid])->first();
                //2018/01/28号如果pos机没找到机主就找填写人
                $agent_uid=empty($pos_info->agent_uid)?$seller_info->inviter_id:$pos_info->agent_uid;


                //获取机主信息
                $agent_info=Agent::where(['id'=>$agent_uid])->first();


                //找到购买用户id
                $buyer_id=$order_info->buyer_id;
                if(empty($buyer_id)){
                    $buyer_id=User::firstOrCreate(['mobile'=>'11111111111'])->id;
                }

                //订单补贴
                $seller_amount=$order_info->seller_amount;
                $platform_amount=$order_info->platform_amount;

                //用于分配给用户系统钱包的金额
                $redpack_amount=$platform_amount*0.05;

                $Bank=new Bank();
                //支付成功
                //订单补贴
                //获取中央银行钱包
                $bank_purse_id=$Bank->get_bank_purse_id();
                //获取平台收益钱包
                $sys_purse_id=$Bank->get_sys_purse_id(1);
                //获取平台积分钱包
                $sys_point_purse_id=$Bank->get_sys_purse_id(3);
                //获取平台红包收益钱包
                $sys_redpack_purse_id=$Bank->get_sys_purse_id(12);
                //获取用户过账钱包
                $buyer_purse_id=$Bank->userWallet($buyer_id,10000,1,['purse_id'])->purse_id;

                //获取pos机购买者(机主)收益钱包
                $agent_purse_id=$Bank->userWallet($agent_uid,1,5,['purse_id'])->purse_id;
                //获取店家名称
                $merchant_info=RuzhuMerchantBasic::where(['uid'=>$seller_info->id])->first(['merchantName','lat','lng']);
                //获取店家名称
                $merchantName=$merchant_info->merchantName;
                //获取商家经纬度(用于发红包)
                $lat=empty($order_info->lat)?$merchant_info->lat:$order_info->lat;
                $lng=empty($order_info->lng)?$merchant_info->lng:$order_info->lng;
                //用户手机号
                $buyer_mobile=User::where(['id'=>$buyer_id])->value('mobile');
                //商家手机号
                $seller_mobile=$seller_info->mobile;
                //机主手机号
                $agent_mobile = Agent::where(['id' => $agent_uid])->value('mobile');

                //商家积分钱包
                $seller_point_purse_id=$Bank->userWallet($uid,3,2,['purse_id'])->purse_id;

                //已分出的额度
                $has_d_amount=0;

                if($platform_amount>0){
                    //1.中央银行入用户钱包
                    $Bank->doApiTransfer($bank_purse_id,$buyer_purse_id,$amount,10013,
                        $buyer_mobile.'在'.$merchantName.'使用'.$pay_type_name.$order_remark.$amount.'元,订单号:'.$order_sn,
                        '用户消费买单');

                    //2.用户钱包到平台收益钱包
                    $Bank->doApiTransfer($buyer_purse_id,$sys_purse_id,$amount,10052,
                        $buyer_mobile.'在'.$merchantName.'使用'.$pay_type_name.$order_remark.$amount.'元,平台获得收益,订单号:'.$order_sn,
                        '消费买单平台获得收益');
                    //3.商家补贴+给第三方手续费
                    $notify_type=$order_info->notify_type;
                    $profit_data=$this->give_seller_profit($notify_type,$uid,$sys_purse_id,$seller_amount,$buyer_mobile,$merchantName,$pay_type_name,$order_remark,$amount,$seller_mobile,$order_sn,$platform_amount,$bank_purse_id,$order_info->pay_method);

                    //累计商家让利和手续费率
                    $has_d_amount+=$seller_amount;
                    $has_d_amount+=$profit_data['poundage'];




                    //2017/12/16修改 费者从商家POS里面消费每单最高只能获得50积分封顶。自动买最低盘，无最低盘挂买盘的最高盘。
                    //5.赠送购买者积分 让利的5-30%
                    //给消费者的积分
                    if($order_info->type==1||$order_info->type==5){
                        $total_buyer_platform_amount=$User->get_platform_amount($buyer_id)+$platform_amount;
                        $buyer_b_config=config('seller.buyer_b_config');
                        foreach ($buyer_b_config as $k=>$v){
                            if($total_buyer_platform_amount>=$v['min_amount']&&$total_buyer_platform_amount<$v['max_amount']){
                                $min_point_radio=$v['min_point_radio']*10;
                                $max_point_radio=$v['max_point_radio']*10;
                                $buyer_radio=rand($min_point_radio,$max_point_radio)/1000;
                                break;
                            }
                        }
                        $user_point=get_last_two_num($platform_amount*($buyer_radio));
                        //订单支付/扫码支付才给积分
                        //获取用户积分钱包
                        $buyer_point_purse_id=$Bank->userWallet($buyer_id,3,1)->purse_id;
                        //5平台给用户积分
                        if($user_point>0){
                            $Bank->doApiTransfer($sys_point_purse_id,$buyer_point_purse_id,$user_point,10020,
                                $buyer_mobile.'在'.$merchantName.$order_remark.$amount.'元,获得积分,订单号:'.$order_sn,
                                '消费买单用户获得积分');
                            $has_d_amount+=$user_point;
                        }
                    }



                    //商家推荐人是机主
                    $operate_id=isset($pos_info->agent_operate_id)?$pos_info->agent_operate_id:$agent_info->operate_id;

                    //2018/01/04 若未特商城pos机未绑定机主 其12.5%给pos机所属运营中心
                    if(empty($agent_uid)){
                        $y_amount=get_last_two_num($platform_amount*(12.5/100));
                        if($y_amount>0){
                            $y_purse_id=$Bank->userWallet($operate_id,1,11)->purse_id;
                            $Bank->doApiTransfer($sys_purse_id,$y_purse_id,$y_amount,10069,
                                $buyer_mobile.'在'.$merchantName.$order_remark.$amount.'元,未特商城pos机所属运营中心'.$agent_mobile.'获得补贴,订单号:'.$order_sn,
                                '消费买单未特商城pos机所属获得补贴');
                            $has_d_amount+=$y_amount;
                        }
                    }else{
                        //6平台pos机购买者(机主)补贴 让利的10%
                        //给该pos机所属代理补贴
                        $agent_amount=get_last_two_num($platform_amount*(10/100));
                        if($agent_amount>0){
                            $Bank->doApiTransfer($sys_purse_id,$agent_purse_id,$agent_amount,10042,
                                $buyer_mobile.'在'.$merchantName.$order_remark.$amount.'元,机主'.$agent_mobile.'获得补贴,订单号:'.$order_sn,
                                '消费买单机主获得补贴');
                            $has_d_amount+=$agent_amount;
                        }

                        //7给机主推荐人补贴 让利的2.5%
                        //获取机主推荐人
                        //2017/12/13号修正 不给
                        $tuijian_agent_id=$agent_info->inviter_id;
                        if(!empty($tuijian_agent_id)){
                            $tuijian_amount=get_last_two_num($platform_amount*(2.5/100));
                            $tuijian_mobile=Agent::where(['id'=>$tuijian_agent_id])->first(['mobile'])->mobile;
                            $tuijian_purse_id=$Bank->userWallet($tuijian_agent_id,1,5)->purse_id;
                            if($tuijian_amount>0){
                                $rs[]=$arr=$Bank->doApiTransfer($sys_purse_id,$tuijian_purse_id,$tuijian_amount,10045,
                                    $buyer_mobile.'在'.$merchantName.$order_remark.$amount.'元,机主'.$tuijian_mobile.'获得二级分销补贴,订单号:'.$order_sn,
                                    '消费买单获得二级分销补贴');
                                $has_d_amount+=$tuijian_amount;
                            }

                        }
                    }

                    //8给机主运营中心分配佣金 一级运营中心2.5%    二级运营中心3%    三级运营中心3.5%
                    //查找机主所属运营中心
                    //若是pos机直接从pos机去三级,否则通过推荐人运营中心获取
                    $o_data=[];
                    if($order_info->type==1||$order_info->type==3){
                        //pos机支付
                        for($i=1;$i<=3;$i++){
                            $opera_id=$pos_info['operate_level'.$i];
                            if(!empty($opera_id)){
                                $opera_info=AgentOperate::where(['id'=>$opera_id])->first(['*']);
                                $o_data[$i]['id']=$opera_info['id'];
                                $o_data[$i]['mobile']=$opera_info['mobile'];
                                $o_data[$i]['level']=$opera_info['level'];
                                $o_data[$i]['radio']=$radio=config('agent.operate_amount')[$i]['radio'];
                                $o_data[$i]['amount']=get_last_two_num($platform_amount*($radio/100));
                            }
                        }
                    }elseif($order_info->type==5){
                        //扫码支付
                        $operate_id=$agent_info->operate_id;
                        $operate_data=AgentOperate::find_operate_level($operate_id);
                        foreach ($operate_data as $k=>$v){
                            $o_data[$k]['id']=$v['id'];
                            $o_data[$k]['mobile']=$v['mobile'];
                            $o_data[$k]['level']=$v['level'];
                            $o_data[$k]['radio']=$radio=config('agent.operate_amount')[$k]['radio'];
                            $o_data[$k]['amount']=get_last_two_num($platform_amount*($radio/100));
                        }
                    }

                    //分润
                    foreach ($o_data as $k=>$v){
                        $operate_amount=$o_data[$k]['amount'];
                        $operate_mobile=$o_data[$k]['mobile'];
                        if($operate_amount>0&&!empty($operate_mobile)){
                            $operate_purse_id=$Bank->userWallet($o_data[$k]['id'],1,11)->purse_id;
                            $Bank->doApiTransfer($sys_purse_id,$operate_purse_id,$operate_amount,10035,
                                $buyer_mobile.'在'.$merchantName.$order_remark.$amount.'元,运营中心'.$operate_mobile.'获得补贴,订单号:'.$order_sn,
                                '消费买单运营中心获得补贴');
                            $has_d_amount+=$operate_amount;
                        }
                    }

                    //查看运营中心(第一级)
//                    $first_o=current($o_data);
//                    $first_o_id=$first_o['id'];
//                    $first_o_level=$first_o['level'];
//                    $first_o_radio=$first_o['radio'];
//                    $operate_info=AgentOperate::where(['id'=>$first_o_id])->first();
//                    if(!empty($operate_info->parent_id)){
//                        $second_o_info=AgentOperate::where(['id'=>$operate_info->parent_id])->first();
//                        if($first_o_level==$second_o_info->level){
//                            //平级
//                            $second_o_radio=$first_o_radio*0.1;
//                        }else{
//                            //下级
//                            $second_o_radio=$first_o_radio*0.05;
//                        }
//                        $second_o_mobile=$second_o_info->mobile;
//                        $second_o_amount=get_last_two_num($platform_amount*($second_o_radio/100));
//                        $second_o_purse_id=$Bank->userWallet($second_o_info->id,1,11)->purse_id;
//                        $Bank->doApiTransfer($sys_purse_id,$second_o_purse_id,$second_o_amount,10116,
//                            $buyer_mobile.'在'.$merchantName.$order_remark.$amount.'元,运营中心获得推荐运营中心补贴,订单号:'.$order_sn,
//                            '消费买单运营中心获得推荐运营中心补贴');
//                        $has_d_amount+=$second_o_amount;
//                    }elseif(!empty($operate_info->inviter_user_id)){
//                        //下级
//                        $second_o_radio=$first_o_radio*0.05;
//                        $second_o_amount=get_last_two_num($platform_amount*($second_o_radio/100));
//                        $second_o_info=User::where(['id'=>$operate_info->inviter_user_id])->first();
//                        $second_o_mobile=$second_o_info->mobile;
//                        $second_o_purse_id=$Bank->userWallet($second_o_info->id,1,1)->purse_id;
//                        $Bank->doApiTransfer($sys_purse_id,$second_o_purse_id,$second_o_amount,10116,
//                            $buyer_mobile.'在'.$merchantName.$order_remark.$amount.'元,用户获得推荐运营中心补贴,订单号:'.$order_sn,
//                            '消费买单用户获得推荐运营中心补贴');
//                        $has_d_amount+=$second_o_amount;
//
//                    }


                    new_logger('job_handleSellerOrder.log','end',['------------']);

                    //10直接给商家补贴积分 20%-30%
                    //获取商家累计让利
                    $total_platform_amount=$Seller->get_platform_amount($uid)+$platform_amount;
                    $seller_b_config=config('seller.seller_b_config');
                    if($total_platform_amount<=10000){
                        $seller_radio=0.2;
                    }else{
                        foreach ($seller_b_config as $k=>$v){
                            if($total_platform_amount>=$v['min_amount']&&$total_platform_amount<$v['max_amount']){
                                $min_point_radio=$v['min_point_radio']*10;
                                $max_point_radio=$v['max_point_radio']*10;
                                $seller_radio=rand($min_point_radio,$max_point_radio)/1000;
                                break;
                            }
                        }
                    }
                    $seller_point_amount=$platform_amount*($seller_radio);
                    if($seller_point_amount>0){
                        $Bank->doApiTransfer($sys_point_purse_id,$seller_point_purse_id,$seller_point_amount,10114,
                            $buyer_mobile.'在'.$merchantName.$order_remark.$amount.'元,商家获得补贴积分,订单号:'.$order_sn,
                            '商家补贴获得补贴积分');
                        $has_d_amount+=$seller_point_amount;
                    }

                    //5%给到红包
//                    if($redpack_amount>0){
//                        $Bank->doApiTransfer($sys_purse_id,$sys_redpack_purse_id,$redpack_amount,10101,
//                            $buyer_mobile.'在'.$merchantName.$order_remark.$amount.'元,系统拨款发红包,订单号:'.$order_sn,
//                            '平台使用收益用于发红包');
//                        $has_d_amount+=$redpack_amount;
//                    }

                    //累计已让利金额
                    $left_amount=$amount-$has_d_amount;
                    //将剩余的金额沉淀
                    $sys_user_id=User::firstOrCreate([
                        'mobile'=>'13000000000'
                    ],[
                        'remark'=>'系统账号'
                    ])->id;
                    $sys_user_purse_id=$Bank->userWallet($sys_user_id,1,1,['purse_id'])->purse_id;
                    if($left_amount>0){
                        $Bank->doApiTransfer($sys_purse_id,$sys_user_purse_id,$left_amount,1000008,
                            $buyer_mobile.'消费,系统账号获得沉淀收益,订单号:'.$order_sn,
                            '用户消费系统账号获得收益');
                    }
                }



                return [
                    'platform_amount'=>$platform_amount,
                    'user_point'=>$user_point,
                    'buyer_mobile'=>$buyer_mobile,
                    'buyer_id'=>$buyer_id,
                    'redpack_amount'=>$redpack_amount,
                    'uid'=>$uid,
                    'lat'=>$lat,
                    'lng'=>$lng,
                    'merchantName'=>$merchantName,
                    'amount'=>$amount,
                    'order_remark'=>$order_remark,
                    'buyer_mobile'=>$buyer_mobile,
                    'seller_mobile'=>$seller_mobile,
                    'agent_amount'=>$agent_amount,
                    'agent_mobile'=>$agent_mobile,
                    'tuijian_amount'=>$tuijian_amount,
                    'tuijian_mobile'=>$tuijian_mobile,
//                    'operate_amount'=>$operate_amount,
//                    'operate_mobile'=>$operate_mobile,
                    'role_mobile'=>$role_mobile,
                    'seller_point_amount'=>$seller_point_amount,
                    'is_a_point'=>$seller_info->is_a_point,
                    'o_data'=>$o_data,
//                    'second_o_mobile'=>$second_o_mobile,
//                    'second_o_amount'=>$second_o_amount
                ];
            });

            //累计商家让利
            $Seller=new Seller();
            $Seller->add_platform_amount($rs['uid'],$rs['platform_amount']);
            //累计用户消费让利
            $User=new User();
            $User->add_platform_amount($rs['buyer_id'],$rs['platform_amount']);

            //无关业务
            //平台让利的5%用于发红包
//            $this->set_redpack($rs['redpack_amount'],$rs['uid'],$rs['lat'],$rs['lng'],$rs['merchantName']);
            //2017/12/16修改 费者从商家POS里面消费每单最高只能获得50积分封顶。自动买最低盘，无最低盘挂买盘的最高盘。
            if($order_info->type==1||($order_info->type==5&&substr(User::where(['id'=>$order_info->buyer_id])->value('mobile'),0,1) == 1)) {
                //强制挂两位小数的积分  java要求 保持精度
                $user_point=$rs['user_point'];
                if($user_point>0.001){
                    //自动挂单
                    $JavaIncrement = new JavaIncrement();
                    $JavaIncrement->automatic_guadan($rs['buyer_mobile'],get_num($user_point),$rs['buyer_id'],3);
                    //2018/04/11商家自动挂单

//                    if($rs['is_a_point']==1){
//                        $User=new User();
//                        $seller_mobile=$rs['seller_mobile'];
//                        $result=$User->get_user_id_by_mobile($seller_mobile,1);
//                        $seller_user_id=$result['data']['user_id'];
//                        $JavaIncrement->automatic_guadan($seller_mobile,get_num($rs['seller_point_amount']),$seller_user_id,4);
//                    }
                }
            }
            //短信通知
            $this->notify($rs);


        }

    }

    /**
     * 处理失败任务
     *
     * @return void
     */
    public function failed()
    {
        //失败则删除缓存里的红包记录 并修改订单记录
        $Gift=new Gift();
        $Gift->del_redis_gift($this->redpack_id);
//        Order::where(['order_sn'=>$this->order_sn])->update(['status'=>1]);
        $content='消费买单回调处理失败,订单编号:'.$this->order_sn;
        $Message=new Message();
        $Message->send_sms('13631272493',16,$content,'',[],0);
    }

    /**
     * @param $notify_type
     * @param $uid
     * @param $sys_purse_id
     * @param $seller_amount
     * @param $buyer_mobile
     * @param $merchantName
     * @param $pay_type_name
     * @param $order_remark
     * @param $amount
     * @param $seller_mobile
     * @param $order_sn
     * @param $platform_amount
     * @param $bank_purse_id
     * 商家分润+手续费
     */
    private function give_seller_profit($notify_type,$uid,$sys_purse_id,$seller_amount,$buyer_mobile,$merchantName,$pay_type_name,$order_remark,$amount,$seller_mobile,$order_sn,$platform_amount,$bank_purse_id,$pay_method){
        $Bank=new Bank();
        if(in_array($notify_type,[1,2,3,5])){
            $reason_arr=[
                //notify_type=>转账原因码
                1=>'10014', //通联
                2=>'10041', //高汇通
                3=>'10079', //越满
                5=>'10075', //环讯
            ];
            //获取商家钱包id
            $seller_purse_id=$Bank->userWallet($uid,4,2)->purse_id;
            //系统收益到商家收益钱包
            $Bank->doApiTransfer($sys_purse_id,$seller_purse_id,$seller_amount,$reason_arr[$notify_type],
                $buyer_mobile.'在'.$merchantName.'使用'.$pay_type_name.$order_remark.$amount.'元,商家'.$seller_mobile.'获得货款,订单号:'.$order_sn,
                '消费买单商家获得货款');

            //4.平台给第三方手续费
            $Order=new Order();
            $poundage=$Order->buckle_fee($notify_type,$amount,$buyer_mobile.'在'.$merchantName.'使用'.$pay_type_name.$order_remark.$amount.'元,平台扣除手续费,订单号:'.$order_sn,0,$pay_method);
        }elseif (in_array($notify_type,[6,7])){
            //银行卡支付
            $reason_arr=[
                6=>[ //key对应notify_type
                    0=>'魔方pos机支付',
                    1=>10081,
                    2=>10073,
                    3=>10074,
                    4=>10077,
                    5=>10083,
                    'owner_type'=>16
                ],
                7=>[
                    0=>'银盛pos机支付',
                    1=>10113,
                    2=>10114,
                    3=>10115,
                    4=>10111,
                    5=>10112,
                    'owner_type'=>17
                ]
            ];

            //若是银盛 (钱已回到商家银行卡上,做回收)
            //获取商家(过账钱包)
            $seller_purse_id=$Bank->userWallet($uid,10000,2)->purse_id;
            //扣除商家充值额度
            $seller_recharge_purse_id=$Bank->userWallet($uid,7,2)->purse_id;
            //魔方手续费
            $t_amount=get_num($amount*(config('pay.notify_type')[7]['poundage']/1000));
            $t_amount=empty($t_amount)?0.01:$t_amount;
            //手续费由平台出
            $Bank->doApiTransfer($seller_recharge_purse_id,$sys_purse_id,$platform_amount,$reason_arr[$notify_type][1],
                $buyer_mobile.'使用'.$reason_arr[$notify_type][0].',系统获得回收商家充值金额,订单号:'.$order_sn,
                '系统回收商家充值金额');
            $Bank->doApiTransfer($sys_purse_id,$seller_purse_id,$amount,$reason_arr[$notify_type][2],
                $buyer_mobile.'使用'.$reason_arr[$notify_type][0].',商家获得货款,订单号:'.$order_sn,
                '系统收益回收');
            $Bank->doApiTransfer($seller_purse_id,$bank_purse_id,$amount-$t_amount,$reason_arr[$notify_type][3],
                $buyer_mobile.'使用'.$reason_arr[$notify_type][0].',商家货款已结算,订单号:'.$order_sn,
                '商家货款结算');

            //4.商家给第三方手续费
            $d_purse_id=$Bank->get_d_purse(1,$reason_arr[$notify_type]['owner_type'])->purse_id;
            $Bank->doApiTransfer($seller_purse_id,$d_purse_id,$t_amount,$reason_arr[$notify_type][4],
                $buyer_mobile.'在'.$merchantName.'使用'.$pay_type_name.$order_remark.$amount.'元,第三方扣除手续费,订单号:'.$order_sn,
                '扣除手续费');
            //平台返还手续费
            $Bank->doApiTransfer($sys_purse_id,$seller_recharge_purse_id,$t_amount,$reason_arr[$notify_type][5],
                $buyer_mobile.'使用'.$reason_arr[$notify_type][0].',系统返还手续费,订单号:'.$order_sn,
                '系统返回手续费');
            $poundage=$t_amount;
        }
        //返回手续费率
        return [
            'poundage'=>$poundage
        ];
    }

    /**
     * 微信通知
     */
    private function notify($rs){
        if($this->is_send==1){
            $amount=$rs['amount'];
            $order_remark=$rs['order_remark'];
            $buyer_mobile=$rs['buyer_mobile'];
            $merchantName=$rs['merchantName'];
            $role_mobile=$rs['role_mobile'];
            $seller_mobile=$rs['seller_mobile'];
            $Weachat=new Weachat();
            $notify_date=date('Y-m-d H:i:s');
            //2017/12/21 加入微信推送通知 商家 /店员
            $amount=get_num($amount);
            $notify_wechat_arr=[
                '消费者消费',
                $notify_date,
                $order_remark.'订单',
                handle_mobile($buyer_mobile),
                '消费金额',
                $amount."\n订单编号:   ".$this->order_sn
            ];
            $notify_msg_remark='消费者于'.$notify_date.'在您的'.$merchantName.'有一笔'.$amount.'元的'.$order_remark.'。';
            if($amount>0){
                //通知店员
                if(!empty($role_mobile)){
                    $user_new_info=User::where(['mobile'=>$role_mobile])->first();
                    if(!isset($user_new_info->id)){
                        User::insert(['mobile'=>$role_mobile,'created_at'=>time2date()]);
                    }
                    $Weachat->push_buy_good_message($role_mobile,1,$notify_wechat_arr,$notify_msg_remark);
                }else{
                    $Weachat->push_buy_good_message($seller_mobile,2,$notify_wechat_arr,$notify_msg_remark);
                }
            }


            //2017/12/21 加入微信推送通知 直推人
            $agent_amount=get_num($rs['agent_amount']);
            $agent_mobile=$rs['agent_mobile'];
            if($agent_amount>0){
                $agent_amount=get_num($agent_amount);
                $notify_wechat_arr=[
                    '消费者消费获得补贴',
                    $notify_date,
                    $order_remark.'订单',
                    handle_mobile($buyer_mobile),
                    '获得补贴',
                    $agent_amount."\n订单编号:   ".$this->order_sn
                ];
                $notify_msg_remark='亲,恭喜您于'.$notify_date.'获得消费者在您推荐的商家-'.$merchantName.'的一笔'.$agent_amount.'元的'.$order_remark.'的补贴。';
                $Weachat->push_buy_good_message($agent_mobile,3,$notify_wechat_arr,$notify_msg_remark);
            }

            //2017/12/21 加入微信推送通知 间推人
            $tuijian_amount=get_num($rs['tuijian_amount']);
            $tuijian_mobile=$rs['tuijian_mobile'];
            if(!empty($tuijian_mobile)) {
                //2017/12/21 加入微信推送通知 间推人
                $tuijian_amount = get_num($tuijian_amount);
                $notify_wechat_arr = [
                    '消费者消费获得补贴',
                    $notify_date,
                    $order_remark.'订单',
                    handle_mobile($buyer_mobile),
                    '获得补贴',
                    $tuijian_amount."\n订单编号:   ".$this->order_sn
                ];
                $notify_msg_remark = '恭喜您于' . $notify_date . '获得一笔' . $tuijian_amount . '的消费者'.$order_remark.'订单的补贴。';
                if ($tuijian_amount > 0) {
                    $Weachat->push_buy_good_message($tuijian_mobile, 3, $notify_wechat_arr, $notify_msg_remark);
                }
            }


            $o_data=$rs['o_data'];
            //2018/04/28通知运营中心
            foreach ($o_data as  $k=>$v){
                $operate_amount = get_num($v['amount']);
                $operate_mobile=$v['mobile'];
                $notify_wechat_arr = [
                    '消费者消费获得补贴',
                    $notify_date,
                    $order_remark.'订单',
                    handle_mobile($buyer_mobile),
                    '获得补贴',
                    $operate_amount."\n订单编号:   ".$this->order_sn
                ];
                $notify_msg_remark = '亲,恭喜您的运营中心于' . $notify_date . '获得消费者在商家-'.$merchantName.'的一笔' . $operate_amount . '元的'.$order_remark.'的补贴。';
                if ($operate_amount > 0&&(substr($operate_mobile,0,1)!='-')) {
                    $Weachat->push_buy_good_message($operate_mobile, 3, $notify_wechat_arr, $notify_msg_remark);
                }
            }

            //2017/12/21 加入微信推送通知 运营中心
//            $operate_amount = get_num($operate_amount);
//            if(!empty($operate_mobile)){
//                $notify_wechat_arr = [
//                    '消费者消费获得补贴',
//                    $notify_date,
//                    $order_remark.'订单',
//                    handle_mobile($buyer_mobile),
//                    '获得补贴',
//                    $operate_amount."\n订单编号:   ".$this->order_sn
//                ];
//                $notify_msg_remark = '亲,恭喜您的运营中心于' . $notify_date . '获得消费者在商家-'.$merchantName.'的一笔' . $operate_amount . '元的'.$order_remark.'的补贴。';
//                if ($operate_amount > 0&&(substr($operate_mobile,0,1)!='-')) {
//                    $Weachat->push_buy_good_message($operate_mobile, 3, $notify_wechat_arr, $notify_msg_remark);
//                }
//            }

            //2018/04/04 加入微信推送通知 商家补贴通知
//            $seller_point_amount = get_num($rs['seller_point_amount']);
//            $seller_mobile=$rs['seller_mobile'];
//            $notify_wechat_arr = [
//                '商家获得补贴积分',
//                $notify_date,
//                $order_remark.'订单',
//                handle_mobile($buyer_mobile),
//                '商家获得补贴积分',
//                $seller_point_amount."\n订单编号:   ".$this->order_sn
//            ];
//            $notify_msg_remark = '消费者于'.$notify_date.'在您的'.$merchantName.'有一笔'.$amount.'元的'.$order_remark.',您获得'.$seller_point_amount.'积分补贴';
//            if ($seller_point_amount > 0) {
//                $Weachat->push_buy_good_message($seller_mobile, 2, $notify_wechat_arr, $notify_msg_remark);
//            }

            //2018/05/16
            $second_o_amount = get_num($rs['second_o_amount']);
            $second_o_mobile=$rs['second_o_mobile'];
            $notify_wechat_arr = [
                '消费者消费获得补贴',
                $notify_date,
                $order_remark.'订单',
                handle_mobile($buyer_mobile),
                '获得补贴',
                $second_o_amount."\n订单编号:   ".$this->order_sn
            ];
            $notify_msg_remark =  '亲,恭喜您于' . $notify_date . '获得消费者在商家-'.$merchantName.'的一笔推荐运营中心的' . $second_o_amount . '元的'.$order_remark.'的补贴。';
            if ($second_o_amount > 0) {
                $Weachat->push_buy_good_message($second_o_mobile, 1, $notify_wechat_arr, $notify_msg_remark);
            }

        }
    }

    /**
     * @param $redpack_amount 红包金额 单位元
     * @param $uid 商家id
     * @param $lat
     * @param $lng
     * 商家让利的5%作为红包
     */
    private function set_redpack($redpack_amount,$uid,$lat,$lng,$name){
        //保留2位 不然会出现进度问题
        $amount=get_num($redpack_amount)*100;
        if($amount>=50){
            $Gift=new Gift();
            $this->redpack_id=$Gift->create_gift($amount,$uid,$lat,$lng,$name);
        }

    }
}
