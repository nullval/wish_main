<?php

namespace App\Jobs;

use App\Models\Api\ApiTdGiveLog;
use App\Models\Api\ApiTdStatement;
use App\Models\Api\ApiTdUser;
use App\Models\Message;
use App\Models\Order;
use App\Models\VOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class handleBuyTdOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $order_sn;
    private $is_send;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order_sn,$is_send=1)
    {
        $this->order_sn=$order_sn;
        $this->is_send=$is_send;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        new_logger('job_handleBuyTdOrder.log','order_sn',['order_sn'=>$this->order_sn]);
        set_time_limit(0);
        $order_sn=$this->order_sn;
        //订单信息
        $order_info=VOrder::where(['order_sn'=>$order_sn])->first();
        //订单实付金额
        $amount=$order_info->actual_amount;
        //购买人
        $buyer_id=$order_info->buyer_id;
//        20元1TD
        $td=get_last_two_num($amount/20);
        //判断有木有生成用户 木有帮生成

        DB::beginTransaction();
        $rs[]=ApiTdUser::where(['id'=>$buyer_id])->increment('freeze_td',$td);
        //生成对账单
        $ApiTdStatement=new ApiTdStatement();
        $ApiTdStatement->create_new_statement($order_sn,$td,$buyer_id);
        DB::commit();
        new_logger('job_handleBuyTdOrder.log','res',$rs);
        new_logger('job_handleBuyTdOrder.log','end',['-------------']);
        $Message=new Message();
        $buyer_info=ApiTdUser::where(['id'=>$buyer_id])->first();
        $mobile=$buyer_info->mobile;
        $Message->send_sms($mobile,16,'亲，恭喜您获得'.get_num($td).'个CHQ，请前往zc.3dqxm.com查看','','',0);
        return true;
    }
    /**
     * 处理失败任务
     *
     * @return void
     */
    public function failed()
    {
        $content='刷pos机购买td订单失败,订单编号:'.$this->order_sn;
        $Message=new Message();
        $Message->send_sms('13631272493',16,$content,'',[],0);
        DB::table('fail_redis_job')->insert(['content'=>$content,'created_at'=>time2date()]);
        // Called when the job is failing...
    }
}
