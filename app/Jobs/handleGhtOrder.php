<?php

namespace App\Jobs;

use App\Models\Order;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class handleGhtOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $order_sn;
    protected $pay_type;
    protected $input_post;
    protected $amount;
    protected $status;
    protected $acct;
    protected $paytime;
    protected $trxcode;
    protected $notify_type;
    protected $seller_name;
    protected $radio;
    protected $trmno;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order_sn,$pay_type,$input_post,$amount,$status,$acct,$paytime,$trxcode,$notify_type=2,$seller_name='',$radio=0,$trmno='')
    {
        $this->order_sn=$order_sn;
        $this->pay_type=$pay_type;
        $this->input_post=$input_post;
        $this->amount=$amount;
        $this->status=$status;
        $this->acct=$acct;
        $this->paytime=$paytime;
        $this->trxcode=$trxcode;
        $this->notify_type=$notify_type;
        $this->seller_name=$seller_name;
        $this->radio=$radio;
        $this->trmno=$trmno;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order_sn=$this->order_sn;
        $pay_type=$this->pay_type;
        $input_post=$this->input_post;
        $amount=$this->amount;
        $status=$this->status;
        $acct=$this->acct;
        $paytime=$this->paytime;
        $trxcode=$this->trxcode;
        $notify_type=$this->notify_type;
        $seller_name=$this->seller_name;
        $radio=$this->radio;
        $trmno=$this->trmno;
        $order_info=Order::where(['order_sn'=>$order_sn])->first();
        //未处理
        if($order_info->status==1){
            //2017/12/31优化 先修改订单再分账,防止并行锁表
            $Order=new Order();
            $rs=$Order->update_pos_success_order($order_sn,$pay_type,$input_post,$amount,$status,$acct,$paytime,$trxcode,$notify_type,$seller_name,$radio,$trmno);
            if($rs){
                //处理订单分润
                $Order->handle_order_benefit($order_sn,$order_info->type);
            }
        }
    }

    /**
     * 处理失败任务
     *
     * @return void
     */
    public function failed()
    {

    }
}
