<?php

namespace App\Jobs;

use App\Models\UserPurse;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class changePoint implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $member_type;
    protected $option;
    protected $user_id;
    protected $amount;
    protected $type;
    protected $arr;
    protected $mobile;
    protected $give_type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($member_type,$option,$user_id,$amount,$type,$arr,$mobile,$give_type)
    {
        $this->member_type=$member_type;
        $this->option=$option;
        $this->user_id=$user_id;
        $this->amount=$amount;
        $this->type=$type;
        $this->mobile=$mobile;
        $this->arr=$arr;
        $this->give_type=$give_type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $member_type=$this->member_type;
        $option=$this->option;
        $user_id=$this->user_id;
        $amount=$this->amount;
        $type=$this->type;
        $mobile=$this->mobile;
        $arr=$this->arr;
        $give_type=$this->give_type;
        $UserPurse=new UserPurse();
        if($member_type==1||empty($member_type)){
            //用户
            $UserPurse->change_point(0,$option,$user_id,$amount,$type,$arr);
        }elseif($member_type==2){
            //商家积分兑换
            $UserPurse->change_seller_point(0,$option,$user_id,$amount,$type,$arr,$mobile,$give_type);
        }
    }

    /**
     * 处理失败任务
     *
     * @return void
     */
    public function failed()
    {
    }
}
