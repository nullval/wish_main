<?php

namespace App\Jobs;

use App\Libraries\msg\WechatPush;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendWechatPush implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $openid;
    protected $template_id;
    protected $param;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($openid,$template_id,$param)
    {
        //
		$this->openid = $openid;
		$this->template_id = $template_id;
		$this->param = $param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
		$msg = new WechatPush();
		$return = $msg->send($this->openid,$this->template_id,$this->param);
		if(empty($return)){
//			throw new ApiException('失败');
		}
    }
	
	
	/**
	 * Handle a job failure.
	 *
	 * @return void
	 */
	public function failed()
	{
		// Called when the job is failing...
		
	}
}
