<?php

namespace App\Jobs;

use App\Libraries\gaohuitong\Ruzhu;
use App\Models\Agent;
use App\Models\Bank;
use App\Models\Message;
use App\Models\RuzhuMerchantBank;
use App\Models\RuzhuMerchantBasic;
use App\Models\Seller;
use App\Models\User;
use App\Models\UserBank;
use App\Models\UserWithdraw;
use DeepCopy\f001\B;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class checkGhtEnterStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $merchantId;
    private $uid;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($merchantId,$uid)
    {
        $this->merchantId=$merchantId;
        $this->uid=$uid;
    }

    /**
     *查询高汇通商家审核状态 (已查询银行卡为准)
     */
    public function handle()
    {
        //只执行一次
//        $this->job->delete();
        $merchantId=$this->merchantId;
        $uid=$this->uid;
//        $Ruzhu=new Ruzhu();
//        $info=$Ruzhu->queryInfo($merchantId);
//        new_logger('notify_checkGhtEnterStatus.log','info',$info);
//        $bank_info_list=$Ruzhu->queryBanklist($merchantId);
//        new_logger('notify_checkGhtEnterStatus.log','bank_info_list',$bank_info_list);
//        //获取默认银行卡
//        $bank_authResult=0;
//        foreach ($bank_info_list['body'] as $k=>$v){
//
//            if($k=='bankaccounList'){
//                if(isset($v['defaultAcc'])){
//                    //一张银行卡
//                    if($v['defaultAcc']==1&&$v['authResult']==1){
//                        //用来绑定银行卡的
//                        $bank_authResult=1;
//                    }
//                }else{
//                    //两张银行卡
//                    foreach($v as $k0=>$v0){
//                        if($v0['defaultAcc']==1&&$v0['authResult']==1){
//                            //用来绑定银行卡的
//                            $bank_authResult=1;
//                        }
//                    }
//                }
//
//            }
//        }
//        new_logger('notify_checkGhtEnterStatus.log','respCode',['respCode1'=>$info['head']['respCode'],
//            'respCode2'=>$bank_info_list['head']['respCode'],
//            'body_authResult'=>$info['body']['authResult'],
//            'bank_authResult'=>$bank_authResult,
//        ]);
//        if($info['head']['respCode']=='000000'&&$bank_info_list['head']['respCode']=='000000'){
//            DB::transaction(function() use($info,$bank_authResult,$uid,$merchantId){
//                $UserBank=new UserBank();
//                if($info['body']['authResult']==1&&$bank_authResult==1){
//                    //入驻成功
//                    RuzhuMerchantBasic::where(['merchantId'=>$merchantId])->update(['status'=>3]);
//                    //入驻成功添加提现银行卡
//                    UserBank::where(['uid'=>$uid,'type'=>2])->delete();
//                    $rmb=RuzhuMerchantBank::where(['merchantId'=>$merchantId])->first();
//                    $UserBank->add_bank($uid,$rmb->name,$rmb->bankName,$rmb->bankaccountNo,$rmb->bankbranchNo,1,2);
//                }elseif($info['body']['authResult']==2||$bank_authResult==2){
//                    //入驻失败
//                    RuzhuMerchantBasic::where(['merchantId'=>$merchantId])->update(['status'=>4]);
//                    //入驻失败修改提现银行卡默认状态
//                    UserBank::where(['uid'=>$uid,'type'=>2])->update(['isdefault'=>0]);
//                }
//            });
//
//        }

        DB::transaction(function() use($merchantId,$uid){
            //默认审核通过
            $UserBank=new UserBank();
            //入驻成功
            RuzhuMerchantBasic::where(['merchantId'=>$merchantId])->update(['status'=>3]);
            //入驻成功添加提现银行卡
            UserBank::where(['uid'=>$uid,'type'=>2])->delete();
            $rmb=RuzhuMerchantBank::where(['merchantId'=>$merchantId])->first();
            $rs=$UserBank->add_bank($uid,$rmb->name,$rmb->bankName,$rmb->bankaccountNo,$rmb->bankbranchNo,1,2,0,$rmb->certNo,$merchantId);
        });

    }
}
