<?php

namespace App\Jobs;

use App\Models\Api\ApiTdGiveLog;
use App\Models\Api\ApiTdStatement;
use App\Models\Api\ApiTdUser;
use App\Models\Bank;
use App\Models\Message;
use App\Models\Order;
use App\Models\VOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class handleSellerRecharge implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $order_sn;
    private $is_send;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order_sn,$is_send=1)
    {
        $this->order_sn=$order_sn;
        $this->is_send=$is_send;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        new_logger('job_handleSellerRecharge.log','order_sn',['order_sn'=>$this->order_sn]);
        set_time_limit(0);
        $order_sn=$this->order_sn;
        //订单信息
        $order_info=Order::where(['order_sn'=>$order_sn])->first();
        //订单实付金额
        $amount=$order_info->actual_amount;
        //购买人 (商家)
        $uid=$order_info->uid;

        //判断有木有生成用户 木有帮生成

//        DB::beginTransaction();

        //转账
        DB::transaction(function() use ($order_sn,$order_info,$uid,$amount){
            $Bank=new Bank();
            //获取中央银行钱包
            $bank_purse_id=$Bank->get_bank_purse_id();
            $buyer_purse_id=$Bank->userWallet($uid,7,2)->purse_id;
            $Bank->doApiTransfer($bank_purse_id,$buyer_purse_id,$amount,10080,
                '商家充值,订单号:'.$order_sn,
                '商家充值');
            //2018/05/22扣除商家手续费 不扣平台
            //2018/06/30扣除平台手续费
            $Order=new Order();
            $Order->buckle_fee($order_info->notify_type,$amount,'商家充值了'.$amount.'元,扣除平台手续费,订单号:'.$order_sn,0,$order_info->pay_method);

        });

//        DB::commit();
        new_logger('job_handleSellerRecharge.log','end',['-------------']);
       return true;
    }
    /**
     * 处理失败任务
     *
     * @return void
     */
    public function failed()
    {
        Order::where(['order_sn'=>$this->order_sn])->update(['status'=>1]);
        $content='刷pos机购买td订单失败,订单编号:'.$this->order_sn;
        $Message=new Message();
        $Message->send_sms('13631272493',16,$content,'',[],0);
        DB::table('fail_redis_job')->insert(['content'=>$content,'created_at'=>time2date()]);
        // Called when the job is failing...
    }
}
