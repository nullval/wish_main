<?php

namespace App\Jobs;

use App\Models\UserWithdraw;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class Withdraw implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $uid;
    private $user_type;
    private $pursetype;
    private $amount;
    /**
     * Create a new job instance.
     *
     * @return void
     * $type 1直推奖提现 2感恩奖提现
     */
    public function __construct($uid,$user_type,$pursetype,$amount,$type)
    {
        $arr['uid']=$this->uid=$uid;
        $arr['user_type']=$this->user_type=$user_type;
        $arr['pursetype']=$this->pursetype=$pursetype;
        $arr['amount']=$this->amount=$amount;
        $arr['type']=$type;
        new_logger('job_Withdraw.log','即时到账申请参数',$arr);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    /**
     * @param $uid 用户id
     * @param $user_type 1用户 2商家 3机主
     * @param $pursetype 钱包类型
     * @param $amount 提现金额
     * 队列提现
     */
    public function handle()
    {
        //只执行一次
//        $this->job->delete();
        //排除机主是1的
        if($this->uid!=1){
            $UserWithdraw=new UserWithdraw();
            $result=$UserWithdraw->sub_withdraw($this->uid,$this->user_type,$this->pursetype,$this->amount,2);
            new_logger('job_Withdraw.log','即时到账申请返回参数',$result);
//            var_dump($result);
        }
    }
}
