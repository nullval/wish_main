<?php

namespace App\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class notify implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    #第三方通知队列
    private $url;
    private $param;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url,$param)
    {
        $this->url=$url;
        $this->param=$param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

    	$log['url']=$this->url;
		$log['param']=$this->param;
    	new_logger('notify.log','通知参数',$log);
        @curl_post($this->url,$this->param);
    }

    /**
     * 处理失败任务
     *
     * @return void
     */
    public function failed()
    {
        $content='任务执行失败';
        DB::table('fail_redis_job')->insert(['content'=>$content,'created_at'=>time2date()]);
        // Called when the job is failing...
    }
}
