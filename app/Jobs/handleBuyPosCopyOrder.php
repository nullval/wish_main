<?php

namespace App\Jobs;

use App\Models\Agent;
use App\Models\AgentOperate;
use App\Models\Bank;
use App\Models\Message;
use App\Models\Order;
use App\Models\Pos;
use App\Models\User;
use App\Models\UserFreezeLog;
use App\Models\UserPurse;
use App\Models\VOrder;
use App\Models\Weachat;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;


class handleBuyPosCopyOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $order_sn;
    private $is_send;


    /**
     * Create a new job instance.
     * @param $order_sn 订单编号
     * @param $is_send 是否发送推送
     * @return void
     */
    public function __construct($order_sn,$is_send=1)
    {
        $this->order_sn=$order_sn;
        $this->is_send=$is_send;
    }





    /*
    * 处理购买pos机订单 (通联)
    * $terminalId 终端号
    * $order_sn 订单编号
    * $amount 消费金额 (以元为单位)
    * $status 订单状态 状态  1处理中 2处理成功 3处理失败
    * $paytime 支付时间
    * $acct 交易卡号
    * $trxcode 交易类型
    */
    public function handle()
    {
        //只执行一次
//        $this->job->delete();
        set_time_limit(0);

        new_logger('job_handleBuyPosOrder.log','order_sn',['order_sn'=>$this->order_sn]);
        #购买pos机
        //判断此用户是否有推荐人，没有根据pos机信息绑定推荐人
        //订单信息
        $order_info=VOrder::where(['order_sn'=>$this->order_sn])->first();
        //实付金额
        $amount=config('pos.pos.amount');
        $terminalId=$order_info->terminalId;
        $buyer_id=$order_info->buyer_id;
        $order_sn=$this->order_sn;
        //用户手机号
        $buyer_mobile=Agent::where(['id'=>$buyer_id])->first(['mobile'])->mobile;
//        $is_new=$order_info->is_new;
        //通过pos机找到相应的归属机主uid
        $pos_info=Pos::where(['terminalId'=>$terminalId])->first();

        if($order_info->status==2) {
            //支付成功
            //判断是否有效订单
            if($order_info['is_true']==1){
                DB::transaction(function() use ($order_info,$amount,$terminalId,$buyer_id,$order_sn,$buyer_mobile,$pos_info){
                    $Bank=new Bank();
                    //真实订单
                    //1.无推荐人绑定推荐关系 (推荐人不一定是未特商城pos机本人)
                    $order_agent_uid=Agent::where(['mobile'=>$order_info->invite_mobile])->value('id');

                    //机主绑定推荐人及运营中心
                    $Agent=new Agent();
                    $Agent->bind_inviter($buyer_id,$order_agent_uid);

                    //2.补贴,绑定pos机
                    //[1]根据金额判断其能绑定多少台pos机
                    $actual_pos_num=1;


                    //绑定的pos机数量

                    //获取中央银行钱包
                    $bank_purse_id=$Bank->get_bank_purse()->purse_id;
                    //获取平台收益钱包
                    $sys_purse_id=$Bank->get_sys_purse(1)->purse_id;
                    //获取用户钱包(也是机主)
                    $buyer_purse_id=$Bank->userWallet($buyer_id,10000,5)->purse_id;
                    //获取推荐人信息
                    $invite_agent_info=Agent::where(['id'=>$order_agent_uid])->first();
                    $invite_agent_mobile=$invite_agent_info->mobile;


                    //0.pos机成本价
                    $cost_price=$pos_info->cost_price;

                    //1.中央银行入用户钱包
                    $Bank->doApiTransfer($bank_purse_id,$buyer_purse_id,$amount,10016,
                        $buyer_mobile.'购买未特商城pos机消费了'.$amount.'元,订单号:'.$order_sn,
                        '用户购买未特商城pos机');


                    //2.钱直接到平台收益
                    $Bank->doApiTransfer($buyer_purse_id,$sys_purse_id,$amount,10017,
                        $buyer_mobile.'购买未特商城pos机消费了'.$amount.'元,平台获得收益'.$amount.',订单号:'.$order_sn,
                        '购买未特商城pos机获得收益');

                    //4.消费买单收取的手续费(第三方)
                    $Order=new Order();
                    $Order->buckle_fee($order_info->notify_type,$amount,$buyer_mobile.'购买未特商城pos机消费了'.$amount.'元,平台扣除手续费,订单号:'.$order_sn,$order_info->pay_method);


                    //5.计算pos机成本
                    //pos机成本价(合计)
                    $cost_price=$cost_price*$actual_pos_num;
                    if(!empty($cost_price)){
                        //平台支付pos机成本价(出中央银行)
                        $Bank->doApiTransfer($sys_purse_id,$bank_purse_id,$cost_price,10019,
                            $buyer_mobile.'购买未特商城pos机消费了'.$amount.'元,平台扣除未特商城pos机成本价'.$cost_price.',订单号:'.$order_sn,
                            '扣除未特商城pos机成本价');
                    }

                    //2018/03/20给直推1000收益
                    //6 直推人
                    $first_part_point=1000*$actual_pos_num;
                    //消费机主推荐人用户身份id
                    $buyer_inviter_purse_id=$Bank->userWallet($order_agent_uid,1,5,['purse_id'])->purse_id;
                    $Bank->doApiTransfer($sys_purse_id,$buyer_inviter_purse_id,$first_part_point,10094,
                        $buyer_mobile.'购买未特商城pos机消费了'.$amount.'元,推荐人'.$invite_agent_mobile.'获得收益'.$first_part_point.',订单号:'.$order_sn,
                        '购买未特商城pos机推荐人获得收益');

                    //解冻推荐人的出彩奖和推荐人的推荐人的分享奖
                    $UserFreezeLog=new UserFreezeLog();
                    $UserFreezeLog->unfreeze_exciting_agent_point($order_agent_uid);

                    //入队
                    dispatch(new SevenToHalf($buyer_id,$order_agent_uid));



                    if($this->is_send==1){
                        //是否发送推送
                        $Weachat=new Weachat();
                        $notify_date=date('Y-m-d H:i:s');


                        //2018/3/20 加入微信推送通知 直推人
                        $first_part_point=get_num($first_part_point);
                        $notify_wechat_arr=[
                            '机主购买未特商城pos机获得收益',
                            $notify_date,
                            '购买未特商城pos机订单',
                            handle_mobile($buyer_mobile),
                            '获得收益',
                            $first_part_point
                        ];
                        $notify_msg_remark='亲,恭喜您于'.$notify_date.'获得直接分享的机主'.$buyer_mobile.'购买未特商城pos机,平台赠送您'.$first_part_point.'收益。';
                        if($first_part_point>0) {
                            $Weachat->push_buy_good_message($invite_agent_mobile, 3, $notify_wechat_arr, $notify_msg_remark);
                        }

                    }


                });


                if(config('app.env')=='online'){
                    //通知管理员入账
                    if($amount>1000){
                        //短信通知管理员购买成功
                        $notify_arr[0]=$buyer_mobile;
                        $notify_arr[1]=$amount;
                        $notify_arr[2]=date('Y-m-d H:i');
                        $notify_arr[3]=$order_sn;
                        $Message=new Message();
                        $Message->notify_buy_pos_success($notify_arr);
                    }
                }

            }
        }
    }

    /**
     * 处理失败任务
     *
     * @return void
     */
    public function failed()
    {
//        VOrder::where(['order_sn'=>$this->order_sn])->update(['status'=>1]);
        $content='购买未特商城pos机回调处理失败,订单编号:'.$this->order_sn;
        DB::table('fail_redis_job')->insert(['content'=>$content,'created_at'=>time2date()]);
        $Message=new Message();
        $Message->send_sms('13631272493',16,$content,'',[],0);
        // Called when the job is failing...
    }
}
