<?php

namespace App\Jobs;

use App\Libraries\Df\esyto\esyto;
use App\Models\Agent;
use App\Models\AgentOperate;
use App\Models\Bank;
use App\Models\MerchantApply;
use App\Models\Message;
use App\Models\PushQrUser;
use App\Models\Seller;
use App\Models\User;
use App\Models\UserWithdraw;
use DeepCopy\f001\B;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class checkPay implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $order_sn;
    private $year;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order_sn,$year)
    {
        $this->order_sn=$order_sn;
        $this->year=$year;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //只执行一次
//        $this->job->delete();
        $user_withdraw_table='user_withdraw_'.$this->year;
        //
        $withdraw_info=DB::table($user_withdraw_table)->where(['spare_order_sn'=>$this->order_sn])->first();
        $arr['spare_order_sn']=$this->order_sn;
        $arr['_cmd']='pos_notifyOrderStatus';
        $arr['username']=env('MERCHANT');
        $arr['password']=env('CRYPTOGRAM');
        $arr['code']=env('DF_CODE');
        $arr['timestamp']=time();
        $code=env('DF_CODE');
        $sign=get_df_pos_sign($arr,$code);
        $arr['sign']=$sign;
        $result=curl_post(env('DF_DOMAIN'),$arr);
        $result=json_decode($result,true);

        new_logger('job_successWithdraw.log','订单编号',['spare_order_sn'=>$this->order_sn]);
        new_logger('job_successWithdraw.log','第三方返回参数',['result'=>$result]);
        if($withdraw_info->status==1){
//        var_dump($result);exit;

            //2018/01/14 根据军哥要求强制成功 如果要走正常流程 请注释下面一行，并把失败的业务反注释
            $result['data']['status']=2;
            $result['data']['remark']='交易成功';

            if ($result['data']['status']==1){
                //处理中不动
                return true;
            }elseif ($result['data']['status']==3){

            }elseif ($result['data']['status']==2){
                $arr1['status']=2;
                $arr1['remark']=$result['data']['remark'];
                $arr1['updated_at']=time2date();
                //提现金额
                $value=$withdraw_info->amount;
                $member_type=$withdraw_info->member_type;
                $UserWithdraw=new UserWithdraw();
                $table=$UserWithdraw->t_arr[$member_type]['table'];
                $username=DB::table($table)->where(['id'=>$withdraw_info->user_id])->value('mobile');
                $name=$UserWithdraw->t_arr[$member_type]['name'];
                $owner_type=$UserWithdraw->t_arr[$member_type]['owner_type'];
                DB::transaction(function() use($withdraw_info,$owner_type,$user_withdraw_table,$username,$arr1,$value){
                    $Bank=new Bank();
                    //提现成功 解冻,扣钱,修改状态和备注信息
                    $Bank->unApiFreeze($withdraw_info->his_id,$this->year);
                    //用户钱包
                    $user_purse_id=$Bank->userWallet($withdraw_info->user_id,$withdraw_info->purse_type,$owner_type)->purse_id;
                    //第三方钱包
                    $d_purse_id=$Bank->get_d_purse(1)->purse_id;
                    //中央银行
                    $bank_purse_id=$Bank->get_bank_purse_id();
                    if($withdraw_info->d_poundage>0){
                        //1.2块钱给第三方的手续费,流入第三方的钱包
                        $Bank->doApiTransfer($user_purse_id,$d_purse_id,$withdraw_info->d_poundage,10005,
                            $username.'提现了'.$value.'元,第三方代付系统获得手续费'.$withdraw_info->d_poundage.',提现申请编号:'.$withdraw_info->id,
                            '提现扣除第三方手续费');
                    }
                    if($withdraw_info->actual_amount>0){
                        //2.用户实收金额流入中央银行
                        $Bank->doApiTransfer($user_purse_id,$bank_purse_id,$withdraw_info->actual_amount,10004,
                            $username.'提现了'.$value.'元,获得金额'.$withdraw_info->actual_amount.',提现申请编号:'.$withdraw_info->id,
                            '提现获得金额');
                    }
                    DB::table($user_withdraw_table)->where(['spare_order_sn'=>$withdraw_info->spare_order_sn])->update($arr1);
                });
                //2018/03/13忽略貨款提現通知
                if(env('APP_DEBUG')==false&&$withdraw_info->purse_type!=4){
                    new_logger('job_successWithdraw.log','start',['time'=>date('Y-m-d H:i:s')]);
                    $Bank=new Bank();
                    $user_amount=$Bank->use_money($withdraw_info->user_id,1,$owner_type);
                    //短信通知管理员提现成功
                    $notify_arr[0]=$name.$username;
                    $notify_arr[1]=get_num($value);
                    $notify_arr[2]=$user_amount;
                    $notify_arr[3]=$withdraw_info->actual_amount;
                    $notify_arr[4]=$withdraw_info->created_at;
                    if($withdraw_info=='huanxun_2'){
                       $df_amount=(new esyto(false))->queryBalance()['availAmt'];
                    }
                    $notify_arr[5]=$df_amount;
                    $Message=new Message();
                    $Message->notify_withdraw_success($notify_arr);
                    new_logger('job_successWithdraw.log','提现成功参数',['message'=>$notify_arr]);
                }

            }
        }

    }

    /**
     * 处理失败任务
     *
     * @return void
     */
    public function failed()
    {
        $content='用户提现回调处理失败,提现编号:'.$this->order_sn;
        DB::table('fail_redis_job')->insert(['content'=>$content,'created_at'=>time2date()]);
        $Message=new Message();
        $Message->send_sms('13631272493',16,$content,'',[],0);
        // Called when the job is failing...
    }
}
