<?php

namespace App\Jobs;

use App\Libraries\JavaInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class handleDownloadQr implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $num;
    private $indexOf;
    private $sid;
    private $order_id;

    /**
     * Create a new job instance.
     *
     * @param $sid
     * @param $num
     * @param $indexOf
     * @param $order_id
     */
    public function __construct($num, $indexOf)
    {
        set_time_limit(300);
        $this->num = $num;
        $this->indexOf = $indexOf;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if (env('APP_ENV') == 'online') {
            //正式服
            $url = 'http://master.hychqt.com/scan_code?s=';
        } else {
            //测试服
            $url = 'http://cs-master.hychqt.com/scan_code?s=';
        }
        new_logger('download.log', '开始执行下载任务');
        $record_id = DB::table('promotional_code_record')->insertGetId([
            'code_count' => $this->num,
            'agent_id' => 0,//
            'order_id' => 1,
            'add_time' => date('Y-m-d H:i:s'),
        ]);
        try {
            $res = (new JavaInterface())->fabrication($this->num, $this->indexOf, $url, env('APP_ENV'));
            new_logger('download.log', '下载情况', $res);
        } catch (\Exception $exception) {
            new_logger('download.log', '下载错误', [$exception->getMessage()]);
            throw new \Exception($exception->getMessage());
        }
        foreach ($res['extensioncode'] as $v) {
            //记录到详情表
            DB::table('extension_code')->insert([
                'agent_id' => 0,//下发到总公司
                'code_content' => $v['codeContent'],
                'code_number' => $v['codeNumber'],
                'code_url' => $v['codeUrl'],
                'physics_Path' => $v['codeUrl'],
                'add_time' => date('Y-m-d H:i:s'),
                'record_id' => 1,// 记录订单ID
                'state' => 1,
            ]);
        }
        //更新到压缩表
        DB::table('promotional_code_record')->where('id', $record_id)->update([
            'compressed_packet_path' => $res['zipPath']//压缩包路径
        ]);

    }

    /**
     * 处理失败任务
     *
     * @return void
     */
    public function failed()
    {
        // Called when the job is failing...
        $content = '下载二维码任务执行失败';
        new_logger('download.log', $content);
        DB::table('fail_redis_job')->insert(['content' => $content, 'created_at' => time2date()]);
    }
}
