<?php

namespace App\Jobs;

use App\Libraries\gaohuitong\Ruzhu;
use App\Models\Agent;
use App\Models\Bank;
use App\Models\Message;
use App\Models\Seller;
use App\Models\User;
use App\Models\UserWithdraw;
use DeepCopy\f001\B;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class checkGhtPay implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $order_sn;
    private $year;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order_sn,$year)
    {
        $this->order_sn=$order_sn;
        $this->year=$year;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //只执行一次
//        $this->job->delete();
        $user_withdraw_table='user_withdraw_'.$this->year;
        //
        $withdraw_info=DB::table($user_withdraw_table)->where(['spare_order_sn'=>$this->order_sn])->first();

        if($withdraw_info->status==1){
//            DB::beginTransaction();
//        var_dump($result);exit;
            $Ruzhu=new Ruzhu();
            //获取商户子商户号
            $child_merchant_id=Seller::where(['id'=>$withdraw_info->user_id])->first(['child_merchant_no'])->child_merchant_no;
            $query_rs=$Ruzhu->payQuery($child_merchant_id,$this->order_sn);
            new_logger('job_checkGhtPay.log','订单号',['order_sn'=>$this->order_sn]);
            new_logger('job_checkGhtPay.log','高汇通商家货款提现订单查询',$query_rs);
            if($query_rs['head']['respType']=='R'){
                //处理中不动
                $status=1;
            }elseif($query_rs['head']['respType']=='S' && $query_rs['head']['respCode']=='000000'){
                //成功
                $status=2;
            }else{
                //失败
                $status=3;
            }
            $remark=$query_rs['head']['respMsg'];

            if ($status==1){
                //处理中不动
                return true;
            }elseif ($status==3){
                //提现失败 解冻,修改状态和备注信息
//                $rs[]=$Bank->unFreeze($withdraw_info->his_id,$this->year);
                $arr1['status']=3;
                $arr1['remark']=$remark;
                $rs[]=DB::table($user_withdraw_table)->where(['spare_order_sn'=>$withdraw_info->spare_order_sn])->update($arr1);

            }elseif ($status==2){
                //提现成功 解冻,扣钱,修改状态和备注信息
//                $rs[]=$Bank->unFreeze($withdraw_info->his_id,$this->year);
                $arr1['status']=2;
                $arr1['remark']=$remark;
                //用户名
                if($withdraw_info->member_type==1){
                    $username=User::where(['id'=>$withdraw_info->user_id])->first()->mobile;
                    $owner_type=1;
                }elseif($withdraw_info->member_type==2){
                    $username=Seller::where(['id'=>$withdraw_info->user_id])->first()->mobile;
                    $owner_type=2;
                }elseif ($withdraw_info->member_type==3){
                    $username=Agent::where(['id'=>$withdraw_info->user_id])->first()->mobile;
                    $owner_type=5;
                }elseif ($withdraw_info->member_type==4){
                    $username='系统';
                    $owner_type=3;
                }

                $rs=DB::table($user_withdraw_table)->where(['spare_order_sn'=>$withdraw_info->spare_order_sn])->update($arr1);
                if($rs){
                    DB::transaction(function($withdraw_info,$owner_type,$username){
                        $Bank=new Bank();
                        //用户钱包 (货款钱包)
                        $user_purse_id=$Bank->userWallet($withdraw_info->user_id,4,$owner_type)->purse_id;
                        //高汇通代付收益钱包
                        $d_purse_id=$Bank->get_d_purse(1,8)->purse_id;
                        //中央银行
                        $bank_purse_id=$Bank->get_bank_purse_id();
                        //收益钱包
                        $sys_purse_id=$Bank->get_sys_purse_id(1);
                        //提现金额
                        $value=$withdraw_info->amount;
                        //1.1.5块钱给高汇通的手续费,流入高汇通的钱包
                        $rs[]=$Bank->doApiTransfer($user_purse_id,$d_purse_id,1.3,10059,
                            $username.'提现了'.$value.'元,高汇通代付系统获得手续费,提现申请编号:'.$withdraw_info->id,
                            '提现扣除第三方手续费');
                        //1.1.5块钱给平台的手续费,流入平台的钱包
                        $rs[]=$Bank->doApiTransfer($user_purse_id,$sys_purse_id,1.7,10060,
                            $username.'提现了'.$value.'元,平台获得手续费,提现申请编号:'.$withdraw_info->id,
                            '提现扣除平台手续费');
                        //3.用户实收金额流入中央银行
                        $rs[]=$Bank->doApiTransfer($user_purse_id,$bank_purse_id,$withdraw_info->actual_amount,10061,
                            $username.'提现了'.$value.'元,获得金额'.$withdraw_info->actual_amount.',提现申请编号:'.$withdraw_info->id,
                            '提现获得金额');
                    });
                }
            }
        }
    }

    /**
     * 处理失败任务
     *
     * @return void
     */
    public function failed()
    {
        $content='商家货款提现回调处理失败,提现编号:'.$this->order_sn;
        DB::table('fail_redis_job')->insert(['content'=>$content,'created_at'=>time2date()]);
        $Message=new Message();
        $Message->send_sms('13631272493',16,$content,'',[],0);
        // Called when the job is failing...
    }

}
