<?php

namespace App\Jobs;

use App\Models\Message;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mobile;
    protected $type;
    protected $remark;
    protected $spare1;
    protected $arr;
    protected $is_limit;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mobile,$type=1,$remark='',$spare1='',$arr=[],$is_limit=1)
    {
        //
		$this->mobile = $mobile;
		$this->type = $type;
		$this->remark = $remark;
        $this->spare1 = $spare1;
        $this->arr = $arr;
        $this->is_limit = $is_limit;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //只执行一次
        //$this->job->delete();
        //
        $Message=new Message();
		$return = $Message->send_sms($this->mobile,$this->type,$this->remark,$this->spare1,$this->arr,$this->is_limit);
		return true;
    }
	
	
	/**
	 * Handle a job failure.
	 *
	 * @return void
	 */
	public function failed()
	{
		// Called when the job is failing...
		
	}
}
