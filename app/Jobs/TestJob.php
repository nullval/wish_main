<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        //
        $this->id=$id;
        return redirect('http://tdzdapi.343wew.top/test/test?id='.$this->id);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
		$url = config('app.env') == 'local' ? 'http://tdzdshopapi.343wew.top/' : 'http://shop-api.3dqxm.com/';
		new_logger('queue_test.log','测试',[config('app.env'),'env',$url]);
    }
}
