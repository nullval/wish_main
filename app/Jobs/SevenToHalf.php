<?php

/**
 * 从第一个人开始，往下推人
 * 以第一个人为根节点，达到7人后触发切割，第一人不变，245,367 分两组分别挂到 2和3的推荐人下面
 * 注意无限触发切割操作，添加进新的队列即可
 */
namespace App\Jobs;

use App\Exceptions\ApiException;
use App\Models\AgentSevenToHalf;
use App\Models\Agent;
use App\Models\Bank;
use App\Models\User;
use App\Models\UserFreezeLog;
use App\Models\Weachat;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class SevenToHalf implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	
	private $id;
	private $inviter_id;
	/**
	 * Create a new job instance.
	 * @param int $id agent表的自增ID
	 * @param int $inviter_id 推荐人ID，同agent表inviter_id
	 * @return void
	 */
	public function __construct($id,$inviter_id)
	{

		$this->id = $id;
		$this->inviter_id = $inviter_id;
	}
	
	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$agent = AgentSevenToHalf::firstOrCreate([
			'agent_id' => $this->id,
		],[
            'parent_id' => AgentSevenToHalf::where(['agent_id'=>$this->inviter_id])->value('id') ?? 1,
			'microtime' => $this->microtime(),
        ]);
		$max_count = 7;
		$parents = $agent->parentSeven($max_count);
		$seven = $parents->sortBy('microtime')->slice(-$max_count)->values();	// 列表最终顺序时间从远到近，最近的(下标0)为根节点
		// 往上找7个人推荐，如果 >=7 个，就触发裂变
		if($parents->count() >= $max_count){
			DB::transaction(function() use ($seven){
				$first = $seven->first();
				$first_parent_id = $first->parent_id;
				$two = $seven->slice(1,1);
				$two = $two->merge($seven->slice(3,2));
				
				$three = $seven->slice(2,1);
				$three = $three->merge($seven->slice(5));

                $first_invite_has = $seven->contains('parent_id',$first->id);

				// 开始修改 3推4
				$parent_id = 1;
				$two->each(function($v) use (&$parent_id){
					$v->parent_id = $parent_id;
					$v->microtime = $this->microtime();
					$v->save();
					$parent_id = $v->id;
				});
				
				$parent_id = 1;
				$three->each(function($v) use (&$parent_id){
					$v->parent_id = $parent_id;
					$v->microtime = $this->microtime();
					$v->save();
					$parent_id = $v->id;
				});
				
				// 出采人默认挂根节点，如果有推荐人就挂到推荐人下面去
				$parent_inviter_id = Agent::where(['id'=>$first->agent_id])->value('inviter_id');
                $first->parent_id=$first_parent_inviter_half = AgentSevenToHalf::where(['agent_id'=>$parent_inviter_id])->value('id') ?? 1;
				$first->microtime = $this->microtime();
				$first->save();

				//出彩人
                $first_id=$first->agent_id;



                $first_exchange_amount=400;
                $first_cost_amount=1600;
                $parent_exchange_amount=600;
                $parent_cost_amount=2400;

                #1赠送奖励
                $this->give_reward($first_id,$parent_inviter_id,$first_exchange_amount,$first_cost_amount,$parent_exchange_amount,$parent_cost_amount);

                #2冻结与解冻奖励
                $this->freeze_point($first_id,$parent_inviter_id,$first_exchange_amount,$first_cost_amount,$parent_exchange_amount,$parent_cost_amount);

                #3微信推送
                $this->weachat_notify($first_id,$parent_inviter_id,$first_exchange_amount,$first_cost_amount,$parent_exchange_amount,$parent_cost_amount);

				// 然后出采人再触发裂变，在进行队列判断
				dispatch((new SevenToHalf($first->agent_id,$first_parent_inviter_half))->onQueue('seven_to_half'));
			});
		}
	}

    /**
     * @param $first_id 出彩人
     * @param $parent_inviter_id 出彩人推荐人
     * @param $first_exchange_amount 出彩奖 兑换积分
     * @param $first_cost_amount 出彩奖 消费积分
     * @param $parent_exchange_amount 分享奖 兑换积分
     * @param $parent_cost_amount 分享奖 消费积分
     * 1赠送奖励
     */
    private function give_reward($first_id,$parent_inviter_id,$first_exchange_amount,$first_cost_amount,$parent_exchange_amount,$parent_cost_amount){
        $first_agent_mobile=Agent::where(['id'=>$first_id])->value('mobile');
        #1赠送奖励
        $Bank=new Bank();
        //出彩奖 2000 分享奖 3000均 80%消费积分 20%兑换积分
        //获取系统兑换积分钱包
        $sys_exchange_purse_id=$Bank->get_sys_purse_id(11);
        //获取系统消费积分钱包
        $sys_cost_purse_id=$Bank->get_sys_purse_id(10);
        // todo.. 两笔奖励
        //赠送出彩奖
        $first_exchange_purse_id=$Bank->userWallet($first_id,11,5)->purse_id;
        $first_cost_purse_id=$Bank->userWallet($first_id,10,5)->purse_id;
        $Bank->doApiTransfer($sys_exchange_purse_id,$first_exchange_purse_id,$first_exchange_amount,10092,
            '队列出彩获得出彩奖。',
            '队列出彩获得兑换积分');
        $Bank->doApiTransfer($sys_cost_purse_id,$first_cost_purse_id,$first_cost_amount,10093,
            '队列出彩获得出彩奖。',
            '队列出彩获得消费积分');
        //赠送分享奖
        $parent_exchange_purse_id=$Bank->userWallet($parent_inviter_id,11,5)->purse_id;
        $parent_cost_purse_id=$Bank->userWallet($parent_inviter_id,10,5)->purse_id;
        $Bank->doApiTransfer($sys_exchange_purse_id,$parent_exchange_purse_id,$parent_exchange_amount,10092,
            $first_agent_mobile.'出彩,推荐人获得分享奖。',
            '推荐人出彩获得兑换积分');
        $Bank->doApiTransfer($sys_cost_purse_id,$parent_cost_purse_id,$parent_cost_amount,10093,
            $first_agent_mobile.'出彩,推荐人获得分享奖。',
            '推荐人出彩获得消费积分');
    }

    /**
     * @param $first_id 出彩人
     * @param $parent_inviter_id 出彩人推荐人
     * @param $first_exchange_amount 出彩奖 兑换积分
     * @param $first_cost_amount 出彩奖 消费积分
     * @param $parent_exchange_amount 分享奖 兑换积分
     * @param $parent_cost_amount 分享奖 消费积分
     * 1冻结与解冻奖励
     */
    private function freeze_point($first_id,$parent_inviter_id,$first_exchange_amount,$first_cost_amount,$parent_exchange_amount,$parent_cost_amount){
        //获取出彩人下级推荐人
        $inviter_has=Agent::where(['inviter_id'=>$first_id])->value('id');
        #2冻结与解冻奖励
        //若出彩人没有推荐其他人,则本人获得的出彩奖和推荐人获得的分享奖冻结
        $UserFreezeLog=new UserFreezeLog();
        if(empty($inviter_has)){
            $UserFreezeLog->freeze_log($first_id,11,$first_exchange_amount,'出彩人未推荐用户,出彩奖冻结',5,1);
            $UserFreezeLog->freeze_log($first_id,10,$first_cost_amount,'出彩人未推荐用户,出彩奖冻结',5,1);
            //冻结分享 该用户第一次出彩没推荐人才冻结,后面的不冻结
            $has_chucai=$UserFreezeLog->where(['type'=>1,'user_id'=>$first_id,'purse_type'=>11])->count('id');
            if($has_chucai<=1){
                $UserFreezeLog->freeze_log($parent_inviter_id,11,$parent_exchange_amount,'推荐的出彩人未推荐用户,分享奖冻结',5,1,$first_id);
                $UserFreezeLog->freeze_log($parent_inviter_id,10,$parent_cost_amount,'推荐的出彩人未推荐用户,分享奖冻结',5,1,$first_id);
            }
        }
    }

    /**
     * @param $first_id 出彩人
     * @param $parent_inviter_id 出彩人推荐人
     * @param $first_exchange_amount 出彩奖 兑换积分
     * @param $first_cost_amount 出彩奖 消费积分
     * @param $parent_exchange_amount 分享奖 兑换积分
     * @param $parent_cost_amount 分享奖 消费积分
     * 3微信通知
     */
    private function weachat_notify($first_id,$parent_inviter_id,$first_exchange_amount,$first_cost_amount,$parent_exchange_amount,$parent_cost_amount){
        //微信推送
        $Weachat=new Weachat();
        $notify_date=date('Y-m-d H:i:s');

        $first_agent_mobile = Agent::where(['id'=>$first_id])->value('mobile');
        $notify_wechat_arr=[
            '出彩获得出彩奖',
            $notify_date,
            '出彩获得出彩奖',
            handle_mobile($first_agent_mobile),
            '获得额度',
            " 兑换积分:".$first_exchange_amount." 消费积分:".$first_cost_amount,
        ];
        $notify_msg_remark='您出彩了,平台赠送您'.$first_exchange_amount.'兑换积分与'.$first_cost_amount.'消费积分。';
        $Weachat->push_buy_good_message($first_agent_mobile,3,$notify_wechat_arr,$notify_msg_remark);

        $parent_mobile = Agent::where(['id'=>$parent_inviter_id])->value('mobile');
        $notify_wechat_arr=[
            '推荐人出彩获得分享奖',
            $notify_date,
            '推荐人出彩获得分享奖',
            handle_mobile($parent_mobile),
            '获得额度',
            " 兑换积分:".$parent_exchange_amount." 消费积分:".$parent_cost_amount,
        ];
        $notify_msg_remark='您推荐的'.$first_agent_mobile.'出彩了,平台赠送您'.$parent_exchange_amount.'兑换积分与'.$parent_cost_amount.'消费积分。';
        $Weachat->push_buy_good_message($parent_mobile,3,$notify_wechat_arr,$notify_msg_remark);
    }
    
    private function microtime(){
    	$microtime = microtime(true);
    	return (string)$microtime;
	}
}