<?php

namespace App\Jobs;

use App\Models\Agent;
use App\Models\AgentOperate;
use App\Models\Bank;
use App\Models\Message;
use App\Models\Order;
use App\Models\Pos;
use App\Models\User;
use App\Models\UserFreezeLog;
use App\Models\UserPurse;
use App\Models\VOrder;
use App\Models\Weachat;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;


class handleBuyPosFirstOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $order_sn;
    private $is_send;


    /**
     * Create a new job instance.
     * @param $order_sn 订单编号
     * @param $is_send 是否发送推送
     * @return void
     */
    public function __construct($order_sn,$is_send=1)
    {
        $this->order_sn=$order_sn;
        $this->is_send=$is_send;
    }





    /*
    * 处理购买pos机订单 (通联)
    * $terminalId 终端号
    * $order_sn 订单编号
    * $amount 消费金额 (以元为单位)
    * $status 订单状态 状态  1处理中 2处理成功 3处理失败
    * $paytime 支付时间
    * $acct 交易卡号
    * $trxcode 交易类型
    */
    public function handle()
    {
        //只执行一次
//        $this->job->delete();
        set_time_limit(0);

        new_logger('job_handleBuyPosOrder.log','order_sn',['order_sn'=>$this->order_sn]);
        #购买pos机
        //判断此用户是否有推荐人，没有根据pos机信息绑定推荐人
        //订单信息
        $order_info=VOrder::where(['order_sn'=>$this->order_sn])->first();
        //实付金额
        $amount=$order_info->actual_amount;
        $buyer_id=$order_info->buyer_id;
        $order_sn=$this->order_sn;

        if($order_info->status==2) {
            //支付成功
            //判断是否有效订单
            if($order_info['is_true']==1){
                $Bank=new Bank();
                //真实订单
                //1.无推荐人绑定推荐关系 (推荐人不一定是未特商城pos机本人)
                $agent_info=Agent::where(['mobile'=>$order_info->invite_mobile])->first(['id','level']);
                $agent_uid=$agent_info->id;
                $agent_level=$agent_info->level;
                //获取中央银行钱包
                $bank_purse_id=$Bank->get_bank_purse_id();
                //获取平台收益钱包
                $sys_purse_id=$Bank->get_sys_purse_id(1);
                //获取用户过账钱包(也是机主)
                $buyer_purse_id=$Bank->userWallet($buyer_id,10000,5,['purse_id'])->purse_id;
                //用户手机号
                $buyer_mobile=Agent::where(['id'=>$buyer_id])->first(['mobile'])->mobile;

                //获取推荐人信息
                $invite_agent_info=Agent::where(['id'=>$agent_uid])->first();
                $agent_mobile=$invite_agent_info->mobile;
                //2018/04/23直推 收益400
                $first_part_point=400;
                //2018/04/23间推 收益100
                $second_part_point=100;
                $invite_agent_id=$invite_agent_info->inviter_id;
                //间推人
                $second_info=Agent::where(['id'=>$invite_agent_id])->first();
                $second_mobile=$second_info->mobile;
                //获取推荐机主收益钱包
                $first_purse_id=$Bank->userWallet($agent_uid,1,5,['purse_id'])->purse_id;
                //获取二级推荐机主收益钱包
                if(!empty($invite_agent_id)){
                    $second_purse_id=$Bank->userWallet($invite_agent_id,1,5,['purse_id'])->purse_id;
                }
                //机主绑定推荐人及运营中心
                $Agent=new Agent();
                $Agent->bind_inviter($buyer_id,$agent_uid);
                $is_direct_buy=$order_info->is_direct_buy;
                $amount=$order_info->amount;


                DB::transaction(function () use($buyer_id,$order_info,$bank_purse_id,$sys_purse_id,$buyer_purse_id,
                    $amount,$buyer_mobile,$order_sn,$first_part_point,$second_part_point,
                    $first_purse_id,$second_purse_id){

                    $Bank=new Bank();
                    //1.中央银行入用户钱包
                    $Bank->doApiTransfer($bank_purse_id,$buyer_purse_id,$amount,10016,
                        $buyer_mobile.'购买未特商城pos机消费了'.$amount.'元,订单号:'.$order_sn,
                        '用户购买未特商城pos机');

                    //2.钱直接到平台收益
                    $Bank->doApiTransfer($buyer_purse_id,$sys_purse_id,$amount,10017,
                        $buyer_mobile.'购买未特商城pos机消费了'.$amount.'元,平台获得收益'.$amount.',订单号:'.$order_sn,
                        '购买未特商城pos机获得收益');
                    //4.消费买单收取的手续费(第三方)
                    $Order=new Order();
                    $Order->buckle_fee($order_info->notify_type,$amount,$buyer_mobile.'购买未特商城pos机消费了'.$amount.'元,平台扣除手续费,订单号:'.$order_sn);

                    //2018/04/14修正 海哥要求
                    //5 直推补贴
                    $Bank->doApiTransfer($sys_purse_id,$first_purse_id,$first_part_point,10107,
                        $buyer_mobile.'购买未特商城pos机消费了'.$amount.'元,机主获得直推补贴收益,订单号:'.$order_sn,
                        '购买未特商城pos机获得直推补贴收益');

                    //2018/004/14修正 海哥要求
                    //6 二级分销补贴
                    if(!empty($second_purse_id)){
                        $Bank->doApiTransfer($sys_purse_id,$second_purse_id,$second_part_point,10108,
                            $buyer_mobile.'购买未特商城pos机消费了'.$amount.'元,机主获得二级分销补贴收益,订单号:'.$order_sn,
                            '购买未特商城pos机获得二级分销收益');
                    }

                    //赠送落地商家赠送收益次数
                    Agent::where(['id'=>$buyer_id])->increment('pos_time',1);
                });

                new_logger('job_handleBuyPosOrder.log','end',['-------------']);

                if($this->is_send==1){
                    //微信通知
                    //是否发送推送
                    $Weachat=new Weachat();
                    $notify_date=date('Y-m-d H:i:s');


                    //2018/04/03 加入微信推送通知 直推人
                    $first_part_point=get_num($first_part_point);
                    $notify_wechat_arr=[
                        '机主购买未特商城pos机获得奖励',
                        $notify_date,
                        '购买未特商城pos机订单',
                        handle_mobile($buyer_mobile),
                        '获得直推补贴',
                        $first_part_point."收益奖励\n订单编号:   ".$order_sn
                    ];
                    $notify_msg_remark='亲,恭喜您于'.$notify_date.'获得直接分享的机主'.$buyer_mobile.',平台赠送您'.$first_part_point.'收益奖励';
                    if($first_part_point>0) {
                        $Weachat->push_buy_good_message($agent_mobile, 3, $notify_wechat_arr, $notify_msg_remark);
                    }

                    //2018/04/03  加入微信推送通知 间接推人
                    $second_part_point=get_num($second_part_point);
                    $notify_wechat_arr=[
                        '机主购买未特商城pos机获得奖励',
                        $notify_date,
                        '购买未特商城pos机订单',
                        handle_mobile($buyer_mobile),
                        '获得二级补贴',
                        $second_part_point."收益奖励'.'\n订单编号:   ".$order_sn
                    ];
                    $notify_msg_remark='亲,恭喜您于'.$notify_date.'获得间接分享的机主'.$buyer_mobile.',平台赠送您'.$second_part_point.'收益奖励';
                    if($second_part_point>0){
                        $Weachat->push_buy_good_message($second_mobile,3,$notify_wechat_arr,$notify_msg_remark);
                    }

                }
            }
        }
    }

    /**
     * 处理失败任务
     *
     * @return void
     */
    public function failed()
    {
//        VOrder::where(['order_sn'=>$this->order_sn])->update(['status'=>1]);
        $content='购买未特商城pos机回调处理失败,订单编号:'.$this->order_sn;
        DB::table('fail_redis_job')->insert(['content'=>$content,'created_at'=>time2date()]);
        $Message=new Message();
        $Message->send_sms('13631272493',16,$content,'',[],0);
        // Called when the job is failing...
    }
}
