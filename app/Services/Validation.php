<?php
namespace App\Services;

use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\DB;

class Validation extends Validator{

    //验证通过用户编号验证用户是否存在
    public function ValidateUserisexit($attribute, $value, $parameters){
        $user_info=DB::table('user')->where(['member_id'=>$value])->first(['member_id']);
        return empty($user_info->member_id)?false:true;
    }

    //验证数值必须是100的整数倍
    public function ValidateIshundred($attribute, $value, $parameters){
        return $value%100===0?true:false;
    }

    //判断是否自己给自己转账
    public function ValidateIsmyself($attribute, $value, $parameters){
        return $value==session('user_info')->member_id?false:true;
    }

    //手机验证
    public function ValidateMobile($attribute, $value, $parameters){
        return preg_match("/^1[3456789]{1}\d{9}$/",$value)?true:false;
    }

    //判断数值必须大于某个数值
    public function ValidateGreaternum($attribute, $value, $parameters){
        return $value>=$parameters[0]?true:false;
    }


}
?>