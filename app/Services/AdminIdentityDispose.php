<?php

namespace App\Services;
use App\Models\AdminRole;
use App\Models\Wish\WishAgent;
use App\Models\Wish\WishCompany;
use App\Models\Wish\WishGeneralize;
use App\Models\Wish\WishSeller;

/**
 * 商家|直推人|代理|分公司 调度
 * User: Administrator
 * Date: 2018/8/11 0011
 * Time: 20:31
 */
class AdminIdentityDispose{
    //商家标识
    const IDENTITY_SELLER = 4;
    //直推人标识
    const IDENTITY_GENERALIZE = 3;
    //代理人标识
    const IDENTITY_AGENT = 2;
    //分公司标识
    const IDENTITY_COMPANY = 1;
    //总公司标识
    const IDENTITY_CORPORATION = 5;

    /**
     * 根据英文记号得到标识
     * @param $sign 分为 seller|generalize|agent|company
     * @return int
     */
    public function getIdentityBySign($sign){
        switch ($sign){
            case 'seller':
                return self::IDENTITY_SELLER;
            case 'generalize':
                return self::IDENTITY_GENERALIZE;
            case 'agent':
                return self::IDENTITY_AGENT;
            case 'company':
                return self::IDENTITY_COMPANY;
            case 'corporation':
                return self::IDENTITY_CORPORATION;
        }
    }

    /**
     * 根据标识得到英文记号
     * @param int $identity
     * @return int
     */
    public function getSignByIdentity($identity){
        switch ($identity){
            case self::IDENTITY_SELLER:
                return 'seller';
            case self::IDENTITY_GENERALIZE:
                return 'generalize';
            case self::IDENTITY_AGENT:
                return 'agent';
            case self::IDENTITY_COMPANY:
                return 'company';
            case self::IDENTITY_CORPORATION:
                return 'corporation';
        }
    }


    /**
     * 根据身份类型返回角色的ID
     * @param $identity
     * @return array
     */
    public function getRoleIdByIdentity($identity){
        switch ($identity){
            case self::IDENTITY_SELLER:
                return [
                  AdminRole::ROLE_SELLER,
                ];
                break;
            case self::IDENTITY_GENERALIZE:
                return [
                    AdminRole::ROLE_GENERALIZE,
                ];
                break;
            case self::IDENTITY_AGENT:
                return [
                    AdminRole::ROLE_AGENT_1,
                    AdminRole::ROLE_AGENT_2,
                    AdminRole::ROLE_AGENT_3,
                    AdminRole::ROLE_AGENT_4,
                    AdminRole::ROLE_AGENT_5,
                ];
                break;
            case self::IDENTITY_COMPANY:
                return [
                    AdminRole::ROLE_COMPANY,
                ];
                break;
            case self::IDENTITY_CORPORATION:
                return [
                    AdminRole::ROLE_CORPORATION,
                ];
                break;
            default:
                return false;
        }
    }

    /**
     * 验证账号是否可以注册，同一个用户有N种角色
     * @param $phone
     * @param int $identity 要验证的角色，不传则验证所有
     * @return bool
     */
    public function checkIsRegister($phone,$identity = 0){
        if(!is_mobile($phone)){
            return false;
        }
        $condition = ['mobile'=>$phone];

        if($identity === 0){
            return false;
        }else{
            //验证指定身份
            switch ($identity){
                case self::IDENTITY_SELLER:
                    //验证商家是否允许注册
                    $result_count = WishSeller::where($condition)->count();
                    break;
                case self::IDENTITY_GENERALIZE:
                    //验证直推人是否允许注册
                    $result_count = WishGeneralize::where($condition)->count();
                    break;
                case self::IDENTITY_AGENT:
                    //验证代理是否允许注册
                    $result_count = WishAgent::where($condition)->count();
                    break;
                case self::IDENTITY_COMPANY:
                    //验证分公司是否允许注册
                    $result_count = WishCompany::where($condition)->count();
                    break;
                default:
                    return false;
            }
            if($result_count >= 1){
                return false;
            }
            return true;
        }
    }



    /**
     * 根据手机号码和身份标识得到该用户的一行数据
     * @param $phone
     * @param int $identity 身份标识
     * @return mixed
     */
    public function getInfoByIdentityAndPhone($phone,$identity){
        if(!is_mobile($phone)){
            return false;
        }
        if(empty($identity)){
            return false;
        }
        $condition = ['mobile'=>$phone];
        switch ($identity){
            case self::IDENTITY_SELLER:
                $result =  WishSeller::where($condition)->first();
                break;
            case self::IDENTITY_GENERALIZE:
                $result =  WishGeneralize::where($condition)->first();
                break;
            case self::IDENTITY_AGENT:
                $result =  WishAgent::where($condition)->first();
                break;
            case self::IDENTITY_COMPANY:
                $result =  WishCompany::where($condition)->first();
                break;
            default:
                $result = false;
        }
        return $result;
    }

    /**
     * 检查当前手机号是否有指定身份
     * @param $mobile
     * @param $identity 当前的身份
     * @param int $isAdmin 默认：跳过总公司的验证
     * @return mixed 成功返回身份标识
     */
    public function checkMobileIsIdentity($mobile,$identity,$isAdmin = 0){

        $condition = ['mobile'=>$mobile];
        $myIdentity = 0;
        switch ($identity){
            case self::IDENTITY_SELLER:
                $result =  WishSeller::where($condition)->count();
                $myIdentity = self::IDENTITY_SELLER;
                break;
            case self::IDENTITY_GENERALIZE:
                $result =  WishGeneralize::where($condition)->count();
                $myIdentity = self::IDENTITY_GENERALIZE;
                break;
            case self::IDENTITY_AGENT:
                $result =  WishAgent::where($condition)->count();
                $myIdentity = self::IDENTITY_AGENT;
                break;
            case self::IDENTITY_COMPANY:
                $result =  WishCompany::where($condition)->count();
                $myIdentity = self::IDENTITY_COMPANY;
                break;
            default:
                $result = false;
        }
        if($isAdmin){
            if($result >= 1){
                return $myIdentity;
            }
        }else{
            $user=session('admin_info');
            if($result >= 1 || $user['role_id'] == AdminRole::ROLE_CORPORATION){
                return $myIdentity;
            }
        }
        return false;
    }

    /**
     * 获取当前登录用户的身份类型
     */
    public function getUserIdentity($roleId){
        switch ($roleId){
            case AdminRole::ROLE_SELLER:
                return self::IDENTITY_SELLER;
                break;
            case AdminRole::ROLE_GENERALIZE:
                return self::IDENTITY_GENERALIZE;
                break;
            case AdminRole::ROLE_AGENT_1:
            case AdminRole::ROLE_AGENT_2:
            case AdminRole::ROLE_AGENT_3:
            case AdminRole::ROLE_AGENT_4:
            case AdminRole::ROLE_AGENT_5:
                return self::IDENTITY_AGENT;
                break;
            case AdminRole::ROLE_COMPANY:
                return self::IDENTITY_COMPANY;
                break;
            case AdminRole::ROLE_CORPORATION:
                return self::IDENTITY_CORPORATION;
                break;
            default:
                return null;
        }
    }

}