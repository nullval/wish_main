<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FreezeHis extends Model
{
	protected $table = 'freeze_his';

    public function __construct($year=0,array $attributes = [])
    {
        parent::__construct($attributes);
        $year=empty($year)?date('Y'):$year;
        $this->table='freeze_his_'.$year;
    }

	/**
	 * 求出冻结金额
	 * @param $purseid
	 * @return string
	 */
	public function GetAoumt($purseid){
        $freezeamount=0;
        for($i=2017;$i<date('Y');$i++){
            $freezeamount+=DB::table('freeze_his_'.$i)->where(['purseid'=>$purseid,'status'=>0])->sum('amount');
        }
        return $freezeamount;
	}
}
