<?php

namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AgentOperate extends Model
{
	protected $table = 'wish_agent_operate';

	protected $point_arr=[
	    '1'=>'50-5',
        '2'=>'300-10',
        '3'=>'1500-15',
        '4'=>'5000-18',
    ];

    /**
     * @param $operate_id
     *
     * 0-50台 5%
     * 50-300台 10%
     * 300-1500台 15%
     * 1500-5000台 18%
     * 5000台以上 20%
     *
     * 2017/12/13号修正 0-1000台 每台1000元 台以上每台500元
     */
    function give_pos_order_profit($operate_id,$actual_pos_num=1,$buyer_mobile='',$order_sn='',$order_amount=0){
        $UserPurse=new UserPurse();
        $operate_info=$this->where(['id'=>$operate_id])->first();
        $operate_mobile=$operate_info->mobile;
        //查询已成功购买pos机订单数目
        $count=DB::table('v_order')->where(['status'=>2,'type'=>2,'operate_id'=>$operate_id,'is_true'=>1])->sum('pos_num');

        $now_count=$count+$actual_pos_num;
//        var_dump($now_count);exit;
        $point_arr=$this->point_arr;
        //pos机销售额
        $pos_amount=5000;
        //购买pos机赠送给运营中心的差额补贴额度
        $amount=0;
        //购买pos机赠送给运营中心的额度
        $s_amount_point=0;
        if(0<$now_count&&$now_count<=50){
            $s_amount_point=5;
        }elseif(50<$now_count&&$now_count<=300){
            $s_amount_point=10;
        }elseif(300<$now_count&&$now_count<=1500){
            $s_amount_point=15;
        }elseif(1500<$now_count&&$now_count<=5000){
            $s_amount_point=18;
        }elseif($now_count>5000){
            $s_amount_point=20;
        }
        if($s_amount_point>0){
            $s_amount=$actual_pos_num*$pos_amount*$s_amount_point/100;
            $UserPurse->give_agent_point($operate_mobile,$s_amount,10055,$buyer_mobile.'购买未特商城pos机消费了'.$order_amount.'元,运营中心获得积分'.$s_amount.',订单号:'.$order_sn.',累计台数:'.$now_count.'台','购买未特商城pos机赠送积分');

            //2017/12/21 加入微信推送通知 运营中心
            $notify_date=date('Y-m-d H:i');
            $Weachat=new Weachat();
            $s_amount=get_num($s_amount);
            $notify_wechat_arr=[
                '机主购买未特商城pos机获得积分补贴',
                $notify_date,
                '购买未特商城pos机订单',
                handle_mobile($buyer_mobile),
                '获得补贴',
                $s_amount
            ];
            $notify_msg_remark='恭喜您的运营中心于'.$notify_date.'获得一笔'.$s_amount.'的机主购买未特商城pos机订单的积分补贴。';
            if($s_amount>0){
                $Weachat->push_buy_good_message($operate_mobile,3,$notify_wechat_arr,$notify_msg_remark);
            }
        }
        //判断是否给一次性补贴
        foreach ($point_arr as $k=>$v){
            $arr1=explode('-',$v);
            $num1=$arr1[0];
            $point1=$arr1[1];
            $max_count=5000;
            if($now_count>=$num1&&$num1>$count){
                if($k==1){
                    //直接赠送
                    $amount=$num1*$pos_amount*$point1/100;
                }else{
                    $arr2=explode('-',$point_arr[$k-1]);
                    $num2=$arr2[0];
                    $point2=$arr2[1];
                    //赠送加补差
                    $amount=$num2*$pos_amount*($point1-$point2)/100;
                }
            }
            if($now_count==($max_count+1)){
                $amount=5000*$pos_amount*20/100;
            }
            $detail='运营中心推荐机主购买未特商城pos机满'.$now_count.'台数获得补贴'.$amount;
            if($amount>0){
                $UserPurse->give_agent_point($operate_mobile,$amount,10043,$detail,'运营中心推荐机主购买未特商城pos机满台数获得差额积分补贴');

//                $Bank=new Bank();
//                //收益到运营中心
//                //获取平台收益钱包
//                $sys_purse_id=$Bank->get_sys_purse(1)->purse_id;
//                //获取运营中心钱包
//                $operate_purse_id=$Bank->userWallet($operate_id,1,11)->purse_id;
//                $Bank->doTransfer($sys_purse_id,$operate_purse_id,$amount,10043,
//                    $detail,
//                    '运营中心推荐机主购买未特商城pos机满台数获得补贴');
//                return arr_post(1,'处理成功');
                break;
            }
        }
        return arr_post(1,'处理成功');
    }



    /**
     * 新增运营中心
     * @param $mobile 运营中心手机号
     * @return mixed
     */
    public function add_operate($mobile,$parent_mobile='',$remarks,$level=1,$inviter_user_mobile=0){
        //判断该手机号是否是机主
        $agent=Agent::where(['mobile' => $mobile])->first();
        if (empty($agent)){
            throw new ApiException('还没成为机主，无法成为运营中心！');
        }
        //判断该机主是否禁用
        if ($agent->status == 0){
            throw new ApiException('该机主已经被禁用，无法成为运营中心！');
        }
        //判断运营中心是否存在
        $operate=$this->where(['mobile' => $mobile])->first();
        if (!empty($operate)){
            throw new ApiException('已经是运营中心，请勿重复添加！');
        }
        if($mobile==$parent_mobile||$mobile==$inviter_user_mobile){
            throw new ApiException('推荐人不能是自己!');
        }
        if((!empty($inviter_user_mobile))&&(!empty($parent_mobile))){
            throw new ApiException('上级运营中心或者推荐人不能同时填写!');
        }
        if(!empty($inviter_user_mobile)){
            //验证手机号码
            if (!preg_match("/^1[3456789]\d{9}$/", $inviter_user_mobile)){
                throw new ApiException('请输入正确的手机号码');
            }
            $operate_arr['inviter_user_id']=User::firstOrCreate(['mobile'=>$inviter_user_mobile])->id;
        }else{
            if(!empty($parent_mobile)){
                $parent_operate_info=$this->where(['mobile' => $parent_mobile])->first();
                if (!isset($parent_operate_info->id)){
                    throw new ApiException('上级运营中心不存在！');
                }


                if($parent_operate_info->level<$level){
                    throw new ApiException('上级运营中心等级必须大于或等于运营中心等级');
                }
                $operate_arr['parent_id'] = $parent_operate_info->id;
            }else{
                if($level!=3){
                    throw new ApiException('请填写上级运营中心手机号！');
                }
            }
        }
        //做事务：新增运营中心
        DB::transaction(function () use($mobile,$remarks,$level,&$operate_arr){
            //1、在运营中心表中插入数据
            $operate_arr['created_at'] = time2date();
            $operate_arr['status'] = 1;
            $operate_arr['mobile'] = $mobile;
            $operate_arr['remarks'] = $remarks;
            $operate_arr['level']=$level;
            $id=$this->insertGetId($operate_arr);

            //2、修改该机主的营业中心id
            Agent::where(['mobile' => $mobile])->update(['operate_id'=>$id,'is_admin'=>1]);

            //3.将之前挂在他下面的机主挂到他的运营中心下面
//            $agent_id=Agent::where(['mobile' => $mobile])->first(['id'])->id;
//            $Agent=new Agent();
//            $down_arr=$Agent->find_down([$agent_id]);
//            if(!empty($down_arr)){
//                Agent::whereIn('id',$down_arr)->update(['operate_id'=>$id]);
//            }

        });
        return true;
    }

    /**
     * 找出运营中心三级 (包含自身)
     */
    public static function find_operate_level($id){
        static $o_arr;
        $operate_info=AgentOperate::where(['id'=>$id])->first();
        if($operate_info->level<=3&&isset($operate_info->level)){
            $parent_id=$operate_info->parent_id;
            $operate_parent_info=AgentOperate::where(['id'=>$parent_id])->first();
            $o_arr[$operate_info->level]=$operate_info;
            if($operate_parent_info->level<=$operate_info->level){
                return collect($o_arr)->toArray();
            }
            self::find_operate_level($parent_id);
        }
        return collect($o_arr)->toArray();
    }

    /**
     * 通过时间查找运营中心三级
     */
    public function find_operate_level_by_time()
    {
        static $t_arr;
        for($level=1;$level<=3;$level++){
            $operate_id=UsedOperate::where(['level'=>$level])->value('operate_id');
            if(empty($operate_id)){
                $info=AgentOperate::where(['level'=>$level])->first();
            }else{
                $info=AgentOperate::where(['level'=>$level])->where('id','>',$operate_id)->first();
                if(!isset($info->id)){
                    $info=AgentOperate::where(['level'=>$level])->orderBy('id','asc')->first();
                }
            }
            $t_arr[$level]=$info;
        }
        return collect($t_arr)->toArray();
    }
}
