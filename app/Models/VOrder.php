<?php
namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Support\Facades\DB;

class VOrder extends CommonModel {
    protected $table = 'v_order';

    /**
     * @param $query
     * @return mixed
     * 查找有效的消费订单
     */
    public function scopeConsumeOrder($query,$status=2)
    {
        return $query->whereIn('v_order.type',[1,3,4,5,7])->where(['v_order.status'=>$status]);
    }

    /**
     * @param $query
     * @return mixed
     * 查找有效的购买pos机子订单
     */
    public function scopePosOrder($query,$status=2)
    {
        return $query->where(['v_order.type'=>2])->where(['v_order.status'=>$status]);
    }


    /**
     * @param $query
     * @return mixed
     * 格式化分页列表，取出对应的图片信息
     */
    public function scopePagesParse($query){
        $data = $query->pages();
        return $data;
    }

    /**
     * @param $query
     * @return mixed
     *查询归属商家的订单
     */
    public function scopeSellerOrder($query)
    {
        return $query->whereIn('type',[1,3,5]);
    }

    //根据条件更新某一订单状态
    public function update_order($where,$update){
        for ($i=2017;$i<=date('Y');$i++){
            DB::table('order_'.$i)->where($where)->update($update);
        }
    }

}