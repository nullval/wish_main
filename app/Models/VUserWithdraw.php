<?php
namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Support\Facades\DB;

class VUserWithdraw extends CommonModel {
    protected $table = 'v_user_withdraw';

    /**
     * @param $query
     * @return mixed
     * 格式化分页列表，取出对应的图片信息
     */
    public function scopePagesParse($query){
        $data = $query->pages();
        return $data;
    }
}