<?php
namespace App\Models;

use App\Exceptions\ApiException;
use App\Jobs\SendMessage;
use App\Libraries\msg\WechatPush;
use Illuminate\Support\Facades\DB;

class Weachat extends CommonModel {

    /**
     * @param $mobile 用户手机号
     * @param $member_type 类型 1用户 2商家 3机主 4地推人员
     * 用户购买商品后通知获得积分进行推送通知 微信/短信
     */
    public function push_buy_good_message($mobile,$member_type=1,$arr=[],$remark='',$is_special=1){
        if($member_type==1){
            $table='user';
        }elseif($member_type==2){
            $table='seller';
        }elseif($member_type==3){
            $table='agent';
        }elseif($member_type==4){
            $table='pushQr_user';
        }

        if($is_special==1){
            //不做特殊处理
            $info=DB::table($table)->where(['mobile'=>$mobile])->first();

            if(!isset($info->id)){
                return false;
//                throw new ApiException('用户不存在');
            }
            $openid=$info->openid;
        }else{
            //做特殊处理
            //暂时只支持用户
            $info=DB::table('user')->where(['mobile'=>$mobile])->first();

            if(!isset($info->id)){
                $openid=$info->openid;
            }else{
                $openid='';
            }
        }
        new_logger('push_buy_good_message.log','post',['openid'=>$openid,'mobile'=>$mobile,'wechat_arr'=>$arr,'msg_remark'=>$remark]);

        if(config('app.env')!='online'){
            return false;
//            throw new ApiException('只有线上才开启推送');
        }


        if(!empty($openid)){
            new_logger('push_buy_good_message.log','type',['type'=>1]);
                //微信推送
                (new WechatPush())->queue($openid,'vkr6jN80iIHJfwFVyTIdEzCiomZ9ST2AGdFmEznTVnc',[
//                'first'=>'标题如：买家推荐人奖励',
//                'tradeDateTime'=>'提交时间',
//                'orderType'=>'订单类型，如pos机机主订单',
//                'customerInfo'=>'买家手机号，隐藏中间四位',
//                'orderItemName'=>'获得奖励',
//                'orderItemData'=>'20积分',
//                'remark'=>'如有疑问请联系客服处理'
                    'first'=>$arr[0],
                    'tradeDateTime'=>$arr[1],
                    'orderType'=>$arr[2],
                    'customerInfo'=>$arr[3],
                    'orderItemName'=>$arr[4],
                    'orderItemData'=>$arr[5],
                    'remark'=>'如有疑问请联系客服处理'
                ]);

        }else{
            new_logger('push_buy_good_message.log','type',['type'=>2]);
            //短信通知
            dispatch(new SendMessage($mobile,16,$remark,'',[],0));

        }
        return true;

    }

}