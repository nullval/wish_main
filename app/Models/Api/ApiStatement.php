<?php

namespace App\Models\Api;

use App\Models\Bank;
use App\Models\Order;
use App\Models\Seller;
use App\Models\SysConfig;
use App\Models\VOrder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ApiStatement extends Model
{
    protected $table = 'statement';

    public function __construct()
    {
    }


    public function create_statement(){
        DB::transaction(function(){
            if(empty($y_date)){
                $yesterday=date("Y-m-d",strtotime("-1 day"));
            }else{
                $yesterday=$y_date;
            }
            //1.形成对账单 D+1
            $seller_list=Seller::get(['id']);
            //统计每个商家
            foreach ($seller_list as $k=>$v){
                $uid=$v->id;
                //获取商家昨天形成的订单 必须是订单支付/消费买单 有效 处理成功的订单
                $sql="SELECT * FROM v_order WHERE uid = ".$uid." AND (type = 1 OR type=3 OR type=5)  AND is_true = 1 AND status = 2 AND ((date_format(created_at,'%Y-%m-%d')='".$yesterday."' AND is_left=0) OR is_left=1)";
//            print_r($sql);exit;
                $order_list=DB::select($sql);
                if(!empty($order_list)){
                    //返现总金额
                    $sum_amount=0;
                    //订单累计
                    $order_list_arr='';
                    foreach ($order_list as $k1=>$v1){
                        $add_amount=$v1->platform_amount;
                        //120%的利润给他返
                        $sum_amount+=$add_amount;
                        $order_list_arr=$order_list_arr.$v1->order_sn.',';
                        //修改订单状态
                        $VOrder=new VOrder();
                        $is_left=$v1->is_left==1?2:3;
                        $VOrder->update_order(['order_sn'=>$v1->order_sn],['is_left'=>$is_left]);
                    }
                    $amount=$sum_amount*1.2;
                    //每天每笔订单的商家让利的千分之六六
//                $day_amount=$sum_amount*0.0066;
                    $day_amount=ceil($amount/180*10000)/10000;
                    //为商家生成对账单
                    $statement_data['seller_uid']=$uid;
                    $statement_data['amount']=$amount;
                    $statement_data['day_amount']=$day_amount;
                    $statement_data['total_day']=0;
                    $statement_data['finish_day']=0;
                    $statement_data['finish_amount']=0;
                    $statement_data['status']=1;
                    $statement_data['created_at']=time2date();
                    $statement_data['updated_at']=time2date();
                    $statement_data['order_list']=$order_list_arr;
                    //判断是否已创建
                    $this->insert($statement_data);
                }
            }
        });
        return true;
    }

    public function back_cach(){
        //2.返现金 D+2
        //统计所有未完成的对账单 (当天形成的订单不返现)
        //全局判断是否允许商家返还积分或货款
        $is_allow_back_point=SysConfig::attrValue('is_allow_back_point')['attrValue'];
        DB::transaction(function() use ($is_allow_back_point){
            if($is_allow_back_point==1){
//            $sql="SELECT * FROM statement WHERE status = 1 AND date_format(created_at,'%Y-%m-%d')<>'".date('Y-m-d')."'";
                $sql="SELECT * FROM statement WHERE status = 1 and amount>0";
                $statement_list=DB::select($sql);
                $Bank=new Bank();
                //获取平台收益钱包
                $sys_purse_id=$Bank->get_sys_purse_id(1);
                //获取平台积分钱包
                $sys_point_purse_id=$Bank->get_sys_purse_id(3);
//            var_dump($statement_list);exit;
                foreach ($statement_list as $k=>$v){
                    //商家id
                    $seller_uid=$v->seller_uid;
                    //获取商家返现类型
                    $seller_info=Seller::where(['seller.id'=>$seller_uid])
                        ->leftJoin('ruzhu_merchant_basic','ruzhu_merchant_basic.uid','=','seller.id')
                        ->select(['seller.*','ruzhu_merchant_basic.status as mer_status'])
                        ->first();
                    //对账单编号
                    $statement_id=$v->id;
                    $credit=$seller_info->credit;
                    $amount=$v->amount;
                    $day_amount=$v->day_amount;
                    //判断商家是否能返还积分 是否被禁止 是否通过商家审核
                    if($seller_info->is_allow_back_point==1&&$seller_info->mer_status==3){
                        //获取商家返现类型
                        $point_or_money=$seller_info->point_or_money;
                        //每天返现额度
                        //没完成仅需返现
                        if($point_or_money==1){
//                        if($v->created_at<'2018-03-07 00:00:00'){
                            //返积分 每天千分之12
                            $day_amount=$day_amount*2;
//                        }else{
//                            //2018/3/7修改算法 根据商家信用返积分
//                            $speech=get_seller_speech($credit);
//                            $day_amount=($amount/1.2)*$speech/100;
//                        }
                        }elseif($point_or_money==2){
                            if($v->created_at<'2018-03-07 00:00:00'){
                                //返货款 每天千分之6
                                $day_amount=$day_amount;
                            }else{
                                //2018/3/7修改算法 根据商家信用返货款
                                $speech=get_seller_speech($credit);
                                $day_amount=($amount/1.2)*$speech/100;
                            }
                        }
                        $day_amount=get_last_two_num($day_amount);
                        //更新字段
                        $statement_update_data['finish_day']=$v->finish_day+1;
                        $old_finish_amount=$v->finish_amount;
                        $statement_update_data['finish_amount']=$v->finish_amount+$day_amount;
                        if($statement_update_data['finish_amount']>=$v->amount){
                            //完成了就不返现
                            $statement_update_data['status']=2;
                            //将剩余的返
                            $day_amount=$v->amount-$old_finish_amount;
                            $statement_update_data['finish_amount']=$v->amount;
                        }else{
                            $statement_update_data['status']=1;
                        }
                        if($day_amount>0){
                            //没完成仅需返现
                            if($point_or_money==1){
                                //2017/12/17到商家积分
                                $seller_point_purse_id=$Bank->userWallet($seller_uid,3,2)->purse_id;
                                //平台积分到商家积分
                                $arr=$Bank->doApiTransfer($sys_point_purse_id,$seller_point_purse_id,$day_amount,10021,
                                    $seller_info->mobile.'补贴获得积分'.$day_amount.',对账单编号:'.$statement_id,
                                    '商家补贴获得积分');
                                $rs[]=$arr['code']==1?true:false;
                            }elseif($point_or_money==2){
                                //返货款
                                //获取商家收益钱包
                                $seller_purse_id=$Bank->userWallet($seller_uid,1,2)->purse_id;
                                //平台收益到商家收益
                                $arr=$Bank->doApiTransfer($sys_purse_id,$seller_purse_id,$day_amount,10022,
                                    $seller_info->mobile.'补贴获得收益'.$day_amount.',对账单编号:'.$statement_id,
                                    '商家补贴获得货款');
                                $rs[]=$arr['code']==1?true:false;
                            }
                            $statement_update_data['updated_at']=time2date();
                            $rs[]=$this->where(['id'=>$v->id])->update($statement_update_data);
                        }
                    }
                }
            }
        });
        return true;
    }

}
