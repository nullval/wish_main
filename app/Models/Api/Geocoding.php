<?php
/**
 * 根据地理坐标获取国家、省份、城市，及周边数据类(利用百度Geocoding API实现)
 * 百度密钥获取方法：http://lbsyun.baidu.com/apiconsole/key?application=key（需要先注册百度开发者账号）
 * Date:    2015-07-30
 * Author:  fdipzone
 * Ver: 1.0
 *
 * Func:
 * Public  getAddressComponent 根据地址获取国家、省份、城市及周边数据
 * Private toCurl              使用curl调用百度Geocoding API
 */
namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;

class Geocoding extends Model
{
	//百度地图AK(密钥)
	const Ak = 'u2htB1u8pi7TjLpQQpu9NRSgQpnG1OKx';
	// 百度Geocoding API
	const API = 'http://api.map.baidu.com/geocoder/v2/';
	//百度地图批量算路API
	const API1 = 'http://api.map.baidu.com/routematrix/v2/driving?';
	
	// 不显示周边数据
	const NO_POIS = 0;
	
	// 显示周边数据
	const POIS = 1;
	
	/**
	 * 根据地址获取国家、省份、城市及周边数据
	 * @param  Decimal $longitude 经度
	 * @param  Decimal $latitude  纬度
	 * @param  Int     $pois      是否显示周边数据
	 * @return Array
	 */
	public static function getAddressComponent($longitude, $latitude, $pois=self::NO_POIS){
		
		$param = array(
			'ak' => self::Ak,
			'location' => implode(',', array($latitude, $longitude)),
			'pois' => $pois,
			'output' => 'json',
		);
		
		// 请求百度api
		$response = self::toCurlPost(self::API, $param);
		
		$result = array();
		
		if($response){
			$result = json_decode($response, true);
		}
		
		return $result;
		
	}

	/**
	 * 根据地址获取国家、省份、城市及周边数据
	 * @param  Decimal $origins 起源的经纬度，用“,”隔开，例如"latitude,$longitude"
	 * @param  Decimal $destinations  终点的经纬度，用“,”隔开，例如"latitude,$longitude"
	 * @return Array
	 */
	public static function getDistance($origins='', $destinations=''){

		$param = array(
			'ak' => self::Ak,
			'output' => 'json',
			'origins' => $origins,
			'destinations' => $destinations,
		);

		// 请求百度api
		$response = self::toCurlGet(self::API1, $param);

		$result = array();

		if($response){
			$result = json_decode($response, true);
		}

		return $result;

	}

	/**
	 * 使用curl调用post请求
	 * @param  String $url    请求的地址
	 * @param  Array  $param  请求的参数
	 * @return JSON
	 */
	private static function toCurlPost($url, $param=array()){
//		dd($url.http_build_query($param));
		$ch = curl_init();

		if(substr($url,0,5)=='https'){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在
		}
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param));

		$response = curl_exec($ch);
		
		if($error=curl_error($ch)){
			return false;
		}
		
		curl_close($ch);
		
		return $response;
		
	}


	/**
	 * 使用curl调用get请求
	 * @param  String $url    请求的地址
	 * @param  Array  $param  请求的参数
	 * @return JSON
	 */
	private static function toCurlGet($url, $param=array()){
//		dd($url.http_build_query($param));
		$ch = curl_init();

		if(substr($url,0,5)=='https'){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在
		}

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url.http_build_query($param));
		curl_setopt($ch, CURLOPT_HEADER, 0);
//		curl_setopt($ch, CURLOPT_POST, true);
//		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param));

		$response = curl_exec($ch);

		if($error=curl_error($ch)){
			return false;
		}

		curl_close($ch);

		return $response;

	}
}
