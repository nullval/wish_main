<?php

namespace App\Models\Api;

use App\Exceptions\ApiException;
use App\Models\TdGiveUseTdLog;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ApiTdStatement extends Model
{
	protected $table = 'td_statement';

    public function __construct()
    {
    }

    /**
     * @param int $y_date
     * 创建刷pos机购买TD释放TD账单
     */
    public function create_new_statement($order_sn,$total_td,$uid){
        DB::beginTransaction();
        $num=5;
        //生成5条记录
        for($i=1;$i<=$num;$i++){
            $d=date('Y-m-d',strtotime(date('Y-m-d'))+$i*30*24*3600);
            $statement=ApiTdGiveUseTdLog::where(['u_date'=>$d,'user_id'=>$uid])->first();
            if(empty($statement->id)){
                $day_amount=$total_td/$num;
                $log['u_date']=$d;
                $log['user_id']=$uid;
                $log['created_at']=time2date();
                $log['status']=1;
                $log['amount']=$day_amount;
                $log['order_list']=$order_sn;
                $log['left_amount']=$day_amount;
                ApiTdGiveUseTdLog::insert($log);
            }else{
                $day_amount=$statement->amount+$total_td/$num;
                $u_date=$d;
                $log['updated_at']=time2date();
                $log['amount']=$day_amount;
                $log['status']=1;
                $log['order_list']=$statement->order_list.','.$order_sn;
                $log['left_amount']=$statement->left_amount+$total_td/$num;
                ApiTdGiveUseTdLog::where(['u_date'=>$u_date,'user_id'=>$uid])->update($log);
            }
        }
        DB::commit();
    }

    /**
     * @param int $is_excute 是否执行 0按标准日期执行 1不安标准日期执行
     */
    public function give_back_td(){
        //不用做事务
        $log_list=ApiTdGiveUseTdLog::where(['status'=>1,'u_date'=>date('Y-m-d')])->get(['id']);
        foreach ($log_list as $k=>$v){
            $this->deliver_td($v->id);
        }
    }

    /**
     * @param $statement_id 对账单id
     * @param $amount 额度
     * @return bool
     * @throws ApiException
     * 根据对账单释放td
     */
    public function deliver_td($log_id){
        DB::beginTransaction();
        $log_info=ApiTdGiveUseTdLog::where(['id'=>$log_id])->first();
        if($log_info->status==1){
            $day_amount=$log_info->left_amount;
            $uid=$log_info->user_id;
            //变换用户额度
            ApiTdUser::where(['id'=>$uid])->increment('use_td',$day_amount);
            ApiTdUser::where(['id'=>$uid])->decrement('freeze_td',$day_amount);
            //写日志
            $log['status']=2;
            $log['updated_at']=time2date();
            $log['left_amount']=0;
            ApiTdGiveUseTdLog::where(['id'=>$log_info->id])->update($log);
        }
        DB::commit();
        return true;
    }

    public function in_time($date){
        for($i=1;$i<=5;$i++){
            $arr[$i]=date('Y-m-d',strtotime($date)+$i*30*24*3600);
        }
        if(in_array(date('Y-m-d'),$arr)){
            return true;
        }
        return false;
    }
}
