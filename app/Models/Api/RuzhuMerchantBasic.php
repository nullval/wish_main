<?php

namespace App\Models\Api;

use App\Models\CommonModel;
use App\Models\VOrder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RuzhuMerchantBasic extends Model
{
	protected $table = 'ruzhu_merchant_basic';

	/**
	 * 查询商家列表
	 * @param $lat 纬度
	 * @param $lng 经度
	 * @param $area_id 行政代码 若不写则显示所在城市的所有商家
	 * @param $sort  1、销量最高 2、距离最近 ，3、获取全部商家列表，4、与$name配合使用(当$name为空，就是不搜索)，不写则按默认排序
	 * @param string $area_code 行政代码
	 * @param string $page 页码
	 * @param string $name 搜索商家名字的内容
	 * @return array
	 */
	public function get_seller_list($lat = '22.533055',$lng ='113.948765',$area_id = null,$area_code,$sort = null,$name = null,$page = 1){
		new_logger('Alliance.log','模型方法get_seller_list参数:',['lat'=> $lat,'lng'=> $lng,'area_id'=> $area_id,'area_code'=> $area_code,'sort'=> $sort,'name'=> $name]);

		//判断排序顺序
		$map = [
			'1'		=>  'volume desc',
			'2'		=>  'Distance_value asc',
		];
		if ($sort == 1 || $sort == 2){
			$orderby = $map[$sort];
		}else{
			$orderby = 'b.id desc';
		}

		//分页准备
		if($page < 1){
			$page = 1;
		}
		//每页的记录数量
		$page_size = 10;
		//起始位置
		$offset = ($page - 1) * $page_size;

		//获取当前所在地区的商家列表
		$seller_list = DB::table('ruzhu_merchant_basic as b') -> where(function ($query) use ($area_id,$area_code,$sort,$name){
			if ($sort == 3){
				return $query->where('b.id','<>',null);
			}elseif(!empty($name)){
				return $query->where('b.merchantName','like','%'.$name.'%');
			}elseif(empty($name) && $sort == 4){
				return $query->where(['b.id' => null]);
			}elseif(!empty($area_id) && !empty($area_code)){
				return $query->where(['b.area_id' => $area_code]);
			}elseif(empty($area_id) && empty($area_code)){
				return $query->where('b.id','<>',null);
			}
		})
			-> select('b.id','b.merchantName','b.uid','b.lat','b.lng','b.logoimage',DB::raw("ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN(($lat * PI() / 180 - lat * PI() / 180) / 2),2) + COS($lat * PI() / 180) * COS(lat * PI() / 180) * POW(SIN(($lng * PI() / 180 - lng * PI() / 180) / 2),2))) * 1000) AS Distance_value"),DB::raw("(`b`.`volume` + c.num) AS volume"),DB::raw("round((`b`.`average` + IFNULL(c.sum / c.num,0)),2) AS average"))
			-> leftJoin(DB::raw("(SELECT b.id,COUNT(v.uid) AS num,SUM(v.amount) AS sum FROM ruzhu_merchant_basic AS b LEFT JOIN v_order AS v ON v.uid = b.uid GROUP BY b.id ORDER BY b.id DESC) AS c"),'c.id','=','b.id')
			-> orderByRaw($orderby)
			-> offset($offset)
			-> limit($page_size)
			-> get();
		foreach ($seller_list as $item => $value){
			if ($value -> Distance_value < 1000){
				if ($value -> Distance_value != 0){
					$value -> Distance_text = $value -> Distance_value.'m' ;
				}else{
					$value -> Distance_text = '0m';
				}
			}else{
				$value -> Distance_text = round($value -> Distance_value/1000, 2).'km' ;
			}
		}
		return $seller_list;
	}

}
