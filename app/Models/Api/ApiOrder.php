<?php

namespace App\Models\Api;

use App\Exceptions\ApiException;
use App\Jobs\SendMessage;
use Illuminate\Database\Eloquent\Model;

class ApiOrder extends Model
{
	protected $table = 'order';

    public function __construct($year=0,array $attributes = [])
    {
        parent::__construct($attributes);
        $year=empty($year)?date('Y'):$year;
        $this->table='order_'.$year;
    }

    /**
     * @param $mobile
     * @param $terminalId
     * @return mixed
     * @throws ApiException
     * 创建刷pos机购买td订单
     */
    public function create_td_order($mobile,$terminalId){
        $pos_info=ApiTdPos::where(['terminalId'=>$terminalId])->first();
        if(empty($mobile)){
            throw new ApiException('购买人手机号码不能为空');
        }
        if (!preg_match("/^1[345789]\d{9}$/", $mobile)){
            throw new ApiException('请输入正确的手机号码');
        }
        //pos机判断
        if(!isset($pos_info->id)){
            throw new ApiException('该未特商城pos机不存在');
        }
        if(empty($pos_info->admin_id)){
            throw new ApiException('该未特商城pos机未绑定管理员,无法使用');
        }
        //用户添加
        $user_info=ApiTdUser::where(['mobile'=>$mobile])->first();
        if(isset($user_info->id)){
            $data['buyer_id']=$user_info->id;
        }else{
            $data['pos_remark']=$mobile;
        }

        $order_sn='td'.create_order_no(time());
        $data['terminalId']=$terminalId;
        $data['created_at']=time2date();
        $data['order_sn']=$order_sn;
        //未特商城pos机所属管理员
        $data['agent_id']=$pos_info->admin_id;
        $data['type']=6;
        $rs=$this->insert($data);
        if($rs){
            return $data;
        }else{
            throw new ApiException('订单生成失败');
        }
    }
}
