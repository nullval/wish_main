<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;

class ApiAgentOperate extends Model
{
	protected $table = 'agent_operate';

    /**
     * @param $operate_id
     * @return int
     * 统计运营中心推荐的商家总数
     */
    public function count_seller($operate_id){
        $count=ApiPushQrQrcode::where(['pushQr_qrcode.status'=>2,'pushQr_qrcode.operate_id'=>$operate_id])
            ->where(['r1.status'=>3])
            ->leftJoin('ruzhu_merchant_basic as r1','r1.uid','=','pushQr_qrcode.seller_id')
            ->count();
        return $count;
    }

    /**
     * @param $operate_id
     * @return float
     * 地推人员地推商家扫码消费后，运营中心能获得的点数 推荐的商家必须300家以上
     * 3 3% 6 4% 9 5% 24以上 10%封顶
     */
    public function pushQr_profit_point($operate_id){
        $point=2.5;
        $count=$this->count_seller($operate_id);
        if($count==300){
            $operate_count=$this->where(['parent_id'=>$operate_id])->count();
            if($operate_count==3){
                $point=3;
            }elseif($operate_count==6){
                $point=4;
            }elseif($operate_count==9){
                $point=5;
            }elseif($operate_count>=24){
                $point=10;
            }
        }
        return $point/100;
    }

}
