<?php
namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Support\Facades\DB;

class TdPos extends CommonModel {
    protected $table = 'td_pos';

    public  function add_pos($id,$terminalId,$mobile){
        if(!empty($mobile)){
            $admin_id=TdUser::where(['mobile'=>$mobile,'is_admin'=>1])->value('id');
            if(empty($admin_id)){
                return arr_post(0,'该管理员不存在');
            }
            $pos_arr['admin_id']=$admin_id;
        }
        if(empty($id)){
            if(empty($terminalId)){
                return arr_post(0,'终端号不能为空');
            }
            $pos_info=$this->where(['terminalId'=>$terminalId])->first();
            if(isset($pos_info->id)){
                return arr_post(0,'该Pos机已存在');
            }
            $pos_arr['terminalId']=$terminalId;
            $pos_arr['created_at']=time2date();
            $this->insert($pos_arr);
        }else{
            $pos_arr['updated_at']=time2date();
            $this->where(['id'=>$id])->update($pos_arr);
        }
        return arr_post(1,'成功');
    }
}