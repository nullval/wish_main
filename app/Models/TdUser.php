<?php
namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Support\Facades\DB;

class TdUser extends CommonModel {
    protected $table = 'td_user';


    public function login($mobile,$password){
        $user_info=$this->where(['mobile'=>$mobile])->first();
        if(!isset($user_info->id)){
            return arr_post(0,'用户不存在');
        }
        if($user_info->password!=md5(md5($mobile.$password).'td')){
            return arr_post(0,'密码不正确');
        }elseif($user_info->status==0){
            return arr_post(0,'您被禁止登录');
        }
        session(['td_user_info'=>$user_info]);
        return arr_post(1,'登录成功');
    }

    /**
     * 重新设置密码
     * @param $mobile 手机号
     * @param $password 新密码
     * @param $password_confirmation 确认密码
     * @param $sms_code 短信验证码
     * @return mixed
     */
    public function set_password($mobile,$password,$password_confirmation,$sms_code){
        //短信验证
        $Message = new Message();
         $Message->check_sms($mobile,$sms_code,18);
        //判断新密码与确认密码是否一致
        if ($password != $password_confirmation){
            return arr_post(0,'新密码与确认密码不一致');
        }
        //判断用户是否存在
        $user_info = $this -> where(['mobile' => $mobile]) -> first();
        if(!isset($user_info -> id)){
            return arr_post(0,'用户不存在');
        }elseif($user_info->status == 0){
            return arr_post(0,'您被禁止登录，所以无法重设密码');
        }
        //验证通过，开始重设密码
        $result = $this -> where(['mobile' => $user_info -> mobile]) -> update(['password' => md5(md5($mobile.$password).'td')]);
        if ($result == 0){
            return arr_post(0,'密码重设失败');
        }
        return arr_post(1,'恭喜您重设密码成功,欢迎您的再一次登录！');
    }

    /**
     * 修改密码
     * @param $old_password 旧密码
     * @param $new_password 新密码
     * @param $new_password_confirmation 确认密码
     * @return mixed
     */
    public function edit_password($old_password,$new_password,$new_password_confirmation)
    {
        $user = session('td_user_info');
        $user_info = $this->where(['mobile' => $user->mobile])->first();
        //判断新密码与确认密码是否一致
        if ($new_password != $new_password_confirmation) {
            return arr_post(0, '新密码与确认密码不一致');
        }
        //判断用户是否存在
        if (!isset($user_info->id)) {
            return arr_post(0, '用户不存在');
        }
        //判断旧密码是否输入正确
        if ($user_info->password != md5(md5($user_info->mobile . $old_password) . 'td')) {
            return arr_post(0, '旧密码不正确，请重新输入正确的密码');
        }

        //验证通过，开始修改密码
        $result = $this->where(['mobile' => $user_info->mobile])->update(['password' => md5(md5($user_info->mobile . $new_password) . 'td')]);
        if ($result == 0) {
            return arr_post(0, '修改密码失败');
        }
        return arr_post(1, '恭喜您修改密码成功！');
    }
    /**
     * @param $mobile 管理员手机号码
     * @param $remarks 备注
     * 添加/编辑管理员 密码为空不修改
     */
    public function add_manager($mobile,$password,$remarks=''){
        $user_info=$this->where(['mobile'=>$mobile,'is_admin'=>1])->first();
        if(isset($user_info->id)){
            return arr_post(0,'该管理员已存在');
        }
        if(!empty($password)){
            $arr['password']=md5(md5($mobile.$password).'td');
        }
        $arr['mobile']=$mobile;
        $arr['remarks']=$remarks;
        $arr['is_admin']=1;
        $arr['created_at']=time2date();
        $arr['updated_at']=time2date();
        $this->where(['mobile'=>$mobile])->updateOrCreate($arr);
        return arr_post(1,'添加成功');

    }
}