<?php

namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserPurse extends Model
{
    protected $table = 'wish_user_purse';

    public function getPurseStatistics(){
        $purse_type=[
            '1'=>'收益钱包',
            '3'=>'积分钱包',
            '4'=>'货款钱包',
            '7'=>'商家充值钱包',
            '10'=>'消费积分',
            '10001'=>'商家待补贴积分'
        ];
        //初始化用户身份
        $data=[
            '1'=>[
                'name'=>'用户'
            ], //用户
            '2'=>[
                'name'=>'商家'
            ], //商家
            '5'=>[
                'name'=>'机主'
            ], //机主
            '11'=>[
                'name'=>'运营中心'
            ] //运营中心
        ];

        foreach ($data as $k=>$v){
            foreach ($purse_type as $k1=>$v2){
                $where=$this->where(['owner_type'=>$k,'purse_type'=>$k1])->where(function($query) use($k1,$k){
                    if($k1==4&&$k==2){
                        //商家货款排除自家
                        return $query->where('owner_id','<>',54);
                    }
                    if($k1==7&&$k==2){
                        //商家收益排除自家
                        return $query->where('owner_id','<>',54);
                    }
                    if($k1==1&&$k==3){
                        //用户积分排除系统管理员
                        return $query->where('owner_id','<>',116);
                    }
                });
                $data[$k][$k1]['balance']=$where->sum('balance');
                $data[$k][$k1]['freeze_value']=$where->sum('freeze_value');
                $data[$k][$k1]['balance']=$data[$k][$k1]['balance']-$data[$k][$k1]['freeze_value'];
            }
        }
        $data1['purse_type_list']=$purse_type;
        $data1['data']=$data;
        return $data1;
    }

    /*
     *@param $option_id 操作员id
     *@param $option 操作类型 1兑换 2转让 兑换:积分购买为TD币 转让:TD币出卖为积分 (1减少 2增加)
     *@param $user_id 用户id
     *@param $amount 兑换/转让额度 消费额度
     *@param $type  1兑换/转让 2积分商城消费扣除 3积分退还
     *@param $arr  备用数组 可传订单编号
     * 积分兑换转让操作
     */
    public function change_point($option_id,$option,$user_id,$amount,$type=1,$arr=[]){
        if($amount<0){
            throw new ApiException('积分额度不能为0');
        }
        $Bank=new Bank();
        //判断用户是否有足够的积分
        if($option==1){
            $user_amoun=$Bank->use_money($user_id,3,1);
            if($user_amoun<$amount){
                throw new ApiException('积分额度不够');
            }
        }
        if(empty($arr[0])){
            throw new ApiException('订单编号不能为空');
        }
        $rs=[];
        $Bank=new Bank();
        $user_mobile=User::where(['id'=>$user_id])->first(['mobile'])->mobile;
        //积分钱包
        $point_purser_id=$Bank->userWallet($user_id,3,1)->purse_id;
        //系统积分钱包
        $sys_purse_id=$Bank->get_sys_purse_id(3);
        //积分商城积分钱包
        $j_purse_id=$Bank->get_d_purse(3,10)->purse_id;
        if($type==1){
            //兑换/转让
            if($option==1){
                //兑换
                $rs[]=$this->j_to_t_action(1,$user_mobile,$amount,$arr[0],$point_purser_id);
            }elseif($option==2){
                //转让
                $rs[]=$this->t_to_j_action(1,$user_mobile,$amount,$arr[0],$point_purser_id,0,1,$user_id);
            }
        }elseif($type==2){
            //消费扣除
            if($option==1){
                $remark=$user_mobile.'用户用积分兑换了商品,订单编号'.$arr[0].',积分消耗了'.$amount;
                //兑换
                $rs[]=$Bank->doApiTransfer($point_purser_id,$j_purse_id,$amount,10029,
                    $remark,
                    '积分兑换商品');
            }
        }elseif($type==3){
            //积分退还
            $remark=$user_mobile.'用户积分退还,积分增加了'.$amount.',订单编号'.$arr[0];
            //转让
            $rs[]=$Bank->doApiTransfer($sys_purse_id,$point_purser_id,$amount,10030,
                $remark,
                '积分退还');
        }
        return true;
    }


    /*
     *@param $option_id 操作员id
     *@param $option 操作类型 1兑换 2转让 兑换:积分购买为CHQ币 转让:CHQ币出卖为积分 (1减少 2增加)
     *@param $user_id 用户id
     *@param $amount 兑换/转让额度 消费额度
     *@param $type  1兑换/转让 2积分商城消费扣除 3积分退还
     *@param $arr  备用数组 可传订单编号
     *@param $give_type  1给积分 2给可提现的增值积分(收益)(只有option=2才使用)
     * 积分兑换转让操作 (商家转让)
     */
    public function change_seller_point($option_id,$option,$user_id,$amount,$type=1,$arr=[],$mobile,$give_type=1){
        $seller_info=Seller::where(['mobile'=>$mobile])->first(['id']);
        if(!isset($seller_info->id)){
            throw new ApiException('不存在该商家');
        }
        $seller_uid=$seller_info->id;
        if($amount<0){
            throw new ApiException('积分额度不能为0');
        }
        $Bank=new Bank();
        //判断用户是否有足够的积分
        if($option==1){
            $user_amoun=$Bank->use_money($seller_uid,3,2);
            if($user_amoun<$amount){
                throw new ApiException('积分额度不够');
            }
        }
        if(empty($arr[0])){
            throw new ApiException('订单编号不能为空');
        }
        $rs=[];
        $Bank=new Bank();
        //用户积分钱包
        $point_purse_id=$Bank->userWallet($user_id,3,1)->purse_id;
        //商家积分钱包
        $point_seller_purse_id=$Bank->userWallet($seller_uid,3,2)->purse_id;
        if($type==1){
            //兑换/转让
            if($option==1){
                //兑换
                //商家兑换 商家->用户->系统
                $rs[]=$this->j_to_t_action(2,$mobile,$amount,$arr[0],$point_purse_id,$point_seller_purse_id);
            }elseif($option==2){
                //转让
                //商家转让 系统->用户->商家
                $rs[]=$this->t_to_j_action(2,$mobile,$amount,$arr[0],$point_purse_id,$point_seller_purse_id,$give_type,$user_id);
            }
        }
        return true;
    }

    /**
     * @param int $member_type 用户身份
     * @param $mobile 用户手机号码
     * @param $amount 兑换额度
     * @param $order_sn 订单编号
     * @param int $point_purse_id 用户身份积分钱包 $member_type==0,1,2必填
     * @param int $point_seller_purse_id 商家身份积分钱包 $member_type=2必填
     * 积分兑换CHQ
     */
    public function j_to_t_action($member_type=1,$mobile,$amount,$order_sn,$point_purse_id=0,$point_seller_purse_id=0){
        DB::transaction(function () use($member_type,$mobile,$amount,$order_sn,$point_purse_id,$point_seller_purse_id){
            $Bank=new Bank();
            $sys_point_purse_id=$Bank->get_sys_purse_id(3);
            if($member_type==1||$member_type==0){
                //用户身份
                //兑换
                $remark=$mobile.'用户用积分兑换CHQ,积分消耗了'.$amount.',订单编号'.$order_sn;
                $rs[]=$Bank->doApiTransfer($point_purse_id,$sys_point_purse_id,$amount,10026,
                    $remark,
                    '积分兑换CHQ');
            }elseif($member_type==2){
                //商家身份
                //兑换
                //商家兑换 商家->用户->系统
//            $rs[]=$Bank->doApiTransfer($point_seller_purse_id,$point_purse_id,$amount,10062,
//                $mobile.'商家用积分转让给用户身份用于兑换CHQ,订单编号'.$order_sn,
//                '商家转让积分给用户');
//            $rs[]=$Bank->doApiTransfer($point_purse_id,$sys_purse_id,$amount,10063,
//                $mobile.'商家用积分兑换CHQ,订单编号'.$order_sn,
//                '积分兑换CHQ');
                $rs[]=$Bank->doApiTransfer($point_seller_purse_id,$sys_point_purse_id,$amount,10063,
                    $mobile.'商家用积分兑换CHQ,订单编号'.$order_sn,
                    '积分兑换CHQ');
            }
        });

        return true;
    }

    /**
     * @param int $member_type 用户身份
     * @param $mobile 用户手机号码
     * @param $amount 兑换额度
     * @param $order_sn 订单编号
     * @param int $point_purse_id 用户身份积分钱包 $member_type==0,1,2必填
     * @param int $point_seller_purse_id 商家身份积分钱包 $member_type=2必填
     * @param $give_type  1给积分 2给可提现的增值积分(收益) $member_type=2才用
     * @param $user_id  用户id $member_type=1才用
     * TD转让积分
     */
    public function t_to_j_action($member_type=1,$mobile,$amount,$order_sn,$point_purse_id=0,$point_seller_purse_id=0,$give_type=1,$user_id=0){
        DB::transaction(function () use($member_type,$mobile,$amount,$order_sn,$point_purse_id,$point_seller_purse_id,$give_type,$user_id,&$t_point){
            $Bank=new Bank();
            //系统积分钱包
            $sys_point_purse_id=$Bank->get_sys_purse_id(3);
            if($member_type==1||$member_type==0){
                //用户身份
                //转让
                //2018/04/09 0.1给到消费积分 0.7给到积分
                $Bank->doApiTransfer($sys_point_purse_id,$point_purse_id,$amount,10027,
                    $mobile.'用户用CHQ转让积分,积分增加了'.$amount.',订单编号'.$order_sn,
                    'CHQ转让积分');

//                $Bank->doApiTransfer($sys_point_purse_id,$point_purse_id,$amount,10027,
//                    $mobile.'用户用CHQ转让积分,积分增加了'.$amount.',订单编号'.$order_sn,
//                    'CHQ转让积分');
            }elseif($member_type==2){
                //商家身份 转让 直接给收益
                //转让
                //商家转让(给积分) 系统->用户->商家
                if($give_type==2){
                    //系统收益钱包
                    $sys_purse_id=$Bank->get_sys_purse_id(1);
                    //商家收益钱包
                    $seller_uid=Seller::where(['mobile'=>$mobile])->value('id');
                    $seller_purse_id=$Bank->userWallet($seller_uid,1,2)->purse_id;
                    $Bank->doApiTransfer($sys_purse_id,$seller_purse_id,$amount,10071,
                        $mobile.'商家用CHQ转让获得收益,订单编号'.$order_sn.'。',
                        '商家获得收益');
                }
            }
        });
        //2018/04/08 0.1自动挂单
        //2018/04/14 0.2回购CHQ
//        if($member_type==1&&$t_point>1){
//            $JavaIncrement = new JavaIncrement();
//            $JavaIncrement->automatic_guadan($mobile,$t_point,$user_id,3,$order_sn);
//        }
        return true;
    }


    /**
     * @param $mobile 机主手机号码/运营中心手机号码
     * @param $amount 金额
     * @param $reason 流水状态码
     * @param $detail 流水详细
     * @param $remarks 流水备注
     * @param $is_api 是否走api接口事务
     * @param $pursetype 积分类型
     * 为机主/运营中心添加积分
     */
    public function give_agent_point($mobile,$amount,$reason ,$detail,$remarks,$pursetype=3){
        $user_info=User::where(['mobile'=>$mobile])->first();
        if(!isset($user_info->id)){
            //没有注册
            $uid=User::insertGetId(['mobile'=>$mobile,'created_at'=>time2date()]);
        }else{
            $uid=$user_info->id;
        }
        $Bank=new Bank();
        //获取机主用户积分钱包
        $purse_id=$Bank->userWallet($uid,$pursetype,1)->purse_id;
        //获取系统积分钱包
        $sys_purse_id=$Bank->get_sys_purse_id($pursetype);
        $rs=$Bank->doApiTransfer($sys_purse_id,$purse_id,$amount,$reason ,$detail,$remarks);
        return $rs;
//        echo var_dump($rs);exit;
    }

    public function back_seller_point(){
        ignore_user_abort(true);
        //允许长时间执行
        set_time_limit(0);
        $Bank=new Bank();
        $this->where(['purse_type'=>10001,'owner_type'=>2])->where('balance','>',0)->get()
        ->each(function ($v) use($Bank) {
            $back_amount=$v->balance*0.002;
            $seller_point_purse_id=$Bank->userWallet($v->owner_id,3,2,['purse_id'])->purse_id;
            $Bank->doApiTransfer($v->purse_id,$seller_point_purse_id,$back_amount,10115,
                '商家补贴积分释放,当前待释放总额度为'.$v->balance,
                '商家补贴积分释放');
        });
    }
}
