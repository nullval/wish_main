<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class ShopTSsoSession  extends Model {
    public $table = 'tsso_session';
    public $connection = 'mysql2';

    /*
     * 通过Ticket来获取请求用户身份
     */
    public function getTicket($ticket) {
        return $this->where(['ticket' => $ticket])->first();
    }

}