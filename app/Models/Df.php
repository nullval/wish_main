<?php
namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Support\Facades\DB;

class Df extends CommonModel {

    /**
     * @param $bank_name银行名称
     * @return mixed
     * 通过银行卡名获取银行代码
     */
    public function getbankcode($bank_name){
        //调用代付平台的查询银行代码的接口
        $arr['bank_name']=$bank_name;
        $arr['_cmd']='pos_getBankCodeByName';
        $arr['username']=env('MERCHANT');
        $arr['password']=env('CRYPTOGRAM');
        $arr['code']=env('DF_CODE');
        $arr['timestamp']=time();
        $sign=get_df_pos_sign($arr);
        $arr['sign']=$sign;
        return curl_post(env('DF_DOMAIN'),$arr);
    }

    
}