<?php

namespace App\Models;

use App\Exceptions\ApiException;
use App\Libraries\JPush\JPush;
use Illuminate\Database\Eloquent\Model;

class OrderQrcode extends Model
{
	protected $table = 'order_qrcode';

    /**
     * @param $order_sn 订单编号
     * @param string $terminalId 通知的终端号
     * 生成小票扫码
     */
	public function create_qrcode($order_sn,$terminalId=''){
        $order_info=VOrder::where(['order_sn'=>$order_sn])->first();
        if(!isset($order_info->id)){
             throw new ApiException('订单不存在');
//           new throw ('订单不存在');
        }elseif($order_info->type!=3){
            throw new ApiException('订单类型不正确');
        }
        $qrcode_id=$this->where(['order_sn'=>$order_sn])->value('id');
	    if(empty($qrcode_id)){
            $qrcode_id=$this->insertGetId([
                'order_sn'=>$order_sn,
                'created_at'=>time2date(),
                'status'=>1
            ]);
        }
        $url=base64_encode(config('config.tdzd_channel_url').'orderQrcode/index?id='.$qrcode_id);
        $info='关注未特商城获取CHQ';
        $data=['url'=>$url,'info'=>$info];
//        if(!empty($terminalId)){
//            $extras = [
//                'data'=> json_encode(['type' => 2,'data' => ['url'=>$url,'info'=>$info]])
//            ];
//            $JPush = new JPush(1);
//            $JPush->push_tags($terminalId,'二维码获取通知','二维码获取通知',$extras);
//        }
        return $data;
    }
}
