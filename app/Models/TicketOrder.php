<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TicketOrder extends Model
{
	protected $table = 'ticket_order';

	/**
	 * 新增发票
	 * @param array $data
	 * @return array
	 */
	public function createorderr($data=array()){
		$order_list=$data['order_list'];
		$amount=$data['amount'];
		$address_id=$data['address_id'];
		$agent_uid=session('agent_info')->id;
		//判断是否有选地址
		if (empty($address_id)){
			return arr_post(0,'请选择收件地址，如没有地址选择，请先去添加地址！');
		}
		//根据地址ID获取相应的地址信息
		$address_info=TicketAddress::where(['id'=>$address_id,'agent_uid'=>$agent_uid])->first();
		//获取发票信息ID
		$info=TicketInfo::where(['agent_uid'=>$agent_uid])->first();
		//判断是否有发票信息
		if (empty($info)){
			return arr_post(0,'由于要用发票抬头等信息生成发票，请先去添加发票信息！');
		}
		$info_id=$info->id;
		//开始事务：生成发票
		DB::beginTransaction();

		//插入发票详情记录，获取相应记录的ID
		$arr['agent_uid']=$agent_uid;
		$arr['Invoice_nature']=1;
		$arr['created_at']=time2date();
		$arr['recipient_name']=$address_info['recipient_name'];
		$arr['status']=1;
		$arr['postalcode']=$address_info['postalcode'];
		$arr['address']=$address_info['address'];
		$arr['mobile']=$address_info['mobile'];
		$arr['order_list']=$order_list;
		$arr['amount']=$amount;
		$arr['isvalid']=1;
		$rs[]=$detail= TicketDetail::insert($arr);

		//插入发票订单数据
		$detail_info=TicketDetail::where(['agent_uid'=>$agent_uid,'order_list'=>$order_list])->first();
		$order['agent_uid']=$agent_uid;
		$order['info_id']=$info_id;
		$order['created_at']=time2date();
		$order['detail_id']=$detail_info['id'];
		$rs[]=$R = $this->insert($order);

		//改变发票的发票生成状态
		$order_list_length=substr($order_list,0,strlen($order_list)-1);
		$orders = explode(",",$order_list_length);
		foreach ($orders as $v){
			$order_info=explode("_",$v);
			$order_id=$order_info[0];
			$year=$order_info[1];
			$Order=new Order($year);
			$result=$Order->where(['id'=>$order_id,])->update(['is_make'=>1]);
			if ($result==0){
				$rs[]=false;
			}

		}

		if(checkTrans($rs)){
			DB::commit();
			//session存储代理信息
			return arr_post(1,'新增发票成功！');
		}else{
			DB::rollBack();
			return arr_post(0,'新增发票失败，请重新选择订单生成发票！');
		}
	}
}
