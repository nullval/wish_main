<?php
namespace App\Models;
use App\Exceptions\ApiException;
use Illuminate\Support\Facades\DB;


class AdminRole extends CommonModel{
    protected $table = 'wish_admin_role';
    //总公司ID
    const ROLE_CORPORATION = 1;
    //分公司ID
    const ROLE_COMPANY =2;
    //一级代理
    const ROLE_AGENT_1=3;
    //二级代理
    const ROLE_AGENT_2=4;
    //三级代理
    const ROLE_AGENT_3=5;
    //四级代理
    const ROLE_AGENT_4=6;
    //五级代理
    const ROLE_AGENT_5=7;
    //店铺直推人
    const ROLE_GENERALIZE=8;
    //店铺
    const ROLE_SELLER=9;

    /**
     * 获取身份类型字段
     * @return int
     */
    public function getIdentityTypeAttribute(){
        switch ($this->attributes['id']){
            case self::ROLE_SELLER:
                $identity = '商家';
                break;
            case self::ROLE_GENERALIZE:
                $identity = '直推人';
                break;
            case self::ROLE_AGENT_1:
            case self::ROLE_AGENT_2:
            case self::ROLE_AGENT_3:
            case self::ROLE_AGENT_4:
            case self::ROLE_AGENT_5:
                $identity = '代理';
                break;
            case self::ROLE_COMPANY:
                $identity = '分公司';
                break;
            case self::ROLE_CORPORATION:
                $identity = '总公司';
                break;
            default:
                $identity = 'null';
        }
        return $identity;
    }

    /**
     * 根据代理等级返回角色ID
     * @param $level
     * @return  int
     */
    public static function getAgentRoleIdByAgentLevel($level){
        switch ($level){
            case 1:
                return self::ROLE_AGENT_1;
            case 2:
                return self::ROLE_AGENT_2;
            case 3:
                return self::ROLE_AGENT_3;
            case 4:
                return self::ROLE_AGENT_4;
            case 5:
                return self::ROLE_AGENT_5;
        }
    }


    /*
     * @param $role_name 角色名称
     * @param $permission 权限列表 以,隔开
     * @param $role_id 角色id
     * 添加管理员
     */
    public function add_role($role_name,$permission,$role_id){
        $role_arr['role_name']=$role_name;
        $role_arr['permission']=$permission;
        if(!empty($role_id)){
            //修改
            $role_arr['updated_at']=time2date();
            $rs=$this->where(['id'=>$role_id])->update($role_arr);
        }else{
            //添加
            $role_arr['created_at']=time2date();
            $rs=$this->insert($role_arr);
        }
        if(!$rs){
            throw new ApiException('处理失败');
        }
        return true;
    }

    public function scopeDetail($query,$admin_id,$field=['*'])
    {
        return $query->where(['id'=>$admin_id])->first($field);
    }



    /**
     * 得到分红比例
     */
    public function admin_dividend(){
        return $this->hasOne('App\Models\AdminDividend','admin_role_id');
    }




}