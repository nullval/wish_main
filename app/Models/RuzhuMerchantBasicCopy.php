<?php

namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RuzhuMerchantBasicCopy extends Model
{
	protected $table = 'wish_ruzhu_merchant_basic_copy';

	/**
	 * 更新/新增商家入驻临时信息
	 * @param $request_info
	 * @param $type 1、新增 2、更新
     * @param $agent_mobile 推荐人手机号码
	 * @return mixed
	 */
	public function add_seller($request_info,$type,$agent_mobile=''){

		//判断商家手机号与法人手机号是否一致
		if($request_info['mobile']!=$request_info['corpmanMobile']){
            throw new ApiException('商家手机号必须与法人手机号一致');
		}

		//判断该身份下是否已存在
		$user_info=Seller::where(['mobile'=>$request_info['mobile']])->first();
		if ($type == 1){
			if(isset($user_info->id)){
                throw new ApiException('该手机号已被注册,请勿重复注册');
			}
		}elseif ($type == 2){
			if(!isset($user_info->id)){
                throw new ApiException('该手机号还未注册，请先去入驻，才能编辑信息');
			}
		}
        $agent_id = 0;
        if(!empty($agent_mobile)){
            $agent_id=Agent::where(['mobile'=>$agent_mobile])->value('id');
            if(empty($agent_id)){
                throw new ApiException('推荐人不存在');
            }
        }

		//判断终端号
//        if($agent_mobile!='18820298599'){
//            $pos = Pos::where(['terminalId' => $request_info['terminalId']]) -> first();
//            if (empty($pos)){
//                throw new ApiException('不好意思，无此终端号的POS机，请重新填写终端号！');
//            }
//            //判断该POS机是否属于该代理的
//            if ($pos -> agent_uid != $agent_id){
//                throw new ApiException('不好意思，此终端号的POS机属于其他人，请填写属于自己的POS机的终端号！');
//            }
//            //判断是否绑定过商家
//            $seller = RuzhuMerchantBasicCopy::where(['terminalId' => $request_info['terminalId']]) -> first();
//            if (!empty($seller)){
//                throw new ApiException('不好意思，此终端号的POS机已经绑定其他商家了，请重新填写终端号！');
//            }
//            $pos1 = Pos::where(['terminalId' => $request_info['terminalId']]) -> first();
//            if ($pos1 -> uid != 0){
//                throw new ApiException('不好意思，此终端号的POS机已经绑定其他商家了，请重新填写终端号！');
//            }
//        }
		//判断是选择银行
		if (empty($request_info['bankCode'])){
            throw new ApiException('还未选择银行！');
		}
		//判断是否有选省市区
		if (empty($request_info['province10']) || empty($request_info['city10'])){
            throw new ApiException('请选择省市区');
		}

		//判断是不是直辖市的郊区,选择区域编号
		if (empty($request_info['district10'])){
			$area_id = $request_info['city_code'];
		}else{
			$area_id = $request_info['district_code'];
		}

		//检查是否上传图片并返回图片地址
		if(!empty($request_info['businessimagebase64'])){
			$result=upload($request_info['businessimagebase64']);
			if($result['code']==0){
                throw new ApiException($result['message']);
			}else{
				$request_info['businessimage']=$result['data']['filepath'];
			}
		}elseif (empty($request_info['businessimagebase64']) && $type == 1){
            throw new ApiException('请上传营业执照');
		}


		//做事务
		DB::beginTransaction();
		//1、更新/新增商家
		if ($type == 1){
			//新增商家
            $reg['inviter_id']=$agent_id;
//            $reg['qr_user_id']=$qr_user_id;
			$reg['mobile']=$request_info['mobile'];//商家手机
			$reg['created_at']=time2date();
			$reg['last_time']=time2date();
			$reg['is_bind_phone']=2; //是否绑定手机
			$reg['child_merchant_no']=$request_info['corpmanMobile']; //高汇通子商户号
			//插入商家表
			$rs[]=$user_id=Seller::insertGetId(filter_update_arr($reg));



		}elseif ($type == 2){
			$user_id = $user_info->id;
		}

		//2、更新/新增商家基本临时信息
		$basic_info = [
			'merchantName'			=> $request_info['merchantName'],	// 店名，要与营业执照上一致
			'shortName'				=> mb_substr($request_info['merchantName'] , 0 , 5,'utf-8'), //简称
			'city'					=> $request_info['city'],		// 城市id
			'merchantAddress'		=> $request_info['merchantAddress'],	// 城市中文名
			'servicePhone'			=> $request_info['servicePhone'],			// 客服电话
			'merchantType'			=> '01',		// 00公司，01个体
			'category'				=> $request_info['category'],		// 类别id
			'corpmanName'			=> $request_info['corpmanName'],		// 法人姓名
			'corpmanId'				=> $request_info['corpmanId'],	// 法人身份证
			'corpmanMobile'			=> $request_info['corpmanMobile'],	// 法人联系手机
			'bankCode'				=> $request_info['bankCode'], //银行代码
			'bankName'				=> $request_info['bankName'], //开户行全称
			'bankaccountNo'			=> $request_info['bankaccountNo'], //开户行账户
			'bankaccountName'		=> $request_info['corpmanName'], //开户行户名
			'businessimage'		=> $request_info['businessimage'], //营业执照图片
			'businessLicense'		=> $request_info['businessLicense'], //营业执照
			'autoCus'				=> 0,		// 0：不自动提现 1：自动提现
			'remark'				=> '商家入驻申请',
			'uid'				=> $user_id,
			'merchantId'				=> $request_info['corpmanMobile'], //子商户号
			'status'				=> 2,
			'area_id'				=> $area_id,
			'lat'				=> $request_info['lat'],  //纬度
			'lng'				=> $request_info['lng'],  //经度
			'terminalId'				=> $request_info['terminalId'],  //POS机终端号
		];

		//商家基本信息登记临时表更新/插入数据
		$basic = $this ->where(['merchantId'=>$user_info->child_merchant_no]) -> first();
		if ($type == 1 || empty($basic)){
			$basic_info['create_time'] = time2date();
			$rs[]=$basic_id=$this->insertGetId(filter_update_arr($basic_info));

			//新增
            //添加管理员
            $Admin=new Admin();
            $admin_id = $Admin->add_admin($reg['mobile'],$request_info['password'],AdminRole::ROLE_SELLER);
            //添加商家与推荐人的关系
            $profitRelation = new ProfitRelation();
            //添加会员账户
            $profitRelation->addRelation($admin_id,AdminRole::ROLE_SELLER,$basic_id,$agent_id);

		}elseif ($type == 2 && !empty($basic)){
			$basic_info['updated_at'] = time2date();
			$result=$this->where(['merchantId'=>$user_info->child_merchant_no])->update(filter_update_arr($basic_info));
			$basic_id = $basic->id;
			$rs[] = true;

			//更新管理员关系
            $admin = new Admin();

            echo 113;exit;

		}

		//3、更新/新增商家银行信息
		$bank_info = [
			'basic_id'		=> $basic_id, //基本信息id
			'merchantId'		=> $request_info['corpmanMobile'], //子商户号
			'bankCode'			=> $request_info['bankCode'],  //银行代码
			'bankaccProp'		=> '0',		// 账号属性 0私人，1公司
			'name'				=> $request_info['corpmanName'], //持卡人
			'bankaccountNo'		=> $request_info['bankaccountNo'], //银行账号
			'bankaccountType'	=> $request_info['bankaccountType'], //银行卡类型
			'certCode'			=> '1',			// 1身份证，2护照，3军官证，4回乡证，5台胞证，6港澳通行证，7国际海员证，8外国人永久居住证，9其他
			'certNo'			=> $request_info['corpmanId'], //证件号
			'remark'		=> '商家银行卡信息登记',		// 备注
//			'create_time'		=> time2date(),
			'bankbranchNo'		=> $request_info['ibankno'], //联行号
			'bankName'			=> $request_info['bankName'],  //银行名称
		];
		$bank = RuzhuMerchantBankCopy::where(['merchantId'=>$user_info->child_merchant_no])->first();
		//更新/插入高汇通银行信息临时表
		if ($type == 1 || empty($bank)){
			$bank_info['created_at'] = time2date();
			$basic_info['create_time'] = time2date();
			$rs[]=RuzhuMerchantBankCopy::insert(filter_update_arr($bank_info));
		}elseif ($type == 2 && !empty($bank)){
			$bank_info['updated_at'] = time2date();
			$result=RuzhuMerchantBankCopy::where(['merchantId'=>$user_info->child_merchant_no])->update(filter_update_arr($bank_info));
			$rs[] = true;

		}


		//4、更新/新增业务临时信息
        $Businesss_info1 = [
            'basic_id'		=> $basic_id,
            'merchantId'		=> $request_info['corpmanMobile'],//子商户号
            'handleType'		=> 0,
            'cycleValue'		=> 0,
            'allotFlag'		=> 0,
            'futureRateValue'		=> 0,//费率
            'futureMinAmount'	=> 0,
            'futrueMaxAmount'			=> 0,
            'remark'			=> '开通商家所有业务',// 备注
            'benefit'		=> 1,
//            'create_time'		=> time2date(),
        ];
		$Businesss = RuzhuMerchantBusinesssCopy::where(['merchantId'=>$user_info->child_merchant_no])->first();
		if ($type == 1 || empty($Businesss)){
			$Businesss_info1['created_at'] = time2date();
			$basic_info['create_time'] = time2date();
			$rs[]=RuzhuMerchantBusinesssCopy::insert(filter_update_arr($Businesss_info1));
		}elseif ($type == 2 && !empty($Businesss)){
			$Businesss_info1['updated_at'] = time2date();
			$result=RuzhuMerchantBusinesssCopy::where(['merchantId'=>$user_info->child_merchant_no])->update(filter_update_arr($Businesss_info1));
			$rs[] = true;
		}

        if(checkTrans($rs)){
			DB::commit();
            return $user_id;
		}else{
			DB::rollBack();
            throw new ApiException('商家入驻失败');
		}
	}
}
