<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class TnetReginfo  extends Model {
    public $table = 'tnet_reginfo';

    //根据手机号查询用户的nodeid
    public function getNodeIdByMobile($mobile){
        return $this->where('nodecode',$mobile)->first();
    }

}