<?php
namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Support\Facades\DB;

class Transfer extends CommonModel {
	protected $table = 'wish_transfer';
	#用户获取游戏币 1群管理费(我的群群成员发红包) 2我的推荐(推荐人发红包) 3.领取红包
	#用户支出游戏币 1兑换商品 2兑换 3发红包

    public function __construct($year=0,array $attributes = [])
    {
        parent::__construct($attributes);
        $year=empty($year)?date('Y'):$year;
        $this->table='transfer_'.$year;
    }

	/*
	 * $purse_id 钱包id
	 * $reason 转账代码 字符串则单一类型 数组则多种类型
	 * 获取某类型流水金额总和
	 */
	public function scopeSumAmount($query,$purse_id,$reason='')
	{
		$arr['sum']=$query
			->where(['into_purse_id'=>$purse_id])
			->when($reason,function ($query) use ($reason,$purse_id) {
				if(is_array($reason)){
					return $query->whereIn('reason',$reason);
				}else{
					return $query->where(['reason'=>$reason]);
				}
			})->sum('into_amount');
		return $arr;
	}

	/*
	 * $purse_id 钱包id
	 * $reason 转账代码
	 * $is_into_or_out 1收入 2支出
	 * $reason 数组 [10200,10500]
	 * 获取某类型流水明细
	 */
	public function scopeList($query,$purse_id,$field=['*'],$reason=[],$is_into_or_out=0)
	{
		$data=$query
			->leftJoin('wish_transfer_reason', 'wish_transfer_reason.reason_code', '=', 'wish_transfer.reason')
			->where(function ($query) use ($is_into_or_out,$purse_id) {
				//判定是收入或者支出流水
				if($is_into_or_out==1){
					return $query->where(['into_purse_id'=>$purse_id]);
				}elseif($is_into_or_out==2){
					return $query->where(['out_purse_id'=>$purse_id]);
				}else{
					return $query->where(['into_purse_id'=>$purse_id])
						->orWhere(['out_purse_id'=>$purse_id]);
				}
			})
			->when($reason, function ($query) use ($reason) {
				return $query->whereIn('reason', $reason);
			})
			->orderBy('transfer_id','desc')
			->select($field);
		return $data;
	}



	/**
	 * @param $query
	 * @return $purse_id 钱包id
	 * 格式化分页列表，取出对应的图片信息
	 */
	public function scopePagesParse($query,$purse_id){
		$data = $query->pages();
		foreach($data as $k=>$v){
			if(!empty($v['out_amount'])){
				$data[$k]['out_amount']=get_last_two_num($v['out_amount']);
				if($purse_id==$data[$k]['out_purse_id']){
					$data[$k]['out_amount']='-'.$data[$k]['out_amount'];
				}
			}

			unset($data[$k]['into_purse_id'],$data[$k]['out_purse_id']);
		}
		return $data;
	}


    /**
     * 判断是否有余额
     * @param $date  包括提现的金额和钱包ID
     * @return bool
     */
    public function IsEnough($purse_id,$amount){

        //转出总金额
        $outamount=$this->get_total_by_purse_id($purse_id,'out');

        //转入总金额
        $intoamount=$this->get_total_by_purse_id($purse_id,'into');


        //冻结总金额
        $freezehis=new FreezeHis();
        $freezeamount=$freezehis->GetAoumt($purse_id);

        //是否具有提现资格
        $sum_aoumt=$intoamount-$outamount-$freezeamount;

        if ($sum_aoumt<$amount){
            return false;
        }
        return true;

    }

    /**
     * @param $purse_id
     * @param string $type out or into
     * 统计某一钱包总收入或总支出
     */
    public function  get_total_by_purse_id($purse_id,$type='out'){
        $sum=0;
        for ($i=2017;$i<=date('Y');$i++){
            $sum+=DB::table('wish_transfer_'.$i)->where([$type.'_purse_id'=>$purse_id])->sum($type.'_amount');
        }
        return $sum;
    }


}