<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018/6/21
 * Time: 10:14
 */

namespace App\Models;


use App\Exceptions\ApiException;

class SellerTicket extends CommonModel
{

    /**
     * 商户后台用户TICKET
     *
     * @var string
     */
    protected $table = 'seller_ticket';

    /**
     * 根据用户ID登录操作，存入并返回ticket
     *
     * @param $uid
     * @return bool|string
     * @throws ApiException
     */
    public function getTicket($uid)
    {
        $ticket = uuid();
        $add = [
            'uid' => $uid,
            'session_id' => session_id(),
            'ticket' => $ticket,
            'status' => 0,
            'ip' => request()->ip(),
            'client_system' => request('ClientSystem'),
            'client_version' => request('ClientVersion'),
            'api_version' => request('ApiVersion', '1.0'),
            'timeout' => request()->server('REQUEST_TIME', time()) + 3600 * 3,
        ];
        $var = $this->create($add);
        if(!empty($var)){
            return $ticket;
        }else{
            throw new ApiException('获取票据异常');
        }
    }

    /**
     * 登出操作，设置状态为已下线
     *
     * @param $uid // 如果传递就全部下线，否则只退出当前ticket
     *
     * @return bool
     */
    public function logout($uid = 0)
    {
        $ticket = request('ticket');
        if ($uid) {
            $var = $this->where(['uid' => $uid])->update(['status' => 2]);
        } else {
            $var = $this->where(['ticket' => $ticket])->update(['status' => 2]);
        }
        return $var;
    }

    /**
     * 根据ticket查询状态
     *
     * @return mixed
     * @throws ApiException
     */
    public function checkLogin()
    {
        $ticket = request('ticket');
        $exist = $this->where(['ticket' => $ticket])->first();
        if (empty($exist)) {
            throw new ApiException('用户凭证验证失败');
        } elseif ($exist->status == 2) {
            throw new ApiException('用户凭证已失效');
        } elseif ($exist->status == 0) {
            $user = Seller::where(['id' => $exist->uid])->first();
            return $user;
        } else {
            throw new ApiException('用户凭证数据状态无效');
        }
    }


}