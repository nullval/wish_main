<?php
namespace App\Models;
use App\Exceptions\NotPermissionException;
use App\Scopes\PermissionIsHideScope;
use Illuminate\Support\Facades\DB;


class AdminPermission extends CommonModel{
    protected $table = 'wish_admin_permission';

    protected static function boot()
    {
        parent::boot();
        //添加全局作用域，涉及这个模型都会加is_list判断这个条件
        static::addGlobalScope(new PermissionIsHideScope());
    }

    /**
     * @param string $permission_list 权限列表 不传则获取所有权限列表
     * @return array
     * 格式化菜单
     */
    public function sort_permission($permission_list=''){
        if(empty($permission_list)){
            $permission_list=$this->get();
        }
        return $this::GetTree($permission_list,0,0);
    }

    private function GetTree($arr,$pid,$step){
        global $tree;
        foreach($arr as $key=>$val) {
            if($val['pid'] == $pid) {
                $flg = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;└--',$step);
                $val['flg_name'] = $flg.$val['name'];
                $val['level']=$step;
                $tree[] = $val;
                $this->GetTree($arr , $val['id'] ,$step+1);
            }
        }
        return $tree;
    }

    public function check_admin_permission(){
        $prefix=request()->route()->getAction()['prefix'];
        $prefix=substr($prefix,1,strlen($prefix));
        $route =request()->route()->getAction()['controller'];
        $action=explode('@', $route)[1];
        //权限验证
        $is_true=0;
        $permission_list=session(session('admin_info')->id.'_permission_list');
        $permission_info=$this->where(['c'=>$prefix,'a'=>$action])->get();
        //只有存在权限列表里面才做验证
        if(!$permission_info->isEmpty()){
            if($prefix){
                foreach($permission_list as $k=>$v){
                    if($v['c']==$prefix&&$v['a']==$action){
                        $is_true=1;
                    }
                }
                if($is_true==0){
                    throw  new NotPermissionException();
                }
            }
        }
    }

}