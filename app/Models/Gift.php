<?php

namespace App\Models;


use App\Exceptions\ApiException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Gift  extends  Model
{
    protected $table = 'gift';
    protected $cache_name = 'chqt_gift_';
    protected $gift_list_name = 'chqt_gift_list_';
    protected $cache_arr_name = 'chqt_gifts';
    private $page_size = 15;

    /*
     * 检查是否有可领取的红包
     * @param $uid 用户id
     * @param $gift_id 红包id
     * @param $lat 纬度
     * @param $lng 经度
     * @parma $step 步长 1度(111公里) 0.45 五公里 //$lat = '22.533055', $lng = '113.948765'
     * @return int 可领取的红包信息
     */
    public function check_gift($uid = 0, $lat = '22.533055', $lng = '113.948765', $step = 0.045)
    {
        //红包数据
        $redis = app('redis.connection');
        $result = [];
        if (!$redis->exists($this->cache_arr_name)) {
            $data = $this->where(['gift.status' => 1])->where('gift.left', '>', 0)->where('gift.expire_time', '>', time())->orderBy('gift.id', 'asc')
                ->get(['gift.id', 'gift.lat', 'gift.lng', 'gift.name', 'gift.expire_time', 'gift.amount']);
            //进行转换
            foreach ($data as $k => $v) {
                //判断redis队列是否有红包
                $num = $redis->llen($this->gift_list_name . $v->id);
                if ($num > 0)
                    $result[] = ['id' => $v->id, 'lat' => $v->lat, 'lng' => $v->lng, 'mername' => $v->name, 'expire_time' => $v->expire_time];
            }
            $redis->set($this->cache_arr_name, json_encode($result));
        } else {
            $result=$redis->get($this->cache_arr_name);
            $result=json_decode($result,true);
        }

        //删除空值
        array_filter($result);
        if ($result) {
            foreach ($result as $k => $val) {
                $num = $redis->llen($this->gift_list_name . $v->id);
                //检测红包，把不符合条件的红包剔除,
                if ($this->checkdata($val)&&$num>0) {
                    //未过期
                    if ($val['lat'] < ($lat + $step) && $val['lat'] > ($lat - $step) && $val['lng'] < ($lng + $step) && $val['lng'] > ($lng - $step)) {
                        //检查是否领取过红包
                        $check = DB::table('gift_his')->where(['gift_id' => $val['id'], 'uid' => $uid])->first();
                        if (empty($check)) {
                            $return = $val;
                            break;
                        }
                    }
                } else {
                    $redis->del($this->cache_arr_name);
                }
            }
        }
        return $return;
    }

    public function  rmcache() {
        $redis = app('redis.connection');
        if ($redis->exists($this->cache_arr_name)) {
             $redis->del($this->cache_arr_name);
        }
        $result = $this->where(['gift.status' => 1])->where('gift.expire_time', '>', time())->get();
        foreach ($result as $key => $val) {
            //清理红包缓存
            $num = $redis->llen($this->gift_list_name . $val->id);
            if ($num > 0) {
                for ($i = 0; $i < $num; $i++) {
                    $redis->lpop($this->gift_list_name . $val->id);
                }
            }
        }
    }

    /*
    * 打开红包
    * @param $uid 领取用户id
    * @param $giftid 领取红包id
    * @return int 领取情况
    */
    public function open_gift($uid = 0, $giftid = 0, $lat = '', $lng = '', $step = 0.045) {
        $redis = app('redis.connection');
        if ($redis->llen($this->gift_list_name . $giftid) == 0) {
            throw new ApiException('红包已抢完');
        }
        //检查是否领取过红包
        $check = DB::table('gift_his')->where(['gift_id' => $giftid, 'uid' => $uid])->first();
        if (!empty($check))
            throw new ApiException('已领取过红包');
        $gift = $this->getgift($giftid);
        //判断经纬度
        if ($gift->lat < ($lat + $step) && $gift->lat > ($lat - $step) && $gift->lng < ($lng + $step) && $gift->lng > ($lng - $step)) {
            $money = $redis->lpop($this->gift_list_name . $giftid);
            return DB::transaction(function () use ($uid, $gift, $money, $lat , $lng) {
                $mark = false;
                //打开红包
                $gif_his_id = DB::table('gift_his')->insertGetId(
                    ['gift_id' => $gift->id, 'status' => 2, 'amount' => $money * 100, 'uid' => $uid, 'created_at' => time2date(), 'updated_at' => time2date(),'lat'=>$lat,'lng'=>$lng]
                );
                if ($gif_his_id)
                    $mark = true;
                $result = DB::table('gift')->where(['id' => $gift->id])->decrement('left', $money * 100);
                if ($result) {
                    $mark = true;
                }
                if ($mark) {
                    //转账业务
                    $mark = false;
                    $Bank = new Bank();
                    //获取系统红包收益钱包
                    $sys_purse_id = $Bank->get_sys_purse_id(12);
                    $purse_id = $Bank->userWallet($uid, 12, 1, ['purse_id'])->purse_id;
                    if ($purse_id) {
                        //异步队列处理
                        $Bank->doApiTransfer($sys_purse_id, $purse_id, $money, 10013, '拆红包获得红包收益', '获得红包收益', 0, false);
                        $mark = true;
                    }
                }
                if ($mark) {
                    //查询商家信息
                    $merchant = DB::table('ruzhu_merchant_basic')->where(['uid' => $gift->merchant_id])->first();
                    return ['money' => sprintf("%.2f", $money), 'id' => $gif_his_id, 'status' => 1, 'gid' => $gift->id, 'name' => $gift->name, 'mername' => $merchant->merchantName];
                } else {
                    throw new ApiException('红包已抢完');
                }
            });
        } else {
            throw new ApiException('红包已抢完');
        }
    }
    private function checkdata($val)
    {
        //删除过期的
        if ($val['expire_time'] <= time()) {
            return false;
        }
        //删除已抢完的
//        $left = $this->getLeft($val['id']);
//        if ($left < 1) {
//            return false;
//        }
        return true;
    }

    /*
     * 红包领取记录
    * @param $giftid 用户id
    * @param $page  页码
    * @return array 领取记录
     */
    public function gifthis($uid, $page)
    {
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $this->page_size;
        $data = DB::table('gift_his')->where(['gift_his.uid' => $uid])->leftJoin('gift', 'gift_his.gift_id', '=', 'gift.id')
            ->limit($this->page_size)->offset($offset)->orderBy('gift_his.id', 'desc')->get(['gift_his.id', DB::raw('round(gift_his.amount/100,2) as money'), DB::raw('date_format(gift_his.created_at,\'%Y-%m-%d %H:%I\') as gettime'), 'gift.name as giftname', DB::raw('round(gift.amount/100,2) as amt')]);
        return $data;
    }

    /*
     * 获取单个红包记录
     */
    public function getgift($giftid)
    {
        $gift = $this->where(['id' => $giftid, 'status' => 1])->first();
        if (empty($gift))
            throw new ApiException('红包记录不存在');
        if ($gift->expire_time < time()) {
            throw new ApiException('红包已过期');
        }
        if ($gift->left < 0) {
            throw new ApiException('红包已抢完');
        }
        return $gift;
    }


    /*
     * 通过$ticket参数获取user信息
     */
    public function getUser($ticket = '') {
        $key = $ticket;
//        $redis = app('redis.connection');
//        if (!$redis->exists($key)) {
        $model = new ShopTSsoSession();
        $info = $model->getTicket($key);
        if (empty($info))
            return json_error('未能找到您的身份凭证信息');
        unset($model);
        $user = $this->getUserbyNodeid($info->node_id);
        if (empty($user)) {
            return json_error('未能找到您的身份信息');
        }
//            $redis->setex($key,300, json_encode(['id'=>$user->id,'nodeid'=>$info->nodeid]));
//        } else {
//            $user = $redis->get($key);
//        }
        return $user;
    }


    /*
         * 通过nodeid获取user信息
         */
    public function getUserbyNodeid($nodeid)
    {
        return DB::table('user')->where(['nodeid' => $nodeid])->first();
    }


    /**
     * @param $amount 以分为单位
     * @param $merchant_id
     * @param $lat
     * @param $lng
     * @return int
     * 添加红包 (插入数据库和缓存中)
     */
    public function create_gift(int $amount, $merchant_id, $lat, $lng, $name)
    {
        if ($amount >= 1) {
            $data = [
                'merchant_id' => $merchant_id,
                'lat' => $lat,
                'lng' => $lng,
                'maxamt' => 20,//z最大2毛钱
                'left' => $amount,
                'amount' => $amount,
                'expire_time' => time() + 3600 * 24,
                'created_at' => time2date(),
                'name' => $name
            ];
            $id = Gift::insertGetId($data);
            if ($id) {
                $moneys=$this->getRedPackage($id, $amount / 100, 30, 0.01);
                Gift::where(['id' => $id])->update(['moneys' => json_encode($moneys)]);
                $redis = app('redis.connection');
                $redis->del($this->cache_arr_name);
            }
            return $id;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     * 删除缓存里的红包记录
     */
    public function del_redis_gift($id) {
        $redis = app('redis.connection');
        //删除缓存
//        if (Cache::store('redis')->has($key)) {
//            Cache::store('redis')->forget($key);
//        }
        if ($redis->exists($this->cache_arr_name)) {
            $redis->del($this->cache_arr_name);
        }
        $num = $redis->llen($this->gift_list_name . $id);
        if ($num > 0) {
            for ($i = 0; $i < $num; $i++) {
                $redis->lpop($this->gift_list_name . $id);
            }
            $redis->lrem($this->gift_list_name . $id,0,0);
        }
        return true;
    }

    /**
     * @return bool
     * 系统回收过期红包
     */
    public function recovery_redpack()
    {
        $this->where('left', '>', 0)->where('expire_time', '<', time())->get()->each(function ($v, $k) { 
            $id = $v->id;
            $left = $v->left;
            DB::transaction(function () use ($id, $left) {
                $Bank = new Bank();
                //获取平台收益钱包
                $sys_purse_id = $Bank->get_sys_purse_id(1);
                //获取平台红包收益钱包
                $sys_redpack_purse_id = $Bank->get_sys_purse_id(12);
                //系统回收
                $Bank->doApiTransfer($sys_redpack_purse_id, $sys_purse_id, $left / 100, 10101,
                    '系统回收剩余红包收益,编号:' . $id,
                    '系统回收剩余红包收益');
                Gift::where(['id' => $id])->update(['left' => 0, 'status' => 2]);
            });
            //删除队列
            $this->del_redis_gift($id);
        });
        return true;
    }


    /*
     * 获取生成红包函数
     * @param $id 红包记录id
     * @param $money 红包总额 单位元
     * @param $num 红包个数
     * @param $min 最小红包金额 单位元
     * @return 生产红包金额数据 为空则无效红包
     */
    function getRedPackage($id,$money, $num, $min)
    {
        //将最大金额  设为红包总数
        $max = $money;
        $data = array();
        //最小金额*数量  不能大于  总金额
        if ($min * $num > $money) {
            return $data;
        }

        //最大金额*数量  不能大于  总金额
        if ($max * $num < $money) {
            return $data;
        }
        //如果红包数量为1  直接返回总数
        if ($num == 1) {
            $data[] = $money;
            return $data;
        }
        $redis = app('redis.connection');
        while ($num >= 1) {
            $num--;
            $kmin = max($min, $money - $num * $max);
            $kmax = min($max, $money - $num * $min);
            $kAvg = $money / ($num + 1);
            //获取最大值和最小值的距离之间的最小值
            $kDis = min($kAvg - $kmin, $kmax - $kAvg);
            //获取0到1之间的随机数与距离最小值相乘得出浮动区间，这使得浮动区间不会超出范围
            $r = ((float)(rand(1, 10000) / 10000) - 0.5) * $kDis * 2;
            $k = round($kAvg + $r, 2);
            $money -= $k;
            $log= $redis->lpush($this->gift_list_name . $id, $k);  // 返回列表长度 1
           // new_logger('getRedPackage.log','随机宏观',$log);
            $data[] = sprintf("%.2f", $k);
        }
        return $data;
    }



}