<?php
namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Support\Facades\DB;

//引入短信sdk
use App\Libraries\msg\REST;

class Message extends CommonModel {
	protected $table = 'message';
	protected $timeout;

	public function __construct(){
		//短信过期时间 10分钟
		$this->timeout=60*10;
	}

	/**
	 * @param $mobile 需要验证的手机号码
	 * @param $code 验证码
	 * @param $type 验证类型 1会员短信登录 2机主短信登录 3机主注册成功 4机主绑定银行卡通知 10系统异常短信通知 11安全码短信 12短信提现管理员提现成功 13购买未特商城pos机短信通知 18td用户忘记密码短信验证
	 * 短信验证
	 */
	public function check_sms($mobile,$code,$type=1){
	    //测试放过验证,正式服苹果审核
	    if(env('APP_DEBUG')==true&&$code=='8888' or $mobile=='18603070086'&&$code=='8888'){
            return true;
        }
        $where=['mobile'=>$mobile,'type'=>$type];
        $info=$this->where($where)->orderBy('id','desc')->first();
        if($code!=$info['code']){
            throw new ApiException('验证码不正确');
        }
        if(time()>$info['passtime']){
            throw new ApiException('验证码已经过期');
        }
        if($info['status']==0){
            throw new ApiException('验证码已失效');
        }
        //将同类型的所有验证码失效
        $rs=$this->where(['type'=>$info['type']])->update(['status'=>0]);
		return true;
	}

	/**
	 * @param $mobile 手机号码
	 * @param $type 验证类型 1会员短信登录 2机主短信登录 3机主注册成功 4机主绑定银行卡通知 5商家短信登录 6java专用 7商家短信注册 10系统异常短信通知 11安全码短信 12短信提现管理员提现成功 13购买未特商城pos机短信通知 14运营中心短信登录 15商家入驻申请 16短信通知 18td用户忘记密码短信验证
     * @param $spare1 备用字段
     * @param $arr 自定义参数
     * @param $is_limit 是否做限制 1是0否
	 * 短信发送
     * 請勿修改格式 謝謝
	 */
	public function send_sms($mobile,$type=1,$remark='',$spare1='',$arr=[],$is_limit=1){
        new_logger('send_sms.log','start',['date'=>date('Y-m-d H:i:s')]);
        if (!preg_match("/^1[345789]\d{9}$/", $mobile)){
            return json_error('请输入正确的手机号码');
        }
		//发送验证码不能少于一分钟
		$time=$this->where(['mobile'=>$mobile])->orderBy('id','desc')->pluck('time')->first();
		if($is_limit==1){
            if(time()<strtotime($time)+60){
                return json_error('发送验证码不能少于一分钟');
            }
        }
		$code=rand(100000,999999);
		//预留对接发送短信
        $REST=new REST();
        if($type==1){
            $message='验证码是'.$code.'，尊敬的会员,请您于10分钟内完成登录认证，请勿告知任何人！';
        }elseif($type==2){
            $message='验证码是'.$code.'，尊敬的机主,请您于10分钟内完成登录，请勿告知任何人！';
        }elseif($type==3){
            $message='亲，感谢您成为未特商城机主，请关注公众号（未特商城平台）进行激活。';
        }elseif($type==4){
            $message='亲，验证码是'.$code.'，正在进行银行卡绑定，请勿泄露给他人。';
        }elseif($type==5){
            $message='验证码是'.$code.'，尊敬的商家,请您于10分钟内完成登录，请勿告知任何人！';
        }elseif($type==6){
            $message=$remark;
        }elseif($type==7){
            $message='验证码是'.$code.'，尊敬的商家,请您于10分钟内完成注册，请勿告知任何人！';
        }elseif($type==10){
            $message='购买未特商城pos机订单金额异常,订单编号:'.$spare1;
        }elseif($type==11){
            $message='用户'.$spare1.'正在系统进行提现!安全码为'.$code.',十分钟内有效。';
        }elseif($type==12){
//            $message=$arr[0].'成功提现了'.$arr[1].'元,余额'.$arr[2].'元,到账'.$arr[3].'元,成功时间:'.$arr[4].'!';
            $message=$arr[0].'提现了'.$arr[1].'元,余额'.$arr[2].'元,时间:'.$arr[4].'!,代付余额:'.$arr[5];
        }elseif($type==13){
//            $message=$arr[0].'成功提现了'.$arr[1].'元,余额'.$arr[2].'元,到账'.$arr[3].'元,成功时间:'.$arr[4].'!';
            $message=$arr[0].'购买未特商城pos机支付了'.$arr[1].'元,时间:'.$arr[2].',订单编号:'.$arr[3];
        }elseif($type==14){
			$message='验证码是'.$code.'，尊敬的运营中心,请您于10分钟内完成登录，请勿告知任何人！';
		}elseif($type==15){
			$message='尊敬的客服,现在有商家已经完成入驻，请尽快去审核！';
		}elseif($type==16){
            $message=$remark;
        }elseif($type==17){
            $message='亲，恭喜您的验证码是'.$code.'，10分钟内有效。';
        }elseif($type==18){
			$message='亲，恭喜您的验证码是'.$code.'，请您于10分钟内完成重设密码，请勿告知任何人！';
		}

        $rs=$REST->send_msg($mobile,$message);

        new_logger('send_sms.log','发送短信结果',$rs);


		if($type==1){
			$remark='注册账号短信验证';
		}elseif($type==2){
			$remark='机主登录';
		}elseif($type==3){
            $remark='机主注册成功';
        }elseif($type==4){
            $remark='机主绑定银行卡';
        }elseif($type==5){
            $remark='商家登录';
        }elseif($type==6){
            $remark='java专用';
        }elseif($type==7){
            $remark='商家注册';
        }elseif($type==10){
            $remark=$remark;
        }elseif($type==11){
            $remark='安全码短信';
        }elseif($type==12){
            $remark='提现成功管理员通知';
        }elseif($type==13){
            $remark='购买未特商城pos机短信通知';
        }elseif($type==14){
			$remark='运营中心登录';
		}elseif($type==14){
			$remark='商家入驻申请';
		}elseif($type==16){
            $remark='短信通知';
        }elseif($type==16){
            $remark='验证码通知';
        }elseif($type==18){
			$remark='td用户忘记密码短信验证';
		}

		$add=[
			'mobile'=>$mobile,
			'code'=>$code,
			'type'=>$type,
			'time'=>time2date(),
			'remark'=>$remark,
			'passtime'=>time()+$this->timeout,
            'message'=>$message
		];
		$id = $this->insertGetId($add);
        return json_success('发送成功');
	}

    /**
     * @param $notify_arr
     * 短信通知管理员机主成功购买pos机
     */
	public function notify_buy_pos_success($notify_arr){
	    $mobile_arr=['18820298599'];
	    foreach ($mobile_arr as $k=>$v){
            $this->send_sms($v,13,'','',$notify_arr,0);
        }
    }

    /**
     * @param $notify_arr
     * 短信通知管理员提现成功
     */
    public function notify_withdraw_success($notify_arr){
        //13996337178财务 陈海燕
        $mobile_arr=['18820298599'];
        foreach ($mobile_arr as $k=>$v){
            $this->send_sms($v,12,'','',$notify_arr,0);
        }
    }
}