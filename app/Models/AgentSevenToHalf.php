<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentSevenToHalf extends Model
{
    //
	protected $guarded = [];
	protected $table = 'agent_seven_to_half';
	
	/**
	 * 新人进来就判断往上是不是7个人
	 * @param int $count 最大往上找的人数
	 * @return mixed
	 */
	public function parentSeven(int $count){
		$data = collect();
		$parent_id = $this->getAttribute('parent_id');
		if($parent_id == 1){
			$parents = collect([$this]);
		}else{
			$parents = $this->where(['parent_id'=>$parent_id])->get();
		}
		if($parents->isNotEmpty() && $count > 0){
			$parents->each(function($v) use (&$data,$count,$parents){
				$find = $this->where(['id'=>$v->parent_id])->first();
				if($find){
					$child = $find->child(true);
//					$child = $find->merge([$this]);
					$parents = $parents->merge($child);
					$data2 = $find->parentSeven($count - 1);
					$data = $data2->merge($parents)->unique('id')->sortBy('microtime')->values();
				}
			});
		}
		return $data;
	}
	
	/**
	 * 查找子集ss
	 */
	public function child($parent = false){
//		$return = collect();
		$id = $this->getAttribute('id');
		$parent_id = $this->getAttribute('parent_id');
		if($parent && $id == 1){
			$data = collect();
		}else{
			$data = $this->where(['parent_id'=>$id])->get();
		}
		
		if($data->isNotEmpty()){
			$data->each(function($v) use (&$data,$parent){
				$list = $v->child($parent);
				$data = $list->merge($data);
			});
		}
		return $data;
	}



    //查找树状结构，数据符合 orgchart前端插件格式 显示到20层
    public function tree_find($agent_id , $current=1 , $level=20 , &$result=[]){
        $agent_info = $this->where(['agent_id'=>$agent_id])->first();
        $agent_id=$agent_info->agent_id;
        $row = [
            'id'=> $agent_id,
            'name' => Agent::where(['id'=>$agent_id])->value('mobile'),
//            'title' => "",
            'className'=>'middle-level',
        ];
        $current++;
        if($current <= $level){
            $table_id=$agent_info->id;
            $child = $this->where(['parent_id'=>$table_id])->get();
            foreach($child as &$v){
                $row['children'][] = $this->tree_find($v->agent_id,$current,$level,$result);
            }
        }
        return $row;
    }

    /**
     * 查找根节点
     * @param $agent_id
     * 返回agent_id
     */
    public function find_root($agent_id){
        $agent_info = $this->where(['agent_id'=>$agent_id])->first(['agent_id','parent_id']);
        if(isset($agent_info->agent_id)){
            if($agent_info->parent_id!=1){
                return $this->find_root($this->where(['id'=>$agent_info->parent_id])->value('agent_id'));
            }
            return $agent_info->agent_id;
        }
        return 0;
    }
	
	/**
	 * @param $arr
	 * @param $child_name
	 * @param $level
	 * @return array
	 */
	public function childrenDeep($arr,$child_name = 'children',$level = 0){
		//存储结果的容器
		$result = array();
		//二维数组的一个元素数组
		$value = $arr[$level];
		//当前元素数组的个数
		$count = count($arr[$level]);
		//复制当前元素数组
		for ($i = 0;$i < $count;$i ++){
			$result[array_keys($value)[$i]] = $value[array_keys($value)[$i]];
		}
		//判断是否为当前新数组添加一个新元素
		if (isset($arr[$level+1])){
			$result[$child_name][] = $this->childrenDeep($arr,$child_name,$level+1);
		}else{
			$result[$child_name] = [];
		}
		return $result;
	}
}
