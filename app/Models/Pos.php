<?php

namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Pos extends CommonModel
{
    protected $table = 'pos';

    /**
     * 编辑/新增pos机
     */
    public function add_pos($id, $terminalId = '', $pos_type, $seller_mobile = '', $agent_mobile = '', $operate_mobile = '', $remark = '', $is_allow_edit = 0
        , $operate_level1 = 0, $operate_level2 = 0, $operate_level3 = 0, $is_direct_buy = 1)
    {
        $post = [
            'admin_id' => session('admin_info')->id,
            'remark' => $remark,
            'is_allow_edit' => $is_allow_edit,
            'operate_level1' => $operate_level1,
            'operate_level2' => $operate_level2,
            'operate_level3' => $operate_level3,
            'is_direct_buy' => $is_direct_buy
        ];
        if ($pos_type == 4 || $pos_type == 5) {
            //魔方要绑定商家,机主和运营中心 (初始化)
            $uid = Seller::where(['mobile' => $seller_mobile])->value('id');
            $agent_uid = Agent::where(['mobile' => $agent_mobile])->value('id');
            $agent_operate_id = AgentOperate::where(['mobile' => $operate_mobile])->value('id');
            if (!empty($seller_mobile)) {
                if (empty($uid)) {
                    throw new ApiException('商家不存在');
                }
                $post['uid'] = $uid;
            }
            if (empty($agent_uid)) {
                //没有则创建
                $agent_uid = Agent::insertGetId(['mobile' => $agent_mobile, 'created_at' => time2date()]);
//                throw new ApiException('机主不存在');
            }
            $post['agent_uid'] = $agent_uid;
            if (empty($agent_operate_id)) {
                throw new ApiException('运营中心不存在');
            }
            $post['agent_operate_id'] = $agent_operate_id;
        }
        if (empty($id)) {
            //检测终端号是否存在
            $pos_id = $this->where(['terminalId' => $terminalId])->value('id');
            if ($pos_id) {
                throw new ApiException('终端号已存在');
            }
            //新增pos机
            $post = array_merge($post, [
                'terminalId' => $terminalId,
                'pos_type' => $pos_type
            ]);
            $this->create($post);
        } else {
            //修改pos机 修改终端号和型号不让改
            $this->where(['id' => $id])->update($post);
        }
        return true;
    }


    public function pos_edit($data)
    {
        if (empty($data['seller_mobile'])) {
            //绑定该POS机
            Pos::where(['terminalId' => $data['terminalId']])->update(['uid' => 0]);
        } else {
            //判断商家是否存在 判断商家是否通过商家入驻
            $seller_id = Seller::where(['mobile' => $data['seller_mobile']])->value('id');
            if (empty($seller_id)) {
                throw new ApiException('该商家不存在，请先让该商家入驻！');
            }
            $status = MerchantApply::where(['uid' => $seller_id])->value('status');
            if ($status != 3) {
                throw new ApiException('该商家的商家入驻审核尚未通过,无法绑定！');
            }

            //判断商户是否绑定其他的机主
            $result = $this->where(['uid' => $seller_id])
                ->whereNotIn('pos.id', [$data['id']])
                ->whereNotIn('agent_uid', [session('agent_info')->id])
                ->leftJoin('agent as u1', 'u1.id', '=', 'pos.agent_uid')
                ->select(['pos.*', 'u1.mobile as agent_mobile'])
                ->first();

            if (!empty($result)) {
                throw new ApiException('该商家已经绑定机主' . $result['agent_mobile'] . '的pos机，无法绑定您的pos机了！');
            }

            //绑定该POS机
            Pos::where(['terminalId' => $data['terminalId']])->update(['uid' => $seller_id]);
        }
        return true;

    }

    /**
     * @param $mac mac地址
     * @param $pos_type 机子类型 1证通 2联迪 3新大陆
     * @param $dev 设备号
     * @return array
     * 通过mac地址生成终端号
     */
    public function add_pos_by_mac($mac, $pos_type, $dev)
    {
        if (!empty($mac)) {
            //兼容旧机器
            Pos::where(['mac' => $mac, 'pos_type' => $pos_type])->update(['dev' => $dev]);
        }
        //以机子类型和设备号做唯一标识
        $pos_info = Pos::where(['pos_type' => $pos_type, 'dev' => $dev])->first();
        if (isset($pos_info->id)) {
            return arr_post(1, '终端号已存在', ['terminalId' => $pos_info->terminalId, 'is_show_shop' => 0]);
        }
        if ($pos_type == 0) {
            $pos_type = 1;
        }
        if ($pos_type == 1) {
            //证通
            $pre = $pos_type . 'xdl';
        } elseif ($pos_type == 2) {
            //联迪
            $pre = $pos_type . 'ld';
        } elseif ($pos_type == 3) {
            //新大陆
            $pre = $pos_type . 'nxdl';
        }

        $terminalId = DB::transaction(function () use ($pre, $pos_type, $dev) {
            $pos_pre_info = PosPre::where(['pre' => $pre])->first();
            if (!isset($pos_pre_info->id)) {
                $pos_pre_data['pre'] = $pre;
                $pos_pre_data['num'] = 1;
                $pos_pre_data['created_at'] = time2date();
                $rs[] = PosPre::insert($pos_pre_data);
            } else {
                $pos_pre_data['pre'] = $pre;
                $pos_pre_data['num'] = $pos_pre_info->num + 1;
                $pos_pre_data['updated_at'] = time2date();
                $rs[] = PosPre::where(['id' => $pos_pre_info->id])->update($pos_pre_data);
            }
            $terminalId = $pos_data['terminalId'] = $pre . ($pos_pre_data['num'] + 100000000);
            $pos_data['pos_type'] = $pos_type;
            if (!empty($mac)) {
                $pos_data['mac'] = $mac;
            }
            $pos_data['dev'] = $dev;
            $pos_data['created_at'] = time2date();
            new_logger('add_pos_by_mac.log', '添加pos参数:', $pos_data);
            $rs[] = Pos::insert($pos_data);
            return $terminalId;
        });

        return arr_post(1, '生成成功', ['terminalId' => $terminalId, 'is_show_shop' => 0]);
    }


    /**
     * 获取POS机列表
     *
     * @param array $query
     */
    public function pos_list(array $query)
    {
        return $this->where($query)
            ->leftJoin('agent', 'pos.agent_uid', '=', 'agent.id')
            ->select(['agent.mobile', 'pos.*'])
            ->orderBy('pos.id', 'desc')
            ->paginate(15);
    }


    /**
     * 批量导入预处理
     * @param array $data
     * @return array
     */
    public function pre_add_pos(array $data)
    {
        $pre_key = "pre_" . time();
        $tmp = [];
        $terminalIds = [];
        foreach ($data as $v) {

            $data = [
//                'admin_id' => session('admin_info')->id,
                'remark' => $v[2],
//                'is_allow_edit' => $v[3],
//                'operate_level1' => $v[9],
//                'operate_level2' => $v[10],
//                'operate_level3' => $v[11],
                'terminalId' => $v[0],
                'seller_mobile' => $v[5],
                'agent_mobile' => $v[6],
                'operate_mobile' => $v[8]
            ];

            try {
                $data = array_merge($data, ['is_direct_buy' => $this->parseStr('is_direct_buy', $v[4])]);
            } catch (ApiException $e) {
                $data = array_merge($data, ['error' => $e->getMessage()]);
                array_push($tmp, $data);
                continue;
            }

            try {
                $data = array_merge($data, ['pos_type' => $this->parseStr('pos_type', $v[1])]);
            } catch (ApiException $e) {
                $data = array_merge($data, ['error' => $e->getMessage()]);
                array_push($tmp, $data);
                continue;
            }
            if ($data['pos_type'] == 4 || $data['pos_type'] == 5) {
                //魔方要绑定商家,机主和运营中心 (初始化)
                $uid = Seller::where(['mobile' => $data['seller_mobile']])->value('id');
                $agent_uid = Agent::where(['mobile' => $data['agent_mobile']])->value('id');
                $agent_operate_id = AgentOperate::where(['mobile' => $data['operate_mobile']])->value('id');
                if (!empty($seller_mobile)) {
                    if (empty($uid)) {
                        $data = array_merge($data, ['error' => '商家不存在']);
                        array_push($tmp, $data);
                        continue;
                    }
                    $data = array_merge($data, ['uid' => $uid]);
                }
                if (empty($agent_operate_id)) {
                    $data = array_merge($data, ['error' => '运营中心不存在']);
                    array_push($tmp, $data);
                    continue;
                }
                $post['agent_operate_id'] = $agent_operate_id;
            }
            //检测终端号是否存在
            $pos_id = $this->where(['terminalId' => $data['terminalId']])->value('id');
            if ($pos_id or in_array($data['terminalId'], $terminalIds)) {
                $data = array_merge($data, ['error' => '终端号已存在']);
                array_push($tmp, $data);
                continue;
            }
            //自动获取获取三级运营中心
            if ($data['is_direct_buy'] == 1) {
                //直接向公司购买 按时间找
                $data1 = AgentOperate::find_operate_level_by_time();
            } else {
                //不是按等级找
                $operate_info = AgentOperate::where(['mobile' => $data['operate_mobile']])->first();
                if (!isset($operate_info->id)) {
                    $data = array_merge($data, ['error' => '运营中心不存在']);
                    array_push($tmp, $data);
                    continue;
                }
                $data1 = AgentOperate::find_operate_level($operate_info->id);
            }
            foreach ($data1 as $key => $val) {
                $data['operate_level' . $key] = $val['id'];
            }
            array_push($terminalIds, $data['terminalId']);
            $data = array_merge($data, ['error' => '']);
            array_push($tmp, $data);
        }
        //加入5分钟缓存
        Cache::store('file')->put($pre_key, serialize($tmp), 5);
        return ['pre_key' => $pre_key, 'pre_data' => $tmp];
    }


    /**
     * 处理预处理订单
     * @param $pre_key
     * @return bool
     * @throws ApiException
     */
    public function handle_pre_add_pos($pre_key)
    {
        set_time_limit(120);
        if (empty($pre_key) or !Cache::store('file')->has($pre_key)) {
            throw new ApiException('预处理数据已失效,请重新上传');
        }
        $data = Cache::store('file')->get($pre_key);
        //删除缓存
        Cache::store('file')->forget($pre_key);
        $data = unserialize($data);
        $count = 0;
        foreach ($data as $v) {
            //跳过有错误的
            if (!empty($v['error'])) {
                continue;
            }
            DB::transaction(function () use ($v, &$count) {
                $Pos = new Pos();
                $Pos->add_pos(0,
                    $v['terminalId'],
                    $v['pos_type'],
                    $v['seller_mobile'],
                    $v['agent_mobile'],
                    $v['operate_mobile'],
                    $v['remark'],
                    0,
                    $v['operate_level1'],
                    $v['operate_level2'],
                    $v['operate_level3'],
                    $v['is_direct_buy']
                );
                $is_direct_buy = $v['is_direct_buy'];
                if ($is_direct_buy == 1) {
                    //公司直购的话,更新运营中心先后进入顺序
                    for ($i = 1; $i <= 3; $i++) {
                        $operate_id = $v['operate_level' . $i];
                        if (!empty($operate_id)) {
                            UsedOperate::updateOrCreate(['level' => $i], ['operate_id' => $operate_id]);
                        }
                    }
                }
                $count++;
            });
        }
        return $count;
    }

    /**
     * 格式化字符串
     *
     * @param $key
     * @param $str
     * @return false|int|string
     * @throws ApiException
     */
    private function parseStr($key, $str)
    {
        switch ($key) {
            case 'is_direct_buy':
                $tmp = [
                    0 => '市场部购买',
                    1 => '向公司购买',
                    2 => '商家自带'
                ];
                $key = array_search($str, $tmp);
                if ($key === false) {
                    throw new ApiException('是否公司直购非法');
                }
                break;
            case 'pos_type':
                $tmp = [
                    4 => '魔方',
                    5 => '银盛'
                ];
                $key = array_search($str, $tmp);
                if ($key === false) {
                    throw new ApiException('POS机型号非法');
                }
                break;
        }

        return $key;
    }


}