<?php
namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Support\Facades\DB;

class SysConfig extends CommonModel {
	protected $table = 'wish_sys_config';

	public function scopeAttrValue($query,$attr_name)
	{
		$data['attrValue']=$query->where(['attr_name'=>$attr_name])
			->first(['attr_value'])
			->attr_value;
		return $data;
	}

    public function get_value($attr_name)
    {
        return $this->where(['attr_name'=>$attr_name])->value('attr_value');
    }

    /**
     * @param $version 版本号
     * @param int $version_type app类型 1/0机主版本 2商家版
     * @param $type 接通的支付类型 1通联 2高汇通
     * @return bool
     * 检测pos机版本
     */
    public function check_pos_version($version,$version_type=0,$type){
	    //2017/12/15只开通高汇通(证通机)商家版本检测
	    //查询的版本字段
	    $attrName='';
	    if($version_type==1||$version_type==0){
            //机主版
            if($type==1){
                //通联
//                $attrName='pos_agent_version';
            }elseif($type==2){
                //高汇通 (证通机)
                $attrName='ght_pos_agent_version';
            }
        }elseif($version_type==2){
	        //商家版
            if($type==1){
                //通联
//                $attrName='pos_seller_version';
            }elseif($type==2){
                //高汇通 (证通机)
                $attrName='ght_pos_seller_version';
            }
        }elseif($version_type==3){
            //珍藏版
            if($type==1){
                //通联
                $attrName='pos_td_version';
            }elseif($type==2){
                //高汇通 (证通机)
                $attrName='ght_pos_td_version';
            }
        }
        if(!empty($attrName)){
            $pos_agent_version=SysConfig::attrValue($attrName)['attrValue'];
            if($version<$pos_agent_version){
                return false;
            }
        }
        return true;

    }


}