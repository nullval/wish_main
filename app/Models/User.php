<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;

class User extends Model
{
    protected $guarded = [];    // 列出想被批量复制保护的字段，取反即为都可以被批量复制，用于 firstOrCreate()、updateOrCreate() 等方法快捷增改
    protected $table = 'wish_user';

    /**
     * @param $mobile 商家手机号
     * @param $password 登录密码
     * @param $inviter_id 邀请人id
     * @return bool
     * 商家注册
     */
    public function seller_register($mobile, $password)
    {
        //判断该身份下是否已存在
        $user_info = $this->where(['mobile' => $mobile])->first(['id']);
        if (isset($user_info->id)) {
            return arr_post(0, '该手机号已被注册商家');
        }
        $reg['mobile'] = $mobile;
        $reg['password'] = md5(md5($password) . 'tdzd');
        $reg['created_at'] = time2date();
        $reg['last_time'] = time2date();
        $reg['is_bind_phone'] = 2;
        $user_info = DB::transaction(function () use ($reg) {
            //1插入用户表
            $rs[] = $user_id = $this->insertGetId(filter_update_arr($reg));
            $user_info = $this->where(['id' => $user_id])->first();
            //2插入商家申请表
            $rs[] = MerchantApply::insert(['uid' => $user_info->id]);
            return $user_info;
        });
        //session存储代理信息
        session(['agent_info' => $user_info]);
        return arr_post(1, '注册成功');

    }


    /**
     * @param $mobile 商家手机号
     * @param $password 登录密码
     * @return bool
     * 商家后台登录
     */
    public function seller_login($mobile, $password)
    {
        $user_info = $this->where(['mobile' => $mobile])->first();
        if (empty($user_info)) {
            return arr_post(0, '用户不存在');
        } else {
            if ($user_info->status == 0) {
                return arr_post(0, '您被禁止登录');
            }
            if (md5(md5($password) . 'sjlm') != $user_info->password) {
                return arr_post(0, '密码不正确');
            } else {
                $this->where(['id' => $user_info->id])->update(['last_time' => time2date()]);
                //session存储用户信息
                session(['seller_info' => $user_info]);
                //登录成功记录信息
                return arr_post(1, '登录成功');
            }
        }
    }

    /**
     * @param $mobile 商家手机号
     * @param $password 登录密码
     * @return bool
     * 业务员后台登录
     */
    public function agent_login($mobile, $password)
    {
        $user_info = $this->where(['mobile' => $mobile])->first();
        if (empty($user_info)) {
            return arr_post(0, '用户不存在');
        } else {
            if ($user_info->status == 0) {
                return arr_post(0, '您被禁止登录');
            }
            if (md5(md5($password) . 'tdzd') != $user_info->password) {
                return arr_post(0, '密码不正确');
            } else {
                $this->where(['id' => $user_info->id])->update(['last_time' => time2date()]);
                //session存储用户信息
                session(['agent_info' => $user_info]);
                //登录成功记录信息
                return arr_post(1, '登录成功');
            }
        }
    }

    /**
     * @param $mobile 商家手机号
     * @param $password 登录密码
     * @return bool
     * pos机商家登录
     */
    public function seller_pos_login($mobile, $password)
    {
        if (empty($mobile)) {
            return arr_post(0, '商家手机号不能为空');
        }
        if (empty($password)) {
            return arr_post(0, '请输入手机密码');
        }
        $user = $this->where(['mobile' => $mobile])->first();
        if (empty($user)) {
            return arr_post(0, '用户不存在');
        } else {
            if ($user->status == 0) {
                return arr_post(0, '您被禁止登录');
            }
            if (md5(md5($password) . 'sjlm') != $user->password) {
                return arr_post(0, '密码不正确');
            } else {
                $uid = $user->id;
                //验证商家是否审核通过
                $status = MerchantApply::where(['uid' => $uid])->first(['status'])->status;
                if ($status != 3) {
                    return arr_post(0, '您的商家信息未通过审核,无法登录');
                } else {
                    $this->where(['id' => $user->id])->update(['last_time' => time2date()]);
                    $ticket = uuid();
                    $add = [
                        'uid' => $uid,
                        'session_id' => session_id(),
                        'ticket' => $ticket,
                        'status' => 0,
                        'ip' => request()->ip(),
                        'client_system' => request('ClientSystem'),
                        'client_version' => request('ClientVersion'),
                        'api_version' => request('ApiVersion', '1.0'),
                        'timeout' => request()->server('REQUEST_TIME', time()) + 3600 * 3,
                    ];
                    $var = UserTicket::create($add);
                    //返回ticket
                    return arr_post(1, '登录成功', ['ticket' => $ticket]);
                }
            }
        }
    }

    /**
     * @param $uid 用户id
     * @param $password 登录密码
     * @return array
     * 验证密码
     */
    public function check_password($uid, $password)
    {
        $password = md5(md5($password));
        $rs = $this->where(['id' => $uid, 'password' => $password])->first();
        return empty($rs) ? arr_post(0, '密码不正确') : arr_post(1, '密码正确');
    }

    /**
     * @param $uid 用户id
     * @param $password
     * @param $password_name f_password 登录密码 s_password 查询密码 t_password 支付密码
     * @return array
     * 更新密码
     */
    public function update_password($id, $password, $password_name)
    {
        $password = md5(md5($password));
        return $this->update_user_info($id, [$password_name => $password]);
    }


    /**
     * 修改密码
     * @param $request
     * @return mixed
     */
    public function agent_change_pwd($request)
    {
        //获取用户信息
        $info = $this->where(['id' => session('agent_info')->id])->first();
        //验证判断
        if (md5(md5($request['old_password']) . 'tdzd') != $info->password) {
            return arr_post(0, '旧登录密码不正确！');
        }
        //更改密码
        $password = md5(md5($request['password']) . 'tdzd');
        $result = $this->where(['id' => session('agent_info')->id])->update(['password' => $password]);
        return arr_post(1, '密码修改成功！');

    }


    /**
     * @param $mobile
     * @param int $member_type
     * @return array
     * 根据手机号获取用户不同身份的id
     */
    public function get_user_id_by_mobile($mobile, $member_type = 1)
    {
        if ($member_type == 1) {
            $table = 'user';
        } elseif ($member_type == 3) {
            $table = 'agent';
        }
        $member_info = DB::table($table)->where(['mobile' => $mobile])->first(['id']);
        if (isset($member_info->id)) {
            $user_id = $member_info->id;
        } else {
            $user_id = DB::table($table)->insertGetId(['mobile' => $mobile, 'created_at' => time2date()]);
        }
        return $user_id;
    }

    /**
     * @param $uid 商家id
     * @param $amount 让利金额
     * 累计消费者让利
     */
    public function add_platform_amount($buyer_id, $amount)
    {
        $key = 'buyer_platform_' . $buyer_id;
        if (Cache::has($key)) {
            $total_amount = Cache::get($key) + $amount;
        } else {
            $total_amount = VOrder::whereIn('type', [1, 3, 5])->where(['status' => 2])->where(['buyer_id' => $buyer_id])->sum('platform_amount') + $amount;
        }
        Cache::put($key, $total_amount, 3600);
        return true;
    }

    /**
     * @param $uid 商家id
     * 获取消费者让利
     */
    public function get_platform_amount($buyer_id)
    {
        $key = 'buyer_platform_' . $buyer_id;
//        if(Cache::has($key)){
//            $total_amount=Cache::get($key);
//        }else{
        $total_amount = VOrder::whereIn('type', [1, 3, 5])->where(['status' => 2])->where(['buyer_id' => $buyer_id])->sum('platform_amount');
//        }
        return $total_amount;
    }

    /**
     * 商城用户注册(已注册的不受影响)
     *
     * @param string $mobile 手机号
     * @return array
     */
    public function main_register($mobile)
    {
        //中转请求积分商城
//        $data['ticket']=$ticket;
        $data['_cmd'] = 'user_main_register';
        $data['ClientSystem'] = 'wish_main';
        $data['mobile'] = $mobile;
        $data['sign'] = get_shop_pos_sign($data);
        $res = json_decode(curl_post(env('SHOP_DOMAIN'), $data), true);
        return arr_post($res['status'], $res['info'], $res['data']);
    }


}
