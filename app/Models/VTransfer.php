<?php
namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Support\Facades\DB;

class VTransfer extends CommonModel {
    protected $table = 'v_transfer';

    /**
     * @param $query
     * @return mixed
     * 格式化分页列表，取出对应的图片信息
     */
    public function scopePagesParse($query){
        $data = $query->pages();
        return $data;
    }

    /**
     * 转账数据统计
     * @param $form 起始时间 格式为YYYY-XX-RR
     * @param $to  截止时间 格式为YYYY-XX-RR
     * @return mixed
     */
    public function get_statistics_data($form,$to){

        //判断是否有选择时间区间，若是没有这从2017-01-01至当前时间加一天
        $form = !empty($form)?$form:'2015-01-01';
        $to = !empty($to)?date("Y-m-d",strtotime("+1 day",strtotime($to))):'2020-12-12';

        //统计数据
//        $reason = DB::select("SELECT t.*, Sum(v.out_amount) AS amount FROM transfer_reason AS t LEFT JOIN ( SELECT * FROM v_transfer WHERE v_transfer.time BETWEEN '$form' AND '$to' ) AS v ON t.reason_code = v.reason GROUP BY t.reason_code, t.reason_id ORDER BY t.reason_id");
        $reason = DB::table('transfer_reason as t')
                -> select(DB::raw('t.reason_id,t.reason_code,t.reason_name,t.remarks,Sum(v.out_amount) AS amount'))
                -> leftJoin(DB::raw("(SELECT * FROM v_transfer WHERE v_transfer.time BETWEEN '$form' AND '$to') AS v "),DB::raw('t.reason_code'),'=',DB::raw('v.reason'))
                -> groupBy(DB::raw('t.reason_code,t.reason_id,t.reason_name,t.remarks'))
                -> orderBy(DB::raw('t.reason_id'))
                -> paginate(15);

//        dd($reason1);
        return $reason;
    }


    /**
     * 转账统计详情
     * @param $form 起始时间 格式为YYYY-XX-RR
     * @param $to  截止时间 格式为YYYY-XX-RR
     * @param $reason_code 转账代码
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function Transfer_detail($reason_code,$form,$to){

        //判断是否有选择时间区间，若是没有这从2017-01-01至当前时间加一天
        $form = !empty($form)?$form:'2017-01-01';
        $to = !empty($to)?date("Y-m-d",strtotime("+1 day",strtotime($to))):'2020-12-12';

        //获取该转账编码的详情
        $Transfer_detail = $this -> where(['reason' => $reason_code])
                         -> whereBetween('time',[$form,$to])
                         -> orderBy('transfer_id','desc')
                         -> paginate(15);
        return $Transfer_detail;
    }

    /**
     * 钱包统计数据
     * @param $owner_type_num 角色类型数量
     * @param $purse_type_num 钱包类型数量
     * @return array
     */
    public function get_purse_data($owner_type_num,$purse_type_num){
        //1、先设置存储统计数据的容器，这是一个三维数组
        $Purse_statistics = array();
        //2、先根据角色类型的数量来分类，再根据钱包类型的数量来分类，得到每个角色的每种钱包的id，进而根据钱包id来统计转账记录的收入和支出的总金额
        //a、划分角色类型
        for ($owner_type = 1; $owner_type <= $owner_type_num; $owner_type++){
            //获取角色的名字
            $Purse_statistics [$owner_type-1]['name'] = $this -> get_user_name($owner_type);
            //b、划分钱包类型
            for ($purse_type = 1; $purse_type <= $purse_type_num; $purse_type++){
                //c、排查中央银行钱包和TD
                if ($purse_type == 5 ||$purse_type == 2){
                    continue;
                }
                //d、获取相应的钱包id数组
                $purse_ids = UserPurse::where(['purse_type' => $purse_type, 'owner_type' => $owner_type]) -> pluck('purse_id');
                //获取相应钱包名称，并作为三维数组的第二个下标
                $purse_name = $this -> get_purse_name($purse_type);
                //e、获取相应钱包的支出和收入的统计数据
                $Purse_statistics [$owner_type-1][$purse_name]['income'] = $this -> where(['into_owner_type' => $owner_type]) -> whereIn('into_purse_id',$purse_ids) -> sum('into_amount');
                $Purse_statistics [$owner_type-1][$purse_name]['pay'] = $this -> where(['out_owner_type' => $owner_type]) -> whereIn('out_purse_id',$purse_ids) -> sum('out_amount');
            }
        }
        return $Purse_statistics;
    }

    /**
     * 获取角色名称
     * @param $type 角色类型
     * @return string
     */
    public function get_user_name($type){
        if($type==1){
            $type_name='用户';
        }elseif($type==2){
            $type_name='商家';
        }elseif($type==3){
            $type_name='系统';
        }elseif($type==4){
            $type_name='中央银行';
        }elseif($type==5){
            $type_name='机主';
        }elseif($type==6){
            $type_name='第三方代付';
        }elseif($type==7){
            $type_name='通联pos机消费';
        }elseif($type==8){
            $type_name='高汇通代付';
        }elseif($type==9){
            $type_name='高汇通pos机消费';
        }elseif($type==10){
            $type_name='积分商城';
        }elseif($type==11){
            $type_name='运营中心';
        }
    
        return $type_name;
    }

    /**
     * 获取钱包名称下标
     * @param $type 钱包类型
     * @return string
     */
    public function get_purse_name($type){
        if($type==1){
            //收益钱包
            $type_name='profit';
        }elseif($type==2){
            //中央银行钱包
            $type_name='bank';
        }elseif($type==3){
            //积分钱包
            $type_name='point';
        }elseif($type==4){
            //货款钱包
            $type_name='goods';
        }elseif($type==5){
            //TD钱包
            $type_name='TD';
        }elseif($type==6){
            //营运中心佣金钱包
            $type_name='commission';
        }
        return $type_name;
    }

    /**
     * @param $query
     * @param $seller_id 商家id
     * @param $date 日期 2017-12-10
     * @return mixed
     * 获取商家相应时间段积分和收益条件筛选拼接字段
     */
    public function scopeGetSellerTraDate($query,$seller_id,$date)
    {
        $Bank=new Bank();
        //获取商家积分和收益钱包
        $purse_id=$Bank->userWallet($seller_id,1,2)->purse_id;
        $point_purse_id=$Bank->userWallet($seller_id,3,2)->purse_id;
        return $query->whereIn('into_purse_id',[$purse_id,$point_purse_id])
            ->whereRaw("FROM_UNIXTIME(create_time,'%Y-%m-%d')='".$date."'")
            ->orderBy('create_time','desc')
            ->orderBy('transfer_id','desc');
    }
}