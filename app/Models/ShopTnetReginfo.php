<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class ShopTnetReginfo  extends Model {
    public $table = 'tnet_reginfo';
    /*
     * 获取微信信息
     */
    public function getWxinfo($nodeid = 0) {
        return $this->where(['nodeid' => $nodeid])->first(['nodecode','nodename','photourl']);
    }
}