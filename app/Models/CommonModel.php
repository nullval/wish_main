<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommonModel extends Model {
	// 定义不能被批量复制的字段，create() ，参考模型赋值文档
	protected $guarded = [];

	public $error = '';

	public function getError(){
		return $this->error;
	}

	public function scopePages($query){
		$page = request()->input('page',1);
		if($page < 1){
			$page = 1;
		}
		$page_size = 15;
		$offset = ($page - 1) * $page_size;
		$data = $query->offset($offset)->limit($page_size)->get();
		return $data;
	}
}