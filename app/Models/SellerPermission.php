<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SellerPermission extends Model
{
	protected $table = 'seller_permission';

	/**
	 * @param string $permission_list 权限列表 不传则获取所有权限列表
	 * @return array
	 * 格式化菜单
	 */
	public function sort_permission($permission_list=''){
		if(empty($permission_list)){
			$permission_list=$this->get();
		}
		return $this::GetTree($permission_list,0,0);
	}

	private function GetTree($arr,$pid,$step){
		global $tree;
		foreach($arr as $key=>$val) {
			if($val['pid'] == $pid) {
				$flg = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;└--',$step);
				$val['flg_name'] = $flg.$val['name'];
				$val['level']=$step;
				$tree[] = $val;
				$this->GetTree($arr , $val['id'] ,$step+1);
			}
		}
		return $tree;
	}
}
