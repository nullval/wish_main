<?php
namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Support\Facades\DB;

//转接java接口
class JavaIncrement extends CommonModel {

    /**
     * @param $mobile
     * @return int 获取商家用户身份的nodeid
     */
    public function get_seller_node_id($mobile){
        $data['_cmd'] = 'user_checkMobile';
        $data['mobile'] = $mobile;
        $data['sign'] = get_shop_pos_sign($data);
        $res = json_decode(curl_post(config('config.tdzd_sapi_url'), $data), true);
        if($res['status']==1){
            return $res['data']['node_id'];
        }
        return 0;
    }

    /**
     * @param $action 方法
     * @param $param 参数
     * @param string $method 请求类型
     * @return mixed
     * 请求java接口获取数据
     */
    public function change_over($action,$param,$method='post'){
        $post_url=config('config.tdzd_td_url');
        $param=$this->get_sign_param($param,$method);
        new_logger('JavaIncrement_change_over.log', 'java', [$param]);
//        var_dump($param);exit;
        $post_url=$post_url.'/'.$action;
        $rs=[];
        if($method=='post'){
//            print_r(curl_post($post_url,$param));exit;
            $result=curl_post($post_url,$param);
            if(is_json($result)){
                $rs=\GuzzleHttp\json_decode($result,true);
            }
        }else{
            $result=file_get_contents($post_url.'?'.$param);
            if(is_json($result)){
                $rs=\GuzzleHttp\json_decode($result,true);
            }
        }
        new_logger('JavaIncrement_change_over.log', 'java回调结果', $rs);
        return $rs;
    }

    /**
     * @param $param 参数
     * @param string $method 请求类型
     * @return string
     * 获取java接口签名
     */
    public function get_sign_param($param,$method='post'){
        unset($param['sign']);
        $fix_param=[
            'serviceTag'=>'TD_B',
            'timestamp'=>time().'00',
            'version'=>'01',
            'key'=>'cTwiCdDZ5Rd1XvxA',
        ];
        $param=array_merge($param,$fix_param);
        ksort($param);
        $param2 = [];
        foreach($param as $k => $v){
            $param2[] = $k.'='.$v;
        }
        $montage_str=implode('&',$param2);
        $sign = strtoupper(md5($montage_str));
        unset($param2['key']);
        $str_t=implode('&',$param2);
        new_logger('JavaIncrement_get_sign_param.log','拼接字段',['str'=>$montage_str]);
//        var_dump($montage_str.'&key=cTwiCdDZ5Rd1XvxA');exit;
        $param['sign'] = $sign;
        new_logger('JavaIncrement_get_sign_param.log','请求java参数',$param);
        if($method=='post'){
            unset($param['key']);
            return $param;
        }else{
            return $str_t.'&sign='.$sign;
        }
    }

    /**
     * @param $mobile 用户手机号码
     * @param $point 积分额度
     * @param $uid 用户id
     * @param int $orderType 3用户 4商家
     * 自动挂高单
     */
    public function automatic_guadan($mobile,$point,$uid,$orderType=3,$order_sn=''){
        //对接shop商城获取nodeid
        //中转请求积分商城
        $data['_cmd'] = 'user_checkMobile';
        $data['mobile'] = $mobile;
        $data['sign'] = get_shop_pos_sign($data);
        $res = json_decode(curl_post(env('SHOP_DOMAIN'), $data), true);
        new_logger('JavaIncrement_automatic_guadan.log', 'shop', $res);
        if ($res['status'] == 1) {
            $nodeId = $res['data']['node_id'];
            //更新用户nodeid
            User::where(['id'=>$uid])->update(['nodeid'=>$nodeId]);
            //对接java挂到最高盘
            $action = 'api/json/sysUser/order.do';
            $param = [
                'uid' => $nodeId,
                'qty' => $point,
                'orderType' => $orderType,
                'phone'=>$mobile,
                'order_sn'=>$order_sn
            ];
//            $java = $this->change_over($action, $param, 'post');
            new_logger('JavaIncrement_automatic_guadan.log', 'param', array_filter($param));
            $java = $this->change_over($action, array_filter($param), 'post');
            new_logger('JavaIncrement_automatic_guadan.log', 'java', $java);
            return $java;
        }
    }

    /**
     * @param $mobile 用户手机号码
     * @param $point 积分额度
     * @param $uid 用户id
     * @param int $orderType 3用户 4商家
     * 积分增值
     */
    public function jfConversionTd($mobile,$point,$uid,$orderType=3){
        $data['_cmd'] = 'user_checkMobile';
        $data['mobile'] = $mobile;
        $data['sign'] = get_shop_pos_sign($data);
        $res = json_decode(curl_post(env('SHOP_DOMAIN'), $data), true);
        new_logger('JavaIncrement_jfConversionTd.log', 'shop', $res);
        if ($res['status'] == 1) {
            $nodeId = $res['data']['node_id'];
            //更新用户nodeid
            User::where(['id'=>$uid])->update(['nodeid'=>$nodeId]);
            //对接java挂到最高盘
            $action = 'api/json/api/jfConversionTd.do';
            $param = [
                'uid' => $nodeId,
                'qty' => $point,
                'orderType' => $orderType,
            ];
            $java = $this->change_over($action, $param, 'post');
            new_logger('JavaIncrement_jfConversionTd.log', 'java', $java);
            return $java;
        }
    }


}