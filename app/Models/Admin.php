<?php

namespace App\Models;

use App\Exceptions\ApiException;
use App\Services\AdminIdentityDispose;
use Illuminate\Support\Facades\DB;


class Admin extends CommonModel
{
    protected $table = 'wish_admin';


    public function scopeDetail($query, $admin_id, $field = ['*'])
    {
        return $query->where(['id' => $admin_id])->first($field);
    }

    /**
     * 管理员后台登录
     * @param $login_arr
     * @param string $loginType 登录
     * @return bool
     * @throws ApiException
     */
    public function login($login_arr,$identity = AdminIdentityDispose::IDENTITY_SELLER)
    {
        $adminIdentityModel = new AdminIdentityDispose();
        //得到要检查登录的身份
        $identityIds = $adminIdentityModel->getRoleIdByIdentity($identity);
        $admin_info = $this->where(['username' => $login_arr['username']])->whereIn('role_id',$identityIds)->first();

        if (empty($admin_info)) {
            throw new ApiException('用户不存在');
        } else {
            if ($admin_info['status'] == 0) {
                throw new ApiException('您被禁止登录');
            }
            if (md5(md5($login_arr['password'])) != $admin_info->password) {
                throw new ApiException('密码不正确');
            } else {

                $this->where(['id' => $admin_info->id])->update(['last_time' => time2date()]);
                $admin_info->role_name = AdminRole::where(['id' => $admin_info->role_id])->value('role_name');
                //session存储用户信息
                session(['admin_info' => $admin_info]);

                //获取用户角色权限列表
                $permission = AdminRole::where(['id' => $admin_info->role_id])->first(['permission'])['permission'];
                $permission = explode(',', $permission);
                $permission_list = AdminPermission::whereIn('id', $permission)->orderBy('sort', 'DESC')->get();
                //格式化菜单
                $AdminPermission = new AdminPermission();
                $permission_list = $AdminPermission->sort_permission($permission_list);
                //session存储用户权限
                session([$admin_info->id . '_permission_list' => $permission_list]);

                //登录成功记录信息
                return true;
            }
        }
    }

    /*
     * @param $username 用户名
     * @param $password 用户密码
     * @param $role_id 角色id
     * @param $admin_id 用户id
     * 添加管理员
     */
    public function add_admin($username, $password = '', $role_id = 0, $admin_id = 0)
    {
        $admin_arr['username'] = $username;
        $admin_arr['role_id'] = $role_id;
        if (!empty($admin_id)) {
            //修改
            if (!empty($password)) {
                $admin_arr['password'] = md5(md5($password));
            }
            $admin_arr['updated_at'] = time2date();
            $this->where(['id' => $admin_id])->update($admin_arr);
        } else {
            //添加
            $admin_arr['password'] = md5(md5($password));
            $admin_arr['last_time'] = time2date();
            $admin_arr['created_at'] = time2date();
            $admin_id = $this->insertGetId($admin_arr);
        }
        return $admin_id;
    }

    /**
     * 获取管理员下面一层孩子
     * param $arr_uid_arr 机主数组
     * @param $admin_id
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public function find_down($admin_id)
    {
        $info = $this->find($admin_id);
        //判断当前角色 $info->role_id
        //找到当前角色所处表的user_id
        if ($info->role_id == 1) {
            $user_id = 0;//默认所有分公司
        } else {
            $user_id = DB::table($this->getCurrentTable($info->role_id))->where('mobile', $info->username)->value('id');
            if (empty($user_id)) throw new \Exception('user_id不存在');
        }
        //找到下级角色对应的表
        $agent_arr = DB::table($this->getChildTable($info->role_id))->where('inviter_id', $user_id)->pluck('mobile');
        $agent_id = $this->whereIn('username', $agent_arr)->pluck('id');
        return $agent_id;
    }

    /**
     * 根据手机号获取admin表的ID
     *
     * @param $mobile
     * @return mixed
     */
    public function getIdByMobile($mobile)
    {
        return $this->where('username', $mobile)->value('id');
    }

    /**
     * 获取当前角色所在表
     *
     * @param $role_id
     * @return string
     * @throws \Exception
     */
    public function getCurrentTable($role_id)
    {
        switch (intval($role_id)) {
//            case 1:
            case 2:
                return 'wish_company';
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                return 'wish_agent';
            case 8:
                return 'wish_generalize';
            case 9:
                return 'wish_seller';
            default:
                throw new \Exception('role_id不在范围内');
        }

    }

    /**
     * 获取当前角色下一级所在表
     *
     * @param $role_id
     * @return string
     * @throws \Exception
     */
    public function getChildTable($role_id)
    {
        switch (intval($role_id)) {
            case 1:
                return 'wish_company';
            case 2:
                return 'wish_agent';
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                return 'wish_generalize';
            case 8:
                return 'wish_seller';
//            case 9:
            default:
                throw new \Exception('role_id不在范围内');

        }

    }
}