<?php

namespace App\Models;

use App\Exceptions\ApiException;
use App\Jobs\handleBuyPosCopyOrder;
use App\Jobs\handleBuyPosFirstOrder;
use App\Jobs\handleBuyPosOrder;
use App\Jobs\handleBuyTdOrder;
use App\Jobs\handleGhtBuyPosOrder;
use App\Jobs\handleGhtSellerOrder;
use App\Jobs\handleSellerFirstOrder;
use App\Jobs\handleSellerOrder;
use App\Jobs\handleSellerRecharge;
use App\Jobs\handleShopOrder;
use App\Jobs\SendMessage;
use App\Libraries\BankSdk;
use App\Models\Api\ApiTdUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    protected $guarded = [];	// 列出想被批量复制保护的字段，取反即为都可以被批量复制，用于 firstOrCreate()、updateOrCreate() 等方法快捷增改
    protected $table = 'order';


    public function __construct($year=0,array $attributes = [])
    {
        parent::__construct($attributes);
        $year=empty($year)?date('Y'):$year;
        $this->table='order_'.$year;
    }

    public function return_pay_type($pay_type_name){
        return collect([
            'wechat'=>1,
            'alipay'=>2,
            'bank'=>3
        ])->get($pay_type_name);
    }

    /**
     * @param $order_sn
     * @param int $is_now
     * @param int $is_true_amount
     * @return array
     * 获取打印订单
     */
    public function get_order_print_info($order_sn,$is_now=1,$is_true_amount=1){
        $order_info=VOrder::where(['order_sn'=>$order_sn])->first();

        if(!isset($order_info->status)){
            throw new ApiException('订单不存在');
        }
        $data['order_sn']=$order_sn;
        $data['status']=$order_info->status;

//        if($data['status']==2){

        //支付类型
        if($order_info->pay_type==1){
            //微信
            $data['bank_code']='BACKSTAGEWECHAT';
            $data['pay_type']='微信支付';
        }elseif($order_info->pay_type==2){
            //支付宝
            $data['bank_code']='BACKSTAGEALIPAY';
            $data['pay_type']='支付宝支付';
        }else{
            //银行卡
            $data['bank_code']='银行直连参数';
            $data['pay_type']='银行卡支付';
        }
        $data['terminal_no']=$order_info->terminalId;//终端号
        $data['trmSeqNo']=$order_info->trmno;//终端号
        $pos_info=Pos::where(['terminalId'=>$order_info->terminalId])->first();
        $uid=empty($pos_info->uid)?$order_info->uid:$pos_info->uid;
        $ruzhu_info=RuzhuMerchantBasic::where(['uid'=>$uid])->first();
        if(isset($ruzhu_info->id)){
            $data['merchantName']=$ruzhu_info->merchantName;//商户名称
            $data['merchant_no']=$ruzhu_info->merchantId;//商户号
        }else{
            $data['merchantName']='创观科技（深圳）有限公司';//商户名称
            $data['merchant_no']='00000000';//商户号
        }
        $data['order_id']=$order_info->id+1000000;//凭证号
        $data['order_no']=$order_sn;//交易单号
        if($is_now==1){
            $data['pay_time']=date('Y/m/d H:i:s');//交易时间
        }elseif($is_now==2){
            $data['pay_time']=$order_info->created_at->format('Y-m-d H:i:s');//交易时间
        }
        // 买家手机号
        if($order_info->type==6){
            $data['mobile_buyer'] = TdUser::where(['id'=>$order_info->buyer_id])->value('mobile');
        }else{
            $data['mobile_buyer'] = $order_info->buyer_id == 64  ? '' : User::where(['id'=>$order_info->buyer_id])->value('mobile');
        }
        $data['mobile_buyer'] = substr($data['mobile_buyer'],0,1) == '1' ? $data['mobile_buyer'] : '';
        // 操作员手机号
        $data['mobile_operate'] = SellerRole::where(['mobile'=>$order_info->role_mobile])->value('role_name');
        $data['type'] = $order_info->type == 5 ? 'qrcode' : '';
        $data['amount']=$is_true_amount==1?$order_info->actual_amount:$order_info->amount;//交易金额
        $data['order_type']=get_order_type_name($order_info->type);
//        }
        return $data;
    }

    /**
     * @param $mobile 刷卡人手机号码
     * @param $terminalId 刷卡用的终端
     * @param $type 订单类型 1订单支付 3消费买单
     * @param $mac
     * @param $role_mobile 店员手机号
     * @param $pay_type 支付方式 alipay,wechat,bank
     * @return array
     * 创建消费订单
     */
    public function create_order($mobile,$terminalId,$type,$role_mobile='',$pay_type='',$amount=0,$notify_type,$lat,$lng,$pay_method){


        $terminalId_pos_info=Pos::where(['terminalId'=>$terminalId])->first();
        if(!isset($terminalId_pos_info->terminalId)){
            throw new ApiException('该终端号不存在');
        }
        $terminalId=$terminalId_pos_info->terminalId;
        $pos_info=Pos::where(['terminalId'=>$terminalId])->first();
        if(empty($pos_info->uid)){
            throw new ApiException('该终端机还未绑定商家,无法使用');
        }

        if(empty($amount)){
            throw new ApiException('支付金额不能为0');
        }
        if(empty($pay_type)){
            throw new ApiException('请选择支付类型');
        }

        //判断商家是否被禁用
        $seller_info=Seller::where(['id'=>$pos_info->uid])->first();
        //金额判断
        is_rand_amount($amount,$pay_type,$seller_info->is_true);

        if($seller_info->status==0){
            throw new ApiException('该商家已被平台禁用，无法使用');
        }

        $rmb_info=RuzhuMerchantBasic::where(['uid'=>$pos_info->uid])->first();
        //判断是否足额
        $this->is_recharge_enough($amount,$pos_info->uid,$rmb_info->merchantId);

        if(!isset($rmb_info->merchantId)){
            throw new ApiException('商家未入驻高汇通无法使用');
        }
        if($rmb_info->status!=3){
            throw new ApiException('该商家未通过商家入驻审核,无法使用');
        }

        if(!empty($role_mobile)){
            $data['role_mobile']=$role_mobile;
        }
        //用于高汇通商家分账
        $data['child_merchant_no']=$rmb_info->merchantId;
        $data['pay_type']=$this->return_pay_type($pay_type);
        $data['amount']=$amount;
        $data['notify_type']=$notify_type;
        $data['uid']=$uid=$terminalId_pos_info->uid;
        if(!empty($type)&&$type==3){
            //消费买单
            //创建一个假的买家
            $mobile='11111111111';
            $order_sn='0003'.create_order_no(time());
            $data['type']=3;
        }else{
            //订单支付
            //消费者手机号码

            if(empty($mobile)){
                throw new ApiException('请输入消费者手机号码');
            } elseif (!preg_match("/^1[345789]\d{9}$/", $mobile)){
                throw new ApiException('请输入正确的手机号码');
            }
            $order_sn='0001'.create_order_no(time());
            $data['type']=1;
        }
        $data['terminalId']=$terminalId;
        $data['buyer_id']=User::firstOrCreate(['mobile'=>$mobile])->id;
        $data['created_at']=time2date();
        $data['order_sn']=$order_sn;
        $data['lat']=$lat;
        $data['lng']=$lng;
        $data['pay_method']=$pay_method;
        Order::insertGetId($data);
        return $data;
    }

    /**
     * @param $seller_id
     * @param $openid
     * @param $amount
     * @param string $mobile
     * @param $pay_type 支付方式 alipay,wechat,bank
     */
    public function create_qr_order($seller_id,$openid,$amount,$mobile = '',$pay_type='wechat',$notify_type){
        sys_ban();
        if (limit_time("09:00","23:59")) {
            throw new ApiException('线下扫码消费时间为09:00-24:00');
        }

        //判断商家是否被禁用
        $seller_info=Seller::where(['id'=>$seller_id])->first();
        if($seller_info->status==0){
            throw new ApiException('该商家已被平台禁用，无法使用');
        }

        $rmb_info=RuzhuMerchantBasic::where(['uid'=>$seller_id])->first();
        if(!isset($rmb_info->merchantId)){
            throw new ApiException('商家未入驻高汇通无法使用');
        }
        if($rmb_info->status!=3){
            throw new ApiException('该商家未通过商家入驻审核,无法使用');
        }
        //用于高汇通商家分账
        $data['child_merchant_no']=$rmb_info->merchantId;
        $user_info=User::where(['mobile'=>$mobile])->first();

        //消费买单
        // 如果这个手机号已经绑定了宝贝宝openid，并且不等于前端传的openid，就需要验证短信验证码并把当前openid改到现在这个手机下
        $openid_mobile = User::where(['openid_baobeibao'=>$openid])->value('mobile');
        if($openid && $mobile && $openid_mobile && $openid_mobile != $mobile){
            $code = request()->input('sms_code');
            if(empty($code)){
                throw new ApiException('请输入验证码');
            }
            $result = (new Message())->check_sms($mobile,$code,1);
        }

        if($openid && $mobile){
            User::where(['openid_baobeibao'=>$openid])->update(['openid_baobeibao'=>null]);
            User::where(['mobile'=>$mobile])->update(['openid_baobeibao'=>$openid]);
        }

        //创建一个假的买家
        $mobile = $mobile ?: '3'.rand(10,99).rand(1000,9999).rand(1000,9999);
        if(!isset($user_info->id)){
            //没有注册
            $user_id=User::insertGetId(['mobile'=>$mobile,'created_at'=>time2date()]);
            $data['buyer_id']=$user_id;
        }else{
            $data['buyer_id']=$user_info->id;
        }
        $order_sn='qr'.create_order_no(time());
        //消费买单
        $data['notify_type']=$notify_type;
        $data['uid']=$seller_id;
        $data['order_sn']=$order_sn;
        $data['amount'] = $amount;
        $data['actual_amount'] = $amount;
        $data['type']=5;
        $data['remark']=$openid;
        $data['pay_type']=$this->return_pay_type($pay_type);
        $data['order_sn']=$order_sn;
        $data['created_at']=time2date();
        $this->insertGetId($data);
        return $data;
    }


    /**
     * @param $mobile 购买pos机的业务员手机号
     * @param $terminalIdArr 想要购买的终端号数组,,隔开 空则不传
     * @param $pos_remark 备注
     * @param $terminalId 刷卡用的终端号
     * @param $pos_num 购买pos机的台数
     * @param $receiver 收货人
     * @param $contact_number 联系电话
     * @param $contact_address 联系地址
     * @param $invite_mobile 购买pos机填写的推荐人手机号码
     * @param $pay_type 支付方式 alipay,wechat,bank
     * @param $amount 支付金额单位为元
     * @param $mac
     * @return array
     * 创建购买pos机订单
     */
    public function create_pos_order($mobile,$terminalIdArr,$pos_remark,$terminalId,$pos_num,$receiver,$contact_number,$contact_address,$invite_mobile,$mac,$pay_type,$amount,$is_direct_buy=1,$notify_type){
//        if(config('app.env')=='online'){
//            return arr_post(0,'暂时未开通');
//        }
        sys_ban();
//        if($pay_type=='bank'){
//            throw new ApiException('不支持银行卡支付');
//        }
//        if($pay_type=='alipay'){
//            throw new ApiException('通道暂未开通');
//        }
        //判断金额
        if (limit_time("09:00","22:00")) {
            throw new ApiException('消费时间为09:00-22:00');
        }

        $Agent=new Agent();
        $new_agent_info=$Agent->add_or_create_agent($mobile);
        $is_new=$new_agent_info['data']['is_new'];
        $data['buyer_id']=$buyer_id=$new_agent_info['data']['agent_id'];



        if(empty($invite_mobile)){
            throw new ApiException('请填写推荐人');
        }
        $invite_mobile_info=Agent::where(['mobile'=>$invite_mobile])->first();
        if(!isset($invite_mobile_info->mobile)){
            throw new ApiException('推荐人不存在');
        }
        $pos_amount=$is_direct_buy==1?config('pos.pos.amount'):config('pos.pos.second_amount');
        if(empty($amount)||$amount!=$pos_amount){
            throw new ApiException('支付金额必须为'.$pos_amount);
        }
        if(empty($pay_type)){
            throw new ApiException('请选择支付类型');
        }

        //验证手机
        if(empty($mobile)){
            throw new ApiException('请输入购买创观pos机的购买者手机号');
        }elseif (!preg_match("/^1[345789]\d{9}$/", $mobile)){
            throw new ApiException('请输入正确的手机号码');
        }
        //查找该pos机所属机主

        if(!empty($terminalId)){
            $terminalId_pos_info=Pos::where(['terminalId'=>$terminalId])->first(['terminalId','pos_type']);

            if(!isset($terminalId_pos_info->terminalId)){
                throw new ApiException('该终端号不存在');
            }
            $terminalId=$terminalId_pos_info->terminalId;

            $new_pos_info=Pos::where(['terminalId'=>$terminalId])->first();
            if(empty($new_pos_info->agent_uid)){
                throw new ApiException('该终端号未激活');
            }

            if($mobile==$invite_mobile){
                throw new ApiException('推荐人不能是本人');
            }
        }




        //拆分pos，查看pos机是否为该代理所有
//        if(!empty($terminalIdArr)){
//            $terminalIdArr_arr=array_filter(explode(',',$terminalIdArr));
//            if($terminalId!='00'){
//                foreach ($terminalIdArr_arr as $k=>$v){
//                    $pos_info=Pos::where(['terminalId'=>$v])->first(['terminalId','agent_uid']);
//                    if(!isset($pos_info->terminalId)){
//                        return arr_post(0,'创观pos机'.$v.'不存在,无法生成订单');
//                    }elseif($pos_info->agent_uid!=$agent_uid){
//                        return arr_post(0,'创观pos机'.$v.',不属于您的创观pos机,无法生成订单');
//                    }
//                    if(!empty($pos_info->uid)){
//                        return arr_post(0,'创观pos机'.$v.'已绑定商家,请解绑再生成订单');
//                    }
//                    if($pos_info->agent_uid==$buyer_id){
//                        return arr_post(0,'创观pos机'.$v.'已属于您的创观pos机,无法绑定');
//                    }
//                    if($pos_info->terminalId==$terminalId){
//                        return arr_post(0,'当前创观pos机不能转让');
//                    }
//                }
//            }
//            if(count($terminalIdArr_arr)>$pos_num){
//                return arr_post(0,'创观pos机数量大于填写的购买数量');
//            }
//        }
        $data['pay_type']=$this->return_pay_type($pay_type);
        $data['is_new']=$is_new;
        $data['created_at']=time2date();
        $data['order_sn']='0002'.create_order_no(time());
        $data['terminalIdArr']=$terminalIdArr;
        $data['pos_remark']=$pos_remark;
        $data['pos_num']=$pos_num;
        $data['receiver']=$receiver;
        $data['contact_number']=$contact_number;
        $data['contact_address']=$contact_address;
        $data['invite_mobile']=$invite_mobile;
        $data['agent_id']=$invite_mobile_info->id;
        $data['terminalId']=$terminalId;
        $data['amount']=$amount;
        $data['type']=2;
        $data['child_merchant_no']=0;
        $data['notify_type']=$notify_type;
        $data['is_direct_buy']=$is_direct_buy;
        $this->insertGetId($data);
        return $data;
    }

    /**
     * 创建商城订单
     */
    public function create_order_shop($order_no,$mobile,$terminalId,$pay_type='',$amount=0,$notify_type){
        sys_ban();
        throw new ApiException('暂时关闭');

//        if($pay_type=='wechat'){
//            return arr_post(0,'收款账户正在合并中');
//        }

        if($pay_type=='bank'){
            throw new ApiException('不支持银行卡支付');
        }
        $user=new User();

        is_rand_amount($amount,$pay_type);
//		$new_agent_info=$Agent->add_or_create_agent($mobile);
        $user = $user->where(['mobile'=>$mobile])->first();
//        if (empty($agent)){
//            return arr_post(0,'该手机号不是机主，无法继续下单');
//        }elseif((empty($agent->inviter_id))&&($agent->is_admin==0)){
//            return arr_post(0,'该手机号不是机主，无法继续下单');
//        }
        $data['buyer_id']=$buyer_id=$user['id'];


        $new_pos_info=Pos::where(['terminalId'=>$terminalId])->first();
        if(empty($new_pos_info->uid)&&$pay_type=='bank'){
            throw new ApiException('pos机未绑定商家，无法使用');
        }

        $agent_uid=$new_pos_info->agent_uid;
        if(empty($new_pos_info->agent_uid)){
            throw new ApiException('该终端号未激活');
        }

        if(empty($amount)){
            throw new ApiException('支付金额不能为0');
        }
        if(empty($pay_type)){
            throw new ApiException('请选择支付类型');
        }

        $data['pay_type']=$this->return_pay_type($pay_type);

        if(empty($terminalId)){
            if(empty($mac)){
                throw new ApiException('该终端号不存在');
            }
            $terminalId_pos_info=Pos::where(['mac'=>$mac])->first(['terminalId']);
        }else{
            $terminalId_pos_info=Pos::where(['terminalId'=>$terminalId])->first(['terminalId']);
        }
        if(!isset($terminalId_pos_info->terminalId)){
            throw new ApiException('该终端号不存在');
        }
        $terminalId=$terminalId_pos_info->terminalId;

        $order_sn=$order_no;
        $data['order_sn']=$order_sn;
        $data['pos_remark']=$mobile;
        $data['agent_id']=$agent_uid;
        $data['terminalId']=$terminalId;
        $data['amount']=$amount;
        $data['type']=4;
        $data['created_at']=time2date();
        $this->insertGetId($data);
        $data['child_merchant_no']=0;
        if($pay_type=='bank'){
            $seller_info=Seller::where(['id'=>$new_pos_info->uid])->first();
            //请求银行支付 (乙贤)
            $result = BankSdk::unified($data['buyer_id'])
                ->orderNo($order_sn)
                ->notifyUrl(url('posPay/bankNotify'))
                ->orderType('亿贤刷卡')
                ->productName('银行卡支付')
                ->param('merchant_no',$seller_info->mobile)
                ->payType('yixian_bank',$amount * 100)
                ->pay();
        }
        return $data;
    }

    /**
     * @param $order_sn 订单
     * @param $pay_type 支付类型 1微信 2支付宝 3银行卡
     * @param $input_post 拼接字段
     * @param $amount 实付金额
     * @param $status 状态
     * @param $acct
     * @param $paytime 支付时间
     * @param $trxcode
     * @param $notify_type 通知类型 1通联 2高汇通
     * @param $seller_name 商家名称
     * @param $radio 分润比例
     * @return bool
     * 更新pos支付成功的订单
     */
    public function update_pos_success_order($order_sn,$pay_type=0,$input_post,$amount,$status,$acct,$paytime,$trxcode,$notify_type,$seller_name='',$radio=0,$trmno=''){
        $order_info=$this->where(['order_sn'=>$order_sn])->first();
        $terminalId=$order_info->terminalId;
        if(!empty($pay_type)){
            $order_arr['pay_type']=$pay_type;
        }
        //金额在此位置不变动
        $order_arr['amount']=$amount;
        $order_arr['notify_remark']=$input_post;
        $pos_info=Pos::where(['terminalId'=>$terminalId])->first();
        //对于购买pos机订单支付的判断其订单是否有效
        if(in_array($order_info->type,[1,3,5])){
            //订单支付 扫码支付
            //pos机所属益邦客
            $order_arr['agent_id']=$pos_info->agent_uid;
            //根据订单类型查找上就挨
            $uid=$order_info->type==5?$order_info->uid:$pos_info->uid;
            //订单补贴
            $seller_info=Seller::where(['id'=>$uid])->first();
            $pla_fee=RuzhuMerchantBusinesss::where(['merchantId'=>$seller_info->child_merchant_no])->first(['futureRateValue'])->futureRateValue;
            $seller_fee=100-floatval($pla_fee);
            $order_arr['seller_amount']=get_last_two_num($amount*($seller_fee/100));
            $order_arr['platform_amount']=$amount-$order_arr['seller_amount'];
        }elseif($order_info->type==2){
            //pos机购买
            //pos销售价
            $pos_amount=$order_info['is_direct_buy']==1?config('pos.pos.amount'):config('pos.pos.second_amount');
            $acutal_amount=$order_info->pos_num*$pos_amount;
            if($pos_amount>$amount&&config('app.env')=='online'){
                $order_arr['is_true']=2;
            }
            //pos所属机主运营中心id
            $operate_id=$pos_info->agent_operate_id;
            $order_arr['operate_id']=$operate_id;
            $order_arr['amount']=$acutal_amount;
        }

        $order_arr['trmno']=$trmno;
        //第三方费率
        $radio=empty($order_info->pay_method)?config('pay.notify_type')[$notify_type]['poundage']:config('pay.notify_type')[$notify_type]['pay_method_poundage'][$order_info->pay_method];
        $poundage=get_num($amount*($radio/1000));
        $order_arr['poundage']=empty($poundage)?0.01:$poundage;
        //根据订单号填充订单信息
        $order_arr['status']=$status;
        $order_arr['updated_at']=time2date();
        $order_arr['paytime']=$paytime;
        $order_arr['acct']=$acct;
        //交易类型
        $order_arr['trxcode']=$trxcode;
        $order_arr['notify_type']=$notify_type;
        $order_arr['actual_amount']=$amount;
        //不再生成对账单
//        $order_arr['is_left']=3;
        //查找GPU中的该订单的上游订单号 2018-6-29 13:52:23
        $order_arr['up_order_sn']=(new FundOrder())->getUpOrderNo($order_sn);
        //更新接收字段
        $rs=$this->where(['order_sn'=>$order_sn])->update($order_arr);
        if($rs){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param $order_sn 订单
     * @param $type 订单类型
     * 处理订单分润
     */
    public function handle_order_benefit($order_sn,$type){
        //是否发送推送
        $is_send=env('APP_ENV')=='online'?1:0;
        $is_send=1;
        //只有对订单处理成功才走以下分账
        //请勿在以下队列中事务中出现对order表的更新,否则有一定概率出现锁表的情况
        if($type==1||$type==3||$type==5){
            //消费买单 订单支付 扫码支付
            dispatch((new handleSellerFirstOrder($order_sn,$is_send))->onQueue('handleOrder'));
//            dispatch(new handleSellerFirstOrder($order_sn,$is_send));
        }elseif ($type==2){
            //购买pos机
//            dispatch((new handleBuyPosFirstOrder($order_sn,$is_send))->onQueue('handleOrder'));
//            dispatch(new handleBuyPosFirstOrder($order_sn,$is_send));
        }elseif ($type == 4){
            // 机主版商城订单
            dispatch((new handleShopOrder($order_sn,$is_send))->onQueue('handleOrder'));
//            dispatch(new handleShopOrder($order_sn,$is_send));
        }elseif ($type == 6){
            // 刷pos机购买td
            dispatch((new handleBuyTdOrder($order_sn,$is_send))->onQueue('handleOrder'));
//            dispatch(new handleBuyTdOrder($order_sn,$is_send));
        }elseif($type == 7){
            //商家充值货款
            dispatch((new handleSellerRecharge($order_sn,$is_send))->onQueue('handleOrder'));
//            dispatch(new handleSellerRecharge($order_sn,$is_send));
        }
    }


    /**
     * @param $notify_type 通知类型 1通联 2高汇通 3环讯 4亿贤 5越满
     * @param $amount 金额元
     * @param $detail 描述
     * @param $seller_purse_id 商家钱包
     * @return bool
     * @throws ApiException
     * 买单扣除手续费
     */
    public function buckle_fee($notify_type,$amount,$detail,$seller_purse_id=0,$pay_method=''){
        $Bank=new Bank();
        $notify_type_info=config('pay.notify_type')[$notify_type];
        //第三方费率
        $radio=empty($pay_method)?$notify_type_info['poundage']:$notify_type_info['pay_method_poundage'][$pay_method];
        $poundage = get_num($amount*($radio/1000));
        $poundage=empty($poundage)?0.01:$poundage;
        //第三方收益钱包
        $d_purse_id = $Bank->get_d_purse(1, $notify_type_info['owner_type'])->purse_id;

        if($poundage>0&&$notify_type!=0){
            if(empty($seller_purse_id)) {
                $sys_purse_id=$Bank->get_sys_purse_id(1);
                //扣除系统
                $Bank->doApiTransfer($sys_purse_id, $d_purse_id, $poundage, config('pay.notify_type')[$notify_type]['poundage_reason'],
                    $detail,
                    '扣除手续费');
            }else{
                $Bank->doApiTransfer($seller_purse_id, $d_purse_id, $poundage, config('pay.notify_type')[$notify_type]['poundage_reason'],
                    $detail,
                    '扣除手续费');
            }
        }


        //返回收取的手续费
        return $poundage;
    }



    /**
     * @param $terminalId 刷卡用的终端
     * @param $pay_type 支付方式 alipay,wechat,bank
     * @return array
     * 创建充值订单
     */
    public function create_recharge_order($terminalId='',$pay_type='',$amount=0,$uid=0,$notify_type,$pay_method){


        if($pay_type=='bank'){
            throw new ApiException('不支持银行卡支付');
        }


        //判断金额
        is_rand_amount($amount,$pay_type);

        if(!empty($terminalId)){
            $pos_info=Pos::where(['terminalId'=>$terminalId])->first();
            if(!isset($pos_info->id)){
                throw new ApiException('该终端号不存在');
            }
            if(empty($pos_info->uid)){
                throw new ApiException('该终端机还未绑定商家,无法使用');
            }
            $uid=$pos_info->uid;
        }

        if(empty($amount)){
            throw new ApiException('充值金额不能为0');
        }
        if(empty($pay_type)){
            throw new ApiException('请选择支付类型');
        }

        //判断商家是否被禁用
        $seller_info=Seller::where(['id'=>$uid])->first();
        if($seller_info->status==0){
            throw new ApiException('该商家已被平台禁用，无法使用');
        }

        $rmb_info=RuzhuMerchantBasic::where(['uid'=>$uid])->first();
        if(!isset($rmb_info->merchantId)){
            throw new ApiException('商家未入驻高汇通无法使用');
        }
        if($rmb_info->status!=3){
            throw new ApiException('该商家未通过商家入驻审核,无法使用');
        }


        $data['child_merchant_no']=$rmb_info->merchantId;
        $data['pay_type']=$this->return_pay_type($pay_type);
        $data['amount']=$amount;
        $data['uid']=$uid;
        $data['buyer_id']=0;
        $order_sn='0007'.create_order_no(time(),3);
        $data['terminalId']=$terminalId;
        $data['created_at']=time2date();
        $data['order_sn']=$order_sn;
        $data['type']=7;
        $data['notify_type']=$notify_type;
        $data['pay_method']=$pay_method;
        Order::insert($data);

        return $data;
    }

    public function is_recharge_enough($amount,$uid,$mobile){
        $Bank=new Bank();
        $seller_recharge_amount=$Bank->use_money($uid,7,2);
        $pla_fee=RuzhuMerchantBusinesss::where(['merchantId'=>$mobile])->value('futureRateValue');
        $seller_fee=100-floatval($pla_fee);
        $seller_amount=get_last_two_num($amount*($seller_fee/100));
        $platform_amount=$amount-$seller_amount;
        if($seller_recharge_amount<$platform_amount||$seller_recharge_amount<=0){
            throw new ApiException('商家充值额度不够,请充值过后再使用此功能');
        }
        return true;
    }

}
