<?php
namespace App\Models;

use App\Exceptions\ApiException;

/**
 * 消费记录
 */
class OfflineOrder extends CommonModel
{
    protected $table = 'offline_order';

    /**
     * @param $query
     * @return mixed
     * 查找有效的消费订单
     */
    public function scopeConsumeOrder($query,$status=2)
    {
        return $query->whereIn('offline_order.type',[1,3,4,5,7])->where(['offline_order.status'=>$status]);
    }


    /**
     * 消费者手机号
     */
    public function consumer_mobile(){
//        return $this->hasOne('App\Models\AdminDividend','admin_role_id');
        return $this->hasOne('App\Models\TnetReginfo', 'nodeid', 'buyer_id');
    }


}