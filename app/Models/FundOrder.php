<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018/6/29
 * Time: 11:33
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FundOrder extends Model
{
    protected $connection = 'gpu';

    protected $table = 'fund_order';

    /**
     * 获取上游订单号
     *
     * @param $order_sn
     * @return string
     */
    public function getUpOrderNo($order_sn)
    {
        $back_json = $this->where(['order_no' => $order_sn])->limit(1)->pluck('back_json')->shift();
        if (empty($back_json) or !is_json($back_json)) {
            return '';
        }
        $back_json = json_decode($back_json,true);
        if (array_key_exists('logNo', $back_json) and array_key_exists('settleDate', $back_json)) {
            return $back_json['logNo'] . $back_json['settleDate'];
        }
        return '';
    }


}