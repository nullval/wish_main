<?php

namespace App\Models;

use App\Jobs\handleDownloadQr;
use App\Libraries\JavaInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PushQrUser extends Model
{
    protected $table = 'wish_pushqr_user';

    /**
     * 登录
     *
     * @deprecated
     * @param $mobile
     * @param $password
     * @param string $openid
     * @return array
     */
    public function login($mobile, $password, $openid = '')
    {
        $info = $this->where(['mobile' => $mobile])->whereNotIn('level', [1])->first();
        if (empty($info)) {
            return arr_post(0, '该账号不存在');
        } else {
            if ($info->status == 0) {
                return arr_post(0, '您被禁止登录');
            } elseif ($info->password != md5(md5($password) + 'pushQr')) {
                return arr_post(0, '密码不正确');
            }
            if (!empty($openid)) {
                //绑定openid
                bind_openid($mobile, $openid);
            }
            //session存储用户信息
            session(['pushQr_user_info' => $info]);
            //登录成功记录信息
            return arr_post(1, '登录成功');
        }

    }

    /**
     * @param $operate_id
     * @param $mobile
     * @return mixed
     * 获取运营中心在地推用户里的用户id
     */
    public function find_operate_in_user($operate_id)
    {
        $mobile = AgentOperate::where(['id' => $operate_id])->value('mobile');
        $id = $this->where(['operate_id' => $operate_id, 'level' => 1])->value('id');
        if (!empty($id)) {
            return $id;
        }
        $arr = [
            'inviter_id' => 0,
            'mobile' => $mobile,
            'operate_id' => $operate_id,
            'status' => 1,
            'created_at' => time2date(),
            'level' => 1
        ];
        return $this->insertGetId($arr);
    }


    /**
     * @param $id 推荐人id
     * @param $mobile 手机号码
     * @param $level 等级 1运营中心 2地推市场部领导人 3地推人员
     * @param string $passsword 登录密码
     * @return array
     * 添加地推人员
     */
    public function add_user($id, $mobile, $level, $passsword = '')
    {
        //获取填写人信息
        $inviter_info = $this->where(['id' => $id])->first();
        if (empty($inviter_info->mobile)) {
            return arr_post(0, '推荐人不存在');
        } elseif ($inviter_info->mobile == $mobile) {
            return arr_post(0, '填写团队成员不能是自己');
        }
        $info = $this->where(['mobile' => $mobile])->first();
        if (isset($info->id)) {
            return arr_post(0, '该团队成员已存在');
        }
        if ($level >= $inviter_info) {
            return arr_post(0, '推荐关系不存在');
        }
        $arr = [
            'inviter_id' => $id,
            'mobile' => $mobile,
            'operate_id' => $inviter_info->operate_id,
            'status' => 1,
            'created_at' => time2date(),
            'level' => $level,
            'password' => md5(md5($passsword) + 'pushQr')
        ];
        $id = $this->insertGetId($arr);
        return arr_post(1, '创建成功');
    }

    /**
     * 判断该用户是否属于用户团队
     *
     * @param $inviter_id  推荐人
     * @param $user_id 用户
     * @return bool
     */
    public function is_belong_to($inviter_id, $user_id)
    {
        $inviter_arr = $this->find_down([$inviter_id]);
        if (!in_array($user_id, $inviter_arr)) {
            //不属于
            return false;
        }
        //属于
        return true;
    }

    /**
     * 获取矿主下面所有推荐人
     *
     * param $arr_uid_arr 矿主数组
     */
    public function find_down($arr_uid_arr = [])
    {
        static $arr = [];
        $agent_arr = $this->whereIn('inviter_id', $arr_uid_arr)->get(['id']);
        $agent_arr = norm_new_arr($agent_arr, 'id');
        if (!empty($agent_arr)) {
            $arr = array_merge($arr, $agent_arr);
            $this->find_down($agent_arr);
        }
        return $arr;
    }

    /**
     * 修改密码
     *
     * @param $uid
     * @param $old_password
     * @param $new_password
     * @return array
     */
    public function edit_password($uid, $old_password, $new_password)
    {
        $info = $this->where(['id' => $uid])->first();
        if (!isset($info->id)) {
            return arr_post(0, '用户不存在');
        }
        if ($info->password != md5(md5($old_password) + 'pushQr')) {
            return arr_post(0, '旧密码不正确');
        }
        $new_password = md5(md5($new_password) + 'pushQr');
        $this->where(['id' => $uid])->update(['password' => $new_password]);
        return arr_post(1, '修改成功');
    }


}
