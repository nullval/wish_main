<?php
namespace App\Models;

use App\Exceptions\ApiException;
use App\Libraries\Df\ght\Daifu;
use App\Libraries\gaohuitong\Ruzhu;
use App\Libraries\Huanxun\Huanxun;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use JPush\Exceptions\APIConnectionException;
use App\Libraries\Df\esyto;
class UserWithdraw extends CommonModel {
    protected $table = 'user_withdraw';
    //根据withdraw里的member_type键值找相应的owner_type和table 1用户 2商家 3矿主 4系统  5运营中心 6地推人员
    public $t_arr=[
        1=>[  //用户
            'owner_type'=>1,
            'table'=>'user',
            'name'=>'用户'
        ],
        2=>[ //商家
            'owner_type'=>2,
            'table'=>'seller',
            'name'=>'商家'
        ],
        3=>[ //机主
            'owner_type'=>5,
            'table'=>'agent',
            'name'=>'机主'
        ],
        4=>[ //系统
            'owner_type'=>3,
            'name'=>'管理员'
        ],
        5=>[ //运营中心
            'owner_type'=>11,
            'table'=>'agent_operate',
            'name'=>'运营中心'
        ],
        6=>[ //地推人员
            'owner_type'=>12,
            'table'=>'pushQr_user',
            'name'=>'地推人员'
        ],
    ];

    public function __construct($year=0,array $attributes = [])
    {
        parent::__construct($attributes);
        $year=empty($year)?date('Y'):$year;
        $this->table='user_withdraw_'.$year;
    }

    public function scopeDetail($query,$id,$field=['*'])
    {
        return $query->where(['id'=>$id])->first($field);
    }

    /**
     * @param $uid 用户id
     * @param $member_type 1用户 2商家 3矿主 4系统  5运营中心 6地推人员
     * @param $pursetype 钱包类型
     * @param $amount 提现金额
     * @param $is_cache 是否并发处理 1是 2否 (不限制即时到账)
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function sub_withdraw($uid,$member_type,$pursetype,$amount,$is_cache=1)
    {
        new_logger('sub_withdraw.log', 'ask', ['uid' => $uid, 'user_type' => $member_type, 'pursetype' => $pursetype, 'amount' => $amount]);
        //只能精准到两位
        $amount = get_num($amount);
        //获取银行卡信息
        if ($member_type == 5) {
            //运营中心与机主共用一张银行卡
            $agent_info = AgentOperate::leftJoin('agent', 'agent.mobile', '=', 'agent_operate.mobile')->where(['agent_operate.id' => $uid])->first(['agent.id']);
            if (isset($agent_info->id)) {
                $agent_uid = $agent_info->id;
            } else {
                $agent_uid = 0;
            }
            $bank_info = UserBank::where(['uid' => $agent_uid, 'type' => 3, 'isdefault' => 1])->first();
        } else {
            $bank_info = UserBank::where(['uid' => $uid, 'type' => $member_type, 'isdefault' => 1])->first();
        }
        if (!isset($bank_info->id)) {
            throw new ApiException('您还未补全您的银行卡信息');
        }
        if (empty($bank_info['ibankno']) || empty($bank_info['account_name']) || empty($bank_info['bank_account'])) {
            throw new ApiException('您还未补全您的银行卡信息');
        }

        $bank = new Bank();
        $owner_type = $this->t_arr[$member_type]['owner_type'];

        //检测用户提现
        $this->check_withdraw($pursetype, $amount, $bank_info['bank_account'], $uid, $owner_type, $member_type);
        //文件缓存处理并发
        if (cache_lock('withdraw_' . $uid . $member_type . $pursetype, $is_cache, 0.1) == true) {
            throw new ApiException('操作频繁');
        }
        $bank_code = '0000';
        $spare_order_sn = time() . rand(1000, 9999);
        //手续费在第三方扣除了 这边只做了记录
//        if($member_type==1){
//            //用户 不扣手续费
//            //加此字段不收手续费
//            if($pursetype==12){
//                //红包收益扣3块
//                $d_poundage=3;
//            }elseif($pursetype==1){
//                //用户收益提现(货款)扣10%
//                $d_poundage=$amount*0.1;
//            }else{
//                //其余暂定扣10%
//                $d_poundage=$amount*0.1;
//            }
//        }elseif($member_type==2||$member_type==6){
//            //其余暂定扣3块
//            if($pursetype==1){
//                //商家收益提现扣3
//                $d_poundage=3;
//            }else{
//                //其余暂定扣10%
//                $d_poundage=$amount*0.1;
//            }
//        }elseif ($member_type==3||$member_type==5){
//            //机主运营中心扣20
//            $d_poundage=3;
//        }
        if($pursetype==7&&$member_type==2){
            //2018/03/30扣除商家充值钱包0.6%+3
            $d_poundage=$amount*0.006+3;
            //小于3块 按3块扣
            $d_poundage=$d_poundage<3?3:$d_poundage;
        }else {
            $d_poundage = 3;
        }


        //验证第三方代付钱包是否足够
        //实际请求金额
        $ask_amount = $amount - $d_poundage;

        //获取用户代付类型
        if ($pursetype == 7 && $member_type == 2) {
            //商家充值
            $pay_method = SysConfig::where(['attr_name' => 'withdraw_recharge'])->value('attr_value');
        } else {
            $pay_method = SysConfig::where(['attr_name' => 'withdraw_' . $member_type])->value('attr_value');
        }

        $method = explode('_', $pay_method)[0];

        if (empty($method)||(!method_exists($this,$method))) {
            throw new ApiException('代付方式不存在');
        }

        //时间限制
        if (limit_time("09:00","22:00")&&($method=='gaohuitong')) {
            //高汇通是早九晚十
            throw new ApiException('提现时间为早上9点到晚上10点。');
        }
        if (limit_time("07:00","20:00")&&($method=='tonglian')) {
            //通联是早七晚八
            throw new ApiException('提现时间为早上7点到晚上8点。');
        }

        //请求代付
        $pay_result = $this->$method($ask_amount, $uid, $pursetype, $amount, $owner_type, $bank_info, $spare_order_sn, $member_type,1);



        //代付标识
        $withdraw_data['pay_method']=$pay_method;
        //处理成功(一定成功) 有问题再看记录自查补单
        $withdraw_data['user_id']=$uid;
        $withdraw_data['amount']=$amount;
        $withdraw_data['his_id']=$pay_result['hisid'];
        $withdraw_data['status']=1;
        $withdraw_data['bank_account']=$bank_info['bank_account'];
        $withdraw_data['account_bank']=$bank_info['account_bank'];
        $withdraw_data['ibankno']=$bank_info['ibankno'];
        $withdraw_data['created_at']=time2date();
        $withdraw_data['d_poundage']=$d_poundage;
        $withdraw_data['actual_amount']=$withdraw_data['amount']-$withdraw_data['d_poundage'];
        $withdraw_data['account_name']=$bank_info['account_name'];
        $withdraw_data['bank_code']=$bank_code;
        $withdraw_data['spare_order_sn']=$spare_order_sn;
        $withdraw_data['member_type']=$member_type;
        $withdraw_data['purse_type']=$pursetype;
        //记录第三方订单号
        $withdraw_data['detail']=$pay_result['result'];
        $this->insert($withdraw_data);
        return true;
    }

    /**
     * @param $spare_order_sn 订单编号
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function issue_withdraw($spare_order_sn)
    {

        //防止测试提现申请
        ban_test();
        $withdraw_info=VUserWithdraw::where(['spare_order_sn'=>$spare_order_sn])->first();
        if(!isset($withdraw_info->id)){
            throw new ApiException('提现记录丢失');
        }
        $uid=$withdraw_info->user_id;
        $member_type=$withdraw_info->member_type;
        $amount=$withdraw_info->amount;
        $d_poundage=$withdraw_info->d_poundage;
        $method=explode('_',$withdraw_info->pay_method)[0];
        $year=$withdraw_info->created_at->format('Y');
        if ($member_type == 5) {
            //渠道商与益邦客共用一张银行卡
            $agent_info = AgentOperate::leftJoin('agent', 'agent.mobile', '=', 'agent_operate.mobile')->where(['agent_operate.id' => $uid])->first(['agent.id']);
            if (isset($agent_info->id)) {
                $agent_uid = $agent_info->id;
            } else {
                $agent_uid = 0;
            }
            $bank_info = UserBank::where(['uid' => $agent_uid, 'type' => 3, 'isdefault' => 1])->first();
        } else {
            $bank_info = UserBank::where(['uid' => $uid, 'type' => $member_type, 'isdefault' => 1])->first();
        }
        if (!isset($bank_info->id)) {
            throw new ApiException('您还未补全您的银行卡信息');
        }
        if (empty($bank_info['ibankno']) || empty($bank_info['account_name']) || empty($bank_info['bank_account']) || empty($bank_info['identity']) || empty($bank_info['phone'])) {
            throw new ApiException('您还未补全您的银行卡信息');
        }

        $ask_amount=$amount-$d_poundage;
        $new_spare_order_sn='00'.$spare_order_sn;
        //请求第三方
        $this->$method($ask_amount, 0, 0, 0, 0, $bank_info, $new_spare_order_sn, 0,0);

        DB::table('user_withdraw_'.$year)->where(['spare_order_sn'=>$spare_order_sn])->update(
            [
                'bank_account'=>$bank_info['bank_account'],
                'account_bank'=>$bank_info['account_bank'],
                'ibankno'=>$bank_info['ibankno'],
                'account_name'=>$bank_info['account_name'],
                'spare_order_sn'=>$new_spare_order_sn,
                'created_at'=>time2date(),
                'updated_at'=>time2date(),
                'detail'=>'用户于'.$withdraw_info->created_at.'卡号填错,于'.date('Y-m-d H:i:s').'重新申请提交提现'
            ]
        );

        return true;
    }

    /**
     * @param $ask_amount 请求第三方支付金额 单位元
     * @param $uid 用户id
     * @param $pursetype 钱包类型
     * @param $amount 用户请求系统金额 单位元
     * @param $owner_type 用户类型
     * @param $bank_info 用户银行信息
     * @param $spare_order_sn 订单号
     * @param $member_type 用户类型
     * 环讯代付
     * 返回冻结记录编号
     */
    public function huanxun($ask_amount,$uid,$pursetype,$amount,$owner_type,$bank_info,$spare_order_sn,$member_type,$is_freeze=1){
        $df = new esyto\esyto(false);
        $balance = $df->queryBalance()['availAmt']; //单位元
        if ((!$balance) || ($balance <= ($ask_amount + 10))) {
            throw new ApiException('第三方资金不足');
        }
        if($is_freeze==1){
            $bank=new Bank();
            //冻结提现金额 (默认收益钱包)
            $hisid=$bank->doFreeze($uid,$pursetype,$amount,'提现冻结',$owner_type);
        }
        $data = [
            'order_sn' =>$spare_order_sn, //商户平台订单号
            'subject' => '代付提现', //收款人姓名
            'amount' => $ask_amount, // 单位元
            'acctNo' => $bank_info['bank_account'], //收款人卡号
            'accBankName' => $bank_info['account_name'], //收款人手机号码
            'bankname' => $bank_info['account_bank'], //
        ];
        $result = $df->pay($data);

        return [
            'hisid'=>$hisid,
            'result'=>$result
        ];
    }

    /**
     * @param $ask_amount 请求第三方支付金额 单位元
     * @param $uid 用户id
     * @param $pursetype 钱包类型
     * @param $amount 用户请求系统金额 单位元
     * @param $owner_type 用户类型
     * @param $bank_info 用户银行信息
     * @param $spare_order_sn 订单号
     * @param $member_type 用户类型
     * 高汇通代付
     * 返回冻结记录编号
     */
    public function gaohuitong($ask_amount,$uid,$pursetype,$amount,$owner_type,$bank_info,$spare_order_sn,$member_type,$is_freeze=1){
        //商家和消费者走高汇通
        $df=new Daifu();
        $balance = $df->queryBalance()['BALANCE']; //单位分
        if ((!$balance) || ($balance <= ($ask_amount + 2) * 100)) {
            throw new ApiException('请联系平台客服');
        }

        if($is_freeze==1){
            $bank=new Bank();
            //冻结提现金额 (默认收益钱包)
            $hisid=$bank->doFreeze($uid,$pursetype,$amount,'提现冻结',$owner_type);
        }

        $param_body = [
            'BANK_CODE' => substr($bank_info['ibankno'], 0, 3),//'102',
            'ACCOUNT_NO' => $bank_info['bank_account'], //收款人卡号
            'ACCOUNT_NAME' => $bank_info['account_name'], //收款人姓名,
            'AMOUNT' => $ask_amount * 100, // 单位分
            'REMARK' => '代付提现',        // 备注，必填
        ];
        $result=$df->pay($spare_order_sn, $param_body);

        return [
            'hisid'=>$hisid,
            'result'=>$result
        ];
    }




    /**
     * @param $pursetype 钱包类型
     * @param $amount 提现金额 单位元
     * @param $bank_account 银行卡号
     * @param $uid 用户id
     * @param $owner_type 提现钱包用户类型
     * @param $member_type 用户类型 1用户 2商家 3矿主 4系统  5运营中心 6地推人员
     * 检测用户是否允许提现
     */
    public function check_withdraw($pursetype,$amount,$bank_account,$uid,$owner_type,$member_type){
//        throw new ApiException('代付通道正在开通中,暂停提现');
        $use_pursetype=[1,3,4,7,12];
        if(!in_array($pursetype,$use_pursetype)){
            throw new ApiException('不支持该币种提现。');
        }
        if($amount>50000||$amount<3){
            throw new ApiException('单笔提现不得低于3元超过50000元。');
        }
        //判断用户的提现金额
        $withdraw_limit_value=SysConfig::where(['attr_name'=>'withdraw_limit_value'])->value('attr_value');
        if ($amount>$withdraw_limit_value){
            throw new ApiException('每次提现不能超出'.$withdraw_limit_value.'元!');
        }
        //对提现卡号金额和提现次数进行判断
        $bank_where=$this->where(['bank_account'=>$bank_account])->whereDate('created_at', date('Y-m-d'));
        $total_sum=$bank_where->sum('amount');
        $total_count=$bank_where->count();
        if($total_sum>50000){
            throw new ApiException('应银联规则,当天同一卡号提现金额不得超过50000元。');
        }elseif(($total_sum+$amount)>50000){
            throw new ApiException('应银联规则,当天同一卡号提现金额不得超过50000元,该卡号当天剩余提现额度为:'.floor(49999-$total_sum).'元');
        }
        if($total_count>10){
            throw new ApiException('应银联规则,当天同一卡号提现次数不能超过10笔。');
        }
        $bank=new Bank();
        //获取用户钱包
        $userpurse=$bank->userWallet($uid,$pursetype,$owner_type);
        $user_amount=$bank->use_money($uid,$pursetype,$owner_type);
        if($user_amount<$amount){
            throw new ApiException('资金不足');
        }
        $transfer=new Transfer();
        $result=$transfer->IsEnough($userpurse->purse_id,$amount);
        if ($result==false){
            throw new ApiException('余额不足');
        }
        //判断是否允许用户提现
        $table=$this->t_arr[$member_type]['table'];
        $user_info=DB::table($table)->where(['id'=>$uid])->first(['is_allow_withdraw','status']);
        $is_allow_withdraw=$user_info->is_allow_withdraw;
        $status=$user_info->status;
        if($is_allow_withdraw==2||empty($status)){
            throw new ApiException('亲，您的帐户有异常，系统自动禁用此功能，等管理员查证后才能重新启用！');
        }
        if($member_type==1){
            //用户限制每天提一次
            if($pursetype==1){
//                throw new ApiException('因新系统上线，代付通道处于测试期，为避免数据错误，平台将有专人联系您，跟您核对数据并处理相关事宜！');
                //收益提现
                $count=$this->where(['user_id'=>$uid,'member_type'=>1])->whereDate('created_at', date('Y-m-d'))->count();
                if($count>=2){
                    throw new ApiException('每个用户每天只能提现一次');
                }
                if($uid!=86&&$uid!=58){
                    if($amount<100){
                        throw new ApiException('提现金额不能少于100');
                    }
                }
            }elseif ($member_type==12){
                //红包收益
                if($amount<100){
                    throw new ApiException('提现金额不能少于100');
                }
            }
        }elseif($member_type==2||$member_type==3||$member_type==5||$member_type==6){
            if($member_type==2&&$uid==54){
                throw new ApiException('禁止管理商家提现');
            }
            if($member_type==2&&$amount<100){
                throw new ApiException('提现金额不能少于100');
            }elseif($member_type!=2&&$amount<100){
                throw new ApiException('提现金额不能少于100');
            }
        }
        //用户（商家和消费者）的收益积分每日每人限提现1000元
        if($pursetype==1){
            $bank_where=$this->where(['member_type'=>$member_type,'purse_type'=>$pursetype,'user_id'=>$uid])->whereDate('created_at', date('Y-m-d'));
            $total_sum=$bank_where->sum('amount');
            $limit_amount=1000;
            if($total_sum>$limit_amount){
                throw new ApiException('当天提现额度已超过'.$limit_amount.'元。');
            }elseif(($total_sum+$amount)>$limit_amount){
                throw new ApiException('当天累计提现额度不能超过'.$limit_amount.'元,当天剩余提现额度为:'.floor($limit_amount-$total_sum).'元');
            }
        }
        //防止测试提现申请
        ban_test();
        return true;
    }



}