<?php

namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;

class Agent extends Model
{
	protected $table = 'wish_agent';
    
    /**
     * @param $mobile 代理手机号
     * @param $sms_code 短信验证码
     * @param $openid openid 与积分商城做关联使用
     * @param $menber_type menber_type 身份类型 1、机主 2、运营中心
     * @return bool
     * 业务员后台登录
     */
    public function agent_login($mobile,$sms_code,$openid=0,$menber_type){

        $agent_info=$this->where(['agent.mobile'=>$mobile])
            ->first();
        if(empty($agent_info)){
            $error=$menber_type==1?'机主不存在':'你还未成为机主,无法进入运营中心';
            throw new ApiException($error);
        }else{
            if((empty($agent_info->inviter_id))&&($agent_info->is_admin==0)){
                $error=$menber_type==1?'您还未成为机主':'你还未成为机主,无法进入运营中心';
                throw new ApiException($error);
            }
            if(!empty($agent_info->openid)){
                throw new ApiException('该账号已绑定其他微信号');
            }
            if($agent_info->status==0){
                throw new ApiException('您被禁止登录');
            }
            if ($menber_type == 2){
                $operate=AgentOperate::where(['mobile'=>$agent_info->mobile])->first();
                if(empty($operate)){
                    throw new ApiException('你还未成为运营中心');
                }
            }



            //验证短信验证码
            $Message=new Message();
            if($menber_type==1||empty($menber_type)){
                //机主
                $d_type=2;
            }elseif($menber_type==2){
                //运营中心
                $d_type=14;
            }
            $Message->check_sms($mobile,$sms_code,$d_type);
            if($menber_type==2){
                //初始化运营中心地推身份
                $info=PushQrUser::where(['level'=>1,'mobile'=>$agent_info->mobile])->first();
                session(['pushQr_user_info'=>$info]);
            }
            if(!empty($openid)){
                //绑定openid
                bind_openid($mobile,$openid);
            }
            $this->where(['id'=>$agent_info->id])->update(['last_time'=>time2date()]);
            //session存储用户信息
            session(['agent_info'=>$agent_info]);
            //登录成功记录信息
            return true;

        }
    }


    /**
     * @param $uid 用户id
     * @param $password 登录密码
     * @return array
     * 验证密码
     */
    public function check_password($uid,$password){
        $password=md5(md5($password));
        $rs=$this->where(['id'=>$uid,'password'=>$password])->first();
        return empty($rs)?arr_post(0,'密码不正确'):arr_post(1,'密码正确');
    }

    /**
     * @param $uid 用户id
     * @param $password
     * @param $password_name f_password 登录密码 s_password 查询密码 t_password 支付密码
     * @return array
     * 更新密码
     */
    public function update_password($id,$password,$password_name){
        $password=md5(md5($password));
        return $this->update_user_info($id,[$password_name=>$password]);
    }


    /**
     * 修改密码
     * @param $request
     * @return mixed
     */
    public function agent_change_pwd($request){
        //获取用户信息
        $info=$this->where(['id'=>session('agent_info')->id])->first();
        //验证判断
        if (md5(md5($request['old_password']).'tdzd')!=$info->password){
            return arr_post(0,'旧登录密码不正确！');
        }
        //更改密码
        $password=md5(md5($request['password']).'tdzd');
        $result=$this->where(['id'=>session('agent_info')->id])->update(['password'=>$password]);
        return arr_post(1,'密码修改成功！');

    }


    /**
     * 新增机主
     * @param $mobile 机主手机号
     * @return mixed
     */
    public function add_agent($id=0,$mobile,$invite_mobile,$operate_mobile,$remark_name,$level=0){
        //判断机主是够存在，不存在则添加
        $agent=$this->where(['mobile'=>$mobile])->first();
        if (!empty($agent)&&(empty($id))){
            throw new ApiException('该机主已存在了！');
        }
        if(!empty($invite_mobile)){
            if($invite_mobile==$mobile){
                throw new ApiException('推荐人不能是自己！');
            }
            $inviter_id=Agent::where(['mobile'=>$invite_mobile])->value('id');
            if(empty($inviter_id)){
                throw new ApiException('推荐人不存在！');
            }
            $agent_arr['inviter_id']=$inviter_id;
        }
        if(!empty($operate_mobile)){
            $operate_id=AgentOperate::where(['mobile'=>$operate_mobile])->value('id');
            if(empty($operate_id)){
                throw new ApiException('运营中心不存在！');
            }
            $agent_arr['operate_id']=$operate_id;
        }
        $agent_arr['created_at']=time2date();
        $agent_arr['is_back']=1;
        $agent_arr['mobile']=$mobile;
        $agent_arr['name']=$remark_name;
        $agent_arr['level']=$level;
        $agent_arr['remark']=date('Y/m/d').'号手动添加 '.$remark_name.'没有买单';
        if(!empty($id)){
            $this->where(['id'=>$id])->update($agent_arr);
        }else{
            $id=$this->insertGetId($agent_arr);
        }
        return $id;

    }


    /*
    * param $current当前层次
    * param $level总共要到几层
    */
    public function find_up($uid, $current=1 , $level=20,$name){
        static $arr=[];
        $agent_info=$this->where(['id'=>$uid])->first(['inviter_id']);
        if(isset($agent_info->inviter_id)){
            $current++;
            if($current <= $level){
                $arr[$name][]=$inviter_id=$agent_info->inviter_id;
                //            var_dump($arr);
                $this->find_up($inviter_id,$current,$level,$name);
            }

        }
        return $arr[$name];
    }

    /*
     * param $arr_uid_arr 机主数组
   * 获取机主下面所有推荐人
   */
    public function find_down($arr_uid_arr=[]){

        static $arr=[];
        $agent_arr=$this->whereIn('inviter_id',$arr_uid_arr)->get(['id']);
        $agent_arr=norm_new_arr($agent_arr,'id');
        if(!empty($agent_arr)){
                $arr=array_merge($arr,$agent_arr);
//                var_dump($agent_arr);exit;
                //            var_dump($arr);
                $this->find_down($agent_arr);
        }
        return $arr;
    }


    /**
     * @param $uid
     * @return int|mixed
     * 向上找代理
     */
    public function find_proxy($uid){
        $agent_info=Agent::where(['id'=>$uid])->first(['id','level','inviter_id']);
        if(!isset($agent_info->id)){
            return 0;
        }elseif($agent_info->level>=3){
            return $agent_info->id;
        }else{
            return $this->find_proxy($agent_info->inviter_id);
        }
    }

    public function update_agent_invite($uid){
        //获取推荐人数组
//        var_dump($uid);
        $inviter_arr=array_filter($this->find_up($uid,1,10,$uid));
//        var_dump($inviter_arr);

        //此位置固定好
//        if (empty($inviter_id)) {
            foreach ($inviter_arr as $k=>$v){
                $invite_floor=$k+1;
                //绑定到机主推荐表
                $agent_invite_info=AgentInviteFloor::where(['agent_uid'=>$v,'floor'=>$invite_floor])->first();
                if(empty($agent_invite_info)){
                    $rs[]=AgentInviteFloor::insert(['agent_uid'=>$v,'floor'=>$invite_floor,'inviter_id_arr'=>$uid]);
                }else{
                    $inviter_id_arr=$agent_invite_info->inviter_id_arr.','.$uid;
                    $rs[]=AgentInviteFloor::where(['agent_uid'=>$v,'floor'=>$invite_floor])->update(['inviter_id_arr'=>$inviter_id_arr]);
                }
            }
//        }
    }


    /**
     * @param $mobile 机主手机号
     * @return array
     * 添加机主
     */
    public function add_or_create_agent($mobile){
        $agent=$this->where(['mobile'=>$mobile])->first();
        if (!empty($agent)){
            $is_new=1;
            $agent_id=$agent->id;
        }else{
            $is_new=0;
            $agent_arr['created_at']=time2date();
            $agent_arr['mobile']=$mobile;
            $agent_id=$this->insertGetId($agent_arr);
        }
        $data['agent_id']=$agent_id;
        $data['is_new']=$is_new;
        return arr_post(1,'新增成功！',$data);
    }

    /**
     * @param $agent_id
     * @param $inviter_id
     * 机主绑定推荐人/及运营中心
     */
    public function bind_inviter($agent_id,$inviter_id){
        //获取机主信息
        $agent=$this->where(['id'=>$agent_id])->first();
        //若无推荐人则绑定
        if(empty($agent->inviter_id)){
            //获取推荐人信息
            $inviter_info=$this->where(['id'=>$inviter_id])->first();
            //绑定推荐人和运营中心
            Agent::where(['id' => $agent_id])->update(['inviter_id' => $inviter_id,'operate_id'=>$inviter_info->operate_id]);
        }
        return true;
    }


	
	
	/**
	 * 按时间节点往上找18层
	 * 判断：此用户下面有人，就往上找9层，往下找9 层，分成1/18份平分
	 * 否则：此用户下面没人，就往上找18个，不满足18个人不分，满足18个就 1/18 份平分
	 * @param $agent_id		// 机主ID，根据这个ID查找上下级关系
	 * @param $amount 		// 分账金额，均分18份
	 * @return array
	 */
    public function parentTime($agent_id,$amount)
	{
		$id = $agent_id;
		$return = [];
		if($amount >= 0.18){
			$return18 = collect([]);
			$floor = $this->where('id', '>', $id)->where('inviter_id', '>', 0)->where(DB::raw('left(mobile,1)'),'=','1')->limit(9)->orderBy('id','asc')->pluck('id');
			$ceil = $this->where('id', '<', $id)->where('inviter_id', '>', 0)->where(DB::raw('left(mobile,1)'),'=','1')->limit(18)->orderBy('id', 'desc')->pluck('id');
			if ($floor->count() > 0) {
				$return18 = $ceil->slice(0, 9)->merge($floor);
			} else {
				if ($ceil->count() >= 18) {
					$return18 = $ceil;
				}
			}
			// 返回需要分配的人员名单，外层平分好金额，循环转账即可
			$return18->each(function($id) use (&$return,$amount){
				$return[] = [
					'user_id'	=> $id,
					'amount'	=> round(floor($amount / 18 * 100) / 100,2),
				];
			});
		}
		return $return;
	}


    /*
   * param $current当前层次
   * param $level总共要到几层
     * 6度人脉
   */
    public function find_six_up($uid, $current=1 , $level=100){

        global $six_arr;
        $agent_info=Agent::where(['id'=>$uid])->first(['inviter_id']);
        if(isset($agent_info->inviter_id)){
            $current++;
            if($current <= $level){
                $inviter_id=$agent_info->inviter_id;

                //实时统计本人下级推荐人及推荐人下级是否>=6是则有效
                //获取推荐人下级推荐人
                $level1=Agent::where(['inviter_id'=>$inviter_id])->get(['id']);
                $count1=count($level1);
                $count2=0;
                $arr=norm_new_arr($level1,'id');
                $arr[]=$inviter_id;
                //获取7天内团队购买的未特商城pos机
                if(!empty($arr)){
                    //7天前
                    $pass_day=date("Y-m-d H:i:s",strtotime("-7 day"));
                    $pos_count=VOrder::whereIn('buyer_id',$arr)
                        ->where(['status'=>2,'type'=>2])
                        ->where('created_at','>',$pass_day)
                        ->count();
                }else{
                    $pos_count=0;
                }

                $sum_count=$count1+$count2;
//                var_dump($inviter_id);
//                var_dump($sum_count);
//                var_dump($pos_count);
//                var_dump('--------');
//                var_dump($inviter_id);
//                var_dump($arr);
//                var_dump($count1);
//                var_dump($count2);
//                var_dump($sum_count);
//                var_dump('--------');
                if($sum_count>=3&&$pos_count>=1){
                    $six_arr[]=$inviter_id;
                }
                $this->find_six_up($inviter_id,$current,$level);
            }
        }
        return $six_arr;
    }

    /**
     * @param $agent_uid 机主id
     * @param $mobile 手机号码
     * @param $amount 冻结额度
     * @param $order_sn 订单号码
     * @return array
     * 购买pos机冻结购买人积分
     */
    public function buy_pos_free_agent_point($agent_uid,$mobile,$amount,$order_sn){
        $Bank=new Bank();
        $User=new User();
        $uid=$User->get_user_id_by_mobile($mobile);
        $hisid=$Bank->doFreeze($uid,3,$amount,'购买pos机积分冻结',1);
        $pos_freeze_point['hisid']=$hisid;
        $pos_freeze_point['order_sn']=$order_sn;
        $pos_freeze_point['amount']=$amount;
        $pos_freeze_point['agent_uid']=$agent_uid;
        $pos_freeze_point['uid']=$uid;
        $pos_freeze_point['created_at']=time2date();
        $pos_freeze_point['status']=1;
        $pos_freeze_point['remark']='购买pos机购买机主积分冻结';
        PosFreezePoint::insert($pos_freeze_point);
        return arr_post(1,'冻结成功');
    }


    /**
     * @param string $order_sn 订单号
     * @return array
     * 返还机主购买pos机冻结的500积分
     */
    public function is_get_point_freeze($order_sn=''){
        //两种方式 1自己已有商家,自己进行复购 2一个月内至少推荐一个商家,并完成流水 一个商家对应一台pos机
        //获取订单信息
        $order_info=VOrder::where(['order_sn'=>$order_sn])->first();
        if(isset($order_info->order_sn)){
            $free_info=PosFreezePoint::where(['order_sn'=>$order_sn])->first();
            $Bank=new Bank();
            //是否返还积分给机主
            $is_give=0;
            //订单生成时间
            $created_at=$order_info->created_at->format('Y-m-d H:i:s');
            //生成订单后30天内的时间
            $future_30_day=date("Y-m-d H:i:s",strtotime("+30 day",strtotime($created_at)));

            $hisid=$free_info->hisid;
            $agent_uid=$free_info->agent_uid;
            $free_status=$free_info->status;
            if($agent_uid!=$order_info->buyer_id){
                return arr_post(0,'订单无效');
            }
            if($free_status!=1){
                return arr_post(0,'订单已处理');
            }

            //1.查询自己是否有复购 (优先级1)
            //查看是否有推荐商家
            $invite_seller_info=Seller::where(['inviter_id'=>$agent_uid])->first();
            if(isset($invite_seller_info->id)){
                //已使用过的复购订单
                $used_buy_pos_order_arr=norm_new_arr(PosFreezePoint::where(['agent_uid'=>$agent_uid,'status'=>2,'seller_uid'=>0])->get(['buy_pos_order_sn']),'buy_pos_order_sn');
                $be_use_order_info=VOrder::where(['status'=>2,'type'=>2,'buyer_id'=>$agent_uid,'is_true'=>1])
                    ->where('created_at','>',$created_at)
                    ->where('created_at','<',$future_30_day)
                    ->whereNotIn('order_sn',$used_buy_pos_order_arr)
                    ->first();
                if(isset($be_use_order_info->order_sn)){
                    //返还
                    $is_give=1;
                    $log['buy_pos_order_sn']=$be_use_order_info->order_sn;
                    $log['updated_at']=time2date();
                    $log['status']=2;
                    $log['remark']='机主自己复购解冻积分';
                }
            }

            if($is_give==0){
                //2.查询一个月内是否推荐商家,并完成流水 (优先级2)
                //已使用过的商家
                $used_seller_arr=norm_new_arr(PosFreezePoint::where(['agent_uid'=>$agent_uid,'status'=>2])->where('seller_uid','>',0)->get(['seller_uid']),'seller_uid');
                //获取机主推荐的商家(去除已使用过的商家)
                $seller_arr=norm_new_arr(Seller::where(['inviter_id'=>$agent_uid])->whereNotIn('id',$used_seller_arr)->get(['id']),'id');
                $be_use_order_info=VOrder::where(['status'=>2])
                    ->whereIn('type',[1,3])
                    ->whereIn('uid',$seller_arr)
                    ->where('created_at','>',$created_at)
                    ->where('created_at','<',$future_30_day)
                    ->first();
                if(isset($be_use_order_info->id)){
                    //返还
                    $is_give=1;
                    $log['seller_uid']=$be_use_order_info->uid;
                    $log['updated_at']=time2date();
                    $log['status']=2;
                    $log['remark']='机主30天内推荐了商家,该商家完成了消费,解冻积分';
                }
            }

            //返还积分给机主
            $y=$order_info->created_at->format('Y');
            if(time()<strtotime($future_30_day)&&$is_give==1){
                DB::transaction(function () use($hisid,$y,$order_sn,$log){
                    $Bank=new Bank();
                    $Bank->unFreeze($hisid,$y);
                    PosFreezePoint::where(['order_sn'=>$order_sn])->update($log);
                });
            }

            //过期返回给系统
            if(time()>strtotime($future_30_day)){
                DB::transaction(function ($hisid,$y,$order_sn){
                    $Bank=new Bank();
                    //获取机主用户积分钱包
                    //$point_purse_id=$Bank->userWallet($free_info->uid,3,1)->purse_id;
                    //获取系统积分钱包
                    //$sys_purse_id=$Bank->get_sys_purse(3)->purse_id;
                    $rs[]=$Bank->unFreeze($hisid,$y);
                    //$rs[]=$Bank->doTransfer($point_purse_id,$sys_purse_id,$free_info->amount,10066,
                    //    '机主购买未特商城pos机后一个月内未推荐了商家使用未特商城pos机,也未复购,系统回收机主积分,订单编号:'.$order_sn,'系统回收机主积分');
                    $log1['updated_at']=time2date();
                    $log1['status']=3;
                    $log1['remark']='机主购买未特商城pos机后一个月内未推荐了商家使用未特商城pos机,也未复购,自动返还';
                    $rs[]=PosFreezePoint::where(['order_sn'=>$order_sn])->update($log1);
                });
            }

            return arr_post(1,'订单未处理成功');
        }
        return arr_post(0,'订单不存在');
    }
	
	/**
	 * 递归查找父级
	 */
    public function parents(){
    	$data = collect();
    	$inviter_id = $this->getAttribute('inviter_id');
		$find = $this->where(['id'=>$inviter_id])->first();
		if($find){
			$data2 = $find->parents();
			$data = $data2->push($find);
		}
		return $data;
	}
	
	/**
	 * 新人进来就判断往上是不是7个人，迁移至 AgentSevenToHalfModel
	 * @param int $count 最大往上找的人数
	 * @return mixed
	 */
//	public function parentSeven(int $count = 7){
// 		$data = collect();
//		$inviter_id = $this->getAttribute('inviter_id');
//		$parents = $this->where(['inviter_id'=>$inviter_id])->where('created_at','>','2017-01-01')->get();
//		if($parents->isNotEmpty() && $count > 0){
//			$parents->each(function($v) use (&$data,$count,$parents){
//				$find = $this->where(['id'=>$v->inviter_id])->first();
//				if($find){
//					$data2 = $find->parentSeven($count - 1);
//					$data = $data2->merge($parents);
//				}
//			});
//		}
//		return $data;
//	}
}
