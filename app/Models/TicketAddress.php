<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketAddress extends Model
{
	protected $table = 'ticket_address';
}
