<?php

namespace App\Models;

use App\Exceptions\ApiException;
use App\Exceptions\NotLoginException;
use App\Libraries\rong_cloud_sdk\Rongyun;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class UserTicket extends CommonModel
{
	protected $table = 'user_ticket';
	
	/**
	 * @param $uid
	 * @return bool|string
	 * 登录操作，存入ticket
	 */
	public function login($uid){
		$ticket = uuid();
		// 登录就刷新融云token
		$rongyun = new Rongyun();
		$user = DB::table('user')->where(['id'=>$uid])->first();
		if(env('IS_ONLINE')==true){
			//获取融云token
			$rong = $rongyun->user()->getToken($uid,$user->username,$user->avatar);
			$rong_token = $rong['token'];
		}else{
			$rong_token=rand(1,99999);
		}
		$add = [
			'uid'			=> $uid,
			'session_id'	=> session_id(),
			'ticket'		=> $ticket,
			'rongcloud_token'=> $rong_token,
			'status'		=> 0,
			'ip'			=> request()->ip(),
			'client_system'	=> request('ClientSystem'),
			'client_version'=> request('ClientVersion'),
			'api_version'	=> request('ApiVersion','1.0'),
			'timeout'		=> request()->server('REQUEST_TIME',time()) + 3600 * 3,
		];
		$var = $this->create($add);
		return $var ? $ticket : false;
	}
	
	/**
	 * @param $uid	// 如果传递就全部下线，否则只退出当前ticket
	 * 登出操作，设置状态为已下线
	 */
	public function logout($uid = 0){
		$ticket = request('ticket');
		if($uid){
			$var = $this->where(['uid'=>$uid])->update(['status'=>2]);
		}else{
			$var = $this->where(['ticket'=>$ticket])->update(['status'=>2]);
		}
		return $var;
	}
	
	/**
	 * @param $query
	 * @return mixed
	 * @throws ApiException
	 * 根据ticket查询状态
	 */
	public function checkLogin(){
		$ticket = request('ticket');
		$exist = $this->where(['ticket'=>$ticket])->first();
		if(empty($exist)){
		    throw new ApiException('用户凭证验证失败');
		}elseif($exist->status == 2){
            throw new ApiException('用户凭证已失效');
		}elseif($exist->status == 0){
			$user = User::select(DB::raw('id,mobile'))->where(['id'=>$exist->uid])->first();
			return $user;
		}else{
            throw new ApiException('用户凭证数据状态无效');
		}
	}
}
