<?php

namespace App\Models;

use App\Exceptions\ApiException;
use function foo\func;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;


class Seller extends Model
{
    protected $table = 'wish_seller';


    /**
     * @param $mobile 商家手机号
     * @param $password 登录密码
     * @param $inviter_id 邀请人id
     * @return bool
     * 商家注册
     */
    public function seller_register($mobile, $password, $openid)
    {
        //判断该身份下是否已存在
        $user_info = $this->where(['mobile' => $mobile])->first(['id']);
        if (isset($user_info->id)) {
            return arr_post(0, '该手机号已被注册商家');
        }
        $reg['mobile'] = $mobile;
        $reg['password'] = md5(md5($password) . 'tdzd');
        $reg['created_at'] = time2date();
        $reg['last_time'] = time2date();
        $reg['is_bind_phone'] = 2;
        $user_info = DB::transaction(function () use ($reg) {
            //1插入用户表
            $user_id = $this->insertGetId(filter_update_arr($reg));
            $user_info = $this->where(['id' => $user_id])->first();
            //2插入商家申请表
            MerchantApply::insert(['uid' => $user_info->id]);
            return $user_info;
        });

        if (!empty($openid)) {
            //绑定openid
            bind_openid($mobile, $openid);
        }
        //session存储代理信息
        session(['agent_info' => $user_info]);
        return arr_post(1, '注册成功');

    }


    /**
     * @param $mobile 商家手机号
     * @param $sms_code 短信码
     * @param $openid openid 与积分商城做关联使用
     * @return bool
     * 商家后台登录
     */
    public function seller_login($mobile, $sms_code, $openid = '')
    {
        //验证短信验证码
        $Message = new Message();
        $Message->check_sms($mobile, $sms_code, 5);
        //判断是否店长登录
        $user_info = $this->where(['mobile' => $mobile])->first();
        if (empty($user_info)) {
            //否
            //判断是否是操作员登录
            $role_info = SellerRole::where(['mobile' => $mobile])->first();
            if (!empty($role_info)) {
                $user_info = $this->where(['id' => $role_info->seller_id])->first();
                SellerRole::where(['mobile' => $role_info->mobile])->update(['lo_time' => time2date()]);
                $user_new_info = User::where(['mobile' => $mobile])->first();
                if (!isset($user_new_info->id)) {
                    User::insert(['mobile' => $mobile, 'openid' => $openid, 'created_at' => time2date()]);
                }
                User::where(['mobile' => $mobile])->update(['openid' => $openid]);
                //session存储商家用户信息
                session(['seller_info' => $user_info]);
                //session存储商家操作员用户信息
                session(['role_info' => $role_info]);

                //获取用户角色权限列表
                $permission = $role_info['permission'];
                $permission = explode(',', $permission);
                $permission_list = SellerPermission::whereIn('id', $permission)->orderBy('sort', 'DESC')->get();
                //格式化菜单
                $SellerPermission = new SellerPermission();
                $permission_list = $SellerPermission->sort_permission($permission_list);
                //session存储用户权限
                session([$user_info->id . '_permission_list' => $permission_list]);
                //获取商家名
                $seller_name = RuzhuMerchantBasic::where(['uid' => $user_info->id])->select('merchantName')->first();
                session(['seller_name' => $seller_name]);


                //登录成功记录信息
                return arr_post(1, '登录成功');

            }
            return arr_post(0, '用户不存在');
        } else {
            //是
            if ($user_info->status == 0) {
                return arr_post(0, '您被禁止登录');
            } else {
                if (!empty($user_info->openid)) {
                    return arr_post(0, '该账号已绑定其他微信号');
                }
                if (!empty($openid)) {
                    //绑定openid
                    bind_openid($mobile, $openid);
                }
                $this->where(['id' => $user_info->id])->update(['last_time' => time2date()]);
                //session存储用户信息
                session(['seller_info' => $user_info]);
                //获取权限列表
                $permission_list = SellerPermission::select('*')->orderBy('sort', 'DESC')->get();
                //格式化菜单
                $SellerPermission = new SellerPermission();
                $permission_list = $SellerPermission->sort_permission($permission_list);
                //session存储用户权限
                session([$user_info->id . '_permission_list' => $permission_list]);
                session(['role_info' => '']);
                session(['seller_name' => '']);
                //登录成功记录信息
                return arr_post(1, '登录成功');
            }
        }
    }


    /**
     * @param $mobile 商家手机号
     * @param $password 登录密码
     * @return bool
     * pos机商家登录
     */
    public function seller_pos_login($mobile, $password)
    {
        if (empty($mobile)) {
            return arr_post(0, '商家手机号不能为空');
        }
        if (empty($password)) {
            return arr_post(0, '请输入手机密码');
        }
        $user = $this->where(['mobile' => $mobile])->first();
        if (empty($user)) {
            return arr_post(0, '用户不存在');
        } else {
            if ($user->status == 0) {
                return arr_post(0, '您被禁止登录');
            }
            if (md5(md5($password) . 'tdzd') != $user->password) {
                return arr_post(0, '密码不正确');
            } else {
                $uid = $user->id;
                //验证商家是否审核通过
                $status = MerchantApply::where(['uid' => $uid])->first(['status'])->status;
                if ($status != 3) {
                    return arr_post(0, '您的商家信息未通过审核,无法登录');
                } else {
                    $this->where(['id' => $user->id])->update(['last_time' => time2date()]);
                    $ticket = uuid();
                    $add = [
                        'uid' => $uid,
                        'session_id' => session_id(),
                        'ticket' => $ticket,
                        'status' => 0,
                        'ip' => request()->ip(),
                        'client_system' => request('ClientSystem'),
                        'client_version' => request('ClientVersion'),
                        'api_version' => request('ApiVersion', '1.0'),
                        'timeout' => request()->server('REQUEST_TIME', time()) + 3600 * 3,
                    ];
                    $var = UserTicket::create($add);
                    //返回ticket
                    return arr_post(1, '登录成功', ['ticket' => $ticket]);
                }
            }
        }
    }

    /**
     * @param $uid 用户id
     * @param $password 登录密码
     * @return array
     * 验证密码
     */
    public function check_password($uid, $password)
    {
        $password = md5(md5($password));
        $rs = $this->where(['id' => $uid, 'password' => $password])->first();
        return empty($rs) ? arr_post(0, '密码不正确') : arr_post(1, '密码正确');
    }

    /**
     * @param $uid 用户id
     * @param $password
     * @param $password_name f_password 登录密码 s_password 查询密码 t_password 支付密码
     * @return array
     * 更新密码
     */
    public function update_password($id, $password, $password_name)
    {
        $password = md5(md5($password));
        return $this->update_user_info($id, [$password_name => $password]);
    }

    /**
     * 计算所有商家信用积分
     */
    public function evaluate_seller_credit()
    {
        //2018/03/12 每15天评定一次
        ignore_user_abort(true);
        //允许长时间执行
        set_time_limit(0);
        if (date('d') == '01' || date('d') == '15') {
            return false;
        }
        $start_time = date("Y-m-d", time() - 16 * 86400) . ' 00:00:00';
        $end_time = date("Y-m-d", time() - 1 * 86400) . ' 23:59:59';
        DB::table('credit_log')->insert(['u_date' => date('Y-m-d'), 'created_at' => time2date()]);
        $this->get(['id', 'credit', 'back_time'])->each(function ($v) use ($start_time, $end_time) {
            $Bank = new Bank();
            $selle_id = $v->id;
            $org_credit = $v->credit;
            //统一按积分算
            //统计商家三天让利
            $total_platform_amount = VOrder::sellerOrder()
                ->where(['status' => 2, 'uid' => $selle_id])
                ->where('created_at', '>', $start_time)
                ->where('created_at', '<', $end_time)
                ->sum('platform_amount');
            //获取商家收益钱包
            $purse_id = $Bank->userWallet($selle_id, 1, 2, ['purse_id'])->purse_id;
            //统计商家三天返利(只限积分)
            $total_rebate_amount = VTransfer::where(['into_purse_id' => $purse_id, 'reason' => 10022])
                ->where('time', '>', $start_time)
                ->where('time', '<', $end_time)
                ->sum('into_amount');
            if ($total_rebate_amount > 0) {
                $credit = 500 + floor(($total_platform_amount / $total_rebate_amount) * 100) + 20 + rand(1, 9);
                if ($credit >= 600) {
                    $credit = 600;
                    if ($org_credit >= 600) {
                        $credit = $org_credit + 1;
                    }
                }
            } elseif ($total_platform_amount > 0 && $total_rebate_amount <= 0) {
                $credit = 600;
            } elseif ($total_platform_amount <= 0 && $total_rebate_amount > 0) {
                $credit = 500;
            } else {
                $credit = 600;
            }
            $this->where(['id' => $selle_id])->update(['credit' => $credit]);
        });
    }


    /**
     * @param $uid 商家id
     * @param $amount 让利金额
     * 累计商家让利
     */
    public function add_platform_amount($uid, $amount)
    {
        $key = 'platform_' . $uid;
        if (Cache::has($key)) {
            $total_amount = Cache::get($key) + $amount;
        } else {
            $total_amount = VOrder::whereIn('type', [1, 3, 5])->where(['status' => 2])->where(['uid' => $uid])->sum('platform_amount') + $amount;
        }
        Cache::put($key, $total_amount, 3600);
        return true;
    }

    /**
     * @param $uid 商家id
     * 获取商家让利
     */
    public function get_platform_amount($uid)
    {
        $key = 'platform_' . $uid;
//        if(Cache::has($key)){
//            $total_amount=Cache::get($key);
//        }else{
        $total_amount = VOrder::whereIn('type', [1, 3, 5])->where(['status' => 2])->where(['uid' => $uid])->sum('platform_amount');
//        }
        return $total_amount;
    }


    /**
     * 商家后台API登录
     *
     * @param $mobile 商家手机号
     * @param $sms_code 短信码
     * @param string $openid openid 与积分商城做关联使用
     * @return integer 登录用户ID
     * @throws ApiException
     */
    public function seller_api_login($mobile, $sms_code, $openid = '')
    {
        //验证短信验证码
        $Message = new Message();
        $Message->check_sms($mobile, $sms_code, 5);
        //判断是否店长登录
        $user_info = $this->where(['mobile' => $mobile])->first();
        if (empty($user_info)) {
            //否
            //判断是否是操作员登录
            $role_info = SellerRole::where(['mobile' => $mobile])->first();
            if (!empty($role_info)) {
                SellerRole::where(['mobile' => $role_info->mobile])->update(['lo_time' => time2date()]);
                $user_new_info = User::where(['mobile' => $mobile])->first();
                if (!isset($user_new_info->id)) {
                    User::insert(['mobile' => $mobile, 'openid' => $openid, 'created_at' => time2date()]);
                }
                User::where(['mobile' => $mobile])->update(['openid' => $openid]);
                //登录成功记录信息
                return $user_new_info->id;
            }
            throw new ApiException('用户不存在');
        } else {
            //是
            if ($user_info->status == 0) {
                throw new ApiException('您被禁止登录');
            } else {
                if (!empty($user_info->openid)) {
                    throw new ApiException('该账号已绑定其他微信号');
                }
                if (!empty($openid)) {
                    //绑定openid
                    bind_openid($mobile, $openid);
                }
                $this->where(['id' => $user_info->id])->update(['last_time' => time2date()]);
                //登录成功记录信息
                return $user_info->id;
            }
        }
    }

}
