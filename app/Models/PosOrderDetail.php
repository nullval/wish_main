<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PosOrderDetail extends Model
{
	protected $table = 'pos_order_detail';

    public function __construct($year=0,array $attributes = [])
    {
        parent::__construct($attributes);
        $year=empty($year)?date('Y'):$year;
        $this->table='pos_order_detail_'.$year;
    }
}
