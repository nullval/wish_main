<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * 关系表
 */
class ProfitRelation extends Model
{
	protected $table = 'wish_profit_relation';

    /**
     * 商家  表名：wish_seller
     * 店铺直推人 表名：wish_agent
     * 代理 表名：wish_agent_operate
     * 分公司 表名：wish_filiale_operate
     */

	//商家
	const IDENTITY_TYPE_SELLER = 4;
	//店铺直推人
    const IDENTITY_TYPE_AGENT = 3;
    //代理
    const IDENTITY_TYPE_AGENT_OPERATE = 2;
    //分公司
    const IDENTITY_TYPE_FILIALE_OPERATE = 1;


    /**
     * 根据角色ID得到身份类型值
     * @param $roleId
     * @return bool
     */
    public function getIdentityTypeByRoleId($roleId){
        switch ($roleId){
            case AdminRole::ROLE_FILIALE_OPERATE:
                return self::IDENTITY_TYPE_FILIALE_OPERATE;
            case AdminRole::ROLE_AGENT_OPERATE_1:
            case AdminRole::ROLE_AGENT_OPERATE_2:
            case AdminRole::ROLE_AGENT_OPERATE_3:
            case AdminRole::ROLE_AGENT_OPERATE_4:
            case AdminRole::ROLE_AGENT_OPERATE_5:
                return self::IDENTITY_TYPE_AGENT_OPERATE;
            case AdminRole::ROLE_AGENT:
                return self::IDENTITY_TYPE_AGENT;
            case AdminRole::ROLE_SELLER:
                return self::IDENTITY_TYPE_SELLER;
            default:
                return false;
        }
    }



    /**
     * 添加关系
     * @param int $adminId 会员表ID
     * @param int $roleId 角色ID
     * @param int $relevanceId 根据类型需要传入的ID
     * @param int $parentId 父ID,添加分公司时，不填
     */
	public function addRelation($adminId,$roleId,$relevanceId,$parentId = 0){

        $identityType = $this->getIdentityTypeByRoleId($roleId);
        $insertData = [
            'wish_admin_id'=>$adminId,
            'identity_type'=>$identityType,
        ];
        switch ($identityType){
            case self::IDENTITY_TYPE_SELLER:
                //商家
                $insertData['seller_id'] = $relevanceId;
                $insertData['parent_seller_reco_id'] = $parentId;
                break;
            case self::IDENTITY_TYPE_AGENT:
                //店铺直推人
                $insertData['seller_reco_id'] = $relevanceId;
                $insertData['parent_agent_id'] = $parentId;
                break;
            case self::IDENTITY_TYPE_AGENT_OPERATE:
                //代理
                $insertData['agent_id'] = $relevanceId;
                $insertData['parent_filiale_id'] = $parentId;
                break;
            case self::IDENTITY_TYPE_FILIALE_OPERATE:
                //分公司
                $insertData['filiale_id'] = $relevanceId;
                break;
        }
        return $this->insertGetId($insertData);
    }

}
