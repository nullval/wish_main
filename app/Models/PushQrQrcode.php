<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PushQrQrcode extends Model
{
    protected $table = 'extension_code';

    public $timestamps = false;

    /**
     * @param $query
     * @return mixed
     * 有效的二维码
     */
    public function scopeTrueQr($query)
    {
        $query->whereNotIn('extension_code.state', [3]);//废弃的二维码
    }

    /**
     * 为运营中心生成二维码
     *
     * @param $push_number 生成数量
     * @param $operate_id 运营中心
     * @return array
     * @throws \Exception
     */
    public function create_qrcode($push_number, $operate_id)
    {
        //获取运营商在地推列表的用户id
        $PushQrUser = new PushQrUser();
        $qr_user_id = $PushQrUser->find_operate_in_user($operate_id);
        DB::beginTransaction();
        for ($i = 1; $i <= $push_number; $i++) {
            $arr = [
                'code' => md5(time() + rand(1000, 9999) + 'pushQr'),
                'status' => 1,
                'created_at' => time2date(),
                'operate_id' => $operate_id,
                'qr_user_id' => $qr_user_id
            ];
            $id = $this->insertGetId($arr);
        }
        DB::commit();
        return arr_post(1, '生成成功');
    }

    /**
     * 绑定二维码
     *
     * @param $push_number
     * @param $from_user_id
     * @param $to_user_id
     * @param $level
     * @return array
     */
    public function bind_qrcode($push_number, $user_id, $mobile, $level)
    {
        $PushQrUser = new PushQrUser();
        $info = PushQrUser::where(['mobile' => $mobile, 'level' => $level])->first();
        if (!isset($info->id)) {
            return arr_post(0, '该身份下的团员不存在');
        }
        $to_user_id = $info->id;
        $rs = $PushQrUser->is_belong_to($user_id, $to_user_id);
        if (!$rs) {
            return arr_post(0, '该团员不属于您的团队，无法绑定二维码');
        }
        $count = $this->where(['qr_user_id' => $user_id, 'status' => 1])->count();
        if ($push_number > $count) {
            return arr_post(0, '二维码库存不足');
        }
        $this->where(['qr_user_id' => $user_id, 'status' => 1])->limit($push_number)->update(['qr_user_id' => $to_user_id]);

        return arr_post(1, '绑定成功');

    }

    /**
     * 加密二维码ID
     * @param $num
     * @return string
     */
    public static function getCodeNumber($num){
        return str_pad(($num - 5) / 88, 9, '0', STR_PAD_LEFT);
    }

    /**
     * 获取我的库存,0为总公司
     * @param $agent_id
     * @return int
     */
    public function getMyNum($agent_id)
    {
        return $this->where('agent_id', $agent_id)->where('state', 1)->count();
    }

}
