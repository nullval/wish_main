<?php
namespace App\Models;

use App\Exceptions\ApiException;
use App\Jobs\doTransfer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class Bank extends CommonModel {

    public $cache_sys_purse='sys_purse_';
    public $cache_bank_purse='bank_purse_';
    public $cache_d_purse='d_purse_';
    public $cache_user_purse='user_purse_';
    public $cache_expire_time=43200; //过期时间单位分钟 (3个月)

    /**
     * 钱包类型 1收益钱包 2.现金钱包(中央银行)
     * 系统收益 固定owner_id 3 初始化 100000000.0000
     * 中央银行 固定owner_id 4
     */

    /**
     * 用户可用金额
     * @param $user_id
     * @param $purse_type
     * @param $owner_type 1-个人钱包 2-商家钱包 3-系统钱包 4-中央银行 5-机主钱包
     * @return int
     */
    public function use_money($user_id,$purse_type,$owner_type){
        $wallet = $this->userWallet($user_id , $purse_type,$owner_type);
        return get_num($wallet->balance - $wallet->freeze_value);
    }

    /**
     * 用户钱包余额冻结
     * @param $user_id
     * @param $purse_type
     * @param $amount
     * @param $remarks
     * @param $owner_type 1-个人钱包 2-商家钱包 3-系统钱包 4-中央银行 5-机主钱包
     * @return bool
     */
    public function doFreeze($user_id ,$purse_type ,$amount ,$remarks = '',$owner_type){
        if($owner_type==3){
            //系统钱包
            $wallet=$this->get_sys_purse($purse_type);
        }else{
            $wallet = $this->userWallet($user_id , $purse_type,$owner_type);
        }
        if(round($wallet->balance - $wallet->freeze_value,4) < $amount){
            throw new ApiException('金额不足');
        }


        // 钱包冻结金额增加
        return DB::transaction(function () use($wallet,$amount,$remarks){
            Db::table('wish_user_purse')->where(['purse_id'=>$wallet->purse_id])->increment('freeze_value',$amount);
            $year=date('Y');
            // 增加资金冻结记录
            $hisCon = [
                'purseid' => $wallet->purse_id,
                'amount' => $amount,
                'remarks' => $remarks,
                'create_time' => time(),
                'created_at'=>time2date()
            ];
            $R = Db::table('freeze_his_'.$year)->insertGetId($hisCon);
            return $R;
        });
    }


    /** 用户钱包解冻
     * @param @hisid 钱包冻结记录id
     * @param $year 解冻哪一年的
     * @param $is_ask_other 1请求第三方 2不请求第三方
     */
    public function unApiFreeze($hisid,$year=0){
        $year=empty($year)?date('Y'):$year;
        $freeze = Db::table('freeze_his_'.$year)->where(['hisid'=>$hisid])->first();
        if(!$freeze){
            throw new ApiException('冻结记录不存在');
        }
        if(1 == $freeze->status){
            throw new ApiException('该冻结记录已解冻');
        }
        DB::transaction(function () use($freeze,$year,$hisid){
            // 冻结金额减少
            $R = Db::table('wish_user_purse')->where(['purse_id'=>$freeze->purseid])->decrement('freeze_value',$freeze->amount);
            if(!$R){
                throw new ApiException('更新钱包冻结金额失败');
            }
            // 冻结记录更新状态
            $R = Db::table('freeze_his_'.$year)->where(['hisid'=>$hisid])->update([
                'status' => 1,
                'optime' => time(),
            ]);
            if(!$R){
                throw new ApiException('更新钱包冻结记录失败');
            }
        });
        return true;
    }

    /** 用户钱包解冻
     * @param @hisid 钱包冻结记录id
     * @param $year 解冻哪一年的
     * @param $is_ask_other 1请求第三方 2不请求第三方
     */
    public function unFreeze($hisid,$year=0,$is_ask_other=1){
        $year=empty($year)?date('Y'):$year;
        $freeze = Db::table('freeze_his_'.$year)->where(['hisid'=>$hisid])->first();
        if(!$freeze){
            throw new ApiException('冻结记录不存在');
        }
        if(1 == $freeze->status){
            throw new ApiException('该冻结记录已解冻');
        }
        DB::transaction(function() use($freeze,$year,$hisid){
            // 冻结金额减少
             Db::table('wish_user_purse')->where(['purse_id'=>$freeze->purseid])->decrement('freeze_value',$freeze->amount);

            // 冻结记录更新状态
             Db::table('freeze_his_'.$year)->where(['hisid'=>$hisid])->update([
                'status' => 1,
                'optime' => time(),
            ]);
        });

        return true;
    }

    /** 用户钱包解冻(部分)
     * @param @hisid 钱包冻结记录id
     * @param $year 解冻哪一年的
     * @param $amount 解冻金额
     */
    public function unApiPathFreeze($hisid,$year=0,$amount){
        $year=empty($year)?date('Y'):$year;
        $freeze = Db::table('freeze_his_'.$year)->where(['hisid'=>$hisid])->first();


        if(round($freeze->amount,4)<$amount){
            throw new ApiException('金额大于冻结记录金额');
        }
        if(!$freeze){
            throw new ApiException('冻结记录不存在');
        }
        if(1 == $freeze->status){
            throw new ApiException('该冻结记录已解冻');
        }
        DB::transaction(function () use($freeze,$year,$hisid,$amount){
            // 冻结金额减少
            Db::table('wish_user_purse')->where(['purse_id'=>$freeze->purseid])->decrement('freeze_value',$amount);
            if($freeze->amount==$amount){
                // 冻结记录更新状态
                 Db::table('freeze_his_'.$year)->where(['hisid'=>$hisid])->update([
                    'status' => 1,
                    'optime' => time(),
                ]);
            }else{
                $left_amount=$freeze->amount-$amount;
                // 冻结记录更新状态
                Db::table('freeze_his_'.$year)->where(['hisid'=>$hisid])->update([
                    'amount' => $left_amount,
                    'optime' => time(),
                ]);
            }

        });

        return true;
    }

    /**
     * 用户钱包转账 (只适合同类型)
     * @param $from_id 转出用户id
     * @param $to_id 转入用户id
     * @param $type 钱包类型 0系统钱包 1游戏币钱包
     * @param $amount 转账金额
     * @param $reason 转账原因
     * @param $detail 流水对应操作详情 表名:表ID
     * @param $remarks 备注
     * @return boolean 是否转账成功
     */
    public function doTransferByUserId($from_id ,$to_id ,$type,$amount ,$reason ,$detail,$remarks = ''){
        if($from_id==-1){
            $from=1;
        }elseif($from_id==-2){
            $from=2;
        }else{
            $from=$this->userWallet($from_id,$type)->purse_id;
        }
        if($to_id==-1){
            $to=1;
        }elseif($to_id==-2){
            $to=2;
        }else{
            $to=$this->userWallet($to_id,$type)->purse_id;
        }
        $rs=$this->doTransfer($from ,$to ,$amount ,$reason ,$detail,$remarks);
        return $rs;
    }


    /**
     * 钱包转账 适用于接口调用 错误返回json 只要一处错误整个进程中段
     * @param $from 转出钱包purse_id
     * @param $to 转入钱包purse_id
     * @param $amount 转账金额
     * @param $reason 转账原因
     * @param $detail 流水对应操作详情 表名:表ID
     * @param $remarks 备注
     * @param $is_synchro 是否同步 true 同步  false 异步队列
     * @return boolean 是否转账成功
     */
    public function doApiTransfer($from ,$to ,$amount ,$reason ,$detail,$remarks = '',$status=0,$is_synchro=true){
        if($is_synchro){
            return $this->_doTransfer($from ,$to ,$amount ,$reason ,$detail,$remarks,$status);
        }else{
            dispatch((new doTransfer($from ,$to ,$amount ,$reason ,$detail,$remarks,$status))->onQueue('handleOrder'));
        }
    }

    /**
     * 废用
     * 钱包转账
     * @param $from 转出钱包purse_id
     * @param $to 转入钱包purse_id
     * @param $amount 转账金额
     * @param $reason 转账原因
     * @param $detail 流水对应操作详情 表名:表ID
     * @param $remarks 备注
     * @return boolean 是否转账成功
     */
    public function doTransfer($from ,$to ,$amount ,$reason ,$detail,$remarks = '',$status=0){
        return $this->_doTransfer($from ,$to ,$amount ,$reason ,$detail,$remarks,$status);
    }

    public function _doTransfer($from ,$to ,$amount ,$reason ,$detail,$remarks = '',$status=0){
        $fromPurse = Db::table('wish_user_purse')->where(['purse_id'=>$from])->first();
        $toPurse = Db::table('wish_user_purse')->where(['purse_id'=>$to])->first();


        if(!$fromPurse || !$toPurse){
            throw new ApiException("转入或转出钱包不存在");
        }
        //系统钱包排除
        if($fromPurse->owner_type!=3&&$fromPurse->owner_type!=4){
            if(round($fromPurse->balance - $fromPurse->freeze_value,4) < $amount){
                throw new ApiException("余额不足!");
            }
        }

        // 转出者减少余额
//        $R = UserPurse::where(['purse_id'=>$from])->update(['balance'=>($fromPurse->balance - $amount)]);
        $R = UserPurse::where(['purse_id'=>$from])->decrement('balance',$amount);

        if(!$R){
            throw new ApiException("转出者账号扣款失败!");
        }
        // 转入者增加余额
//        $R =UserPurse::where(['purse_id'=>$to])->update(['balance'=>($toPurse->balance + $amount)]);
        $R = UserPurse::where(['purse_id'=>$to])->increment('balance',$amount);
        if(!$R){
            throw new ApiException("转入者账号收款失败!");
        }
        $year=date('Y');
        // 添加转账记录
        $transferCon = [
            'out_owner_id' => $fromPurse->owner_id,
            'out_owner_type' => $fromPurse->owner_type,
            'into_owner_id' => $toPurse->owner_id,
            'into_owner_type' => $toPurse->owner_type,
            'out_amount' => $amount,
            'out_purse_id' => $fromPurse->purse_id,
            'into_amount' => $amount,
            'into_purse_id' => $toPurse->purse_id,
            'out_balance' => $fromPurse->balance - $amount,
            'into_balance' => $toPurse->balance + $amount,
            'detail' => $detail,
            'remarks' => $remarks,
            'reason' => $reason,
            'create_time' => time(),
            'status' => $status,
            'time' => time2date()
        ];

        $transfer_id = $R = Db::table('transfer_'.$year)->insertGetId($transferCon);
        if(!$R){
            throw new ApiException("资金流水记录失败!");
        }
        return $transfer_id;


    }



    /**
     * 用户钱包
     * @param $user_id
     * @param $pursetype 钱包类型
     * @param $owner_type 1-个人钱包 2-商家钱包 3-系统钱包 4-中央银行 5-机主钱包
     * @return array
     */
    public function userWallet($user_id ,$pursetype,$owner_type,$field=['*']){
        if( !$pursetype){
            throw new ApiException("用户钱包参数错误!");
        }
        if( empty($user_id)){
            throw new ApiException($user_id."不存在!");
        }
        $purseCon = [
            'owner_type' => $owner_type,
            'owner_id' => $user_id,
            'purse_type' => $pursetype,
        ];
        $purse = DB::table('wish_user_purse')->where($purseCon)->first($field);
        // 如果钱包不存在,则创建一个
        if(!$purse){
            $purseCon['create_time'] = time();
            $id = DB::table('wish_user_purse')->insertGetId($purseCon);
            if(!$id){
                throw new ApiException("获取用户钱包数据失败,请稍后重试");
//				return arr_post(0,'获取用户钱包数据失败,请稍后重试');
            }
            $purse = DB::table('wish_user_purse')->where(['purse_id'=>$id])->first($field);
        }

        //字段处理
        $purse->balance=get_last_two_num($purse->balance);
        $purse->freeze_value=get_last_two_num($purse->freeze_value);
        return $purse;
    }


    /**
     * 获取中央银行钱包
     */
    public function get_bank_purse($pursetype=1,$field=['*']){
        $purse = DB::table('wish_user_purse')->where([
            'owner_type'=>4,
            'owner_id'=>0,
            'purse_type'=>$pursetype
        ])->first($field);
        return $purse;
    }

    /**
     * 获取系统钱包id
     */
    public function get_bank_purse_id($pursetype=1){
        $key=$this->cache_bank_purse.$pursetype;
        $purse_id=Cache::store('file')->get($key);
        if(!$purse_id){
            $purse_id = UserPurse::where([
                'owner_type'=>4,
                'owner_id'=>0,
                'purse_type'=>$pursetype
            ])->value('purse_id');
            if($purse_id){
                //存储3个月
                Cache::store('file')->put($key,(int)$purse_id,$this->cache_expire_time);
            }
        }
        return $purse_id;
    }

    /**
     * 获取系统钱包
     */
    public function get_sys_purse($pursetype,$field=['*']){
        $purse = DB::table('wish_user_purse')->where([
            'owner_type'=>3,
            'owner_id'=>0,
            'purse_type'=>$pursetype
        ])->first($field);
        return $purse;
    }

    /**
     * 获取系统钱包id
     */
    public function get_sys_purse_id($pursetype){
        $key=$this->cache_sys_purse.$pursetype;
        $purse_id=Cache::store('file')->get($key);
        if(!$purse_id){
            $purse_id = UserPurse::where([
                'owner_type'=>3,
                'owner_id'=>0,
                'purse_type'=>$pursetype
            ])->value('purse_id');
            if($purse_id){
                //存储3个月
                Cache::store('file')->put($key,(int)$purse_id,$this->cache_expire_time);
            }
        }
        return $purse_id;
    }

    /**
     * 获取第三方钱包
     * $owner_type 6-第三方代付收益钱包 7-pos机消费收益钱包
     */
    public function get_d_purse($pursetype,$owner_type=6,$field=['*']){
        $purse = DB::table('wish_user_purse')->where([
            'owner_type'=>$owner_type,
            'owner_id'=>0,
            'purse_type'=>$pursetype
        ])->first($field);
        return $purse;
    }

    /**
     * 获取第三方钱包id
     */
    public function get_d_purse_id($pursetype,$owner_type=6){
        $key=$this->cache_d_purse.$pursetype.$owner_type;
        $purse_id=Cache::store('file')->get($key);
        if(!$purse_id){
            $purse_id = UserPurse::where([
                'owner_type'=>$owner_type,
                'owner_id'=>0,
                'purse_type'=>$pursetype
            ])->value('purse_id');
            if($purse_id){
                //存储3个月
                Cache::store('file')->put($key,(int)$purse_id,$this->cache_expire_time);
            }
        }
        return $purse_id;
    }

}