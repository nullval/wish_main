<?php

namespace App\Models\Wish;

use App\Models\CommonModel;

/**
 * 商家推荐人
 */
class WishGeneralize extends CommonModel
{
    protected $table = 'wish_generalize';

    public function generalize_info(){
        return $this->hasOne('App\Models\Wish\WishGeneralizeInfo', 'uid');
    }

    public function wish_agent(){
        return $this->belongsTo('App\Models\Wish\WishAgent', 'inviter_id','id');
    }

    /**
     * 找到当前直推人下所有商家
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function sellers()
    {
        return $this->hasMany('App\Models\Wish\WishSeller','inviter_id');
    }


}
