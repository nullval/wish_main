<?php

namespace App\Models\Wish;

use App\Models\CommonModel;


/**
 * 商家
 */
class WishSeller extends CommonModel
{
    protected $table = 'wish_seller';

    public function seller_info(){
        return $this->hasOne('App\Models\Wish\WishSellerInfo', 'uid');
    }

    public function wish_generalize(){
        return $this->belongsTo('App\Models\Wish\WishGeneralize', 'inviter_id','id');
    }


}
