<?php

namespace App\Models\Wish;

use App\Exceptions\ApiException;
use App\Services\AdminIdentityDispose;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Models\CommonModel;

class WishAgentInfo extends CommonModel
{
    protected $table = 'wish_agent_info';

    /**
     * 更新/新增代理入驻临时信息
     * @param $request_info
     * @param $type 1、新增 2、更新
     * @param $agent_mobile 推荐人手机号码
     * @return mixed
     */
    public function add_agent($request_info, $type, $agent_mobile = '')
    {
        //判断代理手机号与法人手机号是否一致
//        if($request_info['mobile']!=$request_info['corpmanMobile']){
//            throw new ApiException('代理手机号必须与法人手机号一致');
//        }
//        $request_info['corpmanMobile'] =  $request_info['mobile'];

        if ($type == 1) {
            //新增时检查该号码是否已经创建直推人
            $condition = ['mobile' => $request_info['mobile']];
            $wishGeneralize = WishGeneralize::where($condition)->count();
            if ($wishGeneralize >= 1) {
                throw new ApiException('无法创建，该手机号已经绑定了直推人关系');
            }
        }

        //默认代理等级是一级代理
        $request_info['level'] = empty($request_info['level']) ? 1 : $request_info['level'];

        //判断该身份下是否已存在
        $user_info = WishAgent::where(['mobile' => $request_info['mobile']])->first();
        if ($type == 1) {
            //验证该账号是否允许注册
            $adminIdentityModel = new AdminIdentityDispose();
            if (!$adminIdentityModel->checkIsRegister($request_info['mobile'], $adminIdentityModel::IDENTITY_AGENT)) {
                throw new ApiException('该手机号已被注册代理人注册了,请勿重复注册');
            }
        } elseif ($type == 2) {
            if (!isset($user_info->id)) {
                throw new ApiException('该手机号还未注册，请先去入驻，才能编辑信息');
            }
        }


        //检查是否存在推荐人
        $agent_id = 0;
        if (!empty($agent_mobile)) {
            $agent_id = WishCompany::where(['mobile' => $agent_mobile])->value('id');
            if (empty($agent_id)) {
                throw new ApiException('推荐人不存在');
            }
        }
        //判断是选择银行
//        if (empty($request_info['bankCode'])){
//            throw new ApiException('还未选择银行！');
//        }
        //判断是否有选省市区
        if (empty($request_info['province10']) || empty($request_info['city10'])) {
            throw new ApiException('请选择省市区');
        }

        //判断是不是直辖市的郊区,选择区域编号
        if (empty($request_info['district10'])) {
            $area_id = $request_info['city_code'];
        } else {
            $area_id = $request_info['district_code'];
        }

        //检查是否上传图片并返回图片地址
//        if(!empty($request_info['businessimagebase64'])){
//            $result=upload($request_info['businessimagebase64']);
//            if($result['code']==0){
//                throw new ApiException($result['message']);
//            }else{
//                $request_info['businessimage']=$result['data']['filepath'];
//            }
//        }elseif (empty($request_info['businessimagebase64']) && $type == 1){
//            throw new ApiException('请上传营业执照');
//        }

        //做事务
        DB::beginTransaction();
        //新增时，默认密码为手机号
        if ($type == 1 && empty($request_info['password'])) {
            $request_info['password'] = $request_info['mobile'];
        }
        //1、更新/新增代理
        if (!empty($request_info['password'])) {
            $reg['password'] = md5(md5($request_info['password']));
            if ($type == 2) {
                WishAgent::where(['id' => $user_info->id])->update(['password' => $reg['password']]);
            }
        }
        $adminModel = new Admin();
        $wishAgent = new WishAgent();
        //拿到代理角色ID
        $roleId = $wishAgent->getRoleByLevel($request_info['level']);
        if ($type == 1) {
            //新增代理
            $reg['inviter_id'] = $agent_id;
            $reg['mobile'] = $request_info['mobile'];//代理手机
            $reg['created_at'] = time2date();
            $reg['last_time'] = time2date();
            $reg['is_bind_phone'] = 2; //是否绑定手机
//            $reg['child_merchant_no']=$request_info['corpmanMobile'];
            $reg['level'] = $request_info['level']; //代理身份
            //插入代理表
            $rs[] = $user_id = WishAgent::insertGetId(filter_update_arr($reg));
            //添加会员表
            $adminModel->add_admin($request_info['mobile'], $request_info['password'], $roleId);
            //注册商城
            $userModel = new User();
            $result = $userModel->main_register($request_info['mobile']);
            if ($result['code'] != 1) {
                throw new ApiException('注册商城用户失败！' . $result['message']);
            }

        } elseif ($type == 2) {
            $user_id = $user_info->id;
            //修改代理身份
            WishAgent::where(['id' => $user_info->id])->update(['level' => $request_info['level']]);

            $adminIdentityModel = new AdminIdentityDispose();
            $adminId = Admin::where('username', $request_info['mobile'])->whereIn('role_id', $adminIdentityModel->getRoleIdByIdentity(AdminIdentityDispose::IDENTITY_AGENT))->value('id');
            //修改会员表
            $adminModel->add_admin($request_info['mobile'], $request_info['password'], $roleId, $adminId);
        }


        //2、更新/新增代理信息
        $basic_info = [
            'merchantName' => $request_info['merchantName'],    // 店名，要与营业执照上一致
            'shortName' => mb_substr($request_info['merchantName'], 0, 5, 'utf-8'), //简称
//            'city'					=> $request_info['city'],		// 城市id
//            'merchantAddress'		=> $request_info['merchantAddress'],	// 城市中文名
            'servicePhone' => $request_info['servicePhone'],            // 客服电话
            'merchantType' => '01',        // 00公司，01个体
//            'category'				=> $request_info['category'],		// 类别id
//            'corpmanName'			=> $request_info['corpmanName'],		// 法人姓名
//            'corpmanId'				=> $request_info['corpmanId'],	// 法人身份证
//            'corpmanMobile'			=> $request_info['corpmanMobile'],	// 法人联系手机
//            'bankCode'				=> $request_info['bankCode'], //银行代码
//            'bankName'				=> $request_info['bankName'], //开户行全称
//            'bankaccountNo'			=> $request_info['bankaccountNo'], //开户行账户
//            'bankaccountName'		=> $request_info['bankaccountName'], //开户行户名
//            'businessimage'		=> $request_info['businessimage'], //营业执照图片
//            'businessLicense'		=> $request_info['businessLicense'], //营业执照
            'autoCus' => 0,        // 0：不自动提现 1：自动提现
            'remark' => '代理入驻申请',
            'uid' => $user_id,
//            'merchantId'				=> $request_info['corpmanMobile'], //子商户号
            'status' => 2,
            'area_id' => $area_id,
            'lat' => $request_info['lat'],  //纬度
            'lng' => $request_info['lng'],  //经度
        ];

        //代理基本信息登记临时表更新/插入数据
        $basic = $this->where(['uid' => $user_id])->first();
        if ($type == 1) {
            $basic_info['create_time'] = time2date();
            $rs[] = $basic_id = $this->insertGetId(filter_update_arr($basic_info));

        } elseif ($type == 2) {
            $basic_info['updated_at'] = time2date();
            $result = $this->where(['uid' => $user_id])->update(filter_update_arr($basic_info));
            $basic_id = $basic->id;
            $rs[] = true;
        }
        //3、更新/新增代理银行信息
        $bank_info = [
            'basic_id' => $basic_id, //基本信息id
            'merchantId' => $request_info['mobile'], //子商户号
//            'bankCode'			=> $request_info['bankCode'],  //银行代码
            'bankaccProp' => '0',        // 账号属性 0私人，1公司
//            'name'				=> $request_info['corpmanName'], //持卡人
//            'bankaccountNo'		=> $request_info['bankaccountNo'], //银行账号
//            'bankaccountType'	=> $request_info['bankaccountType'], //银行卡类型
            'certCode' => '1',            // 1身份证，2护照，3军官证，4回乡证，5台胞证，6港澳通行证，7国际海员证，8外国人永久居住证，9其他
//            'certNo'			=> $request_info['corpmanId'], //证件号
            'remark' => '代理银行卡信息登记',        // 备注
//			'create_time'		=> time2date(),
//            'bankbranchNo'		=> $request_info['ibankno'], //联行号
//            'bankName'			=> $request_info['bankName'],  //银行名称
        ];
        $bank = WishAgentBank::where(['basic_id' => $basic_id])->first();
        //更新/插入高汇通银行信息临时表
        if ($type == 1 || empty($bank)) {
            $bank_info['created_at'] = time2date();
            $basic_info['create_time'] = time2date();
            $rs[] = WishAgentBank::insert(filter_update_arr($bank_info));
        } elseif ($type == 2 && !empty($bank)) {
            $bank_info['updated_at'] = time2date();
            $result = WishAgentBank::where(['basic_id' => $basic_id])->update(filter_update_arr($bank_info));
            $rs[] = true;
        }

        if (checkTrans($rs)) {
            if ($type == 1) {
                //创建分公司后自动创建直推人
                $wishGeneralizeInfoModel = new WishGeneralizeInfo();
                $wishGeneralizeInfoModel->add_generalize($request_info, 1, $request_info['mobile']);
            }
            DB::commit();
            return $user_id;
        } else {
            DB::rollBack();
            throw new ApiException('代理入驻失败');
        }
    }


}
