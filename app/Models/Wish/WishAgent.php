<?php

namespace App\Models\Wish;

use App\Models\AdminRole;
use App\Models\CommonModel;


/**
 * 商家
 */
class WishAgent extends CommonModel
{
    protected $table = 'wish_agent';
    //代理等级对应的文字
    const AGENT_LEVEL = [
        1=>'一级代理',
        2=>'二级代理',
        3=>'三级代理',
        4=>'四级代理',
        5=>'五级代理',
    ];

    /**
     * 根据代理等级拿到角色Id
     */
    public function getRoleByLevel($level){
        switch ($level){
            case 1:
                return AdminRole::ROLE_AGENT_1;
            case 2:
                return AdminRole::ROLE_AGENT_2;
            case 3:
                return AdminRole::ROLE_AGENT_3;
            case 4:
                return AdminRole::ROLE_AGENT_4;
            case 5:
                return AdminRole::ROLE_AGENT_5;
            default:
                return false;
        }
    }

    /**
     * 代理名称属性访问器 $object->level_text
     * @param $level
     * @return mixed
     */
    public function getLevelTextAttribute($level)
    {
        return self::AGENT_LEVEL[$this->attributes['level']];
    }

    /**
     * 找到当前代理下所有商家
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function getSellers()
    {

    }







}
