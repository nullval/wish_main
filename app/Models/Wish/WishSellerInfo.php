<?php

namespace App\Models\Wish;

use App\Exceptions\ApiException;
use App\Models\PushQrQrcode;
use App\Models\ShopTnetReginfo;
use App\Services\AdminIdentityDispose;
use App\Models\Admin;
use App\Models\AdminRole;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Models\CommonModel;
use labs7in0\coord\Coord;

class WishSellerInfo extends CommonModel
{
    protected $table = 'wish_seller_info';

    /**
     * 更新/新增商家入驻临时信息
     * @param $request_info
     * @param $type 1、新增 2、更新
     * @param $agent_mobile 推荐人手机号码
     * @return mixed
     * @throws \labs7in0\coord\Exceptions\UnknownTypeException
     */
    public function add_seller($request_info, $type, $agent_mobile = '')
    {
        //判断商家手机号与法人手机号是否一致
//        if ($request_info['mobile'] != $request_info['corpmanMobile']) {
//            throw new ApiException('商家手机号必须与法人手机号一致');
//        }
        //验证商家让利
        $divide = intval($request_info['divide']);

        if ($divide < 5 || $divide > 20) {
            throw new ApiException('商家让利必须为5-20(%)之间');
        }

        //判断该身份下是否已存在
        $user_info = WishSeller::where(['mobile' => $request_info['mobile']])->first();
        if ($type == 1) {
            //验证该账号是否允许注册
            $adminIdentityModel = new AdminIdentityDispose();
            if (!$adminIdentityModel->checkIsRegister($request_info['mobile'], $adminIdentityModel::IDENTITY_SELLER)) {
                throw new ApiException('该手机号已被注册或者注册了其它身份,请勿重复注册');
            }
        } elseif ($type == 2) {
            if (!isset($user_info->id)) {
                throw new ApiException('该手机号还未注册');
            }
        }
        //检查是否存在推荐人
        $agent_id = 0;
        if (!empty($agent_mobile)) {
            $agent_id = WishGeneralize::where(['mobile' => $agent_mobile])->value('id');
            if (empty($agent_id)) {
                throw new ApiException('推荐人不存在');
            }
        }
        //判断是选择银行
//        if (empty($request_info['bankCode'])) {
//            throw new ApiException('还未选择银行！');
//        }
        //判断是否有选省市区
        if (empty($request_info['province10']) || empty($request_info['city10'])) {
            throw new ApiException('请选择省市区');
        }

        //判断是不是直辖市的郊区,选择区域编号
        if (empty($request_info['district10'])) {
            $area_id = $request_info['city_code'];
        } else {
            $area_id = $request_info['district_code'];
        }

        //商家基本信息登记临时表更新/插入数据
        $basic = $this->where(['uid' => $user_info->id])->first();

//        dd($request_info);
        //检查是否上传图片并返回图片地址(营业执照)
        if (!empty($request_info['businessimagebase64'])) {
//            $result=upload($request_info['businessimagebase64']);
//            if($result['code']==0){
//                throw new ApiException($result['message']);
//            }else{
            $request_info['businessimage'] = $request_info['businessimagebase64'];
//            }
        } elseif (empty($request_info['businessimagebase64']) && $type == 1) {
            throw new ApiException('请上传营业执照');
        }

        //检查是否上传图片并返回图片地址(LOGO)
        if (!empty($request_info['logobase64'])) {
//            $result=upload($request_info['logobase64']);
//            if($result['code']==0){
//                throw new ApiException($result['message']);
//            }else{
            $request_info['logoimage'] = $request_info['logobase64'];
//            }
        } elseif (empty($request_info['logobase64']) && $type == 1) {
            throw new ApiException('请上传LOGO');
        }

        //banner图处理
        $banner = [];
        //检查是否上传图片并返回图片地址(banner1)
//        if (!empty($request_info['banner1base64'])) {
//            $result=upload($request_info['banner1base64']);
//            if($result['code']==0){
//                throw new ApiException($result['message']);
//            }else{
//            $banner['banner1'] = $request_info['banner1base64'];
//            }
//        }
        //检查是否上传图片并返回图片地址(banner2)
//        if (!empty($request_info['banner2base64'])) {
//            $result=upload($request_info['banner2base64']);
//            if($result['code']==0){
//                throw new ApiException($result['message']);
//            }else{
//            $banner['banner2'] = $request_info['banner2base64'];
//            }
//        }
        //过滤banner
//        $old_banner = json_decode($basic['bannerImage'], true);
//        if ($banner) {
//            if (empty($banner['banner1'])) {
//                $banner['banner1'] = $old_banner['banner1'];
//            }
//            if (empty($banner['banner2'])) {
//                $banner['banner2'] = $old_banner['banner2'];
//            }
//        }


        //做事务
        DB::beginTransaction();
        //新增时，默认密码为手机号
        if ($type == 1 && empty($request_info['password'])) {
            $request_info['password'] = $request_info['mobile'];
        }
        //1、更新/新增商家
        //验证是否需要修改密码
        if (!empty($request_info['password'])) {
            $reg['password'] = md5(md5($request_info['password']));
            if ($type == 2) {
                WishSeller::where(['id' => $user_info->id])->update(['password' => $reg['password']]);
            }
        }
        $adminModel = new Admin();
        if ($type == 1) {
            //新增商家
            $reg['inviter_id'] = $agent_id;
            $reg['mobile'] = $request_info['mobile'];//商家手机
            $reg['created_at'] = time2date();
            $reg['last_time'] = time2date();
            $reg['is_bind_phone'] = 2; //是否绑定手机
            $reg['divide'] = $request_info['divide']; //商家让利
//            $reg['divide']=$request_info['divide']; //商家让利
            $reg['child_merchant_no'] = $request_info['corpmanMobile']; //高汇通子商户号
            //插入商家表
            $rs[] = $user_id = WishSeller::insertGetId(filter_update_arr($reg));

            //注册商城
            $userModel = new User();
            $result = $userModel->main_register($request_info['mobile']);
            if ($result['code'] != 1) {
                throw new ApiException('注册商城用户失败！' . $result['message']);
            }

            //添加会员表
            $adminModel->add_admin($request_info['mobile'], $request_info['password'], AdminRole::ROLE_SELLER);
        } elseif ($type == 2) {
            $user_id = $user_info->id;
            //修改会员表
            if (!empty($request_info['password'])) {
                $adminId = Admin::where('username', $request_info['mobile'])->where('role_id', AdminRole::ROLE_SELLER)->value('id');
                $adminModel->add_admin($request_info['mobile'], $request_info['password'], AdminRole::ROLE_SELLER, $adminId);
            }
            //编辑商家表信息
            WishSeller::where('id', $user_id)->update([
                'divide' => $request_info['divide']
            ]);

        }
        //百度坐标转换为GCJ02坐标 2018-8-13 20:50:30
        $coord = new Coord($request_info['lng'], $request_info['lat'], Coord::BD09);
        $coord_arr = explode(',', (string)$coord->copy()->toGcj02());

        //2、更新/新增商家信息
        $basic_info = [
            'merchantName' => $request_info['merchantName'],    // 店名，要与营业执照上一致
            'shortName' => mb_substr($request_info['merchantName'], 0, 5, 'utf-8'), //简称
//            'city'					=> $request_info['city'],		// 城市id
            'merchantAddress' => $request_info['merchantAddress'],    // 城市中文名
            'servicePhone' => $request_info['servicePhone'],            // 客服电话
            'merchantType' => '01',        // 00公司，01个体
            'category' => $request_info['category'],        // 类别id
            'corpmanName' => '',        // 法人姓名
            'corpmanId' => '',    // 法人身份证
            'corpmanMobile' => '',    // 法人联系手机
            'bankCode' => '', //银行代码
            'bankName' => '', //开户行全称
            'bankaccountNo' => '', //开户行账户
            'bankaccountName' => '', //开户行户名
            'businessLicense' => $request_info['businessLicense'], //营业执照
            'autoCus' => 0,        // 0：不自动提现 1：自动提现
            'remark' => '商家入驻申请',
            'uid' => $user_id,
            'merchantId' => $request_info['mobile'], //子商户号
            'status' => 2,
            'area_id' => $area_id,
            'lat' => $coord_arr[1],  //纬度
            'lng' => $coord_arr[0],  //经度
            'average' => $request_info['average'],
            'business_time' => $request_info['business_time'],
            'corpmanPhone' => $request_info['corpmanPhone'], //座机号码
        ];
        if (!empty($request_info['logoimage'])) {
            $basic_info['logoimage'] = $request_info['logoimage']; //logo
        }
        if (!empty($request_info['businessimage'])) {
            $basic_info['businessimage'] = $request_info['businessimage']; //营业执照
        }
        if (!empty($request_info['banner'])) {
            $basic_info['banner_image'] = json_encode($request_info['banner']); //banner图

        }


        if ($type == 1) {
            $basic_info['create_time'] = time2date();
            $rs[] = $basic_id = $this->insertGetId(filter_update_arr($basic_info));

        } elseif ($type == 2) {
            $basic_info['updated_at'] = time2date();
            $result = $this->where(['uid' => $user_info->id])->update(filter_update_arr($basic_info));
            $basic_id = $basic->id;
            $rs[] = true;
        }

        //3、更新/新增商家银行信息
        $bank_info = [
            'basic_id' => $basic_id, //基本信息id
            'merchantId' => $request_info['mobile'], //子商户号
            'bankCode' => '',  //银行代码
            'bankaccProp' => '0',        // 账号属性 0私人，1公司
            'name' => '', //持卡人
            'bankaccountNo' => '', //银行账号
            'bankaccountType' => '', //银行卡类型
            'certCode' => '1',            // 1身份证，2护照，3军官证，4回乡证，5台胞证，6港澳通行证，7国际海员证，8外国人永久居住证，9其他
            'certNo' => '', //证件号
            'remark' => '商家银行卡信息登记',        // 备注
//			'create_time'		=> time2date(),
//            'bankbranchNo'		=> $request_info['ibankno'], //联行号
            'bankName' => '',  //银行名称
        ];
        $bank = WishSellerBank::where(['basic_id' => $basic_id])->first();
        //更新/插入高汇通银行信息临时表
        if ($type == 1 || empty($bank)) {
            $bank_info['created_at'] = time2date();
            $basic_info['create_time'] = time2date();
            $rs[] = WishSellerBank::insert(filter_update_arr($bank_info));

        } elseif ($type == 2 && !empty($bank)) {
            $bank_info['updated_at'] = time2date();
            $result = WishSellerBank::where(['basic_id' => $basic_id])->update(filter_update_arr($bank_info));
            $rs[] = true;
        }

        if (checkTrans($rs)) {
            DB::commit();
            return $user_id;
        } else {
            DB::rollBack();
            throw new ApiException('商家入驻失败');
        }
    }

    /**
     * 前台信息入驻
     * @param $request_info 商家信息
     * @param string $agent_mobile 推荐人手机号码
     * @throws \labs7in0\coord\Exceptions\UnknownTypeException
     */
    public function front_add_seller($request_info,$agent_mobile = ''){
        //检查二维码
        $ss = $request_info['ss'];
        $code_number = PushQrQrcode::getCodeNumber($ss);
        $info = PushQrQrcode::where('code_number', $code_number)->first();
        if(empty($info) || $info['state'] != 1){
            return api_error('二维码已经被使用了');
        }

        //验证商家让利 corpmanName servicePhone
        $divide = intval($request_info['divide']);
        if ($divide < 5 || $divide > 20) {
            return api_error('商家让利必须为5-20(%)之间');
        }
        //判断该身份下是否已存在
        $user_info = WishSeller::where(['mobile' => $request_info['mobile']])->first();
        //验证该账号是否允许注册
        $adminIdentityModel = new AdminIdentityDispose();
        if (!$adminIdentityModel->checkIsRegister($request_info['mobile'], $adminIdentityModel::IDENTITY_SELLER)) {
            return api_error('该手机号已被注册或者注册了其它身份,请勿重复注册');
        }
        //检查是否存在推荐人
        $agent_id = 0;
        if (!empty($agent_mobile)) {
            $agent_id = WishGeneralize::where(['mobile' => $agent_mobile])->value('id');
            if (empty($agent_id)) {
                return api_error('推荐人不存在');
            }
        }
        //判断是否有选省市区
        if (empty($request_info['city_code']) || empty($request_info['district_code'])) {
            return api_error('请选择省市区');
        }
        //判断是不是直辖市的郊区,选择区域编号
        if (empty($request_info['district_code'])) {
            $area_id = $request_info['city_code'];
        } else {
            $area_id = $request_info['district_code'];
        }
        //商家基本信息登记临时表更新/插入数据
        $basic = $this->where(['uid' => $user_info->id])->first();
        //检查是否上传图片并返回图片地址(营业执照)
        if (!empty($request_info['businessimage'])) {
            $request_info['businessimage'] = $request_info['businessimage'];
        } elseif (empty($request_info['businessimage']) ) {
            return api_error('请上传营业执照');
        }

        //检查是否上传图片并返回图片地址(LOGO)
        if (!empty($request_info['logoimage'])) {
            $request_info['logoimage'] = $request_info['logoimage'];
        } elseif (empty($request_info['logobase64'])) {
            return api_error('请上传LOGO');
        }

        //做事务
        DB::beginTransaction();
        //新增时，不填默认密码为手机号
        if (!empty($request_info['password'])) {
            $reg['password'] = md5(md5($request_info['password']));
        }else{
            $request_info['password'] = $request_info['mobile'];
        }
        $adminModel = new Admin();

        //新增商家
        $reg['inviter_id'] = $agent_id;
        $reg['mobile'] = $request_info['mobile'];//商家手机
        $reg['created_at'] = time2date();
        $reg['last_time'] = time2date();
        $reg['is_bind_phone'] = 2; //是否绑定手机
        $reg['divide'] = $request_info['divide']; //商家让利
//            $reg['divide']=$request_info['divide']; //商家让利
        $reg['child_merchant_no'] = $request_info['corpmanMobile']; //高汇通子商户号
        //插入商家表
        $rs[] = $user_id = WishSeller::insertGetId(filter_update_arr($reg));

        //注册商城
        $userModel = new User();
        $result = $userModel->main_register($request_info['mobile']);
        if ($result['code'] != 1) {
            return api_error('注册商城用户失败！' . $result['message']);
        }

        //添加会员表
        $adminModel->add_admin($request_info['mobile'], $request_info['password'], AdminRole::ROLE_SELLER);

        $address = $this->getAddressByCityId($area_id);
        $coord_arr = $this->getLocation($address['city'],$request_info['merchantAddress'],$address['area']);

        //2、新增商家信息
        $basic_info = [
            'merchantName' => $request_info['merchantName'],    // 店名，要与营业执照上一致
            'shortName' => mb_substr($request_info['merchantName'], 0, 5, 'utf-8'), //简称
//            'city'					=> $request_info['city'],		// 城市id
            'merchantAddress' => $request_info['merchantAddress'],    // 城市中文名
            'servicePhone' => $request_info['servicePhone'],            // 客服电话
            'merchantType' => '01',        // 00公司，01个体
            'category' => $request_info['category'],        // 类别id
            'corpmanName' => '',        // 法人姓名
            'corpmanId' => '',    // 法人身份证
            'corpmanMobile' => '',    // 法人联系手机
            'bankCode' => '', //银行代码
            'bankName' => '', //开户行全称
            'bankaccountNo' => '', //开户行账户
            'bankaccountName' => '', //开户行户名
            'businessLicense' => $request_info['businessLicense'], //营业执照
            'autoCus' => 0,        // 0：不自动提现 1：自动提现
            'remark' => '商家入驻申请',
            'uid' => $user_id,
            'merchantId' => $request_info['mobile'], //子商户号
            'status' => 2,
            'area_id' => $area_id,
            'lat' => $coord_arr[1],  //纬度
            'lng' => $coord_arr[0],  //经度
            'average' => $request_info['average'],
            'business_time' => $request_info['business_time'],
            'corpmanPhone' => $request_info['corpmanPhone'], //座机号码
        ];
        if (!empty($request_info['logoimage'])) {
            $basic_info['logoimage'] = $request_info['logoimage']; //logo
        }
        if (!empty($request_info['businessimage'])) {
            $basic_info['businessimage'] = $request_info['businessimage']; //营业执照
        }
        if (!empty($request_info['banner'])) {
            $basic_info['banner_image'] = $request_info['banner']; //banner图
        }


        $basic_info['create_time'] = time2date();
        $rs[] = $basic_id = $this->insertGetId(filter_update_arr($basic_info));

        //3、新增商家银行信息
        $bank_info = [
            'basic_id' => $basic_id, //基本信息id
            'merchantId' => $request_info['mobile'], //子商户号
            'bankCode' => '',  //银行代码
            'bankaccProp' => '0',        // 账号属性 0私人，1公司
            'name' => '', //持卡人
            'bankaccountNo' => '', //银行账号
            'bankaccountType' => '', //银行卡类型
            'certCode' => '1',            // 1身份证，2护照，3军官证，4回乡证，5台胞证，6港澳通行证，7国际海员证，8外国人永久居住证，9其他
            'certNo' => '', //证件号
            'remark' => '商家银行卡信息登记',        // 备注
//			'create_time'		=> time2date(),
//            'bankbranchNo'		=> $request_info['ibankno'], //联行号
            'bankName' => '',  //银行名称
        ];
        $bank = WishSellerBank::where(['basic_id' => $basic_id])->first();
        //更新/插入高汇通银行信息临时表
        $bank_info['created_at'] = time2date();
        $basic_info['create_time'] = time2date();
        $rs[] = WishSellerBank::insert(filter_update_arr($bank_info));

        //绑定二维码
        $res = PushQrQrcode::where('code_number', $code_number)->update([
            'sid' => $user_id,
            'state' => 2//状态已使用
        ]);

        if (empty($res)) {
            return mobileView('error', '错误提示', '未知错误,请联系客服');
        }

        if (checkTrans($rs)) {
            DB::commit();
            return api_success('商家入驻成功',['uid'=>$user_id]);
        } else {
            DB::rollBack();
            return api_error('商家入驻失败');
        }
    }

    /**
     * 每售出一张未特二维码，该分公司账号获得6元奖励
     * @param $sid 商户id
     */
    public function award_money($sid){
        //获取该商户的分公司id
        $company_id = DB::select("select a.inviter_id
from wish_seller s
inner JOIN wish_generalize g
on s.inviter_id = g.id
inner JOIN wish_agent a
on a.id = g.inviter_id
where s.id = ".$sid)[0]->inviter_id;
        $offline_config =  config('offline');
        //得到可以奖励的分公司
        $offline_company_id = $offline_config['company'];
        $award_company_ids = explode(',',$offline_company_id);
        $search_index = array_search($company_id,$award_company_ids);
        if($search_index === false){
            return false;
        }
        //获取需要奖励的分公司手机号
        $company_mobile = WishCompany::where('id',$award_company_ids[$search_index])->value('mobile');
        //找到用户的nodeid
        $nodeid = (new ShopTnetReginfo())->where('nodecode',$company_mobile)->value('nodeid');
        //奖励金额
        $award_money = $offline_config['award_money'];
        //给用户转账

        $data['_cmd'] = 'OfflineShopJoin_giveNodeAward';
        $data['node_id'] = $nodeid;
        $data['money'] = $award_money;
        $data['seller_id'] = $sid;
        $data['company_id'] = $company_id;

        $postData = get_offline_sign_data($data);
        $res = json_decode(curl_post(env('SHOP_DOMAIN'), $postData), true);
        if($res['status'] == 1){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 根据cityID得到 市和地址
     * @param $area_id
     */
    public function getAddressByCityId($area_id){
        $url = base_path() . '/public/sanji/js/code.json';
        $address = file_get_contents($url);
        $address = json_decode($address,true);
        //找到这个地址信息
        $my_area = '';//区域
        $my_city = '';//城市
        //搜索区域
        foreach ($address as $cityKey=>$city){
            foreach ($city as $k=>$c){
                if($k == $area_id){
                    $my_area = $c;
                    $my_city = $cityKey;
                    break;
                }
            }
            if($my_area) break;
        }
        //搜索城市
        foreach ($address as $cityKey=>$city){
            foreach ($city as $k=>$c){
                if($k == $my_city){
                    $my_city = $c;
                    break;
                }
            }
            if(!is_numeric($my_city)) break;
        }
        return [
            'city'=>$my_city,
            'area'=>$my_area,
        ];
    }

    /**
     * 获取定位
     * @param $city 城市
     * @param $query 详细地址
     * @param $standby 备用搜索地址
     */
    public function getLocation($region,$query,$standby){
        $region = urlencode($region);
        $query = urlencode($query);
        $res = false;
        $result = curl_post('http://api.map.baidu.com/place/v2/suggestion?query='.$query.'&region='.$region.'&city_limit=true&output=json&ak=RNifyeVIz4GNTjvl9XwDUth7ufYPk2pX');
        $result = json_decode($result,true);
        if($result['status']!=0){
            return false;
        }
        $address = $result['result'];
        foreach ($address as $loca){
            if($loca['uid']){
                $res = $loca;
            }
        }
        if(empty($loca)){
            //如果得不到地址，寻址备用地址
            $result = curl_post('http://api.map.baidu.com/place/v2/suggestion?query='.$standby.'&region='.$region.'&city_limit=true&output=json&ak=RNifyeVIz4GNTjvl9XwDUth7ufYPk2pX');
            $result = json_decode($result,true);
            if($result['status']!=0){
                return false;
            }
            $address = $result['result'];
            foreach ($address as $loca){
                if($loca['uid']){
                    $res = $loca;
                }
            }
        }
        if($res){
            $coord = new Coord($res['location']['lng'], $res['location']['lat'], Coord::BD09);
            $coord_arr = explode(',', (string)$coord->copy()->toGcj02());
            return $coord_arr;
        }else{
            return false;
        }
    }


}
