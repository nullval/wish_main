<?php

namespace App\Models\Wish;

use App\Models\Admin;
use Illuminate\Support\Facades\Cache;
use App\Models\CommonModel;


/**
 * 用户Session
 */
class WishAdminSession extends CommonModel
{
    protected $table = 'wish_admin_session';
    //不要自动管理我的created_at和updated_at
    public $timestamps = false;
    //超时时间 当前为三天，单位(秒)
    const TIMEOUT = '259200';

    //状态
    const STATUS_ACTIVITY = 0; //活动
    const STATUS_NORMAL = 1; //正常退出
    const STATUS_COERCE = 2; //强制退出

    /**
     * 获取当前新ticket
     * 将新其它数据设置为status = 2
     *
     * @param $node_id
     * @return bool|string
     */
    public function getTicket($adminId,$clientSource='',$clientSystem='')
    {
        //从缓存读取token
        $ticket = $this->getCacheToken($adminId);
        if($ticket != null){
            return $ticket;
        }
        //到数据库寻找未过期的ticket
        $condition = ['admin_id'=>$adminId];
        $statusCondition = ['status'=>self::STATUS_ACTIVITY];
        //得到活动正常且注销时间为null的数据
        $session = $this->where($condition)->where($statusCondition)->whereNull('logout_time')->whereDate('timeout','>',date('Y-m-d H:i:s'))->first();
        if(empty($session)){
            //获取新的token
            //将其它活动中的token设置为强制退出
            $this->where('admin_id',$adminId)->where('status',self::STATUS_ACTIVITY)->update(['status'=>self::STATUS_COERCE]);
            //过期时间
            $timeout = date('Y-m-d H:i:s',time()+self::TIMEOUT);
            $ticket = uuid();
            $sso = array(
                'session_id' => session_id(),
                'ticket' => $ticket,
                'admin_id' => $adminId,
                'status' => self::STATUS_ACTIVITY, //{活动中=0,正常退出=1,强制退出=2}
                'ip_address' => getClientIp(),
                'client_source' => $clientSource ?: '3',
                'client_system' => $clientSystem ?: 'MicroMessenger',
                'timeout'=>$timeout
            );
            $result = $this->insert($sso);
            if ($result) {
                $this->setCacheToken($adminId, $ticket,(self::TIMEOUT / 60));
                return $ticket;
            }
            return false;
        }else{
            return $session['ticket'];
        }
    }

    /**
     * 退出登录
     * @param $ticket
     * @return mixed
     */
    public function logout($ticket){
        if(empty($ticket)){
            return false;
        }
        $condition = ['ticket'=>$ticket];
        $session = $this->where($condition)->first();
        //如果当前状态为正常
        if($session['status'] == self::STATUS_ACTIVITY){
            //设置正常退出
            $this->where($condition)->update([
                'status'=>self::STATUS_NORMAL,
                'logout_time'=>date('Y-m-d H:i:s'),
                'remarks'=>'用户退出了登录'
            ]);
        }
        //从缓存删除token
        $this->delCacheToken($session['admin_id']);
        return true;
    }

    /**
     * 根据ticket得到wish_admin表数据
     * @param $ticket
     */
    public function getAdminInfoByTicket($ticket){
        if(empty($ticket)){
            return false;
        }
        return Admin::where('a1.ticket',$ticket)
            ->join('wish_admin_session as a1','wish_admin.id','=','a1.admin_id')
            ->select([
                'wish_admin.*'
            ])
            ->first();
    }

    /**
     * 设置token
     */
    public function setCacheToken($adminId,$ticket,$timeOut){
        Cache::store('redis')->put('token_'.$adminId,$ticket,$timeOut);
    }
    /**
     * 从缓存读取token
     */
    public function getCacheToken($adminId){
        return Cache::store('redis')->get('token_'.$adminId);
    }

    /**
     * 从缓存删除token
     */
    public function delCacheToken($adminId){
        return Cache::store('redis')->forget('token_'.$adminId);
    }

}