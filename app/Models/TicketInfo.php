<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketInfo extends Model
{
	protected $table = 'ticket_info';

	/**
	 * 发票信息新增/更新
	 * @param array $data
	 * @return array
	 */
	public function subinfo($data=array()){
		$agent_uid=session('agent_info')->id;
		$customerType=$data['customerType'];
		$registerNo=$data['registerNo'];
		$bank=$data['bank'];
		$bankNo=$data['bankNo'];
		$operatingLicenseAddress=$data['operatingLicenseAddress'];
		$operatingLicensePhone=$data['operatingLicensePhone'];
		$Invoice_type=$data['Invoice_type'];
		$Invoice_header=$data['Invoice_header'];
		//判断机主的发票信息是否存在，不存在则创建，存在则更新
		$invoice_info=$this->where(['agent_uid'=>$agent_uid])->first();
		if (empty($invoice_info)){
			//不存在
			//判断是个人还是企业
			if ($customerType==1){
				//个人
				$arr1['customerType']=$customerType;
				$arr1['Invoice_header']='个人';
				$arr1['Invoice_type']=1;
				$arr1['created_at']=time2date();
				$arr1['agent_uid']=$agent_uid;
				$arr1['issub']=1;
				$this->Insert($arr1);
				return arr_post(1,'提交成功');
			}elseif($customerType==2){
				//企业
				$arr2['customerType']=$customerType;
				$arr2['registerNo']=$registerNo;
				$arr2['bank']=$bank;
				$arr2['bankNo']=$bankNo;
				$arr2['operatingLicenseAddress']=$operatingLicenseAddress;
				$arr2['operatingLicensePhone']=$operatingLicensePhone;
				$arr2['Invoice_type']=$Invoice_type;
				$arr2['Invoice_header']=$Invoice_header;
				$arr2['created_at']=time2date();
				$arr2['agent_uid']=$agent_uid;
				$arr2['issub']=1;
				$this->Insert($arr2);
				return arr_post(1,'提交成功');
			}

		}else{
			//存在
			//判断是个人还是企业
			if ($customerType==1){
				//个人
				$arr1['customerType']=$customerType;
				$arr1['Invoice_header']='个人';
				$arr1['Invoice_type']=1;
				$arr1['registerNo']='';
				$arr1['bank']='';
				$arr1['bankNo']='';
				$arr1['operatingLicenseAddress']='';
				$arr1['operatingLicensePhone']='';
				$arr1['agent_uid']=$agent_uid;
				$arr1['updated_at']=time2date();
				$arr1['issub']=1;
				$this->where(['agent_uid'=>$agent_uid])->update($arr1);
				return arr_post(1,'提交成功');
			}elseif($customerType==2){
				//企业
				$arr2['customerType']=$customerType;
				$arr2['registerNo']=$registerNo;
				$arr2['bank']=$bank;
				$arr2['bankNo']=$bankNo;
				$arr2['operatingLicenseAddress']=$operatingLicenseAddress;
				$arr2['operatingLicensePhone']=$operatingLicensePhone;
				$arr2['Invoice_type']=$Invoice_type;
				$arr2['Invoice_header']=$Invoice_header;
				$arr2['updated_at']=time2date();
				$arr2['agent_uid']=$agent_uid;
				$arr2['issub']=1;
				$this->where(['agent_uid'=>$agent_uid])->update($arr2);
				return arr_post(1,'提交成功');
			}
		}
	}
}
