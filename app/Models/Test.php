<?php
namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Exception;

class Test extends CommonModel {
    protected $table = 'test';


    public function test(){
        $rs=DB::table('test')->insert(['created_at'=>time2date(),'value'=>'aaee']);


    }


    public function userWallet($user_id ,$pursetype,$owner_type,$field=['*']){
        if( !$pursetype){
//            throw new ApiException("用户钱包参数错误!");
			return arr_post(0,'用户钱包参数错误');
        }
        $purseCon = [
            'owner_type' => $owner_type,
            'owner_id' => $user_id,
            'purse_type' => $pursetype,
        ];
        $purse = DB::table('user_purse')->where($purseCon)->first($field);
        // 如果钱包不存在,则创建一个
        if(!$purse){
            $purseCon['create_time'] = time();
            $id = DB::table('user_purse')->insertGetId($purseCon);
            if(!$id){
//                throw new ApiException("获取用户钱包数据失败,请稍后重试");
				return arr_post(0,'获取用户钱包数据失败,请稍后重试');
            }
            $purse = DB::table('user_purse')->where(['purse_id'=>$id])->first($field);
        }

        //字段处理
        $purse->balance=get_last_two_num($purse->balance);
        $purse->freeze_value=get_last_two_num($purse->freeze_value);
        return $purse;
    }
}