<?php
namespace App\Models;

use App\Exceptions\ApiException;
use App\Libraries\JPush\JPush;
use Illuminate\Support\Facades\DB;

class JushMessage extends CommonModel {

    /**
     * @param string $order_sn 订单编号
     * @param int $status 订单状态
     * 向相应的终端号极光推送订单信息(打印信息)
     */
    public function send_order_status($order_sn,$status,$amount,$pay_type=1){
        new_logger('jiguang.log','jiguang',['order_sn'=>$order_sn]);
        /**极光推送**/
        $order_info=Order::where(['order_sn'=>$order_sn])->first();
        $data['order_sn']=$order_sn;
        $data['status']=$status;
        if($pay_type==1){
            $bank_code='BACKSTAGEWECHAT';
            $data['pay_type']='微信支付';
        }elseif($pay_type==2){
            $bank_code='BACKSTAGEALIPAY';
            $data['pay_type']='支付宝支付';
        }elseif($pay_type==3){
            $bank_code='银行直连参数';
            $data['pay_type']='银行卡支付';
        }
        $data['bank_code']=$bank_code; // 银行直连参数，反扫类型
        $data['terminal_no']=$order_info->terminalId;//终端号
        $pos_info=Pos::where(['terminalId'=>$order_info->terminalId])->first();
        $uid=empty($pos_info->uid)?$order_info->uid:$pos_info->uid;
        $ruzhu_info=RuzhuMerchantBasic::where(['uid'=>$uid])->first();
        if(isset($ruzhu_info->id)){
            $data['merchantName']=$ruzhu_info->merchantName;//商户名称
            $data['merchant_no']=$ruzhu_info->merchantId;//商户号
        }else{
            $data['merchantName']='未特商城科技（深圳）有限公司';//商户名称
            $data['merchant_no']='00000000';//商户号
        }
        $data['order_id']=$order_info->id+1000000;//凭证号
        $data['order_no']=$this->order_sn;//交易单号
        $data['pay_time']=$order_info->created_at->format('Y-m-d H:i:s');//交易时间
        $data['amount']=$amount;//交易金额
		$data['type']='';	// 订单类型
		// 买家手机号
		$data['mobile_buyer'] =$order_info->buyer_id == 64 ? '' : User::where(['id'=>$order_info->buyer_id])->value('mobile');
		$data['mobile_buyer'] = substr($data['mobile_buyer'],0,1) == '1' ? $data['mobile_buyer'] : '';
		// 操作员手机号
		$data['mobile_operate'] = SellerRole::where(['mobile'=>$order_info->role_mobile])->value('role_name');

//        return json_success('查询成功',$data);
        if($order_info->type==2||$order_info->type==4||$order_info->type==6){
            //机主版
            $JPush = new JPush(1);
        }elseif($order_info->type==1||$order_info->type==3||$order_info->type==5||$order_info->type==7){
            //商家版
            $JPush = new JPush(2);
            if($order_info->type == 5){
				$data['type'] = 'qrcode';
			}
        }
		// 如果是消费买单，并且是魔方pos机
        if($order_info->type != 1 && $pos_info->pos_type == 4){
        	$JPush = new JPush(4);
		}
        $extras = [
            'data'=> json_encode(['type' => 1,'data' => $data])
        ];

        // 2018-01-11 修改，扫码订单商户下所有pos机都推送消息
		if($order_info->type==5){
            $terminalId_list=Pos::where(['uid'=>$order_info->uid])->get(['terminalId']);
            if(!$terminalId_list->isEmpty()){
                foreach ($terminalId_list as $k=>$v){
                    $terminal_id=$v->terminalId;
                    new_logger('jiguang.log','jiguang',['terminalId'=>$terminal_id]);
                    $JPush->push_tags($terminal_id,'订单支付通知','订单支付通知',$extras);
                }
            }
		}else{
			new_logger('jiguang.log','jiguang',['terminalId'=>$order_info->terminalId]);
			$JPush->push_tags($order_info->terminalId,'订单支付通知','订单支付通知',$extras);
		}
        return true;
    }

    
}