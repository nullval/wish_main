<?php
namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Support\Facades\DB;

class OptionPointAdmin extends CommonModel {
     protected $table = 'option_point_admin';

     public function check_user_status(){
         $username=request('username');
         $secret_key=request('secret_key');
         if(empty($username)){
             throw new ApiException("请传入操作员名称");
         }
         if(empty($secret_key)){
             throw new ApiException("请传入操作员密钥");
         }
         $info=$this->where(['username'=>$username])->first();
         if(!isset($info->id)){
             throw new ApiException("操作员不存在");
         }
//         var_dump($info->secret_key);exit;
         if($info->secret_key!=$secret_key){
             throw new ApiException("操作员密钥不正确");
         }
         if($info->status==0){
             throw new ApiException("该操作员被禁止操作");
         }
         return $info->id;
     }
}