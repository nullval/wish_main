<?php

namespace App\Models;

use App\Jobs\handleDownloadQr;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

//use Illuminate\Support\Facades\DB;

class PushQrOrder extends Model
{
    protected $table = 'wish_pushqr_order';

    /**
     * 创建地推二维码订单
     *
     * @param $contact_user
     * @param $contact_mobile
     * @param $count
     * @param $id_arr
     * @param $operate_id
     * @return array
     * @throws \Exception
     */
    public function add_order($contact_user, $contact_mobile, $count, $id_arr, $operate_id, $role_id)
    {
        $pushQrOrder = new PushQrQrcode();
        $Admin = new Admin();
        $arr['contact_user'] = $contact_user;
        $arr['contact_mobile'] = $contact_mobile;
        $arr['status'] = 1;//已下发
        $arr['count'] = $count;
        $arr['id_arr'] = $id_arr;
        $arr['operate_id'] = $operate_id;
        $arr['created_at'] = time2date();
        $arr['order_sn'] = 'pushQr' . create_order_no(rand(1, 999999));
        //检查$contact_mobile 是否为自己的下级
        $res = DB::table($Admin->getChildTable($role_id))->where('mobile', $contact_mobile)->value('id');
        if (empty($res)) {
            return arr_post(0, '手机号不存在');
        }
        $own_id = DB::table('wish_admin')->where('username', $contact_mobile)->value('id');
        if ($own_id == $operate_id) {
            return arr_post(0, '你不能下发给自己');//2018-8-17 19:31:53
        }
        $children = $Admin->find_down($operate_id)->toArray();
        //检查用户二维码库存
        if (!in_array($own_id, $children)) {
            return arr_post(0, '该手机号非自己的下级');
        }
        if ($role_id == 1) {
            $myNum = $pushQrOrder->getMyNum(0);
        } else {
            $myNum = $pushQrOrder->getMyNum($operate_id);
        }
        if ($myNum < $count) {
            return arr_post(0, '库存不足,请联系上级下发二维码');
        }
        //插入一条订单
        $this->insertGetId($arr);
        //变更二维码归属人
        if ($role_id == 1) {
            $operate_id = 0;
        }
        $pushQrOrder->where('agent_id', $operate_id)->where('state',1)->orderBy('id')->limit($count)->update([
            'agent_id' => $own_id,//新主人
        ]);
        return arr_post(1, '发放成功');
    }

    /**
     * 下载二维码
     *
     * @param $sid
     * @return array
     * @throws \Exception
     */
    public function downloadQr($count)
    {
        //查询订单信息
        //防止重复处理
        $indexOf = DB::table('promotional_code_record')->sum('code_count');////todo 本次下标
        new_logger('download.log', '发布下载任务');
        //发布任务,插入表格
        dispatch((new handleDownloadQr($count, $indexOf))->onQueue('handleOrder'));//执行任务
        return arr_post(1, '下载成功,请稍后查看');
    }

    /**
     * 检查库存
     *
     * @param int $id 审核人员ID
     * @param int $order_id 订单ID
     * @return string
     * @throws \Exception
     */
    public function checkStock($id, $order_id)
    {
        //检查该订单需要的数量
        $count = intval($this->where('id', $order_id)->value('count'));
        //找到自己申请成功的数量
        $my_count = $this->where('status', 1)->where('operate_id', $id)->sum('count');
        //找到自己下级已经成功的数量
        $child_ids = (new Admin())->find_down($id);
        $child_count = $this->where('status', 1)->whereIn('operate_id', $child_ids)->sum('count');
        $stock = intval(bcsub($my_count, $child_count, 0));
        if ($count > $stock) {
            return arr_post(0, "库存不足,您的库存为{$stock}张,本地需要{$count}张,还需购买" . ($count - $stock) . "张,请申请地推二维码");
        } else {
            return arr_post(1, '库存充足');
        }
    }

    /**
     * 获取库存情况
     *
     * @param $id
     * @param $role_id
     * @return array
     * @throws \Exception
     */
    public function getAmount($id, $role_id)
    {
        if ($role_id == 1) {
            $stock = DB::table('extension_code')->where('agent_id', 0)->where('state',1)->count();
        } else {
            $stock = DB::table('extension_code')->where('agent_id', $id)->where('state',1)->count();
        }
        //找到自己申请成功的数量
//        $my_count = $this->where('status', 1)->where('operate_id', $id)->sum('count');
        //找到自己下级已经成功的数量
//        if ($role_id != 9) {
//            $child_ids = (new Admin())->find_down($id);
//            $child_count = $this->where('status', 1)->whereIn('operate_id', $child_ids)->sum('count');
//        } else {
//            $child_count = 0;
//        }
//        $stock = intval(bcsub($my_count, $child_count, 0));
        return [
//            'buy_amount' => $my_count,
//            'sole_out' => $child_count,
            'own_amount' => $stock
        ];
    }

}
