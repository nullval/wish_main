<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SellerRole extends Model
{
	protected $table = 'seller_role';

	/*
	 * @param $role_name 员工编号
	 * @param $permission 权限列表 以,隔开
	 * @param $mobile 员工账号
	 * @param $terminalId POS机终端号
	 * @param $role_id 员工id
	 * 添加管理员

	 */
	public function add_role($request_info){
		$role_name = $request_info['role_name'];
		$mobile = $request_info['mobile'];
//		$terminalId = $request_info['terminalId'];
		$role_id = $request_info['id'];
		$role_job = $request_info['role_job'];
		//判断是否有选择POS机
//		if ($terminalId == '0'){
//			return arr_post(0,'请选择相应的POS机');
//		}
		//判断店员编号是不是整数
		if (strpos($role_name,".") !== false){
			return arr_post(0,'店员编号必须为整数');
		}
		//判断店员编号是不是6位
		if (strlen($role_name) != 6){
			return arr_post(0,'店员编号必须6位数字');
		}
		//判断是否有选择权限
		if (empty($request_info['permission'])){
			return arr_post(0,'请选择相应的权限');
		}
		if (empty($role_id)){
			//判断这个手机是否成为商家
			$seller = Seller::where(['mobile' => $mobile]) -> first();
			if (!empty($seller)){
				return arr_post(0,'该账户已经是商家，无法成为您的员工');
			}
			//判断他是否已经成为其他店的员工
			$role1 = $this -> where([['mobile', '=', $mobile ], ['seller_id', '<>', session('seller_info') -> id]]) -> first();
			if (!empty($role1)){
				return arr_post(0,'该员工已经是其他店的员工，请重新选择其他员工');
			}
			//判断是否添加过
			$role = $this->where(['mobile'=>$mobile,'seller_id'=>session('seller_info') -> id])->first();
			if (!empty($role)){
				return arr_post(0,'该员工已经添加过');
			}
		}


		//寻找所选功能模块的上级目录并赋值给$request_info['permission']
		$permission_pids = SellerPermission::whereIn('id',$request_info['permission'])->get();
		foreach ($permission_pids as $v){
			$request_info['permission'][] = $v['pid'];
		}
		//去掉重复项
		$request_info['permission'] = array_unique($request_info['permission']);
		$permission = implode(',',$request_info['permission']);

		$role_arr['role_name']=$role_name;
		$role_arr['role_job']=$role_job;
		$role_arr['permission']=$permission;
		$role_arr['mobile']=$mobile;
//		$role_arr['terminalId']=$terminalId;
		$role_arr['seller_id']=session('seller_info') -> id;
		if(!empty($role_id)){
			//修改
			$role_arr['updated_at']=time2date();
			$this->where(['id'=>$role_id])->update($role_arr);
		}else{
			//添加
			$role_arr['created_at']=time2date();
			$this->insert($role_arr);
		}
		return arr_post(1,'员工添加成功');
	}


	public function scopeDetail($query,$admin_id,$field=['*'])
	{
		return $query->where(['id'=>$admin_id])->first($field);
	}
}
