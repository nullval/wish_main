<?php
namespace App\Models;
use Illuminate\Support\Facades\DB;


class TdAdmin extends CommonModel{
    protected $table = 'td_admin';


    public function scopeDetail($query,$admin_id,$field=['*'])
    {
        return $query->where(['id'=>$admin_id])->first($field);
    }

    /**
     * @param $login_arr
     * @return bool
     * 管理员后台登录
     */
    public function login($username,$password){
        $admin_info=$this->where(['username'=>$username])->first();
        if(empty($admin_info)){
            return arr_post(0,'用户不存在');
        }else{
            if($admin_info['status']==0){
                return arr_post(0,'您被禁止登录');
            }
            if(md5(md5($password))!=$admin_info->password){
                return arr_post(0,'密码不正确');
            }else{
                $this->where(['id'=>$admin_info->id])->update(['last_time'=>time2date()]);
                //session存储用户信息
                session(['td_admin_info'=>$admin_info]);

//                //获取用户角色权限列表
//                $permission=AdminRole::where(['id'=>$admin_info->role_id])->first(['permission'])['permission'];
//                $permission=explode(',',$permission);
//                $permission_list=AdminPermission::whereIn('id',$permission)->orderBy('sort','DESC')->get();
//                //格式化菜单
//                $AdminPermission=new AdminPermission();
//                $permission_list=$AdminPermission->sort_permission($permission_list);
//                //session存储用户权限
//                session([$admin_info->id.'_permission_list'=>$permission_list]);

                //登录成功记录信息
                return arr_post(1,'登录成功');
            }
        }
    }

    /*
     * @param $username 用户名
     * @param $password 用户密码
     * @param $role_id 角色id
     * @param $admin_id 用户id
     * 添加管理员
     */
    public function add_admin($username,$password='',$role_id=0,$admin_id=0){
        $admin_arr['username']=$username;
        $admin_arr['role_id']=$role_id;
        if(!empty($admin_id)){
            //修改
            if(!empty($password)){
                $admin_arr['password']=md5(md5($password));
            }
            $admin_arr['updated_at']=time2date();
            $this->where(['id'=>$admin_id])->update($admin_arr);
        }else{
            //添加
            $admin_arr['password']=md5(md5($password));
            $admin_arr['last_time']=time2date();
            $admin_arr['created_at']=time2date();
            $this->insert($admin_arr);
        }
        return arr_post(1,'管理员添加成功');
    }
}