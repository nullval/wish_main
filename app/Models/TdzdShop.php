<?php
namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Support\Facades\DB;

//转接天地之道积分商城
class TdzdShop extends CommonModel {

    /**
     * @param $action 方法
     * @param $param 参数
     * @param string $method 请求类型
     * @return mixed
     * 请求天地之道积分商城获取数据
     */
    public function change_over($param){
        $url = env('APP_DEBUG') ? 'http://tdzdshopapi.343wew.top/' : 'http://shop-api.3dqxm.com/';
        $param = $this->get_sign_param($param);
        $result = curl_post($url,$param);
        $result = json_decode($result,true);
        if($result['status'] != 1){
            return arr_post(0,$result['info']);
        }
        return $result['data'];
    }

    /**
     * @param $param 参数
     * @param string $method 请求类型
     * @return string
     * 获取天地之道积分商城接口签名
     */
    public function get_sign_param($param,$method='post'){
        unset($param['sign']);
        $param2 = [
            'ClientSystem'	=> 'tdzd',
            'ClientVersion'	=> '0',
            'ApiVersion'	=> '1.0',
        ];
        $param = array_merge($param,$param2);
        ksort($param);
        $param2 = [];
        foreach($param as $k => $v){
            $param2[] = $k.'='.$v;
        }
        $sign = strtolower(md5(implode('&',$param2).'eB8B2D02F81F02E29FC94461D6F07F57Ds'));
        $param['sign'] = $sign;
        return $param;
    }
}