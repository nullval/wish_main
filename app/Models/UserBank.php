<?php

namespace App\Models;

use App\Exceptions\ApiException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserBank extends Model
{
	protected $table = 'wish_user_bank';

    /**
     * @param $uid
     * @param $account_name
     * @param $account_bank
     * @param $bank_account
     * @param $ibankno
     * @param int $type
     * 更新或编辑默认卡
     */
	public function add_or_update_default_bank($uid,$account_name,$account_bank,$bank_account,$ibankno,$type,$identity='',$phone=''){
        //判断是否有已经绑定过银行卡
        $bank_id=UserBank::where(['uid'=>$uid,'type'=>$type,'isdefault'=>1])->value('id');
        return $this->add_bank($uid,$account_name,$account_bank,$bank_account,$ibankno,1,$type,$bank_id,$identity,$phone);
    }


    /**
     * @param $uid 用户id
     * @param $account_name 持卡人
     * @param $account_bank 开户银行
     * @param $bank_account 银行卡号
     * @param $ibankno 银行卡所属地区银行联行号
     * @param int $isdefault 是否设置为默认 1是 0否
     * @param int $type 用户类型 用户类型 1用户 2商家 3代理(运营商) 4系统  6地推人员
     * @param int $bank_id 银行卡id 有则修改 无则添加
     * @return array
     * 添加/编辑银行卡
     */
    public function add_bank($uid,$account_name,$account_bank,$bank_account,$ibankno,$isdefault=0,$type=1,$bank_id=0,$identity='',$phone=''){
        if(empty($uid)){
            throw new ApiException('用户不存在');
        }
        if(empty($account_name)){
            throw new ApiException('请输入持卡人姓名');
        }
        if(empty($account_bank)){
            throw new ApiException('请输入开户银行');
        }
        if(empty($bank_account)){
            throw new ApiException('请输入银行卡号');
        }
        if(empty($ibankno)){
            throw new ApiException('请输入银行卡所属地区银行联行号');
        }
        if(empty($identity)){
            throw new ApiException('请输入身份证');
        }
        if(empty($phone)){
            throw new ApiException('请输入所在银行预留手机号码');
        }
        $bank_info['uid']=$uid;
        $bank_info['account_name']=$account_name;
        $bank_info['account_bank']=$account_bank;
        $bank_info['bank_account']=$bank_account;
        $bank_info['ibankno']=$ibankno;
        $bank_info['isdefault']=$isdefault;
        $bank_info['identity']=$identity;
        $bank_info['phone']=$phone;
        $bank_info['type']=$type;
        //判断银行卡是否已存在
        $bank_info2=$this->where(['ibankno'=>$ibankno,'account_name'=>$account_name,'account_bank'=>$account_bank,'bank_account'=>$bank_account,'uid'=>$uid,'type'=>$type])->first();
        if(isset($bank_info2->id)){
            throw new ApiException('银行卡已存在');
        }
        if(!empty($bank_id)){
            $bank_info1=$this->where(['id'=>$bank_id])->first();
            if(!isset($bank_info1->id)){
                throw new ApiException('银行卡不存在');
            }
            //编辑
            $bank_info['updated_at']=time2date();
            $this->where(['id'=>$bank_id])->update($bank_info);
            $error_remark='银行卡编辑失败';
        }else{
            //限定10张
            $count=$this->where(['uid'=>$uid,'type'=>$type])->count();
            if($count>=10){
                throw new ApiException('最多绑定10张');
            }
            //添加
            $bank_info['created_at']=time2date();
            $bank_id=$this->insertGetId($bank_info);
            $error_remark='银行卡添加失败';
        }
        if($bank_id){
            if($isdefault==1){
                $this->update_bank_default($bank_id,$uid,$type);
            }
            return true;
        }else{
            throw new ApiException($error_remark);
        }
    }

    /**
     * @param $bank_id 设置为默认的银行卡id
     * @param $uid 用户id
     * @param $type 用户类型 用户类型 1用户 2商家 3代理(机主)
     * 设置用户银行卡为默认银行卡
     */
    public function update_bank_default($bank_id,$uid,$type){
        $bank_info1=$this->where(['id'=>$bank_id])->first();
        if(!isset($bank_info1->id)){
            throw new ApiException('银行卡不存在');
        }
        $this->where(['uid'=>$uid,'type'=>$type])->update(['isdefault'=>0]);
        $this->where(['id'=>$bank_id,'uid'=>$uid,'type'=>$type])->update(['isdefault'=>1]);
        return true;
    }

    /**
     * @param $bank_id 要删除的银行卡id
     * @param $uid 用户id
     * @param $type 用户类型 用户类型 1用户 2商家 3代理(机主)
     * 删除银行卡
     */
    public function del_bank($bank_id,$uid,$type){
        if(empty($bank_id)){
            throw new ApiException('请选择要删除的银行卡');
        }
        if(empty($uid)){
            throw new ApiException('用户不存在');
        }
        $rs=$this->where(['id'=>$bank_id,'uid'=>$uid,'type'=>$type])->delete();
        if($rs){
            return true;
        }else{
            throw new ApiException('银行卡删除失败');
        }
    }


}
