<?php

namespace App\Console;

use App\Jobs\checkGhtEnterStatus;
use App\Jobs\checkPay;
use App\Libraries\Huanxun\Huanxun;
use App\Models\Api\ApiStatement;
use App\Models\Api\ApiTdGiveUseTdLog;
use App\Models\Api\ApiTdStatement;
use App\Models\Gift;
use App\Models\RuzhuMerchantBank;
use App\Models\RuzhuMerchantBasic;
use App\Models\Seller;
use App\Models\StatementDateLog;
use App\Models\Test;
use App\Models\UserBehavior;
use App\Models\UserPurse;
use App\Models\VUserWithdraw;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if(config('app.env')=='online'){
//            $this->createStatement($schedule);
//            $this->backCash($schedule);
//            $this->checkPay($schedule);
//            $this->checkGhtEnterStatus($schedule);
//            $this->giveBackTd($schedule);
//            $this->createBackTdLog($schedule);
//            $this->evaluateSellerCredit($schedule);
//            $this->recoveryRedpack($schedule);
//            $this->backSellerPoint($schedule);
            $this->delLogs($schedule);
            $this->delBehaviorLog($schedule);
        }
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }

    #定期删除5个月的日志文件
    private function delLogs($schedule){
        $schedule->call(function () {
            del_dir(storage_path().'/logs/'.date('Y/m',strtotime('-5 month')));
        })->dailyAt('00:10');
    }

    #每天晚上0点形成商家对账单,并商家返现
    private function createStatement($schedule){
        //2018/4/14号不再生成对账单
//        if(time()<1523635800){
//            $schedule->call(function () {
//                ignore_user_abort(true);
//                //允许长时间执行
//                set_time_limit(0);
//                //获取昨天日期 (方便手动调数据) 手动调数据必须先把achievement_date_log表里昨日的数据删掉 要先看看status是否为0 1执行成功 0执行失败 0才要进行手动调整
////            $y_date='';
//                $yesterday=empty($y_date)?date("Y-m-d",strtotime("-1 day")):$y_date;
//                //判断昨天日期是否执行(防止多次执行)业绩执行时间记录 (无论执行成功或失败)
//                $log_info=StatementDateLog::where(['date'=>$yesterday])->first();
//                if(isset($log_info->id)){
//                    echo '该日期已执行过';exit;
//                }
//                $ApiStatement=new ApiStatement();
//                $ApiStatement->create_statement();
//                //添加业绩执行时间记录
//                StatementDateLog::updateOrCreate(['date'=>$yesterday,'status'=>1]);
//            })->dailyAt('01:10');
//        }
    }

    #每天晚上2点30分根据商家对账单返现
    private function backCash($schedule){
        $schedule->call(function () {
            ignore_user_abort(true);
            //允许长时间执行
            set_time_limit(0);
            $ApiStatement=new ApiStatement();
            $ApiStatement->back_cach();
        })->dailyAt('02:30');
    }

    #每1分钟执行代付系统的查询第三方查询结果
    private function checkPay($schedule){
        $schedule->call(function () {
            //获取所有未有代付结果的订单
            $withdraw_list=VUserWithdraw::where(['status'=>1])->whereIn('purse_type',[1,3,4,7,10,12])->get();
            foreach ($withdraw_list as $k=>$v){
                $i=$v->created_at->format('Y');
                //队列执行
                ((new checkPay($v->spare_order_sn,$i))->onQueue('handleOrder'))->handle();
//                (new checkPay($v->spare_order_sn,$i))->handle();
            }
        })->everyMinute();
    }

    #每1个钟执行查询商家入驻高汇通状态
    private function checkGhtEnterStatus($schedule){
        $schedule->call(function () {
            $seller_list=RuzhuMerchantBasic::where(['status'=>2])->whereNotNull('merchantId')->orderBy('id','desc')->get();
            foreach ($seller_list as $k=>$v){
                //队列执行
                (new checkGhtEnterStatus($v->merchantId,$v->uid))->handle();
            }
        })->everyMinute();
    }

    #每天晚上2点10分根据刷pos机释放TD对账单释放TD
    private function giveBackTd($schedule){
        $schedule->call(function () {
            ignore_user_abort(true);
            //允许长时间执行
            set_time_limit(0);
            $ApiTdStatement=new ApiTdStatement();
            $ApiTdStatement->give_back_td();
        })->dailyAt('02:10');
    }

    #每天晚上1点15分后台创建特殊人员返td脚本
    private function createBackTdLog($schedule){
        $schedule->call(function () {
            //第一个月，在年前2-10号释放500个TD，后面每个月20号释放200个TD，每个人100万封顶，一个月一次，已经确定好了，可以执行了。
            //每月20号生成下次的对账单
            if(date('d')=='20'){
                $arr=[9,10,11,12,13,14];
                foreach($arr as $v){
                    $d=date('Y-m',strtotime(date('Y-m-d'))+30*24*3600);
                    $log['u_date']=$d.'-20';
                    $log['user_id']=$v;
                    $log['created_at']=time2date();
                    $log['status']=1;
                    $log['amount']=200;
                    $log['order_list']='';
                    $log['left_amount']=200;
                    $log['remark']='特殊人员后台数据添加';
                    ApiTdGiveUseTdLog::insert($log);
                }
            }
        })->dailyAt('01:15');
    }

    #每天晚上4点10分计算商家信用积分
    private function evaluateSellerCredit($schedule){
        $schedule->call(function () {
            $Seller=new Seller();
            $Seller->evaluate_seller_credit();
        })->dailyAt('04:10');
    }

    #每1个钟查询平台分润发送的过期红包并回收
    private function recoveryRedpack($schedule){
        $schedule->call(function () {
            $Gift=new Gift();
            $Gift->recovery_redpack();
        })->hourly();
    }

    #原系统商家账户持有“补贴积分”数量每日按照持有总数的2‰（千分之二）释放到新系统对应的商家账户
    private function backSellerPoint($schedule){
        $schedule->call(function () {
            (new UserPurse())->back_seller_point();
        })->dailyAt('01:15');
    }


    #定时清除2个月前的用户行为日志
    private function delBehaviorLog($schedule){
        $schedule->call(function () {
            UserBehavior::whereDate('created_at', '<',date('Y-m-d',strtotime('-2 day')))->delete();
        })->dailyAt('01:15');
    }
}
