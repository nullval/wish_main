<?php
return [
	
	// 分页每页数量
	'page_size'		=> 20,
	
	// 是否测试环境
	'test'			=> config('app.env') == 'production' ? false : true,
	'production'	=> config('app.env') == 'production' ? true : false,
	
	// bug 邮件接收者
	'bug_receives'	=> [
		'512511253@qq.com',
	],
	
	// GPU 电子银行地址
	'gpu_url' => env('GPU_URL', 'http://gpu-center.thinkms.com'),
	'wap_url' => env('WAP_URL', 'http://localhost'),
];
