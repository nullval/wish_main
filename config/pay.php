<?php

return [
    'notify_type' => [
        0=>[
            'name'=>'线下',
            'poundage'=>0, //收取的手续费率  (千分之几)
            'poundage_reason'=>'00001', //转账原因
            'owner_type'=>0,
            'pay_method_poundage'=>[ //各种支付手续费率 (千分之几) 对应订单表的pay_method字段
                'H5'=>0,//H5支付
                'jsapi'=>0, //jsapi支付(微信公众号支付)
                'stage'=>0, //反扫
                'bank'=>0, //银行卡支付
                'app'=>0, //app支付
            ]
        ],
        1=>[
            'name'=>'通联',
            'poundage'=>6, //收取的手续费率  (千分之几)
            'poundage_reason'=>'10018', //转账原因
            'owner_type'=>7,
            'pay_method_poundage'=>[ //各种支付手续费率 (千分之几) 对应订单表的pay_method字段
                'H5'=>6,//H5支付
                'jsapi'=>6, //jsapi支付(微信公众号支付)
                'stage'=>6, //反扫
                'bank'=>6, //银行卡支付
                'app'=>6, //app支付
            ]
        ],
        2=>[
            'name'=>'高汇通',
            'poundage'=>7,
            'poundage_reason'=>'10054',
            'owner_type'=>9,
            'pay_method_poundage'=>[ //各种支付手续费率 (千分之几) 对应订单表的pay_method字段
                'H5'=>7,//H5支付
                'jsapi'=>7, //jsapi支付(微信公众号支付)
                'stage'=>7, //反扫
                'bank'=>7, //银行卡支付
                'app'=>7, //app支付
            ]
        ],
        3=>[
            'name'=>'环讯',
            'poundage'=>8,
            'poundage_reason'=>'10076',
            'owner_type'=>13,
            'pay_method_poundage'=>[ //各种支付手续费率 (千分之几) 对应订单表的pay_method字段
                'H5'=>8,//H5支付
                'jsapi'=>8, //jsapi支付(微信公众号支付)
                'stage'=>8, //反扫
                'bank'=>8, //银行卡支付
                'app'=>8, //app支付
            ]
        ],
        4=>[
            'name'=>'亿贤',
            'poundage'=>6,
            'poundage_reason'=>'10077',
            'owner_type'=>14,
            'pay_method_poundage'=>[ //各种支付手续费率 (千分之几) 对应订单表的pay_method字段
                'H5'=>6,//H5支付
                'jsapi'=>6, //jsapi支付(微信公众号支付)
                'stage'=>6, //反扫
                'bank'=>6, //银行卡支付
                'app'=>6, //app支付
            ]
        ],
        5=>[
            'name'=>'越满',
            'poundage'=>10,
            'poundage_reason'=>'10078',
            'owner_type'=>15,
            'pay_method_poundage'=>[ //各种支付手续费率 (千分之几) 对应订单表的pay_method字段
                'H5'=>10,//H5支付
                'jsapi'=>10, //jsapi支付(微信公众号支付)
                'stage'=>10, //反扫
                'bank'=>10, //银行卡支付
                'app'=>10, //app支付
            ]
        ],
        6=>[
            'name'=>'魔方',
            'poundage'=>6,
            'poundage_reason'=>'10104',
            'owner_type'=>16,
            'pay_method_poundage'=>[ //各种支付手续费率 (千分之几) 对应订单表的pay_method字段
                'H5'=>6,//H5支付
                'jsapi'=>6, //jsapi支付(微信公众号支付)
                'stage'=>6, //反扫
                'bank'=>6, //银行卡支付
                'app'=>6, //app支付
            ]
        ],
        7=>[
            'name'=>'银盛',
            'poundage'=>6,
            'poundage_reason'=>'10077',
            'owner_type'=>17,
            'pay_method_poundage'=>[ //各种支付手续费率 (千分之几) 对应订单表的pay_method字段
                'H5'=>6,//H5支付
                'jsapi'=>6, //jsapi支付(微信公众号支付)
                'stage'=>6, //反扫
                'bank'=>6, //银行卡支付
                'app'=>6, //app支付
            ]
        ],
        8=>[
            'name'=>'扫呗',
            'poundage'=>4.5,
            'poundage_reason'=>'10117',
            'owner_type'=>22,
            'pay_method_poundage'=>[ //各种支付手续费率 (千分之几) 对应订单表的pay_method字段
                'H5'=>8,//H5支付
                'jsapi'=>6, //jsapi支付(微信公众号支付)
                'stage'=>8, //反扫
                'bank'=>6, //银行卡支付
                'app'=>6, //app支付
            ]

        ]

    ],

    'order_type' => [
        1=>'订单支付',
        2=>'购买pos机订单',
        3=>'消费买单',
        4=>'pos机商城购买商品',
        5=>'扫码支付',
        6=>'刷卡购买CHQ',
        7=>'商家充值',
    ],

    'withdraw_type' => [
        1=>'收益钱包',
        4=>'货款钱包',
        7=>'商家充值钱包',
        12=>'红包收益',
    ],

    'pay_status' =>[
        1=>'处理中',
        2=>'支付成功',
        3=>'支付失败'
    ],

    'withdraw_method'=>[
        'gaohuitong_2'=>[
            'name'=>'高汇通',
        ],
        'huanxun_2'=>[
            'name'=>'环讯'
        ],
        'tonglian_2'=>[
            'name'=>'通联'
        ],
        'tianqi_2'=>[
            'name'=>'天亓'
        ]
    ]


];
