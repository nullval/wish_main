<?php

return [
    //商家信用积分配置参数
    'credit' => [
        [
            'speech'=>0.6,
            'min_radio'=>80,
            'max_radio'=>10000
        ],
        [
            'speech'=>0.54,
            'min_radio'=>60,
            'max_radio'=>80
        ],
        [
            'speech'=>0.48,
            'min_radio'=>40,
            'max_radio'=>60
        ],
        [
            'speech'=>0.36,
            'min_radio'=>20,
            'max_radio'=>40
        ],
        [
            'speech'=>0.3,
            'min_radio'=>0,
            'max_radio'=>20
        ]
    ],

    //根据商家让利金额返积分配置
    'seller_b_config'=>[
        0=>[
            'min_amount'=>0, //让利最低金额
            'max_amount'=>10000, //让利最高金额
            'min_point_radio'=>20, //返积分最低比例
            'max_point_radio'=>20, //返积分最高比例
        ],
        1=>[
            'min_amount'=>10000,
            'max_amount'=>50000,
            'min_point_radio'=>20,
            'max_point_radio'=>22,
        ],
        2=>[
            'min_amount'=>50000,
            'max_amount'=>100000,
            'min_point_radio'=>22,
            'max_point_radio'=>24,
        ],
        3=>[
            'min_amount'=>100000,
            'max_amount'=>500000,
            'min_point_radio'=>24,
            'max_point_radio'=>26,
        ],
        4=>[
            'min_amount'=>500000,
            'max_amount'=>1000000,
            'min_point_radio'=>26,
            'max_point_radio'=>28,
        ],
        5=>[
            'min_amount'=>1000000,
            'max_amount'=>1000000000000000,
            'min_point_radio'=>28,
            'max_point_radio'=>30,
        ]
    ],

    //根据消费者让利金额返积分配置
    'buyer_b_config'=>[
        0=>[
            'min_amount'=>0, //让利最低金额
            'max_amount'=>100, //让利最高金额
            'min_point_radio'=>5, //返积分最低比例
            'max_point_radio'=>10, //返积分最高比例
        ],
        1=>[
            'min_amount'=>100,
            'max_amount'=>500,
            'min_point_radio'=>10,
            'max_point_radio'=>12,
        ],
        2=>[
            'min_amount'=>500,
            'max_amount'=>1000,
            'min_point_radio'=>12,
            'max_point_radio'=>14,
        ],
        3=>[
            'min_amount'=>1000,
            'max_amount'=>2000,
            'min_point_radio'=>14,
            'max_point_radio'=>16,
        ],
        4=>[
            'min_amount'=>2000,
            'max_amount'=>5000,
            'min_point_radio'=>16,
            'max_point_radio'=>18,
        ],
        5=>[
            'min_amount'=>5000,
            'max_amount'=>1000000000000000,
            'min_point_radio'=>18,
            'max_point_radio'=>20,
        ],
//        6=>[
//            'min_amount'=>10000,
//            'max_amount'=>50000,
//            'min_point_radio'=>20,
//            'max_point_radio'=>22,
//        ],
//        7=>[
//            'min_amount'=>50000,
//            'max_amount'=>100000,
//            'min_point_radio'=>22,
//            'max_point_radio'=>24,
//        ],
//        8=>[
//            'min_amount'=>100000,
//            'max_amount'=>200000,
//            'min_point_radio'=>24,
//            'max_point_radio'=>26,
//        ],
//        9=>[
//            'min_amount'=>200000,
//            'max_amount'=>300000,
//            'min_point_radio'=>26,
//            'max_point_radio'=>28,
//        ],
//        10=>[
//            'min_amount'=>300000,
//            'max_amount'=>1000000000000000,
//            'min_point_radio'=>28,
//            'max_point_radio'=>30,
//        ]
    ]

];
