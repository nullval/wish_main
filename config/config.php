<?php

return [
    //配置文件
    //积分商城地址
    'tdzd_shop_url' => env('APP_ENV')=='online'?'http://chqt-wap.worldours.com/':'http://cs-wap.worldours.com/',
    //积分商城接口地址
    'tdzd_sapi_url' => env('APP_ENV')=='online'?'http://chqt-sapi.worldours.com/':'http://cs-sapi.worldours.com/',
    //未特商城接口地址
    'tdzd_api_url' => env('APP_ENV')=='online'?'http://chqt-api.worldours.com/':'http://cs-api.worldours.com/',
    //未特商城机主请求地址
    'tdzd_channel_url' => env('APP_ENV')=='online'?'http://chqt-channel.worldours.com/':'http://cs-channel.worldours.com/',
    //td请求地址
    'tdzd_td_url' => env('APP_ENV')=='online'?'http://chqt-exchange.worldours.com':'http://120.79.19.169/test/',
];
