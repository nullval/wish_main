<?php

return [
    //机主购买pos机配置
    'level' => [
        0=>[
            'name'=>'机主', //名称
            'share_amount'=>900, //分享奖励
            'second_amount'=>300, //间推奖励
            'benfit'=>0,
            'buy_count'=>1 //成为该身份购买pos机数量
        ],
        1=>[
            'name'=>'经理',
            'share_amount'=>1000,
            'second_amount'=>350,
            'benfit'=>0,
//            'buy_count'=>5
            'buy_count'=>2
        ],
        2=>[
            'name'=>'总监',
            'share_amount'=>1100,
            'second_amount'=>400,
            'benfit'=>0,
//            'buy_count'=>10
            'buy_count'=>3
        ],
        3=>[
            'name'=>'区代理',
            'share_amount'=>1200,
            'second_amount'=>500,
            'benfit'=>200,
//            'buy_count'=>50
            'buy_count'=>4
        ],
        4=>[
            'name'=>'市代理',
            'share_amount'=>1300,
            'second_amount'=>600,
            'benfit'=>300,
//            'buy_count'=>100
            'buy_count'=>5
        ],
        5=>[
            'name'=>'省代理',
            'share_amount'=>1400,
            'second_amount'=>700,
            'benfit'=>400,
//            'buy_count'=>500
            'buy_count'=>6
        ]
    ],

    'operate_amount'=>[
        1=>[
            'name'=>'一级运营中心',
            'radio'=>2
        ],
        2=>[
            'name'=>'二级运营中心',
            'radio'=>3
        ],
        3=>[
            'name'=>'三级运营中心',
            'radio'=>4
        ],
    ],
];
