<?php
use App\Http\Middleware\SignVerify;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/', function () {
//    return view('welcome');
//});

Route :: get('logs','\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


//高汇通反扫支付成功通知的地址
Route::group(['domain' =>  env('GHT_NOTIFY_URL'),'namespace' => 'Api'], function () {
    // pos机接口对接
    Route::group(['prefix' => 'posPay'],function(){
        Route::any('ghtNotify','PosPayController@ghtNotify');
    });

});

Route::any('/wechat','WechatController@serve');

//总后台
require(__DIR__ . '/route/admin.php');

//线下商城接口接口
require(__DIR__ . '/route/admin_api.php');


//-------------------------------------接口路由-------------------------------------
$group['namespace'] = 'Admin';
$group['middleware']=SignVerify::class;
//线下商城测试
Route::group($group, function () {
    Route::group(['prefix' => 'offline'],function(){
        Route::any('dividend','OfflineController@dividend');
        Route::any('voucher_dividend','OfflineController@voucher_dividend');
    });
});

//商家后台
//require(__DIR__ . '/route/seller.php');

//商家后台API
//require(__DIR__ . '/route/seller_api.php');

//api接口
//require(__DIR__ . '/route/api.php');

//代理后台
//require(__DIR__ . '/route/agent.php');

//刷卡买td用户后台/系统后台（珍藏版后台）
//require(__DIR__ . '/route/td.php');



//定时任务
Route::group(['prefix' => 'crontab'],function(){
//    Route::any('count_achievement','CrontabController@count_achievement');
//    Route::any('clear_user','CrontabController@clear_user');
//    Route::any('test','CrontabController@test');
});
