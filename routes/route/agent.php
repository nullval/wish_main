<?php
use App\Http\Middleware\AgentLogin;//注意引用该类文件
use App\Http\Middleware\PushQrLogin;//注意引用该类文件

//不用做登录验证的
Route::group(['domain' =>  env('APP_AGENT_DOMAIN') . env('APP_DOMAIN'), 'namespace' => 'Agent'], function () {
    Route::any('login','LoginController@login');
    Route::any('login_submit','LoginController@login_submit');
//    Route::any('register','LoginController@register');
//    Route::any('register_submit','LoginController@register_submit');
    Route::any('logout','LoginController@logout');
    Route::any('send_sms','LoginController@send_sms');
//    Route::get('/captcha/{tmp}', 'LoginController@captcha');
    Route::any('send_sms','LoginController@send_sms');
    Route::any('test','LoginController@test');
    Route::any('pushQr_login','LoginController@pushQr_login');
    Route::any('pushQr_login_submit','LoginController@pushQr_login_submit');
    //财务管理 (定时任务查询代付结果)
//    Route::group(['prefix' => 'finance'],function(){
//        Route::any('checkPay','FinanceController@checkPay');
//    });

    Route::group(['prefix' => 'orderQrcode'],function(){
        Route::any('index','OrderQrcodeController@index');
        Route::any('active_qrcode','OrderQrcodeController@active_qrcode');
        Route::any('active_qrcode_submit','OrderQrcodeController@active_qrcode_submit');
        Route::any('force','OrderQrcodeController@force');
        Route::any('check_get_chq','OrderQrcodeController@check_get_chq');
    });
});
//需要做登录验证的(使用中间键)
Route::group(['domain' =>  env('APP_AGENT_DOMAIN') . env('APP_DOMAIN'), 'namespace' => 'Agent','middleware'=>[AgentLogin::class]], function () {
    Route::get('/', function () {
        return redirect('/info/index');
    });
    
    //业务员信息模块
    Route::group(['prefix' => 'info'],function(){
        Route::any('index','MemberController@index');
        Route::any('change_pwd','MemberController@change_pwd');
        Route::any('change_submit','MemberController@change_submit');
        Route::any('inviter_list','MemberController@inviter_list');
        Route::any('nine_floor','MemberController@nine_floor');
        Route::any('send_sms','MemberController@send_sms');
        Route::any('bind_submit','MemberController@bind_submit');

    });

    //pos机管理
    Route::group(['prefix' => 'pos'],function(){
        Route::any('pos_list','PosController@pos_list');
        Route::any('pos_edit','PosController@pos_edit');
        Route::any('pos_edit_submit','PosController@pos_edit_submit');
    });

    //订单管理
    Route::group(['prefix' => 'order'],function(){
        Route::any('order_list','OrderController@order_list');
        Route::any('order_detail','OrderController@order_detail');
        Route::any('bind_pos','OrderController@bind_pos');
    });

    //财务管理
    Route::group(['prefix' => 'finance'],function(){
        Route::any('index','FinanceController@index');
        Route::any('withdraw_list','FinanceController@withdraw_list');
        Route::any('withdraw','FinanceController@withdraw');
        Route::any('getbankcode','FinanceController@getbankcode');
        Route::any('subwithdraw','FinanceController@subwithdraw');
        Route::any('get_bankNo','FinanceController@get_bankNo');
    
    
    
    });

    //POS机管理
    Route::group(['prefix' => 'pos'],function(){
        Route::any('pos_list','PosController@pos_list');

    });

    //发票管理
    Route::group(['prefix' => 'invoice'],function(){
        Route::any('invoice_info','InvoiceController@invoice_info');
        Route::any('invoice_info_submit','InvoiceController@invoice_info_submit');
        Route::any('invoice_list','InvoiceController@invoice_list');
        Route::any('invoice_detail','InvoiceController@invoice_detail');
        Route::any('address_list','InvoiceController@address_list');
        Route::any('address_add','InvoiceController@address_add');
        Route::any('address_add_submit','InvoiceController@address_add_submit');
        Route::any('invoice_add','InvoiceController@invoice_add');
        Route::any('invoice_add_submit','InvoiceController@invoice_add_submit');
        Route::any('address_del','InvoiceController@address_del');
        Route::any('invoice_get','InvoiceController@invoice_get');

    });

    //运营中心
    Route::group(['prefix' => 'operate'],function(){
        Route::any('index','OperateController@index');
        Route::any('child_operate_list','OperateController@child_operate_list');
        Route::any('income','OperateController@income');
        Route::any('withdraw_list','OperateController@withdraw_list');
        Route::any('withdraw','OperateController@withdraw');
        Route::any('subwithdraw','OperateController@subwithdraw');

    });


    //商家管理
    Route::group(['prefix' => 'seller'],function(){
        Route::any('seller_list','SellerController@seller_list');
        Route::any('get_bankNo','SellerController@get_bankNo');
        Route::any('seller_add','SellerController@seller_add');
        Route::any('seller_add_submit','SellerController@seller_add_submit');
        Route::any('seller_edit','SellerController@seller_edit');
        Route::any('seller_edit_submit','SellerController@seller_edit_submit');
        Route::any('saveData','SellerController@saveData');
        Route::any('delData','SellerController@delData');


    });

});

//地推人员权限列表
Route::group(['domain' =>  env('APP_AGENT_DOMAIN') . env('APP_DOMAIN'), 'namespace' => 'Agent','middleware'=>[PushQrLogin::class]], function () {
    //地推人员基本信息
    Route::group(['prefix' => 'pushQrUser'],function(){
        Route::any('send_sms','PushQrUserController@send_sms');
        Route::any('bind_submit','PushQrUserController@bind_submit');
        Route::any('index','PushQrUserController@index');
        Route::any('edit_passrod','PushQrUserController@edit_passrod');
        Route::any('edit_password_submit','PushQrUserController@edit_password_submit');
        Route::any('get_bankNo','PushQrUserController@get_bankNo');
    });

    //地推二维码
    Route::group(['prefix' => 'pushQr'],function(){
        Route::any('pushQr_list','PushQrController@pushQr_list');
        Route::any('bind_pushqr','PushQrController@bind_pushqr');
        Route::any('bind_pushqr_submit','PushQrController@bind_pushqr_submit');
        Route::any('pushQr_order_list','PushQrController@pushQr_order_list');
        Route::any('add_order','PushQrController@add_order');
        Route::any('add_order_submit','PushQrController@add_order_submit');
        Route::any('pushQr_order_detail','PushQrController@pushQr_order_detail');
    });


    //商家管理
    Route::group(['prefix' => 'pushQrSeller'],function(){
        Route::any('seller_list','PushQrSellerController@seller_list');
    });

    //财务管理
    Route::group(['prefix' => 'pushQrFinance'],function(){
        Route::any('index','PushQrFinanceController@index');
        Route::any('withdraw_list','PushQrFinanceController@withdraw_list');
        Route::any('withdraw','PushQrFinanceController@withdraw');
        Route::any('getbankcode','PushQrFinanceController@getbankcode');
        Route::any('subwithdraw','PushQrFinanceController@subwithdraw');
        Route::any('get_bankNo','PushQrFinanceController@get_bankNo');
    });

    //团队管理
    Route::group(['prefix' => 'team'],function(){
        Route::any('team_list','TeamController@team_list');
        Route::any('team_add','TeamController@team_add');
        Route::any('team_add_submit','TeamController@team_add_submit');
        Route::any('pushQr_list','TeamController@pushQr_list');
        Route::any('seller_list','TeamController@seller_list');
    });
});