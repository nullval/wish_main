<?php
use App\Http\Middleware\SignVerify;
use App\Http\Middleware\SignVerifyNew;
$group=['domain' =>  env('APP_API_DOMAIN') . env('APP_DOMAIN'),'namespace' => 'Api'];
Route::group($group, function () {

    Route::get('/', function () {
      return redirect('/index/index');
    });
    // 接口沙盒调试
    Route::group(['prefix' => 'sandbox'],function(){
        Route::any('/','SandboxController@index');
        Route::any('test','SandboxController@test');
        Route::any('index','SandboxController@index');
        Route::post('submit','SandboxController@submit');
    });

    // 接口沙盒调试(新)
    Route::group(['prefix' => 'sandboxnew'],function(){
        Route::any('/','SandboxNewController@index');
        Route::any('test','SandboxNewController@test');
        Route::any('index','SandboxNewController@index');
        Route::post('submit','SandboxNewController@submit');
    });
    // pos机接口对接
    Route::group(['prefix' => 'pos'],function(){
        Route::any('test','PosController@test');
        Route::any('query','PosController@query');
        Route::any('notify','PosController@notify');
        Route::any('ghtNotify','PosController@ghtNotify');
        Route::any('testJpush','PosController@testJpush');
        Route::any('hxNotify','PosController@hxNotify');
    });

    Route::group(['prefix' => 'posPay'],function(){
        Route::any('test','PosPayController@test');
        Route::any('test_sign','PosPayController@test_sign');
        Route::any('bankNotify','PosPayController@bankNotify');
        Route::any('is_create_qrcode','PosPayController@is_create_qrcode');
    });

    //定时任务
    Route::group(['prefix' => 'notify'],function(){
        Route::any('evaluateSellerCredit','NotifyController@evaluateSellerCredit');
        Route::any('checkPay','NotifyController@checkPay');
        Route::any('backCash','NotifyController@backCash');
        Route::any('createStatement','NotifyController@createStatement');
        Route::any('withdraw','NotifyController@withdraw');
        Route::any('checkGhtEnterStatus','NotifyController@checkGhtEnterStatus');
        Route::any('checkGhtPay','NotifyController@checkGhtPay');
        Route::any('test_withdraw','NotifyController@test_withdraw');
        Route::any('update_agent_invite_floor','NotifyController@update_agent_invite_floor');
        Route::any('test','NotifyController@test');
        Route::any('give_operate_old_transfer','NotifyController@give_operate_old_transfer');
        Route::any('back_agent_freeze_point','NotifyController@back_agent_freeze_point');
        Route::any('automaticHandelSellerPoint','NotifyController@automaticHandelSellerPoint');
        Route::any('createBackTdStatement','NotifyController@createBackTdStatement');
        Route::any('giveBackTd','NotifyController@giveBackTd');
        Route::any('createBackTdLog','NotifyController@createBackTdLog');
    });
    
	// 一户一码下单
	Route::group(['prefix'=>'qrpay'],function(){
		Route::any('order_notify','ShopController@order_notify_qr');
		Route::any('order_trans','ShopController@order_trans');
		Route::any('seller_qr','ShopController@seller_qr');
		Route::any('seller_qr_back','ShopController@seller_qr_back');
	});
    Route::group(['prefix' => 'test'],function(){
        Route::any('test','TestController@test');
        Route::any('test_redis','TestController@test_redis');
        Route::any('test1','TestController@test1');
    });

    Route::group(['prefix' => 'point'],function() {
        Route::any('automaticPoint','PointController@automaticPoint');
        Route::any('getMemberType','PointController@getMemberType');
    });

    Route::group(['prefix' => 'pushQr'],function() {
        Route::any('create_seller','PushQrController@create_seller');
        Route::any('add_seller_submit','PushQrController@add_seller_submit');
        Route::any('get_bankNo','PushQrController@get_bankNo');
    });
    // 版本检测
    Route::group(['prefix'=>'check'],function(){
        Route::any('upgrade','CheckController@upgrade');
        Route::any('checkgift','CheckController@checkgift');
        Route::any('opengift','CheckController@opengift');
        Route::any('gifthis','CheckController@gifthis');
        Route::any('getmoney','CheckController@getmoney');
        Route::any('rmcache','CheckController@rmcache');
        Route::any('recovergift','CheckController@recovergift');
    });
});

if(config('app.env')=='online'){
    //线上要算签
    $group['middleware']=SignVerify::class;
}
Route::group($group, function () {
//Route::group(['domain' =>  env('APP_API_DOMAIN') . env('APP_DOMAIN'),'namespace' => 'Api'], function () {
    Route::any('/', function () {
        $_cmd = request('_cmd');
        $request = request()->except(['s']);
        clear_null($request);
        if((!empty($request))&&(!empty($_cmd))){
            $controller = strstr($_cmd, '_', true);
            $action = substr(strstr($_cmd, '_'), 1);
            if((!empty($controller))&&(!empty($action))){
                return curl_post('http://'.request()->server('HTTP_HOST').'/'.$controller.'/'.$action,$request);
            }
            return json_error('请求错误!');
        }
        return json_error('请求错误!');
    });

    // pos机接口对接 （通联）
    Route::group(['prefix' => 'pos'],function(){
        Route::any('login','PosController@login');
        Route::any('createOrder','PosController@createOrder');
        Route::any('createPosOrder','PosController@createPosOrder');
        Route::any('payNotify','PosController@payNotify');
        Route::any('getAgentInfo','PosController@getAgentInfo');
        Route::any('isExit','PosController@isExit');
    });

    // pos机接口对接 (高汇通)
    Route::group(['prefix' => 'posPay'],function(){
        Route::any('createOrder','PosPayController@createOrder');
        Route::any('create_recharge_Order','PosPayController@create_recharge_Order');
        Route::any('createPosOrder','PosPayController@createPosOrder');
        Route::any('getAgentInfo','PosPayController@getAgentInfo');
        Route::any('isExit','PosPayController@isExit');
        Route::any('pay','PosPayController@pay');
        Route::any('payTest','PosPayController@payTest');
        Route::any('checkOrderStatus','PosPayController@checkOrderStatus');
        Route::any('getOrderList','PosPayController@getOrderList');
        Route::any('bindInvterId','PosPayController@bindInvterId');
        Route::any('isSellerExit','PosPayController@isSellerExit');
        Route::any('bindSeller','PosPayController@bindSeller');
        Route::any('isOperateExit','PosPayController@isOperateExit');
        Route::any('bindOperate','PosPayController@bindOperate');
        Route::any('getInfo','PosPayController@getInfo');
        Route::any('isRoleExit','PosPayController@isRoleExit');
        Route::any('bankPay','PosPayController@bankPay');
        Route::any('sweepPay','PosPayController@sweepPay');
    });

    // 积分商城对接
    Route::group(['prefix' => 'point'],function(){
        Route::any('getUserPoint','PointController@getUserPoint');
        Route::any('changeUserPoint','PointController@changeUserPoint');
        Route::any('freezePoint','PointController@freezePoint');
        Route::any('unFreezePoint','PointController@unFreezePoint');
        Route::any('exitsPayPwd','PointController@exitsPayPwd');
        Route::any('checkTicket','PointController@checkTicket');
        Route::any('checkPayPwd','PointController@checkPayPwd');
        Route::any('buyGoodsUserPoint','PointController@buyGoodsUserPoint');
        Route::any('synchroUser','PointController@synchroUser');
        Route::any('checko','PointController@checko');
        Route::any('doTransfer','PointController@doTransfer');
        Route::any('send_msg','PointController@send_msg');
        Route::any('platformAmount','PointController@platformAmount');

        Route::any('addBank','PointController@addBank');
        Route::any('editBank','PointController@editBank');
        Route::any('delBank','PointController@delBank');
        Route::any('getBankList','PointController@getBankList');
        Route::any('setBankDefault','PointController@setBankDefault');
        Route::any('withdraw','PointController@withdraw');
        Route::any('searchIbnkNo','PointController@searchIbnkNo');
        Route::any('getIbnkList','PointController@getIbnkList');
        Route::any('getDefaultBank','PointController@getDefaultBank');
        Route::any('withdrawList','PointController@withdrawList');
        Route::any('transferList','PointController@transferList');
        Route::any('getBankDetail','PointController@getBankDetail');
        Route::any('getUserIdByMobile','PointController@getUserIdByMobile');
        Route::any('notifySendTd', 'PointController@notifySendTd');
        Route::any('backSellerPoint', 'PointController@backSellerPoint');
        Route::any('getUserAllPoint', 'PointController@getUserAllPoint');
        Route::any('getSellerBalance','PointController@getSellerBalance');
    });

    // 商城地址重定向
    Route::group(['prefix'=>'product'],function(){
    	Route::any('{action}','ShopController@index');
	});
    
    // 一户一码下单
	Route::group(['prefix'=>'qrpay'],function(){
		Route::any('seller','ShopController@seller');
		Route::any('seller_bind_sms','ShopController@seller_bind_sms');
		Route::any('order_create','ShopController@order_create_qr');
	});


    // 商家联盟
    Route::group(['prefix'=>'Aliance'],function(){
        Route::any('get_shop_list','AllianceController@get_shop_list');
        Route::any('search','AllianceController@search');
        Route::any('seller_detail','AllianceController@seller_detail');
        Route::any('Location','AllianceController@Location');
        Route::any('get_district_list','AllianceController@get_district_list');
        Route::any('edit','AllianceController@edit');
    });

    // 刷pos机购买td
    Route::group(['prefix'=>'Td'],function(){
        Route::any('createTdOrder','TdController@createTdOrder');
    });
	
	
	Route::group(['prefix'=>'mofang'],function(){
		Route::any('config','MofangController@config');
		Route::any('seller_unified','MofangController@seller_unified');
		Route::any('seller_recharge','MofangController@seller_recharge');
		Route::any('seller_info','MofangController@seller_info');
		Route::any('seller_info2','MofangController@seller_info2');
	});

    Route::group(['prefix'=>'yinsheng'],function(){
        Route::any('config','YinshengController@config');
        Route::any('seller_unified','YinshengController@seller_unified');
        Route::any('seller_recharge','YinshengController@seller_recharge');
        Route::any('seller_info','YinshengController@seller_info');
        Route::any('seller_info2','YinshengController@seller_info2');
    });
});


// 验证ticket中间件重定向
Route::any('sign_verify_failed', function () {
    return json_error('验证签名失败');
})->name('sign_verify_failed');