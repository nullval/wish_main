<?php
use App\Http\Middleware\AdminLogin;//注意引用该类文件
/**
 * PHC管理后台
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/3
 * Time: 17:36
 */
Route::group(['domain' =>  env('APP_ADMIN_DOMAIN') . env('APP_DOMAIN'), 'namespace' => 'Admin'], function () {
    Route::any('login','LoginController@login');
    Route::any('login_submit','LoginController@login_submit');
    Route::any('logout','LoginController@logout');
    Route::any('upload_img','CommonController@upload_img');
    Route::any('send_sms','CommonController@send_sms');
    //扫描二维码
    Route::any('scan_code', 'OrderController@scan_code');

    //环讯商家入驻
    Route::group(['prefix' => 'huanxun'],function(){
        Route::any('open','HuanxunController@open');
        Route::any('update','HuanxunController@update');
        Route::any('bankCard','HuanxunController@bankCard');
        Route::any('toCertificate','HuanxunController@toCertificate');
    });

    //不经过后台直接访问商家
    Route::group(['prefix' => 'seller'],function(){
        Route::any('add','ExternalSellerController@add');
        Route::any('add_submit','ExternalSellerController@add_submit');
    });

//    Route::any('test','LoginController@test');
});
Route::group(['domain' =>  env('APP_ADMIN_DOMAIN') . env('APP_DOMAIN'),'namespace' => 'Admin','middleware'=>[AdminLogin::class]], function () {
    Route::get('/', function () {
        return redirect('/config/admin_list');
    });

    //线下个人管理
    Route::group(['prefix' => 'wish_person'],function($router){
        Route::any('uppwd','WishPersonController@uppwd');
        Route::any('info','WishPersonController@info');
        Route::any('submit_uppwd','WishPersonController@submit_uppwd');
        Route::any('seller_info_submit','WishPersonController@seller_info_submit');
        Route::any('generalize_info_submit','WishPersonController@generalize_info_submit');
        Route::any('agent_info_submit','WishPersonController@agent_info_submit');
        Route::any('company_info_submit','WishPersonController@company_info_submit');
    });

    //财务管理
    Route::group(['prefix' => 'wish_finance'],function($router){
        Route::any('index','WishFinanceController@index');
        Route::any('voucher_order','WishFinanceController@voucher_order');
        Route::any('transfer_list','WishFinanceController@transfer_list');
    });


    //商家管理
    Route::group(['prefix' => 'wish_seller'],function($router){
        Route::any('seller_list','WishSellerController@seller_list');
        Route::any('change_status','WishSellerController@change_status');
        Route::any('seller_add','WishSellerController@seller_add');
        Route::any('seller_add_submit','WishSellerController@seller_add_submit');
        Route::any('seller_edit','WishSellerController@seller_edit');
        Route::any('seller_edit_submit','WishSellerController@seller_edit_submit');

        Route::any('seller_detail','WishSellerController@seller_detail');
        Route::any('seller_consume','WishSellerController@seller_consume');
        Route::any('seller_transfer','WishSellerController@seller_transfer');
    });
    //店铺推荐人
    Route::group(['prefix' => 'wish_generalize'],function($router){
        Route::any('generalize_list','WishGeneralizeController@generalize_list');
        Route::any('change_status','WishGeneralizeController@change_status');
        Route::any('generalize_add','WishGeneralizeController@generalize_add');
        Route::any('generalize_add_submit','WishGeneralizeController@generalize_add_submit');
        Route::any('generalize_edit','WishGeneralizeController@generalize_edit');
        Route::any('generalize_edit_submit','WishGeneralizeController@generalize_edit_submit');
        Route::any('seller_list','WishGeneralizeController@seller_list');

        Route::any('seller_detail','WishGeneralizeController@seller_detail');
        Route::any('seller_consume','WishGeneralizeController@seller_consume');
        Route::any('seller_transfer','WishGeneralizeController@seller_transfer');
    });
    //代理人
    Route::group(['prefix' => 'wish_agent'],function($router){
        Route::any('agent_list','WishAgentController@agent_list');
        Route::any('change_status','WishAgentController@change_status');
        Route::any('agent_add','WishAgentController@agent_add');
        Route::any('agent_add_submit','WishAgentController@agent_add_submit');
        Route::any('agent_edit','WishAgentController@agent_edit');
        Route::any('agent_edit_submit','WishAgentController@agent_edit_submit');
        Route::any('generalize_list','WishAgentController@generalize_list');
        Route::any('seller_list','WishAgentController@seller_list');

        Route::any('seller_detail','WishAgentController@seller_detail');
        Route::any('seller_consume','WishAgentController@seller_consume');
        Route::any('seller_transfer','WishAgentController@seller_transfer');
    });
    //分公司
    Route::group(['prefix' => 'wish_company'],function($router){
        Route::any('company_list','WishCompanyController@company_list');
        Route::any('change_status','WishCompanyController@change_status');
        Route::any('company_add','WishCompanyController@company_add');
        Route::any('company_add_submit','WishCompanyController@company_add_submit');
        Route::any('company_edit','WishCompanyController@company_edit');
        Route::any('company_edit_submit','WishCompanyController@company_edit_submit');
    });
    //-------------------------------------------------------------------------------------------

    Route::group(['prefix' => 'seller'],function(){
        Route::any('getbankcode','SellerController@getbankcode');
        Route::any('seller_list','SellerController@seller_list');
        Route::any('seller_apply_list','SellerController@seller_apply_list');
        Route::any('seller_add','SellerController@seller_add');
        Route::any('seller_add_submit','SellerController@seller_add_submit');
        Route::any('change_status','SellerController@change_status');
        Route::any('finance','SellerController@finance');
        Route::any('statement_list','SellerController@statement_list');
        Route::any('statement_details','SellerController@statement_details');
        Route::any('statement_finance_details','SellerController@statement_finance_details');

        Route::any('edit_apply','SellerController@edit_apply');
        Route::any('edit_apply_submit','SellerController@edit_apply_submit');
        Route::any('verity_apply','SellerController@verity_apply');
        Route::any('verity_apply_submit','SellerController@verity_apply_submit');
        Route::any('edit_fee','SellerController@edit_fee');
        Route::any('edit_fee_submit','SellerController@edit_fee_submit');

        Route::any('user_list','SellerController@user_list');
        Route::any('user_finance','SellerController@user_finance');
        //手动增加用户
        Route::any('add_user','SellerController@add_user');

        Route::any('agent_list','SellerController@agent_list');
        Route::any('agent_finance','SellerController@agent_finance');
        Route::any('agent_add','SellerController@agent_add');
        Route::any('agent_add_submit','SellerController@agent_add_submit');
        Route::any('get_bankNo','SellerController@get_bankNo');

        Route::any('ght_seller_add','SellerController@ght_seller_add');
        Route::any('ght_seller_add_submit_step1','SellerController@ght_seller_add_submit_step1');
        Route::any('ght_seller_add_submit_step2','SellerController@ght_seller_add_submit_step2');
        Route::any('ght_seller_add_submit_step3','SellerController@ght_seller_add_submit_step3');
        Route::any('ght_seller_edit','SellerController@ght_seller_edit');
        Route::any('ght_seller_edit_submit_step1','SellerController@ght_seller_edit_submit_step1');
        Route::any('ght_seller_edit_submit_step2','SellerController@ght_seller_edit_submit_step2');
        Route::any('ght_seller_edit_submit_step3','SellerController@ght_seller_edit_submit_step3');
        Route::any('query','SellerController@query');
        Route::any('ght_seller_edit_copy_step1','SellerController@ght_seller_edit_copy_step1');
        Route::any('ght_seller_edit_copy_step2','SellerController@ght_seller_edit_copy_step2');
        Route::any('ght_seller_edit_copy_step3','SellerController@ght_seller_edit_copy_step3');
        Route::any('yixian_submit','SellerController@yixian_submit');


        Route::any('seller_withdraw_bank','SellerController@seller_withdraw_bank');
        Route::any('seller_withdraw_bank_submit','SellerController@seller_withdraw_bank_submit');

        //运营中心管理
        Route::any('operate_list','SellerController@operate_list');
        Route::any('finance','SellerController@finance');
        Route::any('operate_add','SellerController@operate_add');
        Route::any('operate_add_submit','SellerController@operate_add_submit');
        Route::any('pushqr_list','SellerController@pushqr_list');
        Route::any('create_pushqr','SellerController@create_pushqr');
        Route::any('create_pushqr_submit','SellerController@create_pushqr_submit');
        Route::any('download_pushQr','SellerController@download_pushQr');
        Route::any('agent_tree','SellerController@agent_tree');
        Route::any('update_seller_h_ce','SellerController@update_seller_h_ce');
        Route::any('add_terminalId','SellerController@add_terminalId');
        Route::any('clear_openid','SellerController@clear_openid');
        Route::any('edit_agent','SellerController@edit_agent');
        //分公司中心管理
        Route::any('filiale_operate_list','SellerController@filiale_operate_list');
        Route::any('filiale_finance','SellerController@filiale_finance');
        Route::any('filiale_operate_add','SellerController@filiale_operate_add');
        Route::any('filiale_operate_add_submit','SellerController@filiale_operate_add_submit');
        Route::any('filiale_pushqr_list','SellerController@filiale_pushqr_list');
        Route::any('filiale_create_pushqr','SellerController@filiale_create_pushqr');
        Route::any('filiale_create_pushqr_submit','SellerController@filiale_create_pushqr_submit');
        Route::any('filiale_download_pushQr','SellerController@filiale_download_pushQr');
        Route::any('filiale_agent_tree','SellerController@filiale_agent_tree');
        Route::any('filiale_update_seller_h_ce','SellerController@filiale_update_seller_h_ce');
        Route::any('filiale_add_terminalId','SellerController@filiale_add_terminalId');
        Route::any('filiale_clear_openid','SellerController@filiale_clear_openid');
        Route::any('filiale_edit_agent','SellerController@filiale_edit_agent');

    });

    //订单管理
    Route::group(['prefix' => 'order'],function(){
        Route::any('pos_order_list','OrderController@pos_order_list');
        Route::any('pos_order_detail','OrderController@pos_order_detail');
        Route::any('order_list','OrderController@order_list');
        Route::any('pushQr_order_list','OrderController@pushQr_order_list');
        Route::any('pushQr_order_detail','OrderController@pushQr_order_detail');
        Route::any('pushQr_order_detail_submit','OrderController@pushQr_order_detail_submit');
        Route::any('create_order','OrderController@create_order');
        Route::any('create_pos_order','OrderController@create_pos_order');
        Route::any('create_pos_order_submit','OrderController@create_pos_order_submit');
        #创建二维码订单
        Route::get('add_pushQr_order','OrderController@add_pushQr_order');
        #创建二维码提交
        Route::any('add_pushQr_order_submit','OrderController@add_pushQr_order_submit');
        #二维码列表
        Route::any('pushQr_list','OrderController@pushQr_list');
        #绑定地推二维码
        Route::any('bind_pushqr','OrderController@bind_pushqr');
        #地推二维码下载
        Route::any('pushQr_download','OrderController@pushQr_download');
        #生成二维码
        Route::any('make_qrcode','OrderController@make_qrcode');
        #二维码绑定商家
        Route::any('bind_seller','OrderController@bind_seller');
        #提交二维码绑定商家
        Route::any('bind_seller_submit','OrderController@bind_seller_submit');
    });

    //pos机管理
    Route::group(['prefix' => 'pos'],function(){
        Route::any('pos_list','PosController@pos_list');
        Route::any('pos_edit','PosController@pos_edit');
        Route::any('pos_add','PosController@pos_add');
        Route::any('pos_add_submit','PosController@pos_add_submit');
        Route::any('pos_edit_submit','PosController@pos_edit_submit');
        Route::any('pos_order_list','PosController@pos_order_list');
        Route::any('ajax_get_pos_detail','PosController@ajax_get_pos_detail');
        Route::any('relieve','PosController@relieve');
        Route::any('get_operate','PosController@get_operate');
        Route::any('pos_import','PosController@pos_import');
        Route::any('pos_upload','PosController@pos_upload');
        Route::any('pos_upload_submit','PosController@pos_upload_submit');
        Route::any('pos_demo_download','PosController@pos_demo_download');
    });

    //财务管理
    Route::group(['prefix' => 'finance'],function(){
        Route::any('index','FinanceController@index');
        Route::any('withdraw_list','FinanceController@withdraw_list');
        Route::any('withdraw_detail','FinanceController@withdraw_detail');
        Route::any('withdraw_submit','FinanceController@withdraw_submit');
        Route::any('Transfer_statistics','FinanceController@Transfer_statistics');
        Route::any('Purse_statistics','FinanceController@Purse_statistics');
        Route::any('Transfer_detail','FinanceController@Transfer_detail');
    });

    //系统配置
    Route::group(['prefix' => 'config'],function(){
        Route::any('set_upgrade','ConfigController@set_upgrade');
        Route::any('set_upgrade_submit','ConfigController@set_upgrade_submit');
        Route::any('admin_list','ConfigController@admin_list');
        Route::any('change_admin_status','ConfigController@change_admin_status');
        Route::any('admin_add','ConfigController@admin_add');
        Route::any('admin_add_submit','ConfigController@admin_add_submit');
        Route::any('role_list','ConfigController@role_list');
        Route::any('role_add','ConfigController@role_add');
        Route::any('role_add_submit','ConfigController@role_add_submit');
        Route::any('withdraw','ConfigController@withdraw');
        Route::any('getbankcode','ConfigController@getbankcode');
        Route::any('subwithdraw','ConfigController@subwithdraw');
        Route::any('send_message','ConfigController@send_message');
        Route::any('check_safe_code','ConfigController@check_safe_code');
        Route::any('print_page','ConfigController@print_page');
        Route::any('print_page_submit','ConfigController@print_page_submit');
        Route::any('create_order','ConfigController@create_order');
        Route::any('create_order_submit','ConfigController@create_order_submit');
        Route::any('handle_order','ConfigController@handle_order');
        Route::any('handle_order_submit','ConfigController@handle_order_submit');
        Route::any('set_withdraw','ConfigController@set_withdraw');
        Route::any('set_withdraw_submit','ConfigController@set_withdraw_submit');
        Route::any('recharge','ConfigController@recharge');
        Route::any('recharge_submit','ConfigController@recharge_submit');
        Route::any('issue_withdraw','ConfigController@issue_withdraw');
        Route::any('issue_withdraw_submit','ConfigController@issue_withdraw_submit');
    });


    //发票管理
    Route::group(['prefix' => 'invoice'],function(){
        Route::any('invoice_list','InvoiceController@invoice_list');
        Route::any('invoice_detail','InvoiceController@invoice_detail');
        Route::any('invoice_edit','InvoiceController@invoice_edit');
        Route::any('invoice_edit_submit','InvoiceController@invoice_edit_submit');
    });

    //测试
    Route::group(['prefix' => 'test'],function(){
        Route::any('test_buy_pos','TestController@test_buy_pos');
        Route::any('test_view','TestController@test_view');
        Route::any('test','TestController@test');
    });


});