<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018/6/20
 * Time: 18:48
 */
use App\Http\Middleware\SignVerify;

$group=['domain' =>  env('APP_SELLER_API_DOMAIN') . env('APP_DOMAIN'),'namespace' => 'SellerAdmin'];
//不用做登录验证的
Route::group($group, function () {
    Route::any('wx_login','LoginController@login');
    Route::any('send_sms','LoginController@send_sms');
    Route::any('bind_phone','LoginController@bind_phone');
    Route::any('logout','LoginController@logout');
});

if(config('app.env')=='online'){
    //线上要算签
    $group['middleware']=SignVerify::class;
}

//需要做登录验证的(使用中间键)
Route::group($group, function () {

    Route::any('/', function () {
        $_cmd = request('_cmd');
        $request = request()->except(['s']);
        clear_null($request);
        if(!empty($request)){
            $controller = strstr($_cmd, '_', true);
            $action = substr(strstr($_cmd, '_'), 1);
            return curl_post('http://'.request()->server('HTTP_HOST').'/'.$controller.'/'.$action,$request);
        }else{
            return json_error('你好!');
        }
    });


    //商家入驻模块
    Route::group(['prefix' => 'settle'],function(){
        Route::any('apply','MemberController@apply');
        Route::any('code','MemberController@code');
        Route::any('apply_submit','MemberController@apply_submit');
        Route::any('test','MemberController@test');
        Route::any('seller_info','MemberController@seller_info');
        Route::any('seller_info_submit','MemberController@seller_info_submit');
        Route::any('banner_list','MemberController@banner_list');
        Route::any('banner_add','MemberController@banner_add');
        Route::any('banner_add_submit','MemberController@banner_add_submit');
    });

    //订单管理
    Route::group(['prefix' => 'order'],function(){
        Route::any('order_sum','OrderController@order_sum');
        Route::any('order_list','OrderController@order_list');
        Route::any('order_detail','OrderController@order_detail');
    });

    //财务管理
    Route::group(['prefix' => 'finance'],function(){
        Route::any('index','FinanceController@index');
        Route::any('getbankcode','FinanceController@getbankcode');
        Route::any('withdraw_list','FinanceController@withdraw_list');
        Route::any('withdraw','FinanceController@withdraw');
        Route::any('subwithdraw','FinanceController@subwithdraw');
        Route::any('withdraw_goods_list','FinanceController@withdraw_goods_list');
        Route::any('withdraw_goods','FinanceController@withdraw_goods');
        Route::any('sub_goods_withdraw','FinanceController@sub_goods_withdraw');
        Route::any('huanxun_subwithdraw','FinanceController@huanxun_subwithdraw');
        Route::any('exchange','FinanceController@exchange');
        Route::any('exchange_submit','FinanceController@exchange_submit');
        Route::any('increment_submit','FinanceController@increment_submit');
        Route::any('statement_list','FinanceController@statement_list');
        Route::any('statement_details','FinanceController@statement_details');
        Route::any('statement_finance_details','FinanceController@statement_finance_details');
        Route::any('new_statement_list','FinanceController@new_statement_list');
        Route::any('new_statement_details','FinanceController@new_statement_details');
        Route::any('change_automatic','FinanceController@change_automatic');
        Route::any('exchange_list','FinanceController@exchange_list');
    });

    //POS机管理
    Route::group(['prefix' => 'pos'],function(){
        Route::any('pos_list','PosController@pos_list');

    });

    //店员管理
    Route::group(['prefix' => 'config'],function(){
        Route::any('role_list','ConfigController@role_list');
        Route::any('permission_list','ConfigController@permission_list');
        Route::any('role_add_submit','ConfigController@role_add_submit');
        Route::any('role_del','ConfigController@role_del');
    });


});