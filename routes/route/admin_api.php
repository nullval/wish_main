<?php
//不需要签名的接口
Route::group(['middleware'=>\App\Http\Middleware\EnableCrossRequestMiddleware::class],function(){
    Route::any('/', function () {
        $_cmd = request('_cmd');
        $request = request()->except(['s']);
        clear_null($request);
        if((!empty($request))&&(!empty($_cmd))){
            $controller = strstr($_cmd, '_', true);
            $action = substr(strstr($_cmd, '_'), 1);
            if((!empty($controller))&&(!empty($action))){
                //方案一
                 $result = curl_post('http://'.request()->server('HTTP_HOST').'/api/'.$controller.'/'.$action,$request);
                return response()->json(json_decode($result,true));

//                return $result;
//                return $result;
                //方案二
//                $executeController = app()->make('\App\Http\Controllers\AdminApi\\'.ucfirst($controller).'Controller'); // 调用控制器
//                $result =  $executeController->callAction($action,['a']); //调用控制其方法并传参
//                return response()->json($result);
                //方案三
//                if(config('app.env')=='dev'){
//                    return redirect('/api/'.$controller.'/'.$action);
//                }else{
//                    $result = curl_post('http://'.request()->server('HTTP_HOST').'/api/'.$controller.'/'.$action,$request);
//                    return response()->json(json_decode($result,true));
//                }
            }
            return json_error('请求错误!');}
        return json_error('请求错误!');
    });
});

//需要签名的接口
Route::group(['domain' =>  env('APP_ADMIN_DOMAIN') . env('APP_DOMAIN'),'prefix'=>'api', 'namespace' => 'AdminApi','middleware'=>\App\Http\Middleware\SignVerify::class], function () {

    Route::group(['prefix' => 'seller'],function(){
        //商家入驻接口
        Route::any('add', 'SellerController@add');
        Route::any('list', 'SellerController@list');
        Route::any('manage_list', 'SellerController@manage_list');
        Route::any('get_generalize_mobile', 'SellerController@get_generalize_mobile');
        Route::any('check_user_enter', 'SellerController@check_user_enter');
    });

    //财务管理
    Route::group(['prefix' => 'finance'],function(){
        //消费记录
        Route::any('consume', 'FinanceController@consume');
    });

    //图片上传
    Route::group(['prefix' => 'other'],function(){
        Route::any('upload_img','OtherController@upload_img');
    });


    //用户接口
    Route::group(['prefix' => 'user'],function(){
        //用户登录
        Route::any('login', 'UserController@login');
        Route::any('logout', 'UserController@logout');
    });

    //其它
    Route::group(['prefix' => 'other'],function(){
        Route::any('upload_img', 'OtherController@upload_img');
    });

});

