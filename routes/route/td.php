<?php
use App\Http\Middleware\TdUserLogin;//注意引用该类文件
use App\Http\Middleware\TdAdminLogin;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/11
 * Time: 10:26
 */
Route::group(['domain' =>  'zc.' . env('APP_DOMAIN'), 'namespace' => 'Td'], function () {
    Route::any('login.html','LoginController@user_login');
    Route::any('user_login_submit','LoginController@user_login_submit');
    Route::any('logout','LoginController@logout');
    Route::any('forget_password','LoginController@forget_password');
    Route::any('forget_password_submit','LoginController@forget_password_submit');
    Route::any('send_sms','LoginController@send_sms');

});

Route::group(['domain' =>  'zc.' . env('APP_DOMAIN'), 'namespace' => 'Td','middleware'=>[TdUserLogin::class]], function () {
    Route::get('/', function () {
        return redirect('/index/index');
    });
    Route::group(['prefix' => 'index'],function(){
        Route::any('index','IndexController@index');
        Route::any('edit_user_info_submit','IndexController@edit_user_info_submit');
    });

    Route::group(['prefix' => 'order'],function(){
        Route::any('order_list','OrderController@order_list');
    });

    Route::group(['prefix' => 'statement'],function(){
        Route::any('statement_list','StatementController@statement_list');
    });

    Route::group(['prefix' => 'info'],function(){
        Route::any('edit_password','LoginController@edit_password');
        Route::any('edit_password_submit','LoginController@edit_password_submit');
    });
});

Route::group(['domain' =>  'zc-admin.' . env('APP_DOMAIN'), 'namespace' => 'TdAdmin'], function () {
    Route::any('login.html','LoginController@admin_login');
    Route::any('admin_login_submit','LoginController@admin_login_submit');
    Route::any('logout','LoginController@logout');
});

Route::group(['domain' =>  'zc-admin.' . env('APP_DOMAIN'), 'namespace' => 'TdAdmin','middleware'=>[TdAdminLogin::class]], function () {
    Route::get('/', function () {
        return redirect('/index/index');
    });

    Route::group(['prefix' => 'index'],function(){
        Route::any('index','IndexController@index');
    });

    Route::group(['prefix' => 'manager'],function(){
        Route::any('manager_list','ManagerController@manager_list');
        Route::any('add_manager','ManagerController@add_manager');
        Route::any('add_manager_submit','ManagerController@add_manager_submit');
    });

    Route::group(['prefix' => 'pos'],function(){
        Route::any('pos_list','PosController@pos_list');
        Route::any('add_pos','PosController@add_pos');
        Route::any('add_pos_submit','PosController@add_pos_submit');
        Route::any('edit_pos_submit','PosController@edit_pos_submit');
    });

    Route::group(['prefix' => 'order'],function(){
        Route::any('order_list','OrderController@order_list');
    });

    Route::group(['prefix' => 'statement'],function(){
        Route::any('statement_list','StatementController@statement_list');
    });
});