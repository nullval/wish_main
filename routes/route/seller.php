<?php

use App\Http\Middleware\SellerLogin;//注意引用该类文件

//不用做登录验证的
Route::group(['domain' => env('APP_ADMIN_DOMAIN') . env('APP_DOMAIN'), 'namespace' => 'Seller'], function () {
//    Route::any('login', 'LoginController@login');
//    Route::any('send_sms', 'LoginController@send_sms');
//    Route::any('login_submit', 'LoginController@login_submit');
//    Route::any('register', 'LoginController@register');
//    Route::any('register_submit', 'LoginController@register_submit');
//    Route::any('logout', 'LoginController@logout');
//    Route::any('nologin', 'LoginController@nologin');
//    Route::any('upload_img', 'CommonController@upload_img');
//    //app 下载
//    Route::any('download', 'CommonController@download');
//    Route::get('/captcha/{tmp}', 'LoginController@captcha');
//    //财务管理 (定时任务查询代付结果)
//    Route::group(['prefix' => 'finance'], function () {
//        Route::any('checkPay', 'FinanceController@checkPay');
//    });
    //扫描二维码
    Route::any('scan_code', 'CommonController@scan_code');
});
//需要做登录验证的(使用中间键)
Route::group(['domain' => env('APP_SELLER_DOMAIN') . env('APP_DOMAIN'), 'namespace' => 'Seller', 'middleware' => [SellerLogin::class]], function () {
    Route::get('/', function () {
        return redirect('/settle/apply');
    });

    //商家入驻模块
    Route::group(['prefix' => 'settle'], function () {
        Route::any('apply', 'MemberController@apply');
        Route::any('code', 'MemberController@code');
        Route::any('apply_submit', 'MemberController@apply_submit');
        Route::any('test', 'MemberController@test');
        Route::any('seller_info', 'MemberController@seller_info');
        Route::any('seller_info_submit', 'MemberController@seller_info_submit');
        Route::any('banner_list', 'MemberController@banner_list');
        Route::any('banner_add', 'MemberController@banner_add');
        Route::any('banner_add_submit', 'MemberController@banner_add_submit');
    });

    //订单管理
    Route::group(['prefix' => 'order'], function () {
        Route::any('order_list', 'OrderController@order_list');
    });

    //财务管理
    Route::group(['prefix' => 'finance'], function () {
        Route::any('index', 'FinanceController@index');
        Route::any('getbankcode', 'FinanceController@getbankcode');
        Route::any('withdraw_list', 'FinanceController@withdraw_list');
        Route::any('withdraw', 'FinanceController@withdraw');
        Route::any('subwithdraw', 'FinanceController@subwithdraw');
        Route::any('withdraw_goods_list', 'FinanceController@withdraw_goods_list');
        Route::any('withdraw_goods', 'FinanceController@withdraw_goods');
        Route::any('sub_goods_withdraw', 'FinanceController@sub_goods_withdraw');
        Route::any('huanxun_subwithdraw', 'FinanceController@huanxun_subwithdraw');
        Route::any('exchange', 'FinanceController@exchange');
        Route::any('exchange_submit', 'FinanceController@exchange_submit');
        Route::any('increment_submit', 'FinanceController@increment_submit');
        Route::any('statement_list', 'FinanceController@statement_list');
        Route::any('statement_details', 'FinanceController@statement_details');
        Route::any('statement_finance_details', 'FinanceController@statement_finance_details');
        Route::any('new_statement_list', 'FinanceController@new_statement_list');
        Route::any('new_statement_details', 'FinanceController@new_statement_details');
        Route::any('change_automatic', 'FinanceController@change_automatic');
        Route::any('exchange_list', 'FinanceController@exchange_list');
    });

    //POS机管理
    Route::group(['prefix' => 'pos'], function () {
        Route::any('pos_list', 'PosController@pos_list');

    });

    //POS机管理
    Route::group(['prefix' => 'yixian'], function () {
        Route::any('yixian', 'YixianController@yixian');
        Route::any('yixian_submit', 'YixianController@yixian_submit');

    });


    //系统管理
    Route::group(['prefix' => 'config'], function () {
        Route::any('cashback', 'ConfigController@cashback');
        Route::any('cashback_submit', 'ConfigController@cashback_submit');
        Route::any('role_list', 'ConfigController@role_list');
        Route::any('role_add', 'ConfigController@role_add');
        Route::any('role_add_submit', 'ConfigController@role_add_submit');
        Route::any('role_del', 'ConfigController@role_del');
        Route::any('auto_set', 'ConfigController@auto_set');
        Route::any('auto_set_submit', 'ConfigController@auto_set_submit');
    });
});