@extends('agent.layouts.'.$layouts)

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">团队列表</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="year">--}}
                        {{--<option value="2017">2017</option>--}}
                        {{--<option value="2018" @if(isset($_GET['year'])&&$_GET['year']==2018||date('Y')==2018) selected @endif>2018</option>--}}
                        {{--<option value="2019" @if(isset($_GET['year'])&&$_GET['year']==2019||date('Y')==2019) selected @endif>2019</option>--}}
                        {{--<option value="2020" @if(isset($_GET['year'])&&$_GET['year']==2020||date('Y')==2020) selected @endif>2020</option>--}}
                    {{--</select>--}}
                {{--</div>--}}

                @if(session('pushQr_user_info')->level<=2)
                    {{--运营中心和地推市场部门领导才能看到--}}
                    <div class="col-sm-6" style="float: right ;margin: 10px 0px;">
                            <a href="{{url('/team/team_add')}}" class="btn btn-info" style="float: right;"><i class="fa fa-plus"></i> 添加团队成员</a>
                    </div>
                @endif
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>手机号码</th>
                        <th>推荐人</th>
                        <th>身份</th>
                        {{--<th>状态</th>--}}
                        <th>创建时间</th>
                        <th>地推二维码查看</th>
                        <th>商家查看</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($list as $k=>$v)
                            <tr>
                                <td>{{$v->id}}</td>
                                <td>{{$v->mobile}}</td>
                                <td>{{$v->inviter_mobile}}</td>
                                <td>
                                    @if($v->level==2)
                                            地推市场部领导人
                                        @elseif($v->level==3)
                                            地推人员
                                    @endif
                                </td>
                                {{--<td>{{$v->status}}</td>--}}
                                <td>{{$v->created_at}}</td>
                                <td>
                                    <a href="{{url('/team/pushQr_list')}}?id={{$v->id}}"><span class="label label label-success"><i class="fa fa-eye"></i> 查看</span></a>
                                </td>
                                <td>
                                    <a href="{{url('/team/seller_list')}}?id={{$v->id}}"><span class="label label label-success"><i class="fa fa-eye"></i> 查看</span></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $list->appends([
                         'id'=>isset($_GET['id'])?$_GET['id']:0
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- END RECENT PURCHASES -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post"   action="{{url('/seller/create_pushqr_submit')}}" id="form" class="form-horizontal">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$_GET['id'] or 0}}" name="id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">生成地推商家二维码</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-4 control-label">运营中心 </label>
                            <div class="col-sm-6">
                                <label class="control-label">{{$data['mobile']}}</label>
                            </div>
                        </div>
                        <div>
                            <label class="col-sm-4 control-label">二维码数量</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" autocomplete="off" id="safe_code" name="push_number"/>
                            </div>
                        </div>
                        <div style="clear: both"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success submit">生成</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        $(function () {
            $('.submit').click(function () {
                var num=$('input[name=push_number]').val();
                if(num==''||num==0){
                    alert('地推商家二维码不能为0');
                    return false;
                }
                var message='确定要为运营中心{{$_GET['mobile']}}生成'+num+'张地推商家二维码';
                if(confirm(message)){
                    $('#form').submit();
                }
            });
        })
    </script>
@endsection