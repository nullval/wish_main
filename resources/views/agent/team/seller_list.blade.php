@extends('agent.layouts.'.$layouts)

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">商家列表管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding">--}}
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                <div class="col-sm-12">
                    <span class="form-control">地推人员手机号 : {{$info->mobile}} </span>
                    <span class="form-control">身份 : @if($info->level==1)
                            运营中心
                        @elseif($info->level==2)
                            地推市场部领导人
                        @elseif($info->level==3)
                            地推人员
                        @endif </span>
                    <span class="form-control">推荐人 : {{$info->inviter_mobile}}</span>
                </div>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>商家手机号</th>
                        <th>注册时间</th>
                        <th>入驻状态</th>
                        {{--<th>编辑入驻信息</th>--}}
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($seller_list as $k=>$v)
                            <tr>
                                <td>{{$v['id']}}</td>
                                <td>{{$v['mobile'] or '暂无'}}</td>
                                <td>{{$v['created_at'] or '暂无'}}</td>
                                <td>
                                    @if($v['apply_status']==1)
                                        未申请
                                    @elseif($v['apply_status']==2 || empty($v['apply_status']))
                                        审核中
                                    @elseif($v['apply_status']==3)
                                        审核通过
                                    @elseif($v['apply_status']==4)
                                        审核失败
                                    @endif
                                </td>
                                {{--<td>--}}
                                    {{--<a href="{{url('/seller/seller_edit')}}?uid={{$v['id']}}"><span class="label label label-warning"><i class="fa fa-edit"></i> 编辑</span></a>--}}
                                {{--</td>--}}
                          </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            {{--</div>--}}
            <div class="panel-footer">
                <div class="row">
                    {{ $seller_list->appends([
                    'mobile'=>isset($_GET['mobile'])?$_GET['mobile']:''
                    ])->links() }}
                </div>
                <ul class="pagination" style="float: right">
                    <li>
                        <a href="#">总计条数:{{ $seller_list->total()}}</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            var _token=$('input[name=_token]').attr('tip');
            //启用/禁用用户
            $('.status').click(function(){
                var $this=$(this);
                var status=$(this).attr('tip');
                var id=$(this).attr('tip1');
                var type=2;
                $.ajax({
                    type: "POST",
                    url: '/seller/change_status',
                    data: {_token:_token, status:status,id:id,type:type},
                    dataType: "json",
                    success: function(data){
                        if(data.code==1){
                            var span=$this.children('span');
                            if(status==0){
                                $this.attr('tip',1);
                                span.html('启用');
                                span.removeClass('label-danger');
                                span.addClass('label-success');
                            }else{
                                $this.attr('tip',0);
                                span.html('禁用');
                                span.removeClass('label-success');
                                span.addClass('label-danger');
                            }
                        }
                    }
                });
            });

            $('#search').click(function(){
                location.href="{{url('/team/seller_list')}}?mobile="+$('#mobile').val();
            });

        });
        @if(Session::has('status'))
		    setTimeout( " delData() " , 500 );
        @endif

//    	setTimeout( " delData() " , 500 );


        function delData() {
            $.ajax({
                url: '{{url('/seller/delData')}}',
                type: 'POST', //GET
                async: true,    //或false,是否异步
                data: {},
                timeout: 5000,    //超时时间
                dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success: function (data) {
                    {{--alert({{session('mobile')}})--}}
                            {{--if ("{{old('mobile')}}" == ""){--}}
                            {{--$("[name='mobile']").val("{{session('mobile')}}");--}}
                            {{--}--}}
                            window.onload = function(){
                        setTimeout("location.reload()",500);
                    }

                },
                error: function (xhr, textStatus) {
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },
            })
        }
    </script>
@endsection