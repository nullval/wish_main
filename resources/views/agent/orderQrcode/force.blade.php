<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge，chrome=1">
    <title>获得CHQ</title>
    <style type="text/css" media="screen">
        html,body{text-align: center;font-family: 'Avenir', Helvetica, Arial, sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            color: #000;
            height:100%;
            overflow:hidden;

            position:relative;
            font-size: 16px;}
        .content{position:absolute;left:0;right:0;top:60%;transform:translateY(-100%);display: none;}
        img{width:200px;vertical-align: middle;margin-bottom: 30px;}
        .mask {
            position: absolute; top: 0px; filter: alpha(opacity=60); background-color: #777;
            z-index: 1002; left: 0px;
            opacity:0.5; -moz-opacity:0.5;
            background: url({{asset('images/load.gif')}});
        }
    </style>
</head>
<body>
<div class="content">
        <img src="{{asset('qrcode.jpg')}}" />
        <div>
            <span id="CHQ"></span>
        </div>
</div>
<div id="mask" class="mask"></div>
</body>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/myself.js')}}"></script>
<script>
    var startTime = new Date().getTime();
    function checkCqh(){
        //30秒超时
        if(new Date().getTime() - startTime > 1){
            stop()
            $('#CHQ').text("恭喜您获得CHQ!扫码关注CHQT科技。");
            // $('#CHQ').text("请求超时!扫码关注CHQT科技。");
            return;
        }
        $.ajax({
            url:'{{url('/orderQrcode/check_get_chq')}}',
            type:'POST', //GET
            async:true,    //或false,是否异步
            data:{
                order_sn:'{{$order_sn}}'
            },
            timeout:5000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success:function(data){
                if(data.data.status==true){
                    stop()
                    $('#CHQ').text("恭喜您获得"+data.data.count+"个CHQ!扫码关注CHQT科技。");
                }
            }
        })
    }
    function stop() {
        clearInterval(t);
        hideMask();
        $('.content').show();
    }

    showMask();
    var t=setInterval("checkCqh()",1000);
</script>
</html>
