@extends('agent.layouts.noApp')
<style>
    .title{
        color:black;
        font-size:16px;
    }
</style>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">获取CHQ</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/orderQrcode/active_qrcode_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" class="form-control" name="id"   value="{{$_GET['id']}}">
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">手机号码</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mobile"   placeholder="手机号码" value="{{old('mobile')}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 获取CHQ</button>
                </div>
            </form>
        </div>
    </div>



    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection