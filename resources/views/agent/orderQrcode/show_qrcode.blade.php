<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge，chrome=1">
    <title>{{$title or '成功提示'}}</title>
    <style type="text/css" media="screen">
        html,body{text-align: center;font-family: 'Avenir', Helvetica, Arial, sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            color: #000;
            height:100%;
            overflow:hidden;

            position:relative;
            font-size: 16px;}
        .content{position:absolute;left:0;right:0;top:50%;transform:translateY(-60%);}
        img{width:60%;vertical-align: middle;margin:0 auto;}
        .title{margin-bottom: 10px;}
    </style>
</head>
<body>

<div class="content">
    <div class="title">
        扫码关注公众号
    </div>
    <img src="{{asset("agent/img/qrcode.jpg")}}">
    <div>
        {{$message}}
    </div>
</div>

</body>
</html>