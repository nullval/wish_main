@extends('agent.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">推荐机主</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                <div class="col-sm-6">
                    <input type="text" id="mobile" class="form-control" placeholder="推荐人手机号" style="width: 200px;float:left;" value="@if(isset($_GET['mobile'])&&(!empty($_GET['mobile']))){{$_GET['mobile']}}@endif"/>
                    <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
                </div>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>推荐机主手机号</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($inviter_list as $k=>$v)
                            <tr>
                                <td>{{$v['id']}}</td>
                                <td>{{$v['mobile']}}</td>
                          </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $inviter_list->appends([
                    'mobile'=>isset($_GET['mobile'])?$_GET['mobile']:''
                    ])->links() }}
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){

            $('#search').click(function(){
                location.href="{{url('/info/inviter_list')}}?mobile="+$('#mobile').val();
            });

        });
    </script>
@endsection