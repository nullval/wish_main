@extends('agent.layouts.app')

@section('content')
    <div class="col-md-4 col-xs-12">
        <!-- RECENT PURCHASES -->
        <div class="panel" style="padding-bottom: 20px;">
            <div class="panel-heading">
                <h3 class="panel-title">我得九度人脉</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if ($errors->has('error'))
                        <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                    @endif
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                <div class="col-sm-4">
                    <select class="form-control" id="floor">
                        <option value="1">1度</option>
                        <option value="2" @if(isset($_GET['floor'])&&$_GET['floor']==2) selected @endif>2度</option>
                        <option value="3" @if(isset($_GET['floor'])&&$_GET['floor']==3) selected @endif>3度</option>
                        <option value="4" @if(isset($_GET['floor'])&&$_GET['floor']==4) selected @endif>4度</option>
                        <option value="5" @if(isset($_GET['floor'])&&$_GET['floor']==5) selected @endif>5度</option>
                        <option value="6" @if(isset($_GET['floor'])&&$_GET['floor']==6) selected @endif>6度</option>
                        <option value="7" @if(isset($_GET['floor'])&&$_GET['floor']==7) selected @endif>7度</option>
                        <option value="8" @if(isset($_GET['floor'])&&$_GET['floor']==8) selected @endif>8度</option>
                        <option value="9" @if(isset($_GET['floor'])&&$_GET['floor']==9) selected @endif>9度</option>
                    </select>
                </div>
            </div>

            @foreach($list as $k=>$v)
            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto">
                <div class="panel-body" style="overflow: hidden; width: auto; height: auto">
                    <ul class="list-unstyled activity-list">
                        <li>
                            <img src="{{asset('home/img/avator.jpg')}}" alt="Avatar" class="img-circle pull-left avatar">
                            <p><a href="#">{{$v->mobile}}</a></p>
                            <p>推荐人:{{$v->inviter_mobile}}</p>
                            <p>注册时间:{{$v->created_at}}</p>
                        </li>

                        <ul class="list-unstyled todo-list">
                            <li>
                                <p>
                                    <span class="title">直推总人数</span>
                                    <span class="short-description">{{$v->inviter_count or 0}}人</span>
                                </p>
                                {{--<div class="controls">--}}
                                    {{--<a href="{{url('/finance/invite_bonus_log')}}"  class="btn btn btn-info" style="float:left;"><i class="fa fa-eye"></i> 查看</a>--}}
                                {{--</div>--}}
                            </li>

                        </ul>
                    </ul>
                </div><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 35px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 337.409px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
            @endforeach
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            $('#floor').change(function(){
                location.href="{{url('/info/nine_floor')}}?floor="+$('#floor').val();
            });
        })

    </script>
@endsection
