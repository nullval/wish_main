@extends('agent.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" xmlns="http://www.w3.org/1999/html"/>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">修改密码</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/info/change_submit')}}" class="form-horizontal"enctype='multipart/form-data'>
                <div class="panel-body">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">旧密码 </label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="old_password"   placeholder="旧密码"{{old('old_password')}}>
                            @if ($errors->has('old_password'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('old_password') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">新密码 </label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="password"   placeholder="新密码" {{old('password')}}>
                            @if ($errors->has('password'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">确认新密码 </label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="password_confirmation"   placeholder="确认新密码" {{old('password_confirmation')}}>
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                            <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 修改</button>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>

@endsection
@section('js')
    <script src="{{asset('image_frame/js/jqueryLibrary.min.js')}}"></script>
    <script src="{{asset('image_frame/js/jquery.imgbox.pack.js')}}"></script>
    <script>


    </script>
@endsection