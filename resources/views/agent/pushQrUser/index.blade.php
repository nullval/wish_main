@extends('agent.layouts.'.$layouts)
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" xmlns="http://www.w3.org/1999/html"/>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">地推人员基本信息</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 请慎重提交,提交之后不能修改,如若要修改请联系管理员</div>--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/pushQrUser/bind_submit')}}" class="form-horizontal"enctype='multipart/form-data'>
                <div class="panel-body">
                    {{csrf_field()}}
                    {{--<input type="hidden" value="{{$info->bank_id or old('bank_id')}}" name="bank_id">--}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" >手机号 </label>
                        <div class="col-sm-10" >
                            <label for="signin-text" class="col-sm-2 control-label"style="text-align: left"id="username">{{$info->mobile}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" >推荐人 </label>
                        <div class="col-sm-10" >
                            <label for="signin-text" class="col-sm-2 control-label"style="text-align: left"id="username">{{$info->inviter_mobile or '暂无'}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" >所属运营中心 </label>
                        <div class="col-sm-10" >
                            <label for="signin-text" class="col-sm-2 control-label"style="text-align: left"id="username">{{$info->operate_mobile or '暂无'}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" >身份 </label>
                        <div class="col-sm-10" >
                            <label for="signin-text" class="col-sm-2 control-label"style="text-align: left"id="username">
                                @if($info->level==1)
                                        运营中心
                                    @elseif($info->level==2)
                                        地推市场部领导人
                                    @elseif($info->level==3)
                                        地推人员
                                @endif
                            </label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">银行卡号 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="bank_account"   placeholder="银行卡号" value="{{$bank_info->bank_account or old('bank_account')}}">
                                @if ($errors->has('bank_account'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('bank_account') }}</span>
                                @endif
                            </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">持卡人姓名 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="account_name"   placeholder="持卡人姓名" value="{{$bank_info->account_name or old('account_name')}}">
                            @if ($errors->has('account_name'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('account_name') }}</span>
                            @endif
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">开户银行 </label>
                            <div class="col-sm-10">
                                <select id="bank_name" class="form-control" onchange="getbankNo()">
                                    <option >--请选择银行获取联行号--</option>
                                    @foreach($bank_list as $v)
                                    <option @if(!empty($bank_info)&&$v->bank_name==$bank_info->account_bank)selected="selected"@endif>{{$v->bank_name}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" class="form-control" readonly name="account_bank" id="account_bank"    placeholder="银行卡所属地区银行的联行号" value="{{$bank_info->account_bank or old('account_bank')}}">
                                @if ($errors->has('account_bank'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('account_bank') }}</span>
                                @endif
                            </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">	开户银行代码 </label>--}}
                            {{--<div class="col-sm-10">--}}
                                {{--<input type="text" class="form-control" name="bank_code" id="bank_code" readonly  placeholder="开户银行代码" value="{{$info->bank_code or old('bank_code')}}" style="width: 100%">--}}
                                {{--@if ($errors->has('bank_code'))--}}
                                    {{--<span class="help-block m-b-none text-danger">{{ $errors->first('bank_code') }}</span>--}}
                                {{--@endif--}}
                                {{--<div class="" style="margin-top: 10px; ">--}}
                                    {{--<input type="text" id="bank_name1" class="form-control" placeholder="输入银行名字获取银行代码" style="width: 200px;float:left;"   style="margin-top: 10px">--}}
                                    {{--<a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 获取</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                    {{--</div>--}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">银行卡所属地区银行联行号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" readonly name="ibankno"  id="ibankno"  placeholder="银行卡所属地区银行的联行号" value="{{$bank_info->ibankno or old('ibankno')}}">
                            <span class="help-block m-b-none text-danger">请通过选定银行名字获取联行号</span>
                            @if ($errors->has('ibankno'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('ibankno') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">短信验证码<br><span class="help-block m-b-none text-danger">(每次绑定都需要输入短信验证码)</span> </label>
                        <div class="col-sm-10">
                            <input ype="text" class="form-control" id="sms_code" placeholder="请输入验证码" autocomplete="off" name="sms_code" style="width: 140px;float:left;"   style="margin-top: 10px">
                            <input type="button" class="btn btn btn-info" id="btn" value="发送手机验证码"  onclick="send_sms()">
                            @if ($errors->has('sms_code'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('sms_code') }}</span>
                            @endif
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-plus"></i> 绑定</button>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>

@endsection
@section('js')
    {{--<script src="{{asset('image_frame/js/jqueryLibrary.min.js')}}"></script>--}}
    {{--<script src="{{asset('image_frame/js/jquery.imgbox.pack.js')}}"></script>--}}
    <script>
        function getbankNo() {
            var bank_name=$("select").val();
            $.ajax({
                url:'{{url('/pushQrUser/get_bankNo')}}',
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    bank_name:bank_name
                },
                timeout:5000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data){

                    if (data.code==0){
                        alert(data.message)
                    }else {
                        $('#ibankno').val(data.data.lbnkNo);
                        $('#account_bank').val(data.data.bank_name);
                    }
                },
                error:function(xhr,textStatus){
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },

            })

        }
        $(function(){
            $('#account_bank').change(function(){
                $('input[name=ibankno]').val($('select[name=account_bank] option:selected').attr('tip'));
            });

            $('#search').click(function(){
                var bank_name=$('#bank_name1').val();
                $.ajax({
                    url:'{{url('/finance/getbankcode')}}',
                    type:'POST', //GET
                    async:true,    //或false,是否异步
                    data:{
                        bank_name:bank_name
                    },
                    timeout:5000,    //超时时间
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(data){

                        if (data.code==0){
                            alert(data.message)
                        }else {
                            $('#bank_code').val(data.data.bank_code);
                            $('#bank_cname').text(data.data.bank_name);
                            $('#bank_name').val(data.data.bank_name);
                            $('#account_bank').val(data.data.bank_name);

                        }
                    },
                    error:function(xhr,textStatus){
                        console.log('错误')
                        console.log(xhr)
                        console.log(textStatus)
                    },

                })
            });
        });

        function send_sms(){
            mobile = $("#username").html();
//            if(!(/^1[3|4|5|8][0-9]\d{4,8}$/.test(mobile))){
//                alert("请输入正确的手机号码");return false;
//            }
            //发送短信验证码,并设置调用时间
            $.ajax({
                type: "POST",
                url: '{{url('/pushQrUser/send_sms')}}',
                data: {mobile: mobile},
                dataType: "json",
                success: function (data) {
                    if (data.code == 1) {
                        alert('手机短信已发送');
                    } else {
                        alert(data.message);
                    }
                    new invokeSettime("#btn");
                }
            });
        }

        function invokeSettime(obj){
            var countdown=60;
            settime(obj);
            function settime(obj) {
                if (countdown == 0) {
                    $(obj).attr("disabled",false);
                    $(obj).val("获取验证码");
                    countdown = 60;
                    return;
                } else {
                    $(obj).attr("disabled",true);
                    $(obj).val("(" + countdown + ") s 重新发送");
                    countdown--;
                }
                setTimeout(function() {
                            settime(obj) }
                        ,1000)
            }
        }




//        setTimeout( " delData() " , 500 );

        function delData() {
            $.ajax({
                url: '{{url('/seller/delData')}}',
                type: 'POST', //GET
                async: true,    //或false,是否异步
                data: {},
                timeout: 5000,    //超时时间
                dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success: function (data) {
                    {{--alert({{session('mobile')}})--}}
                            {{--if ("{{old('mobile')}}" == ""){--}}
                            {{--$("[name='mobile']").val("{{session('mobile')}}");--}}
                            {{--}--}}
                            window.onload = function(){
                        setTimeout("location.reload()",500);
                    }

                },
                error: function (xhr, textStatus) {
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },
            })
        }
    </script>
@endsection