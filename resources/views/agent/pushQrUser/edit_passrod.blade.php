@extends('agent.layouts.'.$layouts)
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" />
@section('content')
    <div class="panel panel-headline">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">修改密码</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/pushQrUser/edit_password_submit')}}" class="form-horizontal" enctype='multipart/form-data'>
                <div class="panel-body">
                    {{csrf_field()}}
                    {{--<input type="hidden" value="{{$info->id or old('id')}}" name="id">--}}
                    {{--<input type="hidden" value="{{$info->uid or old('uid')}}" name="uid">--}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">旧密码 </label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="old_password"   placeholder="旧密码" value="{{old('old_password')}}">
                            @if ($errors->has('old_password'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('old_password') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">新密码 </label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="new_password"   placeholder="新密码" value="{{old('new_password')}}">
                            @if ($errors->has('new_password'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('new_password') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">确认密码 </label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="new_password_confirmation"   placeholder="确认密码" value="{{old('new_password_confirmation')}}">
                            @if ($errors->has('new_password_confirmation'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('new_password_confirmation') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-plus"></i> 提交</button>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
<script>
</script>
@endsection