<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>代理管理后台系统</title>
    <link rel="stylesheet" type="text/css" href="{{asset('seller/css/normalize.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('seller/css/demo.css')}}" />
    <!--必要样式-->
    <link rel="stylesheet" type="text/css" href="{{asset('seller/css/component.css')}}" />
    <!--[if IE]>
    <script src="{{asset('seller/js/html5.js')}}"></script>
    <![endif]-->
    <style>
        .f_a{
            display: block;
            height: 46px;
            line-height: 46px;
            background: #dddddd;
            text-align: center;
            border: #dddddd;
            color:#0096e6;
            font-size:12px;
            width:99%;
            cursor:pointer;
            /*border-radius: 50px;*/
        }
    </style>
</head>
<body>
<div class="container demo-1">
    <div class="content">
        <div id="large-header" class="large-header">
            {{--<canvas id="demo-canvas"></canvas>--}}
            <div class="logo_box" style="top:40%">
                <h3>未特商城注册</h3>
                @if ($errors->has('error'))
                    <span class="error" style="margin-left: 20px;margin-bottom: 5px;color: red">{{ $errors->first('login_error') }}</span>
                @endif
                <form action="{{url('register_submit')}}" method="post">
                    {{csrf_field()}}

                    <div class="input_outer">
                        <span class="u_user"></span>
                        <input name="mobile" class="text" style="color: #FFFFFF !important" type="text" placeholder="手机号码" value="{{old('mobile')}}">
                        @if ($errors->has('mobile'))
                            <div style="color:red;">{{ $errors->first('mobile') }}</div>
                        @endif
                    </div>
                    {{--<div class="input_outer">--}}
                        {{--<span class="u_user"></span>--}}
                        {{--<input name="inviter_mobile" class="text" style="color: #FFFFFF !important" type="text" disabled placeholder="推荐人">--}}
                        {{--<input name="inviter_id" type="hidden" >--}}
                    {{--</div>--}}
                    <div class="input_outer">
                        <span class="us_uer"></span>
                        <input name="password" class="text" style="color: #FFFFFF !important; "value="" type="password" placeholder="请输入密码">
                        @if ($errors->has('password'))
                            <div style="color:red;">{{ $errors->first('password') }}</div>
                        @endif
                    </div>
                    <div class="input_outer">
                        <span class="us_uer"></span>
                        <input name="password_confirmation" class="text" style="color: #FFFFFF !important;"value="" type="password" placeholder="请输入确认密码">
                        @if ($errors->has('password_confirmation'))
                            <div style="color:red;">{{ $errors->first('password_confirmation') }}</div>
                        @endif
                    </div>
                    <div class="input_outer" style="border-radius: 0px;width:65%;float:left;">
                        {{--<input type="test" />--}}
                        {{--<span class="us_uer"></span>--}}
                        <input name="code"  class="text" style="margin-left:10px; color: #FFFFFF !important; position:absolute; z-index:100;"value="" type="test" id="code" placeholder="请输入图形验证码">
                    </div>
                    <div class="input_outer" style="border-radius: 0px;width:34%;float:right;padding:0;border: 0 !important;;">
                        <a onclick="javascript:re_captcha();" ><img src="{{ URL('/captcha/1') }}"  alt="验证码" title="刷新图片" width="99%" height="45" id="en_code" border="0" /></a>
                    </div>
                    @if ($errors->has('code'))
                        <div style="color:red;">{{ $errors->first('code') }}</div>
                    @endif
                    <div style="clear: both;"></div>

                    {{--<div class="input_outer" style="border-radius: 0px;width:65%;float:left;">--}}
                        {{--<input type="test" />--}}
                        {{--<span class="us_uer"></span>--}}
                        {{--<input name="sms_code" class="text" style="margin-left:10px; color: #FFFFFF !important; position:absolute; z-index:100;"  value="{{old('sms_code')}}" type="test" placeholder="请输入手机验证码">--}}
                    {{--</div>--}}
                    {{--<div class="input_outer" style="border-radius: 0px;width:34%;float:right;padding:0;border: 0 !important;;">--}}
                        {{--<input type="button" class="f_a" id="btn" value="发送手机验证码" >--}}
                        {{--<a class="f_a">发送手机短信</a>--}}
                    {{--</div>--}}
                    <div style="clear: both;"></div>
                    @if ($errors->has('sms_code'))
                        <div style="color:red;">{{ $errors->first('sms_code') }}</div>
                    @endif

                    @if ($errors->has('error'))
                        <div style="color:red;">{{ $errors->first('error') }}</div>
                    @endif

                    <button type="submit" class="act-but submit"style="color: #FFFFFF;width: 330px;border: 0;">注册</button>
                    <div class="zhuce" style="text-align: right;">
                        <span class="helper-text" ><a href="{{url('/login')}}" >立即登录</a></span>
                    </div>

                    {{--<div class="mb2"><a class="act-but submit" href="javascript:;" style="color: #FFFFFF">登录</a></div>--}}
                </form>
            </div>
        </div>
    </div>
</div><!-- /container -->
{{--<script src="{{asset('seller/js/TweenLite.min.js')}}"></script>--}}
{{--<script src="{{asset('seller/js/EasePack.min.js')}}"></script>--}}
{{--<script src="{{asset('seller/js/rAF.js')}}"></script>--}}
<script src="{{asset('seller/js/demo-1.js')}}"></script>
<script type="text/javascript" src="{{asset('seller/js/jquery.min.js')}}"></script>
{{--<div style="text-align:center;">--}}
{{--<p>更多模板：<a href="http://www.mycodes.net/" target="_blank">源码之家</a></p>--}}
{{--</div>--}}
<script>
    function re_captcha() {
        $url = "{{ URL('/captcha') }}";
        $url = $url + "/" + Math.random();
        document.getElementById('en_code').src=$url;
    }

//    $('#btn').click(function(){
//        code=$("#code").val();
//        mobile=$("input[name=mobile]").val();
//        if(code==''){
//            alert('请输入图形验证码');
//        }else{
//            $.ajax({
//                type: "GET",
//                url: 'check_code',
//                data: {captcha:code},
//                dataType: "json",
//                success: function(data){
//                    if(data.code==0){
//                        alert('图形验证码不正确');
//                    }else{
//
//                        //发送短信验证码,并设置调用时间
//                        $.ajax({
//                            type: "POST",
//                            url: 'send_sms',
//                            data: {mobile:mobile},
//                            dataType: "json",
//                            success: function(data){
//                                if(data.code==1){
//                                    alert('手机短信已发送');
//                                }else{
//                                    alert(data.message);
//                                }
//                                new invokeSettime("#btn");
//                            }
//                        });
////                        new invokeSettime("#btn");
//                    }
//                }
//            });
//        }
//
////        new invokeSettime("#btn");
//    })
//
//    function invokeSettime(obj){
//        var countdown=60;
//        settime(obj);
//        function settime(obj) {
//            if (countdown == 0) {
//                $(obj).attr("disabled",false);
//                $(obj).val("获取验证码");
//                countdown = 60;
//                return;
//            } else {
//                $(obj).attr("disabled",true);
//                $(obj).val("(" + countdown + ") s 重新发送");
//                countdown--;
//            }
//            setTimeout(function() {
//                        settime(obj) }
//                    ,1000)
//        }
//    }

</script>
</body>
</html>