<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
       地推人员管理系统
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Free HTML5 Template by FreeHTML5.co" />
    <meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />
    <meta   http-equiv= "Pragma"   content= "no-cache" />
    <meta   http-equiv= "Cache-Control"   content= "no-cache" />
    <meta   http-equiv= "Expires"   content= "0" />
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="favicon.ico">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('home/img/apple-icon.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('home/img/favicon.png')}}">
    <link rel="stylesheet" href="{{asset('/agent/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/agent/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('/agent/css/beistyle.css')}}">
    <style>
        .f_a{
            /*display: block;*/
            height: 46px;
            line-height: 46px;
            background: white;
            text-align: center;
            border: #dddddd;
            color:#0096e6;
            font-size:13px;
            width:38%;
            cursor:pointer;
            border-radius: 50px;
        }
    </style>

    <!-- Modernizr JS -->
    <script src="{{asset('/agent/js/modernizr-2.6.2.min.js')}}"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="{{asset('/agent/js/respond.min.js')}}"></script>
    <![endif]-->

</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <ul class="menu">
                <li class="active"> </li>
                <!-- 	<li class="active"><a href="index.html">Style 1</a></li> -->
                <!-- 					<li><a href="index2.html">Style 2</a></li>
									<li><a href="index3.html">Style 3</a></li> -->
            </ul>
        </div>
    </div>
    <div class="copyrights">Collect from <a href="http://www.php.cn/"  title="网站模板">网站模板</a></div>
    <div class="row" s>
        <div class="col-md-4 col-md-offset-4">


            <!-- Start Sign In Form -->
            <form action="{{url('pushQr_login_submit')}}" class="fh5co-form animate-box" method="post" data-animate-effect="fadeIn">
                <h2 style="font-size: 28px;text-align: center;">
                        地推人员管理系统
                </h2>
                {{csrf_field()}}
                <div class="col-sm-12" style="text-align: center">
                    <input type="hidden" value="{{isset($_COOKIE['openid'])?$_COOKIE['openid']:0}}" name="openid">
                    <input type="hidden" value="{{$_GET['member_type']}}" name="member_type">
                    @if ($errors->has('login_error'))
                        <span class="error" style="margin-left: 20px;margin-bottom: 5px;color: red">{{ $errors->first('login_error') }}</span>
                    @endif
                    @if(Session::has('status'))
                        <div class="alert alert-info" style="color: green;padding-bottom: 5px; font-size: 21px">{{Session::get('status')}}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="username" class="sr-only">手机号码</label>
                    <input type="text" class="form-control" id="mobile" placeholder="请输入手机号码" autocomplete="off" name="mobile" value="{{old('mobile')}}">
                    @if ($errors->has('mobile'))
                        <div style="color:red;">{{ $errors->first('mobile') }}</div>
                    @endif
                </div>
                {{--<div class="form-group">--}}
                    {{--<label for="password" class="sr-only">密码</label>--}}
                    {{--<input type="password" class="form-control" id="password" placeholder="请输入密码" autocomplete="off" name="password">--}}
                    {{--@if ($errors->has('password'))--}}
                        {{--<div style="color:red;">{{ $errors->first('password') }}</div>--}}
                    {{--@endif--}}
                {{--</div>--}}
                <div class="form-group">
                    <label for="username" class="sr-only">登录密码</label>
                    <input type="password" class="form-control" id="mobile" placeholder="请输入登录密码" autocomplete="off" name="password">
                    @if ($errors->has('password'))
                        <div style="color:red;">{{ $errors->first('password') }}</div>
                    @endif
                </div>
                <div class="form-group" >
                    <input type="submit" value="登 录" class="btn btn-block btn-lg btn-dark" style="font-size: 20px">
                </div>
            </form>
            <!-- END Sign In Form -->

        </div>
    </div>

</div>

<!-- jQuery -->
<script src="{{asset('/agent/js/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('/agent/js/bootstrap.min.js')}}"></script>
<!-- Placeholder -->
<script src="{{asset('/agent/js/jquery.placeholder.min.js')}}"></script>
<!-- Waypoints -->
<script src="{{asset('/agent/js/jquery.waypoints.min.js')}}"></script>
<!-- Main JS -->
<script src="{{asset('/agent/js/main.js')}}"></script>


</body>
</html>

