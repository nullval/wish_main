<!doctype html>
<html>
<head>
    <title>机主管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Sublime Contact Form Widget Responsive, Login form web template,Flat Pricing tables,Flat Drop downs  Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Muli:300,400" rel="stylesheet">
    <!-- /fonts -->
    <!-- css -->
    <link href="{{asset('agent/css/style.css')}}" rel='stylesheet' type='text/css' media="all" />
    <!-- /css -->
    <style>
        .f_a{
            /*display: block;*/
            height: 46px;
            line-height: 46px;
            background: #dddddd;
            text-align: center;
            border: #dddddd;
            color:#0096e6;
            font-size:13px;
            width:33%;
            cursor:pointer;
            /*border-radius: 50px;*/
        }
    </style>
</head>
<body>
<h1 class="agileits w3 wthree w3-agile w3-agileits agileinfo agile">机主管理系统</h1>
<div class="content-w3ls agileits w3 wthree w3-agile w3-agileits agileinfo agile">
    <form action="{{url('login_submit')}}" method="post" class="form-agileits">
        {{csrf_field()}}
        <div class="col-sm-12" style="text-align: center">
            @if ($errors->has('login_error'))
                <span class="error" style="margin-left: 20px;margin-bottom: 5px;color: red">{{ $errors->first('login_error') }}</span>
            @endif
            @if(Session::has('status'))
                <div class="alert alert-info" style="color: green;padding-bottom: 5px; font-size: 21px">{{Session::get('status')}}</div>
            @endif
        </div>
        <input type="text" id="username" name="mobile" placeholder="用户名" title="请输入你的用户名" required="">
        @if ($errors->has('mobile'))
            <div style="color:red;">{{ $errors->first('mobile') }}</div>
        @endif

        <input type="password" id="password" name="password" placeholder="密码" title="请输入你的密码" required="">
        @if ($errors->has('password'))
            <div style="color:red;">{{ $errors->first('password') }}</div>
        @endif
        {{--<input type="text" id="sms_code" name="sms_code" class="text"  placeholder="请输入验证码" title="请输入手机验证码" required="" style="width: 36%">--}}
        {{--<input type="button" class="f_a" id="btn" value="发送手机验证码" >--}}
        {{--<div style="clear: both;"></div>--}}
        {{--@if ($errors->has('sms_code'))--}}
            {{--<div style="color:red;">{{ $errors->first('sms_code') }}</div>--}}
        {{--@endif--}}

        {{--<input type="email" id="email" name="email" placeholder="MAIL@EXAMPLE.COM" title="Please enter a Valid Email Address" required="">--}}
        {{--<textarea id="message" name="message" placeholder="YOUR MESSAGE" title="Please enter Your Comments"></textarea>--}}
        <input type="submit" class="sign-in" value="登录">
    </form>
</div>
{{--<p class="copyright agileits w3 wthree w3-agile w3-agileits agileinfo agile">© 2017 Sublime Contact Form. All Rights Reserved | Design by <a href="https://w3layouts.com/" target="_blank">w3layouts</a></p>--}}
</body>
</html>
<script>

    $('#btn').click(function() {
        mobile = $("input[name=mobile]").val();
        alert(mobile);
        //发送短信验证码,并设置调用时间
        $.ajax({
            type: "POST",
            url: '{{url('/send_sms')}}',
            data: {mobile: mobile},
            dataType: "json",
            success: function (data) {
                if (data.code == 1) {
                    alert('手机短信已发送');
                } else {
                    alert(data.message);
                }
                new invokeSettime("#btn");
            }
        });
    }

    function invokeSettime(obj){
        var countdown=60;
        settime(obj);
        function settime(obj) {
            if (countdown == 0) {
                $(obj).attr("disabled",false);
                $(obj).val("获取验证码");
                countdown = 60;
                return;
            } else {
                $(obj).attr("disabled",true);
                $(obj).val("(" + countdown + ") s 重新发送");
                countdown--;
            }
            setTimeout(function() {
                        settime(obj) }
                    ,1000)
        }
    }

</script>