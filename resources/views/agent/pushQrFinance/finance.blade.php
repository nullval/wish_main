@extends('agent.layouts.pushQr_app')

@section('content')
    <div class="col-md-12 ">
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">财务管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding" style="margin-bottom: 20px;">
                <div class="col-sm-12">
                    <span class="form-control">收益钱包 : {{get_last_two_num($data['x_purse_info']->balance-$data['x_purse_info']->freeze_value)}} </span>
                    <span class="form-control">冻结额度 : {{get_last_two_num($data['x_purse_info']->freeze_value)}}</span>
                </div>

            </div>
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-md-12">--}}
        {{--<!-- RECENT PURCHASES -->--}}
        {{--<div class="panel table-responsive">--}}
            {{--<div class="panel-heading">--}}
                {{--<h3 class="panel-title">机主财务流水</h3>--}}
                {{--<div class="right">--}}
                    {{--<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>--}}
                    {{--<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="panel-body no-padding">--}}
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="year">--}}
                        {{--<option value="2017">2017</option>--}}
                        {{--<option value="2018" @if(isset($_GET['year'])&&$_GET['year']==2018||date('Y')==2018) selected @endif>2018</option>--}}
                        {{--<option value="2019" @if(isset($_GET['year'])&&$_GET['year']==2019||date('Y')==2019) selected @endif>2019</option>--}}
                        {{--<option value="2020" @if(isset($_GET['year'])&&$_GET['year']==2020||date('Y')==2020) selected @endif>2020</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="col-sm-2">
                    <select class="form-control" id="status">
                        <option value="0">全部</option>
                        <option value="1" @if(isset($_GET['status'])&&$_GET['status']==1) selected @endif>收入</option>
                        <option value="2" @if(isset($_GET['status'])&&$_GET['status']==2) selected @endif>支出</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="form-control" id="type">
                        <option value="1" @if(isset($_GET['type'])&&$_GET['type']==1) selected @endif>收益钱包</option>
                        {{--<option value="3" @if(isset($_GET['type'])&&$_GET['type']==3) selected @endif>积分</option>--}}
                    </select>
                </div>
                <div class="col-sm-6">
                    总收入 : <span class="text-danger">+{{get_last_two_num($data['total_into'])}}</span> 总支出 : <span class="text-success">-{{get_last_two_num($data['total_out'])}}</span>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>流水编号</th>
                        <th>类型</th>
                        {{--<th>转入/转出会员编号</th>--}}
                        <th>变动额度</th>
                        <th>余额</th>
                        <th>转账时间</th>
                        <th>描述</th>
                        {{--<th>备注</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($transfer as $k=>$v)
                        <tr>
                            <td>{{$v->transfer_id}}</td>
                            <td>
                                @if($v->into_purse_id==$data['purse_id']) 收入
                                @else 支出
                                @endif
                            </td>
                            {{--<td>{{$v->member_id}}</td>--}}
                            <td>
                                @if($v->into_purse_id==$data['purse_id']) <span class="text-danger">+{{get_last_two_num($v->into_amount)}}</span>
                                @else <span class="text-success">-{{get_last_two_num($v->into_amount)}}</span>
                                @endif
                            </td>
                            <td>
                                @if($v->into_purse_id==$data['purse_id']) {{get_last_two_num($v->into_balance)}}
                                @else {{get_last_two_num($v->out_balance)}}
                                @endif
                            </td>
                            <td>{{date('Y-m-d H:i:s',$v->create_time)}}</td>
                            <td>{{$v->detail}}</td>
                            {{--<td>{{$v->remarks}}</td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            {{--</div>--}}
            <div class="panel-footer">
                <div class="row">
                    {{ $transfer->appends([
                    'status'=>isset($_GET['status'])?$_GET['status']:0,
                    'type'=>isset($_GET['type'])?$_GET['type']:0,
                    'id'=>isset($_GET['id'])?$_GET['id']:0
                    ])->links() }}
                    {{--<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>--}}
                    {{--<div class="col-md-6 text-right"><a href="#" class="btn btn-primary">View All Purchases</a></div>--}}
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            $('#status,#type,#year').change(function(){
                location.href="{{url('/pushQrFinance/index')}}?type="+$('#type').val()+"&status="+$('#status').val();
            });
        });
    </script>
@endsection