@extends('agent.layouts.app')

@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">新增地址</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/invoice/address_add_submit?id=')}}@if(!empty($address)){{$address->id}} @endif">
                <div class="panel-body">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">收件人姓名</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="recipient_name"   placeholder="收件人姓名" value="{{$address->recipient_name or old('recipient_name') }}">
                            @if ($errors->has('recipient_name'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('recipient_name') }}</span>
                            @endif
                        </div>
                    </div>


                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">寄件地址</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="address"   placeholder="寄件地址" value="{{$address->address or old('address')}}">
                            @if ($errors->has('address'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">邮件编码</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="postalcode"   placeholder="邮件编码" value="{{$address->postalcode or old('postalcode')}}">
                            @if ($errors->has('postalcode'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('postalcode') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">收件人手机号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mobile"   placeholder="收件人手机号" value="{{$address->mobile or old('mobile')}}">
                            @if ($errors->has('mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                            @endif
                        </div>
                    </div>


                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    @if(empty($address))
                        <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-plus"></i> 新增</button>
                    @else
                        <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 修改</button>
                    @endif
                    <a href="{{url('/invoice/address_list')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>

                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')

@endsection