@extends('agent.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">发票详情</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding" style="margin-bottom: 20px;">
                <div class="col-sm-6">
                    <span class="form-control">发票性质：
                        @if($invoice_detail['Invoice_nature']==1)
                            纸质发票
                        @elseif($invoice_detail['Invoice_nature']==2)
                            电子发票
                        @endif
                    </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">发票状态：
                        @if($invoice_detail['status']==1)
                            <span class="text-primary">未发货</span>
                        @elseif($invoice_detail['status']==2)
                            <span class="text-success">已发货</span>
                        @elseif($invoice_detail['status']==3)
                            <span class="text-danger">已完成</span>
                        @endif
                    </span>
                </div>


                <div class="col-sm-6">
                    <span class="form-control">发票类型：
                        @if($invoice_info['Invoice_type']==1)
                            增值税普通发票
                        @elseif($invoice_info['Invoice_type']==2)
                            增值税专用发票
                        @endif
                    </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">是否有效：
                        @if($invoice_detail['isvalid']==1)
                            <span class="text-primary">有效</span>
                        @elseif($invoice_detail['isvalid']==2)
                            <span class="text-primary">无效</span>
                        @endif
                    </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">发票金额： <span class="text-danger">{{$invoice_detail['amount'] or '暂无'}} </span></span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">发票抬头： {{$invoice_info['Invoice_header'] or '暂无'}} </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">申请时间： {{$invoice['created_at'] or '暂无'}} </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">发票编号： {{$invoice_detail['Invoice_number'] or '暂无'}} </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">收件人： {{$invoice_detail['recipient_name'] or '暂无'}} </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">收件地址： {{$invoice_detail['address'] or '暂无'}} </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">邮政编号： {{$invoice_detail['postalcode'] or '暂无'}} </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">联系电话： {{$invoice_detail['mobile'] or '暂无'}} </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">物流公司： {{$invoice_detail['Logistics_company'] or '暂无'}} </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">快递编号： {{$invoice_detail['express_number'] or '暂无'}} </span>
                </div>

            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-12">--}}
                    {{--<div class="alert alert-warning"> 订单只有已支付且有效,机主才能将自己的未绑定商家的未特商城pos机绑定给其他机主</div>--}}
                {{--</div>--}}
                <div class="col-sm-6" >
                    <h2 class="panel-title"style="padding: 10px;font-size: 18px">相关订单列表</h2>

                </div>

                <table class="table table-striped">

                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>归属订单号</th>
                        <th>消费金额</th>
                        <th>创建时间</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($order_list as $k=>$v)
                            <tr>
                                <td>{{$v->id or '暂无'}}</td>
                                <td>{{$v->order_sn or '暂无'}}</td>
                                <td class="text-danger">{{$v->amount or '暂无'}}</td>
                                <td>{{$v->created_at or '暂无'}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>

            </div>


        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')

@endsection