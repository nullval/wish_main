@extends('agent.layouts.app')

<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" xmlns="http://www.w3.org/1999/html"
      xmlns="http://www.w3.org/1999/html"/>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">发票信息管理</h3>
                <div class="hr-line-solid"></div>
                @if(empty($invoice_info)||$invoice_info['issub']==0)
                <div class="alert alert-warning"> 请您完善以下发票信息。（发票信息仅限一条，请您慎重填写。）</div>
                @endif
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/invoice/invoice_info_submit')}}" class="form-horizontal"enctype='multipart/form-data' onsubmit="return sumbit_sure()">
                <div class="panel-body">
                    {{csrf_field()}}


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" style="padding-top: 0px">开具类型 </label>
                        <div class="col-sm-10">
                            @if(empty($invoice_info)||$invoice_info['issub']==0)
                            <input type="radio" id="personal" class="control-label " name="customerType" value="1" checked /> 个人
                            <input type="radio" id="company" class="control-label" name="customerType" value="2" /> 企业
                            @elseif(!empty($invoice_info)&&$invoice_info['issub']==1)
                                @if($invoice_info['customerType']==1)
                                    <label for="signin-text" class="col-sm-2 control-label" style="text-align: left;padding-left: 0;padding-top: 0">个人</label>、
                                @elseif($invoice_info['customerType']==2)
                                    <label for="signin-text" class="col-sm-2 control-label" style="text-align: left;padding-left: 0;padding-top: 0">企业</label>、
                                @endif
                            @endif
                        </div>
                    </div>

                    @if(empty($invoice_info)||$invoice_info['issub']==0)
                        <div id="type1">
                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">发票抬头 </label>
                                <div class="col-sm-10">
                                    <label for="signin-text" class="col-sm-2 control-label" style="text-align: left;padding-left: 0;">个人</label>
                                    <input type="hidden" class="form-control" name="Invoice_header"   placeholder="请填写您营业执照上的全称" value="个人">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">发票类型 </label>
                                <div class="col-sm-10">
                                    <label for="signin-text" class="col-sm-2 control-label" style="text-align: left;padding-left: 0;">增值税普通发票</label>
                                </div>
                            </div>
                        </div>

                        <div id="type2">
                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">	发票抬头 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="Invoice_header"   placeholder="请填写您营业执照上的全称" value="{{$invoice_info->Invoice_header or old('Invoice_header')}}">
                                    @if ($errors->has('Invoice_header'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('Invoice_header') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label" style="padding-top: 0px">发票类型 </label>
                                <div class="col-sm-10">
                                    <input type="radio" class="control-label " name="Invoice_type" value="1" checked /> 增值税普通发票
                                    <input type="radio" class="control-label" name="Invoice_type" value="2" /> 增值税专用发票
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">	税务登记证号 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="registerNo"   placeholder="请填写您税务登记上的编号" value="{{$invoice_info->registerNo or old('registerNo')}}">
                                    <span class="help-block m-b-none text-success">
                                        企业：包括公司、非公司制企业法人、企业分支机构、个人独资企业、合伙企业和其他企业

                                        1) 如果您不属于以上"企业"，无税号，可以不用填写

                                        2)如果您属于以上"企业"，税号只能是15、17、18、20位数字或字母和数字的组合，请您准确填写。
                                    </span>
                                    @if ($errors->has('registerNo'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('registerNo') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">	基本开户银行名称 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="bank"   placeholder="请填写您开户许可证上的开户银行" value="{{$invoice_info->bank or old('bank')}}">
                                    @if ($errors->has('bank'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('bank') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">	基本开户账号 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="bankNo"   placeholder="请填写您开户许可证上的银行账号" value="{{$invoice_info->bankNo or old('bankNo')}}">
                                    @if ($errors->has('bankNo'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('bankNo') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">	注册场所地址 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="operatingLicenseAddress"   placeholder="请填写您营业执照上的注册地址" value="{{$invoice_info->operatingLicenseAddress or old('operatingLicenseAddress')}}">
                                    @if ($errors->has('operatingLicenseAddress'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('operatingLicenseAddress') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">	注册固定电话 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="operatingLicensePhone"   placeholder="请填写您公司有效的联系电话" value="{{$invoice_info->operatingLicensePhone or old('operatingLicensePhone')}}">
                                    @if ($errors->has('operatingLicensePhone'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('operatingLicensePhone') }}</span>
                                    @endif
                                </div>
                            </div>

                        </div>


                        {{--<div class="clearfix"></div>--}}
                        {{--<div class="hr-line-dashed"></div>--}}
                        <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>

                    @elseif(!empty($invoice_info)&&$invoice_info['issub']==1)
                        @if($invoice_info['customerType']==1)
                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">发票抬头 </label>
                                <div class="col-sm-10">
                                    <label for="signin-text" class="col-sm-2 control-label" style="text-align: left;padding-left: 0;">个人</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">发票类型 </label>
                                <div class="col-sm-10">
                                    <label for="signin-text" class="col-sm-2 control-label" style="text-align: left;padding-left: 0;">增值税普通发票</label>
                                </div>
                            </div>
                        @elseif($invoice_info['customerType']==2)
                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">	发票抬头 </label>
                                <div class="col-sm-10">
                                    <label for="signin-text" class="col-sm-2 control-label" style="text-align: left;padding-left: 0;">{{$invoice_info->Invoice_header}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label" >发票类型 </label>
                                <div class="col-sm-10">
                                    <label for="signin-text" class="col-sm-2 control-label" style="text-align: left;padding-left: 0;">
                                        @if($invoice_info->Invoice_type==1)
                                            增值税普通发票
                                        @elseif($invoice_info->Invoice_type==2)
                                            增值税专用发票
                                        @endif
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">	税务登记证号 </label>
                                <div class="col-sm-10">
                                    <label for="signin-text" class="col-sm-2 control-label" style="text-align: left;padding-left: 0;">{{$invoice_info->registerNo}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">	基本开户银行名称 </label>
                                <div class="col-sm-10">
                                    <label for="signin-text" class="col-sm-2 control-label" style="text-align: left;padding-left: 0;">{{$invoice_info->bank}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">	基本开户账号 </label>
                                <div class="col-sm-10">
                                    <label for="signin-text" class="col-sm-2 control-label" style="text-align: left;padding-left: 0;">{{$invoice_info->bankNo}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">	注册场所地址 </label>
                                <div class="col-sm-10">
                                    <label for="signin-text" class="col-sm-2 control-label" style="text-align: left;padding-left: 0;">{{$invoice_info->operatingLicenseAddress}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">	注册固定电话 </label>
                                <div class="col-sm-10">
                                    <label for="signin-text" class="col-sm-2 control-label" style="text-align: left;padding-left: 0;">{{$invoice_info->operatingLicensePhone}}</label>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>

@endsection
@section('js')
    {{--<script src="{{asset('image_frame/js/jqueryLibrary.min.js')}}"></script>--}}
    {{--<script src="{{asset('image_frame/js/jquery.imgbox.pack.js')}}"></script>--}}
    <script>
        $(function(){
            $(".img1").imgbox();
            $('#type2').hide();
            $('#personal').click(function(){
                $('#type1').show();
                $('#type2').hide();
            });
            $('#company').click(function(){
                $('#type2').show();
                $('#type1').hide();
            });

        });

        function sumbit_sure(){
            var gnl=confirm("确定要提交?");
            if (gnl==true){
                return true;
            }else{
                window.location.reload();
                return false;
            }
        }
    </script>
@endsection