@extends('agent.layouts.app')

<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" xmlns="http://www.w3.org/1999/html"
      xmlns="http://www.w3.org/1999/html"/>



@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">发票基本信息</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/invoice/invoice_add_submit')}}" class="form-horizontal"enctype='multipart/form-data'>
                <div class="panel-body">
                    {{csrf_field()}}

                    <input type="hidden" class="form-control" name="order_list"   placeholder="归属订单列表" value="{{$info['order_list'] or old('order_list')}}">

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">发票金额</label>
                        <div class="col-sm-10">
                            <input type="hidden" class="form-control" name="amount"   placeholder="发票金额" value="{{$info['amount'] or old('amount')}}">
                            <label for="signin-text" class=" col-sm-2 control-label" style="text-align: left">{{$info['amount']}}</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">开具类型 </label>
                        <div class="col-sm-10">
                            <label for="signin-text" class="col-sm-2 control-label" style="text-align: left">
                                @if($info['ticket_info']['customerType']==1)
                                    个人
                                @elseif($info['ticket_info']['customerType']==2)
                                    企业
                                @endif
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">	发票抬头 </label>
                        <div class="col-sm-10">
                            <label for="signin-text" class="col-sm-2 control-label" style="text-align: left">{{$info['ticket_info']['Invoice_header']}}</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">	发票类型 </label>
                        <div class="col-sm-10">
                            <label for="signin-text" class="col-sm-2 control-label" style="text-align: left">
                                @if($info['ticket_info']['Invoice_type']==1)
                                    增值税普通发票
                                @elseif($info['ticket_info']['Invoice_type']==2)
                                    增值税专用发票
                                @endif
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">	选择收件地址 </label>
                        <div class="col-sm-10">
                            @foreach($info['address_list'] as $k=>$v)

                                    <div class="radio" style="margin-left: 15px">
                                        <input  type="radio" name="address_id" id="radio2" value="{{$v->id}}">
                                        <label for="radio2" style="text-align: left;padding-bottom: 10px">
                                            <div style="padding-bottom: 5px">收货人：{{$v['recipient_name']}}&nbsp;&nbsp;联系电话：{{$v['mobile']}}&nbsp;&nbsp;</div>
                                            <div style="">邮政编码：{{$v['postalcode']}}&nbsp;&nbsp; 收货地址：{{$v['address']}}</div>
                                        </label>
                                    </div>

                            @endforeach

                            {{--<table class="table table-striped">--}}
                                 {{--@foreach($info['address_list'] as $k=>$v)--}}
                                        {{--<tr>--}}
                                            {{--<td><input class="check" type="radio" name="address" value="{{$v->id}}"></td>--}}
                                            {{--<td>{{$v['recipient_name']}}</td>--}}
                                            {{--<td>{{$v['mobile']}}</td>--}}
                                            {{--<td>{{$v['postalcode']}}</td>--}}
                                            {{--<td>{{$v['address']}}</td>--}}
                                        {{--</tr>--}}
                                    {{--@endforeach--}}
                            {{--</table>--}}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                            <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                    <a href="{{url('/order/order_list')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>




                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>

@endsection
@section('js')
    <script src="{{asset('image_frame/js/jqueryLibrary.min.js')}}"></script>
    <script src="{{asset('image_frame/js/jquery.imgbox.pack.js')}}"></script>
    <script>
        $(function(){
            $(".img1").imgbox();

        });
        $('#search').click(function(){
            var bank_name=$('#bank_name1').val();
            $.ajax({
                url:'{{url('/finance/getbankcode')}}',
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    bank_name:bank_name
                },
                timeout:5000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data){

                    if (data.code==0){
                        alert(data.message)
                    }else {
                        $('#bank_code').val(data.data.bank_code);
                        $('#bank_cname').text(data.data.bank_name);
                        $('#bank_name').val(data.data.bank_name);

                    }
                },
                error:function(xhr,textStatus){
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },

            })
        });


    </script>
@endsection