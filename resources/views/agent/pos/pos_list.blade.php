@extends('agent.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">pos机管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding">--}}
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                <div class="col-sm-10">
                    <input type="text" id="terminalId" class="form-control" placeholder="终端号" style="width: 200px;float:left;" value="@if(isset($_GET['terminalId'])&&(!empty($_GET['terminalId']))){{$_GET['terminalId']}}@endif"/>
                    <input type="text" id="seller_mobile" class="form-control" placeholder="绑定商家" style="width: 200px;float:left;" value="@if(isset($_GET['seller_mobile'])&&(!empty($_GET['seller_mobile']))){{$_GET['seller_mobile']}}@endif"/>
                    <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>pos机编号</th>
                        <th>终端号</th>
                        <th>绑定商家</th>
                        {{--<th>编辑</th>--}}
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($pos_list as $k=>$v)
                            <tr>
                                <td>{{$v['id']}}</td>
                                <td>{{$v['terminalId'] or '暂无'}}</td>
                                <td>{{$v['seller_mobile'] or '暂无'}}</td>
                                {{--<td>--}}
                                    {{--<a href="{{url('/pos/pos_edit')}}?id={{$v['id']}}"><span class="label label-warning"><i class="fa fa-edit"></i> 编辑</span>--}}
                                    {{--</a>--}}
                                {{--</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            {{--</div>--}}
            <div class="panel-footer">
                <div class="row">
                    {{ $pos_list->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $pos_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){

            $('#search').click(function(){
                location.href="{{url('/pos/pos_list')}}?seller_mobile="+$('#seller_mobile').val()+"&terminalId="+$('#terminalId').val();
            });

        });
    </script>
@endsection