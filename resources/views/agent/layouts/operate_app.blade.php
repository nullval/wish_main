<!doctype html>
<html lang="en">
<head>
    <title>
        @if(session('member_type') == 2)
            未特商城运营中心后台
        @else
            未特商城地推人员后台
        @endif
    </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{asset('home/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/linearicons/style.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/chartist/css/chartist-custom.css')}}">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{asset('home/css/main.css')}}">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="{{asset('home/css/demo.css')}}">
    <link rel="stylesheet" href="{{asset('home/css/myself.css')}}">
{{--<link rel="stylesheet" href="{{asset('home/css/bootstrap.min.css')}}">--}}
{{--<link href="{{asset('home/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" media="screen">--}}
<!-- GOOGLE FONTS -->
{{--<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">--}}
<!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('home/img/apple-icon.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('home/img/favicon.png')}}">
    <link rel="stylesheet" href="{{asset('css/myself.css')}}">

 {{--<!--    <script src="{{asset('seller/js/uploadPreview.js')}}" type="text/javascript"></script>--}}
    {{--<script src="{{asset('seller/js/ajaxfileupload.js')}}" type="text/javascript"></script> -->--}}
    <link rel="stylesheet" type="text/css" href="{{asset('agent/css/build.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('agent/css/awesome-bootstrap-checkbox.css')}}">
    {{--<link rel="stylesheet" type="text/css" href="{{asset('agent/css/bootstrap.css')}}">--}}
    <link rel="stylesheet" type="text/css" href="{{asset('agent/css/build.less.css')}}">
    <script src="{{asset('seller/js/uploadPreview.js')}}" type="text/javascript"></script>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
        /*body, html{width: 100%;height: 100%;margin:0;font-family:"微软雅黑";font-size:14px;}*/
        #l-map{height:250px;width:100%;}
        /*#r-result{width:100%;}*/
    </style>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=RNifyeVIz4GNTjvl9XwDUth7ufYPk2pX"></script>
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="brand">
            <a href="{{url('/info/index')}}"><img src="{{asset('home/img/logo-dark2.png')}}" alt="Klorofil Logo" class="img-responsive logo"></a>
        </div>
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
            </div>

            <div id="navbar-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown hidden-xs">
                        <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="lnr lnr-alarm"></i>
                        </a>

                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{asset('/home/img/avator.jpg')}}" class="img-circle" alt="Avatar"> <span>
                                {{session('agent_info')->mobile}}
                            </span></a>
                    </li>
                    {{--<li>--}}
                        {{--<a class="update-pro" href="{{url('/logout')}}" title="安全退出"><i class="fa fa-rocket"></i> <span>安全退出</span></a>--}}
                    {{--</li>--}}
                </ul>
            </div>
        </div>
    </nav>
    <!-- END NAVBAR -->
    <!-- LEFT SIDEBAR -->
    {{--获取当前控制器名称--}}
    <?php $prefix=request()->route()->getAction()['prefix']; ?>
    {{--获取当前方法--}}
    <?php $route =request()->route()->getAction()['controller'];
            $action=explode('@', $route)[1];?>

    <div id="sidebar-nav" class="sidebar" style="overflow: scroll;">
        <div class="sidebar-scroll" style="margin-bottom: 30px">
            <nav>
                <ul class="nav">
                    <li>
                        <a href="#operate" data-toggle="collapse" class="collapsed @if($prefix=='/operate') active @endif"><i class="lnr lnr-chart-bars"></i> <span>运营中心</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="operate" class="collapse @if($prefix=='/operate') in @endif">
                            <ul class="nav">
                                <li><a href="{{url('/operate/index')}}" class="@if($action=='index') active @endif">基本信息</a></li>
                                <li><a href="{{url('/operate/child_operate_list')}}" class="@if($action=='child_operate_list') active @endif">子运营中心列表</a></li>
                                <li><a href="{{url('/operate/income?status=0&type=1')}}" class="@if($action=='income') active @endif">我的收益</a></li>
                                <li><a href="{{url('/operate/withdraw_list')}}" class="@if($action=='withdraw_list'||$action=='withdraw_detail') active @endif">提现管理</a></li>
                            </ul>
                        </div>
                    </li>
                    {{--<li>--}}
                        {{--<a href="#pushQr" data-toggle="collapse" class="collapsed @if($prefix=='/pushQr') active @endif"><i class="lnr lnr-chart-bars"></i> <span>地推二维码管理</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>--}}
                        {{--<div id="pushQr" class="collapse @if($prefix=='/pushQr') in @endif">--}}
                            {{--<ul class="nav">--}}
                                {{--<li><a href="{{url('/pushQr/pushQr_order_list')}}" class="@if($action=='pushQr_order_list') active @endif">地推二维码申请列表</a></li>--}}
                                {{--<li><a href="{{url('/pushQr/pushQr_list')}}" class="@if($action=='pushQr_list') active @endif">地推二维码列表</a></li>--}}
                           {{--</ul>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="#seller" data-toggle="collapse" class="collapsed @if($prefix=='/pushQrSeller') active @endif"><i class="lnr lnr-chart-bars"></i> <span>商家管理</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>--}}
                        {{--<div id="seller" class="collapse @if($prefix=='/pushQrSeller') in @endif">--}}
                            {{--<ul class="nav">--}}
                                {{--<li><a href="{{url('/pushQrSeller/seller_list')}}" class="@if($action=='seller_list') active @endif">商家列表管理</a></li>--}}

                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="#team" data-toggle="collapse" class="collapsed @if($prefix=='/team') active @endif"><i class="lnr lnr-chart-bars"></i> <span>我的团队管理</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>--}}
                        {{--<div id="team" class="collapse @if($prefix=='/team') in @endif">--}}
                            {{--<ul class="nav">--}}
                                {{--<li><a href="{{url('/team/team_list')}}" class="@if($action=='team_list') active @endif">团队列表</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                </ul>
            </nav>
        </div>
    </div>
    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @yield('content')
           </div>
        </div>
    </div>
    <!-- END MAIN -->
    {{--<div class="clearfix"></div>--}}
    {{--<footer>--}}
        {{--<div class="container-fluid">--}}
            {{--<p class="copyright">Copyright &copy; 2017.Company name All rights reserved.</p>--}}
        {{--</div>--}}
    {{--</footer>--}}
</div>
<!-- END WRAPPER -->
<!-- Javascript -->
<script src="{{asset('home/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('home/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('home/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('home/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('home/vendor/chartist/js/chartist.min.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor/ueditor.config.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor/ueditor.all.min.js')}}"> </script>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('home/js/klorofil-common.js')}}"></script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor/lang/zh-cn/zh-cn.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('bootstrap/bootstrap-switch/bootstrap-switch.js')}}"> </script>
@yield('js')
<script type="text/javascript" charset="utf-8" src="{{asset('js/myself.js')}}"> </script>

</body>

</html>
