@extends('agent.layouts.'.$layouts)
@section('content')
    <div class="panel panel-headline">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">地推二维码申请订单详情</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 消费分润比例即用户消费后分给商家的比例,20表示20%</div>--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post" id="form"  action="{{url('/pushQr/bind_pushqr_submit')}}" class="form-horizontal">
                <div class="panel-body">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">订单编号 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->order_sn}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">收货人 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->contact_user or '暂无'}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">联系手机号 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->contact_mobile or '暂无'}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">收货地址 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->contact_address or '暂无'}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">申请数量 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->count or '暂无'}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">地推二维码编号 </label>
                        <div class="col-sm-10">
                            <label class="control-label">
                                @foreach($info->id_arr_group as $v)
                                    {{$v}} &nbsp;&nbsp;&nbsp;
                                @endforeach
                            </label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">申请时间 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->created_at or '暂无'}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">快递单号 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->shipping_number or '暂无'}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">状态 </label>
                        <div class="col-sm-10">
                            <label class="control-label">
                                @if($info->status==0)
                                    未处理
                                @elseif($info->status==1)
                                    已处理
                                @endif</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>




                    {{--<a href="{{url('/seller/pushqr_list')}}?id={{$_GET['id']}}&mobile={{$_GET['mobile']}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>--}}
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
@endsection