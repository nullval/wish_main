@extends('agent.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" />
@section('content')
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">商家入驻信息</h3>
            {{--<div class="hr-line-solid"></div>--}}
        </div>
    <div class="panel panel-headline">
        <ul id="myTab" class="nav nav-tabs">
            <li class="active" id="A1">
                <a href="#A" data-toggle="tab">
                    基础信息登记
                </a>
            </li>
            <li id="B1"><a href="#B"  data-toggle="tab">银行卡信息登记</a></li>
            <li id="C1"><a href="#C" data-toggle="tab">开通支付平台业务</a></li>

        </ul>


        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade in active" id="A">
                @if (!$errors->has('error') && !empty($errors->first()))
                    <div class="alert alert-danger"> 您填写的格式不正确，请到相应的步骤中查看错误信息和修正！</div>
                @endif
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
                    <form method="post"   action="{{url('/seller/seller_add_submit_step1')}}" class="form-horizontal" enctype='multipart/form-data'>
                        <div class="panel-body">
                            {{csrf_field()}}


                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="mobile"   placeholder="登录手机号" value="{{old('mobile')}}">
                                    @if ($errors->has('mobile'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            {{--<div class="form-group">--}}
                                {{--<label for="signin-text" class="col-sm-2 control-label">登录密码 </label>--}}
                                {{--<div class="col-sm-10">--}}
                                    {{--<input type="password" class="form-control" name="password"   placeholder="登录密码" value="{{old('password')}}">--}}
                                    {{--@if ($errors->has('password'))--}}
                                        {{--<span class="help-block m-b-none text-danger">{{ $errors->first('password') }}</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="clearfix"></div>--}}
                            {{--<div class="hr-line-dashed"></div>--}}

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户名称 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="merchantName"   placeholder="商户名称" value="{{old('merchantName')}}">
                                    @if ($errors->has('merchantName'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('merchantName') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户简称 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="shortName"   placeholder="商户简称" value="{{old('shortName')}}">
                                    @if ($errors->has('shortName'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('shortName') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户城市 </label>
                                <div class="col-sm-10">

                                    <input type="button" class="btn btn btn-info" id="btn" value="搜索" style="margin-bottom: 10px;" onclick="get_city()">
                                    <input ype="text" class="form-control" id="cityName1" placeholder="请输入城市" autocomplete="off" name="cityName1" style="width: 140px; margin-bottom: 10px; float: left"   style="margin-top: 10px">

                                    <select id="aa" name="city" class="form-control">
                                        @foreach($city_arr as $k=>$v)
                                            <option class="city_option" tip="{{$v->cityName}}" value="{{$v->cityCode}}"@if(old('city')==$v->cityCode) selected @endif>{{$v->cityName}}</option>
                                        @endforeach
                                    </select>
                                    {{--<select id="bb" name="city" class="form-control ">--}}
                                        {{--@foreach($city_arr as $k=>$v)--}}
                                            {{--<option class="city_option" tip="{{$v->cityName}}" value="{{$v->cityCode}}"@if(old('city')==$v->cityCode) selected @endif>{{$v->cityName}}</option>--}}
                                        {{--@endforeach--}}
                                    </select>


                                @if ($errors->has('city'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('city') }}</span>
                                    @endif

                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户地址 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="merchantAddress"   placeholder="商户地址" value="{{old('merchantAddress')}}">
                                    @if ($errors->has('merchantAddress'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('merchantAddress') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">客服电话 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="servicePhone"   placeholder="客服电话" value="{{old('servicePhone')}}">
                                    @if ($errors->has('servicePhone'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('servicePhone') }}</span>
                                    @endif
                                </div>
                            </div>
                            {{--<div class="clearfix"></div>--}}
                            {{--<div class="hr-line-dashed"></div>--}}

                            {{--<div class="form-group">--}}
                                {{--<label for="signin-text" class="col-sm-2 control-label">商户类型 </label>--}}
                                {{--<div class="col-sm-10">--}}
                                    {{--<select name="merchantType" class="form-control">--}}
                                        {{--<option value="00">公司商户</option>--}}
                                        {{--<option value="01">个体商户</option>--}}
                                    {{--</select>--}}
                                    {{--<span class="help-block m-b-none text-success">商户类型即为判断是否对公对私账户，如个体商户为对私账户</span>--}}

                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">经营类目 </label>
                                <div class="col-sm-10">
                                    <select name="category" class="form-control">
                                        @foreach($category_arr as $k=>$v)
                                            <option value="{{$v->categoryCode}}"@if(old('category')==$v->categoryCode) selected @endif>{{$v->categoryName}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('category') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">法人姓名 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="corpmanName"   placeholder="法人姓名" value="{{old('corpmanName')}}">
                                    @if ($errors->has('corpmanName'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('corpmanName') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">法人身份证 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="corpmanId"   placeholder="法人身份证" value="{{old('corpmanId')}}">
                                    @if ($errors->has('corpmanId'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('corpmanId') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">法人联系手机 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="corpmanMobile"   placeholder="法人联系手机" value="{{old('corpmanMobile')}}">
                                    @if ($errors->has('corpmanMobile'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('corpmanMobile') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>


                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">开户行全称 </label>
                                <div class="col-sm-10">
                                    <select name="bankName" id="bankName" class="form-control">
                                        <option value="0" >请选择银行</option>
                                        @foreach($bank_arr as $k=>$v)
                                            <option value="{{$v->bankName}}" tip="{{$v->bankCode}}"@if(old('bankName')==$v->bankName) selected @endif>{{$v->bankName}}</option>
                                        @endforeach
                                    </select>
                                    {{--<span class="second">--}}
                                        {{--<input type="text" name="makeupCo1" autocomplete="off" id="makeupCo1" class="makeinp form-control" onfocus="setfocus1(this)" oninput="setinput1(this);" placeholder="请选择或输入" value="{{old('makeupCo1')}}"/>--}}

                                        {{--<select name="bankName" id="bankName" onchange="changeF1(this)" size="10" class="form-control " style="display:none;">--}}
                                            {{--@foreach($bank_arr as $k=>$v)--}}
                                                {{--<option class="city_option" tip="{{$v->bankCode}}" value="{{$v->bankName}}"@if(old('makeupCo1')==$v->bankName) selected @endif>{{$v->bankName}}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</span>--}}
                                    <input type="hidden"  name="bankCode"value="{{old('bankCode')}}">
                                    @if ($errors->has('category'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('category') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>



                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">营业执照号 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="businessLicense"   placeholder="营业执照号" value="{{old('businessLicense')}}">
                                    @if ($errors->has('businessLicense'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('businessLicense') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">营业执照</label>
                                <ul id="warp">
                                    <li>
                                        <input type="hidden" value="" name="old_businessimage"/>
                                        <div style="text-align: left;" class="col-sm-4">
                                            <img id="imgShow_WU_FILE_0" width="100%"  src="{{asset('/images/timg.jpg')}}" />
                                        </div>
                                        <div class="col-sm-2" style="text-align: right;">
                                            <input type="file" id="up_img_WU_FILE_0" name="businessimage" />
                                            <input  type="hidden" id="uploadfile" name="businessimagebase64" />
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">开户银行 </label>
                                <div class="col-sm-10">
                                    <select id="bank_name" class="form-control" onchange="getbankNo()">
                                        <option >--请选择银行获取联行号--</option>
                                        @foreach($bank_list as $v)
                                            <option >{{$v->bank_name}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" class="form-control" readonly name="account_bank" id="account_bank"    placeholder="银行卡所属地区银行的联行号" value="{{old('account_bank')}}">
                                    @if ($errors->has('account_bank'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('account_bank') }}</span>
                                    @endif
                                </div>

                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">开户行账号 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="bankaccountNo"   placeholder="开户行账号" value="{{old('bankaccountNo')}}">
                                    @if ($errors->has('bankaccountNo'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountNo') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">开户户名 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="bankaccountName"   placeholder="开户户名" value="{{old('bankaccountName')}}">
                                    @if ($errors->has('bankaccountName'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountName') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>



                            {{--<div class="form-group">--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label">银行卡所属地区银行联行号 </label>--}}
                            {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" readonly name="ibankno"  id="ibankno"  placeholder="银行卡所属地区银行的联行号" value="{{old('ibankno')}}">--}}
                            {{--<span class="help-block m-b-none text-danger">请通过选定银行名字获取联行号</span>--}}
                            {{--@if ($errors->has('ibankno'))--}}
                            {{--<span class="help-block m-b-none text-danger">{{ $errors->first('ibankno') }}</span>--}}
                            {{--@endif--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="clearfix"></div>--}}
                            {{--<div class="hr-line-dashed"></div>--}}



                            <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                        </div>
                    </form>
            </div>
            <div class="tab-pane fade" id="B">
                <form method="post"   action="{{url('/seller/seller_add_submit_step2')}}" class="form-horizontal" enctype='multipart/form-data'>
                    <div class="panel-body">
                        {{csrf_field()}}


                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="mobile1"   placeholder="商家手机号" value="{{old('mobile1')}}">
                                @if ($errors->has('mobile1'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('mobile1') }}</span>
                                @endif
                            </div>
                        </div>
                        {{--<div class="clearfix"></div>--}}
                        {{--<div class="hr-line-dashed"></div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label">登录密码 </label>--}}
                            {{--<div class="col-sm-10">--}}
                                {{--<input type="password" class="form-control" name="password"   placeholder="登录密码" value="{{old('password')}}">--}}
                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block m-b-none text-danger">{{ $errors->first('password') }}</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                        {{--<div class="hr-line-dashed"></div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label">子商户编码 </label>--}}
                            {{--<div class="col-sm-10">--}}
                                {{--<input type="text" class="form-control" name="merchantId1"   placeholder="即法人手机号码" value="{{old('merchantId1')}}">--}}
                                {{--@if ($errors->has('merchantId1'))--}}
                                    {{--<span class="help-block m-b-none text-danger">{{ $errors->first('merchantId1') }}</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">开户行全称 </label>
                            <div class="col-sm-10">
                                <select name="bankName1" id="bankName1" class="form-control">
                                    <option value="0" >请选择银行</option>
                                    @foreach($bank_arr as $k=>$v)
                                        <option value="{{$v->bankName}}" tip="{{$v->bankCode}}"@if(old('bankName1')==$v->bankName) selected @endif>{{$v->bankName}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" value="{{old('bankCode1')}}" name="bankCode1"/>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">账号属性 </label>
                            <div class="col-sm-10" style="padding-top: 8px">
                                <input type="radio" class="control-label" name="bankaccProp" value="0" @if(old('bankaccProp')==0) checked @endif/> 私人
                                <input type="radio" class="control-label" name="bankaccProp" value="1" @if(old('bankaccProp')==1) checked @endif/> 公司
                                @if ($errors->has('bankaccProp'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccProp') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">银行卡类型 </label>
                            <div class="col-sm-10" style="padding-top: 8px">
                                <input type="radio" class="control-label" name="bankaccountType" value="1" @if(old('bankaccountType')==1) checked @endif/> 借记卡
                                <input type="radio" class="control-label" name="bankaccountType" value="2" @if(old('bankaccountType')==2) checked @endif/> 贷记卡
                                {{--<input type="radio" class="control-label" name="bankaccountType" value="3" /> 存折--}}
                                @if ($errors->has('bankaccountType'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountType') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">持卡人姓名 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name"   placeholder="持卡人姓名" value="{{old('name')}}">
                                @if ($errors->has('name'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">银行卡号 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="bankaccountNo"   placeholder="银行卡号" value="{{old('bankaccountNo')}}">
                                @if ($errors->has('bankaccountNo'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountNo') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">身份证号码 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="certNo"   placeholder="身份证号码" value="{{old('certNo')}}">
                                @if ($errors->has('certNo'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('certNo') }}</span>
                                @endif
                            </div>
                        </div>


                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">银行联行号的选择范围 </label>
                            <div class="col-sm-10">
                                <select id="bank_name" class="form-control" onchange="getbankNo()">
                                    <option >--请选择银行获取联行号--</option>
                                    @foreach($bank_list as $v)
                                    <option >{{$v->bank_name}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" class="form-control" readonly name="account_bank" id="account_bank"    placeholder="银行卡所属地区银行的联行号" value="{{old('account_bank')}}">
                                @if ($errors->has('account_bank'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('account_bank') }}</span>
                                @endif
                            </div>

                        </div>


                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>



                        <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">银行卡所属地区银行联行号 </label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control" readonly name="ibankno"  id="ibankno"  placeholder="银行卡所属地区银行的联行号" value="{{old('ibankno')}}">
                        <span class="help-block m-b-none text-danger">请通过选定银行名字获取联行号</span>
                        @if ($errors->has('ibankno'))
                        <span class="help-block m-b-none text-danger">{{ $errors->first('ibankno') }}</span>
                        @endif
                        </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>



                        <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="C">
                <div class="alert alert-warning"> 费率即用户消费后分给平台的比例,20表示给平台的折扣为20%,设置的费率不能低于10</div>
                <form method="post"   action="{{url('/seller/seller_add_submit_step3')}}" class="form-horizontal" enctype='multipart/form-data'>
                    <div class="panel-body">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="mobile2"   placeholder="商家手机号" value="{{old('mobile2')}}">
                                @if ($errors->has('mobile2'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('mobile2') }}</span>
                                @endif
                            </div>
                        </div>
                        {{--<div class="clearfix"></div>--}}
                        {{--<div class="hr-line-dashed"></div>--}}

                        {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">登录密码 </label>--}}
                        {{--<div class="col-sm-10">--}}
                        {{--<input type="password" class="form-control" name="password"   placeholder="登录密码" value="{{old('password')}}">--}}
                        {{--@if ($errors->has('password'))--}}
                        {{--<span class="help-block m-b-none text-danger">{{ $errors->first('password') }}</span>--}}
                        {{--@endif--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">子商户编码 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="merchantId2"   placeholder="即法人手机号码" value="{{old('merchantId2')}}">
                                @if ($errors->has('merchantId2'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('merchantId2') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">费率 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="futureRate"   placeholder="费率" value="{{old('futureRate')}}">
                                @if ($errors->has('futureRate'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('futureRate') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>



                        <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                    </div>
                </form>
            </div>
            </div>

        </div>


    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
<script>
    $(document).ready(function(){
        $('#bankName').change(function(){
            $('input[name=bankCode]').val($('#bankName option:selected').attr('tip'));

        });
        $('#bankName1').change(function(){
            $('input[name=bankCode1]').val($('#bankName1 option:selected').attr('tip'));

        });
        $("#up_img_WU_FILE_0").change(function(){
            var v = $(this).val();
            var reader = new FileReader();
            reader.readAsDataURL(this.files[0]);
            reader.onload = function(e){
                console.log(e.target.result);
                $('#uploadfile').val(e.target.result);
            };
        });
    });
    function getbankNo() {
        var bank_name=$("#bank_name").val();
        $.ajax({
            url:'{{url('/seller/get_bankNo')}}',
            type:'POST', //GET
            async:true,    //或false,是否异步
            data:{
                bank_name:bank_name
            },
            timeout:5000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success:function(data){

                if (data.code==0){
                    alert(data.message)
                }else {
                    $('#ibankno').val(data.data.lbnkNo);
                    $('#account_bank').val(data.data.bank_name);
                }
            },
            error:function(xhr,textStatus){
                console.log('错误')
                console.log(xhr)
                console.log(textStatus)
            },

        })

    }
    function get_city(){
        cityName = $("#cityName1").val();
        val=$(".city_option[tip^="+cityName+"]").val();
        $('#aa').val(val);
    }

</script>

@endsection