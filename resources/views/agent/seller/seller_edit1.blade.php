@extends('agent.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" />
@section('content')
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">商家入驻信息编辑</h3>
            {{--<div class="hr-line-solid"></div>--}}
        </div>
    <div class="panel panel-headline">
        <ul id="myTab" class="nav nav-tabs">
            <li class="active">
                <a href="#A" data-toggle="tab">
                    基础信息登记
                </a>
            </li>
            <li><a href="#B"  data-toggle="tab">银行卡信息登记</a></li>
            <li><a href="#C" data-toggle="tab">开通支付平台业务</a></li>

        </ul>


        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade in active" id="A">
                @if (!$errors->has('error') && !empty($errors->first()))
                    <div class="alert alert-danger"> 您填写的格式不正确，请到相应的步骤中查看错误信息和修正！</div>
                @endif
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
                    <form method="post"   action="{{url('/seller/seller_edit_submit_step1')}}" class="form-horizontal" enctype='multipart/form-data'>
                        <div class="panel-body">
                            {{csrf_field()}}


                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">登录手机号 </label>
                                <div class="col-sm-10">
                                    {{--<label class="control-label">{{$seller_info->mobile}}</label>--}}
                                    <input type="text" class="form-control" name="mobile" readonly  placeholder="商家手机号" value="{{$seller_info->mobile or old('mobile')}}">
                                    <input type="hidden" name="uid" value="{{$seller_info->id}}">
                                    @if ($errors->has('mobile'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            {{--<div class="form-group">--}}
                                {{--<label for="signin-text" class="col-sm-2 control-label">登录密码 </label>--}}
                                {{--<div class="col-sm-10">--}}
                                    {{--<input type="password" class="form-control" name="password"   placeholder="登录密码" value="{{old('password')}}">--}}
                                    {{--@if ($errors->has('password'))--}}
                                        {{--<span class="help-block m-b-none text-danger">{{ $errors->first('password') }}</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="clearfix"></div>--}}
                            {{--<div class="hr-line-dashed"></div>--}}

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户名称 </label>
                                <div class="col-sm-10">
                                    {{--<label class="control-label">{{$basic_info->merchantName}}</label>--}}
                                    <input type="text" class="form-control" name="merchantName" @if($status['basic'] == 1) readonly @endif placeholder="商户名称" value="{{$basic_info->merchantName or old('merchantName')}}">
                                    @if ($errors->has('merchantName'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('merchantName') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户简称 </label>
                                <div class="col-sm-10">
                                    {{--<label class="control-label">{{$basic_info->shortName}}</label>--}}

                                    <input type="text" class="form-control"  name="shortName" @if($status['basic'] == 1) readonly @endif  placeholder="商户简称" value="{{$basic_info->shortName or old('shortName')}}">
                                    @if ($errors->has('shortName'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('shortName') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户城市</label>
                                <div class="col-sm-10">
                                    @if($status['basic'] == 1)
                                        @foreach($city_arr as $k=>$v)
                                            @if ($basic_info->city==$v->cityCode )
                                                <input type="text" class="form-control"  name="cityName"  readonly placeholder="商户城市" value="{{$v->cityName or old('cityName')}}">
                                            @endif
                                        @endforeach
                                    @else
                                        <input type="button" class="btn btn btn-info" id="btn" value="搜索" style="margin-bottom: 10px;" onclick="get_city()">
                                        <input ype="text" class="form-control" id="cityName1" placeholder="请输入城市" autocomplete="off" name="cityName1" style="width: 140px; margin-bottom: 10px;float:left;"   style="margin-top: 10px">
                                        <select id="aa" name="city" class="form-control">
                                            @foreach($city_arr as $k=>$v)
                                                <option class="city_option" tip="{{$v->cityName}}" value="{{$v->cityCode}}"
                                                        @if(!empty($basic_info))
                                                            @if($basic_info->city==$v->cityCode)
                                                                selected
                                                            @endif
                                                        @endif
                                                >{{$v->cityName}}</option>
                                            @endforeach
                                        </select>


                                    @endif
                                    @if ($errors->has('city'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('city') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户地址 </label>
                                <div class="col-sm-10">
                                    {{--<label class="control-label">{{$basic_info->merchantAddress}}</label>--}}
                                    <input type="text" class="form-control" name="merchantAddress" @if($status['basic'] == 1) readonly @endif   placeholder="商户地址" value="{{$basic_info->merchantAddress or old('merchantAddress')}}">
                                    @if ($errors->has('merchantAddress'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('merchantAddress') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">客服电话 </label>
                                <div class="col-sm-10">
{{--                                    <label class="control-label">{{$basic_info->servicePhone}}</label>--}}
                                    <input type="text" class="form-control" name="servicePhone" @if($status['basic'] == 1) readonly @endif   placeholder="客服电话" value="{{$basic_info->servicePhone or old('servicePhone')}}">
                                    @if ($errors->has('servicePhone'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('servicePhone') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户类型 </label>
                                <div class="col-sm-10">
                                    @if($status['basic'] == 1)
                                        <input type="text" class="form-control"  name="merchantType"  readonly placeholder="商户类型" value="个体商户">
                                    @else
                                        <select name="merchantType" class="form-control">
                                            {{--<option value="00"@if(!empty($basic_info)) @if($basic_info->merchantType=='00') selected @endif @endif>公司商户</option>--}}
                                            <option value="01"@if(!empty($basic_info)) @if($basic_info->merchantType=='01') selected @endif @endif>个体商户</option>
                                        </select>
                                    @endif

                                    <span class="help-block m-b-none text-success">商户类型即为判断是否对公对私账户，如个体商户为对私账户</span>

                                </div>

                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">经营类目 </label>
                                <div class="col-sm-10">

                                    @if($status['basic'] == 1)
                                        @foreach($category_arr as $k=>$v)
                                            @if ($basic_info->category==$v->categoryCode)
                                                <input type="text" class="form-control"  name="category"  readonly placeholder="经营类目" value="{{$v->categoryName or old('category')}}">
                                            @endif
                                        @endforeach
                                    @else
                                        <select  name="category" class="form-control">
                                            @foreach($category_arr as $k=>$v)
                                                <option value="{{$v->categoryCode}}"
                                                        @if(!empty($basic_info))
                                                            @if ($basic_info->category==$v->categoryCode)
                                                                selected
                                                            @endif
                                                        @endif
                                                >{{$v->categoryName}}</option>
                                            @endforeach
                                        </select>
                                    @endif


                                    @if ($errors->has('category'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('category') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">法人姓名 </label>
                                <div class="col-sm-10">
                                    {{--<label class="control-label">{{$basic_info->corpmanName}}</label>--}}
                                    <input type="text" class="form-control" name="corpmanName" @if($status['basic'] == 1) readonly @endif  placeholder="法人姓名" value="{{$basic_info->corpmanName or old('corpmanName')}}">
                                    @if ($errors->has('corpmanName'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('corpmanName') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">法人身份证 </label>
                                <div class="col-sm-10">
                                    {{--<label class="control-label">{{$basic_info->corpmanId}}</label>--}}
                                    <input type="text" class="form-control" name="corpmanId" @if($status['basic'] == 1) readonly @endif   placeholder="法人身份证" value="{{$basic_info->corpmanId or old('corpmanId')}}">
                                    @if ($errors->has('corpmanId'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('corpmanId') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">法人联系手机 </label>
                                <div class="col-sm-10">
{{--                                    <label class="control-label">{{$basic_info->corpmanMobile}}</label>--}}

                                    <input type="text" class="form-control" name="corpmanMobile"@if($status['basic'] == 1) readonly @endif  placeholder="法人联系手机" value="{{$basic_info->corpmanMobile or old('corpmanMobile')}}">
                                    @if ($errors->has('corpmanMobile'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('corpmanMobile') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>


                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">开户行全称 </label>
                                <div class="col-sm-10">
                                    @if($status['basic'] == 1)
                                        @foreach($bank_arr as $k=>$v)
                                            @if ($basic_info->bankCode==$v->bankCode)
                                                <input type="text" class="form-control"  name="bankName"  readonly placeholder="开户行全称" value="{{$v->bankName or old('bankName')}}">
                                            @endif
                                        @endforeach
                                    @else
                                        <select id="aa" name="bankName" id="bankName" class="form-control">
                                            <option value="0" >请选择银行</option>
                                            @foreach($bank_arr as $k=>$v)
                                                <option value="{{$v->bankName}}" tip="{{$v->bankCode}}"
                                                        @if(!empty($basic_info))
                                                        @if ($basic_info->bankCode==$v->bankCode)
                                                        selected
                                                        @endif
                                                        @endif
                                                >{{$v->bankName}}</option>
                                            @endforeach
                                        </select>
                                        <input type="hidden" value="{{$basic_info->bankCode or old('bankCode')}}" name="bankCode"/>
                                    @endif

                                    @if ($errors->has('category'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('category') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>



                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">营业执照号 </label>
                                <div class="col-sm-10">
                                    {{--<label class="control-label">{{$basic_info->businessLicense}}</label>--}}

                                    <input type="text" class="form-control" name="businessLicense" @if($status['basic'] == 1) readonly @endif  placeholder="营业执照号" value="{{ $basic_info->businessLicense or old('businessLicense')}}">
                                    @if ($errors->has('businessLicense'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('businessLicense') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">营业执照</label>
                                @if($status['basic'] == 1)
                                    <div style="text-align: left;" class="col-sm-4">
                                        <img id="imgShow_WU_FILE_0" width="100%"  src="{{$basic_info->businessimage or asset('/images/timg.jpg')}}" />
                                    </div>
                                @else
                                    <ul id="warp">
                                        <li>
                                            <input type="hidden" value="" name="old_businessimage"/>
                                            <div style="text-align: left;" class="col-sm-4">
                                                <img id="imgShow_WU_FILE_0" width="100%"  src="{{$basic_info->businessimage or asset('/images/timg.jpg')}}" />
                                            </div>
                                            <div class="col-sm-2" style="text-align: right;">
                                                <input type="file" id="up_img_WU_FILE_0" name="businessimage" />
                                                <input  type="hidden" id="uploadfile" name="businessimagebase64" />
                                                <input  type="hidden" name="businessimage" value="{{$basic_info->businessimage or old('businessimage')}}" />
                                            </div>
                                        </li>
                                    </ul>
                                @endif


                            </div>
                            {{--<div class="clearfix"></div>--}}
                            {{--<div class="hr-line-dashed"></div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="signin-text" class="col-sm-2 control-label">开户银行 </label>--}}
                                {{--<div class="col-sm-10">--}}
                                    {{--<select id="bank_name" class="form-control" onchange="getbankNo()">--}}
                                        {{--<option >--请选择银行获取联行号--</option>--}}
                                        {{--@foreach($bank_list as $v)--}}
                                            {{--<option >{{$v->bank_name}}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                    {{--<input type="hidden" class="form-control" readonly name="account_bank" id="account_bank"    placeholder="银行卡所属地区银行的联行号" value="{{old('account_bank')}}">--}}
                                    {{--@if ($errors->has('account_bank'))--}}
                                        {{--<span class="help-block m-b-none text-danger">{{ $errors->first('account_bank') }}</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}

                            {{--</div>--}}
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">开户行账号 </label>
                                <div class="col-sm-10">
{{--                                    <label class="control-label">{{$basic_info->bankaccountNo}}</label>--}}
                                    <input type="text" class="form-control" name="bankaccountNo" @if($status['basic'] == 1) readonly @endif  placeholder="开户行账号" value="{{ $basic_info->bankaccountNo or old('bankaccountNo')}}">
                                    @if ($errors->has('bankaccountNo'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountNo') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">开户户名 </label>
                                <div class="col-sm-10">
                                    {{--<label class="control-label">{{$basic_info->bankaccountName}}</label>--}}

                                    <input type="text" class="form-control" name="bankaccountName" @if($status['basic'] == 1) readonly @endif  placeholder="开户户名" value="{{$basic_info->bankaccountName or old('bankaccountName')}}">
                                    @if ($errors->has('bankaccountName'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountName') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>



                            {{--<div class="form-group">--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label">银行卡所属地区银行联行号 </label>--}}
                            {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" readonly name="ibankno"  id="ibankno"  placeholder="银行卡所属地区银行的联行号" value="{{old('ibankno')}}">--}}
                            {{--<span class="help-block m-b-none text-danger">请通过选定银行名字获取联行号</span>--}}
                            {{--@if ($errors->has('ibankno'))--}}
                            {{--<span class="help-block m-b-none text-danger">{{ $errors->first('ibankno') }}</span>--}}
                            {{--@endif--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="clearfix"></div>--}}
                            {{--<div class="hr-line-dashed"></div>--}}

                            @if($status['basic'] == 0)
                                <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 修改</button>
                            @endif

                        </div>
                    </form>
            </div>
            <div class="tab-pane fade" id="B">
                <form method="post"   action="{{url('/seller/seller_edit_submit_step2')}}" class="form-horizontal" enctype='multipart/form-data'>
                    <div class="panel-body">
                        {{csrf_field()}}


                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="mobile1"  readonly  placeholder="商家手机号" value="{{$seller_info->mobile or old('mobile1')}}">
                                <input type="hidden" value="{{$seller_info->id}}" name="uid"/>
                                @if ($errors->has('mobile1'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('mobile1') }}</span>
                                @endif
                            </div>
                        </div>
                        {{--<div class="clearfix"></div>--}}
                        {{--<div class="hr-line-dashed"></div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label">登录密码 </label>--}}
                            {{--<div class="col-sm-10">--}}
                                {{--<input type="password" class="form-control" name="password"   placeholder="登录密码" value="{{old('password')}}">--}}
                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block m-b-none text-danger">{{ $errors->first('password') }}</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">子商户编码 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="merchantId1" @if($status['bank'] == 1) readonly @endif  placeholder="即法人手机号" value="{{$basic_info->merchantId or old( 'merchantId1')}}">

                                @if ($errors->has('merchantId1'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('merchantId1') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">开户行全称 </label>
                            <div class="col-sm-10">
                                @if($status['bank'] == 1)
                                    @foreach($bank_arr as $k=>$v)
                                        @if(isset($bank_info))
                                            @if($bank_info->bankCode==$v->bankCode)
                                                <input type="text" class="form-control" name="bankName1"  readonly placeholder="开户行全称" value="{{$v->bankName or old( 'bankName1')}}">
                                            @endif
                                        @endif
                                    @endforeach

                                @else
                                    <select name="bankName1" id="bankName1" class="form-control">
                                        <option>--请选择银行--</option>
                                        @foreach($bank_arr as $k=>$v)
                                            <option value="{{$v->bankName}}" tip="{{$v->bankCode}}"
                                                    @if(isset($bank_info))
                                                        @if($bank_info->bankCode==$v->bankCode)
                                                            selected
                                                        @endif
                                                    @endif>{{$v->bankName}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" value="@if(isset($bank_info)){{$bank_info->bankCode}} @endif" name="bankCode1"/>
                                @endif

                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">账号属性 </label>
                            <div class="col-sm-10" style="padding-top: 8px">
                                @if($status['bank'] == 1)
                                    @if($bank_info->bankaccProp==0)
                                        <input type="text" class="form-control"   readonly value="私人">
                                    @elseif($bank_info->bankaccProp==1)
                                        <input type="text" class="form-control"   readonly value="公司">
                                    @endif
                                @else
                                    <input type="radio" class="control-label" name="bankaccProp" value="0"@if(isset($bank_info)) @if($bank_info->bankaccProp==0) checked @endif @endif/> 私人
                                    <input type="radio" class="control-label" name="bankaccProp" value="1"@if(isset($bank_info)) @if($bank_info->bankaccProp==1) checked @endif @endif /> 公司
                                @endif
                                @if ($errors->has('bankaccProp'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccProp') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">银行卡信息 </label>
                            <div class="col-sm-10" style="padding-top: 8px">
                                @if($status['bank'] == 1)
                                    @if($bank_info->bankaccountType==1)
                                        <input type="text" class="form-control"   readonly value="借记卡">
                                    @elseif($bank_info->bankaccountType==2)
                                        <input type="text" class="form-control"   readonly value="贷记卡">
                                    @endif
                                @else
                                    <input type="radio" class="control-label" name="bankaccountType" value="1"@if(isset($bank_info)) @if($bank_info->bankaccountType==1) checked @endif @endif/> 借记卡
                                    <input type="radio" class="control-label" name="bankaccountType" value="2"@if(isset($bank_info)) @if($bank_info->bankaccountType==2) checked @endif @endif /> 贷记卡
                                    {{--<input type="radio" class="control-label" name="bankaccountType" value="3"@if(isset($bank_info)) @if($bank_info->bankaccountType==3) checked @endif @endif /> 存折--}}
                                @endif

                                @if ($errors->has('bankaccountType'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountType') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">持卡人姓名 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" @if($status == 1) readonly @endif  placeholder="持卡人姓名" value="{{$bank_info->name or old('name')}}">
                                @if ($errors->has('name'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">银行卡号 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="bankaccountNo"  @if($status == 1) readonly @endif placeholder="银行卡号" value="{{$bank_info->bankaccountNo or old('bankaccountNo')}}">
                                @if ($errors->has('bankaccountNo'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountNo') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">身份证号码 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="certNo" @if($status == 1) readonly @endif  placeholder="身份证号码" value="{{$bank_info->certNo or old('certNo')}}">
                                @if ($errors->has('certNo'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('certNo') }}</span>
                                @endif
                            </div>
                        </div>

                        @if($status['bank'] == 0)
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">银行联行号的选择范围 </label>
                                <div class="col-sm-10">
                                    <select id="bank_name" class="form-control" onchange="getbankNo()">
                                        <option >--请选择银行获取联行号--</option>
                                        @foreach($bank_list as $v)
                                            <option @if($bank_info->bankbranchNo==$v->lbnkNo) selected @endif >{{$v->bank_name}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" class="form-control" readonly name="account_bank" id="account_bank" value="{{$bank_info->account_bank or old('account_bank')}}">
                                    @if ($errors->has('account_bank'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('account_bank') }}</span>
                                    @endif
                                </div>

                            </div>

                        @endif


                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>



                        <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">银行卡所属地区银行联行号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" readonly name="ibankno"  id="ibankno"  placeholder="银行卡所属地区银行的联行号" value="{{$bank_info->bankbranchNo or old('ibankno')}}">
                            <span class="help-block m-b-none text-danger">请通过选定银行名字获取联行号</span>
                            @if ($errors->has('ibankno'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('ibankno') }}</span>
                            @endif
                        </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        @if($status['bank'] == 0)
                            <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 修改</button>
                        @endif

                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="C">
                <div class="alert alert-warning"> 费率即用户消费后分给平台的比例,按百分比计算,20表示给平台的折扣为20%,设置的费率不能低于10</div>
                <form method="post"   action="{{url('/seller/seller_edit_submit_step3')}}" class="form-horizontal" enctype='multipart/form-data'>
                    <div class="panel-body">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="mobile2"  readonly placeholder="商家手机号" value="{{$seller_info->mobile or old('mobile2')}}">
                                <input type="hidden" value="{{$seller_info->id}}" name="uid"/>
                                @if ($errors->has('mobile2'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('mobile2') }}</span>
                                @endif
                            </div>
                        </div>
                        {{--<div class="clearfix"></div>--}}
                        {{--<div class="hr-line-dashed"></div>--}}

                        {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">登录密码 </label>--}}
                        {{--<div class="col-sm-10">--}}
                        {{--<input type="password" class="form-control" name="password"   placeholder="登录密码" value="{{old('password')}}">--}}
                        {{--@if ($errors->has('password'))--}}
                        {{--<span class="help-block m-b-none text-danger">{{ $errors->first('password') }}</span>--}}
                        {{--@endif--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">子商户编码 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="merchantId2"   @if($status['busi'] == 1) readonly @endif placeholder="即法人手机号码" value="{{$basic_info->corpmanMobile or old('merchantId2')}}">
                                @if ($errors->has('merchantId2'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('merchantId2') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">费率 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="futureRate" @if($status['busi'] == 1) readonly @endif  placeholder="费率" value="{{$busi_info->futureRateValue or  old('futureRate')}}">
                                @if ($errors->has('futureRate'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('futureRate') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        </div>
                        <div class="panel-body no-padding">
                        @if($status['busi'] == 0)
                            @if($_GET['uid']!=54)
                                <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 修改</button>
                            @endif
                        @endif

                    </div>
                </form>
            </div>
            </div>

        </div>


    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
<script>
    $(document).ready(function(){

        $('#bankName').change(function(){
            $('input[name=bankCode]').val($('#bankName option:selected').attr('tip'));

        });
        $('#bankName1').change(function(){
            $('input[name=bankCode1]').val($('#bankName1 option:selected').attr('tip'));

        });
        $("#up_img_WU_FILE_0").change(function(){
            var v = $(this).val();
            var reader = new FileReader();
            reader.readAsDataURL(this.files[0]);
            reader.onload = function(e){
                console.log(e.target.result);
                $('#uploadfile').val(e.target.result);
            };
        });
    });
    function getbankNo() {
        var bank_name=$("#bank_name").val();
        $.ajax({
            url:'{{url('/seller/get_bankNo')}}',
            type:'POST', //GET
            async:true,    //或false,是否异步
            data:{
                bank_name:bank_name
            },
            timeout:5000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success:function(data){

                if (data.code==0){
//                    alert(data.message)
                    $('#ibankno').val('0');
                    $('#account_bank').val("");
                }else {
                    $('#ibankno').val(data.data.lbnkNo);
                    $('#account_bank').val(data.data.bank_name);
                }
            },
            error:function(xhr,textStatus){
                console.log('错误')
                console.log(xhr)
                console.log(textStatus)
            },

        })

    }
    function get_city(){
        cityName = $("#cityName1").val();
        val=$(".city_option[tip^="+cityName+"]").val();
        $('#aa').val(val);
    }

</script>
@endsection