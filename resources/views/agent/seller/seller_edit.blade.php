@extends('agent.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" />
<link href="{{asset('sanji/css/main.css')}}" rel="stylesheet">
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">编辑商家入驻信息</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/seller/seller_edit_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                        <div class="col-sm-10">
                            {{--<label class="control-label">{{$seller_info->mobile}}</label>--}}
                            <input type="text" class="form-control" name="mobile" readonly   placeholder="商家手机号" value="{{$seller_info->mobile or old('mobile')}}">
                            <input type="hidden" name="uid" value="{{$seller_info->id}}">
                            @if ($errors->has('mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商户名称 </label>
                        <div class="col-sm-10">
                            {{--<label class="control-label">{{$basic_info->merchantName}}</label>--}}
                            <input type="text" class="form-control" name="merchantName" @if($status == 1) readonly @endif placeholder="商户名称" value="{{$basic_info->merchantName or old('merchantName')}}">
                            @if ($errors->has('merchantName'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('merchantName') }}</span>
                            @endif
                        </div>
                    </div>
    
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
    
    
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">POS机终端号 </label>
                        <div class="col-sm-10">
                            {{--<label class="control-label">{{$basic_info->merchantName}}</label>--}}
                            <input type="text" class="form-control" name="terminalId" readonly @if($status == 1) readonly @endif placeholder="POS机终端号" value="{{$basic_info->terminalId or old('terminalId')}}">
                            @if ($errors->has('terminalId'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('terminalId') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商户城市</label>
                        <div class="col-sm-10">
                            @if($status == 1)
                                @foreach($city_arr as $k=>$v)
                                    @if ($basic_info->city==$v->cityCode )
                                        <input type="text" class="form-control"  name="cityName"  readonly placeholder="商户城市" value="{{$v->cityName or old('cityName')}}">
                                    @endif
                                @endforeach
                            @else
                                <input type="button" class="btn btn btn-info" id="btn" value="搜索" style="margin-bottom: 10px;" onclick="get_city()">
                                <input ype="text" class="form-control" id="cityName1" placeholder="请输入城市" autocomplete="off" name="cityName1" style="width: 140px; margin-bottom: 10px;float:left;"   style="margin-top: 10px">
                                <select id="aa" name="city" class="form-control">
                                    @foreach($city_arr as $k=>$v)
                                        <option class="city_option" tip="{{$v->cityName}}" value="{{$v->cityCode}}"
                                                @if(!empty($basic_info))
                                                    @if($basic_info->city==$v->cityCode)
                                                        selected
                                                    @endif
                                                @endif
                                        >{{$v->cityName}}</option>
                                    @endforeach
                                </select>


                            @endif
                            @if ($errors->has('city'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('city') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">省市区选择</label>
                        <div class="col-sm-10 form-inline">

                            <div id="distpicker5">
                                <div class="form-group">
                                    <label class="sr-only" for="province10">Province</label>
                                    <select class="form-control" id="province10" name="province10" onchange="get_province_code()"></select>
                                </div>
                                <input type="hidden" name="province_code" id="province_code">
                                <div class="form-group">
                                    <label class="sr-only" for="city10">City</label>
                                    <select class="form-control" id="city10" name="city10" onchange="get_city_code()"></select>
                                </div>
                                <input type="hidden" name="city_code" id="city_code">

                                <div class="form-group">
                                    <label class="sr-only" for="district10">District</label>
                                    <select class="form-control" id="district10" name="district10" onchange="get_district_code()"></select>
                                </div>
                                <input type="hidden" name="district_code" id="district_code">

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">详细地址 </label>
                        <div class="col-sm-10">
                            {{--<label class="control-label">{{$basic_info->merchantAddress}}</label>--}}
                            <input type="text" class="form-control" name="merchantAddress" id="merchantAddress" @if($status == 1) readonly @endif   placeholder="商户地址" value="{{$basic_info->merchantAddress or old('merchantAddress')}}">
                            @if ($errors->has('merchantAddress'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('merchantAddress') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">经营类目 </label>
                        <div class="col-sm-10">

                            @if($status == 1)
                                @foreach($category_arr as $k=>$v)
                                    @if ($basic_info->category==$v->categoryCode)
                                        <input type="text" class="form-control"  name="category"  readonly placeholder="经营类目" value="{{$v->categoryName or old('category')}}">
                                    @endif
                                @endforeach
                            @else
                                <select  name="category" class="form-control">
                                    @foreach($category_arr as $k=>$v)
                                        <option value="{{$v->categoryCode}}"
                                                @if(!empty($basic_info))
                                                    @if ($basic_info->category==$v->categoryCode)
                                                        selected
                                                    @endif
                                                @endif
                                        >{{$v->categoryName}}</option>
                                    @endforeach
                                </select>
                            @endif


                            @if ($errors->has('category'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('category') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">客服电话 </label>
                        <div class="col-sm-10">
                            {{--                                    <label class="control-label">{{$basic_info->servicePhone}}</label>--}}
                            <input type="text" class="form-control" name="servicePhone" @if($status == 1) readonly @endif   placeholder="客服电话" value="{{$basic_info->servicePhone or old('servicePhone')}}">
                            @if ($errors->has('servicePhone'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('servicePhone') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">法人姓名 </label>
                        <div class="col-sm-10">
                            {{--<label class="control-label">{{$basic_info->corpmanName}}</label>--}}
                            <input type="text" class="form-control" name="corpmanName" @if($status == 1) readonly @endif  placeholder="法人姓名" value="{{$basic_info->corpmanName or old('corpmanName')}}">
                            @if ($errors->has('corpmanName'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('corpmanName') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">法人身份证 </label>
                        <div class="col-sm-10">
                            {{--<label class="control-label">{{$basic_info->corpmanId}}</label>--}}
                            <input type="text" class="form-control" name="corpmanId" @if($status == 1) readonly @endif   placeholder="法人身份证" value="{{$basic_info->corpmanId or old('corpmanId')}}">
                            @if ($errors->has('corpmanId'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('corpmanId') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">法人联系手机 </label>
                        <div class="col-sm-10">
                            {{--                                    <label class="control-label">{{$basic_info->corpmanMobile}}</label>--}}

                            <input type="text" class="form-control" name="corpmanMobile"readonly  placeholder="法人联系手机" value="{{$basic_info->corpmanMobile or old('corpmanMobile')}}">
                            @if ($errors->has('corpmanMobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('corpmanMobile') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>



                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">营业执照号 </label>
                        <div class="col-sm-10">
                            {{--<label class="control-label">{{$basic_info->businessLicense}}</label>--}}

                            <input type="text" class="form-control" name="businessLicense" @if($status == 1) readonly @endif  placeholder="营业执照号" value="{{ $basic_info->businessLicense or old('businessLicense')}}">
                            @if ($errors->has('businessLicense'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('businessLicense') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">营业执照</label>
                        @if($status == 1)
                            <div style="text-align: left;" class="col-sm-4">
                                <img id="imgShow_WU_FILE_0" width="100%"  src="{{$basic_info->businessimage or asset('/images/timg.jpg')}}" />
                            </div>
                        @else
                            <ul id="warp">
                                <li>
                                    <input type="hidden" value="" name="old_businessimage"/>
                                    <div style="text-align: left;" class="col-sm-4">
                                        <img id="imgShow_WU_FILE_0" width="100%"  src="{{$basic_info->businessimage or asset('/images/timg.jpg')}}" />
                                    </div>
                                    <div class="col-sm-2" style="text-align: right;">
                                        <input type="file" id="up_img_WU_FILE_0" name="businessimage" />
                                        <input  type="hidden" id="uploadfile" name="businessimagebase64" />
                                        <input  type="hidden" name="businessimage" value="{{$basic_info->businessimage or old('businessimage')}}" />
                                    </div>
                                </li>
                            </ul>
                        @endif


                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">开户行全称 </label>
                        <div class="col-sm-10">
                            @if($status == 1)
                                @foreach($bank_arr as $k=>$v)
                                    @if ($basic_info->bankCode==$v->bankCode)
                                        <input type="text" class="form-control"  name="bankName"  readonly placeholder="开户行全称" value="{{$v->bankName or old('bankName')}}">
                                    @endif
                                @endforeach
                            @else
                                <select id="bankName" name="bankName" id="bankName" class="form-control" onchange="getbankNo()">
                                    <option value="0" >请选择银行</option>
                                    @foreach($bank_arr as $k=>$v)
                                        <option value="{{$v->bankName}}" tip="{{$v->bankCode}}"
                                                @if(!empty($basic_info))
                                                    @if ($basic_info->bankCode==$v->bankCode)
                                                        selected
                                                    @endif
                                                @endif
                                        >{{$v->bankName}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" value="{{$basic_info->bankCode or old('bankCode')}}" name="bankCode"/>
                            @endif

                            @if ($errors->has('category'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('category') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">开户行账号 </label>
                        <div class="col-sm-10">
                            {{--                                    <label class="control-label">{{$basic_info->bankaccountNo}}</label>--}}
                            <input type="text" class="form-control" name="bankaccountNo" @if($status == 1) readonly @endif  placeholder="开户行账号" value="{{ $basic_info->bankaccountNo or old('bankaccountNo')}}">
                            @if ($errors->has('bankaccountNo'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountNo') }}</span>
                            @endif
                        </div>
                    </div>


                    {{--@if($status == 0)--}}
                        {{--<div class="clearfix"></div>--}}
                        {{--<div class="hr-line-dashed"></div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label">联行号可选择的银行范围 </label>--}}
                            {{--<div class="col-sm-10">--}}
                                {{--<select id="bank_name" class="form-control" onchange="getbankNo()">--}}
                                    {{--<option >--请选择银行获取联行号--</option>--}}
                                    {{--@foreach($bank_list as $v)--}}
                                        {{--<option @if($bank_info->bankbranchNo==$v->lbnkNo) selected @endif >{{$v->bank_name}}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                                <input type="hidden" class="form-control" readonly name="account_bank" id="account_bank" value="{{$bank_info->account_bank or old('account_bank')}}">
                                {{--@if ($errors->has('account_bank'))--}}
                                    {{--<span class="help-block m-b-none text-danger">{{ $errors->first('account_bank') }}</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}

                        {{--</div>--}}

                    {{--@endif--}}


                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}



                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">银行卡所属地区银行联行号 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            <input type="hidden" class="form-control" readonly name="ibankno"  id="ibankno"  placeholder="银行卡所属地区银行的联行号" value="{{$bank_info->bankbranchNo or old('ibankno')}}">
                            {{--@if($status == 0)<span class="help-block m-b-none text-danger">请通过选定银行名字获取联行号</span> @endif--}}
                            {{--@if ($errors->has('ibankno'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('ibankno') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}


                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">银行卡类型 </label>
                        <div class="col-sm-10" style="padding-top: 8px">
                            @if($status == 1)
                                @if($bank_info->bankaccountType==1)
                                    <input type="text" class="form-control"   readonly value="借记卡">
                                @elseif($bank_info->bankaccountType==2)
                                    <input type="text" class="form-control"   readonly value="贷记卡">
                                @endif
                            @else
                                {{--<input type="radio" class="control-label" name="bankaccountType" value="1"@if(isset($bank_info)) @if($bank_info->bankaccountType==1) checked @endif @endif/> 借记卡--}}
                                {{--<input type="radio" class="control-label" name="bankaccountType" value="2"@if(isset($bank_info)) @if($bank_info->bankaccountType==2) checked @endif @endif /> 贷记卡--}}
                                {{--<input type="radio" class="control-label" name="bankaccountType" value="3"@if(isset($bank_info)) @if($bank_info->bankaccountType==3) checked @endif @endif /> 存折--}}
                                <input class="form-control" readonly value="借记卡">
                                <input type="hidden" class="form-control" name="bankaccountType" @if($status == 1)  @endif  placeholder="开户行账号" value="{{ $bank_info->bankaccountType or old('bankaccountType')}}">

                            @endif

                            @if ($errors->has('bankaccountType'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountType') }}</span>
                            @endif
                        </div>
                    </div>


                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">费率 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="futureRate" @if($status == 1) readonly @endif  placeholder="费率" value="{{$busi_info->futureRateValue or  old('futureRate')}}">
                            @if($status == 0)<span class="help-block m-b-none text-warning">费率即用户消费后分给平台的让利比例,如：20表示给平台的让利为20%,设置的费率不能低于3</span> @endif

                            @if ($errors->has('futureRate'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('futureRate') }}</span>
                            @endif
                        </div>
                    </div>


                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商家店铺定位 </label>
                        <div class="col-sm-10">
                            <div id="l-map"></div>
                            <div id="r-result"><input class="form-control" type="text" id="suggestId" size="20" value="百度" placeholder="请输入店铺地址的关键字" style="width:100%;margin-top: 20px" /></div>
                            <div id="searchResultPanel" style="border:1px solid #C0C0C0;width:150px;height:auto; display:none;"></div>
                            <input  type="hidden" id="lat" name="lat" value="{{$basic_info->lat or old('lat')}}"/>
                            <input  type="hidden" id="lng" name="lng" value="{{$basic_info->lng or old('lng')}}"/>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    @if($status == 0)
                        <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 修改</button>
                        <a href="{{url('/seller/seller_list')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>
                    @endif

                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')
<script src="{{asset('sanji/js/distpicker.data.js')}}"></script>
<script src="{{asset('sanji/js/distpicker.js')}}"></script>
<script src="{{asset('sanji/js/main.js')}}"></script>
<script>
    var province_name = null
    var city_name = null
    var district_name = null

    var province_code = {{substr($basic_info->area_id,0,2)}}+'0000'
    var city_code = {{substr($basic_info->area_id,0,4)}}+'00'
    var district_code = {{$basic_info->area_id}}
    $(document).ready(function(){
        $('#bankName').change(function(){
            $('input[name=bankCode]').val($('#bankName option:selected').attr('tip'));

        });
        $('#bankName1').change(function(){
            $('input[name=bankCode1]').val($('#bankName1 option:selected').attr('tip'));

        });
        $("#up_img_WU_FILE_0").change(function(){
            var v = $(this).val();
            var reader = new FileReader();
            reader.readAsDataURL(this.files[0]);
            reader.onload = function(e){
                console.log(e.target.result);
                $('#uploadfile').val(e.target.result);
                $('#imgShow_WU_FILE_0').attr('src',e.target.result);
            };
        });

    });

    function getbankNo() {

        var bank_name=$("#bankName").val();
        $.ajax({
            url:'{{url('/seller/get_bankNo')}}',
            type:'POST', //GET
            async:true,    //或false,是否异步
            data:{
                bank_name:bank_name
            },
            timeout:5000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success:function(data){

                if (data.code==0){
//                    alert(data.message)
                    $('#ibankno').val('0');
                    $('#account_bank').val("");
                }else {
                    $('#ibankno').val(data.data.lbnkNo);
                    $('#account_bank').val(data.data.bank_name);
                }
            },
            error:function(xhr,textStatus){
                console.log('错误')
                console.log(xhr)
                console.log(textStatus)
            },

        })

    }
    function get_city(){
        cityName = $("#cityName1").val();
        val=$(".city_option[tip^="+cityName+"]").val();
        $('#aa').val(val);
    }
    function get_province_code() {
        $('#province_code').val($("#province10").find("option:selected").attr("data-code"));
        var j = 0;
        var k = 0;
        $.each(ChineseDistricts[$("#province10").find("option:selected").attr("data-code")], function(i, val) {
            j++;
            if (j == 1){
                $('#city_code').val(i);

                $.each(ChineseDistricts[i], function(i1, val1) {
                    k++;
//                    alert(val1);
                    if (k == 1){
                        $('#district_code').val(i1);
                    }
                });
            }
            if ($("#province10").find("option:selected").attr("data-code") == province_code) {
//                alert(district_name)
                $('#city_code').val(city_code);

                $('#district_code').val(district_code);
            }
        });


//        $('#city_code').val("");
//        $('#district_code').val("");
    }
    function get_city_code() {
        $('#city_code').val($("#city10").find("option:selected").attr("data-code"));
        if (typeof(ChineseDistricts[$("#city10").find("option:selected").attr("data-code")]) == 'undefined') {
            $('#district_code').val("");
        }

        var k = 0;
        $.each(ChineseDistricts[$("#city10").find("option:selected").attr("data-code")], function(i, val) {
            k++;
            if (k == 1){
                $('#district_code').val(i);
            }
        });
//        alert(ChineseDistricts[$('#province_code').val()][1])
//        $('#district_code').val("");

        get_coordinate();

    }
    function get_district_code() {
        $('#district_code').val($("#district10").find("option:selected").attr("data-code"));
        get_coordinate();
    }

    $.each(ChineseDistricts[86], function(i, val) {
        if(i==province_code){
            province_name = val;
//            alert(province_name)

        }
    });
    $.each(ChineseDistricts[province_code], function(i, val) {
        if (i == city_code){
            city_name = val;
//            alert(city_name)
        }
    });
    if (typeof(ChineseDistricts[city_code]) != 'undefined'){
        $.each(ChineseDistricts[city_code], function(i, val) {
            if (i == district_code){
                district_name = val;
//                alert(district_name)

            }
        });
    }

    $("#distpicker5").distpicker({
        province: province_name,
        city: city_name,
        district: district_name
    });
    $(document).ready(function(){
        $('#province_code').val($("#province10").find("option:selected").attr("data-code"));
        $('#city_code').val($("#city10").find("option:selected").attr("data-code"));
        $('#district_code').val($("#district10").find("option:selected").attr("data-code"));

    });


</script>
<script type="text/javascript">
    // 百度地图API功能
    function G(id) {
        return document.getElementById(id);
    }

    var map = new BMap.Map("l-map");
    map.centerAndZoom(new BMap.Point("{{$basic_info->lng}}", "{{$basic_info->lat}}"),12);                   // 初始化地图,设置城市和地图级别。

    var ac = new BMap.Autocomplete(    //建立一个自动完成的对象
            {"input" : "suggestId"
                ,"location" : map
            });

    ac.addEventListener("onhighlight", function(e) {  //鼠标放在下拉列表上的事件
        var str = "";
        var _value = e.fromitem.value;
        var value = "";
        if (e.fromitem.index > -1) {
            value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
        }
        str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;

        value = "";
        if (e.toitem.index > -1) {
            _value = e.toitem.value;
            value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
        }
        str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
        G("searchResultPanel").innerHTML = str;
    });

    var myValue;
    ac.addEventListener("onconfirm", function(e) {    //鼠标点击下拉列表后的事件
        var _value = e.item.value;
        myValue = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
        G("searchResultPanel").innerHTML ="onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue;

        setPlace();
    });

    function setPlace(){
        map.clearOverlays();    //清除地图上所有覆盖物
        function myFun(){
            var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
            map.centerAndZoom(pp, 18);
            $("#lat").val(pp.lat);
            $("#lng").val(pp.lng);
            map.addOverlay(new BMap.Marker(pp));    //添加标注
        }
        var local = new BMap.LocalSearch(map, { //智能搜索
            onSearchComplete: myFun
        });
        local.search(myValue);
    }
    map.enableScrollWheelZoom();   //启用滚轮放大缩小，默认禁用
    map.enableContinuousZoom();    //启用地图惯性拖拽，默认禁用、
    //    单击获取点击的经纬度
    map.addEventListener("click",function(e){
//        alert(e.point.lng + "," + e.point.lat);
        map.clearOverlays();
        var marker = new BMap.Marker(new BMap.Point(e.point.lng, e.point.lat)); // 创建点
        $("#lat").val(e.point.lat);
        $("#lng").val(e.point.lng);
        map.addOverlay(marker);

    });

    $("#merchantAddress").blur(function(){
        get_coordinate()
    });
    $("#province10").blur(function(){
        get_coordinate()
    });

    function get_coordinate() {
        var address = $("#province10").find("option:selected").val()+$("#city10").find("option:selected").val()+$("#district10").find("option:selected").val()+$("#merchantAddress").val();
        var city = $("#province10").find("option:selected").val()+$("#city10").find("option:selected").val();
        // 创建地址解析器实例
        var myGeo = new BMap.Geocoder();
        // 将地址解析结果显示在地图上，并调整地图视野
        myGeo.getPoint(address, function(point){
                    map.clearOverlays();
                    if (point) {
                        map.centerAndZoom(point, 16);
                        $("#lat").val(point.lat);
                        $("#lng").val(point.lng);
                        map.addOverlay(new BMap.Marker(point));
                    }
                },
                city);
    }
    if ("{{!isset($basic_info->lat)?false:true}}" && "{{!isset($basic_info->lng)?false:true}}"){
        theLocation("{{$basic_info->lng}}","{{$basic_info->lat}}")
    }
    // 用经纬度设置地图中心点
    function theLocation(lng,lat){
        map.clearOverlays();
        var new_point = new BMap.Point(lng,lat);
        var marker = new BMap.Marker(new_point);  // 创建标注
        map.addOverlay(marker);              // 将标注添加到地图中
        map.panTo(new_point);

    }



</script>
@endsection