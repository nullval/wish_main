@extends('agent.layouts.app')

@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">绑定pos机</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action=" {{url('/pos/pos_edit_submit')}} ">
                <div class="panel-body">
                    {{csrf_field()}}
                    @if(isset($_GET['id']))
                        <input type="hidden" value="{{$info->id or 0}}" name="id">
                    @endif
                    <input type="hidden" class="form-control" name="terminalId"   placeholder="pos机终端号" value="{{old('terminalId',$info->terminalId)}}">

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">pos机终端号</label>
                        <div class="col-sm-10">
                            <label for="signin-text" class="col-sm-2 control-label" style="text-align: left">{{$info->terminalId}}</label>
                        </div>
                    </div>

                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}
                    {{----}}
                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label" id="text_name">pos机型号</label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="model"   placeholder="pos机型号" value="{{old('model',$info->model)}}">--}}
                            {{--@if ($errors->has('model'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('model') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label" id="text_name">绑定商家</label>
                            {{--<label for="signin-text" class="col-sm-10 control-label" id="text_name">{{$info->seller_mobile or '暂无'}}</label>--}}
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="seller_mobile"   placeholder="绑定商家" value="{{$info->seller_mobile or old('seller_mobile')}}">
                                @if ($errors->has('seller_mobile'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('seller_mobile') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                        {{--只有编辑的时候才能绑定代理--}}
                        {{--<div class="form-group">--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label">绑定代理</label>--}}
                            {{--<div class="col-sm-4">--}}
                                {{--<select class="form-control" name="uid">--}}
                                    {{--<option value="0">-------</option>--}}
                                    {{--@foreach($agent_list as  $k=>$v)--}}
                                        {{--<option value="{{$v->id}}" @if($v->id==$info->uid) selected @endif>{{$v->mobile}}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            {{--</div>--}}
                            {{--<div class="col-sm-6">--}}
                                {{--不选则不绑定--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                        {{--<div class="hr-line-dashed"></div>--}}
                    {{--@endif--}}


                        <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 修改</button>

                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')
@endsection