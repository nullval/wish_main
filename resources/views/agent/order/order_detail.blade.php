@extends('agent.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">子订单管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding" style="margin-bottom: 20px;">
                <div class="col-sm-6">
                    <span class="form-control">订单号{{$order_info->order_sn}}</span>
                    <input type="hidden" value="{{$order_info->order_sn}}" id="order_sn"/>
                    <input type="hidden" value="{{$_GET['year']}}" id="year"/>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">购买的机主 {{$_GET['mobile']}} </span>
                    <input type="hidden" value="{{$_GET['mobile']}}" id="mobile"/>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">订单状态
                        @if($order_info->status==1)
                            <span class="text-primary">处理中</span>
                        @elseif($order_info->status==2)
                            <span class="text-success">支付成功</span>
                        @elseif($order_info->status==3)
                            <span class="text-danger">支付失败</span>
                        @endif
                    </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">消费金额 {{$order_info->amount}} </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">购买台数 {{$order_info->pos_num}} </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">刷卡使用的未特商城pos机 {{$order_info->terminalId}} </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">创建时间 {{$order_info->created_at}} </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">订单是否有效
                        @if($order_info->is_true==1)
                            <span class="text-success">有效</span>
                        @elseif($order_info->is_true==2)
                            <span class="text-danger">无效</span>
                        @endif
                    </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">收货人 {{$order_info->receiver}} </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">联系电话 {{$order_info->contact_number}} </span>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">收货地址 {{$order_info->contact_address}} </span>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                <div class="col-sm-12">
                    <div class="alert alert-warning"> 订单只有已支付且有效,机主才能将自己的未绑定商家的未特商城pos机绑定给其他机主</div>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>子订单编号</th>
                        <th>创建时间</th>
                        {{--<th>绑定的未特商城pos机</th>--}}
                        {{--<th>绑定</th>--}}
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($order_detail_list as $k=>$v)
                            <tr>
                                <td>{{$v->order_sku_sn}}</td>
                                <td>{{$v->created_at}}</td>
                                {{--<td>--}}
                                    {{--@if(empty($v->terminalId))--}}
                                            {{--<select class="terminalId" >--}}
                                                {{--<option value="0">-----</option>--}}
                                                {{--@foreach($pos_list as $k1=>$v1)--}}
                                                    {{--<option value="{{$v1['terminalId']}}">{{$v1['terminalId']}}</option>--}}
                                                {{--@endforeach--}}
                                            {{--</select>--}}
                                        {{--@else--}}
                                            {{--{{$v->terminalId or '暂无'}}--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                    {{--只有订单已支付且订单有效才能绑定--}}
                                    {{--@if($order_info->is_true==1&&$order_info->status==2)--}}
                                        {{--@if(empty($v->terminalId))--}}
                                            {{--<a href="#" class="bind">--}}
                                                {{--<span class="label label-warning"><i class="fa fa-edit"></i> 绑定</span>--}}
                                            {{--</a>--}}
                                        {{--@endif--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $order_detail_list->appends([
                    'order_sn'=>isset($_GET['order_sn'])?$_GET['order_sn']:'',
                    'mobile'=>isset($_GET['mobile'])?$_GET['mobile']:'',
                    'year'=>isset($_GET['year'])?$_GET['year']:date('Y')
                    ])->links() }}
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            _token=$('input[name=_token]').val();
            $('.bind').each(function(){
                $(this).click(function(){
                    terminalId=$(this).parent().prev().find('.terminalId').val();
                    order_sku_sn=$(this).parent().prev().prev().prev().text();
                    if(terminalId==0){
                        alert('请选择要绑定的未特商城pos机');return false;
                    }else{
                        if(confirm('您确定要将'+terminalId+'绑定给机主'+$('#mobile').val())){
                            $.ajax({
                                type: "POST",
                                url: 'bind_pos',
                                data: {_token:_token, terminalId:terminalId,order_sn:$('#order_sn').val(),year:$('#year').val(),order_sku_sn:order_sku_sn},
                                dataType: "json",
                                success: function(data){
                                    if(data.code==0){
                                        alert(data.message);
                                    }else{
                                        alert('绑定成功');
                                        location.reload();
                                    }
                                }
                            });
                        }
                    }
                });
            });
        });
    </script>
@endsection