@extends('agent.layouts.app')
<style>
    .invoice_amount{
        float: right;
        padding-right: 10px;
        padding-top: 4px;
        font-size: 16px;
    }

</style>
@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">订单管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding">--}}
                <div class="col-sm-12">
                    @if ($errors->has('error'))
                        <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                    @endif
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-2" style="margin: 10px 0px">--}}
                    {{--<select class="form-control" id="year">--}}
                        {{--<option value="2017">2017</option>--}}
                        {{--<option value="2018" @if(isset($_GET['year'])&&$_GET['year']==2018||date('Y')==2018) selected @endif>2018</option>--}}
                        {{--<option value="2019" @if(isset($_GET['year'])&&$_GET['year']==2019||date('Y')==2019) selected @endif>2019</option>--}}
                        {{--<option value="2020" @if(isset($_GET['year'])&&$_GET['year']==2020||date('Y')==2020) selected @endif>2020</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="col-sm-8" style="margin: 10px 0px">
                    <input type="text" id="order_sn" class="form-control" placeholder="订单编号" style="width: 200px;float:left;" value="@if(isset($_GET['order_sn'])&&(!empty($_GET['order_sn']))){{$_GET['order_sn']}}@endif"/>
                    <input type="text" id="mobile" class="form-control" placeholder="购买的机主" style="width: 200px;float:left;" value="@if(isset($_GET['mobile'])&&(!empty($_GET['mobile']))){{$_GET['mobile']}}@endif"/>
                    <input type="text" id="terminalId" class="form-control" placeholder="刷卡使用的未特商城pos机" style="width: 200px;float:left;" value="@if(isset($_GET['terminalId'])&&(!empty($_GET['terminalId']))){{$_GET['terminalId']}}@endif"/>
                    <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>

                    {{--<a type="submit" id="claim" class="btn btn btn-info" style="float:right;"> 索取发票</a>--}}
                    {{--<span class="invoice_amount">待开票金额：<span id="amount" style="color: red">0.00</span></span>--}}
                </div>

                <div class="col-sm-1" style="float:right;">
                    <a type="submit" id="claim" style="float:right;margin: 10px 0px" class="btn btn btn-info" > 索取发票</a>
                </div>
                <div class="col-sm-2" style="float:right;">
                    <div style="float:right;margin: 10px 0px"><span class="invoice_amount">待开票金额：<span id="amount" style="color: red;">0.00</span></span></div>
                </div>

                <table class="table table-striped">

                    <thead>
                    <tr>
                        <th></th>
                        <th>订单编号</th>
                        <th>购买的机主</th>
                        <th>订单状态</th>
                        <th>消费金额</th>
                        <th>购买台数</th>
                        <th>刷卡的未特商城pos机</th>
                        <th>创建时间</th>
                        <th>是否有效</th>
                        {{--<th>查看详情</th>--}}
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($order_list as $k=>$v)
                            <tr>
                                <th><input class="check" tip="{{$v->amount or 0}}" type="checkbox" name="{{$v->id}}_{{date('Y',strtotime($v->created_at))}}" value="checkbox"> </th>
                                <td>{{$v->order_sn}}</td>
                                <td>{{$v->mobile or '暂无'}}</td>
                                <td>
                                    @if($v->status==1)
                                        <span class="text-primary">处理中</span>
                                    @elseif($v->status==2)
                                        <span class="text-success">支付成功</span>
                                    @elseif($v->status==3)
                                        <span class="text-danger">支付失败</span>
                                    @endif
                                </td>
                                <td>{{$v->amount or 0}}</td>
                                <td>{{$v->pos_num or 0}}</td>
                                <td>{{$v->terminalId or '暂无'}}</td>
                                <td>{{$v->created_at or 0}}</td>
                                <td>
                                    @if($v->is_true==1)
                                        <span class="text-success">有效</span>
                                    @elseif($v->is_true==2)
                                        <span class="text-danger">无效</span>
                                    @endif
                                </td>
                                {{--<td>--}}
                                    {{--<a href="{{url('order/order_detail')}}?order_sn={{$v->order_sn}}&mobile={{$v->mobile}}&year={{$_GET['year'] or date('Y')}}">--}}
                                        {{--<span class="label label-success"><i class="fa fa-eye"></i> 查看</span>--}}
                                    {{--</a>--}}
                                {{--</td>--}}
                            </tr>
                        @endforeach
                        <tr>
                            <th><input type="checkbox" id="checkAll" value="checkbox"> </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>
                            </th>
                        </tr>
                        </tbody>
                    </form>
                </table>
            {{--</div>--}}
            <div class="panel-footer">
                <div class="row">
                    {{ $order_list->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $order_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){


            $(".check").click(function(){
                $amount=0;
                $(".check:checked").each(function(){
                    $amount=parseFloat($(this).attr('tip'))+$amount;
                });
                $('#amount').html($amount);
            });


            $('#year').change(function(){
                location_a();
            });
            $('#search').click(function(){
                location_a();
            });

            $('#claim').click(function(){
                Claim();
            });
            $('#checkAll').change(function(){
                if($('#checkAll').is(':checked')){
                    $("input[type=checkbox]").prop("checked",true);
                    $amount=0;
                    $(".check:checked").each(function(){
                        $amount=parseFloat($(this).attr('tip'))+$amount;
                    });
                    $('#amount').html($amount);
                }

                else{
                    $("input[type=checkbox]").attr("checked",false);
                    $amount=0;
                    $(".check:checked").each(function(){
                        $amount=parseFloat($(this).prop('tip'))+$amount;
                    });
                    $('#amount').html($amount);
                }
            });

        });
        function location_a() {
            location.href="{{url('/order/order_list')}}?mobile="+$('#mobile').val()+"&order_sn="+$('#order_sn').val()+"&terminalId="+$('#terminalId').val();
        }

        function Claim() {
            var order_list='';
            $('input:checked').each(function(){

                if(typeof($(this).attr('name'))!='undefined'){
                    order_list=$(this).attr('name')+','+order_list;
                }
            });
            var amount=$('#amount').text();
            location.href="{{url('/invoice/invoice_add')}}?order_list="+order_list+"&amount="+amount;
        }
    </script>
@endsection