@extends('agent.layouts.operate_app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" xmlns="http://www.w3.org/1999/html"/>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">运营中心基本信息</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 请慎重提交,提交之后不能修改,如若要修改请联系管理员</div>--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/info/bind_submit')}}" class="form-horizontal"enctype='multipart/form-data'>
                <div class="panel-body">
                    {{csrf_field()}}
                    {{--<input type="hidden" value="{{$info->bank_id or old('bank_id')}}" name="bank_id">--}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" >运营中心手机号 </label>
                        <div class="col-sm-10" >
                            <label for="signin-text" class="col-sm-2 control-label"style="text-align: left"id="username">{{$info->mobile}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" >级别 </label>
                        <div class="col-sm-10" >
                            <label for="signin-text" class="col-sm-2 control-label"style="text-align: left"id="username">
                                @if($info['level']==1)
                                    一级运营中心
                                @elseif($info['level']==2)
                                    二级运营中心
                                @elseif($info['level']==3)
                                    三级运营中心
                                @endif
                            </label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>

@endsection
@section('js')
    {{--<script src="{{asset('image_frame/js/jqueryLibrary.min.js')}}"></script>--}}
    {{--<script src="{{asset('image_frame/js/jquery.imgbox.pack.js')}}"></script>--}}
    <script>
        function getbankNo() {
            var bank_name=$("select").val();
            $.ajax({
                url:'{{url('/finance/get_bankNo')}}',
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    bank_name:bank_name
                },
                timeout:5000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data){

                    if (data.code==0){
                        alert(data.message)
                    }else {
                        $('#ibankno').val(data.data.lbnkNo);
                        $('#account_bank').val(data.data.bank_name);
                    }
                },
                error:function(xhr,textStatus){
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },

            })

        }
        $(function(){
            $('#account_bank').change(function(){
                $('input[name=ibankno]').val($('select[name=account_bank] option:selected').attr('tip'));
            });

            $('#search').click(function(){
                var bank_name=$('#bank_name1').val();
                $.ajax({
                    url:'{{url('/finance/getbankcode')}}',
                    type:'POST', //GET
                    async:true,    //或false,是否异步
                    data:{
                        bank_name:bank_name
                    },
                    timeout:5000,    //超时时间
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(data){

                        if (data.code==0){
                            alert(data.message)
                        }else {
                            $('#bank_code').val(data.data.bank_code);
                            $('#bank_cname').text(data.data.bank_name);
                            $('#bank_name').val(data.data.bank_name);
                            $('#account_bank').val(data.data.bank_name);

                        }
                    },
                    error:function(xhr,textStatus){
                        console.log('错误')
                        console.log(xhr)
                        console.log(textStatus)
                    },

                })
            });
        });

        function send_sms(){
            mobile = $("#username").html();
//            if(!(/^1[3|4|5|8][0-9]\d{4,8}$/.test(mobile))){
//                alert("请输入正确的手机号码");return false;
//            }
            //发送短信验证码,并设置调用时间
            $.ajax({
                type: "POST",
                url: '{{url('/pushQrUser/send_sms')}}',
                data: {mobile: mobile},
                dataType: "json",
                success: function (data) {
                    if (data.code == 1) {
                        alert('手机短信已发送');
                    } else {
                        alert(data.message);
                    }
                    new invokeSettime("#btn");
                }
            });
        }

        function invokeSettime(obj){
            var countdown=60;
            settime(obj);
            function settime(obj) {
                if (countdown == 0) {
                    $(obj).attr("disabled",false);
                    $(obj).val("获取验证码");
                    countdown = 60;
                    return;
                } else {
                    $(obj).attr("disabled",true);
                    $(obj).val("(" + countdown + ") s 重新发送");
                    countdown--;
                }
                setTimeout(function() {
                            settime(obj) }
                        ,1000)
            }
        }




//        setTimeout( " delData() " , 500 );

        function delData() {
            $.ajax({
                url: '{{url('/seller/delData')}}',
                type: 'POST', //GET
                async: true,    //或false,是否异步
                data: {},
                timeout: 5000,    //超时时间
                dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success: function (data) {
                    {{--alert({{session('mobile')}})--}}
                            {{--if ("{{old('mobile')}}" == ""){--}}
                            {{--$("[name='mobile']").val("{{session('mobile')}}");--}}
                            {{--}--}}
                            window.onload = function(){
                        setTimeout("location.reload()",500);
                    }

                },
                error: function (xhr, textStatus) {
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },
            })
        }
    </script>
@endsection