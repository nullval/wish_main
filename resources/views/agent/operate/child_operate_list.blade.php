@extends('agent.layouts.operate_app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">子运营中心列表</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding">--}}
                <div class="col-sm-12" style="margin-bottom: 10px">
                    @if(Session::has('status'))
                        <div class="alert alert-info" style="color: green"> {{Session::get('status')}}</div>
                    @endif
                </div>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>运营中心手机号</th>
                        <th>已开通的商家数量</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($list as $k=>$v)
                        <tr>
                            <td>{{$v->mobile}}</td>
                            {{--<td>{{$v->mobile}}</td>--}}
                            <td>{{$v->seller_count}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            {{--</div>--}}
            <div class="panel-footer">
                <div class="row">
                    {{ $list->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $list->total()}}</a>
                        </li>
                    </ul>
                    {{--<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>--}}
                    {{--<div class="col-md-6 text-right"><a href="#" class="btn btn-primary">View All Purchases</a></div>--}}
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
@endsection