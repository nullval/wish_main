@extends('agent.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">机主提现管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding">--}}
                <div class="col-sm-12" style="margin-bottom: 10px">
                    @if(Session::has('status'))
                        <div class="alert alert-info" style="color: green"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="status">--}}
                        {{--<option value="0">全部</option>--}}
                        {{--<option value="1" @if(isset($_GET['status'])&&$_GET['status']==1) selected @endif>处理中</option>--}}
                        {{--<option value="2" @if(isset($_GET['status'])&&$_GET['status']==2) selected @endif>处理成功</option>--}}
                        {{--<option value="3" @if(isset($_GET['status'])&&$_GET['status']==3) selected @endif>处理失败</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                {{--<div class="col-sm-6">--}}
                    {{--<input type="text" id="mobile" class="form-control" placeholder="商家手机号" style="width: 200px;float:left;" value="@if(isset($_GET['mobile'])&&(!empty($_GET['mobile']))){{$_GET['mobile']}}@endif"/>--}}
                    {{--<a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>--}}
                {{--</div>--}}
                {{--<div class="col-sm-2" style="margin: 10px 0px;">--}}
                    {{--<select class="form-control" id="year">--}}
                        {{--<option value="2017">2017 </option>--}}
                        {{--<option value="2018" @if(isset($_GET['year'])&&$_GET['year']==2018||date('Y')==2018) selected @endif>2018</option>--}}
                        {{--<option value="2019" @if(isset($_GET['year'])&&$_GET['year']==2019||date('Y')==2019) selected @endif>2019</option>--}}
                        {{--<option value="2020" @if(isset($_GET['year'])&&$_GET['year']==2020||date('Y')==2020) selected @endif>2020</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="col-sm-2">
                    <select class="form-control" id="status">
                        <option value="0">全部</option>
                        <option value="1" @if(isset($_GET['status'])&&$_GET['status']==1) selected @endif>处理中</option>
                        <option value="2" @if(isset($_GET['status'])&&$_GET['status']==2) selected @endif>处理成功</option>
                        <option value="3" @if(isset($_GET['status'])&&$_GET['status']==3) selected @endif>处理失败</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="form-control" id="purse_type" name="purse_type">
                        <option value="0">全部</option>
                        <option value="1" @if(isset($_GET['purse_type'])&&$_GET['purse_type']==1) selected @endif>收益钱包</option>
                        {{--<option value="10" @if(isset($_GET['purse_type'])&&$_GET['purse_type']==10) selected @endif>消费积分</option>--}}
                    </select>
                </div>
                <div class="col-sm-6" style="float: right">
                    {{--@if(session('agent_info')->id!=1)--}}
                        <a href="{{url('/finance/withdraw')}}" class="btn btn-info" style="float: right;"><i class="fa fa-plus"></i> 提现</a>
                    {{--@endif--}}
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>提现编号</th>
                        <th>开户名</th>
                        <th>开户行</th>
                        <th>银行卡号</th>
                        <th>提现金额</th>
                        <th>实到金额</th>
                        <th>手续费</th>
                        <th>申请时间</th>
                        <th>状态</th>
                        <th>备注</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user_withdraw as $k=>$v)
                        <tr>
                            <td>{{$v->id}}</td>
                            {{--<td>{{$v->mobile}}</td>--}}
                            <td>{{$v->account_name}}</td>
                            <td>{{$v->account_bank}}</td>
                            <td>{{$v->bank_account}}</td>
                            <td>{{get_last_two_num($v->amount)}}</td>
                            <td>{{get_last_two_num($v->actual_amount)}}</td>
                            <td>{{get_last_two_num($v->d_poundage)}}</td>
                            <td>{{$v->created_at}}</td>
                            <td>
                                @if($v->status==1)    处理中
                                @elseif($v->status==2) 处理成功
                                @elseif($v->status==3) 处理失败
                                @endif
                            </td>
                            <td>{{$v->remark}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            {{--</div>--}}
            <div class="panel-footer">
                <div class="row">
                    {{ $user_withdraw->appends([
                    'status'=>isset($_GET['status'])?$_GET['status']:0
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $user_withdraw->total()}}</a>
                        </li>
                    </ul>
                    {{--<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>--}}
                    {{--<div class="col-md-6 text-right"><a href="#" class="btn btn-primary">View All Purchases</a></div>--}}
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            $('#status,#purse_type').change(function(){
                location.href="{{url('/finance/withdraw_list')}}?status="+$('#status').val()+"&purse_type="+$('#purse_type').val();
            });

        });
    </script>
@endsection