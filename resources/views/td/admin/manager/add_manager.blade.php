@extends('td.admin.layouts.app')

<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" xmlns="http://www.w3.org/1999/html"
      xmlns="http://www.w3.org/1999/html"/>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">添加管理员</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 每次收益提现扣除3块手续费</div>--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/manager/add_manager_submit')}}" class="form-horizontal"enctype='multipart/form-data'>
                <div class="panel-body">
                    {{csrf_field()}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">代付用户名 </label>--}}
                        {{--<div class="col-sm-10" >--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label"style="text-align: left">{{$bank_info->mobile}}</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}




                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">管理员手机号</br> </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mobile"   placeholder="管理员手机号"  value="{{$info->mobile or old('mobile')}}">
                            @if ($errors->has('mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">登录密码</br> </label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="password"   placeholder="登录密码"  value="{{$info->password or old('password')}}">
                            @if ($errors->has('password'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">备注</br> </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="remarks"   placeholder="备注"  value="{{$info->remarks or old('remarks')}}">
                            @if ($errors->has('remarks'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('remarks') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                            <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                    <a href="{{url('/manager/manager_list')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>




                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>

@endsection
@section('js')
    <script>
        $('#search').click(function(){
            var bank_name=$('#bank_name1').val();
            $.ajax({
                url:'{{url('/finance/getbankcode')}}',
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    bank_name:bank_name
                },
                timeout:5000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data){

                    if (data.code==0){
                        alert(data.message)
                    }else {
                        $('#bank_code').val(data.data.bank_code);
                        $('#bank_cname').text(data.data.bank_name);
                        $('#bank_name').val(data.data.bank_name);

                    }
                },
                error:function(xhr,textStatus){
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },

            })
        });


    </script>
@endsection