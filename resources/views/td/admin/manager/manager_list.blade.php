@extends('td.admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">管理员管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding">--}}
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
            <div class="col-sm-12">
                <input type="text" id="mobile" class="form-control" placeholder="管理员手机号" style="width: 200px;float:left;" value="@if(isset($_GET['mobile'])&&(!empty($_GET['mobile']))){{$_GET['mobile']}}@endif"/>
                <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
            </div>

                <div class="col-sm-6" style="float: right ;margin: 10px 0px;">
                    @if(session('agent_info')->id!=1)
                        <a href="{{url('/manager/add_manager')}}" class="btn btn-info" style="float: right;"><i class="fa fa-plus"></i> 添加管理员</a>
                    @endif
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>手机号码</th>
                        <th>备注</th>
                        <th>创建时间</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($user_list as $k=>$v)
                            <tr>
                                <td>{{$v->mobile}}</td>
                                <td>{{$v->remarks}}</td>
                                <td>{{$v->created_at or '暂无'}}</td>
                          </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            {{--</div>--}}
            <div class="panel-footer">
                <div class="row">
                    {{ $user_list->appends([
                    'mobile'=>isset($_GET['mobile'])?$_GET['mobile']:''
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $user_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){

            $('#search').click(function(){
                location.href="{{url('/manager/manager_list')}}?mobile="+$('#mobile').val();
            });

        });
    </script>
@endsection