@extends('td.admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">用户管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding">--}}
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                <div class="col-sm-12">
                    <input type="text" id="mobile" class="form-control" placeholder="用户手机号" style="width: 200px;float:left;" value="@if(isset($_GET['mobile'])&&(!empty($_GET['mobile']))){{$_GET['mobile']}}@endif"/>
                    <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>手机号码</th>
                        {{--<th>归属商家</th>--}}
                        <th>获得总CHQ</th>
                        <th>可用CHQ</th>
                        <th>待释放CHQ</th>
                        {{--<th>平台分润</th>--}}
                        {{--<th>刷卡扣费</th>--}}
                        <th>创建时间</th>
                        <th>区块链地址</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($user_list as $k=>$v)
                            <tr>
                                <td>{{$v->mobile}}</td>
                                {{--<td>{{$v->mobile or '暂无'}}</td>--}}
                                <td>{{get_last_two_num($v->use_td+$v->freeze_td+$v->used_td)}}</td>
                                <td>{{get_last_two_num($v->use_td)}}</td>
                                <td>{{get_last_two_num($v->freeze_td)}}</td>
                                {{--<td>{{$v->platform_amount or 0}}</td>--}}
                                {{--<td>{{$v->poundage or 0}}</td>--}}
                                <td>{{$v->created_at or '暂无'}}</td>
                                <td>{{$v->blockchain or '暂无'}}</td>
                          </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            {{--</div>--}}
            <div class="panel-footer">
                <div class="row">
                    {{ $user_list->appends([
                    'mobile'=>isset($_GET['mobile'])?$_GET['mobile']:''
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $user_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){

            $('#search').click(function(){
                location.href="{{url('/index/index')}}?mobile="+$('#mobile').val();
            });

        });
    </script>
@endsection