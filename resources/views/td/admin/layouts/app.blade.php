<!doctype html>
<html lang="en">

<head>
    <title>创观会员对接管理总后台</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{asset('home/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/linearicons/style.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/chartist/css/chartist-custom.css')}}">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{asset('home/css/main.css')}}">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="{{asset('home/css/demo.css')}}">
    <link rel="stylesheet" href="{{asset('home/css/myself.css')}}">
    <link rel="stylesheet" href="{{asset('bootstrap/bootstrap-switch/bootstrap-switch.css')}}">
    <!-- GOOGLE FONTS -->
    {{--<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">--}}
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('home/img/apple-icon.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('home/img/favicon.png')}}">
    <link rel="stylesheet" href="{{asset('css/myself.css')}}">
    <script src="{{asset('seller/js/uploadPreview.js')}}" type="text/javascript"></script>
    <style>
        .sidebar .nav > li > a:focus, .sidebar .nav > li > a.active{
            border-left-color:#AEB7C2;
        }
        .sidebar .nav > li > a:hover i, .sidebar .nav > li > a:focus i, .sidebar .nav > li > a.active i{
            color: #AEB7C2;
        }
    </style>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
        /*body, html{width: 100%;height: 100%;margin:0;font-family:"微软雅黑";font-size:14px;}*/
        #l-map{height:250px;width:100%;}
        /*#r-result{width:100%;}*/
    </style>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=RNifyeVIz4GNTjvl9XwDUth7ufYPk2pX"></script>
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="brand">
            <a href="{{url('/index/index')}}">创观会员对接管理总后台</a>
        </div>
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
            </div>
            <form class="navbar-form navbar-left">
                <div class="input-group">
                    <input type="text" value="" class="form-control" placeholder="Search dashboard...">
                    <span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
                </div>
            </form>
            {{--<div class="navbar-btn navbar-btn-right">--}}
                {{--<button type="button" class="btn btn-info">Info</button>--}}
            {{--</div>--}}
            <div id="navbar-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="lnr lnr-alarm"></i>
                            {{--<span class="badge bg-danger">5</span>--}}
                        </a>
                        {{--<ul class="dropdown-menu notifications">--}}
                            {{--<li><a href="#" class="notification-item"><span class="dot bg-warning"></span>System space is almost full</a></li>--}}
                            {{--<li><a href="#" class="notification-item"><span class="dot bg-danger"></span>You have 9 unfinished tasks</a></li>--}}
                            {{--<li><a href="#" class="notification-item"><span class="dot bg-success"></span>Monthly report is available</a></li>--}}
                            {{--<li><a href="#" class="notification-item"><span class="dot bg-warning"></span>Weekly meeting in 1 hour</a></li>--}}
                            {{--<li><a href="#" class="notification-item"><span class="dot bg-success"></span>Your request has been approved</a></li>--}}
                            {{--<li><a href="#" class="more">See all notifications</a></li>--}}
                        {{--</ul>--}}
                    </li>
                    {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="lnr lnr-question-circle"></i> <span>Help</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li><a href="#">Basic Use</a></li>--}}
                            {{--<li><a href="#">Working With Data</a></li>--}}
                            {{--<li><a href="#">Security</a></li>--}}
                            {{--<li><a href="#">Troubleshooting</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{asset('/home/img/system.png')}}" class="img-circle" alt="Avatar"> <span>{{session('td_admin_info')->username}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                        <ul class="dropdown-menu">
                            {{--<li><a href="#"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>--}}
                            {{--<li><a href="#"><i class="lnr lnr-envelope"></i> <span>Message</span></a></li>--}}
                            {{--<li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>--}}
                            <li><a href="{{url('/logout')}}"><i class="lnr lnr-exit"></i> <span>退出</span></a></li>
                        </ul>
                    </li>
                    <!-- <li>
                        <a class="update-pro" href="#downloads/klorofil-pro-bootstrap-admin-dashboard-template/?utm_source=klorofil&utm_medium=template&utm_campaign=KlorofilPro" title="Upgrade to Pro" target="_blank"><i class="fa fa-rocket"></i> <span>UPGRADE TO PRO</span></a>
                    </li> -->
                </ul>
            </div>
        </div>
    </nav>
    <!-- END NAVBAR -->
    <!-- LEFT SIDEBAR -->
    {{--获取当前控制器名称--}}
    <?php $prefix=request()->route()->getAction()['prefix']; ?>
    {{--获取当前方法--}}
    <?php $route =request()->route()->getAction()['controller'];
    $action=explode('@', $route)[1];?>

    <div id="sidebar-nav" class="sidebar">
        <div class="sidebar-scroll">
            <nav>
                <ul class="nav">
                    <li>
                        <a href="#index" data-toggle="collapse" class="collapsed @if($prefix=='/index') active @endif"><i class="lnr lnr-home"></i> <span>用户管理</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="index" class="collapse @if($prefix=='/index') in @endif">
                            <ul class="nav">
                                <li><a href="{{url('/index/index')}}" class="@if($action=='index') active @endif">用户管理</a></li>
                                {{--<li><a href="{{url('/info/inviter_list')}}" class="@if($action=='inviter_list') active @endif">推荐机主</a></li>--}}
                                {{--<li><a href="{{url('/info/nine_floor')}}" class="@if($action=='nine_floor') active @endif">我的九度人脉</a></li>--}}
                                {{--<li><a href="{{url('/info/change_pwd')}}" class="@if($action=='change_pwd') active @endif">修改密碼</a></li>--}}
                            </ul>
                        </div>
                    </li>

                    <li>
                        <a href="#manager" data-toggle="collapse" class="collapsed @if($prefix=='/manager') active @endif"><i class="lnr lnr-chart-bars"></i> <span>管理员管理</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="manager" class="collapse @if($prefix=='/manager') in @endif">
                            <ul class="nav">
                                <li><a href="{{url('/manager/manager_list')}}" class="@if($action=='manager_list') active @endif">管理员管理</a></li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <a href="#pos" data-toggle="collapse" class="collapsed @if($prefix=='/pos') active @endif"><i class="lnr lnr-chart-bars"></i> <span>POS机管理</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="pos" class="collapse @if($prefix=='/pos') in @endif">
                            <ul class="nav">
                                <li><a href="{{url('/pos/pos_list')}}" class="@if($action=='pos_list') active @endif">POS机管理</a></li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <a href="#order" data-toggle="collapse" class="collapsed @if($prefix=='/order') active @endif"><i class="lnr lnr-chart-bars"></i> <span>订单管理</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="order" class="collapse @if($prefix=='/order') in @endif">
                            <ul class="nav">
                                <li><a href="{{url('/order/order_list')}}" class="@if($action=='info') active @endif">订单管理</a></li>
                            </ul>
                        </div>
                    </li>


                </ul>
            </nav>
        </div>
    </div>
    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @yield('content')
           </div>
        </div>
    </div>
    <!-- END MAIN -->
    <div class="clearfix"></div>
    <footer>
        <div class="container-fluid">
            <p class="copyright">Copyright &copy; 2017.Company name All rights reserved.</p>
        </div>
    </footer>
</div>
<!-- END WRAPPER -->
<!-- Javascript -->
<script src="{{asset('home/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('home/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('home/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('home/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('home/vendor/chartist/js/chartist.min.js')}}"></script>
<script src="{{asset('home/js/klorofil-common.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor/ueditor.config.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor/ueditor.all.min.js')}}"> </script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor/lang/zh-cn/zh-cn.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('bootstrap/bootstrap-switch/bootstrap-switch.js')}}"> </script>
@yield('js')
<script type="text/javascript" charset="utf-8" src="{{asset('js/myself.js')}}"> </script>
{{--<script src="//apps.bdimg.com/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>--}}
{{--<script src="{{asset('jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js')}}"></script>--}}

</body>

</html>
