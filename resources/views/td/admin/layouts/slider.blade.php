<?php $item_category_list=Cache::store('file')->get('item_category_list'); ?>
<div class="col-md-3 product-bottom">
    <!--categories-->
    <div class=" rsidebar span_1_of_left">
        <h4 class="cate">分类</h4>
        <ul class="menu-drop">
            @foreach($item_category_list as $k=>$v)
                <li class="item{{$k+1}}">
                    <a class="choose @if(isset($_GET['category_id'])&&$_GET['category_id']==$v['category_id']) active @endif" tip="category_id" value="{{$v['category_id']}}" href="#">{{$v['category_name']}} </a>
                </li>
            @endforeach
        </ul>
    </div>
    <!--initiate accordion-->
    <script type="text/javascript">
        $(function() {
            $('.choose').click(function(){
                $category_id=getUrlParam('category_id');
                $item_bought=getUrlParam('item_bought');
                $item_price=getUrlParam('item_price');
                $tip=$(this).attr('tip');
                $value=$(this).attr('value');
                if($tip=='category_id'){
                    $category_id=$value;
                }
                if($tip=='item_bought'){
                    $item_bought=$value;
                }
                if($tip=='item_price'){
                    $item_price=$value;
                }
                if($category_id==null){$category_id=0;}
                if($item_bought==null){$item_bought=0;}
                if($item_price==null){$item_price=0;}
                $url="/index/items_list?category_id="+$category_id+"&item_bought="+$item_bought+"&item_price="+$item_price;
                location.href=$url;
            });
            var menu_ul = $('.menu-drop > li > ul'),
                    menu_a  = $('.menu-drop > li > a');
            menu_ul.hide();
            menu_a.click(function(e) {
                e.preventDefault();
                if(!$(this).hasClass('active')) {
                    menu_a.removeClass('active');
                    menu_ul.filter(':visible').slideUp('normal');
                    $(this).addClass('active').next().stop(true,true).slideDown('normal');
                } else {
                    $(this).removeClass('active');
                    $(this).next().stop(true,true).slideUp('normal');
                }
            });

        });
    </script>

    <!--//menu-->
    <section  class="sky-form">
        <div class=" rsidebar span_1_of_left">
            <h4 class="cate">销量</h4>
            <ul class="menu-drop">
                <li ><a class="choose @if(isset($_GET['item_bought'])&&$_GET['item_bought']==1) active @endif" tip="item_bought" value="1" href="#">从低到高</a></li>
                <li ><a class="choose @if(isset($_GET['item_bought'])&&$_GET['item_bought']==2) active @endif" tip="item_bought" value="2" href="#">从高到低</a></li>
            </ul>
        </div>
    </section>


    <!---->
    <section  class="sky-form">
        <div class=" rsidebar span_1_of_left">
            <h4 class="cate">销量</h4>
            <ul class="menu-drop">
                <li ><a class="choose @if(isset($_GET['item_price'])&&$_GET['item_price']=='0-1000') active @endif" href="#" tip="item_price" value="0-1000">1000以下</a></li>
                <li ><a class="choose @if(isset($_GET['item_price'])&&$_GET['item_price']=='1000-2000') active @endif" href="#" tip="item_price" value="1000-2000">1000-2000</a></li>
                <li ><a class="choose @if(isset($_GET['item_price'])&&$_GET['item_price']=='2000-3000') active @endif" href="#" tip="item_price" value="2000-3000">2000-3000</a></li>
                <li ><a class="choose @if(isset($_GET['item_price'])&&$_GET['item_price']=='3000-4000') active @endif" href="#" tip="item_price" value="3000-4000">3000-4000</a></li>
                <li ><a class="choose @if(isset($_GET['item_price'])&&$_GET['item_price']=='4000') active @endif" href="#" tip="item_price" value="4000">4000以上</a></li>
            </ul>
        </div>
    </section>

</div>
<style>
    .menu-drop .active{
        color:#ed97a0;
    }
</style>