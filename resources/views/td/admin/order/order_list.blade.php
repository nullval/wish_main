@extends('td.admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">订单管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding">--}}
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                <div class="col-sm-12">
                    <input type="text" id="order_sn" class="form-control" placeholder="订单编号" style="width: 200px;float:left;" value="@if(isset($_GET['order_sn'])&&(!empty($_GET['order_sn']))){{$_GET['order_sn']}}@endif"/>
                    <input type="text" id="terminalId" class="form-control" placeholder="刷卡使用的Pos机" style="width: 200px;float:left;" value="@if(isset($_GET['terminalId'])&&(!empty($_GET['terminalId']))){{$_GET['terminalId']}}@endif"/>
                    <input type="text" id="mobile" class="form-control" placeholder="归属管理员" style="width: 200px;float:left;" value="@if(isset($_GET['mobile'])&&(!empty($_GET['mobile']))){{$_GET['mobile']}}@endif"/>
                    <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>订单编号</th>
                        {{--<th>归属商家</th>--}}
                        <th>消费金额</th>
                        <th>获得CHQT</th>
                        <th>刷卡使用的Pos机</th>
                        <th>归属管理员</th>
                        <th>创建时间</th>
                        {{--<th>平台分润</th>--}}
                        {{--<th>刷卡扣费</th>--}}
                        <th>订单状态</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($order_list as $k=>$v)
                            <tr>
                                <td>{{$v->order_sn}}</td>
                                <td>{{get_last_two_num($v->actual_amount)}}</td>
                                <td>{{get_last_two_num($v->actual_amount/20)}}</td>
                                <td>{{$v->terminalId}}</td>
                                <td>{{$v->mobile or '暂无'}}</td>
                                {{--<td>{{$v->platform_amount or 0}}</td>--}}
                                {{--<td>{{$v->poundage or 0}}</td>--}}
                                <td>{{$v->created_at or '暂无'}}</td>
                                <td>
                                    @if($v->status==1)
                                        <span class="text-primary">处理中</span>
                                    @elseif($v->status==2)
                                        <span class="text-success">支付成功</span>
                                    @elseif($v->status==3)
                                        <span class="text-danger">支付失败</span>
                                    @endif
                                </td>
                          </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            {{--</div>--}}
            <div class="panel-footer">
                <div class="row">
                    {{ $order_list->appends([
                    'mobile'=>isset($_GET['mobile'])?$_GET['mobile']:'',
                    'order_sn'=>isset($_GET['order_sn'])?$_GET['order_sn']:'',
                    'terminalId'=>isset($_GET['terminalId'])?$_GET['terminalId']:''
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $order_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){

            $('#search').click(function(){
                location.href="{{url('/order/order_list')}}?mobile="+$('#mobile').val()+"&order_sn="+$('#order_sn').val()+"&terminalId="+$('#terminalId').val();
            });

        });
    </script>
@endsection