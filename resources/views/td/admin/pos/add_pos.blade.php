@extends('td.admin.layouts.app')

<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" xmlns="http://www.w3.org/1999/html"
      xmlns="http://www.w3.org/1999/html"/>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">添加Pos机</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 每次收益提现扣除3块手续费</div>--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="@if(!isset($_GET['id'])||empty($_GET['id'])) {{url('/pos/add_pos_submit')}} @else {{url('/pos/edit_pos_submit')}}  @endif" class="form-horizontal"enctype='multipart/form-data'>
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$_GET['id'] or 0}}" name="id">
                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">代付用户名 </label>--}}
                        {{--<div class="col-sm-10" >--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label"style="text-align: left">{{$bank_info->mobile}}</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}



                    @if(!isset($_GET['id'])||empty($_GET['id']))
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">终端号</br> </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="terminalId"   placeholder="终端号"  value="{{$info->terminalId or old('terminalId')}}">
                                @if ($errors->has('terminalId'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('terminalId') }}</span>
                                @endif
                            </div>
                        </div>
                    @else
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">终端号</br> </label>
                            <div class="col-sm-10">
                                <label class="control-label">{{$info->terminalId or old('terminalId')}}</label>
                            </div>
                        </div>
                    @endif
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">管理员手机号</br> </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mobile"   placeholder="管理员手机号"  value="{{$info->mobile or old('mobile')}}">
                            @if ($errors->has('mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                            @endif
                        </div>
                    </div>


                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                            <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                    <a href="{{url('/pos/pos_list')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>




                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>

@endsection
@section('js')
    <script>
    </script>
@endsection