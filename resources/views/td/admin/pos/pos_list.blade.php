@extends('td.admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">Pos机管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding">--}}
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
            <div class="col-sm-6">
                <input type="text" id="terminalId" class="form-control" placeholder="终端号" style="width: 200px;float:left;" value="@if(isset($_GET['terminalId'])&&(!empty($_GET['terminalId']))){{$_GET['terminalId']}}@endif"/>
                <input type="text" id="mobile" class="form-control" placeholder="管理员手机号" style="width: 200px;float:left;" value="@if(isset($_GET['mobile'])&&(!empty($_GET['mobile']))){{$_GET['mobile']}}@endif"/>
                <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
            </div>

                <div class="col-sm-6" style="float: right ;margin: 10px 0px;">
                    @if(session('agent_info')->id!=1)
                        <a href="{{url('/pos/add_pos')}}" class="btn btn-info" style="float: right;"><i class="fa fa-plus"></i> 添加Pos机</a>
                    @endif
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>终端号</th>
                        <th>绑定管理员</th>
                        <th>创建时间</th>
                        <th>编辑</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($pos_list as $k=>$v)
                            <tr>
                                <td>{{$v->terminalId}}</td>
                                <td>{{$v->mobile}}</td>
                                <td>{{$v->created_at or '暂无'}}</td>
                                <td>
                                    <a href="{{url('/pos/add_pos')}}?id={{$v['id']}}"><span class="label label-warning"><i class="fa fa-edit"></i> 编辑</span>
                                    </a>
                                </td>
                          </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            {{--</div>--}}
            <div class="panel-footer">
                <div class="row">
                    {{ $pos_list->appends([
                    'mobile'=>isset($_GET['mobile'])?$_GET['mobile']:'',
                    'terminalId'=>isset($_GET['terminalId'])?$_GET['terminalId']:''
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $pos_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){

            $('#search').click(function(){
                location.href="{{url('/pos/pos_list')}}?mobile="+$('#mobile').val()+"&terminalId="+$('#terminalId').val();
            });

        });
    </script>
@endsection