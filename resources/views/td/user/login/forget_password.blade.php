<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <title>未特商城珍藏版用户后台</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->

    <link rel="stylesheet" href="{{asset('td/css/supersized.css')}}">
    <link rel="stylesheet" href="{{asset('td/css/login.css')}}">
    <link href="{{asset('td/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="{{asset('td/js/html5.js')}}"></script>
    <![endif]-->
    <script src="{{asset('td/js/jquery-1.8.2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('td/js/jquery.form.js')}}"></script>
    <script type="text/javascript" src="{{asset('td/js/tooltips.js')}}"></script>
    <script type="text/javascript" src="{{asset('td/js/login.js')}}"></script>
</head>

<body style="background-color: black">

<div class="page-container">
    <div class="main_box" style="height: 550px;">
        <div class="login_box">
            <div class="login_logo">
                <img src="{{asset('td/images/logo.png')}}" >
            </div>

            <div class="login_form">
                <form action="{{url('forget_password_submit')}}" id="login_form" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="j_username" class="t">手机号码：</label>
                        <input id="mobile" value="{{old('mobile')}}" name="mobile" type="text" class="form-control x319 in"
                               autocomplete="off">
                        @if ($errors->has('mobile'))
                            <span style="width: 319px; margin-left: 123px; font-weight: 900" class="text-danger">{{ $errors->first('mobile') }}</span>
                        @else
                            <span style="width: 319px; margin-left: 123px; font-weight: 900" class="text-primary"></span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="j_password" class="t">新密码：</label>
                        <input id="password" value="{{old('password')}}" name="password" type="password"
                               class="password form-control x319 in">
                        @if ($errors->has('password'))
                            <span style="width: 319px; margin-left: 123px; font-weight: 900" class="text-danger">{{ $errors->first('password') }}</span>
                        @else
                            <span style="width: 319px; margin-left: 123px; font-weight: 900" class="text-primary"></span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="j_password" class="t">确认密码：</label>
                        <input id="password_confirmation" value="{{old('password_confirmation')}}" name="password_confirmation" type="password"
                               class="password form-control x319 in">
                        @if ($errors->has('password_confirmation'))
                            <span style="width: 319px; margin-left: 123px; font-weight: 900" class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                        @else
                            <span style="width: 319px; margin-left: 123px; font-weight: 900" class="text-primary"></span>
                        @endif
                    </div>
                    <div class="form-group" style="height: 57px">
                        <label for="j_captcha" class="t">验证码：</label>
                        <input id="sms_code" name="sms_code" type="text" class="form-control x164 in" autocomplete="off" value="{{old('sms_code')}}">
                        <input type="button" class="btn btn-secondary" id="btn" value="发送手机验证码"  onclick="send_sms()">
                        @if ($errors->has('sms_code'))
                            <span style="width: 319px; margin-left: 123px; font-weight: 900" class="text-danger">{{ $errors->first('sms_code') }}</span>
                        @elseif($errors->has('forget_error'))
                            <span style="width: 319px; margin-left: 123px; font-weight: 900" class="text-danger">{{ $errors->first('forget_error') }}</span>
                        @else
                            <span style="width: 319px; margin-left: 123px; font-weight: 900" class="text-primary"></span>
                        @endif
                        {{--<img id="captcha_img" alt="点击更换" title="点击更换" src="images/captcha.jpeg" class="m">--}}
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<label class="t"></label>--}}
                        {{--<label for="j_remember">--}}
                            {{--<input id="j_remember" type="checkbox" value="true">&nbsp;记住登陆账号!</label>--}}
                        {{--<a id="retrieve" href="forget_password">忘记密码?</a>--}}
                    {{--</div>--}}
                    <div class="form-group space">
                        <label class="t"></label>　　　
                        <button type="submit"
                                class="btn btn-danger btn-lg">&nbsp;修&nbsp;改&nbsp;</button>
                        <a  class="btn btn-default btn-lg" href="{{url('login.html')}}">&nbsp;返&nbsp;回&nbsp;</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="bottom">Copyright &copy; 2018 - 2019 未特商城珍藏版用户登录</div>
    </div>
</div>

<!-- Javascript -->

{{--<script src="{{asset('td/js/supersized.3.2.7.min.js')}}"></script>--}}
{{--<script src="{{asset('td/js/supersized-init.js')}}"></script>--}}
{{--<script src="{{asset('td/js/scripts.js')}}"></script>--}}
<script>
    function send_sms(){
//        mobile = $("#username").html();
//            if(!(/^1[3|4|5|8][0-9]\d{4,8}$/.test(mobile))){
//                alert("请输入正确的手机号码");return false;
//            }
        //发送短信验证码,并设置调用时间
        $.ajax({
            type: "POST",
            url: '{{url('send_sms')}}',
            data: {mobile: $("#mobile").val()},
            dataType: "json",
            success: function (data) {
                if (data.code == 1) {
                    alert('手机短信已发送');
                } else {
                    alert(data.message);
                }
                new invokeSettime("#btn");
            }
        });
    }

    function invokeSettime(obj){
        var countdown=60;
        settime(obj);
        function settime(obj) {
            if (countdown == 0) {
                $(obj).attr("disabled",false);
                $(obj).val("获取验证码");
                countdown = 60;
                return;
            } else {
                $(obj).attr("disabled",true);
                $(obj).val("(" + countdown + ") s 重新发送");
                countdown--;
            }
            setTimeout(function() {
                        settime(obj) }
                    ,1000)
        }
    }
</script>
</body>
</html>