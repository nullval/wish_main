<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <title>创观会员对接用户管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->

    <link rel="stylesheet" href="{{asset('td/css/supersized.css')}}">
    <link rel="stylesheet" href="{{asset('td/css/login.css')}}">
    <link href="{{asset('td/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="{{asset('td/js/html5.js')}}"></script>
    <![endif]-->
    <script src="{{asset('td/js/jquery-1.8.2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('td/js/jquery.form.js')}}"></script>
    <script type="text/javascript" src="{{asset('td/js/tooltips.js')}}"></script>
    <script type="text/javascript" src="{{asset('td/js/login.js')}}"></script>
    <style>
        #retrieve {
            padding-left: 125px;
            color: #ff2222;
        }
    </style>
</head>

<body style="background-color: black">

<div class="page-container">
    <div class="main_box">
        <div class="login_box">
            <div class="login_logo" style='color:white;font-size:30px'>
                创观会员对接用户管理系统
            </div>

            <div class="login_form">
                <form action="{{url('user_login_submit')}}" id="login_form" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="j_username" class="t">手机号码：</label>
                        <input id="email" value="{{old('mobile')}}" name="mobile" type="text" class="form-control x319 in"
                               autocomplete="off">
                        @if ($errors->has('mobile'))
                            <span style="width: 319px; margin-left: 123px; font-weight: 900" class="text-danger">{{ $errors->first('mobile') }}</span>
                            @else
                            <span style="width: 319px; margin-left: 123px; font-weight: 900" class="text-primary"></span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="j_password" class="t">密&nbsp;&nbsp;&nbsp;　码：</label>
                        <input id="password" value="{{old('password')}}" name="password" type="password"
                               class="password form-control x319 in">
                        @if ($errors->has('password'))
                            <span style="width: 319px; margin-left: 123px; font-weight: 900" class="text-danger">{{ $errors->first('password') }}</span>
                            @elseif($errors->has('login_error'))
                            <span style="width: 319px; margin-left: 123px; font-weight: 900" class="text-danger">{{ $errors->first('login_error') }}</span>
                            @else
                            <span style="width: 319px; margin-left: 123px; font-weight: 900" class="text-primary"></span>
                        @endif
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<label for="j_captcha" class="t">验证码：</label>--}}
                        {{--<input id="j_captcha" name="j_captcha" type="text" class="form-control x164 in">--}}
                        {{--<img id="captcha_img" alt="点击更换" title="点击更换" src="images/captcha.jpeg" class="m">--}}
                    {{--</div>--}}
                    <div class="form-group">
                        <label class="t"></label>
                        <label for="j_remember">
                            <input id="j_remember" type="checkbox" value="true">&nbsp;记住登陆账号!</label>
                        <a id="retrieve" href="forget_password">忘记密码?</a>
                    </div>
                    <div class="form-group space">
                        <label class="t"></label>　　　
                        <button type="submit"
                                class="btn btn-danger btn-lg">&nbsp;登&nbsp;录&nbsp </button>
                        <input type="reset" value="&nbsp;重&nbsp;置&nbsp;" class="btn btn-default btn-lg">
                    </div>
                </form>
            </div>
        </div>
        <div class="bottom">Copyright &copy; 2018 - 2019 创观会员对接用户管理系统</div>
    </div>
</div>

<!-- Javascript -->

<script src="{{asset('js/supersized.3.2.7.min.js')}}"></script>
<script src="{{asset('js/supersized-init.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>
<script>
    @if(Session::has('status'))
        alert("{{Session::get('status')}}");
    @endif
</script>
</body>
</html>