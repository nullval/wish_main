@extends('td.user.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" />
@section('content')
    <div class="panel panel-headline">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">用户信息</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/index/edit_user_info_submit')}}" class="form-horizontal" enctype='multipart/form-data'>
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$info->id or old('id')}}" name="id">
                    <input type="hidden" value="{{$info->uid or old('uid')}}" name="uid">

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">我的总CHQ </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{get_last_two_num($user_info->use_td+$user_info->freeze_td+$user_info->used_td)}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">待释放CHQ </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{get_last_two_num($user_info->freeze_td)}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">可用CHQ </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{get_last_two_num($user_info->use_td)}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">我的区块链地址 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="blockchain"   placeholder="区块链地址" value="{{$user_info->blockchain or old('blockchain')}}">
                            @if ($errors->has('blockchain'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('blockchain') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-plus"></i> 提交</button>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
<script>
    $(document).ready(function(){
        $("#up_img_WU_FILE_0").change(function(){
            var v = $(this).val();
            var reader = new FileReader();
            reader.readAsDataURL(this.files[0]);
            reader.onload = function(e){
                console.log(e.target.result);
                $('#uploadfile').val(e.target.result);
            };
        });
    });
    function getbankNo() {
        var bank_name=$("select").val();
        $.ajax({
            url:'{{url('/seller/get_bankNo')}}',
            type:'POST', //GET
            async:true,    //或false,是否异步
            data:{
                bank_name:bank_name
            },
            timeout:5000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success:function(data){

                if (data.code==0){
                    alert(data.message)
                }else {
                    $('#ibankno').val(data.data.lbnkNo);
                    $('#account_bank').val(data.data.bank_name);
                }
            },
            error:function(xhr,textStatus){
                console.log('错误')
                console.log(xhr)
                console.log(textStatus)
            },

        })

    }


</script>
@endsection