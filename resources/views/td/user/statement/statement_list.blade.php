@extends('td.user.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">释放记录</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding">--}}
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-12">--}}
                    {{--<input type="text" id="order_sn" class="form-control" placeholder="订单编号" style="width: 200px;float:left;" value="@if(isset($_GET['order_sn'])&&(!empty($_GET['order_sn']))){{$_GET['order_sn']}}@endif"/>--}}
                    {{--<input type="text" id="mobile" class="form-control" placeholder="商家手机号" style="width: 200px;float:left;" value="@if(isset($_GET['mobile'])&&(!empty($_GET['mobile']))){{$_GET['mobile']}}@endif"/>--}}
                    {{--<a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>--}}
                {{--</div>--}}
                <table class="table table-striped">
                    <thead>
                    <tr>
                        {{--<th>归属商家</th>--}}
                        <th>释放日期</th>
                        <th>总释放CHQ</th>
                        <th>剩余释放CHQ</th>
                        <th>状态</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($statement_list as $k=>$v)
                            <tr>
                                <td>{{$v->u_date}}</td>
                                <td>{{get_last_two_num($v->amount)}}</td>
                                <td>{{get_last_two_num($v->left_amount)}}</td>
                                <td>
                                    @if($v->status==1)
                                        未释放
                                        @else
                                        已释放
                                    @endif
                                </td>
                          </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            {{--</div>--}}
            <div class="panel-footer">
                <div class="row">
                    {{ $statement_list->appends([
                    'mobile'=>isset($_GET['mobile'])?$_GET['mobile']:'',
                    'order_sn'=>isset($_GET['order_sn'])?$_GET['order_sn']:''
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $statement_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){

            $('#search').click(function(){
                location.href="{{url('/order/order_list')}}?mobile="+$('#mobile').val()+"&order_sn="+$('#order_sn').val();
            });

        });
    </script>
@endsection