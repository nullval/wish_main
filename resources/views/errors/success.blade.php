<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge，chrome=1">
    <title>{{$title or '成功提示'}}</title>
    <style type="text/css" media="screen">
        html,body{text-align: center;font-family: 'Avenir', Helvetica, Arial, sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            color: #000;
            height:100%;
            overflow:hidden;

            position:relative;
            font-size: 16px;}
        .content{position:absolute;left:0;right:0;top:50%;transform:translateY(-100%);}
        img{width:50px;vertical-align: middle;margin-bottom: 30px;}
    </style>
</head>
<body>
<div class="content">
    <img src="{{asset("agent/img/success_logo.png")}}">
    <div>
        {{$message}}
    </div>
</div>

</body>
</html>