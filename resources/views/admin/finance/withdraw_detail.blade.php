@extends('admin.layouts.app')
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">提现申请详情</h3>
                <div class="hr-line-solid"></div>
            </div>
            <form method="post"  action="{{url('/finance/withdraw_submit')}}">
                <div class="panel-body">
                    <input type="hidden" value="{{$info->id}}" name="id">
                    <input type="hidden" value="{{$_GET['year']}}" name="year">
                    <input type="hidden" value="{{$_GET['member_type']}}" name="member_type">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" >提现编号</label>
                        <label for="signin-text" class="col-sm-10 control-label" >{{$info->id}}</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" >会员编号</label>
                        <label for="signin-text" class="col-sm-10 control-label" >{{$_GET['mobile']}}</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" >开户名</label>
                        <label for="signin-text" class="col-sm-10 control-label" >{{$info->account_name}}</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">开户行</label>
                        <label for="signin-text" class="col-sm-10 control-label" >{{$info->account_bank}}</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>



                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">银行卡号</label>
                        <label for="signin-text" class="col-sm-10 control-label" >{{$info->bank_account}}</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">提现金额</label>
                        <label for="signin-text" class="col-sm-10 control-label" >{{get_last_two_num($info->amount)}}</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">实到金额</label>
                        <label for="signin-text" class="col-sm-10 control-label" >{{get_last_two_num($info->actual_amount)}}</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">手续费</label>
                        <label for="signin-text" class="col-sm-10 control-label" >{{get_last_two_num($info->d_poundage)}}</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">申请状态</label>
                        <label for="signin-text" class="col-sm-10 control-label" >
                            @if($info->status==1)    处理中
                            @elseif($info->status==2) 审核通过
                            @elseif($info->status==3) 审核失败
                            @endif</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">申请时间</label>
                        <label for="signin-text" class="col-sm-10 control-label" >{{$info->created_at}}</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    @if($info->status!=1)
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">审核时间</label>
                            <label for="signin-text" class="col-sm-10 control-label" >{{$info->updated_at}}</label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">申请反馈</label>
                            <label for="signin-text" class="col-sm-10 control-label" >{{$info->remark}}</label>

                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                        <a href="{{url('/finance/withdraw_list')}}?year={{$_GET['year']}}&member_type={{$_GET['member_type']}}" class="btn btn-success" style="float:right;"><i class="fa fa-angle-double-left"></i> 返回</a>
                    @else
                        <a href="{{url('/finance/withdraw_list')}}?year={{$_GET['year']}}&member_type={{$_GET['member_type']}}" class="btn btn-success" style="float:right;"><i class="fa fa-angle-double-left"></i> 返回</a>

                        {{--<div class="form-group">--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label" id="text_name">审核</label>--}}
                            {{--<div class="col-sm-10">--}}
                                {{--<input type="radio" class="control-label" name="status" value="2" checked/> 审核通过--}}
                                {{--<input type="radio" class="control-label" name="status" value="3"/> 审核失败--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                        {{--<div class="hr-line-dashed"></div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label" id="text_name">申请反馈</label>--}}
                            {{--<div class="col-sm-10">--}}
                                {{--<textarea name="remark" class="form-control" rows="3"></textarea>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                        {{--<div class="hr-line-dashed"></div>--}}
                        {{--<button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 提交</button>--}}
                    @endif
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
@endsection