@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">提现管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="year">--}}
                        {{--<option value="2017">2017</option>--}}
                        {{--<option value="2018" @if(isset($_GET['year'])&&$_GET['year']==2018||date('Y')==2018) selected @endif>2018</option>--}}
                        {{--<option value="2019" @if(isset($_GET['year'])&&$_GET['year']==2019||date('Y')==2019) selected @endif>2019</option>--}}
                        {{--<option value="2020" @if(isset($_GET['year'])&&$_GET['year']==2020||date('Y')==2020) selected @endif>2020</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="col-sm-2">
                    <select class="form-control" id="status">
                        <option value="0">--状态--</option>
                        <option value="1" @if($_GET['status']==1) selected @endif>处理中</option>
                        <option value="2" @if($_GET['status']==2) selected @endif>处理成功</option>
                        <option value="3" @if($_GET['status']==3) selected @endif>处理失败</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="form-control" id="purse_type">
                        <option value="0">--提现类型--</option>
                        @foreach(config('pay.withdraw_type') as $k=>$v)
                            <option value="{{$k}}" @if($_GET['purse_type']==$k) selected @endif>{{$v}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="form-control" id="member_type">
                        <option value="1" @if($_GET['member_type']==1) selected @endif>用户</option>
                        <option value="2" @if($_GET['member_type']==2) selected @endif>商家</option>
                        <option value="3" @if($_GET['member_type']==3) selected @endif>机主</option>
                        <option value="4" @if($_GET['member_type']==4) selected @endif>系统</option>
                        <option value="5" @if($_GET['member_type']==5) selected @endif>运营中心</option>
                    </select>
                </div>
                <div class="col-sm-4">
                    <a class='input-group date' id='start_time' style="float:left">
                        <input type='text' class="form-control" id='start_time_value' style="width: 150px; height: 30px;" value="{{$_GET['start_time']}}"/>
                        <span class="input-group-addon" style="float: left; width: 50px; height: 30px;">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </a>
                    <span style="float:left" >  ~  </span>
                    <a class='input-group date' id='end_time' style="float:left">
                        <input type='text' class="form-control" id='end_time_value' style="width: 150px; height: 30px;" value="{{$_GET['end_time']}}"/>
                        <span class="input-group-addon" style="float: left; width: 50px; height: 30px;">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </a>
                </div>
                <div class="col-sm-12" style="margin-top: 10px;">
                    <input type="text" id="mobile" class="form-control" placeholder="手机号码" style="width: 200px;float:left;" value="{{$_GET['mobile']}}"/>
                    <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
                    <div class="form-group">
                        <label style="padding:9px 100px 0 0;float:right;" >累计总数: {{$data['total']}}元</label>
                    </div>
                </div>

                <div class="col-sm-12" style="margin-top:10px;" >
                    <span class="form-control">第三方可用余额:{{$data['e_withdraw']}}</span>
                </div>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>提现编号</th>
                        @if(!isset($_GET['member_type'])||isset($_GET['member_type'])&&$_GET['member_type']!=4) <th>手机号码</th> @endif
                        <th>上游查询编号</th>
                        <th>会员类型</th>
                        <th>开户名</th>
                        <th>开户行</th>
                        <th>银行卡号</th>
                        <th>提现类型</th>
                        <th>提现金额</th>
                        <th>实到金额</th>
                        <th>手续费</th>
                        <th>申请时间</th>
                        <th>状态</th>
                        <th>备注</th>
                        <th>审核</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user_withdraw as $k=>$v)
                        <tr>
                            <td>{{$v->id}}</td>
                            @if($v->member_type!=4) <td>{{$v->mobile}}</td> @endif
                            <td>{{$v->spare_order_sn}}</td>
                            <td>
                                @if($v->member_type==1)    会员
                                @elseif($v->member_type==2) 商家
                                @elseif($v->member_type==3) 机主
                                @elseif($v->member_type==4) 系统
                                @endif
                            </td>
                            <td>{{$v->account_name}}</td>
                            <td>{{$v->account_bank}}</td>
                            <td>{{$v->bank_account}}</td>
                            <td>{{get_withdraw_type_name($v->purse_type)}}</td>
                            <td>{{get_last_two_num($v->amount)}}</td>
                            <td>{{get_last_two_num($v->actual_amount)}}</td>
                            <td>{{get_last_two_num($v->d_poundage)}}</td>
                            <td>{{$v->created_at}}</td>
                            <td>
                                @if($v->status==1)    处理中
                                    @elseif($v->status==2) 处理成功
                                    @elseif($v->status==3) 处理失败
                                @endif
                            </td>
                            <td>{{$v->remark}}</td>
                            <td>
                                @if($v->status!=1)
                                    <a href="{{url('/finance/withdraw_detail')}}?spare_order_sn={{$v->spare_order_sn}}&mobile={{$v->mobile}}&year={{$_GET['year']}}&member_type={{$_GET['member_type']}}" /><span class="label label-success"><i class="fa fa-eye"></i> 查看</span>
                                @else
                                    <a href="{{url('/finance/withdraw_detail')}}?spare_order_sn={{$v->spare_order_sn}}&mobile={{$v->mobile}}&year={{$_GET['year']}}&member_type={{$_GET['member_type']}}" /><span class="label label-info"><i class="fa fa-edit"></i> 审核</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $user_withdraw->appends([
                    'status'=>$_GET['status'] ?? 0,
                    'mobile'=>$_GET['mobile'] ?? '',
                    'member_type'=>$_GET['member_type'] ?? 1,
                    'purse_type'=>$_GET['purse_type'] ?? 0,
                    'start_time'=>$_GET['start_time'] ?? '',
                    'end_time'=>$_GET['end_time'] ?? ''
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $user_withdraw->total()}}</a>
                        </li>
                    </ul>
                    {{--<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>--}}
                    {{--<div class="col-md-6 text-right"><a href="#" class="btn btn-primary">View All Purchases</a></div>--}}
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            $('#status,#year,#member_type,#purse_type').change(function(){
                location_a();
            });
            $('#search').click(function(){
                location_a();
            });
            Datetime('#start_time');
            Datetime('#end_time');
        });
        function location_a(){
            location.href="{{url('/finance/withdraw_list')}}?status="+$('#status').val()
                +"&mobile="+$('#mobile').val()
                +"&member_type="+$('#member_type').val()
                +"&purse_type="+$('#purse_type').val()
                +"&start_time="+$("#start_time").find("input").val()
                +"&end_time="+$("#end_time").find("input").val();
        }
    </script>
@endsection