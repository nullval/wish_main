@extends('admin.layouts.app')
{{--<link rel="stylesheet" href="//apps.bdimg.com/libs/jqueryui/1.10.4/css/jquery-ui.min.css">--}}
<link rel="stylesheet" href="{{asset('jquery-ui-1.10.4.custom/css/base/jquery-ui-1.10.4.custom.min.css')}}">
@section('content')<div class="col-md-12">
	{{--功能开发中！！！！--}}
	<!-- RECENT PURCHASES -->
	<div class="panel">
		<div class="panel-heading">
			<h3 class="panel-title">转账统计</h3>
			<div class="right">
				<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
				<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
			</div>
		</div>
		<div class="panel-body no-padding">
			<div class="col-sm-12">
				@if(Session::has('status'))
					<div class="alert alert-info"> {{Session::get('status')}}</div>
				@endif
			</div>

			<div class="col-sm-1" style="padding: 8px 6px;margin-left: 16px;">
				<span for="from" class="text"></span>
				<span for="from" class="">开始日期：</span>
			</div>
			<div class="col-sm-2">
				<input type="text" id="from" name="from" class="form-control"   >
				<input type="hidden" id="from1" value="@if(isset($_GET['from'])&&(!empty($_GET['from']))){{$_GET['from']}}@endif">
			</div>
			<div class="col-sm-1" style="padding: 6px 12px;">
				<span for="to" class="text">结束日期：</span>
			</div>
			<div class="col-sm-2">
				<input type="text" id="to" name="to" class="form-control" >
				<input type="hidden" id="to1" value="@if(isset($_GET['to'])&&(!empty($_GET['to']))){{$_GET['to']}}@endif">

			</div>
			<div class="col-sm-4">
				<a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
			</div>
			<table class="table table-striped">
				<thead>
				<tr>
					<th>转账编号</th>
					<th>转账名称</th>
					<th>总金额</th>
					<th>备注</th>
					<th>详情</th>
				</tr>
				</thead>
				<tbody>
				@foreach($statistics as $k => $v)
					<tr>
						<td>{{$v->reason_code or '暂无'}}</td>
						<td>{{$v->reason_name or '暂无'}}</td>
						<td><span class="text-primary">{{get_last_two_num($v->amount)}}</span></td>
						<td>{{$v->remarks or '暂无'}}</td>
						<td><a href="{{url('/finance/Transfer_detail')}}?id={{$v->reason_id}}&from={{$_GET['from']}}&to={{$_GET['to']}}" /><span class="label label-success"><i class="fa fa-eye"></i> 查看</span></td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
		<div class="panel-footer">
			<div class="row">
				{{ $statistics->appends([
				'from'=>isset($_GET['from'])?$_GET['from']:'',
				'to'=>isset($_GET['to'])?$_GET['to']:''
				])->links() }}
				{{--<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>--}}
				{{--<div class="col-md-6 text-right"><a href="#" class="btn btn-primary">View All Purchases</a></div>--}}
			</div>
		</div>
	</div>
	<!-- END RECENT PURCHASES -->
</div>


@endsection
@section('js')
	<script>

		$(function() {
			$( "#from" ).datepicker({
//				defaultDate: "+1w",
				changeMonth: true,
				numberOfMonths: 2,
				onClose: function( selectedDate ) {
					$( "#to" ).datepicker( "option", "minDate", selectedDate );
				}
			});
			$( "#to" ).datepicker({
//				defaultDate: "+1w",
				changeMonth: true,
				numberOfMonths: 2,
				onClose: function( selectedDate ) {
					$( "#from" ).datepicker( "option", "maxDate", selectedDate );
				}
			});
			$( "#from" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );
			$( "#to" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );

		});
		$(function(){
			$('#search').click(function(){
				location_a();
			});
		});
		function location_a(){

			location.href="{{url('/finance/Transfer_statistics')}}?from="+$('#from').val()+"&to="+$('#to').val();
		}
		$(function(){
			if ($('#from').val() != null){
				$('#from').val($('#from1').val())
			}
			if ($('#to').val() != null){
				$('#to').val($('#to1').val())
			}
		});

	</script>

@endsection