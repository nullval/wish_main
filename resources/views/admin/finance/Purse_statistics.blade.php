@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">钱包统计</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding" style="margin-bottom: 20px;">--}}
                {{--<div class="col-sm-12">--}}
                    {{--<span class="form-control">第三方代付商户号: {{$data['username']}}  </span>--}}
                {{--</div>--}}
                {{--<div class="col-sm-12">--}}
                    {{--<span class="form-control">账户可用余额 :{{$data['balance']}}&nbsp;&nbsp;  冻结资金 :{{$data['freeze_value']}} </span>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="year">--}}
                        {{--<option value="2017">2017</option>--}}
                        {{--<option value="2018" @if(isset($_GET['year'])&&$_GET['year']==2018||date('Y')==2018) selected @endif>2018</option>--}}
                        {{--<option value="2019" @if(isset($_GET['year'])&&$_GET['year']==2019||date('Y')==2019) selected @endif>2019</option>--}}
                        {{--<option value="2020" @if(isset($_GET['year'])&&$_GET['year']==2020||date('Y')==2020) selected @endif>2020</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="status">--}}
                        {{--<option value="0">全部</option>--}}
                        {{--<option value="1" @if(isset($_GET['status'])&&$_GET['status']==1) selected @endif>收入</option>--}}
                        {{--<option value="2" @if(isset($_GET['status'])&&$_GET['status']==2) selected @endif>支出</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="type">--}}
                        {{--<option value="1" @if(isset($_GET['type'])&&$_GET['type']==1) selected @endif>系统收益钱包</option>--}}
                        {{--<option value="2" @if(isset($_GET['type'])&&$_GET['type']==2) selected @endif>中央银行</option>--}}
                        {{--<option value="5" @if(isset($_GET['type'])&&$_GET['type']==5) selected @endif>系统积分钱包</option>--}}
                        {{--<option value="3" @if(isset($_GET['type'])&&$_GET['type']==3) selected @endif>第三方代付收益钱包</option>--}}
                        {{--<option value="4" @if(isset($_GET['type'])&&$_GET['type']==4) selected @endif>通联pos机消费收益钱包</option>--}}
                        {{--<option value="6" @if(isset($_GET['type'])&&$_GET['type']==6) selected @endif>高汇通代付收益钱包</option>--}}
                        {{--<option value="7" @if(isset($_GET['type'])&&$_GET['type']==7) selected @endif>高汇通pos机消费收益钱包</option>--}}
                        {{--<option value="10" @if(isset($_GET['type'])&&$_GET['type']==10) selected @endif>积分商城积分钱包</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                {{--<div class="col-sm-6">--}}
                    {{--<span class="form-control">余额 :--}}
                        {{--@if(isset($_GET['type'])&&$_GET['type']==1)--}}
                            {{--{{get_last_two_num($data['purse_info']->balance-1000000000.0000)}}--}}
                        {{--@else--}}
                            {{--{{get_last_two_num($data['purse_info']->balance)}}--}}
                        {{--@endif--}}

                        {{--初始金额 :--}}
                        {{--@if(isset($_GET['type'])&&$_GET['type']==2)--}}
                            {{--1000000000.0000--}}
                        {{--@else--}}
                            {{--0--}}
                        {{--@endif--}}
                    {{--</span>--}}
                {{--</div>--}}
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>用户类型</th>
                        {{--<th>收益钱包</th>--}}
                        {{--<th>现金钱包</th>--}}
                        {{--<th>积分钱包</th>--}}
                        {{--<th>货款钱包</th>--}}
                        {{--<th>商家充值钱包</th>--}}
                        {{--<th>消费积分</th>--}}

                        @foreach($Purse_statistics['purse_type_list'] as $k => $v)
                            <th>{{$v}}</th>
                        @endforeach
                        {{--<th>TD</th>--}}
                        {{--<th>运营中心佣金(单位：元)</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($Purse_statistics['data'] as $k => $v)
                        <tr>
                            <td>{{$v['name']}}</td>
                            @foreach($v as $item => $value)
                                @if($item != 'name')
                                    <td>余额：<span class="text-danger">+{{get_last_two_num($value['balance'])}}</span>&nbsp;&nbsp;冻结：<span class="text-success">{{get_last_two_num($value['freeze_value'])}}</span></td>
                                @endif
                            @endforeach

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                {{--<div class="row">--}}
                    {{--{{ $transfer->appends(['status'=>isset($_GET['status'])?$_GET['status']:0,--}}
                    {{--'type'=>isset($_GET['type'])?$_GET['type']:0,--}}
                    {{--'year'=>isset($_GET['year'])?$_GET['year']:date('Y')--}}
                    {{--])->links() }}--}}
                    {{--<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>--}}
                    {{--<div class="col-md-6 text-right"><a href="#" class="btn btn-primary">View All Purchases</a></div>--}}
                {{--</div>--}}
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    {{--<script>--}}
        {{--$(function(){--}}
            {{--$('#status,#type,#year').change(function(){--}}
                {{--location.href="{{url('/finance/index')}}?type="+$('#type').val()+"&status="+$('#status').val()+"&year="+$('#year').val();--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
@endsection