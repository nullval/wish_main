@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">流水管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding" style="margin-bottom: 20px;">--}}
                {{--<div class="col-sm-12">--}}
                    {{--<span class="form-control">第三方代付商户号: {{$data['username']}}  </span>--}}
                {{--</div>--}}
                {{--<div class="col-sm-12">--}}
                    {{--<span class="form-control">账户可用余额 :{{$data['balance']}}&nbsp;&nbsp;  冻结资金 :{{$data['freeze_value']}} </span>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="year">--}}
                        {{--<option value="2017">2017</option>--}}
                        {{--<option value="2018" @if(isset($_GET['year'])&&$_GET['year']==2018||date('Y')==2018) selected @endif>2018</option>--}}
                        {{--<option value="2019" @if(isset($_GET['year'])&&$_GET['year']==2019||date('Y')==2019) selected @endif>2019</option>--}}
                        {{--<option value="2020" @if(isset($_GET['year'])&&$_GET['year']==2020||date('Y')==2020) selected @endif>2020</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="col-sm-2">
                    <select class="form-control" id="status">
                        <option value="0">全部</option>
                        <option value="1" @if($_GET['status']==1) selected @endif>收入</option>
                        <option value="2" @if($_GET['status']==2) selected @endif>支出</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="form-control" id="purse_id">
                        @foreach ($sys_purse as $k=>$v)
                            <option value="{{$v->purse_id}}" @if($_GET['purse_id']==$v->purse_id) selected @endif>{{$v->remarks}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-6">
                    <span class="form-control">余额 :
                        @if(isset($_GET['type'])&&$_GET['type']==1)
                            {{get_last_two_num($data['purse_info']->balance-1000000000.0000)}}
                        @else
                            {{get_last_two_num($data['purse_info']->balance)}}
                        @endif

                        初始金额 :
                        @if(isset($_GET['type'])&&$_GET['type']==2)
                            1000000000.0000
                        @else
                            0
                        @endif
                    </span>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>流水编号</th>
                        <th>类型</th>
                        <th>转入手机号</th>
                        <th>转入会员类型</th>
                        <th>转出手机号</th>
                        <th>转出会员类型</th>
                        <th>变动额度</th>
                        <th>余额</th>
                        <th>转账时间</th>
                        <th>描述</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($transfer as $k=>$v)
                        <tr>
                            <td>{{$v->transfer_id}}</td>
                            <td>
                                @if($v->into_purse_id==$data['purse_info']->purse_id) 收入
                                @else 支出
                                @endif
                            </td>
                            <td>@if($v->out_purse_id==$data['purse_info']->purse_id){{$v->mobile}}  @else 本账号 @endif</td>
                            <td>{{get_owner_type_name($v->into_owner_type)}}</td>
                            <td>@if($v->into_purse_id==$data['purse_info']->purse_id){{$v->mobile}} @else 本账号 @endif</td>
                            <td>{{get_owner_type_name($v->out_owner_type)}}</td>
                            <td>
                                @if($v->into_purse_id==$data['purse_info']->purse_id) <span class="text-danger">+{{get_last_two_num($v->into_amount)}}</span>
                                @else <span class="text-success">-{{get_last_two_num($v->into_amount)}}</span>
                                @endif
                            </td>
                            <td>
                                @if($v->into_purse_id==$data['purse_info']->purse_id)
                                    @if(isset($_GET['type'])&&$_GET['type']==1)
                                        {{get_last_two_num($v->into_balance-1000000000.0000)}}
                                    @else
                                        {{get_last_two_num($v->into_balance)}}
                                    @endif
                                @else
                                    @if(isset($_GET['type'])&&$_GET['type']==1)
                                        {{get_last_two_num($v->out_balance-1000000000.0000)}}
                                    @else
                                        {{get_last_two_num($v->out_balance)}}
                                    @endif
                                @endif
                            </td>
                            <td>{{date('Y-m-d H:i:s',$v->create_time)}}</td>
                            <td>{{$v->detail}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $transfer->appends(['status'=>$_GET['status'] ?? 0,
                    'purse_id'=>$_GET['purse_id'] ?? 1
                    ])->links() }}
                    {{--<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>--}}
                    {{--<div class="col-md-6 text-right"><a href="#" class="btn btn-primary">View All Purchases</a></div>--}}
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            $('#status,#purse_id').change(function(){
                location.href="{{url('/finance/index')}}?purse_id="+$('#purse_id').val()
                    +"&status="+$('#status').val();
            });
        });
    </script>
@endsection