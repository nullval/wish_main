@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">统计详情</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding" style="margin-bottom: 20px;">
                <div class="col-sm-12">
                    <span class="form-control">转账代码 :{{$reason_code}} </span>
                </div>
                <div class="col-sm-12">
                    <span class="form-control">转账名称: {{$reason_name}}  </span>
                </div>

            {{--</div>--}}

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>流水编号</th>
                        {{--<th>类型</th>--}}
                        <th>转入手机号</th>
                        <th>转入会员类型</th>
                        <th>转出手机号</th>
                        <th>转出会员类型</th>
                        <th>转账额度</th>
                        {{--<th>余额</th>--}}
                        <th>转账时间</th>
                        <th>描述</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($Transfer_detail as $k=>$v)
                        <tr>
                            <td>{{$v->transfer_id}}</td>
                            {{--<td>--}}
                                {{--@if($v->into_purse_id==$data['purse_info']->purse_id) 收入--}}
                                {{--@else 支出--}}
                                {{--@endif--}}
                            {{--</td>--}}
                            <td>{{get_owner_mobile($v->into_owner_type,$v->into_owner_id)}}</td>
                            <td>{{get_owner_type_name($v->into_owner_type,$v->into_purse_id)}}</td>
                            <td>{{get_owner_mobile($v->out_owner_type,$v->out_owner_id)}}</td>
                            <td>{{get_owner_type_name($v->out_owner_type,$v->out_purse_id)}}</td>
                            <td>
                                <span class="text-primary">{{get_last_two_num($v->out_amount)}}</span>
                                {{--@if($v->into_purse_id==$data['purse_info']->purse_id) <span class="text-danger">+{{get_last_two_num($v->into_amount)}}</span>--}}
                                {{--@else <span class="text-success">-{{get_last_two_num($v->into_amount)}}</span>--}}
                                {{--@endif--}}
                            </td>
                            {{--<td>--}}
                                {{--@if($v->into_purse_id==$data['purse_info']->purse_id)--}}
                                    {{--@if(isset($_GET['type'])&&$_GET['type']==1)--}}
                                        {{--{{get_last_two_num($v->into_balance-1000000000.0000)}}--}}
                                    {{--@else--}}
                                        {{--{{get_last_two_num($v->into_balance)}}--}}
                                    {{--@endif--}}
                                {{--@else--}}
                                    {{--@if(isset($_GET['type'])&&$_GET['type']==1)--}}
                                        {{--{{get_last_two_num($v->out_balance-1000000000.0000)}}--}}
                                    {{--@else--}}
                                        {{--{{get_last_two_num($v->out_balance)}}--}}
                                    {{--@endif--}}
                                {{--@endif--}}
                            {{--</td>--}}
                            <td>{{date('Y-m-d H:i:s',$v->create_time)}}</td>
                            <td>{{$v->detail}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $Transfer_detail->appends([
				'from'=>isset($_GET['from'])?$_GET['from']:'',
				'to'=>isset($_GET['to'])?$_GET['to']:'',
				'id'=>isset($_GET['id'])?$_GET['id']:''
				])->links() }}
                    {{--<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>--}}
                    {{--<div class="col-md-6 text-right"><a href="#" class="btn btn-primary">View All Purchases</a></div>--}}
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    {{--<script>--}}
        {{--$(function(){--}}
            {{--$('#status,#type,#year').change(function(){--}}
                {{--location.href="{{url('/finance/index')}}?type="+$('#type').val()+"&status="+$('#status').val()+"&year="+$('#year').val();--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
@endsection