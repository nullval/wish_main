@extends('admin.layouts.app')

@section('content')
    <div class="panel panel-headline">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">地推二维码申请订单详情</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 消费分润比例即用户消费后分给商家的比例,20表示20%</div>--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post" id="form"  action="{{url('/order/pushQr_order_detail_submit')}}" class="form-horizontal">
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{$_GET['id']}}">
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">订单编号 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->order_sn}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">收货人 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->contact_user or '暂无'}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">联系手机号 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->contact_mobile or '暂无'}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">收货地址 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->contact_address or '暂无'}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">申请数量 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->count or '暂无'}}</label>
                        </div>
                    </div>
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    {{--<div class="form-group" hidden>--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">申请的地推二维码编号 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<label class="control-label">--}}
                                {{--@foreach($info->id_arr_group as $v)--}}
                                    {{--{{$v}} &nbsp;&nbsp;&nbsp;&nbsp;--}}
                                {{--@endforeach--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">申请时间 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->created_at or '暂无'}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">快递公司 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="shipping_company"   placeholder="快递公司" value="{{$info->shipping_company}}">--}}
                            {{--@if ($errors->has('shipping_company'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('shipping_company') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">快递单号 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="shipping_number"   placeholder="快递单号" value="{{$info->shipping_number}}">--}}
                            {{--@if ($errors->has('shipping_number'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('shipping_number') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">状态 </label>
                        <div class="col-sm-10">
                            <input type="radio" value="0" name="status" @if($info->status==0) checked @endif@if($is_visual==false) readonly@endif/> 未处理
                            <input type="radio" value="1" name="status" @if($info->status==1) checked @endif@if($is_visual==false) readonly@endif/> 已处理
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    @if($is_visual==true)
                    <div class="hr-line-dashed"></div>

                    <button type="submit" class="btn btn btn-info submit" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                    @endif

                    <a class="btn btn-success" style="float:right;margin-right: 10px;" onclick="window.history.go(-1)"><i class="fa fa-angle-double-left"></i> 返回</a>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>

@endsection
@section('js')
@endsection