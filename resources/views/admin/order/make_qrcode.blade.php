@extends('admin.layouts.'.$layouts)
@section('content')
    <div class="panel panel-headline">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">生成二维码</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 地推二维码编号指要打印的二维码编号集合，平台将根据相应的二维码编号发送给您，若不填写则会按填写的申请数量生成新的二维码发送给您</div>--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post" id="form"  action="{{url('/order/make_qrcode')}}" class="form-horizontal">
                <div class="panel-body">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">生成数量 </label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="count" placeholder="下发数量" value="{{old('count')}}" min="1">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <button type="submit" class="btn btn btn-info submit" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                    <a class="btn btn-success" style="float:right;margin-right: 10px;" onclick="window.history.go(-1)"><i class="fa fa-angle-double-left"></i> 返回</a>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
@endsection