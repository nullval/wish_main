@extends('admin.layouts.app')

@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">生成订单</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/order/create_pos_order_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">购买人手机号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mobile"   placeholder="购买人手机号" value="{{old('mobile')}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">推荐人手机号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="invite_mobile"   placeholder="推荐人手机号" value="{{old('invite_mobile')}}"/>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">购买的pos机终端号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="terminalId"   placeholder="购买的pos机终端号" value="{{old('terminalId')}}"/>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">是否直接向公司购买</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="is_direct_buy" value="0" @if($info->is_direct_buy==0) checked @endif/> 市场部购买 ({{config('pos.pos')['second_amount']}}元)
                            <input type="radio" class="control-label" name="is_direct_buy" value="1" @if($info->is_direct_buy==1) checked @endif/> 向公司购买 ({{config('pos.pos')['amount']}}元)
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">	短信安全验证 </label>
                        <div class="col-sm-10">
                            <input type="text"  class="form-control" placeholder="输入短信验证码" style="width: 200px;float:left;" name="code">
                            <a  id="get_code" class="btn btn btn-info" style="float:left;"><i class="fa fa-refresh"></i> 发送短信</a>
                            @if ($errors->has('code'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('code') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>



                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 提交</button>
                </div>
            </form>
        </div>
    </div>



    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
    <script>
        $(function(){
            $('#get_code').click(function(){
                $.ajax({
                    type: "POST",
                    url: '{{url('/config/send_message')}}',
                    data: {send_type:3},
                    dataType: "json",
                    success: function(data){
                        if(data.cod=1){
                            alert(data.message);
                        }else{
                            alert(data.message);
                        }
                    }
                });
            });
        })
    </script>
@endsection