@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">地推二维码下发列表</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>


                <div class="col-sm-12" >
                    <ul class="pagination" style="float: left">
                        <li>
                            <span href="#">目前剩余数量:{{ $sum_data['own_amount'] or 0}}</span>
                        </li>
                    </ul>
                </div>
                {{--<div class="col-sm-12">--}}
                {{--<ul id="myTab" class="nav nav-tabs no-padding">--}}
                    {{--@if($role_id!=1)--}}
                    {{--<li @if(Request::input('type')=='mine' or empty(Request::input('type')))class="active"@endif>--}}
                        {{--<a href="{{url('/order/pushQr_order_list')}}?type=mine">--}}
                            {{--我申请的--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--@endif--}}
                    {{--@if($role_id!=9)--}}
                    {{--<li @if(Request::input('type')=='others' or $role_id==1)class="active"@endif>--}}
                        {{--<a href="{{url('/order/pushQr_order_list')}}?type=others">--}}
                            {{--需审批的--}}
                        {{--</a>--}}
                    {{--</li>--}}
                        {{--@endif--}}
                {{--</ul>--}}
                {{--</div>--}}
                <div class="col-sm-12" style="margin-top: 10px;">
                    <input type="text" id="order_sn" class="form-control" placeholder="订单编号" style="width: 200px;float:left;" value="@if(isset($_GET['order_sn'])&&(!empty($_GET['order_sn']))){{$_GET['order_sn']}}@endif"/>
                    <input type="text" id="operate_mobile" class="form-control" placeholder="手机号码" style="width: 200px;float:left;" value="@if(isset($_GET['operate_mobile'])&&(!empty($_GET['operate_mobile']))){{$_GET['operate_mobile']}}@endif"/>
                    <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
                    @if($role_id!=9)
                        <a href="{{url('/order/add_pushQr_order')}}" class="btn btn-info" style="float: right;"><i class="fa fa-plus"></i> 下发二维码</a>
                    @endif
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>订单编号</th>
                        {{--<th>手机号码</th>--}}
                        <th>状态</th>
                        <th>联系人</th>
                        <th>下发数量</th>
                        <th>创建时间</th>
                        {{--<th>处理时间</th>--}}
                        <th>查看</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($order_list as $k=>$v)
                            <tr>
                                <td>{{$v->order_sn}}</td>
                                {{--<td>{{$v->username or '暂无'}}</td>--}}
                                <td>
                                    @if($v->status==0)
                                        未处理
                                    @elseif($v->status==1)
                                        已处理
                                    @endif
                                </td>
                                <td>{{$v->contact_mobile}}</td>
                                <td>{{$v->count}}</td>
                                <td>{{$v->created_at}}</td>
                                {{--<td>{{$v->finish_time or '暂无'}}</td>--}}
                                <td>
                                    <a href="{{url('/order/pushQr_order_detail')}}?id={{$v->id}}"><span class="label label label-success"><i class="fa fa-eye"></i> 查看</span></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $order_list->appends(request()->input())->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $order_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>

        $('#notify_type,#type').change(function(){
            location_a();
        });
        $('.panel').on('click','#search',function(){
            location_a();
        });
        function location_a() {
            location.href="{{url('/order/pushQr_order_list')}}?operate_mobile="+$('#operate_mobile').val()
                +"&order_sn="+$('#order_sn').val();
        }
    </script>
@endsection