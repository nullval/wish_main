@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">购买未特商城pos机订单管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="year">--}}
                        {{--<option value="2017">2017</option>--}}
                        {{--<option value="2018" @if(isset($_GET['year'])&&$_GET['year']==2018||date('Y')==2018) selected @endif>2018</option>--}}
                        {{--<option value="2019" @if(isset($_GET['year'])&&$_GET['year']==2019||date('Y')==2019) selected @endif>2019</option>--}}
                        {{--<option value="2020" @if(isset($_GET['year'])&&$_GET['year']==2020||date('Y')==2020) selected @endif>2020</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="col-sm-2">
                    <select class="form-control" id="is_true">
                        <option value="0">--是否有效--</option>
                        <option value="1" @if($_GET['is_true']==1) selected @endif>有效</option>
                        <option value="2" @if($_GET['is_true']==2) selected @endif>无效</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="form-control" id="notify_type">
                        <option value="0">--支付类型--</option>
                        @foreach(config('pay.notify_type') as $k=>$v)
                            <option value="{{$k}}" @if($_GET['notify_type']==$k) selected @endif>{{$v['name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-4">
                    <a class='input-group date' id='start_time' style="float:left">
                        <input type='text' class="form-control" id='start_time_value' style="width: 150px; height: 30px;" value="{{$_GET['start_time']}}"/>
                        <span class="input-group-addon" style="float: left; width: 50px; height: 30px;">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </a>
                    <span style="float:left" >  ~  </span>
                    <a class='input-group date' id='end_time' style="float:left">
                        <input type='text' class="form-control" id='end_time_value' style="width: 150px; height: 30px;" value="{{$_GET['end_time']}}"/>
                        <span class="input-group-addon" style="float: left; width: 50px; height: 30px;">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </a>
                </div>
                <div class="col-sm-12" style="margin-top: 10px;">
                    <input type="text" id="order_sn" class="form-control" placeholder="订单编号" style="width: 200px;float:left;" value="{{$_GET['order_sn']}}"/>
                    <input type="text" id="buyer_mobile" class="form-control" placeholder="购买的机主" style="width: 200px;float:left;" value="{{$_GET['buyer_mobile']}}"/>
                    <input type="text" id="agent_mobile" class="form-control" placeholder="所属机主" style="width: 200px;float:left;" value="{{$_GET['agent_mobile']}}"/>
                    <input type="text" id="terminalId" class="form-control" placeholder="刷卡使用的未特商城pos机" style="width: 200px;float:left;" value="{{$_GET['terminalId']}}"/>
                    <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
                </div>
                <div class="col-sm-12" >
                    <ul class="pagination" style="float: left">
                        <li>
                            <span href="#">总计实付金额:￥{{ $sum_data['actual_amount'] or 0}}</span>
                            <span href="#">总计刷卡扣费:￥{{ $sum_data['poundage'] or 0}}</span>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-12" style="float: right;">
                    <a href="{{url('/order/create_pos_order')}}" class="btn btn-info" style="float: right;"><i class="fa fa-plus"></i> 购买pos机</a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>订单编号</th>
                        <th>购买人手机号</th>
                        <th>推荐人手机号</th>
                        <th>消费金额</th>
                        <th>实付金额</th>
                        <th>刷卡扣费</th>
                        <th>购买台数</th>
                        <th>购买的pos机</th>
                        <th>是否公司直购</th>
                        <th>创建时间</th>
                        <th>支付公司</th>
                        <th>支付方式</th>
                        <th>订单状态</th>
                        <th>是否有效</th>
                        <th>查看详情</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($order_list as $k=>$v)
                            <tr>
                                <td>{{$v->order_sn}}</td>
                                <td>{{$v->buyer_mobile or '暂无'}}</td>
                                <td>{{$v->agent_mobile or '暂无'}}</td>
                                <td>{{$v->amount or 0}}</td>
                                <td>{{$v->actual_amount or 0}}</td>
                                <td>{{$v->poundage or 0}}</td>
                                <td>{{$v->pos_num or 0}}</td>
                                <td>{{$v->terminalIdArr or '暂无'}}</td>
                                <td>
                                    @if($v->is_direct_buy==1)
                                        <span class="label label-success">是</span>
                                    @elseif($v->is_direct_buy==0)
                                        <span class="label label-danger">否</span>
                                    @endif
                                </td>
                                <td>{{$v->created_at or 0}}</td>
                                <td>
                                    {{get_notify_type_name($v->notify_type)}}
                                </td>
                                <td>
                                    {{get_pay_type_name($v->pay_type)}}
                                </td>
                                <td>
                                    @if($v->status==1)
                                        <span class="text-primary">处理中</span>
                                    @elseif($v->status==2)
                                        <span class="text-success">支付成功</span>
                                    @elseif($v->status==3)
                                        <span class="text-danger">支付失败</span>
                                    @endif
                                </td>
                                <td>
                                    @if($v->is_true==1)
                                        <span class="text-success">有效</span>
                                    @elseif($v->is_true==2)
                                        <span class="text-danger">无效</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{url('order/pos_order_detail')}}?order_sn={{$v->order_sn}}&mobile={{$v->buyer_mobile}}&year={{$_GET['year'] or date('Y')}}">
                                        <span class="label label-success"><i class="fa fa-eye"></i> 查看</span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $order_list->appends([
                    'terminalId'=>$_GET['terminalId'] ?? '',
                    'buyer_mobile'=>$_GET['buyer_mobile'] ?? '',
                    'agent_mobile'=>$_GET['agent_mobile'] ?? '',
                    'order_sn'=>$_GET['order_sn'] ?? '',
                    'notify_type'=>$_GET['notify_type'] ?? 0,
                    'is_true'=>$_GET['is_true'] ?? 0,
                    'start_time'=>$_GET['start_time'] ?? '',
                    'end_time'=>$_GET['end_time'] ?? '',
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $order_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            $('#is_true,#notify_type').change(function(){
                location_a();
            });
            $('#search').click(function(){
                location_a();
            });
            Datetime('#start_time');
            Datetime('#end_time');
        });
        function location_a() {
            location.href="{{url('/order/pos_order_list')}}?buyer_mobile="+$('#buyer_mobile').val()
                +"&agent_mobile="+$('#agent_mobile').val()
                +"&order_sn="+$('#order_sn').val()
                +"&terminalId="+$('#terminalId').val()
                +"&is_true="+$('#is_true').val()
                +"&notify_type="+$('#notify_type').val()
                +"&start_time="+$("#start_time").find("input").val()
                +"&end_time="+$("#end_time").find("input").val();
        }
    </script>
@endsection