@extends('admin.layouts.app')

@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">编辑pos机</h3>
                <div class="hr-line-solid"></div>
                <div class="alert alert-warning"> 魔方pos机的商家必须与进件时的终端号(SN)一致，且不允许二次修改，务必确认填写终端号与进件时相同</div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/pos/pos_edit_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$info->id or 0}}" name="id">
                    <input type="hidden" value="{{$info->pos_type}}" name="pos_type">
                    <input type="hidden" value="{{$info->is_direct_buy}}" name="is_direct_buy">

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" >pos机终端号</label>
                        <label for="signin-text" class="col-sm-10 control-label" >{{$info->terminalId}}</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" >pos机型号</label>
                        <label for="signin-text" class="col-sm-10 control-label" >{{config('pos.pos_type')[$info->pos_type]}}</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">备注</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="remark"   placeholder="备注"  value="{{old('remark',$info->remark)}}">
                            @if ($errors->has('remark'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('remark') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">是否允许修改推荐</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="is_allow_edit" value="1" @if($info->is_allow_edit==1) checked @endif/> 允许
                            <input type="radio" class="control-label" name="is_allow_edit" value="0" @if($info->is_allow_edit==0) checked @endif/> 禁止
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">是否公司直购</label>
                        <label for="signin-text" class="col-sm-10 control-label" >
                            @if($info->is_direct_buy==0)
                                市场部购买
                            @elseif($info->is_direct_buy==1)
                                向公司购买
                            @elseif($info->is_direct_buy==2)
                                商家自带
                            @endif
                        </label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>



                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">绑定商家</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="seller_mobile"   placeholder="绑定商家"  value="{{old('seller_mobile',$info->seller_mobile)}}">
                            @if ($errors->has('seller_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('seller_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">绑定机主</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="agent_mobile"   placeholder="绑定机主"  value="{{old('agent_mobile',$info->agent_mobile)}}">
                            @if ($errors->has('agent_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('agent_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">绑定运营中心</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="operate_mobile"   placeholder="绑定运营中心"  value="{{old('operate_mobile',$info->operate_mobile)}}">
                            @if ($errors->has('operate_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('operate_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">一级运营中心</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" readonly name="operate_level1_mobile"   placeholder="一级运营中心"  value="{{old('operate_level1_mobile',$info->operate1_mobile)}}">
                            @if ($errors->has('operate_level1_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('operate_level1_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">二级运营中心</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" readonly name="operate_level2_mobile"   placeholder="二级运营中心"  value="{{old('operate_level2_mobile',$info->operate2_mobile)}}">
                            @if ($errors->has('operate_level2_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('operate_level2_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">三级运营中心</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" readonly name="operate_level3_mobile"   placeholder="三级运营中心"  value="{{old('operate_level3_mobile',$info->operate3_mobile)}}">
                            @if ($errors->has('operate_level3_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('operate_level3_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <input type="hidden" name="operate_level1" value="{{old('operate_level1',$info->operate_level1)}}"/>
                    <input type="hidden" name="operate_level2" value="{{old('operate_level2',$info->operate_level2)}}"/>
                    <input type="hidden" name="operate_level3" value="{{old('operate_level3',$info->operate_level3)}}"/>


                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 修改</button>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')
    <script>
        $(function(){
            $('.relieve').each(function () {
                $(this).click(function () {
                    $tip=$(this).attr('tip');
                    $tip_name=$(this).attr('tip_name');
                    if(confirm('确定要解除{{$info->terminalId}}的'+$tip_name+'身份?')){
                        $.ajax({
                            url:'{{url('/pos/relieve')}}',
                            type:'POST', //GET
                            data:{
                                type:$tip,
                                terminalId:'{{$info->terminalId}}'
                            },
                            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                            success:function(data) {

                                if (data.code == 0) {
                                    alert(data.message)
                                } else {
                                    location.reload();
                                }
                            }
                        })
                    }
                });
            });

            $('input[name=operate_mobile]').blur(function(){
                var value=$('input[name=operate_mobile]').val();
                var is_direct_buy=$('input[name=is_direct_buy]').val();
                if(value!=''&&is_direct_buy!=1){
                    //若是不是在公司直接购买的
                    getOperateLevel(is_direct_buy);
                }
            });

            function getOperateLevel(is_direct_buy){
                var value=$('input[name=operate_mobile]').val();
                for (var i=0;i<=3;i++)
                {
                    $("input[name=operate_level"+i+"_mobile]").val('');
                    $("input[name=operate_level"+i+"]").val('');
                }
                $.ajax({
                    type: "POST",
                    url: "{{url('/pos/get_operate')}}",
                    data: {is_direct_buy:is_direct_buy, operate_mobile:value},
                    dataType: "json",
                    success: function(data){
                        if(data.code==0){
                            alert(data.message);
                            for (var i=0;i<=3;i++)
                            {
                                $("input[name=operate_level"+i+"_mobile]").val('');
                                $("input[name=operate_level"+i+"]").val('');
                            }
                        }else{
                            $.each(data.data,function(name,value) {
                                $("input[name=operate_level"+name+"_mobile]").val(value.mobile);
                                $("input[name=operate_level"+name+"]").val(value.id);
                            });
                            $.each(data.data,function(name,value) {
                                if(is_direct_buy==1){
                                    $('input[name=operate_mobile]').val(value.mobile);
                                    return false;
                                }
                            });
                        }
                    }
                });
            }
        })
    </script>
@endsection