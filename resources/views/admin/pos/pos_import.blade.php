@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">POS机批量导入</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                <div class="col-sm-12">
                    <a class="btn btn-primary" style="float: right;"><i class="fa fa-file-excel-o"></i> 上传表格</a>
                    <input id="fileupload" type="file" name="files" style="opacity: 0;width:118px;height: 34px;position: absolute;z-index: 2;right: 15px;">
                    <a href="{{url('/pos/pos_demo_download')}}" class="btn btn-info" style="float: right;margin-right: 10px;"><i class="fa fa-download"></i> 下载模板文件</a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>POS机编号</th>
                        <th>POS机型号</th>
                        <th>所属机主</th>
                        <th>绑定商家</th>
                        <th>所属运营中心</th>
                        <th>是否公司直购</th>
                        <th>备注</th>
                        <th>执行状态</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody id="tbody">
                        </tbody>
                    </form>
                </table>
                <div class="clearfix"></div>
                <div class="hr-line-dashed"></div>
                <div class="col-sm-12" id="button_area" hidden>
                    <button type="submit" class="btn btn btn-info" id="submit" style="float:right;" data-pre=""><i class="fa fa-paper-plane-o"></i> 提交</button>
                    <button type="button" class="btn btn-success" id="cancel" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 撤销</button>
                </div>
            </div>

            <div class="panel-footer">
                {{--<div class="row">--}}
                    {{--<ul class="pagination" style="float: right">--}}
                        {{--<li>--}}
                            {{--<a href="#">总计条数:{{ $pos_list->total()}}</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        //撤销
        $('#cancel').on('click',function () {
            if(confirm('您确定要撤销本次操作吗?')){
                window.location.reload();
            }
        });
        //提交
        $('#submit').on('click',function () {
            $.ajax({
                url: "{{url('/pos/pos_upload_submit')}}",
                type: "POST",
                data: {
                    'pre_key':document.getElementById('submit').getAttribute('data-pre')
                }
            }).done(function (data) {
                alert(data.message);
                window.location.reload();
            });
        });
        //文件上传
        $('#fileupload').on('change',function () {
            var fd = new FormData();
            fd.append("xlsfile",this.files[0]);
            $.ajax({
                url: "{{url('/pos/pos_upload')}}",
                type: "POST",
                data: fd,
                processData: false,  // 不处理数据
                contentType: false   // 不设置内容类型
            }).done(function (data) {
                if(data.code==1){
                    var tmp=data.data.pre_data;
                    var html='';
                    for(i=0;i<tmp.length;i++){
                        html+='<tr>\n' +
                            '\t<td>'+tmp[i].terminalId+'</td>\n' +
                            '\t<td>'+parse_pos_type(tmp[i].pos_type)+'</td>\n' +
                            '\t<td>'+tmp[i].agent_mobile+'</td>\n' +
                            '\t<td>'+tmp[i].seller_mobile+'</td>\n' +
                            '\t<td>'+tmp[i].operate_mobile+'</td>\n' +
                            '\t<td>'+parse_is_direct_buy(tmp[i].is_direct_buy)+'</td>\n' +
                            '\t<td>'+tmp[i].remark+'</td>\n' +
                            '\t<td>'+parseError(tmp[i].error)+'</td>\n' +
                            '</tr>';
                    }
                    $('#tbody').html(html);
                    document.getElementById('submit').setAttribute('data-pre',data.data.pre_key);
                    document.getElementById('button_area').hidden=false;
                }else{
                    alert(data.message);
                }
            }).fail();
        });

        /**
         * 处理错误
         *
         * @param msg
         * @returns {string}
         */
        function parseError(msg) {
            if(msg==''){
                return '<span style="color:green;">有效</span>';
            }else{
                return '<span style="color:red;">'+msg+'</span>';
            }
        }

        /**
         * 格式化
         * @param msg
         */
        function parse_is_direct_buy(msg) {
            var tmp={0:'市场部购买',1:'向公司购买',2:'商家自带'};
            return tmp[msg];
        }

        /**
         * 格式化
         *
         * @param msg
         */
        function parse_pos_type(msg) {
            var tmp={4:'魔方',5:'银盛'};
            return tmp[msg];
        }

    </script>
@endsection