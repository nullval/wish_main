@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">pos机管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                <div class="col-sm-2">
                    <select class="form-control" id="pos_type">
                        <option value="0">--pos机型号--</option>
                        @foreach(config('pos.pos_type') as $k=>$v)
                            <option value="{{$k}}" @if($_GET['pos_type']==$k) selected @endif>{{$v}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-10">
                    <a href="{{url('/pos/pos_add')}}" class="btn btn-info" style="float: right;"><i class="fa fa-plus"></i> 新增pos机</a>
                    <a href="{{url('/pos/pos_import')}}" class="btn btn-primary" style="float: right;margin-right: 10px;"><i class="fa fa-file-excel-o"></i> 批量导入</a>
                </div>
                <div class="col-sm-10" style="margin-top: 10px">
                    <input type="text" id="terminalId" class="form-control" placeholder="终端号" style="width: 200px;float:left;" value="{{$_GET['terminalId']}}"/>
                    <input type="text" id="dev" class="form-control" placeholder="设备号" style="width: 200px;float:left;" value="{{$_GET['dev']}}"/>
                    <input type="text" id="seller_mobile" class="form-control" placeholder="绑定商家" style="width: 200px;float:left;" value="{{$_GET['seller_mobile']}}"/>
                    <input type="text" id="agent_mobile" class="form-control" placeholder="所属机主" style="width: 200px;float:left;" value="{{$_GET['agent_mobile']}}"/>
                    <input type="text" id="operate_mobile" class="form-control" placeholder="所属运营中心" style="width: 200px;float:left;" value="{{$_GET['operate_mobile']}}"/>
                    <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>pos机编号</th>
                        <th>终端号</th>
                        <th>所属机主</th>
                        <th>绑定商家</th>
                        <th>所属运营中心</th>
                        <th>pos机型号</th>
                        {{--<th>mac</th>--}}
                        <th>设备号</th>
                        <th>创建时间</th>
                        <th>操作管理员</th>
                        <th>是否公司直购</th>
                        <th>备注</th>
                        <th>编辑</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($pos_list as $k=>$v)
                            <tr>
                                <td>{{$v['id']}}</td>
                                <td>{{$v['terminalId'] or '暂无'}}</td>
                                <td>{{$v['agent_mobile'] or '暂无'}}</td>
                                <td>{{$v['seller_mobile'] or '暂无'}}</td>
                                <td>{{$v['agent_operate_mobile'] or '暂无'}}</td>
                                <td>{{config('pos.pos_type')[$v['pos_type']]}}</td>
                                {{--<td>{{$v['mac'] or '暂无'}}</td>--}}
                                <td>{{$v['dev'] or '暂无'}}</td>
                                <td>{{$v['created_at'] or '暂无'}}</td>
                                <td>{{$v['username'] or '暂无'}}</td>
                                <td>
                                    @if($v->is_direct_buy==0)
                                        市场部购买
                                    @elseif($v->is_direct_buy==1)
                                        向公司购买
                                    @elseif($v->is_direct_buy==2)
                                        商家自带
                                    @endif
                                </td>
                                <td>{{$v['remark']}}</td>
                                <td>
                                    <a href="{{url('/pos/pos_edit')}}?id={{$v['id']}}"><span class="label label-warning"><i class="fa fa-edit"></i> 编辑</span>
                                    </a>
                                </td>
                          </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $pos_list->appends([
                    'terminalId'=>$_GET['terminalId'] ?? '',
                    'seller_mobile'=>$_GET['seller_mobile'] ?? '',
                    'agent_mobile'=>$_GET['agent_mobile'] ?? '',
                    'dev'=>$_GET['dev'] ?? '',
                    'operate_mobile'=>$_GET['operate_mobile'] ?? '',
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $pos_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            $('#pos_type').change(function(){
                location_a();
            });
            $('#search').click(function(){
                location_a();
            });
            function location_a() {
                location.href="{{url('/pos/pos_list')}}?terminalId="+$('#terminalId').val()
                    +'&seller_mobile='+$('#seller_mobile').val()
                    +'&agent_mobile='+$('#agent_mobile').val()
                    +'&dev='+$('#dev').val()
                    +'&operate_mobile='+$('#operate_mobile').val()
                    +'&pos_type='+$('#pos_type').val();
            }
        });
    </script>
@endsection