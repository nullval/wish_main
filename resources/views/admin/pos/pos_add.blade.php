@extends('admin.layouts.app')

@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">新增pos机</h3>
                <div class="hr-line-solid"></div>
                <div class="alert alert-warning"> 公司卖出的pos按照运营中心进入的顺序，直接配置一级运营中心，二级运营中心，三级运营中心,否则按填写的运营中心的推荐关系来配置,添加pos机请慎重操作会及时分账</div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/pos/pos_add_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    @if(isset($_GET['id']))
                        <input type="hidden" value="{{$info->id or 0}}" name="id">
                    @endif

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">pos机终端号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="terminalId"   placeholder="pos机终端号" value="{{old('terminalId',$info->terminalId)}}">
                            @if ($errors->has('terminalId'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('terminalId') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">pos机型号</label>
                        <div class="col-sm-10">
                            {{--<input type="text" class="form-control" name="model"   placeholder="pos机型号" value="{{old('model',$info->model)}}">--}}
                            {{--@if ($errors->has('model'))--}}
                            {{--<span class="help-block m-b-none text-danger">{{ $errors->first('model') }}</span>--}}
                            {{--@endif--}}
                            <select  class="form-control" name="pos_type">
                                @foreach(config('pos.pos_type') as $k=>$v)
                                    <option value="{{$k}}">{{$v}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">备注</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="remark"   placeholder="备注"  value="{{old('remark',$info->remark)}}">
                            @if ($errors->has('remark'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('remark') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">是否允许修改推荐</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="is_allow_edit" value="1" @if($info->is_allow_edit==1) checked @endif/> 允许
                            <input type="radio" class="control-label" name="is_allow_edit" value="0" @if($info->is_allow_edit==0) checked @endif/> 禁止
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">是否直接向公司购买</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="is_direct_buy" value="0" @if($info->is_direct_buy==0) checked @endif/> 市场部购买
                            <input type="radio" class="control-label" name="is_direct_buy" value="1" @if($info->is_direct_buy==1) checked @endif/> 向公司购买
                            <input type="radio" class="control-label" name="is_direct_buy" value="2" @if($info->is_direct_buy==2) checked @endif/> 商家自带
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div id="mofang" >
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">绑定商家</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="seller_mobile"   placeholder="绑定商家"  value="{{old('seller_mobile')}}">
                                @if ($errors->has('seller_mobile'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('seller_mobile') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">绑定机主</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="agent_mobile"   placeholder="绑定机主"  value="{{old('agent_mobile')}}">
                                @if ($errors->has('agent_mobile'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('agent_mobile') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">绑定机主推荐人</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="invite_mobile"   placeholder="绑定机主推荐人"  value="{{old('invite_mobile')}}">
                                @if ($errors->has('invite_mobile'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('invite_mobile') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">绑定运营中心</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="operate_mobile"   placeholder="绑定运营中心"  value="{{old('operate_mobile')}}">
                                @if ($errors->has('operate_mobile'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('operate_mobile') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">一级运营中心</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" readonly name="operate_level1_mobile"   placeholder="一级运营中心"  value="{{old('operate_level1_mobile')}}">
                            @if ($errors->has('operate_level1_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('operate_level1_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">二级运营中心</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" readonly name="operate_level2_mobile"   placeholder="二级运营中心"  value="{{old('operate_level2_mobile')}}">
                            @if ($errors->has('operate_level2_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('operate_level2_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">三级运营中心</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" readonly name="operate_level3_mobile"   placeholder="三级运营中心"  value="{{old('operate_level3_mobile')}}">
                            @if ($errors->has('operate_level3_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('operate_level3_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">	短信安全验证 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text"  class="form-control" placeholder="输入短信验证码" style="width: 200px;float:left;" name="code">--}}
                            {{--<a  id="get_code" class="btn btn btn-info" style="float:left;"><i class="fa fa-refresh"></i> 发送短信</a>--}}
                            {{--@if ($errors->has('code'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('code') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    <input type="hidden" name="operate_level1" value="{{old('operate_level1')}}"/>
                    <input type="hidden" name="operate_level2" value="{{old('operate_level2')}}"/>
                    <input type="hidden" name="operate_level3" value="{{old('operate_level3')}}"/>

                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-plus"></i> 发布</button>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')
    <script>
        $(function(){
            // $('select[name=pos_type]').change(function(){
            //     if($(this).val()==4){
            //         $('#mofang').show();
            //     }else{
            //         $('#mofang').hide();
            //     }
            // });
            $('input[name=operate_mobile]').blur(function(){
                var value=$('input[name=operate_mobile]').val();
                var is_direct_buy=$('input[name=is_direct_buy]:checked').val();
                if(value!=''){
                    //若是不是在公司直接购买的
                    getOperateLevel(is_direct_buy);
                }
            });
            $('input[name=is_direct_buy]').change(function(){
                var is_direct_buy=$('input[name=is_direct_buy]:checked').val();
                if(is_direct_buy==1){
                    $('input[name=operate_mobile]').val('');
                }
                getOperateLevel(is_direct_buy);
            });
            $('#get_code').click(function(){
                $.ajax({
                    type: "POST",
                    url: '{{url('/config/send_message')}}',
                    data: {account_name:$('#bank_id option:selected').attr('tip')},
                    dataType: "json",
                    success: function(data){
                        if(data.cod=1){
                            alert(data.message);
                        }else{
                            alert(data.message);
                        }
                    }
                });
            });
        })
        function getOperateLevel(is_direct_buy){
            var value=$('input[name=operate_mobile]').val();
            for (var i=0;i<=3;i++)
            {
                $("input[name=operate_level"+i+"_mobile]").val('');
                $("input[name=operate_level"+i+"]").val('');
            }
            $.ajax({
                type: "POST",
                url: "{{url('/pos/get_operate')}}",
                data: {is_direct_buy:is_direct_buy, operate_mobile:value},
                dataType: "json",
                success: function(data){
                    if(data.code==0){
                        alert(data.message);
                        for (var i=0;i<=3;i++)
                        {
                            $("input[name=operate_level"+i+"_mobile]").val('');
                            $("input[name=operate_level"+i+"]").val('');
                        }
                    }else{
                        $.each(data.data,function(name,value) {
                            $("input[name=operate_level"+name+"_mobile]").val(value.mobile);
                            $("input[name=operate_level"+name+"]").val(value.id);
                        });
                        $.each(data.data,function(name,value) {
                            if(is_direct_buy==1){
                                $('input[name=operate_mobile]').val(value.mobile);
                                return false;
                            }
                        });
                    }
                }
            });
        }
    </script>
@endsection