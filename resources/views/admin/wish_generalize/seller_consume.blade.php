@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">财务管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding" style="margin-bottom: 20px;">
                {{--<div class="col-sm-12">--}}
                {{--<div class="alert alert-warning"> 由于年假期间，13日-23日微信、支付宝货款将在23日之后统一结算，具体请查看未特商城公众号公告，银行卡货款年假期间即时结算（秒到））</div>--}}
                {{--</div>--}}
                <div class="col-sm-12">
                    <span class="form-control">用户名 : {{$sellerInfo['mobile']}} </span>
                </div>
                <div class="col-sm-12">
                    <span class="form-control">钱包余额 : {{$money}} </span>
                </div>
                {{--<div class="col-sm-12">--}}
                    {{--<span class="form-control">未特推广券可转换数量 : {{$voucher}} (未特推广券进行激活) </span>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">财务流水</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>

                <div class="col-sm-2">
                    <select class="form-control" id="type">
                        <option value="0">全部</option>
                        <option value="1" @if(isset($_GET['type'])&&$_GET['type']==1) selected @endif>支出</option>
                        <option value="2" @if(isset($_GET['type'])&&$_GET['type']==2) selected @endif>收入</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="form-control" id="purse_type">
                        <option value="0" @if(!isset($_GET['purse_type'])||$_GET['purse_type']==0) selected @endif>全部</option>
                        <option value="1" @if(isset($_GET['purse_type'])&&$_GET['purse_type']==1) selected @endif>现金</option>
                        <option value="3" @if(isset($_GET['purse_type'])&&$_GET['purse_type']==3) selected @endif>代金券</option>
                        <option value="4" @if(isset($_GET['purse_type'])&&$_GET['purse_type']==4) selected @endif>推荐奖励</option>
                    </select>
                </div>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>流水编号</th>
                        <th>类型</th>
                        <th>变动额度</th>
                        <th>转账时间</th>
                        <th>描述</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($transferList as $k=>$v)
                        <tr>
                            <td>{{$v['id']}}</td>
                            <td>
                                @if($v['transfer_type']==1) 支出
                                @elseif($v['transfer_type']==2) 收入
                                @else 其它
                                @endif
                            </td>
                            <td>
                                {{$v['amount']}}
                            </td>
                            <td>{{$v['created_at']}}</td>
                            <td>{{$v['reason_name']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{--{{ $transfer->appends([--}}
                    {{--'status'=>isset($_GET['status'])?$_GET['status']:0,--}}
                    {{--'type'=>isset($_GET['type'])?$_GET['type']:0,--}}
                    {{--'id'=>isset($_GET['id'])?$_GET['id']:0--}}
                    {{--])->links() }}--}}

                    <ul class="pagination">
                        @if($page == 2)
                            <li class="disabled"><span>上一页</span></li>
                        @else
                            <li><a href="{{url('/wish_generalize/seller_consume')}}?purse_type={{$purseType}}&amp;type={{$type}}&amp;page={{$page-2}}&amp;uid={{$sellerInfo['id']}}" rel="next">上一页</a></li>
                        @endif
                        <li><a href="{{url('/wish_generalize/seller_consume')}}?purse_type={{$purseType}}&amp;type={{$type}}&amp;page={{$page}}&amp;uid={{$sellerInfo['id']}}" rel="next">下一页</a></li>
                    </ul>

                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            $('#purse_type,#type').change(function(){
                location.href="{{url('/wish_generalize/seller_consume')}}?type="+$('#type').val()+"&purse_type="+$('#purse_type').val()+"&uid={{$sellerInfo['id']}}";
            });
        });
    </script>
@endsection