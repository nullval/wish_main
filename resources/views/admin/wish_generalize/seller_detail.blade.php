@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">商家信息</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding" style="margin-bottom: 20px;">
                <div class="col-sm-12">
                    <span class="form-control"><span class="col-sm-2">商家名称 : </span>{{$seller_info['merchantName']}} </span>
                </div>
                <div class="col-sm-12">
                    <span class="form-control"><span class="col-sm-2">城市 : </span> {{$address}} </span>
                </div>
                <div class="col-sm-12">
                    <span class="form-control"><span class="col-sm-2">详细地址 : </span>{{$seller_info['merchantAddress']}} </span>
                </div>
                <div class="col-sm-12">
                    <span class="form-control"><span class="col-sm-2">座机号码 : </span>{{$seller_info['corpmanPhone']}} </span>
                </div>
                <div class="col-sm-12">
                    <span class="form-control"><span class="col-sm-2">人均消费 : </span>{{$seller_info['average']}} </span>
                </div>
                <div class="col-sm-12">
                    <span class="form-control"><span class="col-sm-2">客服电话 : </span>{{$seller_info['servicePhone']}} </span>
                </div>
                <div class="col-sm-12">
                    <span class="form-control"><span class="col-sm-2">营业时间 : </span>{{$seller_info['business_time']}} </span>
                </div>
                <div class="col-sm-12">
                    <span class="form-control"><span class="col-sm-2">经营类目 : </span>{{$category_text}} </span>
                </div>
                <div class="col-sm-12">
                    <span class="form-control" style="height: 115px"><span class="col-sm-2">logo图片 : </span><img src="{{$seller_info['logoimage']}}" width="100" height="100" /> </span>
                </div>

            </div>
        </div>
    </div>


@endsection
@section('js')
    <script>
        $(function(){

        });
    </script>
@endsection