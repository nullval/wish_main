@extends('admin.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" />
<link href="{{asset('sanji/css/main.css')}}" rel="stylesheet">
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<style type="text/css">
    /*body, html{width: 100%;height: 100%;margin:0;font-family:"微软雅黑";font-size:14px;}*/
    #l-map{height:250px;width:100%;}
    /*#r-result{width:100%;}*/
</style>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=RNifyeVIz4GNTjvl9XwDUth7ufYPk2pX"></script>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">直推人入驻信息</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 商家手机号和法人联系电话必须一致</div>--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/wish_generalize/generalize_add_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">直推人手机号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mobile"  placeholder="直推人手机号" value="@if(old('mobile') != ""){{old('mobile')}}@elseif(session('mobile') != '') {{session('mobile')}}@endif">
                            @if ($errors->has('mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">推荐人手机号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="invite_mobile" @if($supeerior_mobile) readonly @endif  placeholder="推荐人手机号" value="{{$supeerior_mobile}}">
                            @if ($errors->has('invite_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('invite_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">直推人名称 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="merchantName"   placeholder="直推人名称" value="@if(old('merchantName') != ""){{old('merchantName')}}@elseif(session('merchantName') != '') {{session('merchantName')}}@endif">
                            @if ($errors->has('merchantName'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('merchantName') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">直推人密码 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="password"   placeholder="直推人密码(不填则默认为手机号码)" value="">
                            @if ($errors->has('merchantName'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('merchantName') }}</span>
                            @endif
                        </div>
                    </div>

                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}
                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">POS机终端号 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="terminalId"   placeholder="POS机终端号" value="@if(old('terminalId') != ""){{old('terminalId')}}@elseif(session('terminalId') != '') {{session('terminalId')}}@endif">--}}
                            {{--@if ($errors->has('terminalId'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('terminalId') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}


                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">省市区选择</label>
                        <div class="col-sm-10 form-inline">

                            <div id="distpicker5">
                                <div class="form-group">
                                    <label class="sr-only" for="province10">Province</label>
                                    <select class="form-control" id="province10" name="province10" onchange="get_province_code()"></select>
                                </div>
                                <input type="hidden" name="province_code" id="province_code" value="@if(old('province_code') != ""){{old('province_code')}}@elseif(session('province_code') != '') {{session('province_code')}}@endif">
                                <div class="form-group">
                                    <label class="sr-only" for="city10">City</label>
                                    <select class="form-control" id="city10" name="city10" onchange="get_city_code()"></select>
                                </div>
                                <input type="hidden" name="city_code" id="city_code" value="@if(old('city_code') != ""){{old('city_code')}}@elseif(session('city_code') != '') {{session('city_code')}}@endif">

                                <div class="form-group">
                                    <label class="sr-only" for="district10">District</label>
                                    <select class="form-control" id="district10" name="district10" onchange="get_district_code()"></select>
                                </div>
                                <input type="hidden" name="district_code" id="district_code" value="@if(old('district_code') != ""){{old('district_code')}}@elseif(session('district_code') != '') {{session('district_code')}}@endif">

                            </div>

                        </div>
                    </div>

                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">详细地址 </label>--}}
                        {{--<div class="col-sm-10">--}}

                            {{--<input type="text" class="form-control" id="merchantAddress" name="merchantAddress"   placeholder="详细地址" value="@if(old('merchantAddress') != ""){{old('merchantAddress')}}@elseif(session('merchantAddress') != '') {{session('merchantAddress')}}@endif">--}}

                            {{--@if ($errors->has('merchantAddress'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('merchantAddress') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>



                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">直推人姓名 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="corpmanName"   placeholder="法人姓名" value="@if(old('corpmanName') != ""){{old('corpmanName')}}@elseif(session('corpmanName') != '') {{session('corpmanName')}}@endif">--}}
                            {{--@if ($errors->has('corpmanName'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('corpmanName') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">直推人身份证号 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="corpmanId"   placeholder="法人身份证号" value="@if(old('corpmanId') != ""){{old('corpmanId')}}@elseif(session('corpmanId') != '') {{session('corpmanId')}}@endif">--}}
                            {{--@if ($errors->has('corpmanId'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('corpmanId') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">开户银行 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<select name="bankName" id="bankName" class="form-control" onchange="getbankNo()">--}}
                                {{--<option value="0" >请选择银行</option>--}}
                                {{--@foreach($bank_arr as $k=>$v)--}}
                                    {{--<option value="{{$v->bankName}}" tip="{{$v->bankCode}}"--}}
                                            {{--@if(old('bankName') != "")--}}
                                            {{--@if(old('bankName')==$v->bankName)--}}
                                            {{--selected--}}
                                            {{--@endif--}}
                                            {{--@elseif(session('bankName') != '')--}}
                                            {{--@if(session('bankName')==$v->bankName)--}}
                                            {{--selected--}}
                                            {{--@endif--}}
                                            {{--@endif--}}
                                    {{-->{{$v->bankName}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                            {{--<input type="hidden"  name="bankCode"value="@if(old('bankCode') != ""){{old('bankCode')}}@elseif(session('bankCode') != '') {{session('bankCode')}}@endif">--}}
                            {{--@if ($errors->has('category'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('category') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}


                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">开户行账号 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="bankaccountNo"   placeholder="开户行账号" value="@if(old('bankaccountNo') != ""){{old('bankaccountNo')}}@elseif(session('bankaccountNo') != '') {{session('bankaccountNo')}}@endif">--}}
                            {{--@if ($errors->has('bankaccountNo'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountNo') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">开户户名 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<label class="control-label">{{$basic_info->bankaccountName}}</label>--}}

                            {{--<input type="text" class="form-control" name="bankaccountName"   placeholder="开户户名" value="{{$basic_info->bankaccountName or old('bankaccountName')}}">--}}
                            {{--@if ($errors->has('bankaccountName'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountName') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}


                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                    <a href="{{url('/wish_generalize/generalize_list')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>

                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')
    <script src="{{asset('sanji/js/distpicker.data.js')}}"></script>
    <script src="{{asset('sanji/js/distpicker.js')}}"></script>
    <script src="{{asset('sanji/js/main.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('#bankName').change(function(){
                $('input[name=bankCode]').val($('#bankName option:selected').attr('tip'));

            });
            $('#bankName1').change(function(){
                $('input[name=bankCode1]').val($('#bankName1 option:selected').attr('tip'));

            });
            $("#up_img_WU_FILE_0").change(function(){
                var v = $(this).val();
                var reader = new FileReader();
                reader.readAsDataURL(this.files[0]);
                reader.onload = function(e){
                    // console.log(e.target.result);
                    $('#uploadfile').val(e.target.result);
                    $('#imgShow_WU_FILE_0').attr('src',e.target.result);
                };
            });


        });
        function getbankNo() {
            var bank_name=$("#bankName").val();
            $.ajax({
                url:'{{url('/wish_generalize/get_bankNo')}}',
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    bank_name:bank_name
                },
                timeout:5000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data){

                    if (data.code==0){
//                    alert(data.message)
                        $('#ibankno').val('0');
                        $('#account_bank').val("");
                    }else {
                        $('#ibankno').val(data.data.lbnkNo);
                        $('#account_bank').val(data.data.bank_name);
                    }
                },
                error:function(xhr,textStatus){
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },

            })
        }
        function get_city(){
            cityName = $("#cityName1").val();
            val=$(".city_option[tip^="+cityName+"]").val();
            $('#aa').val(val);
        }
        function get_province_code() {
            $('#province_code').val($("#province10").find("option:selected").attr("data-code"));
            var j = 0;
            var k = 0;
            $.each(ChineseDistricts[$("#province10").find("option:selected").attr("data-code")], function(i, val) {
                j++;
                if (j == 1){
                    $('#city_code').val(i);

                    $.each(ChineseDistricts[i], function(i1, val1) {
                        k++;
//                    alert(val1);
                        if (k == 1){
                            $('#district_code').val(i1);
                        }
                    });
                }
            });
//        $('#city_code').val("");
//        $('#district_code').val("");
        }
        function get_city_code() {
            $('#city_code').val($("#city10").find("option:selected").attr("data-code"));
            if (typeof(ChineseDistricts[$("#city10").find("option:selected").attr("data-code")]) == 'undefined') {
                $('#district_code').val("");
            }

            var k = 0;
            $.each(ChineseDistricts[$("#city10").find("option:selected").attr("data-code")], function(i, val) {
                k++;
                if (k == 1){
                    $('#district_code').val(i);
                }
            });
//        $('#district_code').val("");
            get_coordinate();
        }
        function get_district_code() {
            $('#district_code').val($("#district10").find("option:selected").attr("data-code"));
            get_coordinate();
        }


        if ("{{empty(old('province10'))?false:true}}"){
            var province_name = "{{old('province10')}}";
            var city_name = "{{empty(old('city10'))?null:old('city10')}}";
            var district_name = "{{empty(old('district10'))?null:old('district10')}}";
//        alert(province_name)
//        alert(city_name)
//        alert(district_name)
            $("#distpicker5").distpicker({
                province: province_name,
                city: city_name,
                district: district_name
            });

        }else{
            if ("{{session('province10')}}" != ""){
                var province_name = "{{session('province10')}}";
                var city_name = "{{session(old('city10'))==""?null:session('city10')}}";
                var district_name = "{{session(old('district10'))==""?null:session('district10')}}";
//        alert(province_name)
//        alert(city_name)
//        alert(district_name)
                $("#distpicker5").distpicker({
                    province: province_name,
                    city: city_name,
                    district: district_name
                });
            }

        }


    </script>
    <script type="text/javascript">
        // 百度地图API功能
        function G(id) {
            return document.getElementById(id);
        }

        var map = new BMap.Map("l-map");
        @if(session('lng') == '' && session('lat') == '')
        map.centerAndZoom("深圳",12);                   // 初始化地图,设置城市和地图级别。
        @elseif(session('lng') != '' && session('lat') != '')
        map.centerAndZoom(new BMap.Point("{{session('lng')}}", "{{session('lat')}}"),12);                   // 初始化地图,设置城市和地图级别。
                @endif

        var ac = new BMap.Autocomplete(    //建立一个自动完成的对象
            {"input" : "suggestId"
                ,"location" : map
            });

        ac.addEventListener("onhighlight", function(e) {  //鼠标放在下拉列表上的事件
            var str = "";
            var _value = e.fromitem.value;
            var value = "";
            if (e.fromitem.index > -1) {
                value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
            }
            str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;

            value = "";
            if (e.toitem.index > -1) {
                _value = e.toitem.value;
                value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
            }
            str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
            G("searchResultPanel").innerHTML = str;
        });

        var myValue;
        ac.addEventListener("onconfirm", function(e) {    //鼠标点击下拉列表后的事件
            var _value = e.item.value;
            myValue = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
            G("searchResultPanel").innerHTML ="onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue;

            setPlace();
        });

        function setPlace(){
            map.clearOverlays();    //清除地图上所有覆盖物
            function myFun(){
                var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
                map.centerAndZoom(pp, 18);
                $("#lat").val(pp.lat);
                $("#lng").val(pp.lng);
                map.addOverlay(new BMap.Marker(pp));    //添加标注
            }
            var local = new BMap.LocalSearch(map, { //智能搜索
                onSearchComplete: myFun
            });
            local.search(myValue);
        }
        map.enableScrollWheelZoom();   //启用滚轮放大缩小，默认禁用
        map.enableContinuousZoom();    //启用地图惯性拖拽，默认禁用、
        //    单击获取点击的经纬度
        map.addEventListener("click",function(e){
//        alert(e.point.lng + "," + e.point.lat);
            map.clearOverlays();
            var marker = new BMap.Marker(new BMap.Point(e.point.lng, e.point.lat)); // 创建点
            $("#lat").val(e.point.lat);
            $("#lng").val(e.point.lng);
            map.addOverlay(marker);

        });

        $("#merchantAddress").blur(function(){
            get_coordinate()
        });
        $("#province10").blur(function(){
            get_coordinate()
        });

        function get_coordinate() {
            var address = $("#province10").find("option:selected").val()+$("#city10").find("option:selected").val()+$("#district10").find("option:selected").val()+$("#merchantAddress").val();
            var city = $("#province10").find("option:selected").val()+$("#city10").find("option:selected").val();
            // 创建地址解析器实例
            var myGeo = new BMap.Geocoder();
            // 将地址解析结果显示在地图上，并调整地图视野
            myGeo.getPoint(address, function(point){
                    map.clearOverlays();
                    if (point) {
                        map.centerAndZoom(point, 16);
                        $("#lat").val(point.lat);
                        $("#lng").val(point.lng);
                        map.addOverlay(new BMap.Marker(point));
                    }
                },
                city);
        }
        if ("{{empty(old('lat'))?false:true}}" && "{{empty(old('lng'))?false:true}}"){
            theLocation("{{old('lng')}}","{{old('lat')}}")
        }else {
            if ("{{session('lat')}}" != "" && "{{session('lng')}}" != ""){
                theLocation("{{session('lng')}}","{{session('lat')}}")
            }
        }
        // 用经纬度设置地图中心点
        function theLocation(lng,lat){
            map.clearOverlays();
            var new_point = new BMap.Point(lng,lat);
            var marker = new BMap.Marker(new_point);  // 创建标注
            map.addOverlay(marker);              // 将标注添加到地图中
            map.panTo(new_point);

        }


    </script>
    <script type="text/javascript">
        // 百度地图API功能
        function G(id) {
            return document.getElementById(id);
        }

        var map = new BMap.Map("l-map");
        map.centerAndZoom("深圳",12);                   // 初始化地图,设置城市和地图级别。

        var ac = new BMap.Autocomplete(    //建立一个自动完成的对象
            {"input" : "suggestId"
                ,"location" : map
            });

        ac.addEventListener("onhighlight", function(e) {  //鼠标放在下拉列表上的事件
            var str = "";
            var _value = e.fromitem.value;
            var value = "";
            if (e.fromitem.index > -1) {
                value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
            }
            str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;

            value = "";
            if (e.toitem.index > -1) {
                _value = e.toitem.value;
                value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
            }
            str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
            G("searchResultPanel").innerHTML = str;
        });

    </script>
    <script type="text/javascript">

        @if(Session::has('status'))
        setTimeout( " delData() " , 3000 );
        @endif

        function delData() {
            $.ajax({
                url: '{{url('/wish_generalize/delData')}}',
                type: 'POST', //GET
                async: true,    //或false,是否异步
                data: {},
                timeout: 5000,    //超时时间
                dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success: function (data) {
                    {{--alert({{session('mobile')}})--}}
                            {{--if ("{{old('mobile')}}" == ""){--}}
                            {{--$("[name='mobile']").val("{{session('mobile')}}");--}}
                            {{--}--}}
                        window.onload = function(){
                        setTimeout("location.reload()",1000);
                    }

                },
                error: function (xhr, textStatus) {
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },
            })
        }

    </script>
@endsection