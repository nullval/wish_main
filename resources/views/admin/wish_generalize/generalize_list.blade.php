@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">店铺推荐人管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if ($errors->has('error'))
                        <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                    @endif
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                <div class="col-sm-8">
                    <input type="text" id="mobile" class="form-control" placeholder="商家手机号"  style="width: 200px;float:left;" value="{{$_GET['mobile']}}"/>
                    <input type="text" id="name" class="form-control" placeholder="商家名称"  style="width: 200px;float:left;" value="{{$_GET['name']}}"/>
                    <input type="text" id="agent_mobile" class="form-control" placeholder="代理人手机号" style="width: 200px;float:left;" value="{{$_GET['agent_mobile']}}"/>
                    <input type="text" class="form-control"  style='width: 1px;float:left;padding:0px'/>
                    <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
                </div>
                <div class="col-sm-2" style="float: right;">
                    <a href="{{url('/wish_generalize/generalize_add')}}" class="btn btn-info" style="float: right;"><i class="fa fa-plus"></i> 入驻店铺推荐人</a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>直推人手机号</th>
                        <th>直推人名称</th>
                        <th>代理人</th>
                        <th>创建时间</th>
                        <th>是否启用</th>
                        <th>直推人信息</th>
                        {{--<th>提现银行信息</th>--}}
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($generalize_list as $k=>$v)
                            <tr>
                                <td>{{$v['id']}}</td>
                                <td>{{$v['mobile'] or '暂无'}}</td>
                                <td>{{$v['merchantName'] or '暂无'}}</td>
                                <td>{{$v['agent_mobile'] or '暂无'}}</td>
                                {{--<td>{{$v['credit']}}</td>--}}
                                <td>{{$v['created_at'] or '暂无'}}</td>
                                <td>
                                    @if($v['status']==0) <a href="#" class="status" tip="1" tip1="{{$v['id']}}"><span class="label label-success">启用</span></a>
                                    @else <a href="#" class="status" tip="0" tip1="{{$v['id']}}"><span class="label label-danger">禁用</span></a>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{url('/wish_generalize/generalize_edit')}}?uid={{$v['id']}}"><span class="label label label-warning"><i class="fa fa-edit"></i> 编辑</span></a>
                                </td>
                                {{--<td>--}}
                                    {{--<a href="{{url('/wish_generalize/generalize_bank_edit')}}?uid={{$v['id']}}"><span class="label label label-warning"><i class="fa fa-edit"></i> 编辑</span></a>--}}
                                {{--</td>--}}
                          </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $generalize_list->appends([
                    'mobile'=>isset($_GET['mobile'])?$_GET['mobile']:'',
                    'name'=>isset($_GET['name'])?$_GET['name']:'',
                    'agent_mobile'=>isset($_GET['agent_mobile'])?$_GET['agent_mobile']:'',
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $generalize_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            var _token=$('input[name=_token]').attr('tip');
            //启用/禁用用户
            $('.status').click(function(){
                var $this=$(this);
                var status=$(this).attr('tip');
                var id=$(this).attr('tip1');
                $.ajax({
                    type: "POST",
                    url: '/wish_generalize/change_status',
                    data: {_token:_token, status:status,id:id},
                    dataType: "json",
                    success: function(data){
                        if(data.code==1){
                            var span=$this.children('span');
                            if(status==0){
                                $this.attr('tip',1);
                                span.html('启用');
                                span.removeClass('label-danger');
                                span.addClass('label-success');
                            }else{
                                $this.attr('tip',0);
                                span.html('禁用');
                                span.removeClass('label-success');
                                span.addClass('label-danger');
                            }
                        }
                    }
                });
            });

            $('#apply_status').change(function(){
                location_a();
            });
            $('#search').click(function () {
                location_a();
            });

            $('.check_font').each(function(){
                $(this).click(function(){
                    $('#generalize_id').val($(this).attr('tip'));
                });
            });
        });
        function location_a() {
            location.href="{{url('/wish_generalize/generalize_list')}}?mobile="+$('#mobile').val()
                +"&apply_status=" +$('#apply_status').val()
                +"&name=" +$('#name').val()
                +"&agent_mobile=" +$('#agent_mobile').val();
        }
    </script>
@endsection