@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">直推人管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if ($errors->has('error'))
                        <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                    @endif
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                <div class="col-sm-8">
                    <input type="text" id="mobile" class="form-control" placeholder="直推人手机号"  style="width: 200px;float:left;" value="{{$_GET['mobile']}}"/>
                    <input type="text" id="agent_mobile" class="form-control" placeholder="填写代理手机号" style="width: 200px;float:left;" value="{{$_GET['agent_mobile']}}"/>
                    <input type="text" class="form-control"  style='width: 1px;float:left;padding:0px'/>
                    <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>直推人手机号</th>
                        <th>直推人名称</th>
                        <th>代理人手机号</th>
                        <th>创建时间</th>
                        {{--<th>是否启用</th>--}}
                        {{--<th>直推人信息</th>--}}
                        {{--<th>提现银行信息</th>--}}
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($generalize_list as $k=>$v)
                            <tr>
                                <td>{{$v['id']}}</td>
                                <td>{{$v['mobile'] or '暂无'}}</td>
                                <td>{{$v['generalize_info']['merchantName'] or '暂无'}}</td>
                                <td>{{$v['wish_agent']['mobile'] or '暂无'}}</td>
                                {{--<td>{{$v['credit']}}</td>--}}
                                <td>{{$v['created_at'] or '暂无'}}</td>
                                {{--<td>--}}
                                    {{--@if($v['status']==0) <a href="#" class="status" tip="1" tip1="{{$v['id']}}"><span class="label label-success">启用</span></a>--}}
                                    {{--@else <a href="#" class="status" tip="0" tip1="{{$v['id']}}"><span class="label label-danger">禁用</span></a>--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                    {{--<a href="{{url('/wish_generalize/generalize_edit')}}?uid={{$v['id']}}"><span class="label label label-warning"><i class="fa fa-edit"></i> 编辑</span></a>--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                    {{--<a href="{{url('/wish_generalize/generalize_bank_edit')}}?uid={{$v['id']}}"><span class="label label label-warning"><i class="fa fa-edit"></i> 编辑</span></a>--}}
                                {{--</td>--}}
                          </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $generalize_list->appends([
                    'mobile'=>isset($_GET['mobile'])?$_GET['mobile']:'',
                    'name'=>isset($_GET['name'])?$_GET['name']:'',
                    'agent_mobile'=>isset($_GET['agent_mobile'])?$_GET['agent_mobile']:'',
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $generalize_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){

            $('#apply_status').change(function(){
                location_a();
            });
            $('#search').click(function () {
                location_a();
            });

            $('.check_font').each(function(){
                $(this).click(function(){
                    $('#generalize_id').val($(this).attr('tip'));
                });
            });
        });
        function location_a() {
            location.href="{{url('/wish_agent/generalize_list')}}?mobile="+$('#mobile').val()
                +"&apply_status=" +$('#apply_status').val()
                +"&agent_mobile=" +$('#agent_mobile').val();
        }
    </script>
@endsection