@extends('admin.layouts.app')
@section('content')
    <div class="panel panel-headline">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">绑定商家二维码</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post" id="form"  action="{{url('/order/bind_seller_submit')}}" class="form-horizontal">
                <input type="hidden" name="code_id" value="{{$qr_info['id']}}" >
                <div class="panel-body">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">码编号 </label>
                        <div class="col-sm-10">
                            <input type="text" readonly class="form-control" value="{{$qr_info['code_number']}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="seller_mobile"   placeholder="商家手机号" value="{{old('seller_mobile')}}">
                            @if ($errors->has('seller_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('seller_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <button type="submit" class="btn btn btn-info submit" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                    <a class="btn btn-success" style="float:right;margin-right: 10px;" onclick="window.history.go(-1)"><i class="fa fa-angle-double-left"></i> 返回</a>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
@endsection