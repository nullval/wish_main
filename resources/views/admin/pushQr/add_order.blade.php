@extends('admin.layouts.'.$layouts)
@section('content')
    <div class="panel panel-headline">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">地推商家二维码下发</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 地推二维码编号指要打印的二维码编号集合，平台将根据相应的二维码编号发送给您，若不填写则会按填写的申请数量生成新的二维码发送给您</div>--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post" id="form"  action="{{url('/order/add_pushQr_order_submit')}}" class="form-horizontal">
                <div class="panel-body">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">收货人 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="contact_user"   placeholder="收货人" value="{{old('contact_user')}}">
                            @if ($errors->has('contact_user'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('contact_user') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">联系手机号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="contact_mobile"   placeholder="联系手机号" value="{{old('contact_mobile')}}" required>
                            @if ($errors->has('contact_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('contact_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed" hidden></div>

                    <div class="form-group" hidden>
                        <label for="signin-text" class="col-sm-2 control-label">收货地址 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="contact_address"   placeholder="收货地址" value="{{old('contact_address')}}">
                            @if ($errors->has('contact_address'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('contact_address') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">下发数量 </label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="count" placeholder="下发数量" value="{{old('count')}}" min="1">
                            @if ($errors->has('count'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('count') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">申请的地推二维码编号 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="id_arr"   placeholder="地推二维码编号(可不填写)" value="{{old('id_arr')}}">--}}
                            {{--<span class="help-block m-b-none text-success">编号中间以|隔开,例子:10|11|12|13</span>--}}
                            {{--@if ($errors->has('id_arr'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('id_arr') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}
                    <button type="submit" class="btn btn btn-info submit" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                    <a class="btn btn-success" style="float:right;margin-right: 10px;" onclick="window.history.go(-1)"><i class="fa fa-angle-double-left"></i> 返回</a>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
@endsection