@extends('admin.layouts.'.$layouts)
@section('content')
    <div class="panel panel-headline">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">绑定地推商家二维码</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 消费分润比例即用户消费后分给商家的比例,20表示20%</div>--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post" id="form"  action="{{url('/pushQr/bind_pushqr_submit')}}" class="form-horizontal">
                <div class="panel-body">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">团员手机号码 </label>
                        <div class="col-sm-10">
                            <input type="test" class="form-control" name="mobile"   placeholder="地推人员手机号码" value="{{old('mobile')}}">
                            @if ($errors->has('mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">团员身份</label>
                        <div class="col-sm-10">
                            <select name="level" class="form-control">
                                @if(session('pushQr_user_info')->level==1)
                                    {{--运营商--}}
                                    <option value="2">地推市场部领导人</option>
                                @elseif(session('pushQr_user_info')->level==2)
                                    {{--地推市场部领导人--}}
                                    <option value="3">地推人员</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">二维码数量</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="push_number"   placeholder="二维码数量" value="{{old('push_number')}}">
                            @if ($errors->has('push_number'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('push_number') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>



                    <button type="submit" class="btn btn btn-info submit" style="float:right;"><i class="fa fa-paper-plane-o"></i> 绑定</button>
                    {{--<a href="{{url('/seller/pushqr_list')}}?id={{$_GET['id']}}&mobile={{$_GET['mobile']}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>--}}
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
@endsection