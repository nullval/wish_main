<!doctype html>
<html lang="en">

<head>
    <title>绑定二维码</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{asset('home/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/linearicons/style.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/chartist/css/chartist-custom.css')}}">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{asset('home/css/main.css')}}">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="{{asset('home/css/demo.css')}}">
    <link rel="stylesheet" href="{{asset('home/css/myself.css')}}">
    <link rel="stylesheet" href="{{asset('bootstrap/bootstrap-switch/bootstrap-switch.css')}}">
    <!-- GOOGLE FONTS -->
{{--<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">--}}
<!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('home/img/apple-icon.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('home/img/favicon.png')}}">
    <link href="{{asset('home/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('css/myself.css')}}">
    {{--<script src="{{asset('seller/js/uploadPreview.js')}}" type="text/javascript"></script>--}}
    <style>
        .sidebar .nav > li > a:focus, .sidebar .nav > li > a.active{
            border-left-color:#AEB7C2;
        }
        .sidebar .nav > li > a:hover i, .sidebar .nav > li > a:focus i, .sidebar .nav > li > a.active i{
            color: #AEB7C2;
        }
    </style>
    <style type="text/css">
        /*body, html{width: 100%;height: 100%;margin:0;font-family:"微软雅黑";font-size:14px;}*/
        #l-map{height:250px;width:100%;}
        /*#r-result{width:100%;}*/
    </style>
</head>
<body>
<!-- WRAPPER -->
<div id="wrapper">
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <form action="{{url('/scan_code')}}" method="post" role="form">
                    <legend>绑定商户</legend>
                    <div class="form-group">
                        <label for="">商户手机号</label>
                        <input type="tel" class="form-control" name="mobile" placeholder="请输入您的商户手机号" required>
                    </div>
                    <input type="hidden" name="qr_id" value="{{$qr_id}}">
                    <button type="submit" class="btn btn-primary">提交</button>
                </form>
            </div>
        </div>
    </div>
    <!-- END MAIN -->
    <div class="clearfix"></div>
    <footer hidden>
        <div class="container-fluid">
            <p class="copyright">Copyright &copy; 2017.Company name All rights reserved.</p>
        </div>
    </footer>
</div>
<!-- END WRAPPER -->
<!-- Javascript -->
<script src="{{asset('home/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('home/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('home/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('home/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('home/vendor/chartist/js/chartist.min.js')}}"></script>
<script src="{{asset('home/js/klorofil-common.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor/ueditor.config.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor/ueditor.all.min.js')}}"> </script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor/lang/zh-cn/zh-cn.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('bootstrap/bootstrap-switch/bootstrap-switch.js')}}"> </script>
<script src="{{asset('home/js/moment-with-locales.js')}}"></script>
<script src="{{asset('home/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('home/js/bootstrap-datetimepicker.zh-CN.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('js/myself.js')}}"> </script>
<script>

</script>
</body>

</html>
