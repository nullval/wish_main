@extends('admin.layouts.'.$layouts)

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">地推商家二维码列表</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>

                <div class="col-sm-12" style="margin-top: 10px;">
                    <input type="text" id="order_sn" class="form-control" placeholder="码编号" style="width: 200px;float:left;" value="@if(isset($_GET['order_sn'])&&(!empty($_GET['order_sn']))){{$_GET['order_sn']}}@endif"/>
                    <input type="text" id="operate_mobile" class="form-control" placeholder="当前持有人手机号码" style="width: 200px;float:left;" value="@if(isset($_GET['operate_mobile'])&&(!empty($_GET['operate_mobile']))){{$_GET['operate_mobile']}}@endif"/>
                    <div class="btn-group">
                         <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
                    {{--@if($role_id!=1)--}}
                        {{--<a href="{{url('/order/add_pushQr_order')}}" class="btn btn-info" style="float: right;"><i class="fa fa-plus"></i> 申请二维码</a>--}}
                    {{--@endif--}}
                         <button type="button" class="btn btn-primary" id="downzip">下载</button>
                    </div>
                </div>
                <table class="table table-striped" style="margin-top: 20px;">
                    <thead>
                    <tr>
                        <th>
                            <input type="checkbox" id="allcheck">
                        </th>
                        <th>编号</th>
                        <th>码编号</th>
                        <th>状态</th>
                        <th>当前持有人</th>
                        {{--<th>归属人身份</th>--}}
                        <th>绑定商家</th>
                        {{--<th>归属运营中心</th>--}}
                        <th>创建时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($list as $k=>$v)
                            <tr>
                                <td>
                                    <input type="checkbox" class="mycheck" data-id="{{$v->id}}">
                                </td>
                                <td>{{$v->id}}</td>
                                <td>{{$v->code_number}}</td>
                                <td>
                                    @if($v->state==1)
                                            未绑定
                                        @elseif($v->state==2)
                                            已绑定
                                        @elseif($v->state==3)
                                            已废用
                                    @endif
                                </td>
                                <td>{{$v->push_mobile or '暂无'}}</td>
                                {{--<td>--}}
                                    {{--@if($v->user_level==1)--}}
                                        {{--运营商--}}
                                    {{--@elseif($v->user_level==2)--}}
                                        {{--地推市场部领导人--}}
                                    {{--@elseif($v->user_level==3)--}}
                                        {{--地推人员--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                                <td>{{$v->merchantName or '暂无'}}</td>
                                {{--<td>{{$v->operate_mobile or '暂无'}}</td>--}}
                                <td>{{$v->add_time}}</td>
                                <td>
                                    @if($v->state==1)
                                    <a href="{{url('/order/bind_seller')}}?id={{$v->id}}"><span class="label label label-success"><i class="fa fa-eye"></i> 绑定商家</span></a>
                                    @endif
                                    <a href="{{url('/order/pushQr_download')}}?id={{$v->id}}"><span class="label label label-success"><i class="fa fa-eye"></i> 下载</span></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $list->appends(request()->input())->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- END RECENT PURCHASES -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post"   action="{{url('/seller/create_pushqr_submit')}}" id="form" class="form-horizontal">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$_GET['id'] or 0}}" name="id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">生成地推商家二维码</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-4 control-label">运营中心 </label>
                            <div class="col-sm-6">
                                <label class="control-label">{{$data['mobile']}}</label>
                            </div>
                        </div>
                        <div>
                            <label class="col-sm-4 control-label">二维码数量</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" autocomplete="off" id="safe_code" name="push_number"/>
                            </div>
                        </div>
                        <div style="clear: both"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success submit">生成</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            $('#notify_type,#type').change(function(){
                location_a();
            });
            $('#search').click(function(){
                location_a();
            });
        });
        function location_a() {
            location.href="{{url('/order/pushQr_list')}}?operate_mobile="+$('#operate_mobile').val()
                +"&code_number="+$('#order_sn').val();
        }
        
        $("#allcheck").on('click',function () {
            if(this.checked==true){
                $(".mycheck").each(function () {
                    this.checked=true;
                });
            }else{
                // $(".mycheck").attr('checked',false);
                $(".mycheck").each(function () {
                    this.checked=false;
                });
            }
        });
        
        $("#downzip").on('click',function () {
            var tmp=[];
            $(".mycheck:checked").each(function () {
                tmp.push(this.getAttribute('data-id'));
            });
            if(tmp.length<1){
                alert('你什么也没选');
                return false;
            }
            window.location.href="{{url('/order/pushQr_download')}}?ids="+tmp.join(',');
        });
    </script>
@endsection