@extends('admin.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" />
@section('content')
    <div class="panel panel-headline">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">编辑商家入驻信息</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/seller/edit_apply_submit')}}" class="form-horizontal" enctype='multipart/form-data'>
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$info->id or old('id')}}" name="id">
                    <input type="hidden" value="{{$info->uid or old('uid')}}" name="uid">

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                        <div class="col-sm-10">
                            {{$info->mobile}}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商家名称 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="companyName"   placeholder="商家名称" value="{{$info->companyName or old('companyName')}}">
                            @if ($errors->has('companyName'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('companyName') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商家地址 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="companyAddress"   placeholder="商家地址" value="{{$info->companyAddress or old('companyAddress')}}">
                            @if ($errors->has('companyAddress'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('companyAddress') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">营业执照号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="businessLicense"   placeholder="营业执照号" value="{{$info->businessLicense or old('businessLicense')}}">
                            @if ($errors->has('businessLicense'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('businessLicense') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">营业执照</label>
                            <ul id="warp">
                                <li>
                                    <input type="hidden" value="{{$info->businessimage}}" name="old_businessimage"/>
                                    <div style="text-align: left;" class="col-sm-4">
                                        <img id="imgShow_WU_FILE_0" width="100%"  src="@if(empty($info->businessimage)) {{asset('/images/timg.jpg')}} @else {{url($info->businessimage)}} @endif" />
                                    </div>
                                    <div class="col-sm-2" style="text-align: right;">
                                        <input type="file" id="up_img_WU_FILE_0" name="businessimage" />
                                        <input  type="hidden" id="uploadfile" name="businessimagebase64" />
                                    </div>
                                </li>
                            </ul>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">组织机构代码 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="organizationCode"   placeholder="组织机构代码" value="{{$info->organizationCode or old('organizationCode')}}">
                            @if ($errors->has('organizationCode'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('organizationCode') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">联系电话 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="telephone"   placeholder="联系电话" value="{{$info->telephone or old('telephone')}}">
                            @if ($errors->has('telephone'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('telephone') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">法人姓名 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="legalName"   placeholder="法人姓名" value="{{$info->legalName or old('legalName')}}">
                            @if ($errors->has('legalName'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('legalName') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">法人证件类型 </label>
                        <div class="col-sm-10">
                            身份证
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">法人证件号码 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="legalIds"   placeholder="法人证件号码" value="{{$info->legalIds or old('legalIds')}}">
                            @if ($errors->has('legalIds'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('legalIds') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">法人手机号码 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="legalPhone"   placeholder="法人手机号码" value="{{$info->legalPhone or old('legalPhone')}}">
                            @if ($errors->has('legalPhone'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('legalPhone') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">银行卡号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="bank_account"   placeholder="银行卡号" value="{{$bank_info->bank_account or old('bank_account')}}">
                            @if ($errors->has('bank_account'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('bank_account') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">持卡人姓名 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="account_name"   placeholder="持卡人姓名" value="{{$bank_info->account_name or old('account_name')}}">
                            @if ($errors->has('account_name'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('account_name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">开户银行 </label>
                        <div class="col-sm-10">
                            <select id="bank_name" class="form-control" onchange="getbankNo()">
                                <option >--请选择银行获取联行号--</option>
                                @foreach($bank_list as $v)
                                    <option @if($v->bank_name==$bank_info->account_bank) selected @endif>{{$v->bank_name}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" class="form-control" readonly name="account_bank" id="account_bank"    placeholder="银行卡所属地区银行的联行号" value="{{$bank_info->account_bank or old('account_bank')}}">
                            @if ($errors->has('account_bank'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('account_bank') }}</span>
                            @endif
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">银行卡所属地区银行联行号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" readonly name="ibankno"  id="ibankno"  placeholder="银行卡所属地区银行的联行号" value="{{$bank_info->ibankno or old('ibankno')}}">
                            <span class="help-block m-b-none text-danger">请通过选定银行名字获取联行号</span>
                            @if ($errors->has('ibankno'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('ibankno') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>





                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 修改</button>
                    <a href="{{url('seller/seller_list')}}" class="btn btn-success" style="float:right;margin-right:10px;"><i class="fa fa-angle-double-left"></i> 返回</a>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
<script>
    $(document).ready(function(){
        $("#up_img_WU_FILE_0").change(function(){
            var v = $(this).val();
            var reader = new FileReader();
            reader.readAsDataURL(this.files[0]);
            reader.onload = function(e){
                console.log(e.target.result);
                $('#uploadfile').val(e.target.result);
            };
        });
    });
    function getbankNo() {
        var bank_name=$("select").val();
        $.ajax({
            url:'{{url('/seller/get_bankNo')}}',
            type:'POST', //GET
            async:true,    //或false,是否异步
            data:{
                bank_name:bank_name
            },
            timeout:5000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success:function(data){

                if (data.code==0){
                    alert(data.message)
                }else {
                    $('#ibankno').val(data.data.lbnkNo);
                    $('#account_bank').val(data.data.bank_name);
                }
            },
            error:function(xhr,textStatus){
                console.log('错误')
                console.log(xhr)
                console.log(textStatus)
            },

        })

    }


</script>
@endsection