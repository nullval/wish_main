@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">商家管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if ($errors->has('error'))
                        <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                    @endif
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="apply_status">--}}
                        {{--<option value="0">--入驻状态--</option>--}}
                        {{--<option value="1" @if($_GET['apply_status']==1) selected @endif>尚未提交完整信息到第三方审核</option>--}}
                        {{--<option value="2" @if($_GET['apply_status']==2) selected @endif>审核中</option>--}}
                        {{--<option value="3" @if($_GET['apply_status']==3) selected @endif>审核通过</option>--}}
                        {{--<option value="4" @if($_GET['apply_status']==4) selected @endif>审核失败</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="col-sm-8">
                    <input type="text" id="mobile" class="form-control" placeholder="商家手机号"  style="width: 200px;float:left;" value="{{$_GET['mobile']}}"/>
                    <input type="text" id="name" class="form-control" placeholder="商家名称"  style="width: 200px;float:left;" value="{{$_GET['name']}}"/>
                    <input type="text" id="agent_mobile" class="form-control" placeholder="填写人(机主)" style="width: 200px;float:left;" value="{{$_GET['agent_mobile']}}"/>
                    <input type="text" class="form-control"  style='width: 1px;float:left;padding:0px'/>
                    <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
                </div>
                <div class="col-sm-2" style="float: right;">
                    <a href="{{url('/seller/seller_add')}}" class="btn btn-info" style="float: right;"><i class="fa fa-plus"></i> 入驻商家</a>
                </div>
                {{--<div class="col-sm-2" style="float: right;">--}}
                    {{--<a href="{{url('/seller/ght_seller_add')}}" class="btn btn-info" style="float: right;"><i class="fa fa-plus"></i> 商家入驻</a>--}}
                {{--</div>--}}
                {{--ceshi--}}
                {{--测试--}}
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>商家手机号</th>
                        <th>商家名称</th>
                        <th>商家法人</th>
                        <th>填写人</th>
                        {{--<th>信用积分</th>--}}
                        <th>创建时间</th>
                        <th>是否启用</th>
                        {{--<th>入驻状态</th>--}}
                        <th>商家信息</th>
                        <th>提现银行信息</th>
                        {{--<th>入驻审核</th>--}}
                        {{--<th>基本配置</th>--}}
                        {{--<th>财务</th>--}}
                        {{--<th>返现对账单</th>--}}
                        {{--<th>查看前台</th>--}}
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($seller_list as $k=>$v)
                            <tr>
                                <td>{{$v['id']}}</td>
                                <td>{{$v['mobile'] or '暂无'}}</td>
                                <td>{{$v['merchantName'] or '暂无'}}</td>
                                <td>{{$v['corpmanName'] or '暂无'}}</td>
                                <td>{{$v['agent_mobile'] or '暂无'}}</td>
                                {{--<td>{{$v['credit']}}</td>--}}
                                <td>{{$v['created_at'] or '暂无'}}</td>
                                <td>
                                    @if($v['status']==0) <a href="#" class="status" tip="1" tip1="{{$v['id']}}"><span class="label label-success">启用</span></a>
                                    @else <a href="#" class="status" tip="0" tip1="{{$v['id']}}"><span class="label label-danger">禁用</span></a>
                                    @endif
                                </td>
                                {{--<td>--}}
                                    {{--@if($v['is_ruzhu']==0)--}}
                                        {{--尚未提交完整信息到第三方审核--}}
                                    {{--@else--}}
                                        {{--@if($v['apply_status']==1)--}}
                                            {{--未申请--}}
                                        {{--@elseif($v['apply_status']==2)--}}
                                            {{--审核中--}}
                                        {{--@elseif($v['apply_status']==3)--}}
                                            {{--审核通过--}}
                                        {{--@elseif($v['apply_status']==4)--}}
                                            {{--审核失败--}}
                                        {{--@else--}}
                                        {{--@endif--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                                <td>
                                    {{--@if($v['is_ruzhu']==0)--}}
                                        {{--<a href="{{url('/seller/ght_seller_add')}}?uid={{$v['id']}}"><span class="label label label-warning"><i class="fa fa-edit"></i> 提交第三方审核</span></a>--}}
                                    {{--@else--}}
                                        <a href="{{url('/seller/ght_seller_edit')}}?uid={{$v['id']}}"><span class="label label label-warning"><i class="fa fa-edit"></i> 编辑</span></a>
                                    {{--@endif--}}
                                </td>
                                <td>
                                    <a href="{{url('/seller/seller_withdraw_bank')}}?uid={{$v['id']}}"><span class="label label label-warning"><i class="fa fa-edit"></i> 编辑</span></a>
                                </td>
                                {{--<td>--}}
                                    {{--<a href="{{url('/seller/verity_apply')}}?uid={{$v['id']}}"><span class="label label-warning"><i class="fa fa-edit"></i> 审核</span>--}}
                                    {{--</a>--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                    {{--<a href="{{url('/seller/edit_fee')}}?type=2&uid={{$v['id']}}"><span class="label label-warning"><i class="fa fa-edit"></i> 配置</span>--}}
                                    {{--</a>--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                    {{--<a href="{{url('/seller/finance')}}?id={{$v['id']}}&type=1&status=0&year=2017&user_type=2"><span class="label label label-success"><i class="fa fa-eye"></i> 查看</span></a>--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                    {{--<a href="{{url('/seller/statement_list')}}?id={{$v['id']}}"><span class="label label label-success"><i class="fa fa-eye"></i> 查看</span></a>--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                    {{--<a href="#" class="check_font" tip="{{$v['id']}}" data-toggle="modal" data-target="#myModal"><span class="label label label-success"><i class="fa fa-eye"></i> 查看</span></a>--}}
                                {{--</td>--}}
                          </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $seller_list->appends([
                    'mobile'=>isset($_GET['mobile'])?$_GET['mobile']:'',
                    'name'=>isset($_GET['name'])?$_GET['name']:'',
                    'agent_mobile'=>isset($_GET['agent_mobile'])?$_GET['agent_mobile']:'',
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $seller_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div method="post"   action="{{url('/config/check_safe_code')}}" class="form-horizontal">
                            <input type="hidden" name="seller_id" id="seller_id">
                            {{csrf_field()}}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">查看商家后台</h4>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <div class="col-md-4">安全码</div>
                                    <div class="col-md-6">
                                        <input type="password" class="form-control" autocomplete="off" id="safe_code" name="safe_code"/>
                                    </div>
                                </div>
                                <div style="clear: both"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" id="check">查看</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            var _token=$('input[name=_token]').attr('tip');
            //启用/禁用用户
            $('.status').click(function(){
                var $this=$(this);
                var status=$(this).attr('tip');
                var id=$(this).attr('tip1');
                var type=2;
                $.ajax({
                    type: "POST",
                    url: '/seller/change_status',
                    data: {_token:_token, status:status,id:id,type:type},
                    dataType: "json",
                    success: function(data){
                        if(data.code==1){
                            var span=$this.children('span');
                            if(status==0){
                                $this.attr('tip',1);
                                span.html('启用');
                                span.removeClass('label-danger');
                                span.addClass('label-success');
                            }else{
                                $this.attr('tip',0);
                                span.html('禁用');
                                span.removeClass('label-success');
                                span.addClass('label-danger');
                            }
                        }
                    }
                });
            });

            $('#apply_status').change(function(){
                location_a();
            });
            $('#search').click(function () {
                location_a();
            });

            $('.check_font').each(function(){
                $(this).click(function(){
                    $('#seller_id').val($(this).attr('tip'));
                });
            });
            $('#check').click(function(){
                $.ajax({
                    type: "post",
                    url: "{{url('/config/check_safe_code')}}",
                    data: {safe_code:$("#safe_code").val(), seller_id:$("#seller_id").val(),_token:$('input[name=_token]').val()},
                    dataType: "json",
                    success: function(data){
                        if(data['code']==1){
                            window.open("http://{{env('APP_SELLER_DOMAIN')}}{{env('APP_DOMAIN')}}/nologin?code="+data['data']['code']);
                        }else{
                            alert(data['message']);
                        }
                        $('#myModal,.modal-backdrop').hide();
                    }
                });
            });
        });
        function location_a() {
            location.href="{{url('/seller/seller_list')}}?mobile="+$('#mobile').val()
                +"&apply_status=" +$('#apply_status').val()
                +"&name=" +$('#name').val()
                +"&agent_mobile=" +$('#agent_mobile').val();
        }
    </script>
@endsection