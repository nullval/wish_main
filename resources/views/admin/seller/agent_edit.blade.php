@extends('admin.layouts.app')
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">修改机主</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
                {{--<div class="alert alert-warning"> 添加代理请慎重操作,若是会员推代理一次性奖励1000元,代理推代理每出一台获得50元/台的奖励,此操作奖励将直接到账,添加代理必须由系统管理员短信验证</div>--}}
            </div>
            <form method="post" id="id_form" action="{{url('/seller/agent_add_submit')}} ">
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden"  name="id"    value="{{$_GET['id']}}">
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">机主手机号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mobile"   placeholder="机主手机号码" value="{{$info->mobile or old('mobile')}}">
                            @if ($errors->has('mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">机主名称</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="remark_name"   placeholder="机主名称" value="{{$info->name or old('remark_name')}}">
                            @if ($errors->has('remark_name'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('remark_name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label" id="text_name">推荐人手机号</label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="inviter"   placeholder="推荐人手机号" value="{{old('inviter')}}">--}}
                            {{--@if ($errors->has('inviter'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('inviter') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">推荐人</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="invite_mobile"   placeholder="推荐人" value="{{$info->invite_mobile or old('invite_mobile')}}">
                            @if ($errors->has('invite_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('invite_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">运营中心</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="operate_mobile"   placeholder="运营中心" value="{{$info->operate_mobile or old('operate_mobile')}}">
                            @if ($errors->has('operate_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('operate_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    @if(config('app.env')!='online')
                        {{--<div class="form-group">--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label" id="text_name">是否报单</label>--}}
                            {{--<div class="col-sm-10">--}}
                                {{--<input type="radio" class="control-label" name="is_customs" value="1" checked/> 开启--}}
                                {{--<input type="radio" class="control-label" name="is_customs" value="0"/> 关闭--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                        {{--<div class="hr-line-dashed"></div>--}}
                    @endif

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label" id="text_name">机主身份</label>--}}
                        {{--<div class="col-sm-2">--}}
                            {{--<select class="form-control" name="level">--}}
                                {{--@foreach(config('agent.level') as $k=>$v)--}}
                                    {{--<option value="{{$k}}" @if($info->level==$k) selected @endif>{{$v['name']}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label" id="text_name">绑定pos机</label>--}}
                        {{--<div class="col-sm-4">--}}
                            {{--<input type="text" id="terminalId" class="form-control" placeholder="终端号" style="width: 200px;float:left;" value=""/>--}}
                            {{--<a type="submit" id="add" class="btn btn btn-info" style="float:left;"><i class="fa fa-plus"></i></a>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-1">--}}
                            {{--<input type="text" readonly id="count" class="form-control" placeholder="0"/>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    {{--<div class="form-group">--}}
                        {{--<div class="col-sm-12" id="add_te">--}}
                            {{--<span id="add_te" style="display: none"></span>--}}
                            {{--<div class="col-sm-3">--}}
                                {{--<input name="terminalId[]" type="text"  class="form-control" readonly style="width: 150px;float:left;" value=""/>--}}
                                {{--<a style="float:left;padding: 5px;cursor: pointer" class="del_btn">X</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">短信验证码<br> </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input ype="text" class="form-control" id="sms_code" placeholder="请输入验证码" autocomplete="off" name="sms_code" style="width: 140px;float:left;"   style="margin-top: 10px">--}}
                            {{--<input type="button" class="btn btn btn-info" id="btn" value="发送手机验证码"  onclick="send_sms()">--}}
                            {{--@if ($errors->has('sms_code'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('sms_code') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    <a id="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-plus"></i> 编辑</a>

                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')
    <script>
        $(function () {
            var _token=$('input[name=_token]').attr('tip');
            $('#add').click(function(){
                terminalId=$('#terminalId').val();
                console.log(terminalId);
                is_set=1;
                $(".t").each(function () {
                    if($(this).val()==terminalId){
                        alert('该终端号已存在');
                        is_set=0;
                    }
                });
                if(is_set==0){
                     return false;
                }
                // $("input[name=terminalId[]]").each(function () {
                //     if($(this).val()==terminalId){
                //         alert('该终端号已存在');
                //         return false;
                //     }
                // })
                $.ajax({
                    type: "POST",
                    url: '/seller/add_terminalId',
                    data: {_token:_token, terminalId:terminalId},
                    dataType: "json",
                    success: function(data){
                        if(data.code==1){
                            $('#add_te').append('<div class="col-sm-2"><input name="terminalId[]" type="text"  class="form-control t" readonly style="width: 200px;float:left;" value="'+terminalId+'"/><a style="float:left;padding: 5px;cursor: pointer" class="del_btn">X</a></div>');
                            $('#count').val(Number($('#count').val())+1);
                        }else{
                            alert(data.message);
                        }
                    }
                });
            });
            $(document).on('click','.del_btn',function(){
                $(this).parent().remove();
                $('#count').val(Number($('#count').val())-1);
            });
            $('#submit').click(function () {

                mobile=$('input[name=mobile]').val();
                if(mobile==''){
                    alert('机主手机号不能为空');
                    return false;
                }
                if(confirm('确定编辑'+mobile)){
                    $('#id_form').submit();
                }
            });

        })
        function send_sms(){
            //发送短信验证码,并设置调用时间
            $.ajax({
                type: "POST",
                url: '{{url('/send_sms')}}',
                data: {mobile: 13631272493,type:17},
                dataType: "json",
                success: function (data) {
                    if (data.code == 1) {
                        alert('手机短信已发送');
                    } else {
                        alert(data.message);
                    }
                    new invokeSettime("#btn");
                }
            });
        }
    </script>
@endsection