@extends('admin.layouts.app')

@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">提现银行卡信息</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/seller/seller_withdraw_bank_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}

                    <input type="hidden" value="{{$_GET['uid']}}" name="uid">
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">银行卡号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="bank_account"   placeholder="银行卡号" value="{{$bank_info->bank_account or old('bank_account')}}">
                            @if ($errors->has('bank_account'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('bank_account') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">持卡人</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="account_name"   placeholder="持卡人" value="{{$bank_info->account_name or old('account_name')}}">
                            @if ($errors->has('account_name'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('account_name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">身份证</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="identity"   placeholder="身份证" value="{{$bank_info->identity or old('identity')}}">
                            @if ($errors->has('identity'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('identity') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">所在银行预留手机号码</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="phone"   placeholder="所在银行预留手机号码" value="{{$bank_info->phone or old('phone')}}">
                            @if ($errors->has('phone'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">开户银行 </label>
                        <div class="col-sm-10">
                            <select id="bank_name" class="form-control" onchange="getbankNo()">
                                <option >--请选择银行获取联行号--</option>
                                @foreach($bank_list as $v)
                                    <option @if(isset($bank_info))@if($bank_info->ibankno==$v->lbnkNo) selected @endif @endif>{{$v->bank_name}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" class="form-control" readonly name="account_bank" id="account_bank" value="{{$bank_info->account_bank or old('account_bank')}}">
                            @if ($errors->has('account_bank'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('account_bank') }}</span>
                            @endif
                        </div>

                    </div>


                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>



                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">银行卡所属地区银行联行号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" readonly name="ibankno"  id="ibankno"  placeholder="银行卡所属地区银行的联行号" value="{{$bank_info->ibankno or old('ibankno')}}">
                            <span class="help-block m-b-none text-danger">请通过选定银行名字获取联行号</span>
                            @if ($errors->has('ibankno'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('ibankno') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>



                    @if(isset($bank_info))
                        <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 修改</button>
                    @else
                        <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-plus"></i> 发布</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')
    <script>

        function getbankNo() {
            var bank_name=$("#bank_name").val();
            $.ajax({
                url:'{{url('/seller/get_bankNo')}}',
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    bank_name:bank_name
                },
                timeout:5000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data){

                    if (data.code==0){
                        alert(data.message)
                    }else {
                        $('#ibankno').val(data.data.lbnkNo);
                        $('#account_bank').val(data.data.bank_name);
                    }
                },
                error:function(xhr,textStatus){
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },

            })

        }

    </script>
@endsection