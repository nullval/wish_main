@extends('admin.layouts.app')
@section('content')
    <div class="panel panel-headline">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">编辑商家入驻信息</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 消费分润比例即用户消费后分给商家的比例,20表示20%</div>--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/seller/edit_fee_submit?type=2')}}" class="form-horizontal">
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$_GET['uid']}}" name="uid">

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->mobile}}</label>
                        </div>
                    </div>
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}


                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">消费分润比例 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="number" class="form-control" name="seller_fee"   placeholder="消费分润比例" value="{{$info->seller_fee or old('seller_fee')}}">--}}
                            {{--@if ($errors->has('seller_fee'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('seller_fee') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">是否允许提现</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="is_allow_withdraw" value="1" @if($info->is_allow_withdraw==1) checked @endif/> 开启
                            <input type="radio" class="control-label" name="is_allow_withdraw" value="2" @if($info->is_allow_withdraw==2) checked @endif/> 关闭
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">是否允许补贴积分</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="is_allow_back_point" value="1" @if($info->is_allow_back_point==1) checked @endif/> 开启
                            <input type="radio" class="control-label" name="is_allow_back_point" value="2" @if($info->is_allow_back_point==2) checked @endif/> 关闭
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 修改</button>
                    <a href="{{url('/seller/seller_list')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
@endsection