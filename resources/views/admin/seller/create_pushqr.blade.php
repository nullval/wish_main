@extends('admin.layouts.app')
@section('content')
    <div class="panel panel-headline">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">批量生成地推商家二维码</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 消费分润比例即用户消费后分给商家的比例,20表示20%</div>--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post" id="form"  action="{{url('/seller/create_pushqr_submit')}}" class="form-horizontal">
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$_GET['id'] or 0}}" name="id">

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">运营中心 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$_GET['mobile']}}</label>
                        </div>
                    </div>
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}


                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">消费分润比例 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="number" class="form-control" name="seller_fee"   placeholder="消费分润比例" value="{{$info->seller_fee or old('seller_fee')}}">--}}
                            {{--@if ($errors->has('seller_fee'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('seller_fee') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">二维码数量</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="push_number"   placeholder="二维码数量" value="{{old('push_number')}}">
                            @if ($errors->has('push_number'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('push_number') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>



                    <button type="button" class="btn btn btn-info submit" style="float:right;"><i class="fa fa-paper-plane-o"></i> 生成</button>
                    <a href="{{url('/seller/pushqr_list')}}?id={{$_GET['id']}}&mobile={{$_GET['mobile']}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
    <script>
        $(function () {
            $('.submit').click(function () {
                var num=$('input[name=push_number]').val();
                if(num==''||num==0){
                    alert('地推商家二维码不能为0');
                    return false;
                }
                var message='确定要为运营中心{{$_GET['mobile']}}生成'+num+'张地推商家二维码';
                if(confirm(message)){
                    $('#form').submit();
                }
            });
        })
    </script>
@endsection