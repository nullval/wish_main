@extends('admin.layouts.app')

@section('content')
    <link rel="stylesheet" href="{{asset('orgchart/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('orgchart/css/jquery.orgchart.css')}}">
    <link rel="stylesheet" href="{{asset('orgchart/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('orgchart/style.css')}}">
    <input type="hidden" value="{{$user_tree}}" id="data">
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">系谱图</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>


            {{--<div class="panel-body no-padding">--}}
                {{--<div class="col-sm-6">--}}
                    {{--<input type="text" id="member_id" class="form-control" placeholder="会员编号" style="width: 200px;float:left;" value="@if(isset($_GET['member_id'])){{$_GET['member_id']}}@endif"/>--}}
                    {{--<a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>--}}
                {{--</div>--}}
                {{--<div class="col-sm-6">--}}
                    {{--<div class="alert alert-info"> 由于数据流太大,只显示到20层,想要查看下面层级请搜索接点人查看详情</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="col-sm-12" style="background: white;height:1000px">
                <!-- RECENT PURCHASES -->
                <div id="chart-container" style="background: white;height:960px;"></div>
            </div>
            <!-- END RECENT PURCHASES -->
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')
    <script type="text/javascript" src="{{asset('orgchart/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('orgchart/js/jquery.orgchart.js')}}"></script>
    <script type="text/javascript" src="{{asset('orgchart/scripts.js')}}"></script>
    <script>
        $(function(){
            $('.copyright').hide();
        })
@endsection