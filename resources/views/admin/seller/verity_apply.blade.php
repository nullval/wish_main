@extends('admin.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" />
@section('content')
    <div class="panel panel-headline">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">审核商家入驻信息</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/seller/verity_apply_submit')}}" class="form-horizontal">
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$info->id or old('id')}}" name="id">
                    <input type="hidden" value="{{$info->uid or old('uid')}}" name="uid">

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->mobile}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商家名称 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->companyName}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商家地址 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->companyAddress}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">营业执照号 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->businessLicense}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">营业执照</label>
                        <div class="col-sm-4">
                            {{--<a class="img1" href="@if(!empty($info->businessimage)) {{$info->businessimage}} @else {{asset('/images/timg.jpg')}} @endif">--}}
                                <img src="@if(!empty($info->businessimage)) {{$info->businessimage}} @else {{asset('/images/timg.jpg')}} @endif" width="100%"/>
                            {{--</a>--}}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">组织机构代码 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->organizationCode}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">联系电话 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->telephone}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">法人姓名 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->legalName}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">法人证件类型 </label>
                        <div class="col-sm-10">
                            身份证
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">法人证件号码 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->legalIds}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">法人手机号码 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->legalPhone}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">开户银行名称 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->parentBankName}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">开户行支行名称 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->bankName}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">持卡人姓名 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->account_name}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">银行卡号 </label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->account_no}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">申请时间</label>
                        <div class="col-sm-10">
                            <label class="control-label">{{$info->apply_time or '暂无'}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">申请pos机台数</label>--}}
                        {{--<div class="col-sm-10">{{$info->pos_num}}</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}


                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">已打款金额</label>--}}
                        {{--<div class="col-sm-10">{{$info->pos_amount}}</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}


                    @if($info['status']>2)


                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">审核时间</label>
                            <div class="col-sm-10">{{$info['vertify_time']}}</div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">申请状态</label>
                            <div class="col-sm-10">
                                <label class="control-label">@if($info['status']==2)    处理中
                                    @elseif($info['status']==1) 未提交申请
                                    @elseif($info['status']==3) 审核通过
                                    @elseif($info['status']==4) 审核失败
                                    @endif</label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">申请反馈</label>
                            <div class="col-sm-10 ">
                                <label class="control-label">{{$info->remark}}</label>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                        <a href="{{url('seller/seller_list')}}" class="btn btn-success" style="float:right;"><i class="fa fa-angle-double-left"></i> 返回</a>
                    @else

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label" id="text_name">审核</label>
                            <div class="col-sm-10">
                                <input type="radio" class="control-label" name="status" value="2" checked @if($info->status==2) checked @endif/> 审核中
                                <input type="radio" class="control-label" name="status" value="3" checked @if($info->status==3) checked @endif/> 审核通过
                                <input type="radio" class="control-label" name="status" value="4" checked @if($info->status==4) checked @endif/> 审核失败
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label" id="text_name">申请反馈</label>
                            <div class="col-sm-10">
                                <textarea name="remark" class="form-control" rows="3">{{$info->remark}}</textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                        <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 提交</button>
                        <a href="{{url('seller/seller_list')}}" class="btn btn-success" style="float:right;margin-right:10px;"><i class="fa fa-angle-double-left"></i> 返回</a>

                    @endif
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
<script src="{{asset('/js/jquery.min.js')}}"></script>
<script>
    $(function(){
        $('#search').click(function(){
            terminalId=$("#terminalId").val();
            isset=0;
            $('.terminalId').each(function(){
                if(terminalId==$(this).val()){
                    isset=1;
                }
            });
            if(isset==1){
                alert('该pos机已存在列表中');return;
            }
            $.ajax({
                type: "POST",
                url: '/pos/ajax_get_pos_detail',
                data: {terminalId:terminalId},
                dataType: "json",
                success: function(data){
                    if(data.code==0){
                        alert(data.message);
                    }else{
                        data=data.data;
                        html='';
                        html+="<div class='col-sm-4' style='margin-top: 10px;'>"
                                    +"<input type='text' name='terminalId[]' class='form-control terminalId'  style='width: 150px;float:left;' readonly  value='"+data.terminalId+"' >"
                                    +"<a  class='btn del_terminalId' style='float:left;'> X </a>"
                                +"</div>";
//                        alert(html);
                        $('#zan').after(html);
                    }
                }
            });
        });
        //删除pos机
        $(document).on("click", ".del_terminalId", function(){
            //此处的$(this)指$( "#testDiv")，而非$(document)
            $(this).parent().remove();
        });
    });

</script>
@endsection