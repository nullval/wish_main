@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">@if($_GET['user_type']==1) 用户 @elseif($_GET['user_type']==2) 商家 @elseif($_GET['user_type']==3) 机主 @endif财务管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding" style="margin-bottom: 20px;">
                <div class="col-sm-12">
                    <span class="form-control">@if($_GET['user_type']==1) 用户 @elseif($_GET['user_type']==2) 商家 @elseif($_GET['user_type']==3) 机主 @endif手机号 : {{$data['user_info']->mobile}} </span>
                </div>
                <div class="col-sm-12">
                    <span class="form-control">收益钱包 : {{get_last_two_num($data['x_purse_info']->balance-$data['x_purse_info']->freeze_value)}} 冻结额度 : {{get_last_two_num($data['x_purse_info']->freeze_value)}}</span>
                    <span class="form-control">消费积分 : {{get_last_two_num($data['cost_purse_info']->balance-$data['cost_purse_info']->freeze_value)}} 冻结额度 : {{get_last_two_num($data['cost_purse_info']->freeze_value)}}</span>
                </div>
                @if($_GET['user_type']==1||$_GET['user_type']==2)
                    <div class="col-sm-12">
                        <span class="form-control">@if($_GET['user_type']==2) 补贴@endif积分 : {{get_last_two_num($data['j_purse_info']->balance-$data['j_purse_info']->freeze_value)}} 冻结额度 : {{get_last_two_num($data['j_purse_info']->freeze_value)}}</span>
                    </div>
                @endif
                @if($_GET['user_type']==1)
                    <div class="col-sm-12">
                        <span class="form-control">红包收益 : {{get_last_two_num($data['r_purse_info']->balance-$data['r_purse_info']->freeze_value)}} 冻结额度 : {{get_last_two_num($data['r_purse_info']->freeze_value)}}</span>
                        <span class="form-control">CHQ : {{get_last_two_num($data['t_purse_info']['balance'])}} 冻结额度 : {{get_last_two_num($data['t_purse_info']['freeze_value'])}}</span>
                        <span class="form-control">区块链地址 : {{$data['block_address']}}</span>
                    </div>
                @endif
                @if($_GET['user_type']==2)
                    <div class="col-sm-12">
                        <span class="form-control">货款钱包 : {{get_last_two_num($data['h_purse_info']->balance-$data['h_purse_info']->freeze_value)}} 冻结额度 : {{get_last_two_num($data['h_purse_info']->freeze_value)}}</span>
                        <span class="form-control">待补贴积分 : {{get_last_two_num($data['de_purse_info']->balance-$data['de_purse_info']->freeze_value)}} 冻结额度 : {{get_last_two_num($data['de_purse_info']->freeze_value)}}</span>
                        <span class="form-control">商家充值钱包 : {{get_last_two_num($data['b_purse_info']->balance-$data['b_purse_info']->freeze_value)}} 冻结额度 : {{get_last_two_num($data['b_purse_info']->freeze_value)}}</span>
                        <span class="form-control">CHQ : {{get_last_two_num($data['t_purse_info']['balance'])}} 冻结额度 : {{get_last_two_num($data['t_purse_info']['freeze_value'])}}</span>
                    </div>
                @endif
            </div>
        </div>
    </div>

    @if($_GET['type']==2)
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">货款开通的支付平台业务</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>业务类型</th>
                        <th>费率类型</th>
                        <th>费率</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($busi_list as $k=>$v)
                        <tr>
                            <td>{{$k}}</td>
                            <td>{{$v['busiName']}}</td>
                            <td>
                                @if($v['futureRateType']==1) 百分比
                                @else 单笔固定金额
                                @endif
                            </td>
                            <td>{{$v['futureRateValue']}}</td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>
    @endif

    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">财务流水</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="year">--}}
                        {{--<option value="2017">2017</option>--}}
                        {{--<option value="2018" @if(isset($_GET['year'])&&$_GET['year']==2018||date('Y')==2018) selected @endif>2018</option>--}}
                        {{--<option value="2019" @if(isset($_GET['year'])&&$_GET['year']==2019||date('Y')==2019) selected @endif>2019</option>--}}
                        {{--<option value="2020" @if(isset($_GET['year'])&&$_GET['year']==2020||date('Y')==2020) selected @endif>2020</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="col-sm-2">
                    <select class="form-control" id="status">
                        <option value="0">全部</option>
                        <option value="1" @if($_GET['status']==1) selected @endif>收入</option>
                        <option value="2" @if($_GET['status']==2) selected @endif>支出</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="form-control" id="type">
                        <option value="1" @if($_GET['type']==1) selected @endif>收益钱包</option>
                        <option value="10" @if($_GET['type']==10) selected @endif>消费积分</option>
                        @if($_GET['user_type']==1||$_GET['user_type']==2)
                            <option value="3" @if($_GET['type']==3) selected @endif>积分钱包</option>
                            {{--<option value="5" @if(isset($_GET['type'])&&$_GET['type']==5) selected @endif>TD币钱包</option>--}}
                        @endif
                        @if($_GET['user_type']==1)
                            <option value="12" @if($_GET['type']==12) selected @endif>红包收益</option>
                        @endif
                        @if($_GET['user_type']==2)
                            <option value="4" @if($_GET['type']==4) selected @endif>货款钱包</option>
                            <option value="7" @if($_GET['type']==7) selected @endif>商家充值钱包</option>
                            <option value="10001" @if($_GET['type']==10001) selected @endif>待补贴积分</option>
                            {{--<option value="8" @if(isset($_GET['type'])&&$_GET['type']==8) selected @endif>微信钱包</option>--}}
                            {{--<option value="9" @if(isset($_GET['type'])&&$_GET['type']==9) selected @endif>支付宝钱包</option>--}}
                        @endif
                        @if($_GET['user_type']==3)
                            {{--<option value="13" @if($_GET['type']==13) selected @endif>文交所积分</option>--}}
                            {{--<option value="10" @if($_GET['type']==10) selected @endif>消费积分</option>--}}
                            {{--<option value="11" @if($_GET['type']==11) selected @endif>兑换积分</option>--}}
                        @endif
                        @if($_GET['user_type']==4)
                            {{--<option value="10" @if($_GET['type']==10) selected @endif>消费积分</option>--}}
                        @endif
                    </select>
                </div>
                <div class="col-sm-6">
                    总收入 : <span class="text-danger">+{{get_last_two_num($data['total_into'])}}</span> 总支出 : <span class="text-success">-{{get_last_two_num($data['total_out'])}}</span>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>流水编号</th>
                        <th>类型</th>
                        <th>转入/转出会员编号</th>
                        <th>会员类型</th>
                        <th>变动额度</th>
                        <th>余额</th>
                        <th>转账时间</th>
                        <th>描述</th>
                        <th>备注</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($transfer as $k=>$v)
                        <tr>
                            <td>{{$v->transfer_id}}</td>
                            <td>
                                @if($v->into_purse_id==$data['purse_id']) 收入
                                @else 支出
                                @endif
                            </td>
                            <td>{{$v->member_id}}</td>
                            <td>
                                @if($v->into_purse_id==$data['purse_id']) {{get_owner_type_name($v->out_owner_type)}}
                                @else {{get_owner_type_name($v->into_owner_type)}}
                                @endif
                            </td>
                            <td>
                                @if($v->into_purse_id==$data['purse_id']) <span class="text-danger">+{{get_last_two_num($v->into_amount)}}</span>
                                @else <span class="text-success">-{{get_last_two_num($v->into_amount)}}</span>
                                @endif
                            </td>
                            <td>
                                @if($v->into_purse_id==$data['purse_id']) {{get_last_two_num($v->into_balance)}}
                                @else {{get_last_two_num($v->out_balance)}}
                                @endif
                            </td>
                            <td>{{date('Y-m-d H:i:s',$v->create_time)}}</td>
                            <td>{{$v->detail}}</td>
                            <td>{{$v->remarks}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $transfer->appends([
                    'id'=>$_GET['id'] ?? 0,
                    'status'=>$_GET['status'] ?? 0,
                    'type'=>$_GET['type'] ?? 0,
                    'user_type'=>$_GET['user_type'] ?? 1
                    ])->links() }}
                    {{--<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>--}}
                    {{--<div class="col-md-6 text-right"><a href="#" class="btn btn-primary">View All Purchases</a></div>--}}
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            $('#status,#type').change(function(){
                location_a();
            });
        });
        function location_a(){
            location.href="{{url('/seller/finance')}}?id={{$_GET['id']}}&user_type={{$_GET['user_type']}}&type="+$('#type').val()+"&status="+$('#status').val();
        }
    </script>
@endsection