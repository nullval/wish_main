@extends('admin.layouts.app')

@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">新增运营中心</h3>
                <div class="hr-line-solid"></div>
                <div class="alert alert-warning"> 三级运营中心可以不填写推荐人,上级运营中心手机号和推荐人手机号都可作为运营中心得推荐人，填一个即可</div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/seller/operate_add_submit')}} ">
                <div class="panel-body">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">运营中心手机号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mobile"   placeholder="运营中心手机号码" value="{{old('mobile')}}">
                            @if ($errors->has('mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">上级运营中心手机号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="parent_mobile"   placeholder="运营中心手机号码(可不填)" value="{{old('parent_mobile')}}">
                            @if ($errors->has('parent_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('parent_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">推荐人手机号(会员)</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="inviter_user_mobile"   placeholder="推荐人手机号(会员)" value="{{old('inviter_user_mobile')}}">
                            @if ($errors->has('inviter_user_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('inviter_user_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" >级别</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="level">
                                <option value="1" @if($_GET['level']==1) selected @endif>一级运营中心</option>
                                <option value="2" @if($_GET['level']==2) selected @endif>二级运营中心</option>
                                <option value="3" @if($_GET['level']==3) selected @endif>三级运营中心</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">备注</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="remarks"   placeholder="备注" value="{{old('remarks')}}">
                            @if ($errors->has('remarks'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('remarks') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label" id="text_name">推荐人手机号</label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="inviter"   placeholder="推荐人手机号" value="{{old('inviter')}}">--}}
                            {{--@if ($errors->has('inviter'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('inviter') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}




                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-plus"></i> 新增</button>
                    <a href="{{url('seller/operate_list')}}" class="btn btn-success" style="float:right;margin-right:10px;"><i class="fa fa-angle-double-left"></i> 返回</a>

                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')
@endsection