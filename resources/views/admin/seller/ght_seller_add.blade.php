@extends('admin.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" />
<link href="{{asset('sanji/css/main.css')}}" rel="stylesheet">
@section('content')
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">商家入驻信息提交第三方</h3>
            {{--<div class="hr-line-solid"></div>--}}
        </div>
    <div class="panel panel-headline">
        <ul id="myTab" class="nav nav-tabs">
            <li class="active">
                <a href="#A" data-toggle="tab">
                    基础信息登记
                </a>
            </li>
            <li><a href="#B"  data-toggle="tab">银行卡信息登记</a></li>
            <li><a href="#C" data-toggle="tab">开通支付平台业务</a></li>
            <li><a href="#D" data-toggle="tab">亿贤补全资料</a></li>
        </ul>


        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade in active" id="A">
                <div class="alert alert-warning"> 请确认好商家信息再提交第三方审核,已提交的信息无法再次编辑或再次提交第三方审核,只有整个流程走完再编辑</div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
                    <form method="post"   action="{{url('/seller/ght_seller_add_submit_step1')}}" id="form1" class="form-horizontal" enctype='multipart/form-data'>
                        <input type="hidden" value="{{$basic_info->id}}" name="basic_id"/>
                        <div class="panel-body">
                            {{csrf_field()}}


                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">登录手机号</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="mobile"  readonly  placeholder="登录手机号" value="{{$seller_info->mobile or old('mobile')}}">
                                    @if ($errors->has('mobile'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            {{--<div class="form-group">--}}
                                {{--<label for="signin-text" class="col-sm-2 control-label">登录密码 </label>--}}
                                {{--<div class="col-sm-10">--}}
                                    {{--<input type="password" class="form-control" name="password"   placeholder="登录密码" value="{{old('password')}}">--}}
                                    {{--@if ($errors->has('password'))--}}
                                        {{--<span class="help-block m-b-none text-danger">{{ $errors->first('password') }}</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="clearfix"></div>--}}
                            {{--<div class="hr-line-dashed"></div>--}}

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户名称 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="merchantName"   placeholder="商户名称" value="{{$basic_info->merchantName or old('merchantName')}}">
                                    @if ($errors->has('merchantName'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('merchantName') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户简称 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="shortName"   placeholder="商户简称" value="{{$basic_info->shortName or old('shortName')}}">
                                    @if ($errors->has('shortName'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('shortName') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户城市 </label>
                                <div class="col-sm-10">
                                    {{--@foreach($city_arr as $k=>$v)--}}
                                    {{--@if ($basic_info->city==$v->cityCode)--}}
                                    {{--<label class="control-label">{{$v->cityName}}</label>--}}
                                    {{--@endif--}}
                                    {{--@endforeach--}}
                                    <input type="button" class="btn btn btn-info" id="btn" value="搜索" style="margin-bottom: 10px;" onclick="get_city()">
                                    <input type="text" class="form-control" id="cityName1" placeholder="请输入城市" autocomplete="off" name="cityName1" style="width: 140px; margin-bottom: 10px; float: left"   style="margin-top: 10px">
                                    <select id="aa" name="city" class="form-control">
                                        @foreach($city_arr as $k=>$v)
                                            <option class="city_option" tip="{{$v->cityName}}" value="{{$v->cityCode}}"
                                                    @if(!empty($basic_info))
                                                    @if($basic_info->city==$v->cityCode)
                                                    selected
                                                    @endif
                                                    @endif
                                            >{{$v->cityName}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('city'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('city') }}</span>
                                    @endif
                                </div>
                            </div>


                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">省市区选择</label>
                                <div class="col-sm-10 form-inline">

                                    <div id="distpicker5">
                                        <div class="form-group">
                                            <label class="sr-only" for="province10">Province</label>
                                            <select class="form-control" id="province10" name="province10" onchange="get_province_code()" style="margin-right: 35px;"></select>
                                        </div>
                                        <input type="hidden" name="province_code" id="province_code">
                                        <div class="form-group">
                                            <label class="sr-only" for="city10">City</label>
                                            <select class="form-control" id="city10" name="city10" onchange="get_city_code()" style="margin-right: 35px;"></select>
                                        </div>
                                        <input type="hidden" name="city_code" id="city_code">

                                        <div class="form-group">
                                            <label class="sr-only" for="district10">District</label>
                                            <select class="form-control" id="district10" name="district10" onchange="get_district_code()"></select>
                                        </div>
                                        <input type="hidden" name="district_code" id="district_code">

                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户地址 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="merchantAddress"  id="merchantAddress" placeholder="商户地址" value="{{$basic_info->merchantAddress or old('merchantAddress')}}">
                                    @if ($errors->has('merchantAddress'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('merchantAddress') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商家店铺定位 </label>
                                <div class="col-sm-10">
                                    <div style="margin-top: 6px;float: left">纬度：</div><input  type="text" class="form-control" id="lat" name="lat" value="{{$basic_info->lat or old('lat')}}" style="width: 80%;margin-bottom: 10px"/>
                                    <div style="margin-top: 6px;float: left">经度：</div><input  type="text" class="form-control" id="lng" name="lng" value="{{$basic_info->lng or old('lng')}}"style="width: 80%;margin-bottom: 10px"/>
                                    <div id="l-map"></div>
                                    <div id="r-result"><input class="form-control" type="text" id="suggestId" size="20" value="百度" placeholder="请输入店铺地址的关键字" style="width:100%;margin-top: 20px" /></div>
                                    <div id="searchResultPanel" style="border:1px solid #C0C0C0;width:150px;height:auto; display:none;"></div>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">客服电话 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="servicePhone"   placeholder="客服电话" value="{{$basic_info->servicePhone or old('servicePhone')}}">
                                    @if ($errors->has('servicePhone'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('servicePhone') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户类型 </label>
                                <div class="col-sm-10">
                                    {{--@if($basic_info->merchantType=='00')--}}
                                    {{--<label class="control-label" style="padding-left: 10px">公司商户</label>--}}

                                    {{--@elseif($basic_info->merchantType=='01')--}}
                                    {{--<label class="control-label"style="padding-left: 10px" >个体商户</label>--}}
                                    {{--@endif--}}
                                    {{--<input type="hidden" name="merchantType" value="{{$basic_info->merchantType}}">--}}
                                    <select name="merchantType" class="form-control">
                                        {{--<option value="00"@if(!empty($basic_info)) @if($basic_info->merchantType=='00') selected @endif @endif>公司商户</option>--}}
                                        <option value="01"@if(!empty($basic_info)) @if($basic_info->merchantType=='01') selected @endif @endif>个体商户</option>
                                    </select>
                                    <span class="help-block m-b-none text-success">商户类型即为判断是否对公对私账户，如个体商户为对私账户</span>

                                </div>

                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">经营类目 </label>
                                <div class="col-sm-10">
                                    {{--@foreach($category_arr as $k=>$v)--}}
                                    {{--@if ($basic_info->category==$v->categoryCode)--}}
                                    {{--<label class="control-label" style="padding-left: 10px">{{$v->categoryName}}</label>--}}
                                    {{--@endif--}}
                                    {{--@endforeach--}}
                                    {{--<input type="hidden" name="category" value="{{$basic_info->category}}">--}}
                                    <select name="category" class="form-control">
                                        @foreach($category_arr as $k=>$v)
                                            <option value="{{$v->categoryCode}}"
                                                    @if(!empty($basic_info))
                                                    @if ($basic_info->category==$v->categoryCode)
                                                    selected
                                                    @endif
                                                    @endif
                                            >{{$v->categoryName}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('category') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">法人姓名 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="corpmanName"   placeholder="法人姓名" value="{{$basic_info->corpmanName or old('corpmanName')}}">
                                    @if ($errors->has('corpmanName'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('corpmanName') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">法人身份证 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="corpmanId"   placeholder="法人身份证" value="{{$basic_info->corpmanId or old('corpmanId')}}">
                                    @if ($errors->has('corpmanId'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('corpmanId') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">法人联系手机 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="corpmanMobile" readonly  placeholder="法人联系手机" value="{{$basic_info->corpmanMobile or old('corpmanMobile')}}">
                                    @if ($errors->has('corpmanMobile'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('corpmanMobile') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>


                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">开户行全称 </label>
                                <div class="col-sm-10">
                                    {{--@foreach($bank_arr as $k=>$v)--}}
                                    {{--@if ($basic_info->bankCode==$v->bankCode)--}}
                                    {{--<label class="control-label" style="padding-left: 10px">{{$v->bankName}}</label>--}}
                                    {{--@endif--}}
                                    {{--@endforeach--}}
                                    {{--<input type="hidden" value="{{$basic_info->bankCode}}" name="bankCode"/>--}}
                                    {{--<input type="hidden" value="{{$basic_info->bankName}}" name="bankName"/>--}}

                                    <select name="bankName" id="bankName" class="form-control">
                                        <option value="0" >请选择银行</option>
                                        @foreach($bank_arr as $k=>$v)
                                            <option value="{{$v->bankName}}" tip="{{$v->bankCode}}"
                                                    @if(!empty($basic_info))
                                                    @if ($basic_info->bankCode==$v->bankCode)
                                                    selected
                                                    @endif
                                                    @endif
                                            >{{$v->bankName}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" value="{{$basic_info->bankCode or old('bankCode')}}" name="bankCode"/>
                                    @if ($errors->has('category'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('category') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>



                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">营业执照号 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="businessLicense"   placeholder="营业执照号" value="{{$basic_info->businessLicense or old('businessLicense')}}">
                                    @if ($errors->has('businessLicense'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('businessLicense') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">营业执照</label>
                                {{--<div style="text-align: left;" class="col-sm-4">--}}
                                {{--<img id="imgShow_WU_FILE_0" width="100%"  src="{{$basic_info->businessimage}}" />--}}
                                {{--</div>--}}
                                <ul id="warp">
                                    <li>
                                        <input type="hidden" value="" name="old_businessimage"/>
                                        <div style="text-align: left;" class="col-sm-4">
                                            <img id="imgShow_WU_FILE_0" width="100%"  src="{{$basic_info->businessimage or asset('/images/timg.jpg')}}" />
                                        </div>
                                        <div class="col-sm-2" style="text-align: right;">
                                            <input type="file" id="up_img_WU_FILE_0" name="businessimage" />
                                            <input  type="hidden" id="uploadfile" name="businessimagebase64" />
                                            <input  type="hidden" name="businessimage" value="{{$basic_info->businessimage or old('businessimage')}}" />
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            {{--<div class="clearfix"></div>--}}
                            {{--<div class="hr-line-dashed"></div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="signin-text" class="col-sm-2 control-label">开户银行 </label>--}}
                                {{--<div class="col-sm-10">--}}
                                    {{--<select id="bank_name" class="form-control" onchange="getbankNo()">--}}
                                        {{--<option >--请选择银行获取联行号--</option>--}}
                                        {{--@foreach($bank_list as $v)--}}
                                            {{--<option >{{$v->bank_name}}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                    {{--<input type="hidden" class="form-control" readonly name="account_bank" id="account_bank"    placeholder="银行卡所属地区银行的联行号" value="{{old('account_bank')}}">--}}
                                    {{--@if ($errors->has('account_bank'))--}}
                                        {{--<span class="help-block m-b-none text-danger">{{ $errors->first('account_bank') }}</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}

                            {{--</div>--}}
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">开户行账号 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="bankaccountNo"   placeholder="开户行账号" value="{{$basic_info->bankaccountNo or old('bankaccountNo')}}">
                                    @if ($errors->has('bankaccountNo'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountNo') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">开户户名 </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="bankaccountName"   placeholder="开户户名" value="{{$basic_info->bankaccountName or old('bankaccountName')}}">
                                    @if ($errors->has('bankaccountName'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountName') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>



                            {{--<div class="form-group">--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label">银行卡所属地区银行联行号 </label>--}}
                            {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" readonly name="ibankno"  id="ibankno"  placeholder="银行卡所属地区银行的联行号" value="{{old('ibankno')}}">--}}
                            {{--<span class="help-block m-b-none text-danger">请通过选定银行名字获取联行号</span>--}}
                            {{--@if ($errors->has('ibankno'))--}}
                            {{--<span class="help-block m-b-none text-danger">{{ $errors->first('ibankno') }}</span>--}}
                            {{--@endif--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="clearfix"></div>--}}
                            {{--<div class="hr-line-dashed"></div>--}}

                            @if($button_judge['real_basic']==0)
                                <button type="button" class="btn btn btn-info btn_other_1" tip="1" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交第三方</button>
                                <button type="button" class="btn btn btn-info btn_edit_1" tip="1" style="float:right;margin-right: 10px;"><i class="fa fa-paper-plane-o"></i> 修改信息</button>
                            @else
                                <span class="text-primary">已提交给第三方</span>
                            @endif
                        </div>
                    </form>
            </div>
            <div class="tab-pane fade" id="B">
                <form method="post"   action="{{url('/seller/ght_seller_add_submit_step2')}}" id="form2" class="form-horizontal" enctype='multipart/form-data'>
                    <input type="hidden" value="{{$basic_info->id}}" name="basic_id"/>
                    <div class="panel-body">
                        {{csrf_field()}}


                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="mobile"  readonly  placeholder="商家手机号" value="{{$seller_info->mobile or old('mobile')}}">
                                <input type="hidden" value="{{$seller_info->id}}" name="uid"/>

                                @if ($errors->has('mobile'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                                @endif
                            </div>
                        </div>
                        {{--<div class="clearfix"></div>--}}
                        {{--<div class="hr-line-dashed"></div>--}}

                        {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">登录密码 </label>--}}
                        {{--<div class="col-sm-10">--}}
                        {{--<input type="password" class="form-control" name="password"   placeholder="登录密码" value="{{old('password')}}">--}}
                        {{--@if ($errors->has('password'))--}}
                        {{--<span class="help-block m-b-none text-danger">{{ $errors->first('password') }}</span>--}}
                        {{--@endif--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">子商户编码 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="merchantId" readonly  placeholder="即法人手机号码" value="{{$basic_info->merchantId or old( 'merchantId')}}">

                                @if ($errors->has('merchantId'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('merchantId') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">开户行全称 </label>
                            <div class="col-sm-10">
                                <select name="bankName1" id="bankName1" class="form-control">
                                    <option>--请选择银行--</option>
                                    @foreach($bank_arr as $k=>$v)
                                        <option value="{{$v->bankName}}" tip="{{$v->bankCode}}"
                                                @if(isset($bank_info))
                                                @if($bank_info->bankCode==$v->bankCode)
                                                selected
                                                @endif
                                                @endif>{{$v->bankName}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" value="@if(isset($bank_info)){{$bank_info->bankCode}} @endif" name="bankCode1"/>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">账号属性 </label>
                            <div class="col-sm-10" style="padding-top: 8px">
                                <input type="radio" class="control-label" name="bankaccProp" value="0"@if(isset($bank_info)) @if($bank_info->bankaccProp==0) checked @endif @endif/> 私人
                                <input type="radio" class="control-label" name="bankaccProp" value="1"@if(isset($bank_info)) @if($bank_info->bankaccProp==1) checked @endif @endif /> 公司
                                @if ($errors->has('bankaccProp'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccProp') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">银行卡信息 </label>
                            <div class="col-sm-10" style="padding-top: 8px">
                                <input type="radio" class="control-label" name="bankaccountType" value="1"@if(isset($bank_info)) @if($bank_info->bankaccountType==1) checked @endif @endif/> 借记卡
                                <input type="radio" class="control-label" name="bankaccountType" value="2"@if(isset($bank_info)) @if($bank_info->bankaccountType==2) checked @endif @endif /> 贷记卡
                                {{--<input type="radio" class="control-label" name="bankaccountType" value="3"@if(isset($bank_info)) @if($bank_info->bankaccountType==3) checked @endif @endif /> 存折--}}
                                @if ($errors->has('bankaccountType'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountType') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">持卡人姓名 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name"   placeholder="持卡人姓名" value="{{$bank_info->name or old('name')}}">
                                @if ($errors->has('name'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">银行卡号 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="bankaccountNo"   placeholder="银行卡号" value="{{$bank_info->bankaccountNo or old('bankaccountNo')}}">
                                @if ($errors->has('bankaccountNo'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountNo') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">身份证号码 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="certNo"   placeholder="身份证号码" value="{{$bank_info->certNo or old('certNo')}}">
                                @if ($errors->has('certNo'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('certNo') }}</span>
                                @endif
                            </div>
                        </div>


                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">银行联行号的选择范围 </label>
                            <div class="col-sm-10">
                                <select id="bank_name" class="form-control" onchange="getbankNo()">
                                    <option >--请选择银行获取联行号--</option>
                                    @foreach($bank_list as $v)
                                        <option @if($bank_info->bankbranchNo==$v->lbnkNo) selected @endif >{{$v->bank_name}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" class="form-control" readonly name="account_bank" id="account_bank" value="{{$bank_info->account_bank or old('account_bank')}}">
                                @if ($errors->has('account_bank'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('account_bank') }}</span>
                                @endif
                            </div>

                        </div>


                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>



                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">银行卡所属地区银行联行号 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" readonly name="ibankno"  id="ibankno"  placeholder="银行卡所属地区银行的联行号" value="{{$bank_info->bankbranchNo or old('ibankno')}}">
                                <span class="help-block m-b-none text-danger">请通过选定银行名字获取联行号</span>
                                @if ($errors->has('ibankno'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('ibankno') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>


                        @if($button_judge['real_bank']==0)
                            <button type="button" class="btn btn btn-info btn_other_2" tip="2" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交第三方</button>
                            <button type="button" class="btn btn btn-info btn_edit_2" tip="2" style="float:right;margin-right: 10px;"><i class="fa fa-paper-plane-o"></i> 修改信息</button>
                        @else
                            <span class="text-primary">已提交给第三方</span>
                        @endif
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="C">
                <div class="alert alert-warning"> 费率即用户消费后分给平台的比例,20表示给平台的折扣为20%</div>
                <form method="post"   action="{{url('/seller/ght_seller_add_submit_step3')}}" id="form3" class="form-horizontal" enctype='multipart/form-data'>
                    <input type="hidden" value="{{$basic_info->id}}" name="basic_id"/>
                    <div class="panel-body">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="mobile"  readonly placeholder="商家手机号" value="{{$seller_info->mobile or old('mobile')}}">
                                <input type="hidden" value="{{$seller_info->id}}" name="uid"/>
                                @if ($errors->has('mobile'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                                @endif
                            </div>
                        </div>
                        {{--<div class="clearfix"></div>--}}
                        {{--<div class="hr-line-dashed"></div>--}}

                        {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">登录密码 </label>--}}
                        {{--<div class="col-sm-10">--}}
                        {{--<input type="password" class="form-control" name="password"   placeholder="登录密码" value="{{old('password')}}">--}}
                        {{--@if ($errors->has('password'))--}}
                        {{--<span class="help-block m-b-none text-danger">{{ $errors->first('password') }}</span>--}}
                        {{--@endif--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">子商户编码 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="merchantId"  readonly placeholder="即法人手机号码" value="{{$basic_info->corpmanMobile or old('merchantId')}}">
                                @if ($errors->has('merchantId'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('merchantId') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">费率 </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="futureRate"   placeholder="费率" value="{{$busi_info->futureRateValue or  old('futureRate')}}">
                                @if ($errors->has('futureRate'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('futureRate') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>


                        @if($button_judge['real_busi']==0)
                            <button type="button" class="btn btn btn-info btn_other_3" tip="3" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交第三方</button>
                            <button type="button" class="btn btn btn-info btn_edit_3" tip="3" style="float:right;margin-right: 10px;"><i class="fa fa-paper-plane-o"></i> 修改信息</button>
                        @else
                            <span class="text-primary">已提交给第三方</span>
                        @endif

                    </div>
                </form>
            </div>



            <div class="tab-pane fade" id="D">
                <form method="post"   action="{{url('/seller/yixian_submit')}}" id="form4" class="form-horizontal" enctype='multipart/form-data'>
                    <input type="hidden" value="{{$seller_info->id}}" name="seller_id"/>
                    <input type="hidden" value="{{$yixian_info->id}}" name="yixian_id"/>
                    <div class="panel-body">
                        {{csrf_field()}}

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label" id="text_name">税务登记证号</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="TaxRegistrationNo"   placeholder="税务登记证号"  value="{{$yixian_info->TaxRegistrationNo or old('TaxRegistrationNo')}}">
                                @if ($errors->has('TaxRegistrationNo'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('TaxRegistrationNo') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label" id="text_name">开户行所在地区</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="bankarea"   placeholder="开户行所在地区"  value="{{$yixian_info->bankarea or old('bankarea')}}">
                                @if ($errors->has('bankarea'))
                                    <span class="help-block m-b-none text-danger">{{ $errors->first('bankarea') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>


                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label" id="text_name">银行卡正面照片</label>
                            <ul id="warp">
                                <li>
                                    {{--<input type="hidden" value="" name="old_businessimage"/>--}}
                                    <div style="text-align: left;" class="col-sm-4">
                                        {{--<img id="imgShow_WU_FILE_0" width="100%"  src="@if(old('businessimagebase64') != ""){{asset('/images/timg.jpg')}}@elseif(session('businessimagebase64') != '') {{session('img')}} @else {{asset('/images/timg.jpg')}}@endif" />--}}
                                        <img id="imgShow_WU_FILE_5_input" width="100%"  src="{{$yixian_info->bankUrl1 or asset('/images/timg.jpg') }}" />

                                    </div>
                                    <div class="col-sm-2" style="text-align: right;">
                                        <input type="file" id="imgShow_WU_FILE_5" name="businessimage5" />
                                        <input type="hidden" id="imgShow_WU_FILE_5_hid" name="bankUrl1" value="{{$yixian_info->bankUrl1 or asset('/images/timg.jpg') }}"/>
                                        @if ($errors->has('bankUrl1'))
                                            <span class="help-block m-b-none text-danger">{{ $errors->first('bankUrl1') }}</span>
                                        @endif

                                    </div>


                                </li>
                            </ul>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label" id="text_name">银行卡反面照片</label>
                            <ul id="warp">
                                <li>
                                    {{--<input type="hidden" value="" name="old_businessimage"/>--}}
                                    <div style="text-align: left;" class="col-sm-4">
                                        {{--<img id="imgShow_WU_FILE_0" width="100%"  src="@if(old('businessimagebase64') != ""){{asset('/images/timg.jpg')}}@elseif(session('businessimagebase64') != '') {{session('img')}} @else {{asset('/images/timg.jpg')}}@endif" />--}}
                                        <img id="imgShow_WU_FILE_1_input" width="100%"  src="{{$yixian_info->bankUrl2 or asset('/images/timg.jpg') }}" />
                                    </div>
                                    <div class="col-sm-2" style="text-align: right;">
                                        <input type="file" id="imgShow_WU_FILE_1" name="businessimage1" />
                                        <input type="hidden" id="imgShow_WU_FILE_1_hid" name="bankUrl2" value="{{$yixian_info->bankUrl2 or asset('/images/timg.jpg') }} "/>
                                        @if ($errors->has('bankUrl2'))
                                            <span class="help-block m-b-none text-danger">{{ $errors->first('bankUrl2') }}</span>
                                        @endif

                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label" id="text_name">商户身份证正面照片</label>
                            <ul id="warp">
                                <li>
                                    {{--<input type="hidden" value="" name="old_businessimage"/>--}}
                                    <div style="text-align: left;" class="col-sm-4">
                                        {{--<img id="imgShow_WU_FILE_0" width="100%"  src="@if(old('businessimagebase64') != ""){{asset('/images/timg.jpg')}}@elseif(session('businessimagebase64') != '') {{session('img')}} @else {{asset('/images/timg.jpg')}}@endif" />--}}
                                        <img id="imgShow_WU_FILE_2_input" width="100%"  src="{{$yixian_info->ICurl1 or asset('/images/timg.jpg')}}" />

                                    </div>
                                    <div class="col-sm-2" style="text-align: right;">
                                        <input type="file" id="imgShow_WU_FILE_2" name="businessimage2" />
                                        <input type="hidden" id="imgShow_WU_FILE_2_hid" name="ICurl1" value="{{$yixian_info->ICurl1 or asset('/images/timg.jpg')}}"/>
                                        @if ($errors->has('ICurl1'))
                                            <span class="help-block m-b-none text-danger">{{ $errors->first('ICurl1') }}</span>
                                        @endif
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>


                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label" id="text_name">商户身份证反面照片</label>
                            <ul id="warp">
                                <li>
                                    {{--<input type="hidden" value="" name="old_businessimage"/>--}}
                                    <div style="text-align: left;" class="col-sm-4">
                                        {{--<img id="imgShow_WU_FILE_0" width="100%"  src="@if(old('businessimagebase64') != ""){{asset('/images/timg.jpg')}}@elseif(session('businessimagebase64') != '') {{session('img')}} @else {{asset('/images/timg.jpg')}}@endif" />--}}
                                        <img id="imgShow_WU_FILE_3_input" width="100%"  src="{{$yixian_info->ICurl2 or asset('/images/timg.jpg')}}" />

                                    </div>
                                    <div class="col-sm-2" style="text-align: right;">
                                        <input type="file" id="imgShow_WU_FILE_3" name="businessimage3" />
                                        <input type="hidden" id="imgShow_WU_FILE_3_hid" name="ICurl2" value="{{$yixian_info->ICurl2 or asset('/images/timg.jpg')}}"/>
                                        @if ($errors->has('ICurl2'))
                                            <span class="help-block m-b-none text-danger">{{ $errors->first('ICurl2') }}</span>
                                        @endif
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label" id="text_name">商户手持身份证正面照片</label>
                            <ul id="warp">
                                <li>
                                    {{--<input type="hidden" value="" name="old_businessimage"/>--}}
                                    <div style="text-align: left;" class="col-sm-4">
                                        {{--<img id="imgShow_WU_FILE_0" width="100%"  src="@if(old('businessimagebase64') != ""){{asset('/images/timg.jpg')}}@elseif(session('businessimagebase64') != '') {{session('img')}} @else {{asset('/images/timg.jpg')}}@endif" />--}}
                                        <img id="imgShow_WU_FILE_4_input" width="100%"  src="{{$yixian_info->merchantICurl or asset('/images/timg.jpg')}}" />

                                    </div>
                                    <div class="col-sm-2" style="text-align: right;">
                                        <input type="file" id="imgShow_WU_FILE_4" name="businessimage4" />
                                        <input type="hidden" id="imgShow_WU_FILE_4_hid" name="merchantICurl" value="{{$yixian_info->merchantICurl or asset('/images/timg.jpg')}}"/>
                                        @if ($errors->has('merchantICurl'))
                                            <span class="help-block m-b-none text-danger">{{ $errors->first('merchantICurl') }}</span>
                                        @endif
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>


                        <button type="submit" class="btn btn btn-info btn_edit_3" tip="3" style="float:right;margin-right: 10px;"><i class="fa fa-paper-plane-o"></i> 修改信息</button>


                    </div>
                </form>
            </div>
            </div>

        </div>


    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
<script src="{{asset('sanji/js/distpicker.data.js')}}"></script>
<script src="{{asset('sanji/js/distpicker.js')}}"></script>
<script src="{{asset('sanji/js/main.js')}}"></script>
<script>
    var province_name = null
    var city_name = null
    var district_name = null

    var province_code = {{substr($basic_info->area_id,0,2)}}+'0000'
    var city_code = {{substr($basic_info->area_id,0,4)}}+'00'
    var district_code = {{$basic_info->area_id}}
    $(document).ready(function(){
        //提交到第三方
        $('.btn_other_1,.btn_other_2,.btn_other_3').click(function(){
            submit_other($(this).attr('tip'));
        });
        //编辑附表
        $('.btn_edit_1,.btn_edit_2,.btn_edit_3').click(function(){
            submit_edit($(this).attr('tip'));
        });



        $('#bankName').change(function(){
            $('input[name=bankCode]').val($('#bankName option:selected').attr('tip'));

        });
        $('#bankName1').change(function(){
            $('input[name=bankCode1]').val($('#bankName1 option:selected').attr('tip'));

        });
        $("#up_img_WU_FILE_0").change(function(){
            var v = $(this).val();
            var reader = new FileReader();
            reader.readAsDataURL(this.files[0]);
            reader.onload = function(e){
                console.log(e.target.result);
                $('#uploadfile').val(e.target.result);
                $('#imgShow_WU_FILE_0').attr('src',e.target.result);
            };
        });
    });
    function submit_other(i){
        if(confirm('确定提交到第三方?')){
            $("#form"+i).attr('action','{{url('/seller/ght_seller_add_submit_step')}}'+i);
            $("#form"+i).submit();
        }
    }
    function submit_edit(i){
        if(confirm('确定编辑信息?')){
            $("#form"+i).attr('action','{{url('/seller/ght_seller_edit_copy_step')}}'+i);
            $("#form"+i).submit();
        }
    }
    function getbankNo() {
        var bank_name=$("#bank_name").val();
        $.ajax({
            url:'{{url('/seller/get_bankNo')}}',
            type:'POST', //GET
            async:true,    //或false,是否异步
            data:{
                bank_name:bank_name
            },
            timeout:5000,    //超时时间
            dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success:function(data){

                if (data.code==0){
                    alert(data.message)
                }else {
                    $('#ibankno').val(data.data.lbnkNo);
                    $('#account_bank').val(data.data.bank_name);
                }
            },
            error:function(xhr,textStatus){
                console.log('错误')
                console.log(xhr)
                console.log(textStatus)
            },

        })

    }

    function get_province_code() {
        $('#province_code').val($("#province10").find("option:selected").attr("data-code"));
        var j = 0;
        var k = 0;
        $.each(ChineseDistricts[$("#province10").find("option:selected").attr("data-code")], function(i, val) {
            j++;
            if (j == 1){
                $('#city_code').val(i);

                $.each(ChineseDistricts[i], function(i1, val1) {
                    k++;
//                    alert(val1);
                    if (k == 1){
                        $('#district_code').val(i1);
                    }
                });
            }
            if ($("#province10").find("option:selected").attr("data-code") == province_code) {
//                alert(district_name)
                $('#city_code').val(city_code);
                $('#district_code').val(district_code);
            }
        });

//        $('#city_code').val("");
//        $('#district_code').val("");
    }
    function get_city_code() {
        $('#city_code').val($("#city10").find("option:selected").attr("data-code"));
        if (typeof(ChineseDistricts[$("#city10").find("option:selected").attr("data-code")]) == 'undefined') {
            $('#district_code').val("");
        }

        var k = 0;
        $.each(ChineseDistricts[$("#city10").find("option:selected").attr("data-code")], function(i, val) {
            k++;
            if (k == 1){
                $('#district_code').val(i);
            }
        });
//        alert(ChineseDistricts[$('#province_code').val()][1])
//        $('#district_code').val("");
        get_coordinate();
    }
    function get_district_code() {
        $('#district_code').val($("#district10").find("option:selected").attr("data-code"));
        get_coordinate();
    }

    $.each(ChineseDistricts[86], function(i, val) {
        if(i==province_code){
            province_name = val;
//            alert(province_name)

        }
    });
    $.each(ChineseDistricts[province_code], function(i, val) {
        if (i == city_code){
            city_name = val;
//            alert(city_name)
        }
    });
    if (typeof(ChineseDistricts[city_code]) != 'undefined'){
        $.each(ChineseDistricts[city_code], function(i, val) {
            if (i == district_code){
                district_name = val;
//                alert(district_name)

            }
        });
    }

    $("#distpicker5").distpicker({
        province: province_name,
        city: city_name,
        district: district_name
    });
    $(document).ready(function(){
        $('#province_code').val($("#province10").find("option:selected").attr("data-code"));
        $('#city_code').val($("#city10").find("option:selected").attr("data-code"));
        $('#district_code').val($("#district10").find("option:selected").attr("data-code"));

    });

</script>
<script type="text/javascript">
    // 百度地图API功能
    function G(id) {
        return document.getElementById(id);
    }

    var map = new BMap.Map("l-map");
    map.centerAndZoom(new BMap.Point("{{$basic_info->lng}}", "{{$basic_info->lat}}"),12);                   // 初始化地图,设置城市和地图级别。

    var ac = new BMap.Autocomplete(    //建立一个自动完成的对象
            {"input" : "suggestId"
                ,"location" : map
            });

    ac.addEventListener("onhighlight", function(e) {  //鼠标放在下拉列表上的事件
        var str = "";
        var _value = e.fromitem.value;
        var value = "";
        if (e.fromitem.index > -1) {
            value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
        }
        str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;

        value = "";
        if (e.toitem.index > -1) {
            _value = e.toitem.value;
            value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
        }
        str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
        G("searchResultPanel").innerHTML = str;
    });

    var myValue;
    ac.addEventListener("onconfirm", function(e) {    //鼠标点击下拉列表后的事件
        var _value = e.item.value;
        myValue = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
        G("searchResultPanel").innerHTML ="onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue;

        setPlace();
    });

    function setPlace(){
        map.clearOverlays();    //清除地图上所有覆盖物
        function myFun(){
            var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
            map.centerAndZoom(pp, 18);
            $("#lat").val(pp.lat);
            $("#lng").val(pp.lng);
            map.addOverlay(new BMap.Marker(pp));    //添加标注
        }
        var local = new BMap.LocalSearch(map, { //智能搜索
            onSearchComplete: myFun
        });
        local.search(myValue);
    }
    map.enableScrollWheelZoom();   //启用滚轮放大缩小，默认禁用
    map.enableContinuousZoom();    //启用地图惯性拖拽，默认禁用、
    //    单击获取点击的经纬度
    map.addEventListener("click",function(e){
//        alert(e.point.lng + "," + e.point.lat);
        map.clearOverlays();
        var marker = new BMap.Marker(new BMap.Point(e.point.lng, e.point.lat)); // 创建点
        $("#lat").val(e.point.lat);
        $("#lng").val(e.point.lng);
        map.addOverlay(marker);

    });

    $("#merchantAddress").blur(function(){
        get_coordinate()
    });
    $("#province10").blur(function(){
        get_coordinate()
    });

    function get_coordinate() {
        var address = $("#province10").find("option:selected").val()+$("#city10").find("option:selected").val()+$("#district10").find("option:selected").val()+$("#merchantAddress").val();
        var city = $("#province10").find("option:selected").val()+$("#city10").find("option:selected").val();
        // 创建地址解析器实例
        var myGeo = new BMap.Geocoder();
        // 将地址解析结果显示在地图上，并调整地图视野
        myGeo.getPoint(address, function(point){
                    map.clearOverlays();
                    if (point) {
                        map.centerAndZoom(point, 16);
                        $("#lat").val(point.lat);
                        $("#lng").val(point.lng);
                        map.addOverlay(new BMap.Marker(point));
                    }
                },
                city);
    }
    if ("{{!isset($basic_info->lat)?false:true}}" && "{{!isset($basic_info->lng)?false:true}}"){
        theLocation("{{$basic_info->lng}}","{{$basic_info->lat}}")
    }
    // 用经纬度设置地图中心点
    function theLocation(lng,lat){
        map.clearOverlays();
        var new_point = new BMap.Point(lng,lat);
        var marker = new BMap.Marker(new_point);  // 创建标注
        map.addOverlay(marker);              // 将标注添加到地图中
        map.panTo(new_point);

    }
	function get_city(){
		cityName = $("#cityName1").val();
		val=$(".city_option[tip^="+cityName+"]").val();
		$('#aa').val(val);
	}
    $("#imgShow_WU_FILE_5,#imgShow_WU_FILE_1,#imgShow_WU_FILE_2,#imgShow_WU_FILE_3,#imgShow_WU_FILE_4").change(function(){
        name=$(this).attr('id');
        var v = $(this).val();
        var reader = new FileReader();
        reader.readAsDataURL(this.files[0]);
        reader.onload = function(e){
            $.ajax({
                url:'{{url('upload_img')}}',
                type:'POST', //GET
                data:{
                    imagebase64:e.target.result
                },
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data){

                    if(data.code==0){
                        alert(data.message);
                    }else{
                        filepath=data.data.filepath;
//                            alert(filepath);
                        $('#'+name+'_input').attr('src',filepath);
                        $('#'+name+'_hid').val(filepath);
                    }
                },

            })
        };
    });
</script>
@endsection