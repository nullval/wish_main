@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">机主管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                <div class="col-sm-10">
                    <input type="text" id="mobile" class="form-control" placeholder="机主手机号" style="width: 200px;float:left;" value="{{$_GET['mobile']}}"/>
                    <input type="text" id="inviter_mobile" class="form-control" placeholder="推荐机主" style="width: 200px;float:left;" value="{{$_GET['inviter_mobile']}}"/>
                    <input type="text" id="operate_mobile" class="form-control" placeholder="所属运营中心" style="width: 200px;float:left;" value="{{$_GET['operate_mobile']}}"/>
                    <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
                </div>
                <div class="col-sm-2" style="float: right;">
                    <a href="{{url('/seller/agent_add')}}" class="btn btn-info" style="float: right;"><i class="fa fa-plus"></i> 新增机主</a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>机主手机号</th>
                        <th>机主名称</th>
                        <th>推荐机主</th>
                        <th>机主身份</th>
                        <th>所属运营中心</th>
                        <th>运营中心备注</th>
                        <th>是否后台添加</th>
                        <th>创建时间</th>
                        <th>是否启用</th>
                        <th>基本配置</th>
                        <th>财务</th>
                        <th>编辑</th>
                        {{--<th>系谱图</th>--}}
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($agent_list as $k=>$v)
                            <tr>
                                <td>{{$v['id']}}</td>
                                <td>{{$v['mobile'] or '暂无'}}</td>
                                <td>{{$v['name'] or '暂无'}}</td>
                                <td>{{$v['inviter_mobile'] or '暂无'}}</td>
                                <td>{{config('agent.level')[$v['level']]['name']}}</td>
                                <td>{{$v['operate_mobile'] or '暂无'}}</td>
                                <td>{{$v['operate_remarks'] or '暂无'}}</td>
                                <td>@if($v['is_back']==1) 是 @elseif($v['is_back']==0) 否 @endif</td>
                                <td>{{$v['created_at'] or '暂无'}}</td>
                                <td>
                                    @if($v['status']==0) <a href="#" class="status" tip="1" tip1="{{$v['id']}}"><span class="label label-success">启用</span></a>
                                    @else <a href="#" class="status" tip="0" tip1="{{$v['id']}}"><span class="label label-danger">禁用</span></a>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{url('/seller/edit_fee')}}?type=3&uid={{$v['id']}}"><span class="label label-warning"><i class="fa fa-edit"></i> 配置</span>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{url('/seller/finance')}}?id={{$v['id']}}&type=1&status=0&year=2017&user_type=3"><span class="label label label-success"><i class="fa fa-eye"></i> 查看</span></a>
                                </td>
                                <td>
                                    <a href="{{url('/seller/edit_agent')}}?id={{$v['id']}}"><span class="label label label-success"><i class="fa fa-eye"></i> 编辑</span></a>
                                </td>
                                {{--<td>--}}
                                    {{--<a href="{{url('/seller/agent_tree')}}?id={{$v['id']}}"><span class="label label label-success"><i class="fa fa-eye"></i> 查看</span></a>--}}
                                {{--</td>--}}
                          </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $agent_list->appends([
                    'mobile'=>$_GET['mobile'] ?? '',
                    'inviter_mobile'=>$_GET['inviter_mobile'] ?? '',
                    'operate_mobile'=>$_GET['operate_mobile'] ?? ''
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $agent_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            var _token=$('input[name=_token]').attr('tip');
            //启用/禁用用户
            $('.status').click(function(){
                var $this=$(this);
                var status=$(this).attr('tip');
                var id=$(this).attr('tip1');
                var type=3;
                $.ajax({
                    type: "POST",
                    url: '/seller/change_status',
                    data: {_token:_token, status:status,id:id,type:type},
                    dataType: "json",
                    success: function(data){
                        if(data.code==1){
                            var span=$this.children('span');
                            if(status==0){
                                $this.attr('tip',1);
                                span.html('启用');
                                span.removeClass('label-danger');
                                span.addClass('label-success');
                            }else{
                                $this.attr('tip',0);
                                span.html('禁用');
                                span.removeClass('label-success');
                                span.addClass('label-danger');
                            }
                        }
                    }
                });
            });

            $('#search').click(function(){
                location.href="{{url('/seller/agent_list')}}?mobile="+$('#mobile').val()
                    +"&inviter_mobile=" +$('#inviter_mobile').val()
                    +"&operate_mobile="+$('#operate_mobile').val();
            });

        });
    </script>
@endsection