@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">发票管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                <div class="col-sm-10">
                    {{--<input type="text" id="terminalId" class="form-control" placeholder="终端号" style="width: 200px;float:left;" value="@if(isset($_GET['terminalId'])&&(!empty($_GET['terminalId']))){{$_GET['terminalId']}}@endif"/>--}}
                    <input type="text" id="mobile" class="form-control" placeholder="归属机主" style="width: 200px;float:left;" value="@if(isset($_GET['agent_uid'])&&(!empty($_GET['mobile']))){{$_GET['mobile']}}@endif"/>
                    <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        {{--<th>编号</th>--}}
                        <th>所属机主</th>
                        <th>发票金额</th>
                        <th>发生时间</th>
                        <th>发票抬头</th>
                        <th>发票性质</th>
                        <th>收取方式</th>
                        <th>状态</th>
                        <th>查看详情</th>
                        <th>编辑</th>
                        <th>发票备注</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($invoice_list as $k=>$v)
                            <tr>
                                {{--<td>{{$v['id']}}</td>--}}
                                <td>{{$v['mobile'] or '暂无'}}</td>
                                <td>{{$v['amount'] or '暂无'}}</td>
                                <td>{{$v['created_at'] or '暂无'}}</td>
                                <td>{{$v['Invoice_header'] or '暂无'}}</td>
                                <td>@if($v['Invoice_nature']==1)
                                        纸质发票
                                    @elseif($v['Invoice_nature']==2)
                                        电子发票
                                    @endif
                                </td>
                                <td>{{$v['method'] or '暂无'}}</td>
                                <td>@if($v['status']==1)
                                        未发货
                                    @elseif($v['status']==2)
                                        已发货
                                    @elseif($v['status']==3)
                                        已完成
                                    @endif
                                </td>
                                <td>
                                    <a href="{{url('/invoice/invoice_detail')}}?id={{$v['id']}}"><span class="label label label-success"><i class="fa fa-eye"></i> 查看</span></a></a>
                                </td>
                                <td>
                                    <a href="{{url('/invoice/invoice_edit')}}?id={{$v['id']}}"><span class="label label-info"><i class="fa fa-edit"></i> 编辑</span></a>
                                </td>
                                <td>{{$v['remarks'] or '暂无'}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $invoice_list->appends([
                     'mobile'=>isset($_GET['mobile'])?$_GET['mobile']:'',
                    ])->links() }}
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){

            $('#search').click(function(){
                location.href="{{url('/invoice/invoice_list')}}?mobile="+$('#mobile').val();
            });

        });
    </script>
@endsection