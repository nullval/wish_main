@extends('admin.layouts.app')

@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">编辑发票信息</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/invoice/invoice_edit_submit')}} ">
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$info->id}}" name="id">

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">发票序号</label>
                        <div class="col-sm-10">
                                <label class="control-label">{{$info->id}}</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">发票编号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="Invoice_number"   placeholder="发票编号" value="{{old('Invoice_number',$info->Invoice_number)}}">
                            @if ($errors->has('Invoice_number'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('Invoice_number') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">收取方式</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="method"   placeholder="收取方式" value="{{old('method',$info->method)}}">
                            @if ($errors->has('method'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('method') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">物流公司</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="Logistics_company"   placeholder="物流公司" value="{{old('Logistics_company',$info->Logistics_company)}}">
                            @if ($errors->has('Logistics_company'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('Logistics_company') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">快递编号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="express_number"   placeholder="快递编号" value="{{old('express_number',$info->express_number)}}">
                            @if ($errors->has('express_number'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('express_number') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">是否发货</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="status" value="1" @if($info->status==1) checked @endif/> 未发货
                            <input type="radio" class="control-label" name="status" value="2" @if($info->status==2) checked @endif/> 已发货
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">是否有效</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="isvalid" value="1" @if($info->isvalid==1) checked @endif/> 有效
                            <input type="radio" class="control-label" name="isvalid" value="2" @if($info->isvalid==2) checked @endif/> 无效
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">发票备注</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="remarks" style="height: 120px">{{$info->remarks}}</textarea>
                            {{--<input type="text" class="form-control" name="remarks" style="height: 150px; vertical-align: top; "  placeholder="发票备注" value="{{old('remarks',$info->remarks)}}">--}}
                            {{--@if ($errors->has('remarks'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('remarks') }}</span>--}}
                            {{--@endif--}}
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 修改</button>
                    <a href="{{url('/invoice/invoice_list')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')
@endsection