@extends('admin.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" />
<link href="{{asset('sanji/css/main.css')}}" rel="stylesheet">
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<style type="text/css">
    /*body, html{width: 100%;height: 100%;margin:0;font-family:"微软雅黑";font-size:14px;}*/
    #l-map{height:250px;width:100%;}
    /*#r-result{width:100%;}*/
</style>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=RNifyeVIz4GNTjvl9XwDUth7ufYPk2pX"></script>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">商家入驻信息</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 商家手机号和法人联系电话必须一致</div>--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/wish_seller/seller_add_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mobile"  placeholder="商家手机号" value="@if(old('mobile') != ""){{old('mobile')}}@elseif(session('mobile') != '') {{session('mobile')}}@endif">
                            @if ($errors->has('mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">推荐人手机号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="invite_mobile" @if($supeerior_mobile) readonly @endif  placeholder="推荐人手机号" value="@if(old('invite_mobile') != ""){{old('invite_mobile')}}@elseif($supeerior_mobile != '') {{$supeerior_mobile}}@endif">
                            @if ($errors->has('invite_mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('invite_mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商户名称 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="merchantName"   placeholder="商户名称" value="@if(old('merchantName') != ""){{old('merchantName')}}@elseif(session('merchantName') != '') {{session('merchantName')}}@endif">
                            @if ($errors->has('merchantName'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('merchantName') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商户密码 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="password"   placeholder="商户密码(不填则默认为手机号码)" value="">
                            @if ($errors->has('merchantName'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('merchantName') }}</span>
                            @endif
                        </div>
                    </div>

                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}
                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">POS机终端号 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="terminalId"   placeholder="POS机终端号" value="@if(old('terminalId') != ""){{old('terminalId')}}@elseif(session('terminalId') != '') {{session('terminalId')}}@endif">--}}
                            {{--@if ($errors->has('terminalId'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('terminalId') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}


                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">省市区选择</label>
                        <div class="col-sm-10 form-inline">

                            <div id="distpicker5">
                                <div class="form-group">
                                    <label class="sr-only" for="province10">Province</label>
                                    <select class="form-control" id="province10" name="province10" onchange="get_province_code()"></select>
                                </div>
                                <input type="hidden" name="province_code" id="province_code" value="@if(old('province_code') != ""){{old('province_code')}}@elseif(session('province_code') != '') {{session('province_code')}}@endif">
                                <div class="form-group">
                                    <label class="sr-only" for="city10">City</label>
                                    <select class="form-control" id="city10" name="city10" onchange="get_city_code()"></select>
                                </div>
                                <input type="hidden" name="city_code" id="city_code" value="@if(old('city_code') != ""){{old('city_code')}}@elseif(session('city_code') != '') {{session('city_code')}}@endif">

                                <div class="form-group">
                                    <label class="sr-only" for="district10">District</label>
                                    <select class="form-control" id="district10" name="district10" onchange="get_district_code()"></select>
                                </div>
                                <input type="hidden" name="district_code" id="district_code" value="@if(old('district_code') != ""){{old('district_code')}}@elseif(session('district_code') != '') {{session('district_code')}}@endif">

                            </div>

                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">详细地址 </label>
                        <div class="col-sm-10">

                            <input type="text" class="form-control" id="merchantAddress" name="merchantAddress"   placeholder="详细地址" value="@if(old('merchantAddress') != ""){{old('merchantAddress')}}@elseif(session('merchantAddress') != '') {{session('merchantAddress')}}@endif">

                            @if ($errors->has('merchantAddress'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('merchantAddress') }}</span>
                            @endif
                        </div>
                    </div>

                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">座机号码 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<label class="control-label">{{$basic_info->merchantAddress}}</label>--}}
                            {{--<input type="text" class="form-control" name="corpmanPhone" id="corpmanPhone"  placeholder="商户座机号码" value="{{$basic_info->corpmanPhone or old('corpmanPhone')}}">--}}
                            {{--@if ($errors->has('corpmanPhone'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('corpmanPhone') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">人均消费 </label>
                        <div class="col-sm-10">
                            {{--<label class="control-label">{{$basic_info->merchantName}}</label>--}}
                            <input type="text" class="form-control" name="average"   placeholder="人均消费金额" value="{{$basic_info->average or old('average')}}">
                            @if ($errors->has('average'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('average') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">营业时间</label>
                        <div class="col-sm-10">
                            {{--<label class="control-label">{{$basic_info->merchantName}}</label>--}}
                            <input type="text" class="form-control" name="business_time"   placeholder="营业时间" value="8:00-22:00">
                            @if ($errors->has('business_time'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('business_time') }}</span>
                            @endif
                            <span class="help-block m-b-none ">请务必按照指定格式存储 （8:00-22:00）</span>
                        </div>
                    </div>


                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">经营类目 </label>
                        <div class="col-sm-10">
                            <select name="category" class="form-control">
                                @foreach($category_arr as $k=>$v)
                                    <option value="{{$v->categoryCode}}"
                                            @if(old('category') != "")
                                            @if(old('category')==$v->categoryCode)
                                            selected
                                            @endif
                                            @elseif(session('category') != '')
                                            @if(session('category')==$v->categoryCode)
                                            selected
                                            @endif
                                            @endif
                                    >{{$v->categoryName}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('category'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('category') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商家让利 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="divide" placeholder="商家让利(5-20之间,单位(%))" value="{{$seller_info->divide or old('divide')}}">
                            @if ($errors->has('servicePhone'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('servicePhone') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">logo图片（165x165）</label>
                        <ul id="warp">
                            <li>
                                <input type="hidden" value="" name="old_logo"/>
                                <div style="text-align: left;" class="col-sm-2">
                                    <img id="imgShow_WU_FILE_1" width="100%"  src="{{asset('/images/timg.jpg')}}" />
                                </div>
                                <div class="col-sm-4" style="text-align: right;">
                                    <input type="file" id="up_img_WU_FILE_1" name="logo" />
                                    <input  type="hidden" id="uploadfile2" name="logobase64"/>
                                </div>

                            </li>
                        </ul>
                    </div>

                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}
                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">banner图片（一）（375x255）</label>--}}
                        {{--<ul id="warp">--}}
                            {{--<li>--}}
                                {{--<input type="hidden" value="" name="old_banner1"/>--}}
                                {{--<div style="text-align: left;" class="col-sm-3">--}}
                                    {{--<img id="imgShow_WU_FILE_2" width="100%"   src="{{asset('/images/timg.jpg')}}" />--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-4" style="text-align: right;">--}}
                                    {{--<input type="file"  id="up_img_WU_FILE_2" name="banner1" />--}}
                                    {{--<input  type="hidden" id="uploadfile3"  name="banner1base64"/>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}

                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}
                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">banner图片（二）（375x255）</label>--}}
                        {{--<ul id="warp">--}}
                            {{--<li>--}}
                                {{--<input type="hidden" value="" name="old_banner2"/>--}}
                                {{--<div style="text-align: left;" class="col-sm-3">--}}
                                    {{--<img id="imgShow_WU_FILE_3" width="100%"  src="{{asset('/images/timg.jpg')}}" />--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-4" style="text-align: right;">--}}
                                    {{--<input type="file"  id="up_img_WU_FILE_3" name="banner2" />--}}
                                    {{--<input  type="hidden" src="" id="uploadfile4" name="banner2base64"/>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label  for="signin-text" class="col-sm-2 control-label">banner图片(最多三张)</label>
                        <ul id="warp">
                            <li>
                                <div style="text-align: left;margin-bottom: 10px;" class="col-sm-3">
                                    <img width="100%" id="banner_0" src="{{asset('/images/timg.jpg')}}">
                                    <input type="hidden" name="banner[0]" id="banner" >
                                </div>
                                <div style="text-align: left;margin-bottom: 10px;" class="col-sm-3">
                                    <img width="100%" id="banner_1"  src="{{asset('/images/timg.jpg')}}">
                                    <input type="hidden" name="banner[1]" id="banner" >
                                </div>
                                <div style="text-align: left;margin-bottom: 10px;" class="col-sm-3">
                                    <img width="100%" id="banner_2"  src="{{asset('/images/timg.jpg')}}">
                                    <input type="hidden" name="banner[2]" id="banner" >
                                </div>
                            </li>
                        </ul>
                        <label class="col-sm-2"></label>
                        <label class="col-sm-2">
                            {{--<button type="button" id="open_up_banner"  class="btn btn btn-info"><i class="fa  fa-file-picture-o"></i> 选择图片</button>--}}

                            <div class="col-sm-4" style="text-align: right;">
                                <input type="file" id="up_banner" name="up_banner" multiple />
                                <input  type="hidden" multiple />
                            </div>
                        </label>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">客服电话 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="servicePhone"   placeholder="客服电话" value="@if(old('servicePhone') != ""){{old('servicePhone')}}@elseif(session('servicePhone') != '') {{session('servicePhone')}}@endif">
                            @if ($errors->has('servicePhone'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('servicePhone') }}</span>
                            @endif
                        </div>
                    </div>
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}


                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">法人姓名 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="corpmanName"   placeholder="法人姓名" value="@if(old('corpmanName') != ""){{old('corpmanName')}}@elseif(session('corpmanName') != '') {{session('corpmanName')}}@endif">--}}
                            {{--@if ($errors->has('corpmanName'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('corpmanName') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">法人身份证号 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="corpmanId"   placeholder="法人身份证号" value="@if(old('corpmanId') != ""){{old('corpmanId')}}@elseif(session('corpmanId') != '') {{session('corpmanId')}}@endif">--}}
                            {{--@if ($errors->has('corpmanId'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('corpmanId') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">法人联系手机 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="corpmanMobile"   placeholder="法人联系手机" value="@if(old('corpmanMobile') != ""){{old('corpmanMobile')}}@elseif(session('corpmanMobile') != '') {{session('corpmanMobile')}}@endif">--}}
                            {{--@if ($errors->has('corpmanMobile'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('corpmanMobile') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>



                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">营业执照号 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="businessLicense"   placeholder="营业执照号" value="@if(old('businessLicense') != ""){{old('businessLicense')}}@elseif(session('businessLicense') != '') {{session('businessLicense')}}@endif">--}}
                            {{--@if ($errors->has('businessLicense'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('businessLicense') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">营业执照</label>
                        <ul id="warp">
                            <li>
                                <input type="hidden" value="" name="old_businessimage"/>
                                <div style="text-align: left;" class="col-sm-4">
                                    {{--<img id="imgShow_WU_FILE_0" width="100%"  src="@if(old('businessimagebase64') != ""){{asset('/images/timg.jpg')}}@elseif(session('businessimagebase64') != '') {{session('img')}} @else {{asset('/images/timg.jpg')}}@endif" />--}}
                                    <img id="imgShow_WU_FILE_0" width="100%"  src="{{asset('/images/timg.jpg')}}" />
                                </div>
                                <div class="col-sm-2" style="text-align: right;">
                                    <input type="file" id="up_img_WU_FILE_0" name="businessimage" />
                                    {{--<input  type="hidden" id="uploadfile" name="businessimagebase64" value="@if(old('businessimagebase64') != ""){{old('businessimagebase64')}}@elseif(session('businessimagebase64') != '') {{session('businessimagebase64')}}@endif"/>--}}
                                    <input  type="hidden" id="uploadfile" name="businessimagebase64"/>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">开户银行 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<select name="bankName" id="bankName" class="form-control" onchange="getbankNo()">--}}
                                {{--<option value="0" >请选择银行</option>--}}
                                {{--@foreach($bank_arr as $k=>$v)--}}
                                    {{--<option value="{{$v->bankName}}" tip="{{$v->bankCode}}"--}}
                                            {{--@if(old('bankName') != "")--}}
                                            {{--@if(old('bankName')==$v->bankName)--}}
                                            {{--selected--}}
                                            {{--@endif--}}
                                            {{--@elseif(session('bankName') != '')--}}
                                            {{--@if(session('bankName')==$v->bankName)--}}
                                            {{--selected--}}
                                            {{--@endif--}}
                                            {{--@endif--}}
                                    {{-->{{$v->bankName}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                            {{--<input type="hidden"  name="bankCode"value="@if(old('bankCode') != ""){{old('bankCode')}}@elseif(session('bankCode') != '') {{session('bankCode')}}@endif">--}}
                            {{--@if ($errors->has('category'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('category') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}


                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">开户行账号 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="bankaccountNo"   placeholder="开户行账号" value="@if(old('bankaccountNo') != ""){{old('bankaccountNo')}}@elseif(session('bankaccountNo') != '') {{session('bankaccountNo')}}@endif">--}}
                            {{--@if ($errors->has('bankaccountNo'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountNo') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">开户户名 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<label class="control-label">{{$basic_info->bankaccountName}}</label>--}}

                            {{--<input type="text" class="form-control" name="bankaccountName"   placeholder="开户户名" value="{{$basic_info->bankaccountName or old('bankaccountName')}}">--}}
                            {{--@if ($errors->has('bankaccountName'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountName') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}
                    {{--<div class="form-group">--}}
                    {{--<label for="signin-text" class="col-sm-2 control-label">联行号可选择的银行范围 </label>--}}
                    {{--<div class="col-sm-10">--}}
                    {{--<select id="bank_name" class="form-control" onchange="getbankNo()">--}}
                    {{--<option >--请选择银行获取联行号--</option>--}}
                    {{--@foreach($bank_list as $v)--}}
                    {{--<option >{{$v->bank_name}}</option>--}}
                    {{--@endforeach--}}
                    {{--</select>--}}
                    <input type="hidden" class="form-control" readonly name="account_bank" id="account_bank"    placeholder="银行卡所属地区银行的联行号" value="@if(old('account_bank') != ""){{old('account_bank')}}@elseif(session('account_bank') != '') {{session('account_bank')}}@endif">
                    {{--@if ($errors->has('account_bank'))--}}
                    {{--<span class="help-block m-b-none text-danger">{{ $errors->first('account_bank') }}</span>--}}
                    {{--@endif--}}
                    {{--</div>--}}

                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}



                    {{--<div class="form-group">--}}
                    {{--<label for="signin-text" class="col-sm-2 control-label">银行卡所属地区银行联行号 </label>--}}
                    {{--<div class="col-sm-10">--}}
                    <input type="hidden" class="form-control" readonly name="ibankno"  id="ibankno"  placeholder="银行卡所属地区银行的联行号" value="@if(old('ibankno') != ""){{old('ibankno')}}@elseif(session('ibankno') != '') {{session('ibankno')}}@endif">
                    {{--<span class="help-block m-b-none text-danger">请通过选定银行名字获取联行号</span>--}}
                    {{--@if ($errors->has('ibankno'))--}}
                    {{--<span class="help-block m-b-none text-danger">{{ $errors->first('ibankno') }}</span>--}}
                    {{--@endif--}}
                    {{--</div>--}}
                    {{--</div>--}}


                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}
                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">银行卡类型 </label>--}}
                        {{--<div class="col-sm-10" style="padding-top: 8px">--}}
                            {{--<input type="radio" class="control-label" name="bankaccountType" value="1"--}}
                            {{--@if(old('bankaccountType') != "")--}}
                            {{--@if(old('bankaccountType')==1)--}}
                            {{--checked--}}
                            {{--@endif--}}
                            {{--@elseif(session('bankaccountType') != '')--}}
                            {{--@if(session('bankaccountType')==1)--}}
                            {{--checked--}}
                            {{--@endif--}}
                            {{--@endif--}}
                            {{--/> 借记卡--}}
                            {{--<input type="radio" class="control-label" name="bankaccountType" value="2"--}}
                            {{--@if(old('bankaccountType') != "")--}}
                            {{--@if(old('bankaccountType')==2)--}}
                            {{--checked--}}
                            {{--@endif--}}
                            {{--@elseif(session('bankaccountType') != '')--}}
                            {{--@if(session('bankaccountType')==2)--}}
                            {{--checked--}}
                            {{--@endif--}}
                            {{--@endif--}}
                            {{--/> 贷记卡--}}
                            {{--<input type="radio" class="control-label" name="bankaccountType" value="3" /> 存折--}}
                            {{--<input class="form-control" readonly value="借记卡">--}}
                            {{--<input type="hidden" class="form-control" name="bankaccountType"   placeholder="银行卡类型" value="@if(old('bankaccountType') != ""){{old('bankaccountType')}}@elseif(session('bankaccountType') != '') {{session('bankaccountType')}}@else 1 @endif">--}}
                            {{--@if ($errors->has('bankaccountType'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountType') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}


                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}
                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">费率 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="futureRate"   placeholder="费率" value="@if(old('futureRate') != ""){{old('futureRate')}}@elseif(session('futureRate') != '') {{session('futureRate')}}@endif">--}}
                            {{--<span class="help-block m-b-none text-warning">费率即用户消费后分给平台的让利比例,如：20表示给平台的让利为20%,设置的费率不能低于3</span>--}}
                            {{--@if ($errors->has('futureRate'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('futureRate') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}


                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">商家店铺定位 </label>
                        <div class="col-sm-10">
                            <div id="l-map"></div>
                            <div id="r-result"><input class="form-control" type="text" id="suggestId" size="20" value="百度" placeholder="请输入店铺地址的关键字" style="width:100%;margin-top: 20px" /></div>
                            <div id="searchResultPanel" style="border:1px solid #C0C0C0;width:150px;height:auto; display:none;"></div>
                            <input  type="hidden" id="lat" name="lat" value="@if(old('lat') != ""){{old('lat')}}@elseif(session('lat') != '') {{session('lat')}}@endif"/>
                            <input  type="hidden" id="lng" name="lng" value="@if(old('lng') != ""){{old('lng')}}@elseif(session('lng') != '') {{session('lng')}}@endif"/>

                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                    <a href="{{url('/wish_seller/seller_list')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')
    <script src="{{asset('sanji/js/distpicker.data.js')}}"></script>
    <script src="{{asset('sanji/js/distpicker.js')}}"></script>
    <script src="{{asset('sanji/js/main.js')}}"></script>
    <script>

        $("#up_banner").change(function(){

            var iLen = this.files.length;
            var index = 0
            var reader = null;
            for(var i=0;i<iLen;i++){
                if(i < 3){
                    reader = new FileReader()
                    reader.readAsDataURL(this.files[i]);
                    reader.onload = function(e){
                        $.ajax({
                            type: "POST",
                            url: "{{url('/upload_img')}}",
                            dataType: "json",
                            async: false,
                            data:{imagebase64:e.target.result},
                            success: function(data){
                                // benner
                                if(data.code==1){
                                    rendering(data.data.filepath,index)
                                    $('#banner_'+index).attr('src',e.target.result);
                                }else{
                                    alert(data.message);
                                }
                            }
                        });
                        // rendering('xxx',index)
                        index++
                    }
                }
            }
        });
        //渲染
        function rendering(item,index) {
            $("input[name='banner["+index+"]']").val(item)
        }

    </script>
    <script>
        $(document).ready(function(){
            $('#bankName').change(function(){
                $('input[name=bankCode]').val($('#bankName option:selected').attr('tip'));

            });
            $('#bankName1').change(function(){
                $('input[name=bankCode1]').val($('#bankName1 option:selected').attr('tip'));

            });
            $("#up_img_WU_FILE_0").change(function(){
                var v = $(this).val();
                var reader = new FileReader();
                reader.readAsDataURL(this.files[0]);
                reader.onload = function(e){
                    // console.log(e.target.result);
                    // $('#uploadfile').val(e.target.result);
                    $('#imgShow_WU_FILE_0').attr('src',e.target.result);
                    $.post('{{url('/upload_img')}}',{imagebase64:e.target.result},function(data){
                        if(data.code==1){
                            $('#uploadfile').val(data.data.filepath);
                        }else{
                            alert(data.message);
                        }
                    },'json')
                };
            });
            //LOGO的上传
            $("#up_img_WU_FILE_1").change(function(){
                var v = $(this).val();
                var reader = new FileReader();
                reader.readAsDataURL(this.files[0]);
                reader.onload = function(e){
                    // console.log(e.target.result);
                    // $('#uploadfile2').val(e.target.result);
                    $('#imgShow_WU_FILE_1').attr('src',e.target.result);
                    $.post('{{url('/upload_img')}}',{imagebase64:e.target.result},function(data){
                        if(data.code==1){
                            $('#uploadfile2').val(data.data.filepath);
                        }else{
                            alert(data.message);
                        }
                    },'json')
                };
            });

            //banner图一
            $("#up_img_WU_FILE_2").change(function(){
                var v = $(this).val();
                var reader = new FileReader();
                reader.readAsDataURL(this.files[0]);
                reader.onload = function(e){
                    // console.log(e.target.result);
                    // $('#uploadfile3').val(e.target.result);
                    $('#imgShow_WU_FILE_2').attr('src',e.target.result);
                    $.post('{{url('/upload_img')}}',{imagebase64:e.target.result},function(data){
                        if(data.code==1){
                            $('#uploadfile3').val(data.data.filepath);
                        }else{
                            alert(data.message);
                        }
                    },'json')
                };
            });
            //banner图二
            $("#up_img_WU_FILE_3").change(function(){
                var v = $(this).val();
                var reader = new FileReader();
                reader.readAsDataURL(this.files[0]);
                reader.onload = function(e){
                    // console.log(e.target.result);
                    // $('#uploadfile4').val(e.target.result);
                    $('#imgShow_WU_FILE_3').attr('src',e.target.result);
                    $.post('{{url('/upload_img')}}',{imagebase64:e.target.result},function(data){
                        if(data.code==1){
                            $('#uploadfile4').val(data.data.filepath);
                        }else{
                            alert(data.message);
                        }
                    },'json')
                };
            });

        });
        function getbankNo() {
            var bank_name=$("#bankName").val();
            $.ajax({
                url:'{{url('/seller/get_bankNo')}}',
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    bank_name:bank_name
                },
                timeout:5000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data){

                    if (data.code==0){
//                    alert(data.message)
                        $('#ibankno').val('0');
                        $('#account_bank').val("");
                    }else {
                        $('#ibankno').val(data.data.lbnkNo);
                        $('#account_bank').val(data.data.bank_name);
                    }
                },
                error:function(xhr,textStatus){
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },

            })
        }
        function get_city(){
            cityName = $("#cityName1").val();
            val=$(".city_option[tip^="+cityName+"]").val();
            $('#aa').val(val);
        }
        function get_province_code() {
            $('#province_code').val($("#province10").find("option:selected").attr("data-code"));
            var j = 0;
            var k = 0;
            $.each(ChineseDistricts[$("#province10").find("option:selected").attr("data-code")], function(i, val) {
                j++;
                if (j == 1){
                    $('#city_code').val(i);

                    $.each(ChineseDistricts[i], function(i1, val1) {
                        k++;
//                    alert(val1);
                        if (k == 1){
                            $('#district_code').val(i1);
                        }
                    });
                }
            });
//        $('#city_code').val("");
//        $('#district_code').val("");
        }
        function get_city_code() {
            $('#city_code').val($("#city10").find("option:selected").attr("data-code"));
            if (typeof(ChineseDistricts[$("#city10").find("option:selected").attr("data-code")]) == 'undefined') {
                $('#district_code').val("");
            }

            var k = 0;
            $.each(ChineseDistricts[$("#city10").find("option:selected").attr("data-code")], function(i, val) {
                k++;
                if (k == 1){
                    $('#district_code').val(i);
                }
            });
//        $('#district_code').val("");
            get_coordinate();
        }
        function get_district_code() {
            $('#district_code').val($("#district10").find("option:selected").attr("data-code"));
            get_coordinate();
        }


        if ("{{empty(old('province10'))?false:true}}"){
            var province_name = "{{old('province10')}}";
            var city_name = "{{empty(old('city10'))?null:old('city10')}}";
            var district_name = "{{empty(old('district10'))?null:old('district10')}}";
//        alert(province_name)
//        alert(city_name)
//        alert(district_name)
            $("#distpicker5").distpicker({
                province: province_name,
                city: city_name,
                district: district_name
            });

        }else{
            if ("{{session('province10')}}" != ""){
                var province_name = "{{session('province10')}}";
                var city_name = "{{session(old('city10'))==""?null:session('city10')}}";
                var district_name = "{{session(old('district10'))==""?null:session('district10')}}";
//        alert(province_name)
//        alert(city_name)
//        alert(district_name)
                $("#distpicker5").distpicker({
                    province: province_name,
                    city: city_name,
                    district: district_name
                });
            }

        }


    </script>
    <script type="text/javascript">
        // 百度地图API功能
        function G(id) {
            return document.getElementById(id);
        }

        var map = new BMap.Map("l-map");
        @if(session('lng') == '' && session('lat') == '')
        map.centerAndZoom("深圳",12);                   // 初始化地图,设置城市和地图级别。
        @elseif(session('lng') != '' && session('lat') != '')
        map.centerAndZoom(new BMap.Point("{{session('lng')}}", "{{session('lat')}}"),12);                   // 初始化地图,设置城市和地图级别。
                @endif

        var ac = new BMap.Autocomplete(    //建立一个自动完成的对象
            {"input" : "suggestId"
                ,"location" : map
            });

        ac.addEventListener("onhighlight", function(e) {  //鼠标放在下拉列表上的事件
            var str = "";
            var _value = e.fromitem.value;
            var value = "";
            if (e.fromitem.index > -1) {
                value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
            }
            str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;

            value = "";
            if (e.toitem.index > -1) {
                _value = e.toitem.value;
                value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
            }
            str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
            G("searchResultPanel").innerHTML = str;
        });

        var myValue;
        ac.addEventListener("onconfirm", function(e) {    //鼠标点击下拉列表后的事件
            var _value = e.item.value;
            myValue = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
            G("searchResultPanel").innerHTML ="onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue;

            setPlace();
        });

        function setPlace(){
            map.clearOverlays();    //清除地图上所有覆盖物
            function myFun(){
                var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
                map.centerAndZoom(pp, 18);
                $("#lat").val(pp.lat);
                $("#lng").val(pp.lng);
                map.addOverlay(new BMap.Marker(pp));    //添加标注
            }
            var local = new BMap.LocalSearch(map, { //智能搜索
                onSearchComplete: myFun
            });
            local.search(myValue);
        }
        map.enableScrollWheelZoom();   //启用滚轮放大缩小，默认禁用
        map.enableContinuousZoom();    //启用地图惯性拖拽，默认禁用、
        //    单击获取点击的经纬度
        map.addEventListener("click",function(e){
//        alert(e.point.lng + "," + e.point.lat);
            map.clearOverlays();
            var marker = new BMap.Marker(new BMap.Point(e.point.lng, e.point.lat)); // 创建点
            $("#lat").val(e.point.lat);
            $("#lng").val(e.point.lng);
            map.addOverlay(marker);

        });

        $("#merchantAddress").blur(function(){
            get_coordinate()
        });
        $("#province10").blur(function(){
            get_coordinate()
        });

        function get_coordinate() {
            var address = $("#province10").find("option:selected").val()+$("#city10").find("option:selected").val()+$("#district10").find("option:selected").val()+$("#merchantAddress").val();
            var city = $("#province10").find("option:selected").val()+$("#city10").find("option:selected").val();
            // 创建地址解析器实例
            var myGeo = new BMap.Geocoder();
            // 将地址解析结果显示在地图上，并调整地图视野
            myGeo.getPoint(address, function(point){
                    map.clearOverlays();
                    if (point) {
                        map.centerAndZoom(point, 16);
                        $("#lat").val(point.lat);
                        $("#lng").val(point.lng);
                        map.addOverlay(new BMap.Marker(point));
                    }
                },
                city);
        }
        if ("{{empty(old('lat'))?false:true}}" && "{{empty(old('lng'))?false:true}}"){
            theLocation("{{old('lng')}}","{{old('lat')}}")
        }else {
            if ("{{session('lat')}}" != "" && "{{session('lng')}}" != ""){
                theLocation("{{session('lng')}}","{{session('lat')}}")
            }
        }
        // 用经纬度设置地图中心点
        function theLocation(lng,lat){
            map.clearOverlays();
            var new_point = new BMap.Point(lng,lat);
            var marker = new BMap.Marker(new_point);  // 创建标注
            map.addOverlay(marker);              // 将标注添加到地图中
            map.panTo(new_point);

        }


    </script>
    <script type="text/javascript">
        // 百度地图API功能
        function G(id) {
            return document.getElementById(id);
        }

        var map = new BMap.Map("l-map");
        map.centerAndZoom("深圳",12);                   // 初始化地图,设置城市和地图级别。

        var ac = new BMap.Autocomplete(    //建立一个自动完成的对象
            {"input" : "suggestId"
                ,"location" : map
            });

        ac.addEventListener("onhighlight", function(e) {  //鼠标放在下拉列表上的事件
            var str = "";
            var _value = e.fromitem.value;
            var value = "";
            if (e.fromitem.index > -1) {
                value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
            }
            str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;

            value = "";
            if (e.toitem.index > -1) {
                _value = e.toitem.value;
                value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
            }
            str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
            G("searchResultPanel").innerHTML = str;
        });

    </script>
    <script type="text/javascript">

        @if(Session::has('status'))
        setTimeout( " delData() " , 3000 );
        @endif

        function delData() {
            $.ajax({
                url: '{{url('/seller/delData')}}',
                type: 'POST', //GET
                async: true,    //或false,是否异步
                data: {},
                timeout: 5000,    //超时时间
                dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success: function (data) {
                    {{--alert({{session('mobile')}})--}}
                            {{--if ("{{old('mobile')}}" == ""){--}}
                            {{--$("[name='mobile']").val("{{session('mobile')}}");--}}
                            {{--}--}}
                        window.onload = function(){
                        setTimeout("location.reload()",1000);
                    }

                },
                error: function (xhr, textStatus) {
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },
            })
        }

    </script>
@endsection