@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">财务管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding" style="margin-bottom: 20px;">
                {{--<div class="col-sm-12">--}}
                {{--<div class="alert alert-warning"> 由于年假期间，13日-23日微信、支付宝货款将在23日之后统一结算，具体请查看未特商城公众号公告，银行卡货款年假期间即时结算（秒到））</div>--}}
                {{--</div>--}}
                <div class="col-sm-12">
                    <span class="form-control">用户名 : {{$sellerInfo['mobile']}} </span>
                </div>
                <div class="col-sm-12">
                    <span class="form-control">钱包余额 : {{$money}} </span>
                </div>
                {{--<div class="col-sm-12">--}}
                    {{--<span class="form-control">未特推广券可转换数量 : {{$voucher}} (未特推广券进行激活) </span>--}}
                {{--</div>--}}
            </div>
        </div>
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">消费订单管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <form action="" method="get" role="form">
                    <div class="col-sm-2">
                        <select class="form-control" id="type" name="type">
                            <option value="">--订单类型--</option>
                            <option value="3" @if($_GET['type']==3) selected @endif>商家引流奖励分红</option>
                            <option value="4" @if($_GET['type']==4) selected @endif>购买抵扣券分红</option>
                        </select>
                    </div>

                    {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="pay_status">--}}
                    {{--<option value="0" >--订单状态--</option>--}}
                    {{--@foreach(config('pay.pay_status') as $k=>$v)--}}
                    {{--<option value="{{$k}}" @if($_GET['pay_status']==$k) selected @endif>{{$v}}</option>--}}
                    {{--@endforeach--}}
                    {{--</select>--}}
                    {{--</div>--}}
                    <div class="col-sm-4">
                        <a class='input-group date' id='start_time' style="float:left">
                            <input type='text' class="form-control" id='start_time_value' name="start_time" style="width: 150px; height: 30px;" value="{{$_GET['start_time']}}"/>
                            <span class="input-group-addon" style="float: left; width: 50px; height: 30px;">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        </a>
                        <span style="float:left" >  ~  </span>
                        <a class='input-group date' id='end_time' style="float:left">
                            <input type='text' class="form-control" id='end_time_value' name="end_time" style="width: 150px; height: 30px;" value="{{$_GET['end_time']}}"/>
                            <span class="input-group-addon" style="float: left; width: 50px; height: 30px;">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <input type="hidden" name="uid" value="{{$sellerInfo['id']}}" >
                        <button type="submit" id="search" class="input-group btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</button>
                    </div>
                </form>
                {{--@if(config('app.env')!='online')--}}
                {{--<div class="col-sm-12" style="margin-top: 10px;">--}}
                {{--<a type="submit" href="{{url('')}}" class="btn btn btn-info" style="float:left;"><i class="fa fa-plus"></i> 生成购买pos机订单</a>--}}
                {{--<a type="submit" href="{{url('')}}" class="btn btn btn-info" style="float:left; margin-left: 20px;"><i class="fa fa-plus"></i> 生成消费买单</a>--}}
                {{--</div>--}}
                {{--@endif--}}
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>流水ID</th>
                        <th>类型</th>
                        <th>数额</th>
                        <th>备注</th>
                        <th>创建时间</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($order_list as $k=>$v)
                            <tr>
                                <td>{{$v->id}}</td>
                                <td>@if($v->type==3)商家引流奖励分红@elseif($v->type==4)购买抵扣券分红@else未知@endif</td>
                                <td>{{$v->amount or '暂无'}}</td>
                                <td>{{$v->remarks or '暂无'}}</td>
                                <td>{{$v->create_time or '暂无'}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $order_list->appends(request()->input())->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $order_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        Datetime('#start_time');
        Datetime('#end_time');
    </script>
@endsection