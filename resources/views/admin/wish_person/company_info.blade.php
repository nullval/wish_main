@extends('admin.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" />
<link href="{{asset('sanji/css/main.css')}}" rel="stylesheet">
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<style type="text/css">

</style>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">我的资料</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/wish_person/company_info_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">登录手机号 </label>
                        <div class="col-sm-10">
                            {{--<label class="control-label">{{$company_info->mobile}}</label>--}}
                            <input type="text" class="form-control" name="mobile" readonly  placeholder="登录手机号" value="{{$company_info->mobile or old('mobile')}}">
                            <input type="hidden" name="uid" value="{{$company_info->id}}" >
                            @if ($errors->has('mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">分公司名称 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="merchantName"  placeholder="请输入分公司名称" value="{{$basic_info->merchantName or old('merchantName')}}" >
                            @if ($errors->has('merchantName'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('merchantName') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">省市区选择</label>
                        <div class="col-sm-10 form-inline">

                            <div id="distpicker5">
                                <div class="form-group">
                                    <label class="sr-only" for="province10">Province</label>
                                    <select class="form-control" id="province10" name="province10" onchange="get_province_code()"></select>
                                </div>
                                <input type="hidden" name="province_code" id="province_code" value="@if(old('province_code') != ""){{old('province_code')}}@elseif(session('province_code') != '') {{session('province_code')}}@endif">
                                <div class="form-group">
                                    <label class="sr-only" for="city10">City</label>
                                    <select class="form-control" id="city10" name="city10" onchange="get_city_code()"></select>
                                </div>
                                <input type="hidden" name="city_code" id="city_code" value="@if(old('city_code') != ""){{old('city_code')}}@elseif(session('city_code') != '') {{session('city_code')}}@endif">

                                <div class="form-group">
                                    <label class="sr-only" for="district10">District</label>
                                    <select class="form-control" id="district10" name="district10" onchange="get_district_code()"></select>
                                </div>
                                <input type="hidden" name="district_code" id="district_code" value="@if(old('district_code') != ""){{old('district_code')}}@elseif(session('district_code') != '') {{session('district_code')}}@endif">

                            </div>

                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                    {{--<a href="{{url('/wish_seller/seller_list')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>--}}

                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')
    <script src="{{asset('sanji/js/distpicker.data.js')}}"></script>
    <script src="{{asset('sanji/js/distpicker.js')}}"></script>
    <script src="{{asset('sanji/js/main.js')}}"></script>
    <script>
        var province_name = null
        var city_name = null
        var district_name = null

        var province_code = {{substr($basic_info->area_id,0,2)}}+'0000'
        var city_code = {{substr($basic_info->area_id,0,4)}}+'00'
        var district_code = {{$basic_info->area_id}}
        $(document).ready(function(){

            $('#bankName').change(function(){
                $('input[name=bankCode]').val($('#bankName option:selected').attr('tip'));

            });
            $('#bankName1').change(function(){
                $('input[name=bankCode1]').val($('#bankName1 option:selected').attr('tip'));

            });
            $("#up_img_WU_FILE_0").change(function(){
                var v = $(this).val();
                var reader = new FileReader();
                reader.readAsDataURL(this.files[0]);
                reader.onload = function(e){
                    console.log(e.target.result);
                    $('#uploadfile').val(e.target.result);
                };
            });
        });
        function getbankNo() {
            var bank_name=$("#bank_name").val();
            $.ajax({
                url:'{{url('/generalize/get_bankNo')}}',
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    bank_name:bank_name
                },
                timeout:5000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data){

                    if (data.code==0){
                        alert(data.message)
                    }else {
                        $('#ibankno').val(data.data.lbnkNo);
                        $('#account_bank').val(data.data.bank_name);
                    }
                },
                error:function(xhr,textStatus){
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },

            })

        }


        function get_province_code() {
            $('#province_code').val($("#province10").find("option:selected").attr("data-code"));
            var j = 0;
            var k = 0;
            $.each(ChineseDistricts[$("#province10").find("option:selected").attr("data-code")], function(i, val) {
                j++;
                if (j == 1){
                    $('#city_code').val(i);

                    $.each(ChineseDistricts[i], function(i1, val1) {
                        k++;
//                    alert(val1);
                        if (k == 1){
                            $('#district_code').val(i1);
                        }
                    });
                }
                if ($("#province10").find("option:selected").attr("data-code") == province_code) {
//                alert(district_name)
                    $('#city_code').val(city_code);
                    $('#district_code').val(district_code);
                }
            });

//        $('#city_code').val("");
//        $('#district_code').val("");
        }
        function get_city_code() {
            $('#city_code').val($("#city10").find("option:selected").attr("data-code"));
            if (typeof(ChineseDistricts[$("#city10").find("option:selected").attr("data-code")]) == 'undefined') {
                $('#district_code').val("");
            }

            var k = 0;
            $.each(ChineseDistricts[$("#city10").find("option:selected").attr("data-code")], function(i, val) {
                k++;
                if (k == 1){
                    $('#district_code').val(i);
                }
            });
//        alert(ChineseDistricts[$('#province_code').val()][1])
//        $('#district_code').val("");
            get_coordinate();
        }
        function get_district_code() {
            $('#district_code').val($("#district10").find("option:selected").attr("data-code"));
            get_coordinate();
        }

        $.each(ChineseDistricts[86], function(i, val) {
            if(i==province_code){
                province_name = val;
//            alert(province_name)

            }
        });
        $.each(ChineseDistricts[province_code], function(i, val) {
            if (i == city_code){
                city_name = val;
//            alert(city_name)
            }
        });
        if (typeof(ChineseDistricts[city_code]) != 'undefined'){
            $.each(ChineseDistricts[city_code], function(i, val) {
                if (i == district_code){
                    district_name = val;
//                alert(district_name)

                }
            });
        }

        $("#distpicker5").distpicker({
            province: province_name,
            city: city_name,
            district: district_name
        });
        $(document).ready(function(){
            $('#province_code').val($("#province10").find("option:selected").attr("data-code"));
            $('#city_code').val($("#city10").find("option:selected").attr("data-code"));
            $('#district_code').val($("#district10").find("option:selected").attr("data-code"));

        });

    </script>
@endsection