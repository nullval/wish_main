@extends('admin.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" />
<link href="{{asset('sanji/css/main.css')}}" rel="stylesheet">
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<style type="text/css">

</style>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">修改密码</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/wish_person/submit_uppwd')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">原密码 </label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="old_pwd"  placeholder="请输入原密码" >
                            @if ($errors->has('old_pwd'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('old_pwd') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">新密码 </label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="new_pwd"  placeholder="请输入新密码" >
                            @if ($errors->has('new_pwd'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('new_pwd') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">确认新密码 </label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="re_pwd"  placeholder="在此输入新密码" >
                            @if ($errors->has('re_pwd'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('re_pwd') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                    {{--<a href="{{url('/wish_seller/seller_list')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>--}}

                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')

@endsection