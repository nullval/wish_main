@extends('admin.layouts.app')
@section('content')
    <div class="panel panel-headline">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">系统提现</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 消费分润比例即用户消费后分给商家的比例,20表示20%</div>--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/config/issue_withdraw_submit')}}" class="form-horizontal">
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$_GET['uid']}}" name="uid">

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">订单编号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="spare_order_sn"   placeholder="订单编号" value="{{old('spare_order_sn')}}">
                            @if ($errors->has('spare_order_sn'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('spare_order_sn') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">	短信安全验证 </label>
                        <div class="col-sm-10">
                            <input type="text"  class="form-control" placeholder="输入短信验证码" style="width: 200px;float:left;" name="code">
                            <a  id="get_code" class="btn btn btn-info" style="float:left;"><i class="fa fa-refresh"></i> 发送短信</a>
                            @if ($errors->has('code'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('code') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                    <a href="{{url('/config/set_upgrade')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
    <script src="{{asset('image_frame/js/jqueryLibrary.min.js')}}"></script>
    <script src="{{asset('image_frame/js/jquery.imgbox.pack.js')}}"></script>
    <script>
        $('#get_code').click(function(){
            $.ajax({
                type: "POST",
                url: '{{url('/config/send_message')}}',
                data: {send_type:2},
                dataType: "json",
                success: function(data){
                    if(data.cod=1){
                        alert(data.message);
                    }else{
                        alert(data.message);
                    }
                }
            });
        });

    </script>
@endsection