@extends('admin.layouts.app')
<style>
    .title{
        color:black;
        font-size:16px;
    }
</style>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">提现参数设置</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/config/set_withdraw_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-12 control-label title">提现参数设置</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    @foreach($user_pay_info as $k => $v)
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label">{{$v -> remark}}</label>
                            <div class="col-sm-2">
                                <select class="form-control" name="{{$v -> attr_name}}">
                                    @foreach($pay_type as $key => $value)
                                        <option value="{{$key}}" @if($v -> attr_value == $key) selected @endif>{{$value['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                    @endforeach

                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 修改</button>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection