@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">角色管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                <div class="col-sm-12">
                    <a href="{{url('/config/role_add')}}" class="btn btn-info" style="float: right;"><i class="fa fa-plus"></i> 添加角色</a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>角色编号</th>
                        <th>角色名称</th>
                        <th>身份类型</th>
                        <th>引流分红比例</th>
                        <th>抵扣券分红比例</th>
                        <th>编辑</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($role_list as $k=>$v)
                            <tr>
                                <td>{{$v['id']}}</td>
                                <td>{{$v['role_name']}}</td>
                                <td>{{$v['identity_type']}}</td>
                                <td>{{$v['admin_dividend']['offline_dividend']}}%</td>
                                <td>{{$v['admin_dividend']['voucher_dividend']}}%</td>
                                <td>
                                    <a href="{{url('/config/role_add')}}?id={{$v['id']}}"><span class="label label-info"><i class="fa fa-edit"></i> 编辑</span>
                                    </a>
                                </td>
                          </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $role_list->links() }}
                    {{--<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>--}}
                    {{--<div class="col-md-6 text-right"><a href="#" class="btn btn-primary">View All Purchases</a></div>--}}
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            $('#status').change(function(){
                location.href="{{url('/finance/withdraw')}}?status="+$(this).val();
            });
            var _token=$('input[name=_token]').val();
            //启用/禁用用户
            $('.status').click(function(){
                var $this=$(this);
                var status=$(this).attr('tip');
                var id=$(this).attr('tip1');
                $.ajax({
                    type: "POST",
                    url: '/config/change_admin_status',
                    data: {_token:_token, status:status,id:id},
                    dataType: "json",
                    success: function(data){
                        if(data.code==1){
                            var span=$this.children('span');
                            if(status==0){
                                $this.attr('tip',1);
                                span.html('启用');
                                span.removeClass('label-danger');
                                span.addClass('label-success');
                            }else{
                                $this.attr('tip',0);
                                span.html('禁用');
                                span.removeClass('label-success');
                                span.addClass('label-danger');
                            }
                        }
                    }
                });
            });
        });
    </script>
@endsection