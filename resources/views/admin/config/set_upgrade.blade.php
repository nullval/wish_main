@extends('admin.layouts.app')
<style>
    .title{
        color:black;
        font-size:16px;
    }
</style>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">系统参数设置</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/config/set_upgrade_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-12 control-label title">提现参数设置</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>



                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">是否允许平台提现</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="is_allow_withdraw" value="1" @if($data['is_allow_withdraw']==1) checked @endif/> 开启
                            <input type="radio" class="control-label" name="is_allow_withdraw" value="2" @if($data['is_allow_withdraw']==2) checked @endif/> 关闭
                            <span class="help-block m-b-none text-success">关闭则所有人不能提现</span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">提现手续费比例</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="poundage_radio"   placeholder="提现手续费比例" value="{{old('poundage_radio',$data['poundage_radio'])}}">
                            @if ($errors->has('poundage_radio'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('poundage_radio') }}</span>
                            @else
                                <span class="help-block m-b-none text-success">输入5表示每笔提现扣5%的手续费</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">每次提现限制额度</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="withdraw_limit_value"   placeholder="每次提现限制额度" value="{{old('withdraw_limit_value',$data['withdraw_limit_value'])}}">
                            @if ($errors->has('withdraw_limit_value'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('withdraw_limit_value') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">每日提现限制次数</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="withdraw_limit_num"   placeholder="提现手续费" value="{{old('withdraw_limit_num',$data['withdraw_limit_num'])}}">
                            @if ($errors->has('withdraw_limit_num'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('withdraw_limit_num') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">是否允许商家返还积分或货款</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="is_allow_back_point" value="1" @if($data['is_allow_back_point']==1) checked @endif/> 开启
                            <input type="radio" class="control-label" name="is_allow_back_point" value="2" @if($data['is_allow_back_point']==2) checked @endif/> 关闭
                            <span class="help-block m-b-none text-success">禁止则停止商家每日返现</span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">android版本名称</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="version_name"   placeholder="android版本名称" value="{{old('version_name',$data['version_name'])}}">
                            @if ($errors->has('version_name'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('version_name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">android版本号</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="version_code"   placeholder="android版本号" value="{{old('version_code',$data['version_code'])}}">
                            @if ($errors->has('version_code'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('version_code') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">是否强制更新</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="download_type" value="1" @if($data['download_type']==1) checked @endif/> 是
                            <input type="radio" class="control-label" name="download_type" value="2" @if($data['download_type']==2) checked @endif/> 否
                            <span class="help-block m-b-none text-success"></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">android更新地址</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="download_url"   placeholder="android下载更新地址" value="{{old('download_url',$data['download_url'])}}">
                            @if ($errors->has('download_url'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('download_url') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">android更新日志</label>
                        <div class="col-sm-10">
                            <textarea   class="form-control" name="update_desc"   placeholder="android更新日志" >{{old('update_desc',$data['update_desc'])}}
                            </textarea>
                            @if ($errors->has('update_desc'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('update_desc') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 修改</button>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    @if(env('APP_ENV')=='online')
                        <div class="form-group">
                            <label for="signin-text" class="col-sm-12 control-label title">设置按钮</label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-12">
    {{--                            @if(env('APP_ENV')=='online')--}}
                                    <a class="btn btn btn-info" href="{{url('/config/issue_withdraw')}}" style="float:right;margin-left: 10px;"><i class="fa fa-edit"></i> 系统处理提现</a>
                                {{--@endif--}}
                                {{--@if(env('APP_ENV')!='online')--}}
                                    <a class="btn btn btn-info" href="{{url('/config/recharge')}}" style="float:right;margin-left: 10px;"><i class="fa fa-edit"></i> 拨款</a>
                                {{--@endif--}}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                    @endif
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection