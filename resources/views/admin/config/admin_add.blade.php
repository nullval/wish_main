@extends('admin.layouts.app')

@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">添加管理员</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/config/admin_add_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$info->id or 0}}" name="id">
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">账户名</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="username"   placeholder="账户名" value="{{old('username',$info->username)}}">
                            @if ($errors->has('username'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('username') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">密码</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="password"   placeholder="@if(isset($_GET['id'])) 不填则不修改 @else 密码 @endif" value="{{old('password')}}">
                            @if ($errors->has('username'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">角色</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="role_id">
                                {{--@foreach($role_list as $k=>$v)--}}
                                    <option value="1">总公司</option>
                                {{--@endforeach--}}
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 提交</button>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection