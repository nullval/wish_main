@extends('admin.layouts.app')

@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">添加角色</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{$errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/config/role_add_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$info->id or 0}}" name="id">
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">角色名称</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="role_name"   placeholder="角色名称" value="{{old('role_name',$info->role_name)}}">
                            @if ($errors->has('role_name'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('role_name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <?php
                        if((!empty($info->permission))&&isset($info->permission)){
                            $permission_arr=explode(',',$info->permission);
                        }else{
                            $permission_arr=[];
                        }
                    ?>
                    @foreach($permission_list as $k=>$v)
                    <div class="form-group">
                            <label for="signin-text" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                                {{$v['flg_name']}}
                            </div>
                            <div class="col-sm-2">
                                <input type="checkbox" name="permission[]" value="{{$v['id']}}" @if(in_array($v['id'],$permission_arr)) checked @endif>
                            </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    @endforeach


                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 提交</button>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection