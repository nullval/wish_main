@extends('admin.layouts.app')
<style>
    .title{
        color:black;
        font-size:16px;
    }
</style>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">生成订单</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/config/create_order_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">订单类型</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="order_type" value="2" checked /> 购买pos机
                            <input type="radio" class="control-label" name="order_type" value="1" /> 消费买单
                            <input type="radio" class="control-label" name="order_type" value="7" /> 商家充值
                            <span class="help-block m-b-none text-success"></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">pos机终端号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="terminalId"   placeholder="pos机终端号" value="{{old('terminalId')}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">购买人手机号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mobile"   placeholder="购买人手机号" value="{{old('mobile')}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">推荐人手机号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="invite_mobile"   placeholder="邀请人手机号" value="{{old('invite_mobile')}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">支付金额(单位:元)</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="amount"   placeholder="支付金额" value="{{old('amount')}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">支付方式</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="pay_type" value="wechat" checked /> 微信
                            <input type="radio" class="control-label" name="pay_type" value="alipay" /> 支付宝
                            <span class="help-block m-b-none text-success"></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">支付类型</label>
                        <div class="col-sm-2">
                            <select class="form-control" name="notify_type">
                                @foreach(config('pay.notify_type') as $k=>$v)
                                    <option value="{{$k}}" @if($_GET['notify_type']==$k) selected @endif>{{$v['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">支付类型</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="pay_method" value="H5" checked /> H5支付
                            <input type="radio" class="control-label" name="pay_method" value="jsapi" /> 微信公众号支付/支付宝服务窗口
                            <input type="radio" class="control-label" name="pay_method" value="stage" /> 反扫
                            <input type="radio" class="control-label" name="pay_method" value="bank" /> 银行卡支付
                            <input type="radio" class="control-label" name="pay_method" value="app" /> app支付
                            <span class="help-block m-b-none text-success"></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 提交</button>
                </div>
            </form>
        </div>
    </div>



    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection