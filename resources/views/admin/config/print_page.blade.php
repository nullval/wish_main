@extends('admin.layouts.app')
<style>
    .title{
        color:black;
        font-size:16px;
    }
</style>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">打印小票</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/config/print_page_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">订单编号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="order_sn"   placeholder="订单编号" value="{{old('order_sn')}}">
                            @if ($errors->has('order_sn'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('order_sn') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">通知终端号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="terminalId"   placeholder="通知终端号" value="1xdl100000027">
                            @if ($errors->has('terminalId'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('terminalId') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 提交</button>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection