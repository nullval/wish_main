@extends('admin.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" xmlns="http://www.w3.org/1999/html"
      xmlns="http://www.w3.org/1999/html"/>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">提现所需信息</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/config/subwithdraw')}}" class="form-horizontal"enctype='multipart/form-data'>
                <div class="panel-body">
                    {{csrf_field()}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">代付用户名 </label>--}}
                        {{--<div class="col-sm-10" >--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label"style="text-align: left">{{$info->mobile}}</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}


                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">账户可用余额 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label" style="text-align: left">{{get_last_two_num($purse->balance-$purse->freeze_value-1000000000.0000)}}</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">提现金额</br>( 以元为单位) </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="amount"   placeholder="提现金额" value="{{$info->amount or old('amount')}}">
                            @if ($errors->has('amount'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('amount') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">	开户银行 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="bank_name" id="parentBankName" readonly  placeholder="开户银行" value="{{$bank_info->account_bank or old('parentBankName')}}" >
                            @if ($errors->has('parentBankName'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('parentBankName') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">	持卡人 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="account_name" readonly  placeholder="持卡人" value="{{$bank_info->account_name or old('account_name')}}">
                            @if ($errors->has('account_name'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('account_name') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">银行卡号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="bank_account" readonly  placeholder="银行卡号" value="{{$bank_info->bank_account or old('bank_account')}}">
                            @if ($errors->has('bank_account'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('bank_account') }}</span>
                            @endif
                        </div>
                    </div>



                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">	短信安全验证 </label>
                        <div class="col-sm-10">
                            <input type="text"  class="form-control" placeholder="输入短信验证码" style="width: 200px;float:left;" name="code">
                            <a  id="get_code" class="btn btn btn-info" style="float:left;"><i class="fa fa-refresh"></i> 发送短信</a>
                            @if ($errors->has('code'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('code') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {{--<label for="signin-text" class="col-sm-2 control-label">	持卡人 </label>--}}
                        <div class="col-sm-10">
                            <input type="hidden" class="form-control" name="bank_name" id="bank_name"   placeholder="" value="">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                            <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>



                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>

@endsection
@section('js')
    <script src="{{asset('image_frame/js/jqueryLibrary.min.js')}}"></script>
    <script src="{{asset('image_frame/js/jquery.imgbox.pack.js')}}"></script>
    <script>
        $(function(){
            $(".img1").imgbox();

        });
        $('#get_code').click(function(){
            $.ajax({
                type: "POST",
                url: '{{url('/config/send_message')}}',
                data: {account_name:$('#bank_id option:selected').attr('tip')},
                dataType: "json",
                success: function(data){
                    if(data.cod=1){
                        alert(data.message);
                    }else{
                        alert(data.message);
                    }
                }
            });
        });

    </script>
@endsection