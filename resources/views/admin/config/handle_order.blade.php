@extends('admin.layouts.app')
<style>
    .title{
        color:black;
        font-size:16px;
    }
</style>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">分账</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/config/handle_order_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">订单编号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="order_sn"   placeholder="订单编号" value="{{old('order_sn')}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">支付金额(单位:元)</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="amount"   placeholder="支付金额" value="{{old('amount')}}">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">是否完成订单</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="is_update" value="0" checked /> 否
                            <input type="radio" class="control-label" name="is_update" value="1" /> 是
                            <span class="help-block m-b-none text-success"></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">是否分账</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="is_handle" value="0" checked /> 否
                            <input type="radio" class="control-label" name="is_handle" value="1" /> 是
                            <span class="help-block m-b-none text-success"></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">是否标记为漏单</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="is_left" value="0" checked /> 否
                            <input type="radio" class="control-label" name="is_left" value="1" /> 是
                            <span class="help-block m-b-none text-success"></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">支付类型</label>
                        <div class="col-sm-2">
                            <select class="form-control" name="notify_type">
                                @foreach(config('pay.notify_type') as $k=>$v)
                                    <option value="{{$k}}" @if($_GET['notify_type']==$k) selected @endif>{{$v['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 提交</button>
                </div>
            </form>
        </div>
    </div>



    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection