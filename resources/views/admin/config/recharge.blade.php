@extends('admin.layouts.app')
@section('content')
    <div class="panel panel-headline">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">用户充值</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 消费分润比例即用户消费后分给商家的比例,20表示20%</div>--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/config/recharge_submit')}}" class="form-horizontal">
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$_GET['uid']}}" name="uid">

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">手机号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mobile"   placeholder="手机号" value="{{old('mobile')}}">
                            @if ($errors->has('mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">用户类型 </label>
                        <div class="col-sm-10">
                            <select class="form-control" name="owner_type">
                                <option value="1" @if($_GET['owner_type']=='1') selected @endif>用户</option>
                                <option value="2" @if($_GET['owner_type']=='2') selected @endif>商家</option>
                                <option value="5" @if($_GET['owner_type']=='5') selected @endif>机主</option>
                                <option value="11" @if($_GET['owner_type']=='11') selected @endif>运营中心</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">充值类型 </label>
                        <div class="col-sm-10">
                            <select class="form-control" name="pursetype">
                                <option value="1" @if($_GET['pursetype']=='1') selected @endif>收益钱包</option>
                                <option value="3" @if($_GET['pursetype']=='3') selected @endif>积分钱包</option>
                                {{--<option value="7" @if($_GET['pursetype']=='7') selected @endif>商家充值钱包</option>--}}
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">充值金额(元) </label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="amount"   placeholder="充值金额" value="{{old('amount')}}">
                            @if ($errors->has('amount'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('amount') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">	短信安全验证 </label>
                        <div class="col-sm-10">
                            <input type="text"  class="form-control" placeholder="输入短信验证码" style="width: 200px;float:left;" name="code">
                            <a  id="get_code" class="btn btn btn-info" style="float:left;"><i class="fa fa-refresh"></i> 发送短信</a>
                            @if ($errors->has('code'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('code') }}</span>
                            @endif
                        </div>
                    </div>

                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 修改</button>
                    <a href="{{url('/config/set_upgrade')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
    <script src="{{asset('image_frame/js/jqueryLibrary.min.js')}}"></script>
    <script src="{{asset('image_frame/js/jquery.imgbox.pack.js')}}"></script>
    <script>
        $('#get_code').click(function(){
            $.ajax({
                type: "POST",
                url: '{{url('/config/send_message')}}',
                data: {send_type:2},
                dataType: "json",
                success: function(data){
                    if(data.cod=1){
                        alert(data.message);
                    }else{
                        alert(data.message);
                    }
                }
            });
        });

    </script>
@endsection