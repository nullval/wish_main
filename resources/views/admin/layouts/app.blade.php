<!doctype html>
<html lang="en">

<head>
    <title>未特商城总管理后台({{session('admin_info.role_name')}})</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{asset('home/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/linearicons/style.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/chartist/css/chartist-custom.css')}}">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{asset('home/css/main.css')}}">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="{{asset('home/css/demo.css')}}">
    <link rel="stylesheet" href="{{asset('home/css/myself.css')}}">
    <link rel="stylesheet" href="{{asset('bootstrap/bootstrap-switch/bootstrap-switch.css')}}">
    <!-- GOOGLE FONTS -->
    {{--<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">--}}
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('home/img/apple-icon.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('home/img/favicon.png')}}">
    <link href="{{asset('home/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('css/myself.css')}}">
    {{--<script src="{{asset('seller/js/uploadPreview.js')}}" type="text/javascript"></script>--}}
    <style>
        .sidebar .nav > li > a:focus, .sidebar .nav > li > a.active{
            border-left-color:#AEB7C2;
        }
        .sidebar .nav > li > a:hover i, .sidebar .nav > li > a:focus i, .sidebar .nav > li > a.active i{
            color: #AEB7C2;
        }
    </style>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
        /*body, html{width: 100%;height: 100%;margin:0;font-family:"微软雅黑";font-size:14px;}*/
        #l-map{height:250px;width:100%;}
        /*#r-result{width:100%;}*/
    </style>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=RNifyeVIz4GNTjvl9XwDUth7ufYPk2pX"></script>
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="brand">
            <a href="{{url('/index/index')}}">未特商城总后台管理({{session('admin_info.role_name')}})</a>
        </div>
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
            </div>
            {{--<form class="navbar-form navbar-left">--}}
                {{--<div class="input-group">--}}
                    {{--<input type="text" value="" class="form-control" placeholder="Search dashboard...">--}}
                    {{--<span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>--}}
                {{--</div>--}}
            {{--</form>--}}
            {{--<div class="navbar-btn navbar-btn-right">--}}
                {{--<button type="button" class="btn btn-info">Info</button>--}}
            {{--</div>--}}
            <div id="navbar-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="lnr lnr-alarm"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{asset('/home/img/system.png')}}" class="img-circle" alt="Avatar"> <span>{{session('admin_info')->username}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                        <ul class="dropdown-menu">
                            @if(session('admin_info.id') != 1  && session('admin_info.role_id') != 9)
                            <li><a href="{{url('/wish_person/info')}}"><i class="lnr lnr-paperclip"></i> <span>我的资料</span></a></li>
                            @endif

                            <li><a href="{{url('/wish_person/uppwd')}}"><i class="lnr lnr-link"></i> <span>修改密码</span></a></li>
                            <li><a href="{{url('/logout')}}"><i class="lnr lnr-exit"></i> <span>退出</span></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- END NAVBAR -->
    <!-- LEFT SIDEBAR -->
    {{--获取当前控制器名称--}}
    <?php $prefix=request()->route()->getAction()['prefix']; ?>
    {{--获取当前方法--}}
    <?php $route =request()->route()->getAction()['controller'];
    $action=explode('@', $route)[1];?>

    <div id="sidebar-nav" class="sidebar">
        <div class="sidebar-scroll">
            <nav>
                <ul class="nav">
                    <?php
                        $permission_list=session(session('admin_info')->id.'_permission_list');
                    ?>
                    @foreach($permission_list as $k=>$v)
                        @if($v['level']==0)
                            <li>
                                @if(empty($v['a']))
                                    <a href="#{{$v['c']}}" data-toggle="collapse" class="collapsed @if($prefix=="/".$v['c']) active @endif"><i class="{{$v['icron']}}"></i> <span>{{$v['name']}}</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                                    <div id="{{$v['c']}}" class="collapse @if($prefix=="/".$v['c']) in @endif">
                                        <ul class="nav">
                                            @foreach($permission_list as $k1=>$v1)
                                                @if($v1['pid']==$v['id'])
                                                    <li><a href="{{url('/'.$v1['c'].'/'.$v1['a'])}}" class="@if($action==$v1['a']) active @endif">{{$v1['name']}}</a></li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                @else
                                    <a href="{{url('/'.$v['c'].'/'.$v['a'])}}" class="@if($prefix=="/".$v['c']) active @endif"><i class="{{$v['icron']}}"></i> <span>{{$v['name']}}</span></a>
                                @endif
                            </li>
                        @endif
                    @endforeach
                </ul>
            </nav>
        </div>
    </div>
    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @yield('content')
           </div>
        </div>
    </div>
    <!-- END MAIN -->
    <div class="clearfix"></div>
    <footer>
        <div class="container-fluid">
            <p class="copyright">Copyright &copy; 2017.Company name All rights reserved.</p>
        </div>
    </footer>
</div>
<!-- END WRAPPER -->
<!-- Javascript -->
<script src="{{asset('home/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('home/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('home/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('home/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('home/vendor/chartist/js/chartist.min.js')}}"></script>
<script src="{{asset('home/js/klorofil-common.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor/ueditor.config.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor/ueditor.all.min.js')}}"> </script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor/lang/zh-cn/zh-cn.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('bootstrap/bootstrap-switch/bootstrap-switch.js')}}"> </script>
<script src="{{asset('home/js/moment-with-locales.js')}}"></script>
<script src="{{asset('home/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('home/js/bootstrap-datetimepicker.zh-CN.js')}}"></script>
@yield('js')
<script type="text/javascript" charset="utf-8" src="{{asset('js/myself.js')}}"> </script>
{{--<script src="//apps.bdimg.com/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>--}}
{{--<script src="{{asset('jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js')}}"></script>--}}
<script>
    //设置日期时间控件
    function Datetime($obj) {
        $($obj).datetimepicker({
            language: 'zh-CN',//显示中文
            format: 'yyyy-mm-dd',//显示格式
            minView: "month",//设置只显示到月份
            initialDate: new Date(),
            autoclose: true,//选中自动关闭
            todayBtn: true,//显示今日按钮
            locale: moment.locale('zh-cn')
        });
    }
</script>
</body>

</html>
