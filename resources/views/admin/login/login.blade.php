<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta   http-equiv= "Pragma"   content= "no-cache" />
    <meta   http-equiv= "Cache-Control"   content= "no-cache" />
    <meta   http-equiv= "Expires"   content= "0" />
    {{--<meta name="viewport" content="width=device-width, initial-scale=1.0">--}}
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />

    <title>未特商城总后台管理系统</title>
    <link href="{{asset('admin/Wopop_files/style_log.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/Wopop_files/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/Wopop_files/userpanel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/Wopop_files/jquery.ui.all.css')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('home/img/apple-icon.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('home/img/favicon.png')}}">
    <style>
        @media screen and (min-width:320px) and (max-width:414px){
            .login_boder{
                background-size: 100%!important;
            }
            .login_m{
                width: 100%!important;
            }
            .login{
                width: 100%!important;
            }
        }
    </style>
</head>

<body class="login" mycollectionplug="bind">
<div class="login_m">
    <div class="login_logo" width="196" height="46" style="font-size:25px;margin-top:200px;">
        <!--<img src="__PUBLIC__/Admin/Wopop_files/logo.png" width="196" height="46"></div>-->
        {{--@if($loginType == 'seller')--}}
                {{--未特商城商家后台管理系统--}}
            {{--@elseif($loginType == 'generalize')--}}
                {{--未特商城直推人后台管理系统--}}
            {{--@elseif($loginType == 'agent')--}}
                {{--未特商城代理人后台管理系统--}}
            {{--@elseif($loginType == 'company')--}}
                {{--未特商城分公司后台管理系统--}}
            {{--@elseif($loginType == 'corporation')--}}
                {{--未特商城总公司后台管理系统--}}
            {{--@else--}}
        {{--@endif--}}
        未特商城管理系统
        <div class="login_boder" style="margin-top:20px;height: 350px;">

            <div class="login_padding" id="login_model">
                <form method="post" action="{{url('login_submit')}}">
                    {{csrf_field()}}
                    {{--<input type="hidden" name="login_type" value="{{$loginType}}" />--}}
                    <div style="display: flex; justify-content: space-around;align-items: center;align-content: center;margin-bottom: 8px">
                        <h2 style="margin-bottom:0;">登录类型</h2>

                        <select name="login_type"  style="flex: .5;">
                            <option @if($loginType == 'corporation') selected @endif value="corporation">---请选择---</option>
                            <option @if($loginType == 'seller') selected @endif value="seller">商户</option>
                            <option @if($loginType == 'generalize') selected @endif value="generalize">直推人</option>
                            <option @if($loginType == 'agent') selected @endif value="agent">代理</option>
                            <option @if($loginType == 'company') selected @endif value="company">分公司</option>
                        </select>
                    </div>
                    <div style="margin-top: 36px;">
                    <label>
                        <span style="font-size:15px;width: 18%;display: inline-block;">用户名</span>
                        <input type="text" name="username" autocomplete="off" id="username" value="{{old('username')}}" class="txt_input txt_input2" style="width: 74%;">
                    </label>
                    </div>
                    <div>
                    <label>
                        <span style="font-size:15px;width: 18%;display: inline-block;"">密码</span>
                        <input type="password" name="password" autocomplete="off" id="password" value="{{old('password')}}" class="txt_input"  style="width: 74%;">
                    </label>
                    </div>
                    <div style="clear:both;">
                        @if ($errors->has('error'))
                            <span class="help-block m-b-none text-danger" style="color:red">{{ $errors->first('error') }}</span>
                        @endif
                    </div>


                    <p class="forgot"></p>
                    <div class="rem_sub">
                        <div class="rem_sub_l">
                            <input type="checkbox" name="checkbox" id="save_me">
                            <label for="checkbox">记住我</label>
                        </div>
                        <label>
                            <input type="submit"  class="sub_button" name="button" id="button" value="登录" style="opacity: 0.7;background: #2c2e2f;">
                        </label>
                    </div>
                </form >
            </div>

            <div id="forget_model" class="login_padding" style="display:none">
                <br>

                <h1>Forgot password</h1>
                <br>
                <div class="forget_model_h2">(Please enter your registered email below and the system will automatically reset users’ password and send it to user’s registered email address.)</div>
                <label>
                    <input type="text" id="usrmail" class="txt_input txt_input2">
                </label>


                <div class="rem_sub">
                    <div class="rem_sub_l">
                    </div>
                    <label>
                        <input type="submit" class="sub_buttons" name="button" id="Retrievenow" value="Retrieve now" style="opacity: 0.7;">
                        　　　
                        <input type="submit" class="sub_button" name="button" id="denglou" value="Return" style="opacity: 0.7;">　　

                    </label>
                </div>
            </div>






            <!--login_padding  Sign up end-->
        </div><!--login_boder end-->
    </div><!--login_m end-->
    <br> <br>

</body>
<script>

</script>
</html>