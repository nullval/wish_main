@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">消费订单管理</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding">
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="notify_type">--}}
                        {{--<option value="0">--支付类型--</option>--}}
                        {{--@foreach(config('pay.notify_type') as $k=>$v)--}}
                            {{--<option value="{{$k}}" @if($_GET['notify_type']==$k) selected @endif>{{$v['name']}}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                {{--</div>--}}
                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="type">--}}
                        {{--<option value="0">--订单类型--</option>--}}
                        {{--<option value="1" @if($_GET['type']==1) selected @endif>订单支付</option>--}}
                        {{--<option value="3" @if($_GET['type']==3) selected @endif>消费买单</option>--}}
                        {{--<option value="4" @if($_GET['type']==4) selected @endif>pos机商城购买商品</option>--}}
                        {{--<option value="5" @if($_GET['type']==5) selected @endif>线下扫码</option>--}}
                        {{--<option value="7" @if($_GET['type']==7) selected @endif>商家充值</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="col-sm-2">
                    <select class="form-control" id="pay_type">
                        <option value="0" >--支付方式--</option>
                        <option value="1" @if($_GET['pay_type']==1) selected @endif>微信</option>
                        <option value="2" @if($_GET['pay_type']==2) selected @endif>支付宝</option>
                        <option value="3" @if($_GET['pay_type']==3) selected @endif>银行卡</option>
                    </select>
                </div>

                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="pay_status">--}}
                        {{--<option value="0" >--订单状态--</option>--}}
                        {{--@foreach(config('pay.pay_status') as $k=>$v)--}}
                            {{--<option value="{{$k}}" @if($_GET['pay_status']==$k) selected @endif>{{$v}}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="col-sm-4">
                    <a class='input-group date' id='start_time' style="float:left">
                        <input type='text' class="form-control" id='start_time_value' style="width: 150px; height: 30px;" value="{{$_GET['start_time']}}"/>
                        <span class="input-group-addon" style="float: left; width: 50px; height: 30px;">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </a>
                    <span style="float:left" >  ~  </span>
                    <a class='input-group date' id='end_time' style="float:left">
                        <input type='text' class="form-control" id='end_time_value' style="width: 150px; height: 30px;" value="{{$_GET['end_time']}}"/>
                        <span class="input-group-addon" style="float: left; width: 50px; height: 30px;">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </a>
                </div>
                <div class="col-sm-12" style="margin-top: 10px;">
                    <input type="text" id="order_sn" class="form-control" placeholder="订单编号" style="width: 200px;float:left;" value="{{$_GET['order_sn']}}"/>
                    <input type="text" id="buyer_mobile" class="form-control" placeholder="消费会员" style="width: 200px;float:left;" value="{{$_GET['buyer_mobile']}}"/>
                    {{--<input type="text" id="seller_mobile" class="form-control" placeholder="所属商家" style="width: 200px;float:left;" value="{{$_GET['seller_mobile']}}"/>--}}
                    {{--<input type="text" id="agent_mobile" class="form-control" placeholder="所属机主" style="width: 200px;float:left;" value="{{$_GET['agent_mobile']}}"/>--}}
                    {{--<input type="text" id="terminalId" class="form-control" placeholder="刷卡使用的未特商城pos机" style="width: 200px;float:left;" value="{{$_GET['terminalId']}}"/>--}}
                    <a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>
                </div>
                {{--@if(config('app.env')!='online')--}}
                    {{--<div class="col-sm-12" style="margin-top: 10px;">--}}
                        {{--<a type="submit" href="{{url('')}}" class="btn btn btn-info" style="float:left;"><i class="fa fa-plus"></i> 生成购买pos机订单</a>--}}
                        {{--<a type="submit" href="{{url('')}}" class="btn btn btn-info" style="float:left; margin-left: 20px;"><i class="fa fa-plus"></i> 生成消费买单</a>--}}
                    {{--</div>--}}
                {{--@endif--}}
                {{--<div class="col-sm-12" >--}}
                    {{--<ul class="pagination" style="float: left">--}}
                        {{--<li>--}}
                            {{--<span href="#">总计消费金额:￥{{ $sum_data['actual_amount'] or 0}}</span>--}}
                            {{--<span href="#">总计商家货款:￥{{ $sum_data['seller_amount'] or 0}}</span>--}}
                            {{--<span href="#">总计商家让利:￥{{ $sum_data['platform_amount'] or 0}}</span>--}}
                            {{--<span href="#">总计刷卡扣费:￥{{ $sum_data['poundage'] or 0}}</span>--}}
                            {{--<span href="#">商家充值:￥{{ $sum_data['recharge'] or 0}}</span>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>订单编号</th>
                        <th>消费会员</th>
                        {{--<th>所属商家</th>--}}
                        {{--<th>所属机主</th>--}}
                        {{--<th>刷卡的pos机</th>--}}
                        <th>订单类型</th>
                        <th>消费金额</th>
                        <th>实付金额</th>
                        <th>商家分润</th>
                        <th>平台分润</th>
                        <th>刷卡扣费</th>
                        {{--<th>支付公司</th>--}}
                        <th>支付方式</th>
                        <th>订单状态</th>
                        {{--<th>上游编号</th>--}}
                        <th>创建时间</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($order_list as $k=>$v)
                            {{--<tr>--}}
                                {{--<td colspan="16">商家名称:{{$v->seller_name}}</td>--}}
                            {{--</tr>--}}
                            <tr>
                                <td>{{$v->order_sn}}</td>
                                <td>{{$v->buyer_mobile or '暂无'}}</td>
                                {{--<td>{{$v->seller_mobile or '暂无'}}</td>--}}
                                {{--<td>{{$v->agent_mobile or '暂无'}}</td>--}}
                                {{--<td>{{$v->terminalId or '暂无'}}</td>--}}
                                {{--<td>{{$v->mobile or '暂无'}}</td>--}}
                                <td>{{config('pay.order_type')[$v->type]}}</td>
                                <td>{{$v->amount or 0}}</td>
                                <td>{{$v->actual_amount or 0}}</td>
                                <td>{{$v->seller_amount or 0}}</td>
                                <td>{{$v->platform_amount or 0}}</td>
                                <td>{{$v->poundage or 0}}</td>
                                {{--<td>--}}
                                    {{--{{get_notify_type_name($v->notify_type)}}--}}
                                {{--</td>--}}
                                <td>
                                    {{get_pay_type_name($v->pay_type)}}
                                </td>
                                <td>
                                    @if($v->status==1)
                                        <span class="text-primary">处理中</span>
                                    @elseif($v->status==2)
                                        <span class="text-success">支付成功</span>
                                    @elseif($v->status==3)
                                        <span class="text-danger">支付失败</span>
                                    @endif
                                </td>
                                {{--<td>{{$v->up_order_sn or '暂无'}}</td>--}}
                                <td>{{$v->created_at or '暂无'}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $order_list->appends([
                    'terminalId'=>$_GET['terminalId'] ?? '',
                    'buyer_mobile'=>$_GET['buyer_mobile'] ?? '',
                    'seller_mobile'=>$_GET['seller_mobile'] ?? '',
                    'agent_mobile'=>$_GET['agent_mobile'] ?? '',
                    'order_sn'=>$_GET['order_sn'] ?? '',
                    'notify_type'=>$_GET['notify_type'] ?? 0,
                    'type'=>$_GET['type'] ?? 0,
                    'pay_type'=>$_GET['pay_type'] ?? 0,
                    'start_time'=>$_GET['start_time'] ?? '',
                    'end_time'=>$_GET['end_time'] ?? '',
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $order_list->total()}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            $('#notify_type,#type,#pay_type,#pay_status').change(function(){
                location_a();
            });
            $('#search').click(function(){
                location_a();
            });
            Datetime('#start_time');
            Datetime('#end_time');
        });
        function location_a() {
            var seller_mobile = $('#seller_mobile').val()
            var buyer_mobile = $('#buyer_mobile').val()
            var agent_mobile = $('#agent_mobile').val()
            var order_sn = $('#order_sn').val()
            var year = $('#year').val()
            var notify_type = $('#notify_type').val()
            var type = $('#type').val()
            var pay_type = $('#pay_type').val()
            var start_time = $('#start_time').find("input").val()
            var end_time = $('#end_time').find("input").val()
            var pay_status = $('#pay_status').val()

            location.href="{{url('/seller_manager/consume_list')}}?seller_mobile="+(seller_mobile==undefined?'':seller_mobile)
                +"&buyer_mobile="+(buyer_mobile==undefined?'':buyer_mobile)
                +"&agent_mobile="+(agent_mobile==undefined?'':agent_mobile)
                +"&order_sn="+(order_sn==undefined?'':order_sn)
                +"&year="+(year==undefined?'':year)
                +"&notify_type="+(notify_type==undefined?'':notify_type)
                +"&type="+(type==undefined?'':type)
                +"&pay_type="+(pay_type==undefined?'':pay_type)
                +"&start_time="+(start_time==undefined?'':start_time)
                +"&end_time="+(end_time==undefined?'':end_time)
                +"&pay_status="+(pay_status==undefined?'':pay_status)
        }
    </script>
@endsection