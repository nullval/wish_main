<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta   http-equiv= "Pragma"   content= "no-cache" />
    <meta   http-equiv= "Cache-Control"   content= "no-cache" />
    <meta   http-equiv= "Expires"   content= "0" />
    <title>未特商城商家后台管理系统</title>
    <link rel="stylesheet" type="text/css" href="{{asset('seller/css/normalize.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('seller/css/demo.css')}}" />
    <!--必要样式-->
    <link rel="stylesheet" type="text/css" href="{{asset('seller/css/component.css')}}" />
    <!--[if IE]>
    <script src="{{asset('seller/js/html5.js')}}"></script>
    <![endif]-->
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('home/img/apple-icon.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('home/img/favicon.png')}}">
    <style>
        .f_a{
            display: block;
            height: 46px;
            line-height: 46px;
            background: #dddddd;
            text-align: center;
            border: #dddddd;
            color:#0096e6;
            font-size:12px;
            width:99%;
            cursor:pointer;
            /*border-radius: 50px;*/
        }
    </style>
</head>
<body>
<div class="container demo-1">
    <div class="content">
        <div id="large-header" class="large-header">
            {{--<canvas id="demo-canvas"></canvas>--}}
            <div class="logo_box">
                <h3>未特商城商家后台管理系统</h3>
                <form action="{{url('login_submit')}}"  method="post">
                    {{csrf_field()}}
                    <div class="col-sm-12" style="text-align: center">
                        @if ($errors->has('error'))
                            <span class="error" style="margin-left: 20px;margin-bottom: 5px;color: red">{{ $errors->first('error') }}</span>
                        @endif
                        @if(Session::has('status'))
                            <div class="alert alert-info" style="color: green;padding-bottom: 5px; font-size: 21px">{{Session::get('status')}}</div>
                        @endif
                    </div>

                    <div class="input_outer">
                        <span class="u_user"></span>
                        <input name="mobile" class="text" style="color: #FFFFFF !important" type="text" placeholder="请输入手机号码" value="{{old('mobile')}}">
                        @if ($errors->has('mobile'))
                            <div style="color:red;">{{ $errors->first('mobile') }}</div>
                        @endif
                    </div>
                    {{--<div class="input_outer">--}}
                        {{--<span class="us_uer"></span>--}}
                        {{--<input name="password" class="text" style="color: #FFFFFF !important;"value="" type="password" autocomplete="off" placeholder="请输入密码">--}}
                        {{--@if ($errors->has('password'))--}}
                            {{--<div style="color:red;">{{ $errors->first('password') }}</div>--}}
                        {{--@endif--}}
                        {{--@if ($errors->has('login_error'))--}}
                            {{--<div style="color:red;">{{ $errors->first('login_error') }}</div>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    <div class="input_outer" style="border-radius: 0px;width:65%;float:left;">
                        <input type="hidden" value="{{isset($_COOKIE['openid'])?$_COOKIE['openid']:0}}" name="openid">
                        {{--<input type="test" />--}}
                        {{--<span class="us_uer"></span>--}}
                        <input name="sms_code" class="text" style="margin-left:10px; color: #FFFFFF !important; position:absolute; z-index:100;"  value="{{old('sms_code')}}" type="test" placeholder="请输入手机验证码">
                    </div>
                    <div class="input_outer" style="border-radius: 0px;width:34%;float:right;padding:0;border: 0 !important;;">
                        <input type="button" class="f_a" id="btn" value="发送手机验证码" onclick="send_sms()">
                        {{--<a class="f_a">发送手机短信</a>--}}
                    </div>
                    <div style="clear: both;"></div>
                    @if ($errors->has('sms_code'))
                        <div style="color:red;">{{ $errors->first('sms_code') }}</div>
                    @endif

                    <button type="submit" class="act-but submit"style="color: #FFFFFF;width: 330px;border: 0;">登录</button>
                    {{--<div class="zhuce" style="text-align: right;">--}}
                        {{--<span class="helper-text" ><a href="{{url('/register')}}" >立即注册</a></span>--}}
                    {{--</div>--}}

                    {{--<div class="mb2"><a class="act-but submit" href="javascript:;" style="color: #FFFFFF">登录</a></div>--}}
                </form>
            </div>
        </div>
    </div>
</div><!-- /container -->
<script type="text/javascript" src="{{asset('seller/js/jquery.min.js')}}"></script>
<script>
    function send_sms(){
        mobile = $("input[name=mobile]").val();
//        if(!(/^1[3|4|5|8][0-9]\d{4,8}$/.test(mobile))){
//            alert("请输入正确的手机号码");return false;
//        }
        //发送短信验证码,并设置调用时间
        $.ajax({
            type: "POST",
            url: '{{url('/send_sms')}}',
            data: {mobile: mobile,type:5},
            dataType: "json",
            success: function (data) {
                if (data.code == 1) {
                    alert('短信已发送');
                } else {
                    alert(data.message);
                }
                new invokeSettime("#btn");
            }
        });
    }
</script>
{{--<script src="{{asset('seller/js/TweenLite.min.js')}}"></script>--}}
{{--<script src="{{asset('seller/js/EasePack.min.js')}}"></script>--}}
{{--<script src="{{asset('seller/js/rAF.js')}}"></script>--}}
<script src="{{asset('seller/js/demo-1.js')}}"></script>

{{--<div style="text-align:center;">--}}
{{--<p>更多模板：<a href="http://www.mycodes.net/" target="_blank">源码之家</a></p>--}}
{{--</div>--}}
</body>
</html>