@extends('seller.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" />
@section('content')
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">商家入驻信息</h3>
            {{--<div class="hr-line-solid"></div>--}}
        </div>
        <div class="panel panel-headline">
            <ul id="myTab" class="nav nav-tabs">
                <li class="active">
                    <a href="#A" data-toggle="tab">
                        基础信息登记
                    </a>
                </li>
                <li><a href="#B"  data-toggle="tab">银行卡信息登记</a></li>
                <li><a href="#C" data-toggle="tab">开通支付平台业务</a></li>

            </ul>


            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade in active" id="A">
                    @if ($errors->has('error'))
                        <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                    @endif
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                    <form method="post"   action="{{url('/seller/ght_seller_edit_submit_step1')}}" class="form-horizontal" enctype='multipart/form-data'>
                        <div class="panel-body">
                            {{csrf_field()}}


                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                                <div class="col-sm-10">
                                    <label class="control-label">{{$seller_info->mobile}}</label>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商家信用分 </label>
                                <div class="col-sm-10">
                                    <label class="control-label">{{$seller_info->credit}}</label>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户名称 </label>
                                <div class="col-sm-10">
                                    <label class="control-label">{{$basic_info->merchantName}}</label>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户简称 </label>
                                <div class="col-sm-10">
                                    <label class="control-label">{{$basic_info->shortName}}</label>


                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户城市 </label>
                                <div class="col-sm-10">
                                    @foreach($city_arr as $k=>$v)
                                        @if ($basic_info->city==$v->cityCode)
                                            <label class="control-label">{{$v->cityName}}</label>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户地址 </label>
                                <div class="col-sm-10">
                                    <label class="control-label">{{$basic_info->merchantAddress}}</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">客服电话 </label>
                                <div class="col-sm-10">
                                    <label class="control-label">{{$basic_info->servicePhone}}</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商户类型 </label>
                                <div class="col-sm-10">
                                    @if($basic_info->merchantType=='00')
                                        <label class="control-label" >公司商户</label>

                                    @elseif($basic_info->merchantType=='01')
                                        <label class="control-label" >个体商户</label>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">经营类目 </label>
                                <div class="col-sm-10">
                                    @foreach($category_arr as $k=>$v)
                                        @if ($basic_info->category==$v->categoryCode)
                                            <label class="control-label" >{{$v->categoryName}}</label>
                                        @endif
                                    @endforeach

                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">法人姓名 </label>
                                <div class="col-sm-10">
                                    <label class="control-label">{{$basic_info->corpmanName}}</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">法人身份证 </label>
                                <div class="col-sm-10">
                                    <label class="control-label">{{$basic_info->corpmanId}}</label>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">法人联系手机 </label>
                                <div class="col-sm-10">
                                    <label class="control-label">{{$basic_info->corpmanMobile}}</label>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>


                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">开户行全称 </label>
                                <div class="col-sm-10">
                                    @foreach($bank_arr as $k=>$v)
                                        @if ($basic_info->bankCode==$v->bankCode)
                                            <label class="control-label" >{{$v->bankName}}</label>
                                        @endif
                                    @endforeach

                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>



                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">营业执照号 </label>
                                <div class="col-sm-10">
                                    <label class="control-label">{{$basic_info->businessLicense}}</label>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">营业执照</label>
                                <div style="text-align: left;" class="col-sm-4">
                                    <img id="imgShow_WU_FILE_0" width="100%"  src="{{$basic_info->businessimage or asset('/images/timg.jpg')}}" />
                                </div>

                            </div>

                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">开户行账号 </label>
                                <div class="col-sm-10">
                                    <label class="control-label">{{$basic_info->bankaccountNo}}</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">开户户名 </label>
                                <div class="col-sm-10">
                                    <label class="control-label">{{$basic_info->bankaccountName}}</label>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="B">
                    <form method="post"   action="{{url('/seller/ght_seller_edit_submit_step2')}}" class="form-horizontal" enctype='multipart/form-data'>
                        <div class="panel-body">
                            {{csrf_field()}}


                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                                <div class="col-sm-10">
                                    <label class="control-label">{{$seller_info->mobile}}</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">子商户编码 </label>
                                <div class="col-sm-10">
                                    <label class="control-label">{{$bank_info->merchantId}}</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">开户行全称 </label>
                                <div class="col-sm-10">
                                    @foreach($bank_arr as $k=>$v)
                                        @if($bank_info->bankCode==$v->bankCode)
                                            <label class="control-label" >{{$v->bankName}} </label>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>


                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">持卡人姓名 </label>
                                <div class="col-sm-10">
                                    <label class="control-label" >{{$bank_info->name}} </label>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">银行卡号 </label>
                                <div class="col-sm-10">
                                    <label class="control-label" >{{$bank_info->bankaccountNo}} </label>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">身份证号码 </label>
                                <div class="col-sm-10">
                                    <label class="control-label" >{{$bank_info->certNo}} </label>
                                </div>
                            </div>


                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">联行号所属银行 </label>
                                <div class="col-sm-10">
                                    @foreach($bank_list as $v)
                                        @if($v->lbnkNo==$bank->ibankno)
                                            <label class="control-label" >{{$v->bank_name}} </label>
                                        @endif
                                    @endforeach
                                </div>
                            </div>


                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>



                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">银行卡所属地区银行联行号 </label>
                                <div class="col-sm-10">
                                    <label class="control-label" >{{$bank->ibankno}} </label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>


                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="C">
                    <div class="alert alert-warning"> 费率即用户消费后分给平台的比例,按百分比计算,20表示给平台的折扣为20%</div>
                    <form method="post"   action="{{url('/seller/ght_seller_edit_submit_step3')}}" class="form-horizontal" enctype='multipart/form-data'>
                        <div class="panel-body">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">商家手机号 </label>
                                <div class="col-sm-10">
                                    <label class="control-label" >{{$seller_info->mobile}} </label>

                                </div>
                            </div>



                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">子商户编码 </label>
                                <div class="col-sm-10">
                                    <label class="control-label" >{{$busi_info->merchantId}} </label>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label">费率 </label>
                                <div class="col-sm-10">
                                    <label class="control-label" >{{$busi_info->futureRateValue}} </label>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="hr-line-dashed"></div>

                            {{--<h2 class="panel-title">货款开通的支付平台业务</h2>--}}
                        </div>
                        {{--<div class="panel-body no-padding">--}}
                        {{--<table class="table table-striped">--}}
                        {{--<thead>--}}
                        {{--<tr>--}}
                        {{--<th>编号</th>--}}
                        {{--<th>业务类型</th>--}}
                        {{--<th>费率类型</th>--}}
                        {{--<th>费率</th>--}}
                        {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody>--}}
                        {{--@foreach($busi_list as $k=>$v)--}}
                        {{--<tr>--}}
                        {{--<td>{{$k}}</td>--}}
                        {{--<td>{{$v['busiName']}}</td>--}}
                        {{--<td>--}}
                        {{--@if($v['futureRateType']==1) 百分比--}}
                        {{--@else 单笔固定金额--}}
                        {{--@endif--}}
                        {{--</td>--}}
                        {{--<td>{{$v['futureRateValue']}}</td>--}}

                        {{--</tr>--}}
                        {{--@endforeach--}}
                        {{--</tbody>--}}
                        {{--</table>--}}

                        {{--</div>--}}
                    </form>
                </div>
            </div>

        </div>


    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection
@section('js')
    <script>
        $(document).ready(function(){
            $('#bankName').change(function(){
                $('input[name=bankCode]').val($('#bankName option:selected').attr('tip'));

            });
            $('#bankName1').change(function(){
                $('input[name=bankCode1]').val($('#bankName1 option:selected').attr('tip'));

            });
            $("#up_img_WU_FILE_0").change(function(){
                var v = $(this).val();
                var reader = new FileReader();
                reader.readAsDataURL(this.files[0]);
                reader.onload = function(e){
                    console.log(e.target.result);
                    $('#uploadfile').val(e.target.result);
                };
            });
        });
        function getbankNo() {
            var bank_name=$("#bank_name").val();
            $.ajax({
                url:'{{url('/seller/get_bankNo')}}',
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    bank_name:bank_name
                },
                timeout:5000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data){

                    if (data.code==0){
                        alert(data.message)
                    }else {
                        $('#ibankno').val(data.data.lbnkNo);
                        $('#account_bank').val(data.data.bank_name);
                    }
                },
                error:function(xhr,textStatus){
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },

            })

        }

    </script>
@endsection