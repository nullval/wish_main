@extends('seller.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" />
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">
                    @if(empty($banner_info))
                        新增banner
                    @else
                        编辑banner
                    @endif
                </h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/settle/banner_add_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$banner_info->id or old('id')}}" name="id">
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">banner图名称 </label>
                        <div class="col-sm-10">
                            {{--<label class="control-label">{{$seller_info->mobile}}</label>--}}
                            <input type="text" class="form-control" name="name"    placeholder="banner图名称" value="{{$banner_info->name or old('name')}}">
                            @if ($errors->has('name'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">banner图片</label>

                            <ul id="warp">
                                <li>
                                    <input type="hidden" value="" name="old_businessimage"/>
                                    <div style="text-align: left;" class="col-sm-4">
                                        <img id="imgShow_WU_FILE_0" width="100%"  src="@if($banner_info->businessimage != ""){{$banner_info->businessimage}}@else{{asset('/images/timg.jpg')}}@endif" />
                                    </div>
                                    <div class="col-sm-2" style="text-align: right;">
                                        <input type="file" id="up_img_WU_FILE_0" name="businessimage" />
                                        <input  type="hidden" id="uploadfile" name="businessimagebase64" />
                                        <input  type="hidden" name="businessimage" value="{{$banner_info->businessimage or old('businessimage')}}" />
                                    </div>
                                </li>
                            </ul>


                    </div>


                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                        <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 发布</button>
                    <a href="{{url('/settle/banner_list')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>

                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')
<script>

    $(document).ready(function(){
        $('#bankName').change(function(){
            $('input[name=bankCode]').val($('#bankName option:selected').attr('tip'));

        });
        $('#bankName1').change(function(){
            $('input[name=bankCode1]').val($('#bankName1 option:selected').attr('tip'));

        });
        $("#up_img_WU_FILE_0").change(function(){
            var v = $(this).val();
            var reader = new FileReader();
            reader.readAsDataURL(this.files[0]);
            reader.onload = function(e){
                console.log(e.target.result);
                $('#uploadfile').val(e.target.result);
            };
        });

    });


</script>
@endsection