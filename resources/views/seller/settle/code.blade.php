@extends('seller.layouts.app')
@section('content')
    <div class="panel panel-headline">
        <div class="panel ">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">我的台码</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <div class="panel-body">
                <div class="col-sm-10">
                    <img class="img-responsive center-block" style="float: left" src="{{config('config.tdzd_api_url')}}qrpay/seller_qr?sid={{session('seller_info')->id}}" >
                    {{--<img class="img-responsive center-block" style="float: right" src="http://api.3dqxm.com/qrpay/seller_qr_back?sid={{session('seller_info')->id}}" >--}}
                </div>
            </div>

        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>

@endsection
@section('js')

@endsection