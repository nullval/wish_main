@extends('seller.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">商家联盟banner列表</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding">--}}
                <div class="col-sm-12" style="margin-bottom: 10px">
                    @if ($errors->has('error'))
                        <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                    @endif
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                @if($count < 3)
                    <div class="col-sm-6" style="float: right">
                        <a href="{{url('/settle/banner_add')}}" class="btn btn-info" style="float: right;"><i class="fa fa-plus"></i> 新增banner</a>
                    </div>
                @endif

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>banner名称</th>
                        <th>banner缩略图</th>
                        <th>编辑</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($banner_list as $k => $v)
                        <tr>
                            <td>{{$v -> name or '暂无'}}</td>
                            <td><img src="{{$v -> businessimage or asset('/images/timg.jpg')}}" style="width: 80px;height: 80px"></td>
                            <td>
                                <a href="{{url('/settle/banner_add')}}?id={{$v -> id }}"><span class="label label-info"><i class="fa fa-edit"></i> 编辑</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            {{--</div>--}}
            <div class="panel-footer">
                <div class="row">
                    {{ $banner_list->appends([

                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $banner_list->total()}}</a>
                        </li>
                    </ul>
                    {{--<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>--}}
                    {{--<div class="col-md-6 text-right"><a href="#" class="btn btn-primary">View All Purchases</a></div>--}}
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $(function(){
            $('#year,#status').change(function(){
                location.href="{{url('/finance/withdraw_list')}}?status="+$('#status').val();
            });
        });
    </script>
@endsection