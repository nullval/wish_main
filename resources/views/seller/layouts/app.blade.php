<!doctype html>
<html lang="en">

<head>
    <title>
        @if(session('role_info') != '')
            {{session('seller_name') -> merchantName.'后台'}}
        @else
            未特商城商家后台
        @endif

    </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{asset('home/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/linearicons/style.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/chartist/css/chartist-custom.css')}}">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{asset('home/css/main.css')}}">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="{{asset('home/css/demo.css')}}">
    <link rel="stylesheet" href="{{asset('home/css/myself.css')}}">
    {{--<link rel="stylesheet" href="{{asset('home/css/bootstrap.min.css')}}">--}}
    {{--<link href="{{asset('home/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" media="screen">--}}
    <!-- GOOGLE FONTS -->
    {{--<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">--}}
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('home/img/apple-icon.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('home/img/favicon.png')}}">
    <link rel="stylesheet" href="{{asset('css/myself.css')}}">
    <script src="{{asset('seller/js/uploadPreview.js')}}" type="text/javascript"></script>
    <script src="{{asset('seller/js/ajaxfileupload.js')}}" type="text/javascript"></script>

</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="brand">
            <a href="{{url('/index/index')}}"><img src="{{asset('home/img/logo-dark1.png')}}" alt="Klorofil Logo" class="img-responsive logo"></a>
        </div>
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
            </div>
            {{--<form class="navbar-form navbar-left">--}}
                {{--<div class="input-group">--}}
                    {{--<input type="text" value="" class="form-control" placeholder="Search dashboard...">--}}
                    {{--<span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>--}}
                {{--</div>--}}
            {{--</form>--}}
            {{--<div class="navbar-btn navbar-btn-right">--}}
                {{--<a class="btn btn-success update-pro" href="#downloads/klorofil-pro-bootstrap-admin-dashboard-template/?utm_source=klorofil&utm_medium=template&utm_campaign=KlorofilPro" title="Upgrade to Pro" target="_blank"><i class="fa fa-rocket"></i> <span>UPGRADE TO PRO</span></a>--}}
            {{--</div>--}}
            <div id="navbar-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown hidden-xs">
                        <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <i class="lnr lnr-alarm"></i>
                            {{--<span class="badge bg-danger">5</span>--}}
                        </a>
                        {{--<ul class="dropdown-menu notifications">--}}
                            {{--<li><a href="#" class="notification-item"><span class="dot bg-warning"></span>System space is almost full</a></li>--}}
                            {{--<li><a href="#" class="notification-item"><span class="dot bg-danger"></span>You have 9 unfinished tasks</a></li>--}}
                            {{--<li><a href="#" class="notification-item"><span class="dot bg-success"></span>Monthly report is available</a></li>--}}
                            {{--<li><a href="#" class="notification-item"><span class="dot bg-warning"></span>Weekly meeting in 1 hour</a></li>--}}
                            {{--<li><a href="#" class="notification-item"><span class="dot bg-success"></span>Your request has been approved</a></li>--}}
                            {{--<li><a href="#" class="more">See all notifications</a></li>--}}
                        {{--</ul>--}}
                    </li>
                    {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="lnr lnr-question-circle"></i> <span>Help</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li><a href="#">Basic Use</a></li>--}}
                            {{--<li><a href="#">Working With Data</a></li>--}}
                            {{--<li><a href="#">Security</a></li>--}}
                            {{--<li><a href="#">Troubleshooting</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{asset('/home/img/avator.jpg')}}" class="img-circle" alt="Avatar"> <span>@if(session('role_info') == ''){{session('seller_info')->mobile}}@else{{session('role_info')->mobile}}@endif</span></a>
                        {{--<i class="icon-submenu lnr lnr-chevron-down"></i>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li><a href="#"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>--}}
                            {{--<li><a href="#"><i class="lnr lnr-envelope"></i> <span>Message</span></a></li>--}}
                            {{--<li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>--}}
                            {{--<li><a href="{{url('/logout')}}"><i class="lnr lnr-exit"></i> <span>退出</span></a></li>--}}
                        {{--</ul>--}}
                    </li>
                    {{--<li>--}}
                        {{--<a class="update-pro" href="{{url('/logout')}}" title="安全退出"><i class="fa fa-rocket"></i> <span>安全退出</span></a>--}}
                    {{--</li>--}}
                </ul>
            </div>
        </div>
    </nav>
    <!-- END NAVBAR -->
    <!-- LEFT SIDEBAR -->
    {{--获取当前控制器名称--}}
    <?php $prefix=request()->route()->getAction()['prefix']; ?>
    {{--获取当前方法--}}
    <?php $route =request()->route()->getAction()['controller'];
            $action=explode('@', $route)[1];?>

    <div id="sidebar-nav" class="sidebar" style="overflow: auto;">
        <div class="sidebar-scroll" style="margin-bottom: 30px">
            <nav>
                <ul class="nav">
                    <?php
                        $permission_list=session(session('seller_info')->id.'_permission_list');
                    ?>
                    @foreach($permission_list as $k=>$v)
                        @if($v['level']==0)
                            <li>
                                @if(empty($v['a']))
                                    <a href="#{{$v['c']}}" data-toggle="collapse" class="collapsed @if($prefix=="/".$v['c']) active @endif"><i class="{{$v['icron']}}"></i> <span>{{$v['name']}}</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                                    <div id="{{$v['c']}}" class="collapse @if($prefix=="/".$v['c']) in @endif">
                                        <ul class="nav">
                                            @foreach($permission_list as $k1=>$v1)
                                                {{--@if($v1['a'] != 'code')--}}
                                                @if($v1['is_list'] == 1)
                                                    @if($v1['pid']==$v['id'])
                                                        <li><a href="{{url('/'.$v1['c'].'/'.$v1['a'])}}" class="@if($action==$v1['a']) active @endif">{{$v1['name']}}</a></li>
                                                    @endif
                                                @endif
                                                {{--@endif--}}
                                            @endforeach
                                        </ul>
                                    </div>
                                @else
                                    <a href="{{url('/'.$v['c'].'/'.$v['a'])}}" class="@if($prefix=="/".$v['c']) active @endif"><i class="{{$v['icron']}}"></i> <span>{{$v['name']}}</span></a>
                                @endif
                            </li>
                        @endif
                    @endforeach
                    @if(!empty(session('seller_info')))
                            @if(env('APP_ENV')=='online')
                                <li><a href="{{config('config.tdzd_sapi_url')}}oauth/weixin?state=5" ><i class="lnr lnr-home"></i> <span>积分增值</span></a></li>
                            @else
                                <li><a href="{{config('config.tdzd_shop_url')}}businessIncrement/dist/#/" ><i class="lnr lnr-home"></i> <span>积分增值</span></a></li>
                            @endif
                    @endif

                    {{--<li><a href="{{'/index/index'}}" class="@if($prefix=='/index') active @endif"><i class="lnr lnr-home"></i> <span>首页</span></a></li>--}}
                    {{--<li>--}}
                        {{--<a href="#index" data-toggle="collapse" class="collapsed @if($prefix=='/settle') active @endif"><i class="lnr lnr-home"></i> <span>商家入驻</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>--}}
                        {{--<div id="index" class="collapse @if($prefix=='/settle') in @endif">--}}
                            {{--<ul class="nav">--}}
                                {{--<li><a href="{{url('/index/index')}}" class="@if($action=='index') active @endif">商家入驻</a></li>--}}
                                {{--<li><a href="{{url('/settle/apply')}}" class="@if($action=='apply') active @endif">商家入驻</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</li>--}}


                    {{--<li>--}}
                        {{--<a href="#member" data-toggle="collapse" class="collapsed @if($prefix=='/order') active @endif"><i class="lnr lnr-linearicons"></i> <span>订单管理</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>--}}
                        {{--<div id="member" class="collapse @if($prefix=='/order') in @endif">--}}
                            {{--<ul class="nav">--}}
                                {{--<li><a href="{{url('/order/order_list')}}" class="@if($action=='order_list') active @endif">未特商城pos机消费订单列表</a></li>--}}
                                {{--<li><a href="{{url('/member/invite_user')}}" class="@if($action=='invite_user') active @endif">推广会员</a></li>--}}
                                {{--<li><a href="{{url('/member/achievement')}}" class="@if($action=='achievement') active @endif">业绩查询</a></li>--}}
                                {{--<li><a href="{{url('/member/tree')}}" class="@if($action=='tree') active @endif">会员查询</a></li>--}}
                                {{--@if(\Illuminate\Support\Facades\Cache::get('is_open_upgrade')==1)--}}
                                    {{--<li><a href="{{url('/member/upgrade')}}" class="@if($action=='upgrade') active @endif">补单升级</a></li>--}}
                                {{--@endif--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</li>--}}

                    {{--<li>--}}
                        {{--<a href="#pos" data-toggle="collapse" class="collapsed @if($prefix=='/pos') active @endif"><i class="fa fa-credit-card"></i> <span>未特商城pos机管理</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>--}}
                        {{--<div id="pos" class="collapse @if($prefix=='/pos') in @endif">--}}
                            {{--<ul class="nav">--}}
                                {{--<li><a href="{{url('/pos/pos_list')}}" class="@if($action=='pos_list') active @endif">所属未特商城pos机</a></li>           {{--<li>--}}
                        {{--<a href="#pos" data-toggle="collapse" class="collapsed @if($prefix=='/pos') active @endif"><i class="fa fa-credit-card"></i> <span>未特商城pos机管理</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>--}}
                        {{--<div id="pos" class="collapse @if($prefix=='/pos') in @endif">--}}
                            {{--<ul class="nav">--}}
                                {{--<li><a href="{{url('/pos/pos_list')}}" class="@if($action=='pos_list') active @endif">所属未特商城pos机</a></li>--}}
                                {{--<li><a href="{{url('/member/invite_user')}}" class="@if($action=='invite_user') active @endif">推广会员</a></li>--}}
                                {{--<li><a href="{{url('/member/achievement')}}" class="@if($action=='achievement') active @endif">业绩查询</a></li>--}}
                                {{--<li><a href="{{url('/member/tree')}}" class="@if($action=='tree') active @endif">会员查询</a></li>--}}
                                {{--@if(\Illuminate\Support\Facades\Cache::get('is_open_upgrade')==1)--}}
                                {{--<li><a href="{{url('/member/upgrade')}}" class="@if($action=='upgrade') active @endif">补单升级</a></li>--}}
                                {{--@endif--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</li>--}}

                    {{--2016/12/31 特殊处理，该账号无法看到财务--}}
                    {{--@if(!in_array(session('seller_info')->mobile,['15916598554']))--}}
                    {{--<li>--}}
                        {{--<a href="#finance" data-toggle="collapse" class="collapsed @if($prefix=='/finance') active @endif"><i class="lnr lnr-chart-bars"></i> <span>财务管理</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>--}}
                        {{--<div id="finance" class="collapse @if($prefix=='/finance') in @endif">--}}
                            {{--<ul class="nav">--}}
                                {{--<li><a href="{{url('/finance/index?type=0&status=0')}}" class="@if($action=='detail') active @endif">流水管理</a></li>--}}
                                {{--<li><a href="{{url('/finance/withdraw_list')}}" class="@if($action=='withdraw_list'||$action=='withdraw') active @endif">收益提现</a></li>--}}
                                {{--<li><a href="{{url('/finance/withdraw_goods_list')}}" class="@if($action=='withdraw_goods_list'||$action=='withdraw_goods') active @endif">货款提现</a></li>--}}
                                {{--<li><a href="{{url('/finance/exchange')}}" class="@if($action=='exchange_list'||$action=='exchange') active @endif">积分兑换</a></li>--}}
                                {{--<li><a href="{{url('/finance/new_statement_list')}}" class="@if($action=='new_statement_list'||$action=='new_statement_details') active @endif">每日补贴列表</a></li>--}}
                                {{--<li><a href="{{url('/finance/statement_list')}}" class="@if($action=='statement_list'||$action=='statement_details') active @endif">补贴对账单</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--@endif--}}

                    {{--<li>--}}
                        {{--<a href="#config" data-toggle="collapse" class="collapsed @if($prefix=='/config') active @endif"><i class="fa fa-cog"></i> <span>系统管理</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>--}}
                        {{--<div id="config" class="collapse @if($prefix=='/config') in @endif">--}}
                            {{--<ul class="nav">--}}
                                {{--<li><a href="{{url('/config/cashback')}}" class="@if($action=='cashback') active @endif">返现管理</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</li>--}}

                    {{--</li>--}}
                </ul>
            </nav>
        </div>
    </div>
    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @yield('content')
           </div>
        </div>
    </div>
    <!-- END MAIN -->
    {{--<div class="clearfix"></div>--}}
    {{--<footer>--}}
        {{--<div class="container-fluid">--}}
            {{--<p class="copyright">Copyright &copy; 2017.Company name All rights reserved.</p>--}}
        {{--</div>--}}
    {{--</footer>--}}
</div>
<!-- END WRAPPER -->
<!-- Javascript -->
<script src="{{asset('home/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('home/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('home/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('home/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('home/vendor/chartist/js/chartist.min.js')}}"></script>
<script src="{{asset('home/js/klorofil-common.js')}}"></script>
<script type="text/javascript" src="{{asset('home/js/bootstrap-datetimepicker.js')}}" charset="UTF-8"></script>
<script type="text/javascript" src="{{asset('home/js/bootstrap-datetimepicker.zh-CN.js')}}" charset="UTF-8"></script>
<script type="text/javascript" src="{{asset('js/myself.js')}}" charset="UTF-8"></script>
@yield('js')
<script type="text/javascript" charset="utf-8" src="{{asset('js/myself.js')}}"> </script>
</body>

</html>
