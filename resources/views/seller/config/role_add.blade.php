@extends('seller.layouts.app')

@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">添加店员</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{$errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action="{{url('/config/role_add_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$info->id or 0}}" name="id">
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">店员职位</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="role_job"   placeholder="例如收银员、财务、服务员" value="{{old('role_job',$info->role_job)}}">
                            @if ($errors->has('role_job'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('role_job') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">店员编号（操作员）</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="role_name"   placeholder="店员编号" value="{{old('role_name',$info->role_name)}}">
                            @if ($errors->has('role_name'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('role_name') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">店员账号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mobile"   placeholder="店员手机号" value="{{old('mobile',$info->mobile)}}">
                            @if ($errors->has('mobile'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('mobile') }}</span>
                            @endif
                        </div>
                    </div>

                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">所属POS机</label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<select  name="terminalId" class="form-control">--}}
                                {{--<option value="0">--请选择POS机--</option>--}}
                                {{--@foreach($pos_list as $k=>$v)--}}
                                    {{--<option value="{{$v->terminalId}}"--}}
                                            {{--@if(!empty($info))--}}
                                                {{--@if ($info->terminalId==$v->terminalId)--}}
                                                    {{--selected--}}
                                                {{--@elseif(old('terminalId') == $v->terminalId)--}}
                                                    {{--selected--}}
                                                {{--@endif--}}
                                            {{--@endif--}}
                                    {{-->{{$v->terminalId}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            权限列表：
                        </div>
                    </div>


                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <?php
                        if((!empty($info->permission))&&isset($info->permission)){
                            $permission_arr=explode(',',$info->permission);
                        }else{
                            $permission_arr=[];
                        }
                    ?>
                    @foreach($permission_list as $k=>$v)
                        @if($v['pid'] != '0')
                        <div class="form-group">
                                <label for="signin-text" class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    {{$v['flg_name']}}
                                    <input style="float: right" type="checkbox" name="permission[]" value="{{$v['id']}}" @if(in_array($v['id'],$permission_arr)) checked @elseif(in_array($v['id'],old('permission'))) checked @endif>
                                </div>
                                {{--<div class="col-sm-2">--}}
                                    {{--<input type="checkbox" name="permission[]" value="{{$v['id']}}" @if(in_array($v['id'],$permission_arr)) checked @endif>--}}
                                {{--</div>--}}
                        </div>
                        <div class="clearfix"></div>
                        <div class="hr-line-dashed"></div>
                        @endif
                    @endforeach


                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 提交</button>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection