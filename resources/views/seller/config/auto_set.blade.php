@extends('seller.layouts.app')
<style>
    .title{
        color:black;
        font-size:16px;
    }
</style>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">货款自动提现设置</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <div class="col-sm-12">
                <div class="alert alert-warning"> 设置货款提现额度后,每次有消费者消费后您获得货款,如果您获得的货款达到提现货款,系统将直接将您获得货款打到您提交申请的银行卡账户中</div>
            </div>
            <form method="post"  action="{{url('/config/auto_set_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-12 control-label title">提现参数设置</label>--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">提现货款</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" value="{{$seller_info->withdraw_amount}}" name="withdraw_amount">
                            {{--<span class="help-block m-b-none text-success">关闭则所有人不能提现</span>--}}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 修改</button>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection