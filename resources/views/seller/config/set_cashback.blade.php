@extends('seller.layouts.app')
<style>
    .title{
        color:black;
        font-size:16px;
    }
</style>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">补贴参数设置</h3>
                <div class="hr-line-solid"></div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <div class="col-sm-12">
                <div class="alert alert-warning"> 如果设置自动挂单,则用户每笔消费补贴的积分将自动挂单去兑换CHQ</div>
            </div>
            <form method="post"  action="{{url('/config/cashback_submit')}}">
                <div class="panel-body">
                    {{csrf_field()}}
                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-12 control-label title">提现参数设置</label>--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">是否自动挂单</label>
                        <div class="col-sm-10">
                            <input type="radio" class="control-label" name="is_a_point" value="0" @if($data['is_a_point']==0) checked @endif/> 手动挂单
                            <input type="radio" class="control-label" name="is_a_point" value="1" @if($data['is_a_point']==1) checked @endif/> 自动挂单
                            {{--<span class="help-block m-b-none text-success">关闭则所有人不能提现</span>--}}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-edit"></i> 修改</button>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
@endsection