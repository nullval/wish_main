@extends('seller.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" xmlns="http://www.w3.org/1999/html"
      xmlns="http://www.w3.org/1999/html"/>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">积分增值</h3>
                <div class="hr-line-solid"></div>
                {{--<div class="alert alert-warning"> 说明：1.手动兑换操作:您的积分将直接根据当前成交最高价+{{$config_data['manual']}}自动兑换成TD，请到未特商城微信公众号查看TD详情;<br />--}}
                    {{--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.积分增值操作:您的积分将直接根据当前成交价自动兑换成TD，然后兑换成功的TD将自动按当前成交价+{{$config_data['bounds']}}转换成余额,详情请到<a href="{{url('finance/exchange_list')}}">积分增值列表</a>查看;<br />--}}
                    {{--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.手动兑换/积分增值:积分最少大于1个起可兑；一键操作后，账户可用积分总余额将全部使用。--}}
                {{--</div>--}}
                <div class="alert alert-warning"> 说明：积分增值:积分最少大于1个起可兑；一键操作后，账户可用积分总余额将全部使用,详情请到下面积分增值列表查看。
                </div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post" id="form"  action="{{url('/finance/exchange_submit')}}" class="form-horizontal"enctype='multipart/form-data'>
                <div class="panel-body">
                    {{csrf_field()}}
                    <input type="hidden" value="1" name="type">
                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">代付用户名 </label>--}}
                        {{--<div class="col-sm-10" >--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label"style="text-align: left">{{$info->mobile}}</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">账户可用积分 </label>
                        <div class="col-sm-10">
                            <label for="signin-text" class="col-sm-2 control-label" style="text-align: left">{{get_last_two_num($purse->balance-$purse->freeze_value)}}</label>
                            {{--<button type="button" class="btn btn btn-info submit2" style="float:right; margin-left: 10px"><i class="fa fa-paper-plane-o"></i> 积分增值</button>--}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">账户冻结积分 </label>
                        <div class="col-sm-10">
                            <label for="signin-text" class="col-sm-2 control-label" style="text-align: left">{{get_last_two_num($purse->freeze_value)}}</label>
                        </div>
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">开启自动挂单 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<label class="control-label" >--}}
                                {{--<input type="radio" class="control-label" name="is_automatic" value="1" @if($seller_info->is_automatic==1) checked @endif> 开启--}}
                                {{--<input type="radio" class="control-label" name="is_automatic" value="0" @if($seller_info->is_automatic==0) checked @endif> 关闭--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}




                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                        {{--<button type="button" class="btn btn btn-info submit1" style="float:right;"><i class="fa fa-paper-plane-o"></i> 手动兑换</button>--}}
                        {{--<a href="{{url('/finance/withdraw_list')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>--}}

                </div>
            </form>


            <div class="panel table-responsive">
                <div class="panel-heading">
                    <h3 class="panel-title">积分增值列表</h3>
                    <div class="right">
                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                        <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                    </div>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>订单编号</th>
                        <th>增值的积分</th>
                        <th>预计收益</th>
                        <th>实际收益</th>
                        <th>进度</th>
                        <th>增值时间</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($new_exchange_list as $k=>$v)
                        <tr>
                            <td>{{$v['orderNum']}}</td>
                            <td>{{get_last_two_num($v['useInte'])}}</td>
                            <td>{{get_last_two_num($v['getInte'])}}</td>
                            <td>{{get_last_two_num($v['getInte']*$v['schedules'])}}</td>
                            <td>{{$v['schedules']*100}}%</td>
                            <td>{{date('Y-m-d H:i:s',substr($v['orderTimes'],0,strlen($v['orderTime'])-3))}}</td>
                            {{--<td>--}}
                                {{--<a href="{{url('/finance/statement_finance_details')}}?id={{$v['id']}}"><span class="label label label-success"><i class="fa fa-eye"></i> 查看</span></a>--}}
                            {{--</td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    {{ $paginator
                    ->appends([
                        'orderType'=>isset($_GET['orderType'])?$_GET['orderType']:0,
                    ])
                    ->render() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $total }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        </div>
    </div>





    {{--遮罩层--}}
    <div id="mask" class="mask"></div>
    {{--<div class="col-md-12">--}}
        {{--<!-- RECENT PURCHASES -->--}}
        {{--<div class="panel table-responsive">--}}
            {{--<div class="panel-heading">--}}
                {{--<h3 class="panel-title">积分增值列表</h3>--}}
                {{--<div class="right">--}}
                    {{--<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>--}}
                    {{--<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<table class="table table-striped">--}}
                {{--<thead>--}}
                {{--<tr>--}}
                    {{--<th>订单编号</th>--}}
                    {{--<th>买卖类型</th>--}}
                    {{--<th>订单TD价格</th>--}}
                    {{--<th>订单个数</th>--}}
                    {{--<th>已撮合个数</th>--}}
                    {{--<th>撤销个数</th>--}}
                    {{--<th>订单状态</th>--}}
                    {{--<th>手续费</th>--}}
                    {{--<th>订单类型</th>--}}
                    {{--<th>下单时间</th>--}}
                {{--</tr>--}}
                {{--</thead>--}}
                {{--<tbody>--}}
                {{--@foreach($new_exchange_list as $k=>$v)--}}
                    {{--<tr>--}}
                        {{--<td>{{$v['orderNo']}}</td>--}}
                        {{--<td>--}}
                            {{--@if($v['bsFlag']==1)--}}
                                {{--积分兑换TD--}}
                            {{--@elseif($v['bsFlag']==2)--}}
                                {{--@if(isset($v['orderAttr'])&&$v['orderAttr']==9)--}}
                                    {{--TD转成收益余额--}}
                                {{--@else--}}
                                    {{--TD转让积分--}}
                                {{--@endif--}}
                            {{--@endif--}}
                        {{--</td>--}}
                        {{--<td>{{$v['orderPrice']/10000}}</td>--}}
                        {{--<td>{{$v['orderQty']/10000}}</td>--}}
                        {{--<td>{{$v['matchQty']/10000}}</td>--}}
                        {{--<td>{{$v['cancelQty']/10000}}</td>--}}
                        {{--<td>--}}
                            {{--@if($v['orderStatus']==1)--}}
                                {{--增值进行中--}}
                            {{--@elseif($v['orderStatus']==2)--}}
                                {{--部分增值成功--}}
                            {{--@elseif($v['orderStatus']==3)--}}
                                {{--交易完成--}}
                            {{--@elseif($v['orderStatus']==4)--}}
                                {{--关闭操作--}}
                            {{--@endif--}}
                        {{--</td>--}}
                        {{--<td>{{$v['feeRate']}}</td>--}}
                        {{--<td>--}}
                            {{--@if(isset($v['orderAttr'])&&$v['orderAttr']==9)--}}
                                {{--积分增值订单--}}
                            {{--@else--}}
                                {{--手动兑换订单--}}
                            {{--@endif--}}
                        {{--</td>--}}
                        {{--<td>{{date('Y-m-d H:i:s',substr($v['orderTime'],0,strlen($v['orderTime'])-3))}}</td>--}}
                        {{--<td>--}}
                        {{--<a href="{{url('/finance/statement_finance_details')}}?id={{$v['id']}}"><span class="label label label-success"><i class="fa fa-eye"></i> 查看</span></a>--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                {{--@endforeach--}}
                {{--</tbody>--}}
            {{--</table>--}}
            {{--</div>--}}
            {{--<div class="panel-footer">--}}
                {{--<div class="row">--}}
                    {{--{{ $paginator--}}
                    {{--->appends([--}}
                        {{--'orderType'=>isset($_GET['orderType'])?$_GET['orderType']:0,--}}
                    {{--])--}}
                    {{--->render() }}--}}
                    {{--<ul class="pagination" style="float: right">--}}
                        {{--<li>--}}
                            {{--<a href="#">总计条数:{{ $total }}</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- END RECENT PURCHASES -->--}}
    {{--</div>--}}
@endsection
@section('js')
    <script>
        $('.submit1').click(function () {
            $('input[name=type]').val(1);
            $('#form').submit();
        });
        $('.submit2').click(function () {
            $('input[name=type]').val(2);
            $('#form').submit();
        });
        $('#search').click(function(){
            var bank_name=$('#bank_name1').val();
            $.ajax({
                url:'{{url('/finance/getbankcode')}}',
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    bank_name:bank_name
                },
                timeout:5000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data){

                    if (data.code==0){
                        alert(data.message)
                    }else {
                        $('#bank_code').val(data.data.bank_code);
                        $('#bank_cname').text(data.data.bank_name);
                        $('#bank_name').val(data.data.bank_name);

                    }
                },
                error:function(xhr,textStatus){
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },

            })
        });
        $('input[name=is_automatic]').change(function () {
            var is_automatic=$(this).val();
            $.ajax({
                url:'{{url('/finance/change_automatic')}}',
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    is_automatic:is_automatic
                },
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data) {

                }
            })
        });


    </script>
@endsection