@extends('seller.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" xmlns="http://www.w3.org/1999/html"
      xmlns="http://www.w3.org/1999/html"/>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">货款提现所需信息</h3>
                <div class="hr-line-solid"></div>
                <div class="alert alert-warning"> 每次货款提现扣除3块手续费</div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/finance/sub_goods_withdraw')}}" class="form-horizontal"enctype='multipart/form-data'>
                <div class="panel-body">
                    {{csrf_field()}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">代付用户名 </label>--}}
                        {{--<div class="col-sm-10" >--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label"style="text-align: left">{{$info->mobile}}</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">账户可用余额 </label>
                        <div class="col-sm-10">
                            <label for="signin-text" class="col-sm-2 control-label" style="text-align: left">{{get_last_two_num($balance)}}</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">提现金额</br>( 以元为单位) </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="amount"   placeholder="提现金额" value="{{$info->amount or old('amount')}}">
                            @if ($errors->has('amount'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('amount') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">	开户银行 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="bankName" id="parentBankName" readonly  placeholder="开户银行" value="{{$bank_info->bankName or old('bankName')}}" >
                            @if ($errors->has('bankName'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('bankName') }}</span>
                            @endif
                        </div>
                    </div>

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">	银行代码 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="bank_code" id="bank_code" readonly  placeholder="银行代码" value="{{$bank_info->bank_code or old('bank_code')}}">--}}
                            {{--@if ($errors->has('bank_code'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('bank_code') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">	联行号 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="ibankno" readonly  placeholder="联行号" value="{{$bank_info->ibankno or old('ibankno')}}">--}}
                            {{--@if ($errors->has('ibankno'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('ibankno') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">	持卡人 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" readonly  placeholder="持卡人" value="{{$bank_info->name or old('name')}}">
                            @if ($errors->has('name'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">银行卡号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="bankaccountNo" readonly  placeholder="银行卡号" value="{{$bank_info->bankaccountNo or old('bankaccountNo')}}">
                            @if ($errors->has('bankaccountNo'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('bankaccountNo') }}</span>
                            @endif
                        </div>
                    </div>



                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                        <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                        <a href="{{url('/finance/withdraw_goods_list')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>

@endsection
@section('js')
    <script>
        $('#search').click(function(){
            var bank_name=$('#bank_name1').val();
            $.ajax({
                url:'{{url('/finance/getbankcode')}}',
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    bank_name:bank_name
                },
                timeout:5000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data){

                    if (data.code==0){
                        alert(data.message)
                    }else {
                        $('#bank_code').val(data.data.bank_code);
                        $('#bank_cname').text(data.data.bank_name);
                        $('#bank_name').val(data.data.bank_name);

                    }
                },
                error:function(xhr,textStatus){
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },

            })
        });


    </script>
@endsection