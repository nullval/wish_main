@extends('seller.layouts.app')

@section('content')
    {{--<div class="col-md-12">--}}
        {{--<div class="panel ">--}}
            {{--<div class="panel-heading">--}}
                {{--<h3 class="panel-title">补贴管理</h3>--}}
                {{--<div class="right">--}}
                    {{--<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>--}}
                    {{--<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="panel-body no-padding" style="margin-bottom: 20px;">--}}
                {{--<div class="col-sm-12">--}}
                    {{--<span class="form-control">商户手机号 : {{$seller_info->mobile}} </span>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">每日补贴列表</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding">--}}
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="year">--}}
                        {{--<option value="2017">2017</option>--}}
                        {{--<option value="2018" @if(isset($_GET['year'])&&$_GET['year']==2018) selected @endif>2018</option>--}}
                        {{--<option value="2019" @if(isset($_GET['year'])&&$_GET['year']==2019) selected @endif>2019</option>--}}
                        {{--<option value="2020" @if(isset($_GET['year'])&&$_GET['year']==2020) selected @endif>2020</option>--}}
                    {{--</select>--}}
                {{--</div>--}}

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>日期</th>
                        <th>补贴总额度</th>
                        {{--<th>类型</th>--}}
                        {{--<th>每天补贴额度</th>--}}
                        {{--<th>已补贴额度</th>--}}
                        {{--<th>已补贴天数</th>--}}
                        {{--<th>处理状态</th>--}}
                        {{--<th>创建时间</th>--}}
                        {{--<th>更新时间</th>--}}
                        <th>查看详情</th>
                        {{--<th>查看补贴记录</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($list as $k=>$v)
                        <tr>
                            <td>{{$v->datetime}}</td>
                            <td>{{get_last_two_num($v->amount)}}</td>
                            {{--<td>@if($v->reason==10021) 积分--}}
                                {{--@elseif($v->reason==10022) 收益--}}
                                {{--@endif--}}
                            {{--</td>--}}
                            {{--<td>{{$v['day_amount'] or '暂无'}}</td>--}}
                            {{--<td>{{$v['finish_amount'] or '暂无'}}</td>--}}
                            {{--<td>{{$v['finish_day'] or '暂无'}}</td>--}}
                            {{--<td>--}}
                                {{--@if($v['status']==1)--}}
                                    {{--处理中--}}
                                {{--@elseif($v['status']==2)--}}
                                    {{--处理完毕--}}
                                {{--@elseif($v['status']==3)--}}
                                    {{--作废--}}
                                {{--@endif--}}
                            {{--</td>--}}
                            {{--<td>{{$v->created_at->format('Y-m-d')}}</td>--}}
                            {{--<td>{{$v['updated_at'] or '暂无'}}</td>--}}
                            <td>
                                <a href="{{url('/finance/new_statement_details')}}?date={{$v->datetime}}"><span class="label label label-success"><i class="fa fa-eye"></i> 查看</span></a>
                            </td>
                            {{--<td>--}}
                                {{--<a href="{{url('/finance/statement_finance_details')}}?id={{$v['id']}}"><span class="label label label-success"><i class="fa fa-eye"></i> 查看</span></a>--}}
                            {{--</td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            {{--</div>--}}
            <div class="panel-footer">
                <div class="row">
                    {{ $list->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $list->total()}}</a>
                        </li>
                    </ul>
                    {{--<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>--}}
                    {{--<div class="col-md-6 text-right"><a href="#" class="btn btn-primary">View All Purchases</a></div>--}}
                </div>
                {{--'year'=>isset($_GET['year'])?$_GET['year']:date('Y')--}}
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    {{--<script>--}}
        {{--$(function(){--}}
            {{--$('#status').change(function(){--}}
                {{--location.href="{{url('/seller/user_finance')}}?id={{$_GET['id']}}&type={{$_GET['type']}}&status="+$(this).val()+'&year={{$_GET['year']}}';--}}
            {{--});--}}
            {{--$('#type').change(function(){--}}
                {{--location.href="{{url('/seller/user_finance')}}?id={{$_GET['id']}}&type="+$(this).val()+"&status={{$_GET['status']}}&year={{$_GET['year']}}";--}}
            {{--});--}}
            {{--$('#year').change(function(){--}}
                {{--location.href="{{url('/seller/user_finance')}}?id={{$_GET['id']}}&type={{$_GET['type']}}&status={{$_GET['status']}}&year="+$(this).val();--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
@endsection