@extends('seller.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">补贴详情</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding" style="margin-bottom: 20px;">--}}
                <div class="col-sm-6">
                    <span class="form-control">日期: {{$_GET['date']}} </span>
                    <span class="form-control">当日获得总收益: {{$data['amount']}} </span>
                    <span class="form-control">当日获得总积分: {{$data['point']}} </span>
                    {{--<input type="hidden" value="{{$statement_info->id}}" id="id"/>--}}
                    {{--<input type="hidden" value="{{$_GET['year']}}" id="year"/>--}}
                </div>
            {{--<div class="panel-body no-padding">--}}

                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-12">--}}
                    {{--<div class="alert alert-warning"> 订单只有已支付且有效,机主才能将自己的未绑定商家的未特商城pos机绑定给其他机主</div>--}}
                {{--</div>--}}
                <div class="col-sm-6" >
                    {{--<h2 class="panel-title"style="padding: 10px;font-size: 18px">归属订单列表</h2>--}}

                </div>
                <table class="table table-striped">

                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>变动额度</th>
                        <th>收益类型</th>
                        <th>备注</th>
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($list as $k=>$v)
                            <tr>
                                <td>{{$k+1}}</td>
                                <td style="color: red">+{{get_last_two_num($v->into_amount)}}</td>
                                <td>@if($v->reason==10021) 积分
                                    @elseif($v->reason==10022) 收益
                                    @endif
                                </td>
                                <td>{{$v->detail}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>

            {{--</div>--}}

            <div class="panel-footer">
                <div class="row">
                    {{ $list->appends([
                    'date'=>isset($_GET['date'])?$_GET['date']:'',
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $list->total()}}</a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <a href="{{url('/finance/new_statement_list')}}" class="btn btn-primary" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>
                </div>
            </div>
        {{--</div>--}}
        <!-- END RECENT PURCHASES -->
    </div>

@endsection