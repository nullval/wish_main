@extends('seller.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">补贴记录详情</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body no-padding" style="margin-bottom: 20px;">
                <div class="col-sm-6">
                    <span class="form-control">对账单编号 {{$statement_info['id']}} </span>
                    {{--<input type="hidden" value="{{$statement_info->id}}" id="id"/>--}}
                    {{--<input type="hidden" value="{{$_GET['year']}}" id="year"/>--}}
                </div>
                {{--<div class="col-sm-6">--}}
                    {{--<span class="form-control">归属商家 {{$seller_info['mobile'] or '暂无'}} </span>--}}
                    {{--<input type="hidden" value="{{$_GET['mobile']}}" id="mobile"/>--}}
                {{--</div>--}}
                {{--<div class="col-sm-6">--}}
                    {{--<span class="form-control">对账单状态--}}
                        {{--@if($statement_info['status']==1)--}}
                            {{--<span class="text-primary">处理中</span>--}}
                        {{--@elseif($statement_info['status']==2)--}}
                            {{--<span class="text-success">处理完毕</span>--}}
                        {{--@elseif($statement_info['status']==3)--}}
                            {{--<span class="text-danger">作废</span>--}}
                        {{--@endif--}}
                    {{--</span>--}}
                {{--</div>--}}
                <div class="col-sm-6">
                    <span class="form-control">补贴总额度 {{$statement_info['amount'] or '暂无'}} </span>
                </div>
                {{--<div class="col-sm-6">--}}
                    {{--<span class="form-control">每天补贴额度 {{$statement_info['day_amount'] or '暂无'}} </span>--}}
                {{--</div>--}}
                {{--<div class="col-sm-6">--}}
                    {{--<span class="form-control">已返天数 {{$statement_info['finish_day'] or '暂无'}} </span>--}}
                {{--</div>--}}
                <div class="col-sm-6">
                    <span class="form-control">已补贴额度 {{$statement_info['finish_amount'] or '暂无'}} </span>
                </div>
                {{--                <div class="col-sm-6">--}}
                                    {{--<span class="form-control">订单是否有效--}}
                                        {{--@if($statement_info['']is_true==1)--}}
                                            {{--<span class="text-success">有效</span>--}}
                                        {{--@elseif($statement_info['']is_true==2)--}}
                                            {{--<span class="text-danger">无效</span>--}}
                                        {{--@endif--}}
                                    {{--</span>--}}
                                {{--</div>--}}
                {{--<div class="col-sm-6">--}}
                    {{--<span class="form-control">创建时间 {{$statement_info['created_at'] or '暂无'}} </span>--}}
                {{--</div>--}}
                {{--<div class="col-sm-6">--}}
                    {{--<span class="form-control">更新时间 {{$statement_info['updated_at'] or '暂无'}} </span>--}}
                {{--</div>--}}
                {{--<div class="col-sm-6">--}}
                {{--<span class="form-control">收货地址 {{$statement_info->contact_address}} </span>--}}
                {{--</div>--}}
            </div>

            <div class="panel-body no-padding">

                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-12">--}}
                    {{--<div class="alert alert-warning"> 订单只有已支付且有效,机主才能将自己的未绑定商家的未特商城pos机绑定给其他机主</div>--}}
                {{--</div>--}}
                <div class="col-sm-6" >
                    <h2 class="panel-title"style="padding: 10px;font-size: 18px">补贴记录</h2>

                </div>
                <table class="table table-striped">

                    <thead>
                    <tr>
                        {{--<th>编号</th>--}}
                        <th>补贴额度</th>
                        <th>类型</th>
                        <th>补贴时间</th>
                        {{--<th>备注</th>--}}
                        {{--<th>刷卡扣费</th>--}}
                        {{--<th>创建时间</th>--}}
                        {{--<th>绑定的未特商城pos机</th>--}}
                        {{--<th>绑定</th>--}}
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($order_list as $k=>$v)
                            <tr>
                                {{--<td>{{$k+1}}</td>--}}
                                <td style="color: red">+{{get_last_two_num($v->into_amount)}}</td>
                                <td>@if($v->reason==10021) 积分
                                @elseif($v->reason==10022) 收益
                                @endif
                                </td>
                                {{--<td>{{$v->poundage or 0}}</td>--}}
                                <td>{{explode(' ',$v->time)[0]}}</td>
                                {{--<td>{{$v->detail}}</td>--}}
                                {{--<td>{{$v->created_at}}</td>--}}
                                {{--<td>--}}
                                    {{--@if(empty($v->terminalId))--}}
                                            {{--<select class="terminalId" >--}}
                                                {{--<option value="0">-----</option>--}}
                                                {{--@foreach($pos_list as $k1=>$v1)--}}
                                                    {{--<option value="{{$v1['terminalId']}}">{{$v1['terminalId']}}</option>--}}
                                                {{--@endforeach--}}
                                            {{--</select>--}}
                                        {{--@else--}}
                                            {{--{{$v->terminalId or '暂无'}}--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                    {{--只有订单已支付且订单有效才能绑定--}}
                                    {{--@if($statement_info->is_true==1&&$statement_info->status==2)--}}
                                        {{--@if(empty($v->terminalId))--}}
                                            {{--<a href="#" class="bind">--}}
                                                {{--<span class="label label-warning"><i class="fa fa-edit"></i> 绑定</span>--}}
                                            {{--</a>--}}
                                        {{--@endif--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>

            </div>

            <div class="panel-footer">
                <div class="row">
                    {{ $order_list->appends([
                    'id'=>isset($_GET['id'])?$_GET['id']:0
                    ])->links() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $order_list->total()}}</a>
                        </li>
                    </ul>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    <a href="{{url('/finance/statement_list')}}" class="btn btn-primary" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
@endsection