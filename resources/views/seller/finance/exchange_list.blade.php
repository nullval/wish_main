@extends('seller.layouts.app')

@section('content')
    {{--<div class="col-md-12">--}}
        {{--<div class="panel ">--}}
            {{--<div class="panel-heading">--}}
                {{--<h3 class="panel-title">补贴对账单管理</h3>--}}
                {{--<div class="right">--}}
                    {{--<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>--}}
                    {{--<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="panel-body no-padding" style="margin-bottom: 20px;">--}}
                {{--<div class="col-sm-12">--}}
                    {{--<span class="form-control">商户手机号 : {{$seller_info->mobile}} </span>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">积分增值列表</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="col-sm-12">--}}
                {{--<div class="alert alert-warning"> 234</div>--}}
            {{--</div>--}}
            {{--<div class="panel-body no-padding">--}}
                {{--<div class="col-sm-12">--}}
                    {{--<div class="alert alert-warning"> 商家每天产生所有的订单会生成对账单,平台会根据对账单每日对商家进行补贴</div>--}}
                    {{--@if(Session::has('status'))--}}
                        {{--<div class="alert alert-info"> {{Session::get('status')}}</div>--}}
                    {{--@endif--}}
                {{--</div>--}}
                {{--<div class="col-sm-2">--}}
                    {{--<select class="form-control" id="year">--}}
                        {{--<option value="2017">2017</option>--}}
                        {{--<option value="2018" @if(isset($_GET['year'])&&$_GET['year']==2018) selected @endif>2018</option>--}}
                        {{--<option value="2019" @if(isset($_GET['year'])&&$_GET['year']==2019) selected @endif>2019</option>--}}
                        {{--<option value="2020" @if(isset($_GET['year'])&&$_GET['year']==2020) selected @endif>2020</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
            {{--<div class="col-sm-2">--}}
                {{--<select class="form-control" id="orderType">--}}
                    {{--<option value="0" @if(!isset($_GET['orderType'])||$_GET['orderType']==0) selected @endif>全部</option>--}}
                    {{--<option value="2" @if(isset($_GET['orderType'])&&$_GET['orderType']==2) selected @endif>积分增值</option>--}}
                    {{--<option value="1" @if(isset($_GET['orderType'])&&$_GET['orderType']==1) selected @endif>手动兑换</option>--}}
                {{--</select>--}}
            {{--</div>--}}
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>订单编号</th>
                        <th>买卖类型</th>
                        <th>订单TD价格</th>
                        <th>订单个数</th>
                        <th>已撮合个数</th>
                        {{--<th>撤销个数</th>--}}
                        <th>订单状态</th>
                        <th>手续费</th>
                        <th>下单时间</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($new_exchange_list as $k=>$v)
                        <tr>
                            <td>{{$v['orderNo']}}</td>
                            <td>
                                @if($v['bsFlag']==1)
                                    积分兑换TD
                                @elseif($v['bsFlag']==2)
                                    @if(isset($v['orderAttr'])&&$v['orderAttr']==9)
                                        TD转成收益余额
                                    @else
                                        TD转让积分
                                    @endif
                                @endif
                            </td>
                            <td>{{$v['orderPrice']/10000}}</td>
                            <td>{{$v['orderQty']/10000}}</td>
                            <td>{{$v['matchQty']/10000}}</td>
                            {{--<td>{{$v['cancelQty']/10000}}</td>--}}
                            <td>
                                @if($v['orderStatus']==1)
                                    增值进行中
                                @elseif($v['orderStatus']==2)
                                    部分增值成功
                                @elseif($v['orderStatus']==3)
                                    交易完成
                                @elseif($v['orderStatus']==4)
                                    关闭操作
                                @endif
                            </td>
                            <td>{{$v['feeRate']}}</td>
                            <td>{{date('Y-m-d H:i:s',substr($v['orderTime'],0,strlen($v['orderTime'])-3))}}</td>
                            {{--<td>--}}
                                {{--<a href="{{url('/finance/statement_finance_details')}}?id={{$v['id']}}"><span class="label label label-success"><i class="fa fa-eye"></i> 查看</span></a>--}}
                            {{--</td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            {{--</div>--}}
            <div class="panel-footer">
                <div class="row">
                    {{ $paginator
                    ->appends([
                        'orderType'=>isset($_GET['orderType'])?$_GET['orderType']:0,
                    ])
                    ->render() }}
                    <ul class="pagination" style="float: right">
                        <li>
                            <a href="#">总计条数:{{ $total }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    <script>
        $()
    </script>
    <script>
        $(function(){
            $('#orderType').change(function(){
                location_a();
            });
            function location_a(){
                location.href="{{url('/finance/exchange_list')}}?orderType="+$('#orderType').val();
            }
        });
    </script>
@endsection