@extends('seller.layouts.app')
<link href="{{asset('image_frame/css/css.css')}}" type="text/css" rel="stylesheet" xmlns="http://www.w3.org/1999/html"
      xmlns="http://www.w3.org/1999/html"/>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">提现所需信息</h3>
                <div class="hr-line-solid"></div>
                {{--@if(!isset($_GET['pursetype'])||$_GET['pursetype']==1)--}}
                    <div class="alert alert-warning"> 每次收益提现扣除3元,提现额度不能少于100,提现时间为早上10点到下午4点</div>
                {{--@elseif($_GET['pursetype']==8||$_GET['pursetype']==9||$_GET['pursetype']==4)--}}
                    {{--<div class="alert alert-warning"> 根据支付公司要求货款提现时间为早上9点到晚上12点,提现额度不能少于20<br />--}}
                        {{--1.工作日：早上9点到下午4点，提现2小时之内到账;<br />--}}
                        {{--下午4点到晚上12点，提现T+1到账;<br />--}}
                        {{--工作日不限制提现额度;<br />--}}
                        {{--2.周末:提现每笔不超过5万，当天提现额度最高20万;<br />--}}
                    {{--</div>--}}
                {{--@elseif($_GET['pursetype']==7)--}}
                    {{--<div class="alert alert-warning">商家充值钱包提现将在T+2到达--}}
                    {{--</div>--}}
                {{--@endif--}}
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"   action="{{url('/finance/subwithdraw')}}" class="form-horizontal"enctype='multipart/form-data'>
                <div class="panel-body">
                    {{csrf_field()}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">代付用户名 </label>--}}
                        {{--<div class="col-sm-10" >--}}
                            {{--<label for="signin-text" class="col-sm-2 control-label"style="text-align: left">{{$info->mobile}}</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<div class="hr-line-dashed"></div>--}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">提现类型  </label>
                        <div class="col-sm-10">
                            <select class="form-control" id="pursetype" name="pursetype">
                                <option value="1" @if(!isset($_GET['pursetype'])||$_GET['pursetype']==1) selected @endif>收益钱包</option>
{{--                                <option value="3" @if(isset($_GET['pursetype'])&&$_GET['pursetype']==3) selected @endif>积分钱包</option>--}}
                                <option value="4" @if(isset($_GET['pursetype'])&&$_GET['pursetype']==4) selected @endif>货款钱包</option>
                                <option value="7" @if(isset($_GET['pursetype'])&&$_GET['pursetype']==7) selected @endif>商家充值钱包</option>
                                {{--<option value="8" @if(isset($_GET['pursetype'])&&$_GET['pursetype']==8) selected @endif>微信钱包</option>--}}
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">账户可用余额 </label>
                        <div class="col-sm-10">
                            <label for="signin-text" class="col-sm-2 control-label" style="text-align: left">{{get_last_two_num($purse->balance-$purse->freeze_value)}}</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">提现金额</br>( 以元为单位) </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="amount"   placeholder="提现金额" value="{{$info->amount or old('amount')}}">
                            @if ($errors->has('amount'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('amount') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">	身份证 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="identity"   placeholder="身份证"  readonly value="{{$bank_info->identity or old('identity')}}">
                            @if ($errors->has('identity'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('identity') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">	所在银行预留手机号码 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="phone"   placeholder="所在银行预留手机号码"  readonly value="{{$bank_info->phone or old('phone')}}">
                            @if ($errors->has('phone'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">	开户银行 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="bank_name" id="parentBankName" readonly  placeholder="开户银行" value="{{$bank_info->account_bank or old('parentBankName')}}" >
                            @if ($errors->has('parentBankName'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('parentBankName') }}</span>
                            @endif
                        </div>
                    </div>

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">	银行代码 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="bank_code" id="bank_code" readonly  placeholder="银行代码" value="{{$bank_info->bank_code or old('bank_code')}}">--}}
                            {{--@if ($errors->has('bank_code'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('bank_code') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label for="signin-text" class="col-sm-2 control-label">	联行号 </label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="text" class="form-control" name="ibankno" readonly  placeholder="联行号" value="{{$bank_info->ibankno or old('ibankno')}}">--}}
                            {{--@if ($errors->has('ibankno'))--}}
                                {{--<span class="help-block m-b-none text-danger">{{ $errors->first('ibankno') }}</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">	持卡人 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="account_name" readonly  placeholder="持卡人" value="{{$bank_info->account_name or old('account_name')}}">
                            @if ($errors->has('account_name'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('account_name') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label">银行卡号 </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="bank_account" readonly  placeholder="银行卡号" value="{{$bank_info->bank_account or old('bank_account')}}">
                            @if ($errors->has('bank_account'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('bank_account') }}</span>
                            @endif
                        </div>
                    </div>



                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                        <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                        <a href="{{url('/finance/withdraw_list')}}" class="btn btn-success" style="float:right;margin-right: 10px;"><i class="fa fa-angle-double-left"></i> 返回</a>

                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask"></div>

@endsection
@section('js')
    <script>
        $('#search').click(function(){
            var bank_name=$('#bank_name1').val();
            $.ajax({
                url:'{{url('/finance/getbankcode')}}',
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    bank_name:bank_name
                },
                timeout:5000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data){

                    if (data.code==0){
                        alert(data.message)
                    }else {
                        $('#bank_code').val(data.data.bank_code);
                        $('#bank_cname').text(data.data.bank_name);
                        $('#bank_name').val(data.data.bank_name);

                    }
                },
                error:function(xhr,textStatus){
                    console.log('错误')
                    console.log(xhr)
                    console.log(textStatus)
                },

            })
        });
        $('#pursetype').change(function () {
            $pursetype=$(this).val();
            location.href="{{url('/finance/withdraw')}}?pursetype="+$pursetype;
        });


    </script>
@endsection