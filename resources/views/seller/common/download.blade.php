<html>
<head>
    <meta charset="UTF-8">
    {{--<meta name='apple-itunes-app' content='app-id=1398228674'>--}}
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>未特商城</title>
    <style>
        /* CSS Document */
        @media (min-width: 350px) {
            .ad {
                background-size: 130px;
                width: 130px;
                height: 100px;
                position: absolute;
                right: 0;
            }
        }

        * {
            margin: 0;
            padding: 0;
        }

        li ul img {
            margin: 0px;
            padding: 0px;
        }

        ul {
            list-style-type: none;
        }

        body {
            margin: 0px;
            padding: 0px;
        }

        p, div {
            cursor: default;
        }

        .bg {
            background-repeat: no-repeat;
            background-position: center;
        }

        .container {
            max-width: 414px;
            width: 100%;
            margin: 0px auto;
        }

        .wrap {
            margin: 0px auto;
        }

        .box {
            padding-top: 50px;
        }

        .con_phone {
            width: 60%;
        }

        .tit_left {
            display: inline-block;
            vertical-align: middle;
            width: 20px;
        }

        .tit_right {
            display: inline-block;
            vertical-align: middle;
            font-size: 16px;
        }

        .text {
            font-size: 12px;
            color: #999999;
            margin-left: 10px;
        }

        .tit {
            margin-top: 10%;
        }

        .down {
            text-align: center;
            border: 1px solid #777777;
            width: 50%;
            height: 30px;
            line-height: 30px;
            border-radius: 5px;
        }

        .logo {
            width: 70%;
            margin-top: 15px;
        }

        .introduction {
            font-size: 10px;
            width: 90%;
            text-align: justify;
            margin-top: 10px;
        }

        .footer {
            color: #FFF;
            background-color: rgba(0, 113, 188, 0.7);
            height: 30px;
            margin: 0px auto;
            text-align: center;
            position: fixed;
            bottom: 0;
            width: 100%;
            vertical-align: middle;
            line-height: 30px;
        }

        .panel_wrap_aside * {
            padding: 0;
            margin: 0;
        }

        .wrap_aside {
            width: 85%;
            max-width: 300px;
            background-color: #f7f6f6;
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            z-index: 100;
            transform: translateX(-100%);
            -ms-transform: translateX(-100%);
            -moz-transform: translateX(-100%);
            -webkit-transform: translateX(-100%);
            -o-transform: translateX(-100%);
            transition: 0.3s;
            -ms-transition: 0.3s;
            -moz-transition: 0.3s;
            -webkit-transition: 0.3s;
            -o-transition: 0.3s;
        }

        .wrap_aside_bg {
            width: 100%;
            background-color: rgba(0, 0, 0, 0.5);
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            z-index: 98;
            opacity: 0;
            display: none;
            transition: 0.3s;
            -ms-transition: 0.3s;
            -moz-transition: 0.3s;
            -webkit-transition: 0.3s;
            -o-transition: 0.3s;
        }

        .panel_wrap_aside.show .wrap_aside {
            transform: translateX(0);
            -ms-transform: translateX(0);
            -moz-transform: translateX(0);
            -webkit-transform: translateX(0);
            -o-transform: translateX(0);
            transition: 0.4s;
            -ms-transition: 0.4s;
            -moz-transition: 0.4s;
            -webkit-transition: 0.4s;
            -o-transition: 0.4s;
        }

        .panel_wrap_aside.show .wrap_aside_bg {
            display: block;
            opacity: 1;
            transition: 0.3s;
            -ms-transition: 0.3s;
            -moz-transition: 0.3s;
            -webkit-transition: 0.3s;
            -o-transition: 0.3s;
        }

        .wrap_aside .topPart_li {
            display: flex;
            justify-content: space-around;
            align-items: center;
            margin-bottom: 35px;
        }

        .wrap_aside .topPart_li .left_li {
            width: 45px;
            height: 40px;
            line-height: 40px;
            color: #999;
            display: block;
            text-decoration: none;
        }

        .wrap_aside .topPart_li .mid_li {
            width: 160px;
            height: 40px;
            font-size: 17px;
            text-align: center;
            line-height: 40px;
            color: #323232;
        }

        .wrap_aside .topPart_li .right_li {
            width: 25px;
            height: 40px;
            line-height: 40px;
        }

        .wrap_aside .midPart_li {
            height: 100px;
            display: flex;
            justify-content: space-around;
            align-items: center;
            margin-bottom: 20px;
        }

        .wrap_aside .midPart_li .item {
            padding: 10px 1px;
            display: block;
            text-decoration: none;
        }

        .wrap_aside .midPart_li div:active {
            background-color: #eaeaea;
        }

        .wrap_aside .midPart_li .left_li {
            width: 80px;
            font-size: 15px;
            text-align: center;

        }

        .wrap_aside .midPart_li .mid_li {
            width: 80px;
            font-size: 16px;
            text-align: center;
            position: relative;

        }

        .wrap_aside .midPart_li .mid_li .freeYuyue {
            position: absolute;
            top: -12%;
            left: 62%;
        }

        .wrap_aside .midPart_li .right_li {
            width: 80px;
            font-size: 16px;
            text-align: center;
        }

        .wrap_aside .downLoadApp_li {
            display: block;
            height: 44px;
            background-color: #15B8FF;
            color: white;
            font-size: 14px;
            text-align: center;
            line-height: 42px;
            text-decoration: none;
        }

        .wrap_aside .vip_li {
            display: block;
            height: 42px;
            background-color: #F6B900;
            color: white;
            font-size: 14px;
            text-align: center;
            line-height: 42px;
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHoAAABUCAYAAABN/yOiAAAAAXNSR0IArs4c6QAADtFJREFUeAHtnQeYU1UWx/+vJBmKYgEEP0T4aBbEsoIKispiY/EDsQKLiiu6qyuCoIK6tmUXUVGwUvzYxYLKsiJ8SrUgYBdFUQR7QXBFwAJCyntvzznJy2SSDJNyM5Nk3v2+mddLzi/n3nPPvedE29ny8p6w7AWOg0Yo86Jp2AFD79Ng09TlZf5RUz6exns82ClyKbsdAtqDXXZcUz5QHLQHO0U2ZbWjCmgPdlmxrfJhUkB7sKvIp2w20oL2YJcN3/gHqRa0Bzsuo7JY2S1oD3ZZMJYPUSNoD3Z5wNYz+RjiSSKPkniWMrmgDs/RjmgD7ai2Ob+BeAjJUyhOpJzvUnwXZqTR7msXuweNIQfmjJDXDZ43Gc67X7qvnvWy3NylGWm0K6Vi1mwXsr1+E+wPv0Vg9tWeZrvgaJkVaL6uGGEnQg6dPxmhQfd7sBMg82pWVXfitcVSjSdDxvZd0dfcpzEqlt8KVJgInng7nO+2Jr5+VuvlUI1nrdGuhIpBs9NC3qMBzOGno2IFQfYZiNy7ANi3MbTDWruvnvWyHAy0nDXalVZdaXYyZK19C5gX94TRvyucTT8hMvUFWE+/Dq1DCzLQRsrrBs++F86ab9xXz3pZypqdN2iWVm3DToasH0nW9n+vgbVyHSIPL4W9dI1A1Lq0FsjONz/CoSpdP6QV6ivsnKvuRHWozWo8GTK3yTr1m21qg0MD7kmF/O0WgSsG2toN9IUYWS+rcSWgGXptwE4HmZ8toJev41UpriZjzwZAwwAZZD7gt1DUGq+nsJWBZgkXEnYi5PCwaXCta61jS+i9O8NevrYKZK6ugz1uAcIWAs+OBvZrUq9hKwVdKNhVIF8+HX4Cp/fqTN8sP3x3DYbz1WZY81YhrslkeYeGTYfz+f8QHDAxFfafppJFbsL/6BVimcs3JId/pWSNKzHG0slIlYGmtdsPgcVjwR4vdoZw8U+9FMYpXeDsCBJsH0Ls7vxpR9TwojYZjSsAOhY8+x5g2w6g6R4IPDNKoIYG3QffxCHQj2yL0MD7YL/xabrXz2pfKVjjBQPNklIC29BhjuyDyJQX4tU1SLLG2d2g9+gEa/YbBHxX3Lpmq1prFBCt5+o9GbbWfj8gGFEG2f1GFDvsgoJWBtuVZpqlW11Lm0yQ8fNvcpZ/xp9h9D1KXKECe1cY/llXQaeBD1WanPw6xQxbeRud/OELaqAl9JNZk13Ierd2Ajl05QypxrmP7X/ir9VD5qp9wRhov2ub/PpZbRdzm11wjXYlpaQad29GS/OG/jCHngTn683ST3Yh8yn+uaOgNfQjeNp4aPvvLdW41mzP9JrMkPl88qzhN2rXz5sEZ1Xuw5v8/GLU7IJrNH9wLko1m4wt47TDoTVpCPuj76Bxf9ktBE4/rgMiDy6RPc7GbQj1v5sMuUmphpcLea+GCJ58O3XR1tHw5oiy1Oxa02iXgzLNZoPsguPgu76f9JHt599F+O7nyJpuA//ki2F//B2cH38Vq9t66zNY0150XyG6TITcf6J0xWAa8E+/DHrPg8pOs2tNo10pK9NsahCtJ1/Drq43IDxyJrR2VPUGfNBplMqm/rM1752olb5PI2gt93IfXz1kPmLQ956qfAauUR8931JMbXata7QrPGWa7d4wtvRRH1vbuzH1rSclHYltxjRZ77Q/7FVfIHgOGXHcHw+wA+VK6Md2QGgwTVxYuT799TnsLYY2u9Y12pWTMs12bxhbak0o+ndnKGlvbDOxur7oIWgHNIX/6atlvDoRskajXFqnlunvkcPeYtDsOgPN8ioEbGfb9tSqmh+WCJnaZHvhagTPuht6m+aoeGd8pSa/9TnMc48VT1o5wa5T0IWA7ZALVLpKfHO3JEFmHzgXdrLYn26i4S+tsroORaQ6Z2ud3aYpsMlTp7Xax71zVsu61Ow6B82SUqnZ1oLVCI+YKZ1ZoRCDzG1y+Mano9Y1H3DbZBrLTmmTybvGbXcKbILsnzpMnCsgN2supa5gFwVoFpgq2M7qr2DNX0Xq6oj1zNOINOonWzR44btzMLRDW1VCTjS8CGLFB3fC6Hd0lF8y7M6tBLJ+5lEIj58XNeByIU3X1AXsOrO6q5ORamuc55DZNE+MtVPcoGRo2es3Rt2hCda1Rv3visU3INh3Amxqp+OFnDLyZel8gFTx4RGPUrfu1fjhfFZq0xovGo12BaZKs937Wc++Ha2uyRIPXTKF/KNmCmQ+1ziuI5xgGPbqr91Lo0saAXM2bIVGGo9fdsJ+94uqx/PYqk3NLjrQLDfVsF0W5pATiKie2ibTCfrvaZYKh/CQMRYv3CZPGwa9zxEIcftOExzEQGO/uKJSW7CLEjTLsBCwIw8tRbDPHanOEGrD2T9uL3q/Ep8LmYY6pbqe/qIYaNaSD+D88HPleQrWagN20bXRyXJT3WYn35+3uQvlG9Mf4dvmiNay1osmu5CzaZMpWABbtqd7TI37CtlmFz1olk5twI5TyAVyi71gXtAdxuDj4Xz5Q/Xu1/hDql8pFOySAM1iUQ1bp2FOe3FCVc0PyRQyDZLoHVpC69gCxkmHSvuOiAVrwXuwHl8Je8U6vlvOpRCwSwY0S00ZbJr6W7F6AqxHXkL4b7OjQGqCTNL3P06DHmSdazz5MFZs8sRFHl4Ca86bwE/RaUzusXyWqmGXFGgWnCrYxvk0lj35IkTuX0x/i+C/90Lou2mTtQObRuO6tvwKbd89YI7pR7NbaO44+ctlpinNSUsuGjlZtMYNUic8JJ9YzbZK2CUHmmWiGjbIicb+7kycIRK0N3e0xHIJZOqOBRaORWTyQlhPrIwjYw8cz1WzP9qAEE87zrGogl2SoFlmqmBzcIBJ2h156jU473+9WxwpkClqk4tvwiAYF5+I8DWPCWzW5MCca8QwC55L4+LkdMmnqIBdsqBZcKpgZwKhOsjutS5sbgrMC0+IRonEILM1DouMtaded0/Pepkv7KJ1mGQiiUI4VdI9tybIfE34+lmwX/kYvuGnw9n8S3TmSkyTdZpGbPTrmu7WGe/L16lS0qBZSoWGnQlk9p/7/nE+dbUOAY9v83g4D6a4xaGwIK35nu5mzst8YJc8aJZaoWBnApm9aoGFY2D88QSERj0GjuB0yADz3TNEHChCleLCeF65ipIr7LIAzQJUDbsmyFqbZvA9MBSBV26REbHgaf+E9dgKmLeeI2Pe9uufxGFbK9YjMmMZv6aSkgvskjbG0klNlYHmG3ce9N5dov3kmHUN8ohxFKdxymFRbxi1xeGJz8OaRePT5BljyOZfTkH4uidgzVyeYo1XeV8a506MLqlyLMONbAy0sgPNMlICm6QImhNuDuwBc1gvgMBolD3BsWzYb34qs1ik30yRmVySIctO+ifW+KAeCB5zk0x+kP00jSnwxjgJKuCcK/mUTGGXJWgWnBLYdB+9F/myu7aDs3UHnO+3SdhOsiZWB9kFyP1q58MN7qYs9T8cKSNkkXFzyYVaeNhlC5qlqQp2FUJJGzVBTjq9crMZxYh1a19rsMvGGKuUYOWaagOt8s7RtVwhmxQJWrGMrPPPvkfosukwbzpL2vbk+2ezXZOBVtagWVCFgm2O7VfF8MoUCkP2jegD+72vZFpSOtgc3J9L2R3ssgfNAlMNm0eyzOFnIHLHPLGu41CaN4mvpluJQ17zLUIUEhShCE+eg5YI2z9vNAI0GzUlcCDdDdPsqw52vQDN8lAJm12cICcIR27yqBcXmS687GYZr5Yd7j8a5+ZikhazJodvnk3Dm5tljnjkARoidWFT9Ij94ocyG9VethYOJefJtaSDXW9As9CUwebkdEOnQD/5UPhnXgHz2jMll5lFgBKnA+vHtEdg0VgJ4rNeWEOW+3Y4NG4d4jxp9AXhqI847CU3wjj9CESeeUvis7k/nk9Jhl3WVnd1glJljXOIre/+oRSm2xBhrsYfebnqIzkciDIocJZhyZZE/m4OBpBzyYvGQfewHUpSuw3m5b2jY9xnUDoO8pXz5ESVXa96CZppqIItZDl4nrQ8XsghwgnvjO4dEaYJCQGKuwblDw9xcjtaxmHT/LLAq7dBb9tc4It3jYBzEjzVsOstaIaiFDbdj/3j5qi+ME7tIvPK+CcfuPvkbNwqQfbsH5fZJpTZkGHzcaPnwbDXbYRDo17S1bryVNFu1bDrVRsd17jYirI2O3Y/jbSVU19Z89+RHCjBXn8Xi9o3tj8iE+aTd4x+62P+tfQNC4HbbIZs0UxUTpTDqYykuqYkO5xrPNkaz7fNrtca7YJXrdnufXnpo0mHxsDuCFPOM2vu27R9EY1VHy05UiIzXpZx6yptNllRrNl+atv1g/bHru5kyR/fCb6bBiDYe1zOUZwe6BiVQsD2jR8IYyjNJRv+b0llCQ79OfxAGf0yhvSMJqSlQZF4mx0z0CRjMWUt5q6YpMbkd6QEOtyG51rqddWdKDTl1XjbZjAuOQkRiqXmfKVctNZNEfgPWd20LzLxOVnXKISHg+59NH3YoCBA+5NN0JMh88V5QObLTf7nlagEGDZpdh+NfslO+qF5CIZ/1wMUjMdJ48WpQt2oeEorGuqM3LdI5pZxPzx01b8QeXQ5/BSoz6WKJufxDomXehqdKA1aV6bZ7Bi5dBo5Pw6Gb8qlUdhu7rKILU/lPGl8jn8StdsDusFa+gFCY2ZVVtdJ75bPptdGVyM9VW02O1X8Tw4XiGHSXI36zA51pxKLJJ6npLQctM95VgpRPI2uRqqqNJsTv3PaaJ6C5H/wEvCMUPZ7m9edCezdCBVryYlC+cbZfcq/ClCo4oHejWRVw+ZqvMGau2CO7it+b05dqZGbFFTNczYFZ8OW3bxNfoe8qjsD+amqxlmDDUpWZ7/0kThSdEqnEaBqfWdrcpGmCdLL4NUyPsXT6AxEpUqzOeqSswzz+DMXzn3m/EBDngWGLM+SJ3r/apSAMtgJT2LHCKfCqo3iVd1ZSllZNe4+l7xlKgPo3dsmLz3QyRLJYFs57Ayeme8pHugcJVhqsD3QOYLmy0oJtgc6D9ClBNsDnSfoUoHtgVYAuhRge6AVgS522P8H8uSelMpsweoAAAAASUVORK5CYII=);
            background-repeat: no-repeat;
            background-size: 62px auto;
            text-decoration: none;
            background-position: 238px 0px;
        }

        .wrap_aside .showStudyItem {
            overflow: hidden;
            margin-top: 20px;
        }

        .wrap_aside .showStudyItem .item {
            width: 28%;
            height: 30px;
            background: white;
            border: 1px solid #EAEAEA;
            float: left;
            margin: 2%;
            color: #646464;
            font-size: 14px;
            text-align: center;
            line-height: 30px;
            display: block;
            text-decoration: none;
        }

        .wrap_aside .showStudyItem .item:active {
            background: #EAEAEA;
        }

        .wrap_aside .bottomPanel {
            width: 200px;
            height: 26px;
            text-align: center;
            font-size: 12px;
            line-height: 26px;
            margin: auto;
            color: #999;
            position: absolute;
            bottom: 20px;
            left: 0;
            right: 0
        }

        .wrap_aside .bottomPanel .item {
            width: 77px;
            display: inline-block;
            color: #646464;
            text-decoration: none;
            border: 1px solid #eaeaea;
            text-align: center;
            margin-right: 10px
        }

        .wrap_aside .bottomPanel .item:active {
            background: #EAEAEA;
        }

        .visibility_hidden {
            visibility: hidden;
            display: none;
        }

        .head {
            position: relative;
        }

        .content {
            transform: translatey(-35px);
            font-family: SimHei;
        }

        #logo {
            width: 100px;
        }

        #download {
            margin-left: auto;
            margin-right: auto;
            width: 230px;
        }

        #title {
            margin-left: 46px;
            margin-right: 46px;
            border-bottom: 1px solid #7A7B7E;
            padding: 10px;
            font-weight: 600;
            color: #7A7B7E;
        }

        #desc {
            text-align: left;
            margin-left: 46px;
            margin-right: 46px;
            padding: 12px 12px 30px 12px;
            font-size: 12px;
            line-height: 18px;
            color: #7C7C7C;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>

<body>
<div class="head text-center">
    <img src="{{asset('images/appa.png')}}" id="bg" style="width:100%;">

</div>
<div class="text-center content">
    <img src="{{asset('images/logo.png')}}" id="logo">
    <div id="title">未特商城</div>
    <div id="desc">
        未特商城以区块链技术为链条，以智能POS机为载体，连接所有线下实体商家，打造出一个全新的互惠共赢的支付生态系统。
    </div>
    <div>
        <div class="text-center">
            <img src="{{asset('images/button.png')}}" id="download">
        </div>
    </div>
</div>
<div id="shareMask" style="position:fixed;top:0;height:100%;width:100%;display:none;">
    <div id="toFriendMask"
         style="position:absolute;top:0;left:0;background:black;opacity:0.7;height:100%;width:100%"></div>
    <div id="toFriendContent" style="position:absolute;top:0;left:0;width:100%">
        <img src="{{asset('images/arrow_top.png')}}" style="float:right;width:50px;margin-right:27px;margin-top:5px;">
        <p style="color:white;text-align:center;font-size:15px;font-weight:bold;line-height:200%;margin-top:25%;">
            点击右上角<br>选择在浏览器打开下载</p>
    </div>
</div>
<script>

    var redirectUrl = {
        iosAppStore: 'https://itunes.apple.com/cn/app/id1398228674',
        androidDownload: '{{$download_url}}'
    };
    document.getElementById('download').onclick = downloadApp;

    //下载APP
    function downloadApp() {
        if (Service.UserAgent.isWeixin()) {
            // if (Service.UserAgent.isIos()) {
            //     location.href = redirectUrl.iosAppStore;
            // } else {
            androidWeixinDownload();
            // }
        } else {
            if (Service.UserAgent.isIos()) {
                location.href = redirectUrl.iosAppStore;
            } else {
                location.href = redirectUrl.androidDownload;
            }
        }
    }

    //安卓非微信自动下载
    window.onload = function () {
        if (!Service.UserAgent.isWeixin() && Service.UserAgent.isAndroid()) {
            setTimeout(function () {
                location.href = redirectUrl.androidDownload;
            }, 50);
        }
    }

    /**
     * 安卓微信下载提示
     */
    function androidWeixinDownload() {
        if (document.getElementById('shareMask').style.display == "block") {
            document.getElementById('shareMask').style.display = 'none';
        } else {
            document.getElementById('shareMask').style.display = 'block';
        }
    }

    window.Service = {};
    window.Service.UserAgent = {
        _userAgent: null,
        getUserAgent: function () {
            if (this._userAgent === null) {
                this._userAgent = navigator.userAgent;
            }
            return this._userAgent;
        },
        _checkUserAgent: function (agent) {
            return this.getUserAgent().indexOf(agent) !== -1
        },
        isMobile: function () {
            return !!this.getUserAgent().match(/Android|iPhone|Mobile/);
        },
        isWeixin: function () {
            return !!this.getUserAgent().match(/MicroMessenger\/(\d)*/);
        },
        isAndroid: function () {
            return this._checkUserAgent('Android');
        },
        isIos: function () {
            return this.getUserAgent().match(/iPhone|iPad/);
        },
        isQQBrowser: function () {
            return this._checkUserAgent('QQ');
        }
    };

    window.___mobile_sidebar_inited = false;


</script>


</body>
</html>