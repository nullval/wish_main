@extends('seller.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- RECENT PURCHASES -->
        <div class="panel table-responsive">
            <div class="panel-heading">
                <h3 class="panel-title">所属pos机</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            {{--<div class="panel-body no-padding">--}}
                <div class="col-sm-12">
                    @if(Session::has('status'))
                        <div class="alert alert-info"> {{Session::get('status')}}</div>
                    @endif
                </div>
                {{--<div class="col-sm-6">--}}
                    {{--<input type="text" id="terminalId" class="form-control" placeholder="终端号" style="width: 200px;float:left;" value="@if(isset($_GET['terminalId'])&&(!empty($_GET['terminalId']))){{$_GET['terminalId']}}@endif"/>--}}
                    {{--<input type="text" id="mobile" class="form-control" placeholder="绑定用户" style="width: 200px;float:left;" value="@if(isset($_GET['mobile'])&&(!empty($_GET['mobile']))){{$_GET['mobile']}}@endif"/>--}}
                    {{--<a type="submit" id="search" class="btn btn btn-info" style="float:left;"><i class="fa fa-search"></i> 搜索</a>--}}
                {{--</div>--}}
                {{--<div class="col-sm-6">--}}
                    {{--<a href="{{url('/pos/pos_add')}}" class="btn btn-info" style="float: right;"><i class="fa fa-plus"></i> 新增pos机</a>--}}
                {{--</div>--}}
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>pos机编号</th>
                        <th>终端号</th>
                        <th>所属机主</th>
                        {{--<th>绑定商家</th>--}}
                        {{--<th>创建时间</th>--}}
                        {{--<th>操作管理员</th>--}}
                        {{--<th>绑定时间</th>--}}
                        {{--<th>编辑</th>--}}
                    </tr>
                    </thead>
                    <form>
                        {{csrf_field()}}
                        <tbody>
                        @foreach($pos_list as $k=>$v)
                            <tr>
                                <td>{{$v['id']}}</td>
                                <td>{{$v['terminalId'] or '暂无'}}</td>
                                <td>{{$v['mobile'] or '暂无'}}</td>
                                {{--<td>{{$v['mobile'] or '暂无'}}</td>--}}
                                {{--<td>{{$v['created_at'] or '暂无'}}</td>--}}
                                {{--<td>{{$v['username'] or '暂无'}}</td>--}}
                                {{--<td>{{$v['bind_time'] or '暂无'}}</td>--}}
                                {{--<td>--}}
                                    {{--<a href="{{url('/pos/pos_add')}}?id={{$v['id']}}"><span class="label label-warning"><i class="fa fa-edit"></i> 编辑</span>--}}
                                    {{--</a>--}}
                                {{--</td>--}}
                          </tr>
                        @endforeach
                        </tbody>
                    </form>
                </table>
            {{--</div>--}}
            <div class="panel-footer">
                <div class="row">
                    {{ $pos_list->links() }}
                </div>
            </div>
        </div>
        <!-- END RECENT PURCHASES -->
    </div>

@endsection
@section('js')
    {{--<script>--}}
        {{--$(function(){--}}

            {{--$('#search').click(function(){--}}
                {{--location.href="{{url('/pos/pos_list')}}?terminalId="+$('#terminalId').val()+'&mobile='+$('#mobile').val();--}}
            {{--});--}}

        {{--});--}}
    {{--</script>--}}
@endsection