@extends('seller.layouts.app')
<style>
    ul li{
        list-style-type:none;
    }
</style>
@section('content')
    <div class="panel panel-headline">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px;font-weight: 300;">补全资料</h3>
                <div class="hr-line-solid"></div>
                <div class="alert alert-warning"> 开户行所在地区是指商家入驻填写银行卡信息中填写的银行卡开户行地址</div>
                @if ($errors->has('error'))
                    <div class="alert alert-danger"> {{ $errors->first('error') }}</div>
                @endif
                @if(Session::has('status'))
                    <div class="alert alert-info"> {{Session::get('status')}}</div>
                @endif
            </div>
            <form method="post"  action=" {{url('/yixian/yixian_submit')}} ">
                <div class="panel-body">
                    {{csrf_field()}}
                    @if(isset($_GET['id']))
                        <input type="hidden" value="{{$info->id or 0}}" name="id">
                    @endif

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">税务登记证号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="TaxRegistrationNo" @if(isset($info->is_submit)&&$info->is_submit == 1) readonly @endif  placeholder="税务登记证号"  value="{{$info->TaxRegistrationNo or old('TaxRegistrationNo')}}">
                            @if ($errors->has('TaxRegistrationNo'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('TaxRegistrationNo') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">开户行所在地区</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="bankarea"  @if(isset($info->is_submit)&&$info->is_submit == 1) readonly @endif placeholder="开户行所在地区"  value="{{$info->bankarea or old('bankarea')}}">
                            @if ($errors->has('bankarea'))
                                <span class="help-block m-b-none text-danger">{{ $errors->first('bankarea') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">银行卡正面照片</label>
                        <ul id="warp">
                            <li>
                                {{--<input type="hidden" value="" name="old_businessimage"/>--}}
                                <div style="text-align: left;" class="col-sm-4">
                                    {{--<img id="imgShow_WU_FILE_0" width="100%"  src="@if(old('businessimagebase64') != ""){{asset('/images/timg.jpg')}}@elseif(session('businessimagebase64') != '') {{session('img')}} @else {{asset('/images/timg.jpg')}}@endif" />--}}
                                    <img id="imgShow_WU_FILE_0_input" width="100%"  src="{{$info->bankUrl1 or asset('/images/timg.jpg') }}" />

                                </div>
                                <div class="col-sm-2" style="text-align: right;">
                                    @if(!isset($info->is_submit)||$info->is_submit == 0)
                                        <input type="file" id="imgShow_WU_FILE_0" name="businessimage0" />
                                        <input type="hidden" id="imgShow_WU_FILE_0_hid" name="bankUrl1" />
                                    @endif
                                    @if ($errors->has('bankUrl1'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('bankUrl1') }}</span>
                                    @endif

                                </div>


                            </li>
                        </ul>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">银行卡反面照片</label>
                        <ul id="warp">
                            <li>
                                {{--<input type="hidden" value="" name="old_businessimage"/>--}}
                                <div style="text-align: left;" class="col-sm-4">
                                    {{--<img id="imgShow_WU_FILE_0" width="100%"  src="@if(old('businessimagebase64') != ""){{asset('/images/timg.jpg')}}@elseif(session('businessimagebase64') != '') {{session('img')}} @else {{asset('/images/timg.jpg')}}@endif" />--}}
                                    <img id="imgShow_WU_FILE_1_input" width="100%"  src="{{$info->bankUrl2 or asset('/images/timg.jpg') }}" />
                                </div>
                                <div class="col-sm-2" style="text-align: right;">
                                    @if(!isset($info->is_submit)||$info->is_submit == 0)
                                        <input type="file" id="imgShow_WU_FILE_1" name="businessimage1" />
                                        <input type="hidden" id="imgShow_WU_FILE_1_hid" name="bankUrl2" />
                                    @endif
                                    @if ($errors->has('bankUrl2'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('bankUrl2') }}</span>
                                    @endif

                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">商户身份证正面照片</label>
                        <ul id="warp">
                            <li>
                                {{--<input type="hidden" value="" name="old_businessimage"/>--}}
                                <div style="text-align: left;" class="col-sm-4">
                                    {{--<img id="imgShow_WU_FILE_0" width="100%"  src="@if(old('businessimagebase64') != ""){{asset('/images/timg.jpg')}}@elseif(session('businessimagebase64') != '') {{session('img')}} @else {{asset('/images/timg.jpg')}}@endif" />--}}
                                    <img id="imgShow_WU_FILE_2_input" width="100%"  src="{{$info->ICurl1 or asset('/images/timg.jpg')}}" />

                                </div>
                                <div class="col-sm-2" style="text-align: right;">
                                    @if(!isset($info->is_submit)||$info->is_submit == 0)
                                        <input type="file" id="imgShow_WU_FILE_2" name="businessimage2" />
                                        <input type="hidden" id="imgShow_WU_FILE_2_hid" name="ICurl1" />
                                    @endif
                                    @if ($errors->has('ICurl1'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('ICurl1') }}</span>
                                    @endif
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">商户身份证反面照片</label>
                        <ul id="warp">
                            <li>
                                {{--<input type="hidden" value="" name="old_businessimage"/>--}}
                                <div style="text-align: left;" class="col-sm-4">
                                    {{--<img id="imgShow_WU_FILE_0" width="100%"  src="@if(old('businessimagebase64') != ""){{asset('/images/timg.jpg')}}@elseif(session('businessimagebase64') != '') {{session('img')}} @else {{asset('/images/timg.jpg')}}@endif" />--}}
                                    <img id="imgShow_WU_FILE_3_input" width="100%"  src="{{$info->ICurl2 or asset('/images/timg.jpg')}}" />

                                </div>
                                <div class="col-sm-2" style="text-align: right;">
                                    @if(!isset($info->is_submit)||$info->is_submit == 0)
                                        <input type="file" id="imgShow_WU_FILE_3" name="businessimage3" />
                                        <input type="hidden" id="imgShow_WU_FILE_3_hid" name="ICurl2" />
                                    @endif
                                    @if ($errors->has('ICurl2'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('ICurl2') }}</span>
                                    @endif
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label for="signin-text" class="col-sm-2 control-label" id="text_name">商户手持身份证正面照片</label>
                        <ul id="warp">
                            <li>
                                {{--<input type="hidden" value="" name="old_businessimage"/>--}}
                                <div style="text-align: left;" class="col-sm-4">
                                    {{--<img id="imgShow_WU_FILE_0" width="100%"  src="@if(old('businessimagebase64') != ""){{asset('/images/timg.jpg')}}@elseif(session('businessimagebase64') != '') {{session('img')}} @else {{asset('/images/timg.jpg')}}@endif" />--}}
                                    <img id="imgShow_WU_FILE_4_input" width="100%"  src="{{$info->merchantICurl or asset('/images/timg.jpg')}}" />

                                </div>
                                <div class="col-sm-2" style="text-align: right;">
                                    @if(!isset($info->is_submit)||$info->is_submit == 0)
                                        <input type="file" id="imgShow_WU_FILE_4" name="businessimage4" />
                                        <input type="hidden" id="imgShow_WU_FILE_4_hid" name="merchantICurl" />
                                    @endif
                                    @if ($errors->has('merchantICurl'))
                                        <span class="help-block m-b-none text-danger">{{ $errors->first('merchantICurl') }}</span>
                                    @endif
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="clearfix"></div>
                    <div class="hr-line-dashed"></div>
                    @if(!isset($info->is_submit)||$info->is_submit == 0)
                        <button type="submit" class="btn btn btn-info" style="float:right;"><i class="fa fa-paper-plane-o"></i> 提交</button>
                    @endif
                    {{--<a href="{{url('/pos/pos_list')}}" class="btn btn-success" style="float:right;margin-right:10px;"><i class="fa fa-angle-double-left"></i> 返回</a>--}}
                </div>
            </form>
        </div>
    </div>
    {{--遮罩层--}}
    <div id="mask" class="mask" ></div>
@endsection
@section('js')
<script>
    $(function () {
        $("#imgShow_WU_FILE_0,#imgShow_WU_FILE_1,#imgShow_WU_FILE_2,#imgShow_WU_FILE_3,#imgShow_WU_FILE_4").change(function(){
            name=$(this).attr('id');
            var v = $(this).val();
            var reader = new FileReader();
            reader.readAsDataURL(this.files[0]);
            reader.onload = function(e){
                $.ajax({
                    url:'{{url('upload_img')}}',
                    type:'POST', //GET
                    data:{
                        imagebase64:e.target.result
                    },
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(data){

                        if(data.code==0){
                            alert(data.message);
                        }else{
                            filepath=data.data.filepath;
//                            alert(filepath);
                            $('#'+name+'_input').attr('src',filepath);
                            $('#'+name+'_hid').val(filepath);
                        }
                    },

                })
            };



        });
    })
</script>
@endsection